package com.magneta.casino.services;

import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.UserRegistrationBean;

public interface UserRegistrationService {

	Long registerUser(UserRegistrationBean user) throws ServiceException;
	void resetPassword(Long userId,String oldPassword,String newPassword) throws ServiceException;
	void updatePassword(Long userId,String newPassword) throws ServiceException;
	boolean verifyPassword(Long userId,String password) throws ServiceException;
	void registerCorporate(UserRegistrationBean user, CorporateBean corporateInfo) throws ServiceException;
}
