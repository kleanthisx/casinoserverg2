package com.magneta.casino.services;

import java.sql.SQLException;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBlacklistBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UsersBlacklistService {
	
	public ServiceResultSet<UserBlacklistBean> getUserBlackLists(SearchContext<UserBlacklistBean> searchContext, int limit,int offset,SortContext<UserBlacklistBean> sortContext) throws ServiceException;
	public ServiceResultSet<UserBlacklistBean> getUserBlackLists(int limit,int offset) throws ServiceException;
	public UserBlacklistBean isUserBlackList(String username,String firstname,String lastname,String email,String countryCode) throws ServiceException;
	public UserBlacklistBean getUserBlacklist(Long blackListId) throws ServiceException;
	public int getUserBlackListsSize(SearchContext<UserBlacklistBean> searchContext) throws ServiceException;
	public boolean deleteBlacklistUser(UserBlacklistBean userBlacklistBean) throws ServiceException, SQLException;
	public void insertBlacklistUser(UserBlacklistBean userBlacklistBean) throws ServiceException;
	public void updateBlacklistUser(UserBlacklistBean userBlacklistBean) throws ServiceException;
}
