package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement 
public class RolePrivilegeBean implements Serializable,Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7226011820100066942L;
	private String privilege;
	private boolean allowWrite;

	public RolePrivilegeBean() {
		super();
	}
	
	public RolePrivilegeBean(RolePrivilegeBean orig) {
		this.privilege = orig.privilege;
		this.allowWrite = orig.allowWrite;
	}
	
	@Override
	public Object clone() {
		return new RolePrivilegeBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof RolePrivilegeBean)) {
			return false;
		}

		return this.privilege.equals(((RolePrivilegeBean)o).privilege);
	}
	
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	
	@XmlElement
	public String getPrivilege() {
		return privilege;
	}
	public void setAllowWrite(boolean allowWrite) {
		this.allowWrite = allowWrite;
	}
	
	@XmlElement
	public boolean isAllowWrite() {
		return allowWrite;
	}
	
	
	
}
