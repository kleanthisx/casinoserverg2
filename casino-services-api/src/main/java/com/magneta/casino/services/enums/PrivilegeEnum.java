package com.magneta.casino.services.enums;

public enum PrivilegeEnum {
	ALERTS,
	ARTICLES_WRITER,
	BACKOFFICE_ENTRY,
	CLIENT_CREDITS,
	COLLECT_PAYMENT,
	COMMENTS,
	CORPORATE,
	GAME_CONFIG,
	GAME_HISTORY,
	FINACIAL_PERIOD,
	FUNDS,
	PROFILES,
	REPORTS,
	ROLE,
	SYSTEM_ADMIN,
	TABLES,
	USER_BONUS;
	
	public String getId() {
		return this.name();
	}
}
