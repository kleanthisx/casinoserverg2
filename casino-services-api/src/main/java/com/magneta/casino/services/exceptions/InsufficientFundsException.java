package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class InsufficientFundsException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3236872479181027066L;
	
	public InsufficientFundsException() {
		super("Insufficient funds");
	}

	public InsufficientFundsException(String message) {
		super(message);
	}

}
