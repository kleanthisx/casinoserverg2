package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.builder.HashCodeBuilder;
import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("payout_transactions")
public class UserWithdrawalBean implements Serializable,Cloneable{
	private static final long serialVersionUID = 3680313980223667509L;
	
	private Long transactionId;
	private Date dateCompleted;
	private Double transactionAmount;
	private String userAccount;
	private Date transactionDate;
	private Long userId;
	private String type;
	private Long approvedBy;
	private String comment;
	private String referenceNo;
	
	public UserWithdrawalBean() {
		super();
	}
	
	protected UserWithdrawalBean(UserWithdrawalBean original) {
		super();
		this.transactionId = original.transactionId;
		this.dateCompleted = original.dateCompleted;
		this.transactionAmount = original.transactionAmount;
		this.userAccount = original.userAccount;
		this.transactionDate = original.transactionDate;
		this.userId = original.userId;
		this.type = original.type;
		this.approvedBy = original.getApprovedBy();
		this.comment = original.getComment();
		this.referenceNo = original.getReferenceNo();
	}
	
	@Override
	public Object clone() {
		return new UserWithdrawalBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserWithdrawalBean)) {
			return false;
		}
		return this.transactionId.equals(((UserWithdrawalBean)o).transactionId);
	}

	@XmlAttribute
	@SQLColumn(value="payout_id",isId=true,refreshOnInsert=true)
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long payoutId) {
		this.transactionId = payoutId;
	}

	@XmlElement
	@SQLColumn(value="time_completed", refreshOnInsert=true)
	public Date getDateCompleted() {
		return dateCompleted;
	}
	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}
	@XmlElement
	@SQLColumn("amount")
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double amount) {
		this.transactionAmount = amount;
	}

	@XmlElement
	@SQLColumn("ps_user_account")
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlElement
	@SQLColumn("ps_specific")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@XmlElement
	@SQLColumn("approve_user_id")
	public Long getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}
	
	@XmlElement
	@SQLColumn("comment")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@XmlElement
	@SQLColumn("ps_reference_no")
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
}
