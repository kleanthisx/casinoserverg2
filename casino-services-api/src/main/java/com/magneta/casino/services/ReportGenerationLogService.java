package com.magneta.casino.services;

import com.magneta.casino.services.beans.ReportGenerationLogBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface ReportGenerationLogService {

	void logReportGeneration(ReportGenerationLogBean reportLog) throws ServiceException;
	ServiceResultSet<ReportGenerationLogBean> getReportLogs(SearchContext<ReportGenerationLogBean> searchContext, int limit, int offset, SortContext<ReportGenerationLogBean> sortContext) throws ServiceException;
	int getReportLogsSize(SearchContext<ReportGenerationLogBean> searchContext)
			throws ServiceException;
}
