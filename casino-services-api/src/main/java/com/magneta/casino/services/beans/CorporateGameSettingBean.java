package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("corporate_game_settings")
public class CorporateGameSettingBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1618340381710778899L;
	private Long corporateId;
	private Integer gameId;
	private Double minBet;
	private Double maxBet;
	
	public CorporateGameSettingBean() {
		super();
	}
	
	public CorporateGameSettingBean(CorporateGameSettingBean o) {
		this.corporateId = o.getCorporateId();
		this.gameId = o.getGameId();
		this.minBet = o.getMinBet();
		this.maxBet = o.getMaxBet();
	}
	
	@SQLColumn(value="corporate_id", isId=true)
	@XmlElement
	public Long getCorporateId() {
		return corporateId;
	}
	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}
	
	@SQLColumn(value="game_id", isId=true)
	@XmlElement
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	
	@SQLColumn("min_bet")
	@XmlElement
	public Double getMinBet() {
		return minBet;
	}
	public void setMinBet(Double minBet) {
		this.minBet = minBet;
	}
	
	@SQLColumn("max_bet")
	@XmlElement
	public Double getMaxBet() {
		return maxBet;
	}
	public void setMaxBet(Double maxBet) {
		this.maxBet = maxBet;
	}
	
	@Override
	public Object clone() {
		return new CorporateGameSettingBean(this);
	}
}
