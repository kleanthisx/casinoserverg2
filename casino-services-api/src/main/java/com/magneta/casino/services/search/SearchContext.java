package com.magneta.casino.services.search;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.search.SearchCondition;

public interface SearchContext<T> {
   SearchCondition<T> getCondition() throws ServiceException;
   String getExpression();
}
