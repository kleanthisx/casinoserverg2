package com.magneta.casino.services.search;

import java.lang.reflect.Array;
import java.text.CharacterIterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Date;
import java.util.Iterator;

public class FiqlContextBuilder {

	private static final String[] DATE_FORMATS = {
    	"yyyy-MM-dd'T'HH:mm:ss.SSSZ",
    	"yyyy-MM-dd"
    };
	
	private static void appendParameter(StringBuilder builder, Object o) {
		
		if (o == null) {
			builder.append("NULL");
		} else if (o instanceof SearchContext) {
			SearchContext<?> ctx = (SearchContext<?>)o;
			builder.append('(').append(ctx.getExpression()).append(')');
		} else if (o instanceof Date) {
			DateFormat df = new SimpleDateFormat(DATE_FORMATS[0]);
			builder.append(df.format((Date)o));
		} else if (o instanceof Iterable) {
			Iterable<?> iterable = (Iterable<?>)o;
			
			Iterator<?> iter = iterable.iterator();
			
			builder.append('{');
			boolean first = true;
			
			while(iter.hasNext()) {
				if (!first) {
					builder.append(',');
				} else {
					first = false;
				}
				
				appendParameter(builder, iter.next());
			}
			
			builder.append('}');
		} else if (o.getClass().isArray()) {
			int nElements = java.lang.reflect.Array.getLength(o);
			
			builder.append('{');
			boolean first = true;
			
			for (int i=0; i < nElements; i++) {
				if (!first) {
					builder.append(',');
				} else {
					first = false;
				}
				appendParameter(builder, Array.get(o, i));
			}
			
			builder.append('}');
			
		} else {
			String out = o.toString();

			CharacterIterator it = new StringCharacterIterator(out);

			for (char ch=it.first(); ch != CharacterIterator.DONE; ch=it.next()) {
				if (ch == '\\' || ch == ')' || ch == ';' || ch == ',') {
					builder.append('\\');
				}

				builder.append(ch);
			}
		}
	}
	
	/**
	 * Builds an FIQL SearchContext
	 * @param <T> The bean type to search
	 * @param cls The bean class to search 
	 * @param fmt The FIQL query with ? for each parameter
	 * @param args The parameters
	 * @return
	 */
	public <T> SearchContext<T> build(Class<T> cls, String fmt, Object ... args) {
		return FiqlContextBuilder.create(cls, fmt, args);
	}
	
	public static <T> SearchContext<T> create(Class<T> cls, String fmt, Object ... args) {
		StringBuilder expression = new StringBuilder();
		int argIndex = 0;
		
		CharacterIterator it = new StringCharacterIterator(fmt);
		
		for (char ch=it.first(); ch != CharacterIterator.DONE; ch=it.next()) {
			if (ch == '?') {
				if (args == null || args.length <= argIndex) {
					throw new RuntimeException("Parameter " + (argIndex+1) + " not found!");
				}
				
				appendParameter(expression, args[argIndex]);
				argIndex++;
			} else {
				expression.append(ch);
			}
		}
		
		if (argIndex > 0 && args.length > argIndex) {
			throw new RuntimeException("More parameters than expected");
		}
		
		return new SearchContextImpl<T>(cls, expression.toString());
	}
}
