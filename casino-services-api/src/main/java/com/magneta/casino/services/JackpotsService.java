package com.magneta.casino.services;

import com.magneta.casino.services.beans.JackpotBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface JackpotsService {

	ServiceResultSet<JackpotBean> getActiveJackpots(SearchContext<JackpotBean> searchContext,SortContext<JackpotBean> sortContext,int limit,int offset) throws ServiceException;
	ServiceResultSet<JackpotBean> getActiveJackpots(int limit,int offset) throws ServiceException;
	ServiceResultSet<JackpotBean> getJackpots(SearchContext<JackpotBean> searchContext,SortContext<JackpotBean> sortContext,int limit,int offset) throws ServiceException;
	ServiceResultSet<JackpotBean> getJackpots(int limit,int offset) throws ServiceException;
	JackpotBean getJackpot(Integer jackpotId) throws ServiceException;
	int getActiveJackpotsSize(SearchContext<JackpotBean> searchContext) throws ServiceException;
	int getJackpotsSize(SearchContext<JackpotBean> searchContext) throws ServiceException;
}
