package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable(value="user_balance", expression=true)
public class UserBalanceBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 2786499267297727868L;
	private Long userId;
	private Double balance;
	private Double playCredits;
	
	public UserBalanceBean() {
		super();
	}
	
	public UserBalanceBean(UserBalanceBean original) {
		super();
		this.setUserId(original.getUserId());
		this.setBalance(original.getBalance());
		this.setPlayCredits(original.getPlayCredits());
	}
	
	@Override
	public Object clone() {
		return new UserBalanceBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserBalanceBean)) {
			return false;
		}
		
		return this.getBalance().equals(((UserBalanceBean)o).getBalance());
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlAttribute
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@XmlElement
	@SQLColumn("balance")
	public Double getBalance() {
		return balance;
	}

	public void setPlayCredits(Double playCredits) {
		this.playCredits = playCredits;
	}

	@XmlElement
	@SQLColumn("play_credits")
	public Double getPlayCredits() {
		return playCredits;
	}
}
