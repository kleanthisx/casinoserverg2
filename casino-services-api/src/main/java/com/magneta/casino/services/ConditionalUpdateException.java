package com.magneta.casino.services;


public class ConditionalUpdateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3157948591160772043L;
	
	public ConditionalUpdateException() {
		super();
	}

	public ConditionalUpdateException(String message, Throwable e) {
		super(message, e);
	}
	
	public ConditionalUpdateException(String message) {
		super(message);
	}
}
