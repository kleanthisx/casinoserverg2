package com.magneta.casino.services.enums;

public enum UserTypeEnum {

	NORMAL_USER(0),
	STAFF_USER(3),
	CORPORATE_ACCOUNT(4),
	CORPORATE_PLAYER(5),
	DEMO_USER(6),
	SYSTEM(1024);

	private final int id;
	
	UserTypeEnum(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public static UserTypeEnum valueOf(int val) {
		for (UserTypeEnum v: UserTypeEnum.values()) {
			if (v.id == val) {
				return v;
			}
		}
		throw new IllegalArgumentException("Invalid UserTypeEnum value");
	}
	
	public boolean isSynthetic() {
		return this.id > 100;
	}
}