package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement 
public class SizeBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 6139457118649726312L;
	private Integer size;
	
	public SizeBean() {
		super();
	}
	
	
	public SizeBean(SizeBean original) {
		super();
		this.setSize(original.getSize());
	}
	
	
	@Override
	public Object clone() {
		return new SizeBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	public void setSize(Integer size) {
		this.size = size;
	}

	@XmlElement
	public Integer getSize() {
		return size;
	}
	
}
