package com.magneta.casino.services.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement
public class LocationBean extends CountryBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3261237736098100599L;

	private String city;
	private String region;
	private String timeZone;
	
	public LocationBean() {
		super();
	}
	
	public LocationBean(CountryBean original) {
		super(original);
	}
	
	public LocationBean(LocationBean original) {
		super(original);
		this.setCity(original.getCity());
	}
	

	@Override
	public Object clone() {
		return new LocationBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof LocationBean)) {
			return false;
		}
		return this.getCode().equals(((LocationBean)o).getCode());
	}

	public void setCity(String city) {
		this.city = city;
	}

	@XmlElement
	public String getCity() {
		return city;
	}

	@XmlElement
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@XmlElement
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
}
