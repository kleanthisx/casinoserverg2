package com.magneta.casino.services.utils;

import java.lang.reflect.Method;

import com.magneta.casino.services.annotations.SQLColumn;

public class BeanUtil {

	private static Method getMethod(Class<? extends Object> k, String methodName) {
		try {
			return k.getMethod(methodName);
		} catch (SecurityException e) {
			return null;
		} catch (NoSuchMethodException e) {
			return null;
		}
	}
	
	private static Method getSetterMethod(Class<? extends Object> k, String methodName, Class<?> param) {
		try {
			return k.getMethod(methodName, param);
		} catch (SecurityException e) {
			return null;
		} catch (NoSuchMethodException e) {
			return null;
		}
	}
	
	public static String getPropertyName(Method getter) {
		String name = getter.getName();
		
		if (name.startsWith("get")) {
			return Character.toLowerCase(name.charAt(3)) +  name.substring(4);
		} else if (name.startsWith("is")) {
			return Character.toLowerCase(name.charAt(2)) +  name.substring(3);
		}
		
		return null;
	}
	
	public static Method getGetter(Class<? extends Object> k, String property) {
		if (k == null || property == null) {
			return null;
		}
		
		Method m;
		
		m = getMethod(k, "get" + property);
		if (m == null) {
			m = getMethod(k, "get" + Character.toUpperCase(property.charAt(0)) + property.substring(1)); 
		}
		
		if (m == null) {
			m = getMethod(k, "is" + property);
		}
		
		if (m == null) {
			m = getMethod(k, "is" + Character.toUpperCase(property.charAt(0)) + property.substring(1)); 
		}
		
		return m;
	}
	
	public static Method getSetter(Class<? extends Object> k, String property, Class<?> valueType) {
		if (k == null || property == null) {
			return null;
		}
		
		Method m;
		
		m = getSetterMethod(k, "set" + property, valueType);
		if (m == null) {
			m = getSetterMethod(k, "set" + Character.toUpperCase(property.charAt(0)) + property.substring(1), valueType); 
		}
		
		return m;
	}
	
	public static String getColumnName(Method m, String property) {
		if (m != null) {
			SQLColumn col = m.getAnnotation(SQLColumn.class);
			if (col != null) {
				return col.value();
			}			
		} 
		return null;
	}
}
