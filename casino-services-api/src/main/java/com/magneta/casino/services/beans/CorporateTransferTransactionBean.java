package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@SQLTable("corporate_deposit_transfers")
public class CorporateTransferTransactionBean implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2798298739978691300L;
	private Long transferId;
	private Date transferDate;
	private Long transferUser;
	private Long userId;
	private Double amount;
	private String description;

	public CorporateTransferTransactionBean() {
		super();
	}

	public CorporateTransferTransactionBean(CorporateTransferTransactionBean orig) {
		this.transferId = orig.getTransferId();
		this.transferDate = orig.getTransferDate();
		this.transferUser = orig.getTransferUser();
		this.userId = orig.getUserId();
		this.amount = orig.getAmount();
		this.description = orig.getDescription();
	}

	@Override
	public Object clone() {
		return new CorporateTransferTransactionBean(this);
	}

	@SQLColumn(value="transfer_id", isId=true, refreshOnInsert=true)
	public Long getTransferId() {
		return transferId;
	}

	public void setTransferId(Long transferId) {
		this.transferId = transferId;
	}

	@SQLColumn("transfer_date")
	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	@SQLColumn("transfer_user")
	public Long getTransferUser() {
		return transferUser;
	}

	public void setTransferUser(Long transferUser) {
		this.transferUser = transferUser;
	}

	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@SQLColumn("amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@SQLColumn("transfer_desc")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
