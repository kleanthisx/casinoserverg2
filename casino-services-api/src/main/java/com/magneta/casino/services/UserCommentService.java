package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserCommentBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserCommentService {

	UserCommentBean getComment(Long userId, Long commentId) throws ServiceException;

	ServiceResultSet<UserCommentBean> getComments(Long userId,
			SearchContext<UserCommentBean> searchContext, int limit,
			int offset, SortContext<UserCommentBean> sortContext)
			throws ServiceException;

	int getCommentsSize(Long userId,
			SearchContext<UserCommentBean> searchContext)
			throws ServiceException;

	void createComment(UserCommentBean comment) throws ServiceException;
	void deleteComment(Long userId, Long commentId) throws ServiceException;

	void editComment(UserCommentBean comment) throws ServiceException;
}
