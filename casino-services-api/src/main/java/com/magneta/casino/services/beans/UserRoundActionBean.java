package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;


@XmlRootElement 
public class UserRoundActionBean implements Serializable, Cloneable  {
	private static final long serialVersionUID = 3155308012771340724L;

	private Long userId;
	private Integer actionId;
	private String actionDescription;
	private String actionValue;
	private Double amount;
	private Integer seatId;
	private Date actionTime;
	private Date transactionTime;
	private String actionArea;
	private Integer subActionId;
	private Double newBalance;
	
	public UserRoundActionBean() {
		super();
	}
	
	public UserRoundActionBean(UserRoundActionBean original) {
		super();
		this.userId = original.getUserId();
		this.actionId = original.getActionId();
		this.actionDescription = original.getActionDescription();
		this.actionValue = original.getActionValue();
		this.amount = original.getAmount();
		this.seatId = original.getSeatId();
		this.actionTime = original.getActionTime();
		this.transactionTime = original.getTransactionTime();
		this.actionArea = original.getActionArea();
		this.subActionId = original.getSubActionId();
		this.newBalance = original.getNewBalance();
	}
	
	@Override
	public Object clone() {
		return new UserRoundActionBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserRoundActionBean)) {
			return false;
		}
		return this.hashCode() == ((UserRoundActionBean)o).hashCode();
	}
	
	
	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlAttribute
	@SQLColumn("action_id")
	public Integer getActionId() {
		return actionId;
	}
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}
	
	@XmlElement
	@SQLColumn("action_desc")
	public String getActionDescription() {
		return actionDescription;
	}
	public void setActionDescription(String actionDescription) {
		this.actionDescription = actionDescription;
	}
	
	@XmlElement
	@SQLColumn("action_value")
	public String getActionValue() {
		return actionValue;
	}

	public void setActionValue(String actionValue) {
		this.actionValue = actionValue;
	}
	@XmlElement
	@SQLColumn("amount")
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@XmlElement
	@SQLColumn("seat_id")
	public Integer getSeatId() {
		return seatId;
	}
	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}
	
	@XmlElement
	@SQLColumn("action_time")
	public Date getActionTime() {
		return actionTime;
	}
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
	
	@XmlElement
	@SQLColumn("user_transactions.transaction_time")
	public Date getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	@XmlElement
	@SQLColumn("game_round_actions.action_area")
	public String getActionArea() {
		return actionArea;
	}
	public void setActionArea(String actionArea) {
		this.actionArea = actionArea;
	}
	
	@XmlElement
	@SQLColumn("game_round_actions.sub_action_id")
	public Integer getSubActionId() {
		return subActionId;
	}
	public void setSubActionId(Integer subActionId) {
		this.subActionId = subActionId;
	}
	
	@XmlElement
	@SQLColumn("user_transactions.new_balance")
	public Double getNewBalance() {
		return newBalance;
	}
	public void setNewBalance(Double newBalance) {
		this.newBalance = newBalance;
	}
	

}
