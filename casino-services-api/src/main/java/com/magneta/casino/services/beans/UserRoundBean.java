package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable(value="game_rounds AS rounds INNER JOIN game_tables AS tables ON rounds.table_id=tables.table_id",expression=true)
public class UserRoundBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 6861458996456869342L;

	private Long tableId;
	private Integer roundId;
	
	private Date dateOpened;
	private Date dateClosed;
	
	private Integer gameId;
	private Long gameConfigId;
	
	public UserRoundBean() {
		super();
	}
		
	public UserRoundBean(UserRoundBean original) {
		super();
		this.roundId = original.getRoundId();
		this.tableId = original.getTableId();
		this.dateOpened = original.getDateOpened();
		this.dateClosed = original.getDateClosed();
		this.gameId = original.getGameId();
		this.gameConfigId = original.getGameConfigId();
	}
	
	@Override
	public Object clone() {
		return new UserRoundBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserRoundBean)) {
			return false;
		}

		String uniqueChar = Integer.toString(this.getRoundId()) + "-" + Long.toString(this.getTableId());
		String oUnichar = Integer.toString(((UserRoundBean)o).getRoundId()) + "-" + Long.toString(((UserRoundBean)o).getTableId());
		return uniqueChar.equals(oUnichar);
	}

	@XmlAttribute
	@SQLColumn("tables.table_id")
	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}
	
	@XmlAttribute
	@SQLColumn("rounds.round_id")
	public Integer getRoundId() {
		return roundId;
	}

	public void setRoundId(Integer roundId) {
		this.roundId = roundId;
	}

	@XmlElement
	@SQLColumn("rounds.date_opened")
	public Date getDateOpened() {
		return dateOpened;
	}

	public void setDateOpened(Date dateOpened) {
		this.dateOpened = dateOpened;
	}

	@XmlElement
	@SQLColumn("tables.game_type_id")
	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}
	@XmlElement
	@SQLColumn("rounds.date_closed")
	public Date getDateClosed() {
		return dateClosed;
	}

	public void setGameConfigId(Long gameConfigId) {
		this.gameConfigId = gameConfigId;
	}

	@XmlElement
	@SQLColumn("rounds.game_config_id")
	public Long getGameConfigId() {
		return gameConfigId;
	}
}
