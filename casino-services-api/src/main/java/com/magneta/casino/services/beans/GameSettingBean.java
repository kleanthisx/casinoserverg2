package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("game_types")
public class GameSettingBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1618340381710778899L;
	private Integer gameId;
	private Double minBet;
	private Double maxBet;
	
	public GameSettingBean() {
		super();
	}
	
	public GameSettingBean(GameSettingBean o) {
		this.gameId = o.getGameId();
		this.minBet = o.getMinBet();
		this.maxBet = o.getMaxBet();
	}
	
	@SQLColumn(value="game_type_id", isId=true)
	@XmlElement
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	
	@SQLColumn("min_bet")
	@XmlElement
	public Double getMinBet() {
		return minBet;
	}
	public void setMinBet(Double minBet) {
		this.minBet = minBet;
	}
	
	@SQLColumn("max_bet")
	@XmlElement
	public Double getMaxBet() {
		return maxBet;
	}
	public void setMaxBet(Double maxBet) {
		this.maxBet = maxBet;
	}
	
	@Override
	public Object clone() {
		return new GameSettingBean(this);
	}
}
