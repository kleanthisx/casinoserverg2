package com.magneta.casino.services.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class CollectionUtils {

	public static <T> List<T> copyList(List<T> original) {
		if(original == null) {
			return null;
		}
		
		List<T> copyList = new ArrayList<T>();
		ListIterator<T> iter = original.listIterator();
		
		while (iter.hasNext()) {
			copyList.add(iter.next());
		}
		return copyList;
	}
	
}
