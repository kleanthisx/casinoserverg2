package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("payment_type")
public class PaymentTypeBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String paymentType;
	private boolean enabled;
	private Integer weight;

	/**
	 * 
	 */
	public PaymentTypeBean() {
		super();
	}
	
	@Override
	public Object clone() {
		PaymentTypeBean clone = new PaymentTypeBean();
		clone.setEnabled(isEnabled());
		clone.setPaymentType(getPaymentType());
		clone.setWeight(getWeight());
		
		return clone;
	}
	
	/**
	 * @return the paymentType
	 */
	@XmlAttribute
	@SQLColumn(value="payment_type",isId=true)
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the enabled
	 */
	@XmlElement
	@SQLColumn("enabled")
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@XmlElement
	@SQLColumn("weight")
	public Integer getWeight() {
		return weight;
	}
	
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
}
