package com.magneta.casino.services;

import java.util.List;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserRoundActionBean;
import com.magneta.casino.services.beans.UserRoundBean;
import com.magneta.casino.services.beans.UserRoundFilteredActionBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserRoundsService {
	List<UserRoundActionBean> getRoundActions(Long userId,UserRoundBean roundBean)throws ServiceException;
	List<UserRoundFilteredActionBean> getFilteredRoundActions(Long userId,UserRoundBean roundBean)throws ServiceException;
	UserRoundBean getRound(Long userId,Long tableId,Integer roundId) throws ServiceException;
	ServiceResultSet<UserRoundBean> getRounds(Long userId,SearchContext<UserRoundBean> searchContext,int limit,int offset,SortContext<UserRoundBean> sortContext) throws ServiceException;
	int getRoundsSize(Long userId,SearchContext<UserRoundBean> searchContext)throws ServiceException;
}
