package com.magneta.casino.services.utils;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.enums.SettingTypeEnum;
import com.magneta.casino.services.exceptions.InvalidParameterException;

public class SettingsHelper {
	public static Integer getInteger(SettingBean setting) throws ServiceException{
		if(setting == null) {
			throw new InvalidParameterException("Setting bean cannot be null");
		}
		if(setting.getTypeEnum() == SettingTypeEnum.DOUBLE) {
			Integer result = Integer.parseInt(setting.getValue());
			return result;
		}

		throw new InvalidParameterException("Could not parse value to an integer");
	}

	public static Double getDouble(SettingBean setting) throws ServiceException {
		if(setting == null) {
			throw new InvalidParameterException("Setting bean cannot be null");
		}
		if(setting.getTypeEnum() == SettingTypeEnum.DOUBLE) {
			Double result = Double.parseDouble(setting.getValue());
			return result;
		}

		throw new InvalidParameterException("Could not parse value to a double");
	}

	public static Boolean getBoolean(SettingBean setting) throws ServiceException {
		if(setting == null) {
			throw new InvalidParameterException("Setting bean cannot be null");
		}
		if(setting.getTypeEnum() == SettingTypeEnum.BOOLEAN) {
			Boolean result = Boolean.parseBoolean(setting.getValue());
			return result;
		}

		throw new InvalidParameterException("Could not parse value to a boolean");
	}
}
