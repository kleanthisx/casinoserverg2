package com.magneta.casino.services;

import com.magneta.casino.services.beans.GameTableBean;

public interface GameTableService {

	GameTableBean getActiveTable(Long userId, Integer gameId) throws ServiceException;
	GameTableBean getTable(Long tableId) throws ServiceException;
	
	void moveTable(Long tableId, Integer serverId) throws ServiceException;
	Integer getCurrentRoundId(Long tableId) throws ServiceException;
}
