package com.magneta.casino.services;

import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface PaymentTypeService {

	ServiceResultSet<PaymentTypeBean> getPaymentTypes(SearchContext<PaymentTypeBean> searchContext,int limit,int offset,SortContext<PaymentTypeBean> sortContext) throws ServiceException;
	PaymentTypeBean createPaymentType(PaymentTypeBean paymentTypeBean) throws ServiceException;
	void updatePaymentType(PaymentTypeBean paymentTypeBean) throws ServiceException;
	int getPaymentTypesSize(SearchContext<PaymentTypeBean> searchContext) throws ServiceException;
	PaymentTypeBean getPaymentType(String type) throws ServiceException;
}
