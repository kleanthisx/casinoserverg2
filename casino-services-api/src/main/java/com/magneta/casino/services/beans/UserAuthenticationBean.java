package com.magneta.casino.services.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.enums.UserTypeEnum;

@XmlRootElement
public class UserAuthenticationBean {

	private UserTypeEnum userType;
	private Long userId;
	private List<RolePrivilegeBean> privileges;
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@XmlElement
	public Long getUserId() {
		return userId;
	}
	
	public void setUserType(UserTypeEnum userType) {
		this.userType = userType;
	}
	
	@XmlElement
	public UserTypeEnum getUserType() {
		return userType;
	}
	
	public void setPrivileges(List<RolePrivilegeBean> privileges) {
		this.privileges = privileges;
	}
	
	@XmlElement
	public List<RolePrivilegeBean> getPrivileges() {
		return privileges;
	}
	
}
