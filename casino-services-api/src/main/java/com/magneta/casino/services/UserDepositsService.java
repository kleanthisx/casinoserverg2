package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserDepositBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserDepositsService {
	UserDepositBean getDeposit(Long depositId) throws ServiceException;
	UserDepositBean getDeposit(Long userId, Long depositId) throws ServiceException;
	ServiceResultSet<UserDepositBean> getDeposits(Long userId,SearchContext<UserDepositBean> searchContext,int limit,int offset,SortContext<UserDepositBean> sortContext) throws ServiceException;
	ServiceResultSet<UserDepositBean> getDeposits(Long userId,int limit, int offset) throws ServiceException;
	UserDepositBean getLastCompletedDeposit(Long userId,SearchContext<UserDepositBean> searchContext) throws ServiceException;
	int getDepositsSize(Long userId,SearchContext<UserDepositBean> searchContext) throws ServiceException;
	
	
	/**
	 * Creates a manual deposit.
	 * 
	 * @param deposit.
	 * @throws ServiceException
	 */
	void createManualDeposit(UserDepositBean deposit) throws ServiceException;
}
