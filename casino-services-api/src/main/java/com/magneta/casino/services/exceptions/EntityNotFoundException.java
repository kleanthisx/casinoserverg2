package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class EntityNotFoundException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1880201722209732713L;
	
	public EntityNotFoundException() {
		super("Entity not found");
	}

	public EntityNotFoundException(String message) {
		super(message);
	}
	
	public EntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
