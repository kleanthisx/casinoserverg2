package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class AccessDeniedException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public AccessDeniedException() {
		this("Access denied");
	}
	
	public AccessDeniedException(String message) {
		super(message);
	}

	public AccessDeniedException(String message, Throwable cause) {
		super(message, cause);
	}
}
