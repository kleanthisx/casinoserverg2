package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@SQLTable("web_logins")
@XmlRootElement 
public class WebLoginBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 6255412106455967247L;
	private Long webLoginId;
	private Long userId;
	private Date loginDate;
	private String applicationName;
    private String clientIp;
	
	public WebLoginBean() {
		super();
	}
	
	public WebLoginBean(WebLoginBean original) {
		super();
		this.webLoginId = original.getWebLoginId();
		this.clientIp = original.getClientIp();
		this.userId = original.getUserId();
		this.loginDate = original.getLoginDate();
		this.applicationName = original.getApplicationName();
	}

	@Override
	public Object clone() {
		return new WebLoginBean(this);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}
	
	
	@XmlElement
	@SQLColumn("login_time")
	public final Date getLoginDate() {
		return loginDate;
	}
	public final void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	@XmlElement
	@SQLColumn("application")
	public String getApplicationName() {
		return applicationName;
	}

	public void setWebLoginId(Long webLoginId) {
		this.webLoginId = webLoginId;
	}

	@XmlAttribute
	@SQLColumn(value="web_login_id",isId=true, refreshOnInsert=true)
	public Long getWebLoginId() {
		return webLoginId;
	}

	/**
	 * @param clientIp the clientIp to set
	 */
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	/**
	 * @return the clientIp
	 */
	@XmlElement
	@SQLColumn("client_ip")
	public String getClientIp() {
		return clientIp;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the userId
	 */
	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
}
