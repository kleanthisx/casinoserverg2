package com.magneta.casino.services.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class BeanTypeInspector<T> {

	/* Properties cache */
	private Map<String, BeanProperty<T,?>> properties = new HashMap<String, BeanProperty<T,?>>();
	
    private Class<T> tclass;
    
    public BeanTypeInspector(Class<T> klass) {
    	this.tclass = klass;
    }

    public BeanProperty<T,?> getProperty(String name) {
    	BeanProperty<T,?> prop = properties.get(name);
    	
    	if (prop == null) {
    		if (properties.containsKey(prop)) {
    			return null;
    		}
    		
    		prop = BeanProperty.createInstance(tclass, name);
    		properties.put(name, prop);
    	}
    	
    	return prop;
    }
    
    public Class<T> getBeanType() {
    	return this.tclass;
    }
    
    /**
     * Creates a bean. The bean must have a no-argument construct.
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public T createBean() throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
    	Constructor<T> constructor = this.tclass.getConstructor((Class[])null);
    	return constructor.newInstance((Object[])null);
    }
}
