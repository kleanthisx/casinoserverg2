package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class InvalidAuthenticationCredentialsException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public InvalidAuthenticationCredentialsException() {
		this("Invalid username or password");
	}
	
	public InvalidAuthenticationCredentialsException(String message) {
		super(message);
	}

	public InvalidAuthenticationCredentialsException(String message, Throwable cause) {
		super(message, cause);
	}
}
