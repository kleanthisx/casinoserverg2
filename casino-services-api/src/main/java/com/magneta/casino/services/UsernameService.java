package com.magneta.casino.services;

public interface UsernameService {

	public String getUsername(Long userId) throws ServiceException;
}
