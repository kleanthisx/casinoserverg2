package com.magneta.casino.services.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PhoneUtils {

	private static HashMap<String,String> codes = new HashMap<String,String>();

	static {
		/* Europe (3-4) */
		codes.put("30", "GR");
		codes.put("31", "NL");
		codes.put("32", "BE");
		codes.put("33", "FR");
		codes.put("34", "ES");
		codes.put("350", "GI");
		codes.put("351", "PT");
		codes.put("352", "LU");
		codes.put("353", "IE");
		codes.put("354", "IS");
		codes.put("355", "AL");
		codes.put("356", "MT");
		codes.put("357", "CY");
		codes.put("358", "FI");
		codes.put("35818","AX");
		codes.put("359", "BG");
		codes.put("36", "HU");
		codes.put("370", "LT");
		codes.put("371", "LV");
		codes.put("372", "EE");
		codes.put("373", "MD");
		codes.put("374", "AM");
		codes.put("375", "BY");
		codes.put("376", "AD");
		codes.put("377", "MC");
		codes.put("378", "SM");
		codes.put("379", "VA");
		codes.put("380", "UA");
		codes.put("381", "RS");
		codes.put("382", "ME");
		codes.put("385", "HR");
		codes.put("386", "SI");
		codes.put("387", "BA");
		codes.put("389", "MK");
		codes.put("39", "IT");
		codes.put("3906698", "VA");
		codes.put("40", "RO");
		codes.put("41", "CH");
		codes.put("420", "CZ");
		codes.put("421", "SK");
		codes.put("423", "LI");
		codes.put("43", "AT");
		codes.put("44", "GB");
		codes.put("441481", "GG");
		codes.put("441534", "JE");
		codes.put("441624", "IM");
		codes.put("45", "DK");
		codes.put("46", "SE");
		codes.put("47", "NO");
		codes.put("4779", "SJ");
		codes.put("48", "PL");
		codes.put("49", "DE");

		/* Latin America (5) */
		codes.put("54", "AR");
	}

	private static boolean isValidPhoneChar(char c) {
		return Character.isDigit(c) || c == ' ' || c == '-' || c == '(' || c == ')';
	}
	/**
	 * Checks whether the given string is a valid phone number
	 * @param phone
	 * @return
	 */
	public static boolean isValidPhone(String phone) {
		if (phone == null)
			return true;

		phone = phone.trim();

		if (phone.isEmpty())
			return true;

		char firstChar = phone.charAt(0); 

		if (firstChar == '+') {
			phone = phone.substring(1);
		}

		for (char c: phone.toCharArray()) {
			if (!isValidPhoneChar(c)) {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Normalizes a phone number. It removes all spaces and non-digit numbers except leading +
	 * @param phone
	 * @return
	 */
	public static String normalizePhone(String phone) {
		
		if (phone == null) {
			return null;
		}
		
		phone = phone.trim();
		if (phone.isEmpty())
			return null;
		
		StringBuilder builder = new StringBuilder();
		
		char[] chars = phone.toCharArray();
		
		if (chars[0] == '+' || Character.isDigit(chars[0])) {
			builder.append(chars[0]);
		}
		
		for (int i=1; i < chars.length; i++) {
			if (Character.isDigit(chars[i])) {
				builder.append(chars[i]);
			}
		}
		
		return builder.toString();
	}
	
	public static String getCountryCode(String phone) {
		boolean found = false;
		String countryCode = null;
		if(phone == null || phone.isEmpty()) {
			return null;
		}
		//If the phone is not in the correct format return
		if(!phone.startsWith("+")) {
			return null;
		}
		//Cut the first character +
		phone = phone.substring(1);
		do{
			countryCode = codes.get(phone);
			if(countryCode != null && !countryCode.isEmpty()) {
				found = true;
				break;
			}
			phone = phone.substring(0, phone.length() - 1);
		}while(!found && !phone.isEmpty());
		
		return countryCode;
	}
	
	public static String getPhoneWithoutCountryCode(String phone) {
		if(!phone.startsWith("+")) {
			return null;
		}
		String countryCode = getCountryCode(phone);
		if(countryCode == null || countryCode.isEmpty()) {
			return null;
		}
		//Cut the first character +
		phone = phone.substring(1);
		
		
		Set<String> keys = codes.keySet();
	    Iterator<String> iter = keys.iterator();
	    String numericCountryCode = null;
	    while (iter.hasNext()) {
	    	String value =  iter.next();
	    	if(codes.get(value).equalsIgnoreCase(countryCode)) { 
	    		numericCountryCode = value;
	    		break;
	    	}
	    }
	    if(numericCountryCode == null || numericCountryCode.isEmpty()) {
	    	return null;
	    }
	    return phone.substring(numericCountryCode.length());
	}
	
	public static boolean phoneIsValid(String phone) {
		String countryCode = PhoneUtils.getCountryCode(phone);
		if(countryCode == null || countryCode.isEmpty()) {
			return false;
		}
		return true;
	}
	
}
