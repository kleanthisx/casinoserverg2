package com.magneta.casino.services.search.parser;

import java.io.IOException;
import java.io.PrintStream;

import com.magneta.casino.services.search.FiqlParseException;
import com.magneta.casino.services.search.parser.ASTOperator.BooleanOp;

public class ASTComparison implements ASTNode {
	
	public enum CompareOp {
		EQUALS("=="),
	    NOT_EQUALS("!="),
		GREATER_THAN("=gt="),
	    GREATER_OR_EQUALS("=ge="),
	    LESS_THAN("=lt="),
	    LESS_OR_EQUALS("=le="),
	    EQUALS_IGNORE_CASE("~="),
	    NOT_EQUALS_IGNORE_CASE("!~="),
		IN_LIST("=in="),
		NOT_IN_LIST("!in=");
		
		private String op;
		
		private CompareOp(String op) {
			this.op = op;
		}
		
		public String getOp() {
			return op;
		}

		public boolean is(String s) {
			return op.equals(s);
		}
		
		public static CompareOp parse(String s) throws FiqlParseException {
			
			for (CompareOp c: CompareOp.values()) {
				if (c.is(s)) {
					return c;
				}
			}
			
			throw new FiqlParseException("Invalid comparison operator " + s);
		}
	}
	
	private String propertyName;
	private CompareOp op;
	private String sValue;
	
	public String getPropertyName() {
		return this.propertyName;
	}
	
	public CompareOp getOp() {
		return this.op;
	}
	
	public String getValue() {
		return this.sValue;
	}
	
	private static boolean isOperatorStart(char c) {
		return (c == '!' || c == '=' || c == '~');
	}
	
	/*
	 * attribute
	 * Ends with: ! =
	 */
	private void parseProperty(ParserContext s) throws FiqlParseException {
		char [] buf = new char[1];
		StringBuilder property = new StringBuilder();
		boolean inEscape = false;
		
		try {
			while (s.read(buf)) {
				if (inEscape) {
					property.append(buf[0]);
					inEscape = false;
					continue;
				}
				
				if (buf[0] == '\\') {
					inEscape = true;
					continue;
				}
				
				if (isOperatorStart(buf[0])) {
					/* Return the beginning of the operator to the reader */
					s.unread();
					this.propertyName = property.toString();
					return;
				}
				
				property.append(buf[0]);
			}
		} catch (IOException e) {
			s.parseException("I/O Error while parsing attribitute", e);
		}
		
		if (this.propertyName == null) {
			s.parseException("Expected property. Found \"" + property.toString() + "\"");
		}
	}
	
	/* compare op
	 * Ends with: =
	*/
	private void parseCompareOp(ParserContext s) throws FiqlParseException {
		char [] buf = new char[1];
		StringBuilder op = new StringBuilder();
		
		try {
			
			if (s.read(buf)) {
				op.append(buf[0]);
			}
			
			while (s.read(buf)) {
				op.append(buf[0]);
				if (buf[0] == '=') {
					this.op = CompareOp.parse(op.toString());
					return;
				}
			}
		} catch (IOException e) {
			s.parseException("I/O error while parsing operator", e);
		} catch (FiqlParseException e) {
			s.parseException("Expected comparison operator. Found " + op);
		}
		s.parseException("Expected comparison operator. Found " + op);
	}
	
	/*
	 * value
	 * Ends with: BooleanOp or )
	 * 
	 * Escape any character with \x where x is any character
	 */
	private void parseValue(ParserContext s) throws FiqlParseException {
		char [] buf = new char[1];
		StringBuilder value = new StringBuilder();
		boolean inEscape = false;
		
		try {
			while (s.read(buf)) {
				
				if (inEscape) {
					value.append(buf[0]);
					inEscape = false;
					continue;
				}
				
				if (buf[0] == '\\') {
					inEscape = true;
					continue;
				}

				if (buf[0] == ')' || BooleanOp.isAssignableFrom(buf[0])) {
					s.unread();
					break;
				}
				
				value.append(buf[0]);
			}
		} catch (IOException e) {
			s.parseException("I/O error while parsing value", e);
		}
		
		if (inEscape) {
			s.parseException("Escape character at the end of a value");
		}
		
		this.sValue = value.toString();
	}

	private void parseValueList(ParserContext s) throws FiqlParseException {
		char [] buf = new char[1];
		StringBuilder value = new StringBuilder();
		boolean inEscape = false;
		
		try {
			if (s.read(buf)) {
				if (buf[0] != '{') {
					s.parseException("Expected value list found: " + buf[0]);
				}
				value.append(buf[0]);
			}
			
			while (s.read(buf)) {
				
				if (inEscape) {
					value.append(buf[0]);
					inEscape = false;
					continue;
				}
				
				if (buf[0] == '\\') {
					inEscape = true;
					continue;
				}

				value.append(buf[0]);
				if (buf[0] == '}') {
					break;
				}
			}
		} catch (IOException e) {
			s.parseException("I/O error while parsing value", e);
		}
		
		if (inEscape) {
			s.parseException("Escape character at the end of a value");
		}
		
		this.sValue = value.toString();
	}
	
	@Override
	public void parse(ParserContext s) throws FiqlParseException {
		parseProperty(s);
		parseCompareOp(s);
		
		if (this.op.equals(CompareOp.IN_LIST) || this.op.equals(CompareOp.NOT_IN_LIST)) {
			parseValueList(s);
		} else {
			parseValue(s);
		}
	}
	
	@Override
	public void print(PrintStream out) {
		out.append("COMPARISON: ");
		out.append(this.propertyName);
		out.append(this.op.getOp());
		if (this.sValue == null) {
			out.append("<NULL>");
		} else {
			out.append(this.sValue);
		}
		out.append("\n");
		
	}
}
