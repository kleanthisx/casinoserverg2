package com.magneta.casino.services.beans;

import java.net.URL;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement 
public class GameDescriptorBean extends GameBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8698494593854825284L;
	private URL gameLogoUrl; 
	private URL gameLobbyUrl;

	
	public GameDescriptorBean() {
		super();
	}
	
	public GameDescriptorBean(GameBean bean) {
		super(bean);
	}
	
	public GameDescriptorBean(GameDescriptorBean bean) {
		super(bean);
		this.gameLogoUrl = bean.gameLogoUrl;
		this.gameLobbyUrl = bean.gameLobbyUrl;
	}
	
	@Override
	public Object clone() {
		return new GameDescriptorBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameDescriptorBean)) {
			return false;
		}
		
		return this.getGameId().equals(((GameDescriptorBean)o).getGameId());
	}

	public void setGameLogoUrl(URL gameLogoUrl) {
		this.gameLogoUrl = gameLogoUrl;
	}

	@XmlElement
	public URL getGameLogoUrl() {
		return gameLogoUrl;
	}

	public void setGameLobbyUrl(URL gameLobbyUrl) {
		this.gameLobbyUrl = gameLobbyUrl;
	}

	@XmlElement
	public URL getGameLobbyUrl() {
		return gameLobbyUrl;
	}
}
