package com.magneta.casino.services.search;

import java.lang.reflect.Method;

import com.magneta.casino.services.utils.BeanUtil;

public class PropertySort<T> {
	
	public enum SortOrder {
		ASC, DESC, NONE
	}
	
	private String property;
	private SortOrder order;
	private final Method getter;
	
	/**
	 * 
	 * @param klass
	 * @param property Case-sensitive
	 */
	public PropertySort(Class<T> klass, String property) {
		this.property = property;
		this.getter = BeanUtil.getGetter(klass, property);
	}

	public void setOrder(SortOrder order) {
		this.order = order;
	}

	public SortOrder getOrder() {
		return order;
	}
	
	public String getProperty() {
		return property;
	}
	
	public String getSQLColumnName() {
		return BeanUtil.getColumnName(getter, property);
	}
	
	private Object getValue(Method m, Object o) {
		try {
			return m.invoke(o);
		} catch (Exception e) {
			return null;
		}
	}
	
	public boolean shouldSwap(T a, T b) {
		if (order.equals(SortOrder.NONE)) {
			return false;
		}
		
		int diff = compare(a, b);
		
		if (order == SortOrder.ASC) {
			return diff > 0;
		} 
		return diff < 0;
	}
	
	@SuppressWarnings("unchecked")
	public int compare(T a, T b) { 
		Object aVal = getValue(getter, a);
		Object bVal = getValue(getter, b);

		if (aVal == null && bVal == null)
			return 0;
		
		if (aVal == null)
			return -1;
		
		if (bVal == null)
			return 1;
		
		if (aVal instanceof Comparable) {
			return ((Comparable<Object>)aVal).compareTo(bVal);
		}

		return 0;
	}
}
