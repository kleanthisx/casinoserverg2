package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("game_clients")
public class GameClientBean  implements Serializable, Cloneable {
	private static final long serialVersionUID = -1645933322161431755L;
	private Integer clientId;
	private String clientDescription;
	private Integer clientVersion;
	private String clientLatestVersion;
	
	public GameClientBean() {
		super();
	}
	
	public GameClientBean(GameClientBean original) {
		super();
		this.setClientId(original.getClientId());
		this.setClientDescription(original.getClientDescription());
		this.setClientVersion(original.getClientVersion());
		this.setClientLatestVersion(original.getClientLatestVersion());
	}
	
	@Override
	public Object clone() {
		return new GameClientBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameClientBean)) {
			return false;
		}
		return this.getClientId().equals(((GameClientBean)o).getClientId());
	}
	
	@XmlAttribute
	@SQLColumn(value="client_id", isId=true)
	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	@XmlElement
	@SQLColumn("client_description")
	public String getClientDescription() {
		return clientDescription;
	}

	public void setClientDescription(String clientDescription) {
		this.clientDescription = clientDescription;
	}

	@XmlElement
	@SQLColumn("client_version")
	public Integer getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(Integer clientVersion) {
		this.clientVersion = clientVersion;
	}

	@XmlElement
	@SQLColumn("client_latest_version")
	public String getClientLatestVersion() {
		return clientLatestVersion;
	}

	public void setClientLatestVersion(String clientLatestVersion) {
		this.clientLatestVersion = clientLatestVersion;
	}
}
