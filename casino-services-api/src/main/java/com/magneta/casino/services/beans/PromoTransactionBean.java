package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("promo_transactions")
public class PromoTransactionBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1670855757154511702L;
	private Long transactionId;
	private Long userId;
	private Double amount;
	private Double newBalance;
	private Double newPlayCredits;
	private Date transactionTime;
	
	public PromoTransactionBean() {
		super();
	}
	
	public PromoTransactionBean(PromoTransactionBean orig) {
		this.transactionId = orig.getTransactionId();
		this.userId = orig.getUserId();
		this.amount = orig.getAmount();
		this.newBalance = orig.getNewBalance();
		this.newPlayCredits = orig.getNewPlayCredits();
		this.transactionTime = orig.getTransactionTime();
	}
	
	@Override
	public Object clone() {
		return new PromoTransactionBean(this);
	}

	@XmlAttribute
	@SQLColumn(value="promo_transaction_id", isId=true)
	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlElement
	@SQLColumn("amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@XmlElement
	@SQLColumn(value="new_play_credits",refreshOnInsert=true)
	public Double getNewPlayCredits() {
		return newPlayCredits;
	}

	public void setNewPlayCredits(Double newPlayCredits) {
		this.newPlayCredits = newPlayCredits;
	}

	@XmlElement
	@SQLColumn(value="new_balance",refreshOnInsert=true)
	public Double getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(Double newBalance) {
		this.newBalance = newBalance;
	}

	@XmlElement
	@SQLColumn(value="transaction_time",refreshOnInsert=true)
	public Date getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}
}
