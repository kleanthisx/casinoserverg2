package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.net.URL;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement 
public class GameSymbolBean implements Serializable,Cloneable {
	private static final long serialVersionUID = -3768468064678381286L;
	private String symbolName;
	private URL symbolUrl;
	
	
	public GameSymbolBean() {
		super();
	}
	
	protected GameSymbolBean(GameSymbolBean bean) {
		this.symbolName = bean.symbolName;
		this.symbolUrl = bean.symbolUrl;
	}
	
	@Override
	public Object clone() {
		return new GameSymbolBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameSymbolBean)) {
			return false;
		}
		
		return this.hashCode() == (((GameSymbolBean)o).hashCode());
	}

	@XmlElement
	public String getSymbolName() {
		return symbolName;
	}

	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}

	@XmlElement
	public URL getSymbolUrl() {
		return symbolUrl;
	}

	public void setSymbolUrl(URL symbolUrl) {
		this.symbolUrl = symbolUrl;
	}
}
