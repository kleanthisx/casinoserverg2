package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class DatabaseException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public DatabaseException(String message) {
		super(message);
	}

	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
}
