package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;
import com.magneta.casino.services.enums.SettingTypeEnum;

@XmlRootElement
@SQLTable("system_settings")
public class SettingBean implements Serializable,Cloneable {

	private static final long serialVersionUID = 3192624873204837188L;
	private String key;
	private String value;
	private Integer type;
	private String description;
	private Boolean hide;
	private String settingSection;
	private String settingSubsection;
	
	public SettingBean() {
		super();
	}
	
	public SettingBean(SettingBean original) {
		super();
		this.key = original.key;
		this.value = original.value;
		this.type = original.type;
		this.description = original.description;
		this.hide = original.hide;
		this.settingSection = original.settingSection;
		this.settingSubsection = original.settingSubsection;
	}

	@Override
	public Object clone() {
		return new SettingBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof SettingBean)) {
			return false;
		}
		
		return this.getKey().equals(((SettingBean)o).getKey());
	}

	@XmlAttribute
	@SQLColumn(value="setting_key", isId=true)
	public String getKey() {
		return key;
	}

	
	public void setKey(String key) {
		this.key = key;
	}

	@XmlElement
	@SQLColumn("setting_value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlElement
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public SettingTypeEnum getTypeEnum() {
		try {
			return SettingTypeEnum.valueOf(this.getType());
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	@XmlElement
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement
	public Boolean getHide() {
		return hide;
	}

	public void setHide(Boolean hide) {
		this.hide = hide;
	}

	@XmlElement
	public String getSettingSection() {
		return settingSection;
	}

	public void setSettingSection(String section) {
		settingSection = section;
	}

	@XmlElement
	public String getSettingSubsection() {
		return settingSubsection;
	}

	public void setSettingSubsection(String subsection) {
		settingSubsection = subsection;
	}
}
