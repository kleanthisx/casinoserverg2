package com.magneta.casino.services;

import com.magneta.casino.services.beans.CorporateGameSettingBean;

public interface CorporateGameSettingsService {

	CorporateGameSettingBean getCorporateGameSettings(Long corporateId, Integer gameId) throws ServiceException;
	CorporateGameSettingBean getEffectiveCorporateGameSettings(Long corporateId, Integer gameId) throws ServiceException;
}
