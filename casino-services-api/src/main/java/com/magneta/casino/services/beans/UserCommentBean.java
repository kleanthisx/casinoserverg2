package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("user_comments")
public class UserCommentBean implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8448789657265882690L;

	private Long messageId;
	private Long userId;
	private Long commenterId;
	private Date dateCreated;
	private String comment;

	public UserCommentBean() {
		super();
	}
	
	protected UserCommentBean(UserCommentBean orig) { 
		
	}
	
	@Override
	public Object clone() {
		return new UserCommentBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@XmlAttribute
	@SQLColumn(value="message_id", isId=true, refreshOnInsert=true)
	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlElement
	@SQLColumn("commenter_id")
	public Long getCommenterId() {
		return commenterId;
	}

	public void setCommenterId(Long commenterId) {
		this.commenterId = commenterId;
	}

	@XmlElement
	@SQLColumn(value="date_created", refreshOnInsert=true)
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@XmlElement
	@SQLColumn("comment")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
