package com.magneta.casino.services.search.parser;

import java.io.PrintStream;

import com.magneta.casino.services.search.FiqlParseException;

/**
 * 
 * @author anarxia
 *
 */
public interface ASTNode {

	/** Parses the given reader
	 * This method should only be used by the FiqlParser.
	*/
	public void parse(ParserContext s) throws FiqlParseException;
	
	/** Prints the ASTNode (for debugging) */
	public void print(PrintStream out);
}
