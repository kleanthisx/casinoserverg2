package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;

@XmlRootElement 
public class JackpotBean  implements Serializable,Cloneable {
	private static final long serialVersionUID = -1182399464356956165L;
	private Integer jackpotId;
	private Integer gameId;
	private Date finishDate;
	private Integer payCount;
	private Double initialAmount;
	private Double betContribution;
	private Double betAmount;
	private Date startDate;
	private Double maximumAmount;
	private Boolean mysteryJackpot;
	private Long targetCorporate;
	private Integer configId;
	private Integer drawNumber;
	private Double minimumPayout;
	private Double minimumJackpotBalance;
	private Double balance;
	private Integer winners;
	
	
	public JackpotBean() {
		super();
	}

	public JackpotBean(JackpotBean original) {
		super();
		this.jackpotId = original.jackpotId;
		this.gameId = original.gameId;
		this.finishDate = original.finishDate;
		this.payCount = original.payCount;
		this.initialAmount = original.initialAmount;
		this.betContribution = original.betContribution;
		this.betAmount = original.betAmount;
		this.startDate = original.startDate;
		this.maximumAmount = original.maximumAmount;
		this.mysteryJackpot = original.mysteryJackpot;
		this.targetCorporate = original.targetCorporate;
		this.configId = original.configId;
		this.drawNumber = original.drawNumber;
		this.minimumPayout = original.minimumPayout;
		this.minimumJackpotBalance = original.minimumJackpotBalance;
		this.balance = original.balance;
	}

	@Override
	public Object clone() {
		return new JackpotBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof JackpotBean)) {
			return false;
		}

		return this.getJackpotId().equals(((JackpotBean)o).getJackpotId());
	}
	
	@XmlAttribute
	@SQLColumn("jackpots.jackpot_id")
	public Integer getJackpotId() {
		return jackpotId;
	}
	
	public void setJackpotId(Integer jackpotId) {
		this.jackpotId = jackpotId;
	}
	
	@XmlElement
	@SQLColumn("jackpots.game_id")
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	
	@XmlElement
	@SQLColumn("jackpots.finish_date")
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	@XmlElement
	@SQLColumn("jackpots.pay_count")
	public Integer getPayCount() {
		return payCount;
	}
	public void setPayCount(Integer payCount) {
		this.payCount = payCount;
	}
	
	@XmlElement
	@SQLColumn("jackpots.initial_amount")
	public Double getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(Double initialAmount) {
		this.initialAmount = initialAmount;
	}
	
	@XmlElement
	@SQLColumn("jackpots.bet_contrib")
	public Double getBetContribution() {
		return betContribution;
	}
	public void setBetContribution(Double betContribution) {
		this.betContribution = betContribution;
	}
	
	@XmlElement
	@SQLColumn("jackpots.bet_amount")
	public Double getBetAmount() {
		return betAmount;
	}
	
	public void setBetAmount(Double betAmount) {
		this.betAmount = betAmount;
	}
	
	@XmlElement
	@SQLColumn("jackpots.start_date")
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@XmlElement
	@SQLColumn("jackpots.maximum_amount")
	public Double getMaximumAmount() {
		return maximumAmount;
	}
	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}
	
	@XmlElement
	@SQLColumn("jackpots.auto")
	public Boolean getMysteryJackpot() {
		return mysteryJackpot;
	}
	public void setMysteryJackpot(Boolean mysteryJackpot) {
		this.mysteryJackpot = mysteryJackpot;
	}
	
	@XmlElement
	@SQLColumn("jackpots.target_corporate")
	public Long getTargetCorporate() {
		return targetCorporate;
	}
	public void setTargetCorporate(Long targetCorporate) {
		this.targetCorporate = targetCorporate;
	}
	
	@XmlElement
	@SQLColumn("jackpots.config_id")
	public Integer getConfigId() {
		return configId;
	}
	public void setConfigId(Integer configId) {
		this.configId = configId;
	}
	
	@XmlElement
	@SQLColumn("jackpots.draw_number")
	public Integer getDrawNumber() {
		return drawNumber;
	}
	public void setDrawNumber(Integer drawNumber) {
		this.drawNumber = drawNumber;
	}
	
	@XmlElement
	@SQLColumn("jackpots.min_payout")
	public Double getMinimumPayout() {
		return minimumPayout;
	}
	public void setMinimumPayout(Double minimumPayout) {
		this.minimumPayout = minimumPayout;
	}
	
	@XmlElement
	@SQLColumn("jackpots.min_jackpot_balance")
	public Double getMinimumJackpotBalance() {
		return minimumJackpotBalance;
	}
	public void setMinimumJackpotBalance(Double minimumJackpotBalance) {
		this.minimumJackpotBalance = minimumJackpotBalance;
	}
	
	@XmlElement
	@SQLColumn("jackpot_balances.balance")
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public void setWinners(Integer winners) {
		this.winners = winners;
	}

	@XmlElement
	@SQLColumn("jackpot_balances.winners")
	public Integer getWinners() {
		return winners;
	}
}
