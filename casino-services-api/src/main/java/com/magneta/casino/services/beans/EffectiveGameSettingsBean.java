package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EffectiveGameSettingsBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 408439334052438688L;
	private Double minBet;
	private Double maxBet;
	
	@XmlElement
	public Double getMinBet() {
		return minBet;
	}
	public void setMinBet(Double minBet) {
		this.minBet = minBet;
	}
	
	@XmlElement
	public Double getMaxBet() {
		return maxBet;
	}
	public void setMaxBet(Double maxBet) {
		this.maxBet = maxBet;
	}
}
