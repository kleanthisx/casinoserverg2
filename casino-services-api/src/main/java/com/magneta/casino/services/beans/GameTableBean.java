package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("game_tables")
public class GameTableBean implements Cloneable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6700333673995449716L;
	private Long tableId;
	private Integer gameId;
	
	private Long owner;
	private Date dateCreated;
	private Date dateClosed;

	
	private Integer serverId;
	
	public GameTableBean() {
		super();
	}
	
	public GameTableBean(GameTableBean orig) {
		this.tableId = orig.getTableId();
		this.gameId = orig.getGameId();
		this.owner = orig.getOwner();
		this.dateCreated = orig.getDateCreated();
		this.dateClosed = orig.getDateClosed();
	}
	
	@Override
	public Object clone() {
		return new GameTableBean(this);
	}

	@XmlAttribute
	@SQLColumn(value="table_id", isId=true, refreshOnInsert=true)
	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}

	@XmlElement
	@SQLColumn("game_type_id")
	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	@XmlElement
	@SQLColumn("table_owner")
	public Long getOwner() {
		return owner;
	}

	public void setOwner(Long owner) {
		this.owner = owner;
	}

	@XmlElement
	@SQLColumn("date_created")
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@XmlElement
	@SQLColumn("date_closed")
	public Date getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	@XmlElement
	@SQLColumn("server_id")
	public Integer getServerId() {
		return serverId;
	}

	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}
	
	@Override
	public int hashCode() {
		if (tableId == null) {
			return new Long(-1).hashCode();
		}
		return tableId.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameTableBean)) {
			return false;
		}
		
		GameTableBean other = (GameTableBean)o;
		
		if (this.tableId == null) {
			return this == o;
		}
		
		return this.tableId.equals(other.getTableId());
	}
}
