package com.magneta.casino.services.utils;

import java.io.IOException;
import java.util.List;

import com.magneta.casino.services.search.PropertySort;

public class SortUtil {
	
	private static void append(Appendable sb, CharSequence s) {
		try {
			sb.append(s);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static final <T> void appendSqlEpilogue(Appendable sb, List<PropertySort<T>> constraints, int offset, int limit) {
		boolean isFirst = true;

		if(constraints != null && constraints.size() > 0) {
			for (PropertySort<T> s: constraints) {
				if (s.getOrder().equals(PropertySort.SortOrder.NONE)) {
					continue;
				}

				String columnName = s.getSQLColumnName();
				if (columnName == null)
					continue;
				
				if (isFirst) {
					append(sb, " ORDER BY ");
					isFirst = false;
				} else {
					append(sb, ", ");
				}

				append(sb, columnName);

				if (s.getOrder() == PropertySort.SortOrder.ASC) {
					append(sb, " ASC NULLS FIRST");
				} else {
					append(sb, " DESC NULLS LAST");
				}
			}
		}
	
		if (limit > 0) {
			append(sb, " LIMIT ");
			append(sb, Integer.toString(limit));
		}

		if (offset > 0) {
			append(sb, " OFFSET ");
			append(sb, Integer.toString(offset));
		}
	}
}
