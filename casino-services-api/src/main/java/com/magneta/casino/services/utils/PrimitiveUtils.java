package com.magneta.casino.services.utils;
public final class PrimitiveUtils {
    
    private PrimitiveUtils() {
        
    }

    @SuppressWarnings("unchecked")
	public static <T> T read(String value, Class<T> type) {
        Object ret = value;

        if (Integer.TYPE.equals(type)) {
            ret = Integer.valueOf(value);
        } else if (Byte.TYPE.equals(type)) {
            ret = Byte.valueOf(value);
        } else if (Short.TYPE.equals(type)) {
            ret = Short.valueOf(value);
        } else if (Long.TYPE.equals(type)) {
            ret = Long.valueOf(value);
        } else if (Float.TYPE.equals(type)) {
            ret = Float.valueOf(value);
        } else if (Double.TYPE.equals(type)) {
            ret = Double.valueOf(value);
        } else if (Boolean.TYPE.equals(type)) {
            ret = Boolean.valueOf(value);
        } else if (Character.TYPE.equals(type)) {
            ret = value.charAt(0);
        }
        return (T)ret;
    }
}