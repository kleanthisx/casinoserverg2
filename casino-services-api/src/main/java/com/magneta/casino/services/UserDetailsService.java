package com.magneta.casino.services;

import java.util.List;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserDetailsService {
	UserDetailsBean getUser(Long userId) throws ServiceException;
	ServiceResultSet<UserDetailsBean> getUsers(SearchContext<UserDetailsBean> searchContext,int limit,int offset,SortContext<UserDetailsBean> sortContext) throws ServiceException;
	ServiceResultSet<UserDetailsBean> getUsers(int limit,int offset) throws ServiceException;
	void updateUser(UserDetailsBean user) throws ServiceException,ConditionalUpdateException;
	UserBalanceBean getUserBalance(Long userId) throws ServiceException;
	int getUsersSize(SearchContext<UserDetailsBean> searchContext) throws ServiceException;
	boolean deleteUser(UserDetailsBean userDetailsBean) throws ServiceException;
	void closeAccount(Long userId) throws ServiceException;
	
	List<UserBalanceBean> getUserBalances(List<Long> userIds)
			throws ServiceException;
}
