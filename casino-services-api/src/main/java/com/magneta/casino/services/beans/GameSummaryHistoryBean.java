package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable(value="game_period_amounts INNER JOIN game_tables ON game_period_amounts.table_id = game_tables.table_id",
		groupBy="game_period_amounts.user_id, game_period_amounts.period_date, game_tables.game_type_id",
		expression=true)
public class GameSummaryHistoryBean  implements Serializable,Cloneable {
	private static final long serialVersionUID = 6139457118649726312L;
	
	private Long userId;
	private Integer gameId;
	private Date periodDate;
	
	private Long rounds;
	
	private Double bets;
	private Double wins;
	
	private Double realBets;
	private Double realWins;
	private Double jackpotWins;
	
	public GameSummaryHistoryBean() {
		super();
	}
	
	public GameSummaryHistoryBean(GameSummaryHistoryBean original) {
		this.gameId = original.getGameId();
		this.bets = original.getBets();
		this.wins = original.getWins();
		this.realBets = original.getRealBets();
		this.realWins = original.getRealWins();
		this.jackpotWins = original.getJackpotWins();
		this.rounds = original.getRounds();
	}
	
	@Override
	public Object clone() {
		return new GameSummaryHistoryBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameSummaryHistoryBean)) {
			return false;
		}
		
		return this.getGameId().equals(((GameSummaryHistoryBean)o).getGameId());
	}

	@XmlAttribute
	@SQLColumn("game_type_id")
	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	@XmlElement
	@SQLColumn(value="SUM(bets)", expression=true, aggregate=true)
	public Double getBets() {
		return bets;
	}

	public void setBets(Double bets) {
		this.bets = bets;
	}

	@XmlElement
	@SQLColumn(value="SUM(wins)", expression=true, aggregate=true)
	public Double getWins() {
		return wins;
	}

	public void setWins(Double wins) {
		this.wins = wins;
	}

	@XmlElement
	@SQLColumn(value="SUM(jackpot_wins)", expression=true, aggregate=true)
	public Double getJackpotWins() {
		return jackpotWins;
	}

	public void setJackpotWins(Double jackpotWins) {
		this.jackpotWins = jackpotWins;
	}

	@XmlElement
	@SQLColumn(value="SUM(rounds)", expression=true, aggregate=true)
	public Long getRounds() {
		return rounds;
	}

	public void setRounds(Long rounds) {
		this.rounds = rounds;
	}
	
	@XmlElement
	@SQLColumn("period_date")
	public Date getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	@XmlElement
	@SQLColumn(value="SUM(real_wins)", expression=true, aggregate=true)
	public Double getRealWins() {
		return realWins;
	}

	public void setRealWins(Double realWins) {
		this.realWins = realWins;
	}

	@XmlElement
	@SQLColumn(value="SUM(real_bets)", expression=true, aggregate=true)
	public Double getRealBets() {
		return realBets;
	}

	public void setRealBets(Double realBets) {
		this.realBets = realBets;
	}

	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}	
}
