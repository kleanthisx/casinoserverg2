/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.magneta.casino.services.search;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.services.search.parser.ASTOperator.BooleanOp;


/**
 * Composite BooleanOp search condition.
 * 
 *  SQL: (<cond0> ... <op> <condN>)
 */
public abstract class AbstractComplexCondition<T> implements SearchCondition<T> {

	protected final List<SearchCondition<T>> conditions;
	private final BooleanOp op;

	protected AbstractComplexCondition(BooleanOp op) {
		this.conditions = new ArrayList<SearchCondition<T>>();
		this.op = op;
	}

	public void addCondition(SearchCondition<T> condition) {
		this.conditions.add(condition);
	}

	public SearchCondition<T> getLastCondition() {
		if (this.conditions.isEmpty())
			return null;

		return this.conditions.get(this.conditions.size() - 1);
	}

	public int conditionCount() {
		return this.conditions.size();
	}

	@Override
	public List<T> findAll(Iterable<T> pojos) {
		List<T> result = new ArrayList<T>();
		for (T pojo : pojos) {
			if (isMet(pojo)) {
				result.add(pojo);
			}
		}
		return result;
	}

	@Override
	public int countAll(Iterable<T> pojos) {
		int count = 0;
		for (T pojo : pojos) {
			if (isMet(pojo)) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Boolean Operator this SearchCondition represents
	 * @return condition type
	 */
	 public final BooleanOp getOperator() {
		return op;
	}

	private final void append(Appendable sb, CharSequence s) {
		try {
			sb.append(s);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void appendSQLSearchString(Appendable sb) {
		boolean first = true;
		append(sb, "(");
		for (SearchCondition<T> sc : conditions) {
			if (!first) {
				try {
					sb.append(" ");

					switch(op) {
					case AND:
						sb.append("AND");
						break;
					case OR:
						sb.append("OR");
					default:
						throw new RuntimeException("Unsupported binary operator");
					}

					sb.append(" ");
				} catch (IOException e) {
					throw new RuntimeException(e);
				}

			} else {
				first = false;
			}


			sc.appendSQLSearchString(sb);
		}
		append(sb, ")");
	}

	@Override
	public int setSQLStatementValues(PreparedStatement stmt, int startIndex) throws SQLException {
		int count = 0;

		for (SearchCondition<T> sc : conditions) {
			count += sc.setSQLStatementValues(stmt, startIndex + count);
		}

		return count;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("(");  	
		boolean first = true;
		for (SearchCondition<T> sc : conditions) {
			if (!first) {
				sb.append(" " + op.toString() + " ");
			} else {
				first = false;
			}
			sb.append(sc.toString());
		}
		sb.append(")");

		return sb.toString();
	}
}
