package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBalanceAdjustmentBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserBalanceAdjustmentsService {

	ServiceResultSet<UserBalanceAdjustmentBean> getAdjustments(Long userId, SearchContext<UserBalanceAdjustmentBean> searchContext, int limit, int offset, SortContext<UserBalanceAdjustmentBean> sortContext) throws ServiceException;
	int getAdjustmentsSize(Long userId, SearchContext<UserBalanceAdjustmentBean> searchContext) throws ServiceException;
	UserBalanceAdjustmentBean getAdjustment(Long userId, Long adjustmentId) throws ServiceException;
	void createAdjustment(UserBalanceAdjustmentBean adjustment) throws ServiceException;
	UserBalanceAdjustmentBean clearBalance(Long userId) throws ServiceException;
	
}
