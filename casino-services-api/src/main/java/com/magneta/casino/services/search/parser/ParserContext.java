package com.magneta.casino.services.search.parser;

import java.io.IOException;
import java.io.Reader;

import com.magneta.casino.services.search.FiqlParseException;

public class ParserContext {

	private Reader r;
	private long pos;

	public ParserContext(Reader r) {
		this.r = r;
		this.pos = 0;
	}
	
	public boolean read(char[] buf) throws IOException {
		int bytesRead = r.read(buf, 0, buf.length);

		if (bytesRead == buf.length) {
			pos += bytesRead;
			return true;
		}

		return false;
	}
	
	public void unread() throws IOException {
		r.skip(-1);
		pos--;
	}
	
	public FiqlParseException parseException(Throwable e) throws FiqlParseException {
		throw new FiqlParseException("FIQL parse error at character " + pos, e);
	}
	
	public FiqlParseException parseException(String msg, Throwable e) throws FiqlParseException {
		throw new FiqlParseException("FIQL parse error at character " + pos + ": " + msg, e);
	}
	
	public FiqlParseException parseException(String msg) throws FiqlParseException {
		throw new FiqlParseException("FIQL parse error at character " + pos + ": " + msg);
	}
}
