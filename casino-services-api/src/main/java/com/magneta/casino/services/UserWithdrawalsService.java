package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserDepositBean;
import com.magneta.casino.services.beans.UserWithdrawalBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserWithdrawalsService {

	UserWithdrawalBean getWithdrawal(Long userId, Long withdrawalId) throws ServiceException;
	UserWithdrawalBean getWithdrawal(Long withdrawalId) throws ServiceException;
	
	ServiceResultSet<UserWithdrawalBean> getWithdrawals(Long userId,SearchContext<UserWithdrawalBean> searchContext,int limit,int offset,SortContext<UserWithdrawalBean> sortContext) throws ServiceException;
	ServiceResultSet<UserWithdrawalBean> getWithdrawals(Long userId,int limit, int offset) throws ServiceException;
	int getWithdrawalsSize(Long userId,SearchContext<UserWithdrawalBean> searchContext) throws ServiceException;

	String getUserAccount(UserDepositBean userDepositBean);
	
	void createManualWithdrawal(UserWithdrawalBean withdrawal) throws ServiceException;
}
