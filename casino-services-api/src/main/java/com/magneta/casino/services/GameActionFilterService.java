package com.magneta.casino.services;

import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.games.configuration.GameConfiguration;

public interface GameActionFilterService {

	ActionFilter getFilter(Integer gameId) throws ServiceException;
	ActionFilter getFilter(Integer gameId, GameConfiguration config) throws ServiceException;
}