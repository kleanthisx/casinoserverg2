package com.magneta.casino.services;


import com.magneta.casino.services.beans.GameSymbolBean;

public interface GameImagesService {
	public GameSymbolBean getGameImage(Integer gameId,String imageKey) throws ServiceException;
}
