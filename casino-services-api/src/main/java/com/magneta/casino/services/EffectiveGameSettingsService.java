package com.magneta.casino.services;

import com.magneta.casino.services.beans.EffectiveGameSettingsBean;

public interface EffectiveGameSettingsService {

	/**
	 * Returns the effective game settings for the given player.
	 * 
	 * @param userId UserId of the player
	 * @param corporateId The corporateId of the players immediate corporate
	 * @param gameId The game to retrieve the settings for.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	EffectiveGameSettingsBean getEffectiveGameSettings(Long userId,
			Long corporateId, Integer gameId) throws ServiceException;
}
