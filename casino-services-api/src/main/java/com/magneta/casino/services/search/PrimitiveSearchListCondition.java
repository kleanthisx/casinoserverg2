/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.magneta.casino.services.search;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.search.parser.ASTComparison.CompareOp;
import com.magneta.casino.services.utils.BeanProperty;

public class PrimitiveSearchListCondition<T> implements SearchCondition<T> {
    
    private BeanProperty<T,?> property;
    private Object[] propertyValues;
    private CompareOp op;
    
    public PrimitiveSearchListCondition(BeanProperty<T,?> property, 
                                    Object[] propertyValues,
                                    CompareOp op) {
        this.property = property;
        this.propertyValues = propertyValues;
        this.op = op;
    }
    
    @Override
	public List<T> findAll(Iterable<T> pojos) {
        List<T> result = new ArrayList<T>();
        for (T pojo : pojos) {
            if (isMet(pojo)) {
                result.add(pojo);
            }
        }
        return result;
    }
    
    @Override
    public int countAll(Iterable<T> pojos) {
    	int count = 0;
    	for (T pojo : pojos) {
    		if (isMet(pojo)) {
    			count++;
    		}
    	}
    	return count;
    }

    public CompareOp getComparisonOperator() {
        return op;
    }

    @Override
	public boolean isMet(T pojo) {
    	Object lValue = property.getValue(pojo);
    	
    	boolean foundInList = false;
    	
    	for (Object value: propertyValues) {
    		if (value == null && lValue == null) {
    			foundInList = true;
    			break;
    		}
    		
    		if (value == null || lValue == null)
    			continue;
    		
    		if (value.equals(lValue)) {
    			foundInList = true;
    			break;
    		}
    	}

    	if (op == CompareOp.NOT_IN_LIST) {
    		return !foundInList;
    	}
    	return foundInList;
    }

    @Override
	public void appendSQLSearchString(Appendable sb) {
    	String columnName = null;
    	String columnType = null;
    	
    	SQLColumn col = property.getAnnotation(SQLColumn.class);
    	if (col != null) {
    		columnName = col.value();
    		columnType = col.sqlType();
    		if ("".equals(columnType)) {
    			columnType = null;
    		}
    	}

    	if (columnName == null) {
    		columnName = property.getName();
    	}

    	SearchUtils.sqlApppendCondition(sb, columnName, op, propertyValues, columnType);
    }
    
    @Override
	public int setSQLStatementValues(PreparedStatement stmt, int startIndex) throws SQLException {

    	int index = startIndex;
    	
    	for (Object value: propertyValues) {
    		index += SearchUtils.setSQLStatementValue(stmt, index, op, value);
    	}

    	return index - startIndex;
    }
    
    @Override
	public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append(this.property.getName());
    	sb.append(this.op.getOp());
    	
    	for (Object value: this.propertyValues) {
    		sb.append(value == null?" <null>":" " + value);
    	}
    	
    	return sb.toString();
    }
}
