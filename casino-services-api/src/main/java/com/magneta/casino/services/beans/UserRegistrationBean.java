package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement 
public class UserRegistrationBean extends UserDetailsBean implements Serializable, Cloneable {

	private static final long serialVersionUID = 1848839687211978870L;
	private Long corporateId;
	private String password;
	
	public UserRegistrationBean() {
		super();
	}
	
	public UserRegistrationBean(UserDetailsBean original) {
		super(original);
	}

	public UserRegistrationBean(UserRegistrationBean original) {
		super(original);
		this.corporateId = original.getCorporateId();
		this.password = original.getPassword();
	}
	
	@Override
	public Object clone() {
		return new UserRegistrationBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}
	
	@XmlElement
	public Long getCorporateId() {
		return corporateId;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@XmlElement
	public String getPassword() {
		return password;
	}
}
