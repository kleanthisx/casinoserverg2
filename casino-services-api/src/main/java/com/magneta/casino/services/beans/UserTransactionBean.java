package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("user_transactions")
public class UserTransactionBean  implements Serializable, Cloneable {
	private static final long serialVersionUID = 8224156905566449055L;
	
	private Long userId;
	private Date date;
	private Double amount;
	private Long paymentId; 
	private Long payoutId;
	private Long tableId;
	private Integer roundId;
	private Integer actionId ;
	private Integer subActionId;
	private Long adjustmentId;
	private Double balanceChange;
	private Double playCredits;
	private Long transactionId;
	private Long depositTransferId ;
	private Long affiliateTransactionId ;
	private String description;

	public UserTransactionBean(){
		super();
	}
	
	public UserTransactionBean(UserTransactionBean original){
		super();
		
		this.userId = original.userId;
		this.date = original.date;
		this.amount = original.amount;
		this.paymentId = original.paymentId;
		this.payoutId = original.payoutId;
		this.tableId = original.tableId;
		this.roundId = original.roundId;
		this.actionId = original.actionId;
		this.subActionId = original.subActionId;
		this.adjustmentId = original.adjustmentId;
		this.balanceChange = original.balanceChange;
		this.playCredits = original.playCredits;
		this.transactionId = original.transactionId;
		this.depositTransferId = original.depositTransferId;
		this.affiliateTransactionId = original.affiliateTransactionId;
		this.description = original.description;
	}
	
	@Override
	public Object clone() {
		return new UserTransactionBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserTransactionBean)) {
			return false;
		}

		return this.getTransactionId().equals(((UserTransactionBean)o).getTransactionId());
	}
	
	@XmlAttribute
	@SQLColumn(value="user_transaction_id",isId=true)
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@XmlElement
	@SQLColumn("transaction_time")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@XmlElement
	@SQLColumn("amount")
	public Double getAmount(){
		return amount;
	}
	public void setAmount(Double amount){
		this.amount = amount;
	}
	
	
	@XmlElement
	@SQLColumn("payment_transaction_id")
	public Long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}
	
	@XmlElement
	@SQLColumn("payout_id")
	public Long getPayoutId() {
		return payoutId;
	}
	public void setPayoutId(Long payoutId) {
		this.payoutId = payoutId;
	}
	
	@XmlElement
	@SQLColumn("action_table_id")
	public Long getTableId() {
		return tableId;
	}
	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}
	
	@XmlElement
	@SQLColumn("action_round_id")
	public Integer getRoundId() {
		return roundId;
	}
	public void setRoundId(Integer roundId) {
		this.roundId = roundId;
	}

	@XmlElement
	@SQLColumn("action_action_id")
	public Integer getActionId() {
		return actionId;
	}
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}
	
	@XmlElement
	@SQLColumn("action_sub_action_id")
	public Integer getSubActionId() {
		return subActionId;
	}
	public void setSubActionId(Integer subActionId) {
		this.subActionId = subActionId;
	}
	
	@XmlElement
	@SQLColumn("adjustment_id")
	public Long getAdjustmentId() {
		return adjustmentId;
	}
	public void setAdjustmentId(Long adjustmentId) {
		this.adjustmentId = adjustmentId;
	}
	
	@XmlElement
	@SQLColumn("new_balance")
	public Double getBalanceChange() {
		return balanceChange;
	}
	public void setBalanceChange(Double balanceChange) {
		this.balanceChange = balanceChange;
	}

	@XmlElement
	@SQLColumn("new_play_credits")
	public Double getPlayCredits() {
		return playCredits;
	}
	public void setPlayCredits(Double playCredits) {
		this.playCredits = playCredits;
	}
	
	@XmlElement
	@SQLColumn("deposit_transfer_id")
	public Long getDepositTransferId() {
		return depositTransferId;
	}
	public void setDepositTransferId(Long depositTransferId) {
		this.depositTransferId = depositTransferId;
	}
	
	@XmlElement
	@SQLColumn("affiliate_earnings_transaction_id")
	public Long getAffiliateTransactionId() {
		return affiliateTransactionId;
	}
	public void setAffiliateTransactionId(Long affiliateTransactionId) {
		this.affiliateTransactionId = affiliateTransactionId;
	}
	
	@XmlElement
	@SQLColumn("details")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}



