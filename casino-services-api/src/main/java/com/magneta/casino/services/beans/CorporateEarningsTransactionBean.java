package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;


@SQLTable("affiliate_earnings_transactions")
public class CorporateEarningsTransactionBean implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -789116766797336541L;
	private Long transactionId;
	private Long corporateId;
	private Long initiatorUserId;
	private Double amount;
	private String details;
	private Double newEarnings;
	private Date transactionDate;
	
	public CorporateEarningsTransactionBean() {
		super();
	}
	
	public CorporateEarningsTransactionBean(CorporateEarningsTransactionBean original) {
		super();
		this.transactionId = original.getTransactionId();
		this.corporateId = original.getCorporateId();
		this.initiatorUserId = original.getInitiatorUserId();
		this.amount = original.getAmount();
		this.details = original.getDetails();
		this.newEarnings = original.getNewEarnings();
		this.transactionDate = original.getTransactionDate();
	}

	@Override
	public Object clone() {
		return new CorporateEarningsTransactionBean(this);
	}
	
	@SQLColumn("affiliate_id")
	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}

	@SQLColumn("amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@SQLColumn("details")
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@SQLColumn(value="new_earnings", refreshOnInsert=true, skipUpdate=true)
	public Double getNewEarnings() {
		return newEarnings;
	}

	public void setNewEarnings(Double newEarnings) {
		this.newEarnings = newEarnings;
	}

	@SQLColumn(value="transaction_id", isId=true, refreshOnInsert=true, skipUpdate=true)
	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	@SQLColumn("initiator_user_id")
	public Long getInitiatorUserId() {
		return initiatorUserId;
	}

	public void setInitiatorUserId(Long initiatorUserId) {
		this.initiatorUserId = initiatorUserId;
	}

	@SQLColumn(value="transaction_date", refreshOnInsert=true)
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
}
