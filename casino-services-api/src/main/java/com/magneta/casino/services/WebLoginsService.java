package com.magneta.casino.services;

import java.util.List;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.WebLoginBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

/**
 * Service to handle logins for all Casino Web Applications.
 */
public interface WebLoginsService {

	WebLoginBean getLogin(Long webLoginId, Long userId) throws ServiceException;
	ServiceResultSet<WebLoginBean> getLogins(Long userId,SearchContext<WebLoginBean> searchContext,int limit,int offset,SortContext<WebLoginBean> sortContext) throws ServiceException;
	ServiceResultSet<WebLoginBean> getLogins(Long userId,int limit,int offset) throws ServiceException;
	Long recordLogin(WebLoginBean bean) throws ServiceException;
	int getLoginsSize(Long userId,SearchContext<WebLoginBean> searchContext) throws ServiceException;
	
	ServiceResultSet<WebLoginBean> getLogins(SearchContext<WebLoginBean> searchContext, int limit, int offset,
			SortContext<WebLoginBean> sortContext) throws ServiceException;
	int getLoginsSize(SearchContext<WebLoginBean> searchContext)
			throws ServiceException;
	
	List<Long> getDistinctLogins(SearchContext<WebLoginBean> searchContext,int limit,int offset,
			SortContext<WebLoginBean> sortContext) throws ServiceException;
	int getDistinctLoginsSize(SearchContext<WebLoginBean> searchContext) throws ServiceException;
}
