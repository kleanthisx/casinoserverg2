package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@SQLTable("user_logins")
@XmlRootElement 
public class UserLoginBean  implements Serializable,Cloneable {
	private static final long serialVersionUID = 6255412106455967247L;
	
	private Long userId;
	private Long userLoginId;
	private Date loginDate;
	private Date logoutDate;
	private String loginIp;
	private String macAddess;
	
	public UserLoginBean() {
		super();
	}
	
	public UserLoginBean(UserLoginBean original) {
		super();
		this.userId = original.getUserId();
		this.userLoginId = original.getUserLoginId();
		this.loginDate = original.getLoginDate();
		this.logoutDate = original.getLogoutDate();
		this.loginIp = original.getLoginIp();
		this.macAddess = original.getMacAddess();
	}
	
	@Override
	public Object clone() {
		return new UserLoginBean(this);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}
	
	@XmlElement
	@SQLColumn("device_hash_code")
	public final String getMacAddess() {
		return macAddess;
	}
	public final void setMacAddess(String macAddess) {
		this.macAddess = macAddess;
	}
	
	@XmlElement
	@SQLColumn("login_date")
	public final Date getLoginDate() {
		return loginDate;
	}
	public final void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	
	@XmlElement
	@SQLColumn("client_ip")
	public final String getLoginIp() {
		return loginIp;
	}
	public final void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public void setUserLoginId(Long userLoginId) {
		this.userLoginId = userLoginId;
	}

	@XmlElement
	@SQLColumn("user_login_id")
	public Long getUserLoginId() {
		return userLoginId;
	}

	public void setLogoutDate(Date logoutDate) {
		this.logoutDate = logoutDate;
	}

	@XmlElement
	@SQLColumn("logout_date")
	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
}
