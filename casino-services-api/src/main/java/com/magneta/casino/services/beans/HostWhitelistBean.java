package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("hosts_whitelist")
public class HostWhitelistBean implements Serializable,Cloneable  {
	private static final long serialVersionUID = -1471460331391534132L;
	private Long id;
	private String description;
	private String ip;
	
	public HostWhitelistBean(){
		super();
	}
	
	public HostWhitelistBean(HostWhitelistBean original){
		super();
		this.id = original.id;
		this.ip = original.ip;
		this.description = original.description;
	}
	
	@Override
	public Object clone(){
		return new HostWhitelistBean(this);
	}
	
	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o){
		if(o == null || !(o instanceof HostWhitelistBean)){
			return false;
		}	
		return this.getId().equals(((HostWhitelistBean)o).getId());
	}
	
	@XmlAttribute
	@SQLColumn(value="item_id",isId=true)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@XmlElement
	@SQLColumn("item_desc")
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement
	@SQLColumn(value="item_ip", sqlType="inet")
	@NotNull
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
}
