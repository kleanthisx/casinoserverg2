package com.magneta.casino.services.search.parser;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.services.search.FiqlParseException;

public class ASTExpression implements ASTNode {

	private final List<ASTNode> subNodes;
	
	public ASTExpression() {
		this.subNodes = new ArrayList<ASTNode>();
	}
	
	private enum ParseState {
		OP,
		EXPRESSION;
	}
	
	public List<ASTNode> getSubNodes() {
		return this.subNodes;
	}
	
	@Override
	public void parse(ParserContext s) throws FiqlParseException {
		char [] buf = new char[1];
		
		ParseState state = ParseState.EXPRESSION;
		
		try {
			while (s.read(buf)) {
				if (state == ParseState.OP) {
					if (buf[0] == ')') {
						break;
					}
					
					s.unread();
					ASTOperator op = new ASTOperator();
					op.parse(s);
					this.subNodes.add(op);
					state = ParseState.EXPRESSION;
					continue;
				}
				
				ASTNode subNode;
				if (buf[0] == '(') {
					subNode = new ASTExpression();
				} else {
					s.unread();
					subNode = new ASTComparison();
				}

				subNode.parse(s);
				this.subNodes.add(subNode);
				
				state = ParseState.OP;
			}
			
		} catch (Throwable e) {
			if (e instanceof FiqlParseException) {
				throw (FiqlParseException)e;
			}
			s.parseException(e);
		}
		
		if (state == ParseState.EXPRESSION && !this.subNodes.isEmpty()) {
			s.parseException("Operator at the end of expression");
		}
	}
	
	@Override
	public void print(PrintStream out) {
		out.append("EXPRESSION\n");
		for (ASTNode sub: subNodes) {
			sub.print(out);
		}
		out.append("END EXPRESSION\n");
	}
}
