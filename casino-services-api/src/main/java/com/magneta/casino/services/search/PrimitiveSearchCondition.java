/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.magneta.casino.services.search;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.search.parser.ASTComparison.CompareOp;
import com.magneta.casino.services.utils.BeanProperty;

public class PrimitiveSearchCondition<T> implements SearchCondition<T> {
    
    private BeanProperty<T,?> property;
    private Object propertyValue;
    private CompareOp op;
    
    public PrimitiveSearchCondition(BeanProperty<T,?> property, 
                                    Object propertyValue,
                                    CompareOp op) {
        this.property = property;
        this.propertyValue = propertyValue;
        this.op = op;
    }
    
    @Override
	public List<T> findAll(Iterable<T> pojos) {
        List<T> result = new ArrayList<T>();
        for (T pojo : pojos) {
            if (isMet(pojo)) {
                result.add(pojo);
            }
        }
        return result;
    }
    
    @Override
	public int countAll(Iterable<T> pojos) {
        int count = 0;
        
        for (T pojo : pojos) {
            if (isMet(pojo)) {
                count++;
            }
        }
        return count;
    }


    public CompareOp getComparisonOperator() {
        return op;
    }

    @Override
	public boolean isMet(T pojo) {
    	Object lValue = property.getValue(pojo);
    	boolean met = compare(lValue, op, propertyValue);
    	return met;
    }

    @Override
	public void appendSQLSearchString(Appendable sb) {
    	String columnName = null;
    	String columnType = null;

    	
    	SQLColumn col = property.getAnnotation(SQLColumn.class);
    	if (col != null) {
    		columnName = col.value();
    		columnType = col.sqlType();
    		if ("".equals(columnType)) {
    			columnType = null;
    		}
    	}

    	if (columnName == null) {
    		columnName = property.getName();
    	}

    	SearchUtils.sqlApppendCondition(sb, columnName, op, propertyValue, columnType);
    }
    
    @Override
	public int setSQLStatementValues(PreparedStatement stmt, int startIndex) throws SQLException {
    	return SearchUtils.setSQLStatementValue(stmt, startIndex, op, propertyValue);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private boolean compare(Object lval, CompareOp cond, Object rval) {
        boolean compares = true;
        
        if (cond == CompareOp.EQUALS || cond == CompareOp.NOT_EQUALS ) {
            if (lval == null || rval == null) {
                compares = (lval == rval);
            } else {
                if (lval instanceof String) {
                    compares = textCompare((String)lval, (String)rval);
                } else {
                    compares = lval.equals(rval);
                }
            }
            if (cond == CompareOp.NOT_EQUALS) {
                compares = !compares;
            }
        } else if (cond == CompareOp.EQUALS_IGNORE_CASE || cond == CompareOp.NOT_EQUALS_IGNORE_CASE) {
            if (rval == null || lval == null ) {
                compares = (lval == rval);
            } else {
                if (lval instanceof String) {
                    compares = textCompareIgnoreCase((String)lval, (String)rval);
                } else {
                    compares = lval.equals(rval);
                }
            }
            if (cond == CompareOp.NOT_EQUALS_IGNORE_CASE) {
                compares = !compares;
            }
        } else {
        	if (rval == null || lval == null) {
                compares = false;
            } else if (lval instanceof Comparable && rval instanceof Comparable) {
                Comparable lcomp = (Comparable)lval;
                Comparable rcomp = (Comparable)rval;
                int comp = lcomp.compareTo(rcomp);
                switch (cond) {
                case GREATER_THAN:
                    compares = comp > 0;
                    break;
                case GREATER_OR_EQUALS:
                    compares = comp >= 0;
                    break;
                case LESS_THAN:
                    compares = comp < 0;
                    break;
                case LESS_OR_EQUALS:
                    compares = comp <= 0;
                    break;
                default:
                    String msg = String.format("Condition type %s is not supported", cond.name());
                    throw new RuntimeException(msg);
                }
            }
        }
        return compares;
    }

    private boolean textCompareIgnoreCase(String lval, String rval) {
    	return textCompare(lval.toLowerCase(), rval.toLowerCase());
	}
    
    private boolean textCompare(String lval, String rval) {
        // check wild cards
        boolean starts = false;
        boolean ends = false;
        if (rval.charAt(0) == '*') {
            ends = true;
            rval = rval.substring(1);
        }
        if (rval.charAt(rval.length() - 1) == '*') {
            starts = true;
            rval = rval.substring(0, rval.length() - 1);
        }
        if (starts || ends) {
            // wild card tests
        	if (starts && ends) {
        		return lval.contains(rval);
        	} else if (ends) {
                return lval.endsWith(rval);
            } else {
                return lval.startsWith(rval);
            }
        }

        return lval.equals(rval);
    }
    
    @Override
	public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append(this.property.getName());
    	sb.append(this.op.getOp());
    	sb.append(this.propertyValue == null?"<null>":this.propertyValue);
    	
    	return sb.toString();
    }
}
