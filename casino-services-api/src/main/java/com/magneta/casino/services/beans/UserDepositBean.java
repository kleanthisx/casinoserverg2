package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("payment_transactions")
public class UserDepositBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 6861458996456869342L;
	
	private Long transactionId;
	private Long userId;
	private Long approvedBy;
	private Date dateCompleted;
	private Double transactionAmount;
	private String userAccount;
	private String type;
	private String referenceNo;
	private String comment;	


	public UserDepositBean() {
		super();
	}
		
	protected UserDepositBean(UserDepositBean original) {
		super();
		this.transactionId = original.transactionId;
		this.userId = original.userId;
		this.transactionAmount = original.transactionAmount;
		this.dateCompleted = original.dateCompleted;
		this.userAccount = original.userAccount;
		this.type = original.type;
		this.referenceNo = original.referenceNo;
		this.comment = original.comment;
		this.approvedBy = original.approvedBy;
	}
	
	@Override
	public Object clone() {
		return new UserDepositBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserDepositBean)) {
			return false;
		}

		return this.getTransactionId().equals(((UserDepositBean)o).getTransactionId());
	}

	@XmlAttribute
	@SQLColumn(value="transaction_id", isId=true, refreshOnInsert=true)
	public final Long getTransactionId() {
		return transactionId;
	}
	public final void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	@XmlElement
	@SQLColumn("amount")
	public final Double getTransactionAmount() {
		return transactionAmount;
	}
	public final void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	@XmlElement
	@SQLColumn(value="time_completed", refreshOnInsert=true)
	public final Date getDateCompleted() {
		return dateCompleted;
	}
	public final void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	@XmlElement
	@SQLColumn("ps_user_account")
	public String getUserAccount() {
		return userAccount;
	}

	public void setType(String type) {
		this.type = type;
	}
	@XmlElement
	@SQLColumn("ps_specific")
	public String getType() {
		return type;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	@XmlElement
	@SQLColumn("ps_reference_no")
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@XmlElement
	@SQLColumn("comment")
	public String getComment() {
		return comment;
	}

	@XmlElement
	@SQLColumn("approve_user_id")
	public Long getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}
}
