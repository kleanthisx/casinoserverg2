package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;
import com.magneta.casino.services.enums.UserTypeEnum;

@XmlRootElement 
@SQLTable("users")
public class UserDetailsBean  implements Serializable, Cloneable {
	private static final long serialVersionUID = -2587166483573921199L;

	private Long id;
	private String userName;
	private String nickName;
	private String firstName;
	private String middleName;
	private String lastName;
	private Date registeredDate;
	private Boolean closed;
	private String phone;
	private String email;
	private String region;
	private String town;
	private String countryCode;
	private Boolean isActive;
	private Integer userType;
	private String postalAddress;
	private String postalCode;
	private String timezone;
	private Long modifierUser;

	public UserDetailsBean() {
		super();
	}

	public UserDetailsBean(UserDetailsBean original) {
		super();
		this.id = original.getId();
		this.userName = original.getUserName();
		this.nickName = original.getNickName();
		this.firstName = original.getFirstName();
		this.middleName = original.getMiddleName();
		this.lastName = original.getLastName();
		this.registeredDate = original.getRegisteredDate();
		this.closed = original.getIsClosed();
		this.phone = original.getPhone();
		this.email = original.getEmail();
		this.region = original.getRegion();
		this.town = original.getTown();
		this.countryCode = original.getCountryCode();
		this.isActive = original.getIsActive();
		this.userType = original.getUserType();
		this.postalAddress = original.getPostalAddress();
		this.postalCode = original.getPostalCode();
		this.setTimezone(original.getTimezone());
	}


	@Override
	public Object clone() {
		return new UserDetailsBean(this);
	}


	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof UserDetailsBean)) {
			return false;
		}

		return this.getId().equals(((UserDetailsBean)obj).getId());
	}

	@XmlAttribute
	@SQLColumn(value="user_id",isId=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@XmlElement
	@SQLColumn("username")
	@NotNull
	public final String getUserName() {
		return userName;
	}
	public final void setUserName(String userName) {
		this.userName = userName;
	}
	@XmlElement
	@SQLColumn("nickname")
	@Size(min=6)
	public final String getNickName() {
		return nickName;
	}
	public final void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@XmlElement
	@SQLColumn("first_name")
	@NotNull
	public final String getFirstName() {
		return firstName;
	}
	public final void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@XmlElement
	@SQLColumn("middle_name")
	public final String getMiddleName() {
		return middleName;
	}
	public final void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@XmlElement
	@SQLColumn("last_name")
	@NotNull
	public final String getLastName() {
		return lastName;
	}
	public final void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@XmlElement
	@SQLColumn("register_date")
	public final Date getRegisteredDate() {
		return registeredDate;
	}
	public final void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	@XmlElement
	@SQLColumn("closed")
	public final Boolean getIsClosed() {
		return closed;
	}
	public final void setIsClosed(Boolean closed) {
		this.closed = closed;
	}

	@XmlElement
	@SQLColumn("phone")
	public final String getPhone() {
		return phone;
	}
	public final void setPhone(String phone) {
		this.phone = phone;
	}

	@XmlElement
	@SQLColumn("email")
	@NotNull
	public final String getEmail() {
		return email;
	}
	public final void setEmail(String email) {
		this.email = email;
	}

	@XmlElement
	@SQLColumn("region")
	public final String getRegion() {
		return region;
	}
	public final void setRegion(String region) {
		this.region = region;
	}

	@XmlElement
	@SQLColumn("town")
	public final String getTown() {
		return town;
	}
	public final void setTown(String town) {
		this.town = town;
	}
	@XmlElement
	@SQLColumn("is_active")
	public final Boolean getIsActive() {
		return isActive;
	}
	public final void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@XmlElement
	@SQLColumn("user_type")
	public final Integer getUserType() {
		return userType;
	}

	public final UserTypeEnum getUserTypeEnum() {
		return UserTypeEnum.valueOf(this.getUserType());
	}
	
	public final void setUserTypeEnum(UserTypeEnum userType) {
		setUserType(userType.getId());
	}

	public final void setUserType(Integer userType) {
		this.userType = userType;
	}

	@XmlElement
	@SQLColumn("postal_address")
	public final String getPostalAddress() {
		return postalAddress;
	}
	public final void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	@XmlElement
	@SQLColumn("postal_code")
	public final String getPostalCode() {
		return postalCode;
	}
	public final void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		if (this.id == null) {
			return "[New User]";
		}

		return this.id.toString();
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@XmlElement
	@SQLColumn("country")
	public String getCountryCode() {
		return countryCode;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@XmlElement
	@SQLColumn("time_zone")
	public String getTimezone() {
		return timezone;
	}

	public void setModifierUser(Long modifierUser) {
		this.modifierUser = modifierUser;
	}
	
	@XmlElement
	@SQLColumn("modifier_user")
	public Long getModifierUser() {
		return modifierUser;
	}
}
