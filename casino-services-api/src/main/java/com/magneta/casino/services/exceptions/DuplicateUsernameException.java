package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class DuplicateUsernameException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public DuplicateUsernameException() {
		this("Username already used");
	}
	
	public DuplicateUsernameException(String message) {
		super(message);
	}

	public DuplicateUsernameException(String message, Throwable cause) {
		super(message, cause);
	}
}
