package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class InvalidUserInformation extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public InvalidUserInformation() {
		this("User contains invalid information");
	}
	
	public InvalidUserInformation(String message) {
		super(message);
	}

	public InvalidUserInformation(String message, Throwable cause) {
		super(message, cause);
	}
}
