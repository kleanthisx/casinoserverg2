package com.magneta.casino.services.enums;

import java.util.Locale;
import java.util.ResourceBundle;


public enum GameCategoryEnum {

	CARD_GAMES(1),
	SLOT_GAMES(2),
	VIRTUAL_RACES(3),
	TABLE_GAMES(4),
	LOTTERY(5);
	
	private final int val;
	
	GameCategoryEnum(int val) {
		this.val = val;
	}
	
	public int getId() {
		return this.val;
	}

	public String getName(Locale locale) {
		ResourceBundle resources = ResourceBundle.getBundle("com.magneta.casino.services.enums.GameCategoryEnum", locale);
		
		return resources.getString(this.name());
	}
	
	public static GameCategoryEnum valueOf(int val) {
		GameCategoryEnum[] vals = GameCategoryEnum.values();
		
		for (GameCategoryEnum e: vals) {
			if (e.val == val) {
				return e;
			}
		}
		
		throw new IllegalArgumentException("Invalid GameCategoryEnum value");
	}
}
