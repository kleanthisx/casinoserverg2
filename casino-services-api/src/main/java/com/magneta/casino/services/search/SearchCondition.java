package com.magneta.casino.services.search;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public interface SearchCondition<T> {
    
    /**
     * Checks if the given pojo instance meets this search condition
     * 
     * @param pojo the object which will be checked
     * @return true if the pojo meets this search condition, false - otherwise
     */
    boolean isMet(T pojo);
    
    /**
     * Returns a list of pojos matching the condition
     * @param pojos list of pojos
     * @return list of the matching pojos or null if none have been found
     */
    List<T> findAll(Iterable<T> pojos);
    
    /**
     * Counts the pojos matching the condition
     * @param pojos list of pojos
     */
	int countAll(Iterable<T> pojos);
    
    /**
     * Utility method to append the search criteria for SQL (after WHERE)
     * @return
     */    
    public void appendSQLSearchString(Appendable sb);

    /**
     * Sets the values for the this search condition starting from the given index.
     * @param stmt The statement to set the values.
     * @param startIndex The index of the first value.
     * @return The number of values set.
     * 
     * @throws SQLException
     */
    int setSQLStatementValues(PreparedStatement stmt, int startIndex) throws SQLException;
}
