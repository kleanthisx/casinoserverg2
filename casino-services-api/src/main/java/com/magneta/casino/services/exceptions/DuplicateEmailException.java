package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class DuplicateEmailException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public DuplicateEmailException() {
		this("Email already used");
	}
	
	public DuplicateEmailException(String message) {
		super(message);
	}

	public DuplicateEmailException(String message, Throwable cause) {
		super(message, cause);
	}
}
