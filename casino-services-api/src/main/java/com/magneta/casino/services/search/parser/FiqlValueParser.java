package com.magneta.casino.services.search.parser;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.magneta.casino.services.search.FiqlParseException;
import com.magneta.casino.services.utils.PrimitiveUtils;
import au.com.bytecode.opencsv.CSVReader;


public class FiqlValueParser {

	public static Object[] parseValues(Class<?> valueType, String value) throws FiqlParseException {

		value = value.substring(1, value.length() - 1);

		CSVReader parser = new CSVReader(new StringReader(value), ',', '\"', '\'');

		String[] valString;
		try {
			valString = parser.readNext();
		} catch (IOException e) {
			throw new FiqlParseException("Unable to parse multi-value list", e);
		} finally {
			try {
				parser.close();
			} catch (IOException e) {
			}
		}
		
		if (valString == null) {
			return new Object[0];
		}
		
		Object[] values = new Object[valString.length];

		for (int i=0; i < valString.length; i++) {
			values[i] = parseValue(valueType, valString[i]);
		}
		return values;
	}
	
	
	/** 
	 * Parses a value of the given type
	 * @param valueType
	 * @param value
	 * @return
	 * @throws FiqlParseException
	 */
	public static Object parseValue(Class<?> valueType, String value) throws FiqlParseException {
		Object castedValue = value;
        
        if (value == null || value.equals("NULL")) {
        	return null;
        }
        
        if (Date.class.isAssignableFrom(valueType)) {
        	castedValue = parseDate(value);
        } else {
            try {
                castedValue = convertStringToPrimitive(value, valueType);
            } catch (Throwable e) {
                throw new FiqlParseException("Cannot convert String value \"" + value
                                             + "\" to a value of class " + valueType.getName(), e);
            }
        }
        return castedValue;
	}
	
	private static <T> T convertStringToPrimitive(String value, Class<T> cls) throws Exception {
		if (String.class == cls) {
			return cls.cast(value);
		}
		if (cls.isPrimitive()) {
			return PrimitiveUtils.read(value, cls);
		} 
		Method m = cls.getMethod("valueOf", new Class[]{String.class});
		return cls.cast(m.invoke(null, value));
	}

	private static String[] DATE_FORMATS = {
    	"yyyy-MM-dd'T'HH:mm:ss.SSSZ",
    	"yyyy-MM-dd"
    };

    private static Date parseDate(String value){
    	Date val = null;
        DateFormat df;
        
        for (String f: DATE_FORMATS) {
        	df = new SimpleDateFormat(f);
        	try {
        		val = df.parse(value);
        		break;
        	} catch(ParseException e) {
        		continue;
        	}
        }

        return val;
    }
}
