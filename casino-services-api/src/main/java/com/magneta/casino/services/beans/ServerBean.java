package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("servers")
public class ServerBean implements Serializable,Cloneable {
	
	private static final long serialVersionUID = 1219416725707983607L;
	
	private Integer serverId;
	private String serverDescription;
	private String serverLocalAddress;
	private Integer serverLocalPort;
	private Boolean serverRunning;
	
	public ServerBean() {
		super();
	}
	
	public ServerBean(ServerBean original) {
		this.serverId = original.serverId;
		this.serverDescription = original.serverDescription;
		this.serverLocalAddress = original.serverLocalAddress;
		this.serverLocalPort = original.serverLocalPort;
		this.serverRunning = original.serverRunning;
	}
	
	@Override
	public Object clone() {
		return new ServerBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof ServerBean)) {
			return false;
		}

		return this.getServerId().equals(((ServerBean)o).getServerId());
	}

	@XmlAttribute
	@SQLColumn(value="server_id", isId=true)
	public Integer getServerId() {
		return serverId;
	}
	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}
	
	@XmlElement
	@SQLColumn("server_desc")
	public String getServerDescription() {
		return serverDescription;
	}
	public void setServerDescription(String serverDescription) {
		this.serverDescription = serverDescription;
	}
	
	@XmlElement
	@SQLColumn("server_local_address")
	public String getServerLocalAddress() {
		return serverLocalAddress;
	}
	public void setServerLocalAddress(String serverLocalAddress) {
		this.serverLocalAddress = serverLocalAddress;
	}
	
	@XmlElement
	@SQLColumn("server_local_port")
	public Integer getServerLocalPort() {
		return serverLocalPort;
	}
	public void setServerLocalPort(Integer serverLocalPort) {
		this.serverLocalPort = serverLocalPort;
	}

	@XmlElement
	@SQLColumn("server_running")
	public Boolean getServerRunning() {
		return serverRunning;
	}
	public void setServerRunning(Boolean serverRunning) {
		this.serverRunning = serverRunning;
	}
}
