package com.magneta.casino.services;

import java.util.List;

import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.beans.GameDescriptorBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface GamesService {

	GameBean getGame(Integer gameId) throws ServiceException;
	List<GameBean> getGames() throws ServiceException;
	ServiceResultSet<GameBean> getGames(int limit, int offset)  throws ServiceException;
	ServiceResultSet<GameBean> getGames(SearchContext<GameBean> searchContext,SortContext<GameBean> sortContext,int limit,int offset) throws ServiceException;
	GameDescriptorBean getGameDescriptor(Integer gameId) throws ServiceException;
	int getGamesSize(SearchContext<GameBean> searchContext) throws ServiceException;
	void insertGame(GameBean gameBean) throws ServiceException;
	void updateGame(GameBean gameBean) throws ServiceException;
	
	GameConfigBean getGameActiveConfig(Integer gameId) throws ServiceException;
	void setGameActiveConfig(Integer gameId, Long configId)	throws ServiceException;
}
