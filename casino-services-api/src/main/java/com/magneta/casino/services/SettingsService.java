package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface SettingsService {

	<T> T getSettingValue(String settingKey, Class<T> type) throws ServiceException;
	<T> T getSettingValue(SettingBean setting, Class<T> type) throws ServiceException;
	
	SettingBean getSetting(String settingKey) throws ServiceException;
	ServiceResultSet<SettingBean> getSettings(int limit, int offset)  throws ServiceException;
	ServiceResultSet<SettingBean> getSettings(SearchContext<SettingBean> searchContext,SortContext<SettingBean> sortContext,int limit,int offset) throws ServiceException;
	int getSettingsSize(SearchContext<SettingBean> searchContext) throws ServiceException;
	
	void invalidate();
}
