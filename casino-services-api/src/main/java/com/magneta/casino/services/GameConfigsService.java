package com.magneta.casino.services;


import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.games.configuration.GameConfiguration;

public interface GameConfigsService {

	public GameConfigBean getGameConfig(Integer gameId,Long configId) throws ServiceException;
	public ServiceResultSet<GameConfigBean> getGameConfigs(Integer gameId,int limit, int offset)  throws ServiceException;
	public ServiceResultSet<GameConfigBean> getGameConfigs(Integer gameId,SearchContext<GameConfigBean> searchContext,SortContext<GameConfigBean> sortContext,int limit,int offset) throws ServiceException;
	public int getGameConfigsSize(Integer gameId,SearchContext<GameConfigBean> searchContext) throws ServiceException;
	public GameConfiguration getFilteredConfiguration(Integer gameId,Long gameConfigId) throws ServiceException;
	public void insertConfig(GameConfigBean bean) throws ServiceException;
	public void updateConfig(GameConfigBean bean) throws ServiceException, ConditionalUpdateException;
}
