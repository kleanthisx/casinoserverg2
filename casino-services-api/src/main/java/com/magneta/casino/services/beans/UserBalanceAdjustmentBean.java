package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement 
@SQLTable("user_balance_adjustments")
public class UserBalanceAdjustmentBean implements Serializable, Cloneable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7989031075894539446L;

	private Long adjustmentId;
	private Double adjustmentAmount;
	private Long userId;
	private Date adjustmentDate;
	private Long adjustmentUser;
	private String adjustmentDescription;
	
	public UserBalanceAdjustmentBean() {
		super();
	}
	
	public UserBalanceAdjustmentBean(UserBalanceAdjustmentBean orig) {
		super();
		this.adjustmentId = orig.adjustmentId;
		this.adjustmentAmount = orig.adjustmentAmount;
		this.userId = orig.userId;
		this.adjustmentDate = orig.adjustmentDate;
		this.adjustmentUser = orig.adjustmentUser;
		this.adjustmentDescription = orig.adjustmentDescription;
	}
	
	@Override
	public Object clone() {
		return new UserBalanceAdjustmentBean(this);
	}


	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof UserBalanceAdjustmentBean)) {
			return false;
		}

		return this.getAdjustmentId().equals(((UserBalanceAdjustmentBean)obj).getAdjustmentId());
	}
	
	@XmlElement
	@SQLColumn(value="adjustment_date", refreshOnInsert=true)
	public Date getAdjustmentDate() {
		return adjustmentDate;
	}
	public void setAdjustmentDate(Date adjustmentDate) {
		this.adjustmentDate = adjustmentDate;
	}
	
	@XmlElement
	@SQLColumn("adjustment_user")
	public Long getAdjustmentUser() {
		return adjustmentUser;
	}
	public void setAdjustmentUser(Long adjustmentUser) {
		this.adjustmentUser = adjustmentUser;
	}
	
	@XmlElement
	@SQLColumn("adjustment_description")
	public String getAdjustmentDescription() {
		return adjustmentDescription;
	}
	public void setAdjustmentDescription(String adjustmentDescription) {
		this.adjustmentDescription = adjustmentDescription;
	}
	
	@XmlElement
	@SQLColumn("adjustment_amount")
	public Double getAdjustmentAmount() {
		return adjustmentAmount;
	}
	public void setAdjustmentAmount(Double adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}
	
	@XmlAttribute
	@SQLColumn(value="adjustment_id", isId=true, refreshOnInsert=true)
	public Long getAdjustmentId() {
		return adjustmentId;
	}
	public void setAdjustmentId(Long adjustmentId) {
		this.adjustmentId = adjustmentId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userId
	 */
	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}

	
}
