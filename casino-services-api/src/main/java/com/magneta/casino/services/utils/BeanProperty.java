package com.magneta.casino.services.utils;

import java.beans.IntrospectionException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A property of a bean.
 * @author anarxia
 *
 * @param <T>
 * @param <E>
 */
public class BeanProperty<T, E> {

	private final String name;
	private final Class<E> type;
	
	private final Method getter;
	private final Method setter;
	
	public BeanProperty(String name, Class<E> type, Method getter, Method setter) {
		this.name = name;
		this.type = type;
		this.getter = getter;
		this.setter = setter;
	}
	
	/**
	 * Gets the name of the property.
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets the type of the property.
	 * @return
	 */
	public Class<E> getType() {
		return this.type;
	}
	
	public Method getGetter() {
		return this.getter;
	}
	
	public Method getSetter() {
		return this.setter;
	}
	
	public boolean isReadOnly() {
		return this.setter == null;
	}
	
	@SuppressWarnings("unchecked")
	public E getValue(T obj) {
		try {
			return (E)this.getter.invoke(obj);
		} catch (Exception e) {
			return null;
		}
	}
	
	public void setValue(T obj, Object value) throws IntrospectionException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    	if (this.isReadOnly()) {
    		throw new IntrospectionException("Propery" + this.name + " is read-only");
    	}
    	
    	/* Primitive type and null */
    	if (type.isPrimitive() && value == null) { 
    		/* Dont know what to do primitive type cannot be assigned to null*/
    	} else {
    		setter.invoke(obj, value);
    	}
    }
	
	public <A extends Annotation> A getAnnotation(Class<A> klass)  {
		return this.getter.getAnnotation(klass);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> BeanProperty<T,?> createInstance(Class<T> beanType, String property) {
		Method getter = BeanUtil.getGetter(beanType, property);
		if (getter == null) {
			return null;
		}

		Class<?> type = getter.getReturnType();
		
		Method setter = BeanUtil.getSetter(beanType, property, type);
		
		return new BeanProperty(property, type, getter, setter);
	}
}
