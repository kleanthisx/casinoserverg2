package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;

@XmlRootElement 
public class GameConfigBean  implements Serializable,Cloneable {
	private static final long serialVersionUID = 6139457118649726312L;
	private Integer gameId;
	private String configValue;
	private String configComment;
	private Date configDate;
	private Long configId;
	private Long creatorId;
	
	public GameConfigBean() {
		super();
	}
	
	public GameConfigBean(GameConfigBean original) {
		super();
		this.configId = original.getConfigId();
		this.gameId = original.getGameId();
		this.configValue = original.getConfigValue();
		this.configComment = original.getConfigComment();
		this.configDate = original.getConfigDate();
		this.creatorId = original.getCreatorId();
	}
	
	@Override
	public Object clone() {
		return new GameConfigBean(this);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameConfigBean)) {
			return false;
		}
		
		return this.getConfigId().equals(((GameConfigBean)o).getConfigId());
	}
	

	
	@XmlAttribute
	@SQLColumn("config_id")
	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	
	@XmlElement
	@SQLColumn("config_value")
	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	@XmlElement
	@SQLColumn("config_comment")
	public String getConfigComment() {
		return configComment;
	}

	public void setConfigComment(String configComment) {
		this.configComment = configComment;
	}

	@XmlElement
	@SQLColumn("config_date")
	public Date getConfigDate() {
		return configDate;
	}

	public void setConfigDate(Date configDate) {
		this.configDate = configDate;
	}

	@XmlElement
	@SQLColumn("modified_by")
	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long modifiedBy) {
		this.creatorId = modifiedBy;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	

	@XmlElement
	@SQLColumn("game_id")
	public Integer getGameId() {
		return gameId;
	}	
}
