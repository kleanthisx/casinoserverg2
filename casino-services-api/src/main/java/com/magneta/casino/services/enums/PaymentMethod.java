/**
 * 
 */
package com.magneta.casino.services.enums;

/**
 * @author maria
 *
 */
public enum PaymentMethod {
	BOLETO,
	CASH,
    CREDIT_CARD,
    ELV,
    ENVOY,
    EPAY_BG,
    GIROPAY,
    IDEAL,
    MISTER_CASH,
    MONEYBOOKERS,
    MONETA,
    NETELLER,
    PAYSAFE_CARD,
    UKASH,
    VOUCHER,
    WEB_MONEY,
    OTHER,
    CHEQUE,
    WIRE_TRANSFER
}
