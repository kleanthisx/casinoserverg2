package com.magneta.casino.services;

import com.magneta.casino.services.beans.RoleBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface RoleService {

	RoleBean getRole(Integer roleId) throws ServiceException;
	ServiceResultSet<RoleBean> getRoles(SearchContext<RoleBean> searchContext,int limit,int offset,SortContext<RoleBean> sortContext) throws ServiceException;
	void createRole(RoleBean roleBean) throws ServiceException;
	void updateRole(RoleBean roleBean) throws ServiceException;
}
