/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.magneta.casino.services.search;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.search.parser.ASTNode;
import com.magneta.casino.services.search.parser.FiqlConditionBuilder;
import com.magneta.casino.services.search.parser.FiqlParser;
import com.magneta.casino.services.utils.BeanTypeInspector;


public class SearchContextImpl<T> implements SearchContext<T> {

	private String expression;
	private Class<T> cls;
	private SearchCondition<T> condition;
	
	public SearchContextImpl(Class<T> cls,String expression){
		this.expression = expression;
		this.cls = cls;
	}
	
	public SearchCondition<T> parseCondition() throws ServiceException {

		if (expression == null) {
			throw new ServiceException("FIQL expression is empty");
		}
		
		FiqlParser parser = new FiqlParser(); 

		try {
			ASTNode node = parser.parse(expression);
			FiqlConditionBuilder<T> cond = new FiqlConditionBuilder<T>(new BeanTypeInspector<T>(cls));
			return cond.build(node);
		} catch (FiqlParseException ex) {
			throw new ServiceException("Invalid FIQL expression: " + expression, ex);
		}
	}

	@Override
	public SearchCondition<T> getCondition() throws ServiceException {
		if (this.condition == null) {
			this.condition = parseCondition();
		}
		
		return this.condition;
	}
	
	@Override
	public String getExpression() {
		return expression;
	}
}
