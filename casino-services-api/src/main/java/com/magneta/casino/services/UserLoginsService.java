package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserLoginBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface UserLoginsService {
	public UserLoginBean getLogin(Long userLoginId,Long userId) throws ServiceException;
	public ServiceResultSet<UserLoginBean> getLogins(Long userId,SearchContext<UserLoginBean> searchContext,int limit,int offset,SortContext<UserLoginBean> sortContext) throws ServiceException;
	public ServiceResultSet<UserLoginBean> getLogins(Long userId,int limit,int offset) throws ServiceException;
	public ServiceResultSet<UserLoginBean> getLogins(SearchContext<UserLoginBean> searchContext) throws ServiceException;
	public int getLoginsSize(Long userId,SearchContext<UserLoginBean> searchContext) throws ServiceException;
}
