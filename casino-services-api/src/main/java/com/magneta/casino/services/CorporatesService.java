package com.magneta.casino.services;

import java.util.List;

import com.magneta.casino.services.beans.AffiliateUsersBean;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface CorporatesService {
	ServiceResultSet<CorporateBean> getCorporates(SearchContext<CorporateBean> searchContext,SortContext<CorporateBean> sortContext,int limit,int offset) throws ServiceException;
	ServiceResultSet<CorporateBean> getCorporates(int limit,int offset) throws ServiceException;
	CorporateBean getCorporate(Long corporateId) throws ServiceException;
	CorporateBean getSuperCorporate(Long userId) throws ServiceException;
	int getCorporatesSize(SearchContext<CorporateBean> searchContext) throws ServiceException;
	
	/**
	 * Returns the corporate hierarchy for the given user.
	 * The first userId returned is for the immediate super followed by
	 * his super etc.  
	 * @param corporateId
	 * @return
	 * @throws ServiceException
	 */
	List<Long> getCorporateHierarchy(Long corporateId) throws ServiceException;
	
	/**
	 * Gets immediate sub-accounts for the given corporate.
	 * @param corporateId
	 * @param searchContext
	 * @param sortContext
	 * @return
	 * @throws ServiceException
	 */
	List<AffiliateUsersBean> getSubAccounts(Long corporateId,
			SearchContext<AffiliateUsersBean> searchContext,
			SortContext<AffiliateUsersBean> sortContext)
			throws ServiceException;
	int getSubAccountsSize(Long corporateId,
			SearchContext<AffiliateUsersBean> searchContext)
			throws ServiceException;
	
	ServiceResultSet<CorporateBean> getSuperCorporates(SearchContext<CorporateBean> searchContext,
			SortContext<CorporateBean> sortContext, int limit, int offset) throws ServiceException;
	int getSuperCorporatesSize(SearchContext<CorporateBean> searchContext) throws ServiceException;
}
