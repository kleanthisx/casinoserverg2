package com.magneta.casino.services.enums;

public enum UserAffiliationEnum {

    USER_AFFILIATION(1),
    SUB_CORPORATE_AFFILIATION(3);
    
    private int id;
    
    UserAffiliationEnum(int id) {
    	this.id = id;
    }
    
    public int getId() {
    	return this.id;
    }
}
