package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("game_types")
public class GameBean  implements Serializable,Cloneable {
	
	private static final long serialVersionUID = 6139457118649726312L;
	
	private Integer gameId;
	private Integer categoryId;
	private String name;
	private String gameTemplate;

	public GameBean() {
		super();
	}
	
	public GameBean(GameBean original) {
		this.gameId = original.gameId;
		this.categoryId = original.categoryId;
		this.name = original.name;
		this.gameTemplate = original.gameTemplate;
	}
	
	@Override
	public Object clone() {
		return new GameBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameBean)) {
			return false;
		}
		
		return this.getGameId().equals(((GameBean)o).getGameId());
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	
	@XmlAttribute
	@NotNull
	@SQLColumn(value="game_type_id",isId=true)
	public Integer getGameId() {
		return gameId;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement
	@NotNull
	@SQLColumn("game_type_variant")
	public String getName() {
		return name;
	}
	
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@XmlElement
	@NotNull
	@SQLColumn("game_category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	/**
	 * @param gameTemplate the gameTemplate to set
	 */
	public void setGameTemplate(String gameTemplate) {
		this.gameTemplate = gameTemplate;
	}

	@XmlElement
	@NotNull
	@SQLColumn("game_template")
	public String getGameTemplate() {
		return gameTemplate;
	}
	

	
}
