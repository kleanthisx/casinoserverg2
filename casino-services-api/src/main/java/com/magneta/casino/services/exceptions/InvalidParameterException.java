package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class InvalidParameterException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1880201722209732713L;
	
	public InvalidParameterException() {
		super("Invalid parameter");
	}

	public InvalidParameterException(String message) {
		super(message);
	}
	
	public InvalidParameterException(String message, Throwable cause) {
		super(message, cause);
	}

}
