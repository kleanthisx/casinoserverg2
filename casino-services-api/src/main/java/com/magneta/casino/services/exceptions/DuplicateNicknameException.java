package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class DuplicateNicknameException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5172950887092241445L;

	public DuplicateNicknameException() {
		this("Nickname already used");
	}
	
	public DuplicateNicknameException(String message) {
		super(message);
	}

	public DuplicateNicknameException(String message, Throwable cause) {
		super(message, cause);
	}
}
