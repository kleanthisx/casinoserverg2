package com.magneta.casino.services.beans;

import java.util.List;

public class ServiceResultSet<T> {

	private List<T> result;
	private boolean hasNext;
	private boolean hasPrev;
	
	public void setResult(List<T> result) {
		this.result = result;
	}
	public List<T> getResult() {
		return result;
	}
	
	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}
	
	public boolean getHasNext() {
		return hasNext;
	}
	
	public void setHasPrev(boolean hasPrev) {
		this.hasPrev = hasPrev;
	}
	
	public boolean getHasPrev() {
		return hasPrev;
	}
	
	
}
