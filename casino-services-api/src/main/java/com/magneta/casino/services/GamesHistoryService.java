package com.magneta.casino.services;

import java.util.Date;

import com.magneta.casino.services.beans.GameSummaryBean;
import com.magneta.casino.services.beans.GameSummaryHistoryBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface GamesHistoryService {

	ServiceResultSet<GameSummaryHistoryBean> getSummaryGameHistory(Long userId,SearchContext<GameSummaryHistoryBean> searchContext,SortContext<GameSummaryHistoryBean> sortContext,int limit,int offset)  throws ServiceException;
	
	ServiceResultSet<GameSummaryHistoryBean> getSummaryGameHistory(Long userId,int limit,int offset)  throws ServiceException;
	
	int getSummaryGameHistorySize(Long userId,SearchContext<GameSummaryHistoryBean> searchContext) throws ServiceException;
	
	ServiceResultSet<GameSummaryBean> getGameSummary(Long userId,
			Date minDate, Date maxDate,
			SearchContext<GameSummaryBean> searchContext,
			SortContext<GameSummaryBean> sortContext, int limit, int offset)
			throws ServiceException;
	
	int getGameSummarySize(Long userId, Date minDate, Date maxDate,
			SearchContext<GameSummaryBean> searchContext,
			SortContext<GameSummaryBean> sortContext, int limit, int offset)
			throws ServiceException;
}
