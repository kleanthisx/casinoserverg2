package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("countries")
public class CountryBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 3192624873204837188L;
	private String code;
	private String name;
	private Boolean enabled;
	
	public CountryBean() {
		super();
	}
	
	public CountryBean(CountryBean original) {
		super();
		this.code = original.getCode();
		this.name = original.getName();
		this.enabled = original.isEnabled();
	}

	@Override
	public Object clone() {
		return new CountryBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof CountryBean)) {
			return false;
		}
		
		return this.getCode().equals(((CountryBean)o).getCode());
	}

	
	@XmlAttribute
	@SQLColumn(value="country_code", isId=true)
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement
	@SQLColumn("country_name")
	public String getName() {
		return name;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	@XmlElement
	@SQLColumn("accept")
	public boolean isEnabled() {
		return enabled;
	}	
}
