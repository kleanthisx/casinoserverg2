package com.magneta.casino.services;

import com.magneta.casino.services.beans.PromoTransactionBean;

public interface PromoTransactionService {

	void createPromoTransaction(PromoTransactionBean transaction) throws ServiceException;
}
