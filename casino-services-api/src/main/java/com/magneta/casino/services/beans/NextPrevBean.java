package com.magneta.casino.services.beans;

public class NextPrevBean {

	private boolean next;
	private boolean prev;

	public void setNext(boolean next) {
		this.next = next;
	}
	public boolean hasNext() {
		return next;
	}
	public void setPrev(boolean prev) {
		this.prev = prev;
	}
	public boolean hasPrev() {
		return prev;
	}
	
}
