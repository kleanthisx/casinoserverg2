package com.magneta.casino.services;

import java.util.List;

import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface CountryService {

	public List<CountryBean> getCountries() throws ServiceException;
	public ServiceResultSet<CountryBean> getCountries(SearchContext<CountryBean> searchContext,SortContext<CountryBean> sortContext,int limit,int offset) throws ServiceException;
	public ServiceResultSet<CountryBean> getCountries(int limit,int offset) throws ServiceException;
	public CountryBean getCountry(String code) throws ServiceException;
	public void update(CountryBean country) throws ServiceException,ConditionalUpdateException;
	public int getCountriesSize(SearchContext<CountryBean> searchContext) throws ServiceException;
}
