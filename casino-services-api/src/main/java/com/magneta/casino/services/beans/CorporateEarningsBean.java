package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("affiliate_earnings")
public class CorporateEarningsBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 2786499267297727868L;
	private Long corporateId;
	private Double earnings;
	
	public CorporateEarningsBean() {
		super();
	}
	
	public CorporateEarningsBean(CorporateEarningsBean original) {
		super();
		this.setEarnings(original.getEarnings());
		this.setCorporateId(original.getCorporateId());
	}
	
	@Override
	public Object clone() {
		return new CorporateEarningsBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof CorporateEarningsBean)) {
			return false;
		}
		
		return this.getCorporateId().equals(((CorporateEarningsBean)o).getCorporateId());
	}
	
	
	@XmlAttribute
	@SQLColumn("affiliate_id")
	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}

	@XmlElement
	@SQLColumn("earnings")
	public Double getEarnings() {
		return earnings;
	}

	public void setEarnings(Double earnings) {
		this.earnings = earnings;
	}
}
