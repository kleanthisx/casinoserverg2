package com.magneta.casino.services;

import java.util.Currency;

public interface CurrencyService {
	Currency getActiveCurrency();
}
