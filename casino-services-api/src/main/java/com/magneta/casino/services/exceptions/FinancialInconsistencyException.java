package com.magneta.casino.services.exceptions;

import com.magneta.casino.services.ServiceException;

public class FinancialInconsistencyException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3236872479181027066L;

	public FinancialInconsistencyException(String message) {
		super(message);
	}

}
