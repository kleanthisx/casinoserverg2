package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;


@XmlRootElement 
public class UserRoundFilteredActionBean implements Serializable, Cloneable  {
	private static final long serialVersionUID = 1460014319614472599L;
	private String actionDescription;
	private List<UserRoundFilteredActionValueBean> actionValue;
	private Double amount;
	private Double balance;
	private String area;
	private Integer seat;
	private Date actionTime;
	
	public UserRoundFilteredActionBean() {
		super();
	}
	
	protected UserRoundFilteredActionBean(UserRoundFilteredActionBean original) {
		super();
		this.actionDescription = original.getActionDescription();
		this.actionValue = original.getActionValue();
		this.amount = original.getAmount();
		this.balance = original.getBalance();
		this.area = original.getArea();
		this.seat = original.getSeat();
		this.actionTime = original.getActionTime();
	}
	
	@Override
	public Object clone() {
		return new UserRoundFilteredActionBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserRoundActionBean)) {
			return false;
		}
		return this.hashCode() == ((UserRoundActionBean)o).hashCode();
	}
	
	
	@XmlElement
	public String getActionDescription() {
		return actionDescription;
	}
	public void setActionDescription(String actionDescription) {
		this.actionDescription = actionDescription;
	}
	@XmlElement
	public List<UserRoundFilteredActionValueBean> getActionValue() {
		return actionValue;
	}
	public void setActionValue(List<UserRoundFilteredActionValueBean> actionValue) {
		this.actionValue = actionValue;
	}
	@XmlElement
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@XmlElement
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	@XmlElement
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	@XmlElement
	public Integer getSeat() {
		return seat;
	}
	public void setSeat(Integer seat) {
		this.seat = seat;
	}

	public Date getActionTime() {
		return actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
}
