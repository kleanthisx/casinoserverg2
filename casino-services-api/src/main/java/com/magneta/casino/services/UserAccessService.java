package com.magneta.casino.services;

public interface UserAccessService {
	public boolean canAccess(Long userId);	
}
