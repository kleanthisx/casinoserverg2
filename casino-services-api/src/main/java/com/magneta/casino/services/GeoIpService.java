package com.magneta.casino.services;

import com.magneta.casino.services.beans.LocationBean;

public interface GeoIpService {
	public LocationBean getLocationFromIp(String ip) throws ServiceException;
}
