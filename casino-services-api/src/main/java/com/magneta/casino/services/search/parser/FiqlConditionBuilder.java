package com.magneta.casino.services.search.parser;

import java.util.List;

import com.magneta.casino.services.search.AbstractComplexCondition;
import com.magneta.casino.services.search.AndSearchCondition;
import com.magneta.casino.services.search.FiqlParseException;
import com.magneta.casino.services.search.OrSearchCondition;
import com.magneta.casino.services.search.PrimitiveSearchCondition;
import com.magneta.casino.services.search.PrimitiveSearchListCondition;
import com.magneta.casino.services.search.SearchCondition;
import com.magneta.casino.services.search.parser.ASTComparison.CompareOp;
import com.magneta.casino.services.search.parser.ASTOperator.BooleanOp;
import com.magneta.casino.services.utils.BeanProperty;
import com.magneta.casino.services.utils.BeanTypeInspector;

public class FiqlConditionBuilder<T> {

	public static final void main(String [] args) throws FiqlParseException {

		if (args.length < 2) {
			System.err.println("Usage: prog <class> <fiql-expression>");
			return;
		}

		Class<?> klass;

		try {
			klass = Class.forName(args[0]);
		} catch (ClassNotFoundException e) {
			System.err.println("Could not find class " + args[0]);
			return;
		}

		String expression = args[1];

		System.out.println("Expression: " + expression + "\n");

		FiqlParser parser = new FiqlParser();

		long parseStart = System.currentTimeMillis();
		ASTNode rootNode = parser.parse(expression);
		System.out.println("Parsing finished in " + (System.currentTimeMillis() - parseStart) + "ms");

		@SuppressWarnings({ "unchecked", "rawtypes" })
		FiqlConditionBuilder<?> condBuilder = new FiqlConditionBuilder(klass);

		long condStart = System.currentTimeMillis();
		SearchCondition<?> cond = condBuilder.build(rootNode);
		long condEnd = System.currentTimeMillis();
		System.out.println("Conditions build in " + (condEnd - condStart) + "ms");
		System.out.println("Total time: " + (condEnd - parseStart) + "ms");


		System.out.println(cond.toString());
		
		StringBuilder sb = new StringBuilder();
		cond.appendSQLSearchString(sb);
		
		System.out.println("\n" + sb.toString());
	}

	private BeanTypeInspector<T> beanSpector;

	public FiqlConditionBuilder(BeanTypeInspector<T> beanspector) {
		this.beanSpector = beanspector;
	}

	public FiqlConditionBuilder(Class<T> klass) {
		this.beanSpector = new BeanTypeInspector<T>(klass);
	} 

	private SearchCondition<T> buildComparison(ASTComparison comp) throws FiqlParseException {
		return createComparisonInstance(comp.getPropertyName(), comp.getOp(), comp.getValue());
	}

	protected SearchCondition<T> createComparisonInstance(String compName, CompareOp op, String value) throws FiqlParseException {
		BeanProperty<T,?> prop = beanSpector.getProperty(compName);

		if (prop == null) {
			throw new FiqlParseException("Invalid property " + compName);
		}

		if (op == CompareOp.IN_LIST || op == CompareOp.NOT_IN_LIST) {
			Object[] values = FiqlValueParser.parseValues(prop.getType(), value);
			return new PrimitiveSearchListCondition<T>(prop, values, op);
		} 
		
		Object val = FiqlValueParser.parseValue(prop.getType(), value);
		return new PrimitiveSearchCondition<T>(prop, val, op);
	}

	protected AbstractComplexCondition<T> createCondititonInstance(BooleanOp op) {
		if (op == BooleanOp.AND) {
			return new AndSearchCondition<T>();
		} else if (op == BooleanOp.OR) {
			return new OrSearchCondition<T>();
		}

		return null;
	}

	private SearchCondition<T> buildExpression(ASTExpression exp) throws FiqlParseException {
		List<ASTNode> subNodes = exp.getSubNodes();

		if (subNodes == null || subNodes.isEmpty()) {
			return null;
		}

		SearchCondition<T> root = null;

		BooleanOp lastOp = null;

		for (ASTNode node: subNodes) {

			/* Operator node. Simply remember it for later */
			if (node instanceof ASTOperator) {
				lastOp = ((ASTOperator)node).getOp();
			} else {
				/* Build the search condition for the current node */
				SearchCondition<T> nodeCondition;

				if (node instanceof ASTExpression) {
					nodeCondition = buildExpression((ASTExpression)node);
				} else if (node instanceof ASTComparison) {
					nodeCondition = buildComparison((ASTComparison)node);
				} else {
					throw new FiqlParseException("Expected comparison/expression found " + node.getClass().getCanonicalName());
				}

				/* The node did not have any conditions so skip it */
				if (nodeCondition == null)
					continue;

				/* First expression/comparison set as root
				 */
				if (root == null) {
					root = nodeCondition;	
				} else {
					if (lastOp == null) {
						throw new FiqlParseException("found two consecutive expressions/comparisons. Operator expected");
					}
					
					/* Condition same as root append to it */
					if ((root instanceof AbstractComplexCondition) && ((AbstractComplexCondition<T>)root).getOperator() == lastOp) {
						((AbstractComplexCondition<T>)root).addCondition(nodeCondition);
					} else {
						AbstractComplexCondition<T> newCond = createCondititonInstance(lastOp);
						newCond.addCondition(root);
						newCond.addCondition(nodeCondition);

						root = newCond;
					}
				}
			}
		}

		return root;
	}

	public SearchCondition<T> build(String fiqlExpression) throws FiqlParseException {
		FiqlParser parser = new FiqlParser();
		ASTNode rootNode = parser.parse(fiqlExpression);

		return build(rootNode);
	}

	public final SearchCondition<T> build(ASTNode rootNode) throws FiqlParseException {
		if (rootNode == null) {
			return null;
		}

		if (rootNode instanceof ASTComparison) {
			return buildComparison((ASTComparison)rootNode);
		} else if (rootNode instanceof ASTExpression) {
			return buildExpression((ASTExpression)rootNode);
		}

		throw new FiqlParseException("Root node not an expression/comparison");
	}
}
