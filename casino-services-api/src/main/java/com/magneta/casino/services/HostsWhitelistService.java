package com.magneta.casino.services;

import java.sql.SQLException;

import com.magneta.casino.services.beans.HostWhitelistBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface HostsWhitelistService {
	public ServiceResultSet<HostWhitelistBean> getHosts(SearchContext<HostWhitelistBean> searchContext, int limit,int offset,SortContext<HostWhitelistBean> sortContext) throws ServiceException;
	public int getHostsSize(SearchContext<HostWhitelistBean> searchContext) throws ServiceException;
	public HostWhitelistBean getHost(Long itemId) throws ServiceException;
	public void updateHost(HostWhitelistBean hostWhitelist) throws ServiceException;
	public void insertHost(HostWhitelistBean hostWhitelist) throws ServiceException;
	public boolean deleteHost(HostWhitelistBean hostWhitelist) throws ServiceException, SQLException;
	public boolean isHost(String ip) throws ServiceException;
}
