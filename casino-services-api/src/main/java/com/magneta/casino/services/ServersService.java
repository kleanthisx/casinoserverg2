package com.magneta.casino.services;

import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface ServersService {
	ServerBean getServer(int serverId) throws ServiceException;
	ServiceResultSet<ServerBean> getServers(int limit, int offset)  throws ServiceException;
	ServiceResultSet<ServerBean> getServers(SearchContext<ServerBean> searchContext,SortContext<ServerBean> sortContext,int limit,int offset) throws ServiceException;
	int getServersSize(SearchContext<ServerBean> searchContext) throws ServiceException;
	void addServer(ServerBean server) throws ServiceException;
	void deleteServer(ServerBean server) throws ServiceException;
}
