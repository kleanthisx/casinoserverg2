package com.magneta.casino.services.exceptions;

public class AccountLevelViolation extends AccessDeniedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -782258438625589374L;

	public AccountLevelViolation() {
		super("Account creation is not allowed due to restrictions on your account");
	}

	public AccountLevelViolation(String string) {
		super(string);
	}
}
