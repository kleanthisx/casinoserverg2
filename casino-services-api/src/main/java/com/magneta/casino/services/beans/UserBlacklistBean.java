package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement 
@SQLTable("users_blacklist")
public class UserBlacklistBean  implements Serializable,Cloneable{
	private static final long serialVersionUID = -8035156844822714683L;
	private Long itemId;
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String countryCode;
	
	
	public UserBlacklistBean() {
		super();
	}
	
	public UserBlacklistBean(UserBlacklistBean original) {
		super();
		this.itemId = original.itemId;
		this.username = original.username;
		this.firstName = original.firstName;
		this.lastName = original.lastName;
		this.email = original.email;
		this.countryCode = original.countryCode;
	}
	
	@Override
	public Object clone() {
		return new UserBlacklistBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserBlacklistBean)) {
			return false;
		}

		return this.getItemId().equals(((UserBlacklistBean)o).getItemId());
	}
	
	@XmlAttribute
	@SQLColumn(value="item_id",isId=true)
	public Long getItemId() {
		return itemId;
	}
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	@XmlElement
	@SQLColumn("username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@XmlElement
	@SQLColumn("first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@XmlElement
	@SQLColumn("last_name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@XmlElement
	@SQLColumn("email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@XmlElement
	@SQLColumn("country")
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
