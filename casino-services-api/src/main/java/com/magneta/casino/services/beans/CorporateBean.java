package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("affiliates")
public class CorporateBean implements Serializable,Cloneable{

	private static final long serialVersionUID = -5760895204981043071L;
	private Long affiliateId;
	private Double affiliateRate;
	private Boolean approved;
	private String paymentPeriod;
	private Boolean allowFreeCredits;
	private Double freeCreditsRate;
	private Long modifierUser;
	private Boolean allowFullFreeCredits;
	private Boolean requiresApproval;
	private Integer subLevels;
	private Boolean requiredSuperApproval;
	
	public CorporateBean() {
		super();
	}

	public CorporateBean(CorporateBean original) {
		super();
		this.affiliateId = original.affiliateId;
		this.affiliateRate = original.affiliateRate;
		this.approved = original.approved;
		this.paymentPeriod = original.paymentPeriod;
		this.allowFreeCredits = original.allowFreeCredits;
		this.freeCreditsRate = original.freeCreditsRate;
		this.modifierUser = original.modifierUser;
		this.allowFullFreeCredits = original.allowFullFreeCredits;
		this.requiresApproval = original.requiresApproval;
		this.subLevels = original.subLevels;
		this.requiredSuperApproval = original.requiredSuperApproval;
	}
	

	@Override
	public Object clone() {
		return new CorporateBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof CorporateBean)) {
			return false;
		}
		
		return this.getAffiliateId().equals(((CorporateBean)o).getAffiliateId());
	}

	@XmlAttribute
	@SQLColumn(value="affiliate_id", isId=true)
	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	@XmlElement
	@SQLColumn("affiliate_rate")
	public Double getAffiliateRate() {
		return affiliateRate;
	}

	public void setAffiliateRate(Double affiliateRate) {
		this.affiliateRate = affiliateRate;
	}

	@XmlElement
	@SQLColumn("approved")
	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	@XmlElement
	@SQLColumn(value="payment_period", sqlType="interval")
	public String getPaymentPeriod() {
		return paymentPeriod;
	}

	public void setPaymentPeriod(String paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

	@XmlElement
	@SQLColumn("allow_free_credits")
	public Boolean getAllowFreeCredits() {
		return allowFreeCredits;
	}

	public void setAllowFreeCredits(Boolean allowFreeCredits) {
		this.allowFreeCredits = allowFreeCredits;
	}

	@XmlElement
	@SQLColumn("free_credits_rate")
	public Double getFreeCreditsRate() {
		return freeCreditsRate;
	}

	public void setFreeCreditsRate(Double freeCreditsRate) {
		this.freeCreditsRate = freeCreditsRate;
	}

	@XmlElement
	@SQLColumn("modifier_user")
	public Long getModifierUser() {
		return modifierUser;
	}

	public void setModifierUser(Long modifierUser) {
		this.modifierUser = modifierUser;
	}

	@XmlElement
	@SQLColumn("allow_full_free_credits")
	public Boolean getAllowFullFreeCredits() {
		return allowFullFreeCredits;
	}

	public void setAllowFullFreeCredits(Boolean allowFullFreeCredits) {
		this.allowFullFreeCredits = allowFullFreeCredits;
	}

	@XmlElement
	@SQLColumn("require_approval")
	public Boolean getRequiresApproval() {
		return requiresApproval;
	}

	public void setRequiresApproval(Boolean requiresApproval) {
		this.requiresApproval = requiresApproval;
	}

	@XmlElement
	@SQLColumn("sub_levels")
	public Integer getSubLevels() {
		return subLevels;
	}

	public void setSubLevels(Integer subLevels) {
		this.subLevels = subLevels;
	}

	@XmlElement
	@SQLColumn("requires_super_approval")
	public Boolean getRequiredSuperApproval() {
		return requiredSuperApproval;
	}

	public void setRequiredSuperApproval(Boolean requiredSuperApproval) {
		this.requiredSuperApproval = requiredSuperApproval;
	}
}
