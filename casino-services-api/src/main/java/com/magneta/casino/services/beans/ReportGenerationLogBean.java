package com.magneta.casino.services.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

/**
 * @author anarxia
 *
 */
@XmlRootElement
@SQLTable("report_generation_logs")
public class ReportGenerationLogBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6600196245884324539L;
	private Long userId;
	private String applicationName;
	private String reportName;
	private Date fromDate;
	private Date toDate;
	private String clientIp;
	private Date generationDate;
	private Long reportGenerationLogId;
	private String requestUrl;
	
	public ReportGenerationLogBean() {
		
	}
	
	public ReportGenerationLogBean(ReportGenerationLogBean orig) {
		this.userId = orig.getUserId();
		this.applicationName = orig.getApplicationName();
		this.reportName = orig.getReportName();
		this.fromDate = orig.getFromDate();
		this.toDate = orig.getToDate();
		this.clientIp = orig.getClientIp();
		this.generationDate = orig.getGenerationDate();
		this.reportGenerationLogId = orig.getReportGenerationLogId();
		this.requestUrl = orig.getRequestUrl();
	}
	
	@Override
	public Object clone() {
		return new ReportGenerationLogBean(this);
	}
	
	@XmlElement
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@XmlElement
	@SQLColumn("application_name")
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	@XmlElement
	@SQLColumn("report_name")
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	@XmlElement
	@SQLColumn("from_date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	@XmlElement
	@SQLColumn("to_date")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	@XmlElement
	@SQLColumn("user_ip")
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	
	@XmlElement
	@SQLColumn(value="generation_date", refreshOnInsert=true)
	public Date getGenerationDate() {
		return generationDate;
	}
	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}
	
	@XmlAttribute
	@SQLColumn(value="report_generation_log_id",isId=true, refreshOnInsert=true)
	public Long getReportGenerationLogId() {
		return reportGenerationLogId;
	}
	public void setReportGenerationLogId(Long reportGenerationLogId) {
		this.reportGenerationLogId = reportGenerationLogId;
	}
	
	@XmlElement
	@SQLColumn("request_url")
	public String getRequestUrl() {
		return requestUrl;
	}
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
}
