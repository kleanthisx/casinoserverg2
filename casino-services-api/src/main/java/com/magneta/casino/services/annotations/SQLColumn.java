package com.magneta.casino.services.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SQLColumn {

	/**
	 * The name of the column in the table
	 */
	public String value();
	
	/**
	 * Whether the column is not really a column
	 * but an sql expression. Expression columns
	 * are be used for searching and during update/insert
	 * they are automatically refreshed.
	 * 
	 * @return
	 */
	public boolean expression() default false;
	
	/**
	 * Whether the column is not really a column
	 * but an sql aggregate function. expression MUST
	 * also be true in this case. Aggregates cannot be
	 * used as search conditions.
	 * 
	 * @return
	 */
	public boolean aggregate() default false;
	
	/**
	 * Whether the column needs to be refreshed
	 * on every update. Please note that expressions
	 * will be refreshed regardless of this.
	 * @return
	 */
	public boolean refreshOnUpdate() default false;
	
	/**
	 * Whether the column needs to be refreshed
	 * on every insert. Please note that expressions
	 * will be refreshed regardless of this.
	 * @return
	 */
	public boolean refreshOnInsert() default false;
	
	/**
	 * Whether the column should never be updated.
	 * 
	 * @return
	 */
	public boolean skipUpdate() default false;
	
	/**
	 * Whether the column is part of the unique identifier
	 * of the object in the database (ie part of primary key).
	 * 
	 * @return
	 */
	public boolean isId() default false;
	
	/**
	 * The type in the database for the column.
	 * It is used to cast database values for comparison
	 * against the java type.
	 * @return
	 */
	public String sqlType() default "";
}
