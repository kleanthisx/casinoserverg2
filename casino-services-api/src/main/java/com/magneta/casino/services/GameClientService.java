package com.magneta.casino.services;

import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.beans.GameClientGameBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public interface GameClientService {
	
	ServiceResultSet<GameClientBean> getGameClients(SearchContext<GameClientBean> searchContext, int limit,int offset,SortContext<GameClientBean> sortContext) throws ServiceException;
	int getGameClientsSize(SearchContext<GameClientBean> searchContext) throws ServiceException;
	
	ServiceResultSet<GameClientBean> getGameClients(int limit,int offset) throws ServiceException;
	
	GameClientBean getGameClient(Integer clientId)throws ServiceException;
	
	ServiceResultSet<GameClientGameBean> getGameClientGames(Integer clientId,SearchContext<GameClientGameBean> searchContext, int limit, int offset,SortContext<GameClientGameBean> sortContext)throws ServiceException;
	int getGameClientGamesSize(Integer clientId,SearchContext<GameClientGameBean> searchContext) throws ServiceException;
	
	void addGameClient(GameClientBean client) throws ServiceException;
	void deleteGameClient(Integer clientId) throws ServiceException;
	void updateGameClient(GameClientBean client) throws ServiceException;
}
