package com.magneta.casino.services.search;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.magneta.casino.services.search.parser.ASTComparison.CompareOp;

public final class SearchUtils {

	private static final DecimalFormat format;
	
	static {
		format = new DecimalFormat();
		format.setGroupingUsed(false);
		DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		format.setDecimalFormatSymbols(symbols);
	}

	public static void startSqlQuery(StringBuilder sb, String table, String... columns) {
		sb.append("SELECT ");
		if (columns != null && columns.length > 0) {
			for (int i = 0; i < columns.length; i++) {
				sb.append(columns[i]);
				if (i + 1 < columns.length) {
					sb.append(",");
				}
			}
		} else {
			sb.append("*");
		}
		sb.append(" FROM ").append(table).append(" WHERE ");
	}

	private static void append(Appendable sb, CharSequence s) {
		try {
			sb.append(s);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void append(Appendable sb, int i) {
		try {
			sb.append(format.format(i));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void sqlAppendValueMark(Appendable sb, String columnType, Object value) {
		if (value == null) {
			append(sb, "NULL");
		}
		
		if(columnType == null || columnType.isEmpty()) {
			append(sb, "?");
		} else {
			append(sb, "CAST (? AS " + columnType +")");
		}
	}
	
	private static void sqlAppendEqualsIgnoreCase(Appendable sb, String column, Object value, String columnType) {
		if(value == null) {
			append(sb, column);
			append(sb, " IS NULL");
		} else if (value instanceof String) {
			String val = (String)value;
			
			if(val.startsWith("*") || val.endsWith("*")) {
				if (val.startsWith("*") && val.endsWith("*")) {
					append(sb, "POSITION(LOWER(?) IN LOWER(");
					append(sb, column);
					append(sb, "))");
					append(sb, " >= 1");
				} else if (val.startsWith("*")) {
					append(sb, "POSITION(LOWER(?) IN LOWER(");
					append(sb, column);
					append(sb, ")) = GREATEST(1, LENGTH(");
					append(sb, column);
					append(sb, ") - ");
					append(sb, val.length() - 2);
					append(sb, ")");
				} else {
					append(sb, "POSITION(LOWER(?) IN LOWER(");
					append(sb, column);
					append(sb, ")) = 1");
				}
			} else {
				append(sb, "LOWER(");
				append(sb, column);
				append(sb, ") = LOWER(");
				sqlAppendValueMark(sb, columnType, value);
				append(sb, ")");
			}
		} else {
			append(sb, column);
			append(sb, " = ");
			sqlAppendValueMark(sb, columnType, value);
		}
	}
	
	private static void sqlAppendNotEqualsIgnoreCase(Appendable sb, String column, Object value, String columnType) {
		if(value == null) {
			append(sb, column);
			append(sb, " IS NOT NULL");
		} else if (value instanceof String) {
			String val = (String)value;
			
			if(val.startsWith("*") || val.endsWith("*")) {
				if (val.startsWith("*") && val.endsWith("*")) {
					append(sb, "POSITION(LOWER(?) IN LOWER(");
					append(sb, column);
					append(sb, ")) < 1");
				} else if (val.startsWith("*")) {
					append(sb, "POSITION(LOWER(?) IN LOWER(");
					append(sb, column);
					append(sb, ")) <> GREATEST(1, LENGTH(");
					append(sb, column);
					append(sb, ") - ");
					append(sb, val.length() - 2);
					append(sb, ")");
				} else {
					append(sb, "POSITION(LOWER(?) IN LOWER(");
					append(sb, column);
					append(sb, ")) < 1");
				}
			} else {
				append(sb, " LOWER(");
				append(sb, column);
				append(sb, ") <> LOWER(");
				sqlAppendValueMark(sb, columnType, value);
				append(sb, ")");
			}
		} else {
			append(sb, column);
			append(sb, " <> ");
			sqlAppendValueMark(sb, columnType, value);
		}
	}
	
	private static void sqlAppendEquals(Appendable sb, String column, Object value, String columnType) {
		if(value == null) {
			append(sb, column);
			append(sb, " IS NULL");
		} else if (value instanceof String) {
			String val = (String)value;

			if(val.startsWith("*") || val.endsWith("*")) {
				if (val.startsWith("*") && val.endsWith("*")) {
					append(sb, "POSITION(? IN ");
					append(sb, column);
					append(sb, ")");
					append(sb, " >= 1");
				} else if (val.startsWith("*")) {
					append(sb, "POSITION(? IN ");
					append(sb, column);
					append(sb, ")");
					append(sb, " = GREATEST(1, LENGTH(");
					append(sb, column);
					append(sb, ") - ");
					append(sb, val.length() - 2);
					append(sb, ")");
				} else {
					append(sb, "POSITION(? IN ");
					append(sb, column);
					append(sb, ") = 1");
				}
				
			} else {
				append(sb, column);
				append(sb, " = ");
				sqlAppendValueMark(sb, columnType, value);
			}
		} else {
			append(sb, column);
			append(sb, " = ");
			sqlAppendValueMark(sb, columnType, value);
		}
	}
	
	private static void sqlApppendNotEquals(Appendable sb, String column, Object value, String columnType) {
		if(value == null) {
			append(sb, column);
			append(sb, " IS NOT NULL");
		} else if (value instanceof String) {
			String val = (String)value;

			if(val.startsWith("*") || val.endsWith("*")) {
				if (val.startsWith("*") && val.endsWith("*")) {
					append(sb, "POSITION(? IN ");
					append(sb, column);
					append(sb, ")");
					append(sb, " < 1");
				} else if (val.startsWith("*")) {
					append(sb, "POSITION(? IN ");
					append(sb, column);
					append(sb, ")");
					append(sb, " <> GREATEST(1, LENGTH(");
					append(sb, column);
					append(sb, ") - ");
					append(sb, val.length() - 2);
					append(sb, ")");
				} else {
					append(sb, "POSITION(? IN ");
					append(sb, column);
					append(sb, ") < 1");
				}
			} else {
				append(sb, column);
				append(sb, " <> ");
				sqlAppendValueMark(sb, columnType, value);
			}
		} else {
			append(sb, column);
			append(sb, " <> ");
			sqlAppendValueMark(sb, columnType, value);
		}
	}
	
	
	public static void sqlApppendCondition(Appendable sb, String column, CompareOp ct, Object value, String columnType) {
		switch (ct) {
		case EQUALS:
			sqlAppendEquals(sb, column, value, columnType);
			break;
		case NOT_EQUALS:
			sqlApppendNotEquals(sb, column, value, columnType);
			break;
		case EQUALS_IGNORE_CASE:
			sqlAppendEqualsIgnoreCase(sb, column, value, columnType);
			break;
		case NOT_EQUALS_IGNORE_CASE:
			sqlAppendNotEqualsIgnoreCase(sb, column, value, columnType);
			break;
		case GREATER_THAN:
			append(sb, column);
			append(sb, " > ");
			sqlAppendValueMark(sb, columnType, value);
			break;
		case GREATER_OR_EQUALS:
			append(sb, column);
			append(sb, " >= ");
			sqlAppendValueMark(sb, columnType, value);
			break;
		case LESS_THAN:
			append(sb, column);
			append(sb, " < ");
			sqlAppendValueMark(sb, columnType, value);
			break;
		case LESS_OR_EQUALS:
			append(sb, column);
			append(sb, " <= ");
			sqlAppendValueMark(sb, columnType, value);
			break;
		case IN_LIST:
		case NOT_IN_LIST:
			Object[] values = (Object[])value;

			/* Empty list. Postgres does not support this. */
			if (values == null || values.length == 0) {
				if (ct == CompareOp.NOT_IN_LIST) {
					append(sb, column);
					append(sb, " IS NOT NULL");
				} else {
					append(sb, "1 <> 1");
				}
			} else {
				append(sb, column);
				if (ct == CompareOp.NOT_IN_LIST) {
					append(sb, " NOT");
				}
				append(sb, " IN (");

				for (int i=0; i < values.length; i++) {
					if (i > 0) {
						append(sb, ", ");
					}

					sqlAppendValueMark(sb, columnType, values[i]);
				}
				append(sb, ")");
			}
			break;
		default:
			String msg = String.format("Condition type %s is not supported", ct.name());
			throw new RuntimeException(msg);
		}
	}
	
	public static int setSQLStatementValue(PreparedStatement stmt, int startIndex, CompareOp op, Object propertyValue) throws SQLException {
    	if (propertyValue == null ) {
    		return 0;
    	}

    	if (propertyValue instanceof String
    		&& (op == CompareOp.EQUALS || op == CompareOp.EQUALS_IGNORE_CASE 
    		    || op == CompareOp.NOT_EQUALS
    		    || op == CompareOp.NOT_EQUALS_IGNORE_CASE)) {
    		String prop = (String)propertyValue;

    		if (prop.startsWith("*")) {
    			prop = prop.substring(1);
    		} 
    		
    		if (prop.endsWith("*")) {
    			prop = prop.substring(0, prop.length() - 1);
    		}
    		
    		stmt.setString(startIndex, prop);
    		
    		return 1;
    	} else if (propertyValue instanceof Date) {
    		Date date = (Date)propertyValue;
    		Timestamp timestamp = new Timestamp(date.getTime());
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
            stmt.setTimestamp(startIndex, timestamp, cal);
            return 1;
    	} else {		 
    		stmt.setObject(startIndex, propertyValue);
    		return 1;
    	}
    }
}
