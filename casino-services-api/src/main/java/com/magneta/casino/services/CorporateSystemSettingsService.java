package com.magneta.casino.services;

public interface CorporateSystemSettingsService {

	public String getSetting(Long corporateId, String key) throws ServiceException;
	public void setSetting(Long corporateId, String key, String value) throws ServiceException;
	public void removeSetting(Long corporateId, String key) throws ServiceException;
}
