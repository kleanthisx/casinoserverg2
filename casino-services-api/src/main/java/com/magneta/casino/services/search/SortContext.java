package com.magneta.casino.services.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.magneta.casino.services.search.PropertySort.SortOrder;

public class SortContext<T> {
	
	protected String expression;

	public SortContext(String expression) { 
		this.expression = expression;
	}
	
	public List<PropertySort<T>> getConditions(Class<T> klass) {
		if(expression == null || expression.isEmpty()) {
			return null;
		}
		
		String[] expressions = expression.split(",");
		List<PropertySort<T>> retList = new ArrayList<PropertySort<T>>(expressions.length);

		for(String property : expressions) {
			SortOrder order = PropertySort.SortOrder.ASC;
			
			if (property.charAt(0) == '!') {
				order = PropertySort.SortOrder.DESC;
				property = property.substring(1);
			}

			PropertySort<T> propertySort = new PropertySort<T>(klass,property);
			propertySort.setOrder(order);
			retList.add(propertySort);
		}
		
		if(retList.isEmpty()) {
			return null;
		}
		
		return retList;
	}
	
	public String getExpression() {
		return expression;
	}
	
	private static final class ListComparator<T> implements Comparator<T> {

		private final List<PropertySort<T>> sortProperties;
		
		public ListComparator(List<PropertySort<T>> sortProperties) {
			this.sortProperties = sortProperties;
		}
		
		@Override
		public int compare(T a, T b) {
			for (PropertySort<T> sort: sortProperties) {
				if (sort.getOrder() == SortOrder.NONE) {
					continue;
				}
				
				int comparison = sort.compare(a, b);
				
				if (comparison == 0) {
					continue;
				}

				if (sort.getOrder() == SortOrder.ASC) {
					return comparison;
				}
				return -comparison;
			}
			return 0;
		}
		
	}
	
	public void sortList(Class<T> klass, List<T> list) {
		if (list == null)
			return;
		
		List<PropertySort<T>> sortProperties = getConditions(klass);
		
		if (sortProperties == null)
			return;

		Collections.sort(list, new ListComparator<T>(sortProperties));		
	}
}
