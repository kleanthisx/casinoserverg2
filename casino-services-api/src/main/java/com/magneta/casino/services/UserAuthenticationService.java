package com.magneta.casino.services;

import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;

public interface UserAuthenticationService {

	void authenticateUser(String username, String password, UserTypeEnum[] types, PrivilegeEnum requiredPrivilege) throws ServiceException;
	UserAuthenticationBean createUserAuthenticationBean(Long userId) throws ServiceException;
	void logoutUser();
}
