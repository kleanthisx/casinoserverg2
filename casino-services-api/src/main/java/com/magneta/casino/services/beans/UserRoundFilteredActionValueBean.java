package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlRootElement 
public class UserRoundFilteredActionValueBean implements Serializable, Cloneable  {
	private static final long serialVersionUID = 3155308012771340724L;

	public enum ValueType {
		LITERAL,
		SYMBOL, CARD
	}
	
	private ValueType type;
	private String value;
	
	public UserRoundFilteredActionValueBean() {
		super();
	}
	
	public UserRoundFilteredActionValueBean(UserRoundFilteredActionValueBean original) {
		super();
		this.setType(original.getType());
		this.setValue(original.getValue());
	}
	
	@Override
	public Object clone() {
		return new UserRoundFilteredActionValueBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof UserRoundFilteredActionValueBean)) {
			return false;
		}
		return this.hashCode() == ((UserRoundFilteredActionValueBean)o).hashCode();
	}

	public void setType(ValueType type) {
		this.type = type;
	}

	@XmlElement
	public ValueType getType() {
		return type;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlElement
	public String getValue() {
		return value;
	}
}
