package com.magneta.casino.services.enums;

public enum SettingTypeEnum {
	INTEGER(0),
	DOUBLE(1),
	BOOLEAN(2),
	STRING(3),
	PERCENTAGE(4),
	COUNTRY(5),
	MULTILINE_STRING(6),
	LONG(7);
	
	private final int val;
	
	SettingTypeEnum(int val) {
		this.val = val;
	}
	
	public int getIntVal() {
		return this.val;
	}
	
	public static SettingTypeEnum valueOf(int val) {
		SettingTypeEnum[] vals = SettingTypeEnum.values();
		
		for (SettingTypeEnum e: vals) {
			if (e.val == val) {
				return e;
			}
		}
		
		throw new IllegalArgumentException("Invalid SettingTypeEnum value");
	}
}