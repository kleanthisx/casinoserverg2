package com.magneta.casino.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

public class TimeZoneService {
	private List<String> timeZones;

	private static final String[] invalidPrefix = {
		"Etc",
		"SystemV"
	};

	private boolean isValidId(String id) {
		if (id.length() < 3)
			return false;

		if (!id.contains("/")) {
			return false;
		}

		String[] parts = id.split("/");

		for (String prefix: invalidPrefix) {
			if (parts[0].equalsIgnoreCase(prefix)) {
				return false;
			}
		}

		return true;
	}

	public TimeZoneService() {
		timeZones = new ArrayList<String>();
		String[] zoneIds = TimeZone.getAvailableIDs();
		for (String id: zoneIds){
			if (isValidId(id)) {
				timeZones.add(id);
			}
		}
		Collections.sort(timeZones);
	}

	public List<String> getTimeZones() {
		return timeZones;
	}
}
