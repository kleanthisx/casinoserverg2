package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;

@XmlRootElement 
public class GameClientGameBean extends GameBean implements Serializable,Cloneable {
	private static final long serialVersionUID = 6139457118649726312L;
	private Boolean enabled;
	
	public GameClientGameBean() {
		super();
	}
	
	
	public GameClientGameBean(GameBean original) {
		super(original);
	}
	
	public GameClientGameBean(GameClientGameBean original) {
		super(original);
		this.setEnabled(original.getEnabled());
	}
	
	@Override
	public Object clone() {
		return new GameClientGameBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof GameClientGameBean)) {
			return false;
		}
		
		return this.getGameId().equals(((GameClientGameBean)o).getGameId());
	}
	
	@XmlElement
	@SQLColumn("enabled")
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return enabled;
	}
	
}
