package com.magneta.casino.services;

import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.CorporateEarningsTransactionBean;

public interface CorporateEarningsService {

	CorporateEarningsBean getEarnings(Long corporateId) throws ServiceException;
	void transferEarnings(CorporateEarningsTransactionBean transaction) throws ServiceException;
}
