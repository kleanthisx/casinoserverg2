package com.magneta.casino.services;

import com.magneta.casino.services.beans.CorporateTransferTransactionBean;

public interface CorporateTransfersService {

	CorporateTransferTransactionBean getTransfer(Long transferId) throws ServiceException;

	void createTransfer(CorporateTransferTransactionBean transfer)
			throws ServiceException;
}
