package com.magneta.casino.services;

import java.util.Locale;

/**
 * Service for parsing currency.
 * 
 * The service will always ensure that excess precision is trimmed
 * as per the active currency.
 * 
 * @author anarxia
 *
 */
public interface CurrencyParser {

	/**
	 * Parses a double using Locale.US.
	 * 
	 * We do not use the default locale for consistency reasons.
	 * @param doubleString
	 * @return
	 */
	Double parseDouble(String doubleString);
	
	/**
	 * Parses a double for the given locale.
	 * 
	 * The parser will automatically truncate decimals more
	 * than the currency allows.
	 * 
	 * @param locale
	 * @param doubleString
	 * @return
	 */
	Double parseDouble(Locale locale, String doubleString);

	/**
	 * Normalizes a double when received from untrusted sources.
	 * 
	 * This methods ensures that the returned double is:
	 * 
	 * 1. Within the allowed precision of the active currency.
	 * 2. Always 0.0 for any approximation of zero. (A lot of code assumes that).
	 * 
	 * 
	 * @param d
	 * @return
	 */
	double normalizeDouble(double d);

	/**
	 * Formats a double for the given locale.
	 * 
	 * @param amount
	 * @param locale
	 * @return
	 */
	String formatDouble(Double amount, Locale locale);

	/**
	 * Formats a double using Locale.US.
	 * 
	 * We do not use the default locale for consistency reasons.
	 * 
	 * @param amount The amount to format.
	 * @return
	 */
	String formatDouble(Double amount);
	
	/**
	 * Formats a double with its currency symbol for the given locale.
	 * 
	 * @param amount
	 * @param locale
	 * @return
	 */
	String formatCurrency(Double amount, Locale locale);

	/**
	 * Formats a double with its currency symbol using Locale.US.
	 * 
	 * We do not use the default locale for consistency reasons.
	 * 
	 * @param amount The amount to format.
	 * @return
	 */
	String formatCurrency(Double amount);

	String getCurrencySymbol(Locale locale);
}
