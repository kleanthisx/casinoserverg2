package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("affiliate_users")
public class AffiliateUsersBean implements Serializable,Cloneable{

	private static final long serialVersionUID = -3814029298014561899L;
	
	private Long affiliateId;
	private Long userId;
	private Integer affiliation;
	
	public AffiliateUsersBean(){
		super();
	}
	
	public AffiliateUsersBean(AffiliateUsersBean original){
		this.affiliateId = original.affiliateId;
		this.userId = original.userId;
		this.affiliation = original.affiliation;
	}
	
	@Override
	public Object clone() {
		return new AffiliateUsersBean(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}
	
	@XmlAttribute
	@SQLColumn("user_id")
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@XmlElement
	@SQLColumn("affiliate_id")
	public Long getAffiliateId() {
		return affiliateId;
	}
	
	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	
	@XmlElement
	@SQLColumn("affiliation")
	public Integer getAffiliation() {
		return affiliation;
	}
	
	public void setAffiliation(Integer affiliation) {
		this.affiliation = affiliation;
	}

	
}
