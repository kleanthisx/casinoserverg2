package com.magneta.casino.services.search.parser;

import java.io.StringReader;
import java.util.List;

import com.magneta.casino.services.search.FiqlParseException;

/**
 * Parser for Magneta FIQL expressions.
 * 
 * Note: The Magneta FIQL dialect is based on FIQL but it contains extensions to the
 * standard. Whitespaces are not allowed anywhere outside of values. Special characters
 * can be escaped using \
 * 
 * <p>Expression</p>
 * <code>
 * property&lt;COMPARISON>&lt;VALUE>
 * </code>
 * 
 * <p>Logical Operators:</p>
 * <table>
 * <tr><td>;</td><td>And</td></tr>
 * <tr><td>,</td><td>Or</td></tr>
 * </table>
 * 
 * <p>Comparison:</p>
 * <table>
 * <tr><td>==</td><td>Equals</td></tr>
 * <tr><td>!=</td><td>Not equals</td></tr>
 * <tr><td>=gt=</td><td>Greater than</td></tr>
 * <tr><td>=ge=</td><td>Greater or equal</td></tr>
 * <tr><td>=lt=</td><td>Less than</td></tr>
 * <tr><td>=le=</td><td>Less or equal</td></tr>
 * <tr><td>~=</td><td>Equals ignore case</td></tr>
 * <tr><td>!~=</td><td>Not equals ignoring case</td></tr>
 * <tr><td>=in=</td><td>In list</td></tr>
 * <tr><td>!in=</td><td>Not in list</td></tr>
 * </table>
 * <p>Properties:</p>
 * Properties end on the first = ! ~
 * <p>Values:</p>
 * Values end on the first right parenthesis ")" or Logical operator.
 * String Values may start and/or end with * for substring matching.
 * Nulls values may be specified with "NULL". 
 * @author anarxia
 */
public class FiqlParser {
    
    /**
     * Parses expression. Names used in FIQL expression are names of properties
     * in a type T.
     * <p>
     * Example:
     * 
     * <pre>
     * class Condition {
     *   public String getFoo() {...}
     *   public void setFoo(String foo) {...}
     *   public int getBar() {...}
     *   public void setBar(int bar) {...}
     * }
     * 
     * FiqlParser parser = new FiqlParser();
     * parser.parse("foo==mystery*;bar=ge=10");
     * </pre>
     * 
     * @param fiqlExpression expression of filter.
     * @return ASTComparison/ASTExpression representing search structure.
     * 
     * @throws FiqlParseException when expression does not follow Magneta FIQL grammar
     */
    public ASTNode parse(String fiqlExpression) throws FiqlParseException {
    	StringReader s = new StringReader(fiqlExpression);
    	ASTExpression ast = new ASTExpression();
        
        ast.parse(new ParserContext(s));
        
        List<ASTNode> subNodes = ast.getSubNodes();
        
        if (subNodes == null || subNodes.isEmpty()) {
        	return null;
        }
        
        if (subNodes.size() == 1) {
        	return subNodes.get(0);
        }
        
        return ast;
    }
}
