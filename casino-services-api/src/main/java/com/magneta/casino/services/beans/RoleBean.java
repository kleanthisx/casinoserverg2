package com.magneta.casino.services.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;

@XmlRootElement
@SQLTable("roles")
public class RoleBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 251714701797609972L;
	private Integer roleId;
	private String description;
	
	@Override
	public Object clone() {
		RoleBean clone = new RoleBean();
		clone.setRoleId(getRoleId());
		clone.setDescription(getDescription());
		return clone;
	}
	
	@XmlAttribute
	@SQLColumn(value="role_id", isId=true, refreshOnInsert=true)
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	@XmlElement
	@SQLColumn("role_desc")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
