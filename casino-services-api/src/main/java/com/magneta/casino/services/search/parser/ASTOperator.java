package com.magneta.casino.services.search.parser;

import java.io.IOException;
import java.io.PrintStream;

import com.magneta.casino.services.search.FiqlParseException;

public class ASTOperator implements ASTNode {
	public enum BooleanOp {
		AND(';', "AND"),
		OR(',', "OR");
		
		private char op;
		private String s;
		
		private BooleanOp(char op, String s) {
			this.op = op;
			this.s = s;
		}
		
		@Override
		public String toString() {
			return s;
		}
		
		public char getOp() {
			return op;
		}
		
		public boolean is(char s) {
			return op == s;
		}
		
		public static BooleanOp parse(char c) throws FiqlParseException {
			for (BooleanOp o: BooleanOp.values()) {
				if (o.is(c))
					return o;
			}
			
			throw new FiqlParseException("Invalid operator \"" + c + "\"");
		}
		
		public static boolean isAssignableFrom(char c) {
			for (BooleanOp o: BooleanOp.values()) {
				if (o.is(c))
					return true;
			}
			
			return false;
		}
	}
	
	private BooleanOp op;
	
	@Override
	public void print(PrintStream out) {
		out.append(op.op + "\n");
		
	}
	
	public BooleanOp getOp() {
		return this.op;
	}

	@Override
	public void parse(ParserContext s) throws FiqlParseException {
		char [] buf = new char[1];
		
		try {
			if (s.read(buf)) {
				this.op = BooleanOp.parse(buf[0]);
			}
		} catch (IOException e) {
			s.parseException(e);
		} catch (FiqlParseException e) {
			s.parseException(e.getMessage());
		}
		
	}
}
