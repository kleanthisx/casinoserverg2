package com.magneta.casino.internal.beans;

import com.magneta.casino.services.enums.UserTypeEnum;

public class SystemPrincipalBean extends ExtendedPrincipalBeanImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = -740383558012085164L;

	public SystemPrincipalBean() {
		super();
		this.setType(UserTypeEnum.SYSTEM);
	}
}
