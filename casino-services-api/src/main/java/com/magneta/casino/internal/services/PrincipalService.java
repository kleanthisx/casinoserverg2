package com.magneta.casino.internal.services;


import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.UserAuthenticationBean;

/**
 * Provides the security principal across services.
 * 
 * @author anarxia
 *
 */
public interface PrincipalService {
	
	public ExtendedPrincipalBean getPrincipal();
	
	/**
	 * This method is called when a user has been authenticated.
	 * 
	 * Implementations should log the user authentication and set the current principal.
	 *  
	 * @param authenticatedUser
	 * @throws ServiceException
	 */
	public void onUserAuthenticated(UserAuthenticationBean authenticatedUser) throws ServiceException;	
	public void onUserLogout();
	

	public void pushSystem();
	public void popSystem();
	public void resetSystem();
}
