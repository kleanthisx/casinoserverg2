package com.magneta.casino.internal.services;

import java.sql.Connection;

import com.magneta.casino.services.exceptions.DatabaseException;

public interface DatabaseService {

	Connection getConnection(boolean readOnly) throws DatabaseException;
}
