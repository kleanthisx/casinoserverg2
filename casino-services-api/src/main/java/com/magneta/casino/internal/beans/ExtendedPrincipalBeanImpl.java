package com.magneta.casino.internal.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.magneta.casino.services.beans.RolePrivilegeBean;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;

public class ExtendedPrincipalBeanImpl implements ExtendedPrincipalBean {
	private static final long serialVersionUID = -5686199851348451878L;
	
	private Long userId;
	private UserTypeEnum type;
	private Map<String,Boolean> privileges;

	public ExtendedPrincipalBeanImpl() {
		super();
	}
	
	public ExtendedPrincipalBeanImpl(ExtendedPrincipalBeanImpl original) {
		super();
		this.userId = original.userId;
		this.type = original.type;
		this.privileges = original.privileges;
	}

	public ExtendedPrincipalBeanImpl(UserAuthenticationBean authenticationBean) {
		super();
		
		this.userId = authenticationBean.getUserId();
		this.privileges = new HashMap<String,Boolean>();
		this.type = authenticationBean.getUserType();
		
		List<RolePrivilegeBean> privs = authenticationBean.getPrivileges();
		
		if (privs != null) {
			for (RolePrivilegeBean priv: privs) {
				privileges.put(priv.getPrivilege(), priv.isAllowWrite());
			}
		}
	}
	
	@Override
	public Object clone() {
		return new ExtendedPrincipalBeanImpl(this);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof ExtendedPrincipalBean)) {
			return false;
		}
		return this.getUserId().equals(((ExtendedPrincipalBean)o).getUserId())
			&& this.isSystem() == ((ExtendedPrincipalBean)o).isSystem();
	}

	@Override
	public Long getUserId() {
		return userId;
	}

	@Override
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Override
	public String getName() {
		if (this.isSystem()) {
			return "SYSTEM";
		}
		
		return Long.toString(this.userId);
	}

	@Override
	public void setType(UserTypeEnum userType) {
		this.type = userType;
	}

	@Override
	public UserTypeEnum getType() {
		if (this.isSystem()) {
			return UserTypeEnum.SYSTEM;
		}
		return type;
	}
	
	/**
	 * Checks whether the principal has the given privilege.
	 * 
	 * @param privilege The privilege to check.
	 * @param write Whether write permission for the privilege is required.
	 * 
	 * @return
	 */
	@Override
	public boolean hasPrivilege(String privilege, boolean write) {
		if (isSystem()) {
			return true;
		}
		
		Boolean priv = privileges.get(privilege);
		
		if (priv == null) {
			return false;
		}
		
		if (write) {
			return priv;
		}

		return true;
	}
	
	@Override
	public boolean hasPrivilege(PrivilegeEnum priv, boolean write) {
		return hasPrivilege(priv.getId(), write);
	}
	
	@Override
	public final boolean isSystem() {
		return this.type == UserTypeEnum.SYSTEM;
	}
}
