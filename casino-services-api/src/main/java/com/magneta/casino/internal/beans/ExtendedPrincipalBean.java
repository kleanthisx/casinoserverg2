package com.magneta.casino.internal.beans;

import java.io.Serializable;
import java.security.Principal;

import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;

public interface ExtendedPrincipalBean extends Serializable,Cloneable,Principal {
	public Long getUserId();
	public void setUserId(Long userId);
	public void setType(UserTypeEnum userType);
	public UserTypeEnum getType();
	
	/**
	 * Checks whether the principal has the given privilege.
	 * 
	 * @param privilege The privilege to check.
	 * @param write Whether write permission for the privilege is required.
	 * 
	 * @return
	 */
	public boolean hasPrivilege(String privilege, boolean write);
	public boolean hasPrivilege(PrivilegeEnum priv, boolean write);
	public boolean isSystem();
}
