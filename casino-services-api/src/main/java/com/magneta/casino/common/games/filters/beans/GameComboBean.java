package com.magneta.casino.common.games.filters.beans;

import java.io.Serializable;


public class GameComboBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final ComboSymbolBean[] symbols;
	private double payout;
	private final String details;
	private final boolean updatable;
	
	public GameComboBean(ComboSymbolBean[] symbols, double payout) {
		this(symbols, "", payout, true);
	}

	public GameComboBean(ComboSymbolBean[] symbols, String details, double payout) {
        this(symbols,details,payout,true);
    }
	
	public GameComboBean(ComboSymbolBean[] symbols, String details, double payout, boolean updatable) {
	    super();
        this.symbols = symbols;
        this.payout = payout;
        this.details = details;
        this.updatable = updatable;
	}
	
	public GameComboBean(GameComboBean bean){
	    this.symbols = bean.symbols;
        this.payout = bean.payout;
        this.details = bean.details;
        this.updatable = bean.updatable;
	}
	
	public double getPayout() {
		return payout;
	}

	public void setPayout(double payout) {
		this.payout = payout;
	}

	public ComboSymbolBean[] getSymbols() {
		return symbols;
	}
    
    public String getDetails() {
        return details;
    }

    public boolean isUpdatable() {
        return updatable;
    }
}
