package com.magneta.casino.common.games.filters.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GameActionBean {
    
    private int seat;
    private String action;
    private String area;
    private String value;
    private Double amount;

    private boolean generated;
    
    private Double balance;
    private Date actionTime;

    private List<GameActionValue> values;
    
    public GameActionBean() {
    	super();
    }
    
    public String getAction() {
        return action;
    }
    
    public void setAction(String action) {
        this.action = action;
    }
    
    public int getSeat() {
        return seat;
    }
    
    public void setSeat(int seat) {
        this.seat = seat;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public boolean getGenerated() {
        return generated;
    }
    
    public void setGenerated(boolean generated) {
        this.generated = generated;
    }
    
    public Double getAmount() {
        return amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getArea() {
        return area;
    }
    
    public void setArea(String area) {
        this.area = area;
    }

    
    public Double getBalance() {
        return balance;
    }
    
    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void addImageValue(String image, String alt){
    	if (values == null){
            values = new ArrayList<GameActionValue>(5);
        }
        
		values.add(new GameActionValue(alt,image));
    }

    public List<GameActionValue> getValues() {
    	return values;
    }

    public boolean hasImageValues(){
        return (values != null);
    }
    
	public Date getActionTime() {
		return actionTime;
	}
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
}
