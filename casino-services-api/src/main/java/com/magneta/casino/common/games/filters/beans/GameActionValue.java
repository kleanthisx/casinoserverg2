/**
 * 
 */
package com.magneta.casino.common.games.filters.beans;

public class GameActionValue {
    private String image;
    private String val;

    public GameActionValue(String val, String image){
        this.val = val;
        this.image = image;
    }

    public String getImage(){
        return image;
    }

    public String getValue(){
        return val;
    }

    public boolean hasImage(){
        return (image != null);
    }
}
