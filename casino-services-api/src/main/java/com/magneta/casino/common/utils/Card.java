/**
 * 
 */
package com.magneta.casino.common.utils;


/**
 * @author Nicos
 *
 */
public class Card {

    /**
     * The constant value used for the clubs card type.
     */
    public static final int CLUBS_CARD = 0;
    /**
     * The constant value used for the diamonds card type.
     */
    public static final int DIAMONDS_CARD = 1;
    /**
     * The constant value used for the hearts card type.
     */
    public static final int HEARTS_CARD = 2;
    /**
     * The constant value used for the spades card type.
     */
    public static final int SPADES_CARD = 3;
    /**
     * The constant value used for cards with not type (i.e. Joker).
     */
    public static final int NO_TYPE_CARD = 4;
    
    /**
     * The constant value used for the ace card.
     */
    public static final int ACE = 1;
    /**
     * The constant value used for the jack card.
     */
    public static final int JACK = 11;
    /**
     * The constant value used for the queen card.
     */
    public static final int QUEEN = 12;
    /**
     * The constant value used for the king card.
     */
    public static final int KING = 13;
    /**
     * The constant value used for the joker card.
     */
    public static final int JOKER = 14;
    
    public static final int MAX_CARD = JOKER;
    
    /**
     * The constant value used for a red card.
     */
    public static final int RED_CARD = 100;
    
    /**
     * The constant value used for a black card.
     */
    public static final int BLACK_CARD = 200;
    
    /**
     * Returns the race of the card.
     * @param card The card whose race is requested.
     */
    public static int getCardRace(int card) {
        return (card >> 4) & 0x0F;
    }

    /**
     * Returns the card number.
     * @param card The card whose number is requested.
     */
    public static int getCardNumber(int card) {
        return (card) & 0x0F;
    }
    
    public static int getCardColour(int card){
        int race = getCardRace(card);
        
        if ((race == HEARTS_CARD) || (race == DIAMONDS_CARD)){
            return RED_CARD;
        } else if ((race == CLUBS_CARD) || (race == SPADES_CARD)){
            return BLACK_CARD;
        } else {
            return -1;
        }
    }
    
    public static int getCard(int value, int race){
        return (race << 4) | value;
    }
}
