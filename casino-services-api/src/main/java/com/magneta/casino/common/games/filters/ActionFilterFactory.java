/**
 * 
 */
package com.magneta.casino.common.games.filters;


/**
 * @author anarxia
 *
 */
public interface ActionFilterFactory {

   Class<? extends ActionFilter> getActionFilterClass(String gameTemplate);
}
