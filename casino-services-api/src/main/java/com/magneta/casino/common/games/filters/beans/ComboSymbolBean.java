package com.magneta.casino.common.games.filters.beans;

import java.io.Serializable;

public class ComboSymbolBean implements Serializable{
	private static final long serialVersionUID = 1L;

	private String image;
	private String alt;
	
	public ComboSymbolBean(String image, String alt) {
		this.image = image;
		this.alt = alt;
	}

	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getAlt() {
		return alt;
	}
	
	public void setAlt(String alt) {
		this.alt = alt;
	}
}
