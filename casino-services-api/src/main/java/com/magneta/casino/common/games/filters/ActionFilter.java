package com.magneta.casino.common.games.filters;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.GameConfiguration;

public interface ActionFilter {

    int getGameId();
    void setGameId(int gameId);
    
    void setShowGuards(boolean showGuards);
	void setConfiguration(GameConfiguration config);
	
	List<GameActionBean> filterActions(List<GameActionBean> actions);
}
