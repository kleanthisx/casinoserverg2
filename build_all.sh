#!/bin/sh

set -e

mvn=mvn
mvnopts="-Denv=distribution -Dmaven.test.skip=true -DincludeScope=runtime"

$mvn -f casino-server/pom.xml $mvnopts clean package

