package com.magneta.test.utils.xml;

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XmlUtils {

	public static String marshall(Object o) throws JAXBException{
		JAXBContext context = JAXBContext.newInstance(o.getClass());
        Marshaller marshaller = context.createMarshaller();

        StringWriter sw = new StringWriter();
        marshaller.marshal(o, sw);
        return sw.toString();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T unmarshall(Class<T> klass, File temp) throws JAXBException{
		 T t ; 
		 JAXBContext cntxt = JAXBContext.newInstance(klass);
         Unmarshaller unmarshaller = cntxt.createUnmarshaller();
         t =(T)unmarshaller.unmarshal(temp); 
         return t;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T unmarshall(Class<T> klass, InputStream is) throws JAXBException {
		 T t ; 
		 JAXBContext cntxt = JAXBContext.newInstance(klass);
         Unmarshaller unmarshaller = cntxt.createUnmarshaller();
         t =(T)unmarshaller.unmarshal(is); 
         return t;
	}
}
