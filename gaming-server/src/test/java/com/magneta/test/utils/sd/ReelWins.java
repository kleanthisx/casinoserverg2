package com.magneta.test.utils.sd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"_boolean"})
public class ReelWins {

	@XmlElement(name = "boolean", nillable = true)
	protected List<Boolean> _boolean;

	public List<Boolean> getBoolean() {
		if (_boolean == null) {
			_boolean = new ArrayList<Boolean>();
		}
		return this._boolean;
	}

}