package com.magneta.test.games.poker;


//import static org.easymock.EasyMock.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.RoundOptions;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;
import com.magneta.games.fixedodd.IslandStudHandGuardParams;
import com.magneta.games.fixedodd.IslandStudPokerGuard;
import com.magneta.games.poker.islandstudpoker.IslandStudPokerRound;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { GameAction.class,GameSettings.class })
public class IslandStudPokerRoundTestCase {

	private static final Logger log = LoggerFactory.getLogger(IslandStudPokerRoundTestCase.class);
	
	GameSettings gamesettings;
	RoundOptions options;
	IslandStudPokerRound islandStudPokerRound;
	GameAction dealAction;

	Integer[] playerHand ;
	Integer[] dealerHand ;
	IslandStudHandGuardParams params = new IslandStudHandGuardParams();
	// player wins < 0 = player wins

	private PokerTestUtil poker = PokerTestUtil.getInstance();
	
	@Before
	public void initialise() throws ActionFailedException{
		log.info("initialising");
		gamesettings = mock(GameSettings.class);
		options = new RoundOptions(1, 0.0, 100.0, gamesettings );
		islandStudPokerRound = new IslandStudPokerRound();
		islandStudPokerRound .init(new GameBean(), new com.magneta.casino.games.templates.impl.IslandStudPoker(), new IslandStudPokerConfiguration(), options, 1, 1, new byte[]{'a','1','2'});
		loadParams();
		dealAction = mock(GameAction.class);
		when(gamesettings.get("guard", Boolean.TYPE)).thenReturn(true);
		when(dealAction.createGeneratedAction(null, 0, "GUARD", null, 0.0)).thenReturn(dealAction);
		params.setParentAction(dealAction);
	}

	private void loadParams() {
		params = new IslandStudHandGuardParams();
		params.setGameID(72);
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setOwnerID(this.options.getOwner());
		params.setConfig(new IslandStudPokerConfiguration());
		params.setBetAmount(5);
		params.setJackpotBet(1);
		params.setSettings(options.getGameSettings());
	}

	@Test()
	public void testHighest(){		
		Assert.assertEquals(" strait testing dealer over player ",0, islandStudPokerRound.compareHands(new Integer[]{1,2,3,20,5}, new Integer[]{17,18,19,4,21}));
	}
	// < http://www.texasholdem-poker.com/splitpot1>
	// straits and flushes and royal flushes.
	@Test()
	public void test3Highest(){
		Assert.assertEquals(" strait testing dealer over player ",-6, islandStudPokerRound.compareHands(new Integer[]{1,2,3,20,5}, new Integer[]{18,19,4,21,22}));
	}
	
	@Test()
	public void test3_1_Highest(){
		Assert.assertEquals(" strait testing dealer over player ",0, islandStudPokerRound.compareHands(new Integer[]{2,3,20,5,6}, new Integer[]{18,19,4,21,22}));
	}
	
	@Test()
	public void test3_2_Highest(){
		Assert.assertEquals(" strait testing dealer over player ",1, islandStudPokerRound.compareHands(new Integer[]{3,20,5,6,7}, new Integer[]{18,19,4,21,22}));
	}

	//@Test()
	public void testGenerationtst(){

		Map<String,Integer> wantedresults = new HashMap<String, Integer>();
		initialiseWantedResults(wantedresults);
		boolean done = false;
		for (int z = 0;z < 500000; z++) {
			if(done)
				break;
			// Initialising deck and eanted results.
			Deck cardDeck = new Deck(1, 0);
			cardDeck.shuffleCards(new Random());
			playerHand = new Integer[(5)];
			dealerHand = new Integer[(5)];
			for (int i= 0; i < playerHand.length; i++) {
				playerHand[i] = cardDeck.getNextCard();	
			}

			for (int i= 0; i < dealerHand.length; i++) {
				dealerHand[i] = cardDeck.getNextCard();			
			}

			int playerHandValue = islandStudPokerRound.getWinningCombination(playerHand, new boolean[(7)]);
			int dealerHandValue = islandStudPokerRound.getWinningCombination(dealerHand, new boolean[(7)]);

			boolean fitForTest = false;
			if (2>Math.abs(playerHandValue-dealerHandValue)){
				fitForTest = true;
			}

			int result = islandStudPokerRound.compareHands(dealerHand, playerHand); 

			if(fitForTest && isWanted(wantedresults,playerHandValue,dealerHandValue,result) && IslandStudPokerRound.checkIfDealerQualifies(playerHandValue, dealerHand)){
				String str = Arrays.toString(playerHand)+","+Arrays.toString(dealerHand)+":"+playerHandValue+","+dealerHandValue+","+result;
				poker.writeToFile("testsIsland.txt",str);
			}

			log.info("result = {}", result);
			done = checkDone(wantedresults);
		}
	}

	private boolean checkDone(Map<String, Integer> wantedresults) {
		for (String s : wantedresults.keySet()){
			if (wantedresults.get(s).intValue()<4){
				return false;
			}
		}
		return true;

	}

	private void initialiseWantedResults(Map<String, Integer> wantedresults) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				for (int j2 = -1; j2 < 2; j2++) {
					String key = i+","+j+","+j2;
					wantedresults.put(key, new Integer(0));
				}
			}
		}

	}

	private boolean isWanted(Map<String, Integer> wantedresults, int playerHandValue, int dealerHandValue,int result) {
		Integer res = wantedresults.get(playerHandValue+","+dealerHandValue+","+result);
		if(res==null) 
			return false;
		if (res.intValue()<4){
			res++;
			wantedresults.put(playerHandValue+","+dealerHandValue+","+result,res);
			return true;
		}
		return false;

	}
	@Test
	public void testsFromfile(){

		playerHand = new Integer[(5)];
		dealerHand = new Integer[(5)];
		
		poker.readFromFile(getClass().getResourceAsStream("/"+"testsIsland.txt"));
		poker.loadnextResult(playerHand, dealerHand);
		when(gamesettings.get("guard", Boolean.TYPE)).thenReturn(false);
		for (int i = 0; i < poker.getTestSize(); i++) {
			int result = poker.getresult();
			Assert.assertEquals("result : "+i+" has failed with cards: player "+Arrays.toString(playerHand).toString()+" and dealer "+Arrays.toString(dealerHand).toString() , result ,islandStudPokerRound.compareHands(dealerHand, playerHand));
		}

	}
	
	@Test
	public void testsGuardFromfile(){

		playerHand = new Integer[(5)];
		dealerHand = new Integer[(5)];

		poker.readFromFile(getClass().getResourceAsStream("/testsIsland.txt"));
		when(gamesettings.get("guard", Boolean.TYPE)).thenReturn(true);
		for (int i = 0; i < poker.getTestSize()-1; i++) {
			poker.loadnextResult(playerHand, dealerHand);
			params.setPlayerhand(playerHand);
			params.setDealerhand(dealerHand);
			int result = poker.getresult();
			if(result<0 && islandStudPokerRound.getWinningCombination(playerHand,new boolean[(playerHand.length)])>0 ){
				IslandStudPokerGuard.guardFirstHand(params);
				Assert.assertEquals("result : "+(-i)+" has failed with cards: player "+Arrays.toString(dealerHand).toString()+" and dealer "+Arrays.toString(playerHand ).toString() , -1*result ,islandStudPokerRound.compareHands(dealerHand, playerHand));	
				log.info("pernasss: {}", i);
			}
			log.info("perna: {}", i);
		}

	}

	@After
	public void teardown(){

	}

}
