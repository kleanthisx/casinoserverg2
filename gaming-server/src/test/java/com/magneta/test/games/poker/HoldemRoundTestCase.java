package com.magneta.test.games.poker;


//import static org.easymock.EasyMock.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.RoundOptions;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.fixedodd.HoldEmHandGuardParams;
import com.magneta.games.fixedodd.HoldEmPokerGuard;
import com.magneta.games.poker.texasholdem.TexasHoldEmRound;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { GameAction.class,GameSettings.class })
public class HoldemRoundTestCase {

	private static final Logger log = LoggerFactory.getLogger(HoldemRoundTestCase.class);
	
	GameSettings gamesettings;
	RoundOptions options;
	TexasHoldEmRound texasHoldEmRound;
	GameAction dealAction;

	Integer[] playerHand ;
	Integer[] dealerHand ;
	Integer[] commonHand ;
	HoldEmHandGuardParams params = new HoldEmHandGuardParams();
	// player wins < 0 = player wins

	private PokerTestUtil poker = PokerTestUtil.getInstance();
	
	@Before
	public void initialise() throws ActionFailedException{
		log.info("initialising");
		gamesettings = mock(GameSettings.class);
		options = new RoundOptions(1, 0.0, 100.0, gamesettings );
		GameBean game = new GameBean();
		game.setGameId(1);
		game.setName("base");
		texasHoldEmRound = new TexasHoldEmRound();
		texasHoldEmRound.init(game, new com.magneta.casino.games.templates.impl.TexasHoldEm(), new TexasHoldemConfiguration(), options, 1, 1, new byte[]{'a','1','2'});
		
		loadParams();
		dealAction = mock(GameAction.class);
		when(gamesettings.get("guard", Boolean.TYPE)).thenReturn(true);
		when(dealAction.createGeneratedAction(null, 0, "GUARD", null, 0.0)).thenReturn(dealAction);
		params.setParentAction(dealAction);

	}

	private void loadParams() {
		params = new HoldEmHandGuardParams();
		params.setGameID(72);
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setCommonhand(commonHand);
		params.setOwnerID(this.options.getOwner());
		params.setConfig(new TexasHoldemConfiguration());
		params.setBetAmount(5);
		params.setSidebet(1);
		params.setSettings(options.getGameSettings());
	}

	//	@Test()
	public void testGenerationtst(){

		Map<String,Integer> wantedresults = new HashMap<String, Integer>();
		initialiseWantedResults(wantedresults);
		boolean done = false;
		for (int z = 0;z < 500000; z++) {
			if(done)
				break;
			// Initialising deck and eanted results.
			Deck cardDeck = new Deck(1, 0);
			cardDeck.shuffleCards(new Random());
			playerHand = new Integer[(2)];
			dealerHand = new Integer[(2)];
			commonHand = new Integer[(5)];
			for (int i= 0; i < playerHand.length; i++) {
				playerHand[i] = cardDeck.getNextCard();	
			}

			for (int i= 0; i < dealerHand.length; i++) {
				dealerHand[i] = cardDeck.getNextCard();			
			}

			for (int i= 0; i < commonHand.length; i++) {
				commonHand[i]= cardDeck.getNextCard();
			}

			int playerHandValue = texasHoldEmRound.evaluateSeatFullHand(playerHand, commonHand, new boolean[(7)]);
			int dealerHandValue = texasHoldEmRound.evaluateSeatFullHand(dealerHand, commonHand, new boolean[(7)]);

			boolean fitForTest = false;
			if (9>Math.abs(playerHandValue-dealerHandValue)){
				fitForTest = true;
			}

			Integer[] dealerFullHand = new Integer[7];
			Integer[] playerFullHand = new Integer[7];

			for(int i=0;i<5;i++){
				dealerFullHand[i] = commonHand[i];
				playerFullHand[i] = commonHand[i];
			}

			for(int i=0;i<2;i++){
				dealerFullHand[i+5] = dealerHand[i];
				playerFullHand[i+5] = playerHand[i];
			}

			int result = texasHoldEmRound.compareHands(dealerFullHand, playerFullHand, new StringBuilder()); 

			if(fitForTest && isWanted(wantedresults,playerHandValue,dealerHandValue,result)){
				String str = Arrays.toString(playerHand)+","+Arrays.toString(dealerHand)+","+Arrays.toString(commonHand)+":"+playerHandValue+","+dealerHandValue+","+result;
				poker.writeToFile("tests.txt",str);
			}

			log.info("result = {}", result);
			done = checkDone(wantedresults);
		}
	}

	private boolean checkDone(Map<String, Integer> wantedresults) {
		for (String s : wantedresults.keySet()){
			if (wantedresults.get(s).intValue()<4){
				return false;
			}
		}
		return true;

	}

	private void initialiseWantedResults(Map<String, Integer> wantedresults) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				for (int j2 = -1; j2 < 2; j2++) {
					String key = i+","+j+","+j2;
					wantedresults.put(key, new Integer(0));
				}
			}
		}

	}

	private boolean isWanted(Map<String, Integer> wantedresults, int playerHandValue, int dealerHandValue,int result) {
		Integer res = wantedresults.get(playerHandValue+","+dealerHandValue+","+result);
		if(res==null) 
			return false;
		if (res.intValue()<4){
			res++;
			wantedresults.put(playerHandValue+","+dealerHandValue+","+result,res);
			return true;
		}
		return false;

	}

	@Test()
	public void testfile(){

		playerHand = new Integer[(7)];
		dealerHand = new Integer[(7)];
		commonHand = new Integer[(5)];

		poker.readFromFile(getClass().getResourceAsStream("/"+"tests.txt"));
		poker.loadnextResult(playerHand, dealerHand,commonHand);
		for (int i = 0; i < commonHand.length; i++) {
			playerHand[i+2] = commonHand[i];
			dealerHand[i+2] = commonHand[i];
		}
		for (int i = 0; i < poker.getTestSize(); i++) {
			int result = poker.getresult();
			Assert.assertEquals("result : "+i+" has failed with cards: player "+Arrays.toString(playerHand).toString()+" and dealer "+Arrays.toString(dealerHand).toString() , result ,texasHoldEmRound.compareHands(dealerHand, playerHand, new StringBuilder()));
			log.debug("perna : {}", i);
		}
	}

	@Test()
	public void testHighest(){		
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{1,2,3,20,5,10,11}, new Integer[]{1,2,3,4,5, 21,22}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{1,2,3,20,5}, new Integer[]{17,18,19,4,21,22}, new StringBuilder()));
	}
	// < http://www.texasholdem-poker.com/splitpot1>
	// straits and flushes and royal flushes.

	@Test()
	public void test2Highest(){		
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{6, 39, 5, 45, 26, 51, 36 }, new Integer[]{17, 50, 5, 45, 26, 51, 36 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",0, texasHoldEmRound.compareHands(new Integer[]{6, 39, 5, 45, 26, 51, 36}, new Integer[]{6, 39, 5, 45, 26, 51, 36}, new StringBuilder()));
	}

	@Test()
	public void test3Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{4, 57, 8, 39, 21, 58, 38}, new Integer[]{25, 27, 8 ,39, 21, 58, 38}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{4, 57, 8, 39, 21, 58, 38}, new Integer[]{25, 27, 8 ,39, 21, 58, 38}, new StringBuilder()));
	}

	@Test()
	public void test4Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{29, 58, 55, 54, 57, 45, 56}, new Integer[]{49, 53, 55, 54, 57, 45, 56}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",+1, texasHoldEmRound.compareHands(new Integer[]{29, 58, 55, 54, 57, 45, 56}, new Integer[]{49, 53, 55, 54, 57, 45, 56}, new StringBuilder()));
	}

	@Test()
	public void test5Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{33, 34, 43, 35, 2, 41, 20}, new Integer[]{45, 44, 43, 35, 2, 41, 20}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",+1, texasHoldEmRound.compareHands(new Integer[]{33, 34, 43, 35, 2, 41, 20}, new Integer[]{45, 44, 43, 35, 2, 41, 20}, new StringBuilder()));
	}

	@Test()
	public void test6Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{20, 40, 45, 43, 51, 36, 41}, new Integer[]{49, 39, 45, 43, 51, 36, 41}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",+1, texasHoldEmRound.compareHands(new Integer[]{20, 40, 45, 43, 51, 36, 41   }, new Integer[]{49, 39, 45, 43, 51, 36, 41}, new StringBuilder()));
	}

	@Test()
	public void test7Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{9, 8, 43, 10, 2, 13, 4}, new Integer[]{1, 58, 43, 10, 2, 13, 4}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{9, 8, 43, 10, 2, 13, 4}, new Integer[]{1, 58, 43, 10, 2, 13, 4}, new StringBuilder()));
	}

	@Test()
	public void test17(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 34, 5, 22, 52, 3}, new Integer[]{54, 17, 34, 5, 22, 52, 3}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",0, texasHoldEmRound.compareHands(new Integer[]{53, 49, 34, 5, 22, 52, 3}, new Integer[]{54, 17, 34, 5, 22, 52, 3}, new StringBuilder()));
	}

	@Test()
	public void test8Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{9, 8, 43, 10, 2, 13, 4 }, new Integer[]{1, 58, 43, 10, 2, 13, 4 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{9, 8, 43, 10, 2, 13, 4 }, new Integer[]{1, 58, 43, 10, 2, 13, 4}, new StringBuilder()));
	}

	@Test()
	public void fok1(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{49, 1, 33, 17, 21, 13, 61 }, new Integer[]{45, 29, 33, 17, 21, 13, 61 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",1, texasHoldEmRound.compareHands(new Integer[]{49, 1, 33, 17, 21, 13, 61 }, new Integer[]{45, 29, 33, 17, 21, 13, 61  }, new StringBuilder()));
	}

	@Test()
	public void fok2(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{49, 21, 33, 17, 1, 13, 61 }, new Integer[]{45, 29, 33, 17, 1 ,13, 61  }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",1, texasHoldEmRound.compareHands(new Integer[]{49, 21, 33, 17, 1, 13, 61 }, new Integer[]{45, 29, 33, 17, 1, 13, 61 }, new StringBuilder()));
	}

	@Test()
	public void fh1(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 33, 5, 22, 52, 1  }, new Integer[]{54, 17, 33, 5, 22, 52, 1  }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 49, 33, 5, 22, 52, 1  }, new Integer[]{54, 17, 33, 5, 22, 52, 1 }, new StringBuilder()));
	}

	@Test()
	public void fh2(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 38, 5, 22, 21, 1   }, new Integer[]{54, 17, 38, 5, 22, 21, 1 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 49, 38, 5, 22, 21, 1     }, new Integer[]{54, 17, 38, 5, 22, 21, 1}, new StringBuilder()));
	}

	@Test()
	public void fh3(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 38, 5, 22, 21, 9   }, new Integer[]{54, 17, 38, 5, 22, 21, 9 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 49, 38, 5, 22, 21, 9}, new Integer[]{54, 17, 38, 5, 22, 21, 9}, new StringBuilder()));
	}

	@Test()
	public void tfk1(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 38, 5, 22, 21, 9   }, new Integer[]{54, 17, 38, 5, 22, 21, 9 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 49, 38, 5, 22, 21, 9}, new Integer[]{54, 17, 38, 5, 22, 21, 9}, new StringBuilder()));
	}

	public void tfk2(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 2, 37, 20, 21, 9, 49}, new Integer[]{54, 22, 37, 20, 21, 9, 49}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 2, 37, 20, 21, 9, 49}, new Integer[]{54, 22, 37, 20, 21, 9, 49}, new StringBuilder()));
	}

	@Test()
	public void tfk3(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 5, 38, 20, 21, 9, 49}, new Integer[]{54, 22, 38, 20, 21, 9, 49}, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 5, 38, 20, 21, 9, 49}, new Integer[]{54, 22, 38, 20, 21, 9, 49}, new StringBuilder()));
	}

	// two pairs player has sixes and dealer has 5s 
	@Test()
	public void test_P1_Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 42, 5, 22, 59, 12  }, new Integer[]{54, 17, 42, 5, 22, 59, 12 }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-4, texasHoldEmRound.compareHands(new Integer[]{53, 49, 42, 5, 22, 59, 12  }, new Integer[]{1, 58, 43, 10, 2, 13, 4}, new StringBuilder()));
	}

	// two pairs player has sixes and dealer has 5s and both have  
	@Test()
	public void test_2P1_Highest(){
		log.info(""+texasHoldEmRound.compareHands(new Integer[]{53, 49, 42, 5, 22, 59, 1 }, new Integer[]{54, 17, 42, 5,22, 59, 1  }, new StringBuilder()));
		Assert.assertEquals(" strait testing dealer over player ",-1, texasHoldEmRound.compareHands(new Integer[]{53, 49, 42, 5, 22, 59, 1  }, new Integer[]{54, 17, 42, 5,22, 59, 1 }, new StringBuilder()));
	}

	// two pairs player has sixes and dealer has 5s and both have  
	@Test()
	public void test_side1(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AA_AA,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{33,1 }, new Integer[]{49,17})); 
	}

	@Test()
	public void test_side2(){
		Assert.assertEquals(" straight testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AA,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{17,1 }, new Integer[]{49,2})); 
	}

	@Test()
	public void test_side3(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AK_SUITED,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{33,45 }, new Integer[]{49,2})); 
	}

	@Test()
	public void test_side4(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AK_UNSUITED,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{1,45 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side5(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AQ_AJ_SUITED,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{33,44 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side6(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AQ_AJ_SUITED,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{33,43 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side7(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AQ_AJ_UNSUITED,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{1,44 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side8(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.AQ_AJ_UNSUITED,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{1,43 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side9(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.KK_QQ_JJ,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{13,45 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side10(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.KK_QQ_JJ,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{12,44 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side11(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.KK_QQ_JJ,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{11,43 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side12(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.KK_QQ_JJ,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{29,45 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side13(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.KK_QQ_JJ,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{28,44 }, new Integer[]{49,2})); 
	}
	
	@Test()
	public void test_side14(){
		Assert.assertEquals(" strait testing dealer over player ",TexasHoldemConfiguration.TexasPokerSideGame.KK_QQ_JJ,TexasHoldEmRound.checkForSidegameCombination(new Integer[]{27,43 }, new Integer[]{49,2})); 
	}

	@Test
	@PrepareForTest( { GameAction.class })
	public void test_guard1(){
		playerHand = new Integer[]{17, 33};
		dealerHand = new Integer[]{1, 12};
		commonHand = new Integer[]{37, 38 , 27, 39, 52};
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setCommonhand(commonHand);
		HoldEmPokerGuard.guardFirstHand(params);
		Assert.assertEquals(" strait testing dealer over player ",new Integer(1),playerHand[0]);
	}
	
	@Test
	@PrepareForTest( { GameAction.class })
	public void test_guard2(){
		playerHand = new Integer[]{1, 12};
		dealerHand = new Integer[]{17, 33};
		commonHand = new Integer[]{37, 38 , 27, 39, 52};
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setCommonhand(commonHand);
		params.setBetAmount(5);
		params.setSidebet(10);
		HoldEmPokerGuard.guardFirstHand(params);
		Assert.assertEquals(" strait testing dealer over player ",new Integer(1),playerHand[0]);
	}
	
	@Test
	@PrepareForTest( { GameAction.class })
	public void test_guard3(){
		playerHand = new Integer[]{1, 12};
		dealerHand = new Integer[]{17, 33};
		commonHand = new Integer[]{37, 38 , 27, 39, 52};
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setCommonhand(commonHand);
		params.setBetAmount(25);
		params.setSidebet(10);
		HoldEmPokerGuard.guardFirstHand(params);
		Assert.assertEquals(" strait testing dealer over player ",new Integer(1),playerHand[0]);
	}


	@After
	public void teardown(){

	}

}