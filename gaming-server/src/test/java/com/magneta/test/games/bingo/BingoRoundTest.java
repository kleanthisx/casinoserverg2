package com.magneta.test.games.bingo;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.magneta.games.bingo.BingoCard;
import com.magneta.games.bingo.BingoCardFactory;
import com.magneta.games.configuration.bingo.BingoConfiguration;

public class BingoRoundTest {

	BingoConfiguration config;
	int[] picks;
	boolean[] hitNumbers;
	double winAmount;
	int hitCounter;


	@Test 
	public void testBingoCardFactory1(){
		List<BingoCard> result = BingoCardFactory.getBingoSetOfSix(new Random(0001));
		for(BingoCard card:result){
			System.out.println(""+card.toString());
		}
	}

	@Test 
	public void testBingoCardFactory2(){
		List<BingoCard> result = BingoCardFactory.getBingoSetOfSix(new Random(0001));
		for(BingoCard card:result){
			//			System.out.println(""+card.toString());
			BingoCard tmp = BingoCard.parse(card.toString());
			//			System.out.println(""+tmp.toString());
			Assert.assertArrayEquals("asdasd", card.toString().getBytes(), tmp.toString().getBytes());
		}
	}

	@Test 
	public void testBingoCardFactory3(){
		List<BingoCard> result = BingoCardFactory.getBingoSetOfSix(new Random(0001));
		int picks[] = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,87}; 
		Assert.assertEquals("asdasd",true,result.get(0).hasWon(picks));
		
		picks = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,3,3,3,3,3,3,3,3,3}; 
		Assert.assertEquals("asdasd",false,result.get(0).hasWon(picks));

		picks = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,3,3,3,3,3,3,3,3,3,87}; 
		Assert.assertEquals("asdasd",true,result.get(0).hasWon(picks));
		
		picks = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,87}; 
		Assert.assertEquals("asdasd",15,result.get(0).numOfBallsToWin(picks));
		
		picks = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,3,3,3,3,3,3,3,3,3}; 
		Assert.assertEquals("asdasd",-1,result.get(0).numOfBallsToWin(picks));

		picks = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,3,3,3,3,3,3,3,3,3,87}; 
		Assert.assertEquals("asdasd",24,result.get(0).numOfBallsToWin(picks));
		
		picks = new int[]{15,21,61,71,84,6,31,47,50,72,25,35,54,62,3,3,3,3,3,3,3,3,87,3}; 
		Assert.assertEquals("asdasd",23,result.get(0).numOfBallsToWin(picks));

	}
	
	@Test
	public void testConfig(){
		config = new BingoConfiguration();
		Assert.assertEquals("asdasd",false,config.isJackpotCombination(16, 15));
		Assert.assertEquals("asdasd",true,config.isJackpotCombination(15, 15));
		Assert.assertEquals(1.0,config.getJackpotPercentage(15, 15),0.0);
		
	}
	
	@Test
	public void testSort(){
		int tst[] = new int[]{1,2,3,4,5,9,8,6,7};
		int tst2[] = new int[]{1,2,3,4,5,6,7,8,9};
		Arrays.sort(tst);
		Assert.assertArrayEquals(tst2, tst);
		
	}
	
	@Test
	public void testShufleNumbers(){
		System.out.println("Testing ShufleNumbers()");
		Random random = new Random(0001);
		int[][] result = BingoCardFactory.getShufledNumbers(BingoCardFactory.CARD_NUMBERS, random);
		for (int i = 0; i < result.length; i++) {
			System.out.println(Arrays.toString(result[i]));
		}
		Assert.assertArrayEquals(result[0], new int[]{6, 9, 3, 8, 5, 4, 2, 1, 7});
	}

}
