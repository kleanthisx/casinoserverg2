package com.magneta.test.games.bingo;

import java.util.List;
import java.util.Random;

import org.junit.Test;

import com.magneta.games.bingo.BingoCard;
import com.magneta.games.bingo.BingoCardFactory;
import com.magneta.games.configuration.bingo.BingoConfiguration;

public class BingoRoundTest2 {

	BingoConfiguration config;
	int[] picks;
	boolean[] hitNumbers;
	double winAmount;
	int hitCounter;

	@Test
	public void testRTP(){

		// get card 
		List<BingoCard> result = BingoCardFactory.getBingoSetOfSix(new Random(0001));
		// BingoCard card =  result.get(0);
		// setup balls
		picks = new int[90];
		for (int i = 0; i < picks.length; i++) {
			picks[i]=i+1;
		}

		// sertup random 
		Random random = new Random();
		// initialise configuration
		config = new BingoConfiguration();

		int rounds = 10000;
		int test = 3;
		int numOfCards=6;


		for (int t = 0; t < test; t++) {
			int averageBalls = 0;
			double sumofWins = 0.0;
			for (int i = 0; i < rounds; i++) {
				// shufling
				shuffleBalls(random);
				double maxWin = 0.0;
				int minballs = 0;
				for (int j = 0; j < numOfCards; j++) {
					BingoCard card = result.get(j);
					int numberOfBalls = card.numOfBallsToWin(picks);
					double payout =  config.getMultiplier(numberOfBalls, 15);
					if(payout>maxWin){
						maxWin = payout;
						minballs = numberOfBalls;
					}
				}
				averageBalls+=minballs;
				sumofWins += maxWin;
			} 

			System.out.println("Test "+t+" report : Run "+rounds+" rounds and the result is "+sumofWins/(rounds*numOfCards)+" RTP. average balls : "+averageBalls/rounds  );
			//			Assert.assertEquals("failed",sumofWins/rounds,0.7878,0.05);
		}
	}

	@Test
	public void testLineRTP(){

		// get card 
		List<BingoCard> result = BingoCardFactory.getBingoSetOfSix(new Random(0001));
		// setup balls
		picks = new int[90];
		for (int i = 0; i < picks.length; i++) {
			picks[i]=i+1;
		}

		// sertup random 
		Random random = new Random();
		// initialise configuration
		config = new BingoConfiguration();

		int rounds = 100000;
		int test = 3;
		int numerOfCards =6;
		

		for (int t = 0; t < test; t++) {
			int averageBallsNeeded = 0;
			double sumofWins = 0.0;
			for (int i = 0; i < rounds; i++) {
				//shufling
				shuffleBalls(random);
				double maxpayout =0.0;
				int ballsNeeded = 0;
				for (int c = 0; c < numerOfCards; c++) {
					BingoCard card = result.get(c);
					for(int j = 0; j < 3; j++){
						int numberOfBalls = card.numOfBallsToWinOneLine(picks, j);
						double payout  =  config.getMultiplier(numberOfBalls, 5);
						if(payout>maxpayout){
							maxpayout=payout;
							ballsNeeded = numberOfBalls;
						}
					}
				}
				sumofWins += maxpayout;
				averageBallsNeeded+= ballsNeeded;
			} 

			System.out.println("Test "+t+" report : Run "+rounds+" rounds and the result is "+sumofWins/(rounds*numerOfCards)+" RTP. Average balls for line :" + averageBallsNeeded/rounds );
			//			Assert.assertEquals("failed",sumofWins/rounds,0.7878,0.05);
		}
	}

	public void shuffleBalls(Random sr) {

		long[] ballRank = new long[picks.length];

		for (int rank=0; rank < ballRank.length; rank++) {
			ballRank[rank] = sr.nextLong();
		}

		for (int i=0; i < picks.length; i++) {
			for (int j=i+1; j < picks.length; j++) {
				if (ballRank[i] < ballRank[j]) {
					long tmp = ballRank[i];
					ballRank[i] = ballRank[j];
					ballRank[j] = tmp;

					int tmp_ball = picks[i];
					picks[i] = picks[j];
					picks[j] = tmp_ball;
				}
			}
		}

	}

}
