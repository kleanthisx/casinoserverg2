package com.magneta.test.games;

import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.games.GamePlayer;

/**
 * Utility class for tests.
 * @author anarxia
 *
 */
public class Util {

	/**
	 * Creates a player suitable for tests.
	 * 
	 * @return
	 */
    public static GamePlayer createPlayer() {
    	UserAuthenticationBean authBean = new UserAuthenticationBean();
    	authBean.setUserType(UserTypeEnum.NORMAL_USER);
    	authBean.setUserId(-1l);
    	return new GamePlayer(authBean, null, null);
    }
}
