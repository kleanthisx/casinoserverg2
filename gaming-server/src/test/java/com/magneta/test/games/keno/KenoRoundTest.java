package com.magneta.test.games.keno;

import org.junit.Assert;
import org.junit.Test;

import com.magneta.games.configuration.keno.KenoConfiguration;
import com.magneta.games.keno.KenoBet;

public class KenoRoundTest {

	KenoConfiguration config;
	KenoBet bet;
	int[] picks;
	boolean[] hitNumbers;
	double winAmount;
	int hitCounter;
	
	@Test
	public void kenoBetTest(){
		testBet(new int[]{1}, 3.0,true);
		
		testBet(new int[]{1,31}, 1.0,true);
		testBet(new int[]{1,2}, 8.0,true);
		
		testBet(new int[]{31,32,33}, 0.0,true);
		testBet(new int[]{1,32,33}, 0.0,true);
		testBet(new int[]{1,2,31}, 3.0,true);
		testBet(new int[]{1,2,3} , 30.0,true);
		
		testBet(new int[]{31,32,33,34}, 0.0,true);
		testBet(new int[]{1,32,33,34}, 0.0,true);
		testBet(new int[]{1,2,33,34}, 0.0,true);
		testBet(new int[]{1,2,3,31}, 5.5,true);
		testBet(new int[]{1,2,4,3} , 130.0,true);

		testBet(new int[]{31,32,33,34,35}, 0.0,true);
		testBet(new int[]{1,32,33,34,35}, 0.0,true);
		testBet(new int[]{1,2,33,34,35}, 0.0,true);
		testBet(new int[]{1,2,3,31,32}, 2.5,true);
		testBet(new int[]{1,2,3,4,32}, 25.0,true);
		testBet(new int[]{1,2,3,4,5} , 500.0,true);
		
		testBet(new int[]{31,32,33,34,35,36}, 0.0,true);
		testBet(new int[]{1,32,33,34,35,36}, 0.0,true);
		testBet(new int[]{1,2,33,34,35,36}, 0.0,true);
		testBet(new int[]{1,2,3,31,32,33}, 1.5,true);
		testBet(new int[]{1,2,3,4,32,33}, 8.0,true);
		testBet(new int[]{1,2,3,4,5,32} , 60.0,true);
		testBet(new int[]{1,2,3,4,5,6} , 180.0,true);
		
		testBet(new int[]{31,32,33,34,35,36,37}, 0.0,true);
		testBet(new int[]{1,2,3,31,32,33,34}, 1.0,true);
		testBet(new int[]{1,2,3,4,32,33,34}, 3.5,true);
		testBet(new int[]{1,2,3,4,5,32,33} , 30.0,true);
		testBet(new int[]{1,2,3,4,5,6,33} , 120.0,true);
		testBet(new int[]{1,2,3,4,5,6,7} , 6000.0,true);
		
		testBet(new int[]{31,32,33,34,35,36,37,38}, 0.0,true);
		testBet(new int[]{1,2,3,4,31,32,33,38} , 2.5,true);
		testBet(new int[]{1,2,3,4,5,32,33,38} , 12.0,true);
		testBet(new int[]{1,2,3,4,5,6,33,38} , 60.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,38} , 1500.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,8} , 8000.0,true);
		
		testBet(new int[]{31,32,33,34,35,36,37,38,39}, 0.0,true);
		testBet(new int[]{1,32,33,34,35,36,37,38,39}, 0.0,true);
		testBet(new int[]{1,2,33,34,35,36,37,38,39}, 0.0,true);
		testBet(new int[]{1,2,3,34,35,36,37,38,39}, 0.0,true);
		testBet(new int[]{1,2,3,4,35,36,37,38,39}, 0.0,true);
		testBet(new int[]{1,2,3,4,5,32,33,38,39} , 6.0,true);
		testBet(new int[]{1,2,3,4,5,6,33,38,39} , 30.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,38,39} , 300.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,8,39} , 6000.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,8,9} , 10000.0,true);
		
		testBet(new int[]{31,32,33,34,35,36,37,38,39,40}, 2.5,true);
		testBet(new int[]{1,32,33,34,35,36,37,38,39,40}, 0.0,true);
		testBet(new int[]{1,2,33,34,35,36,37,38,39,40}, 0.0,true);
		testBet(new int[]{1,2,3,34,35,36,37,38,39,40}, 0.0,true);
		testBet(new int[]{1,2,3,4,35,36,37,38,39,40}, 0.0,true);
		testBet(new int[]{1,2,3,4,5,32,33,38,39,40} , 2.5,true);
		testBet(new int[]{1,2,3,4,5,6,33,38,39,40} , 25.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,38,39,40} , 100.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,8,39,40} , 750.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,8,9,40} , 8000.0,true);
		testBet(new int[]{1,2,3,4,5,6,7,8,9,10} , 10000.0,true);
	}

	private void testBet(int[] bet_p,double expected_pay,boolean expected) {
		config = new KenoConfiguration();
		picks = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
		winAmount =0.0;
		hitCounter = 0;
		bet = new KenoBet(1.0,bet_p);
		hitNumbers = bet.hasWon(picks);
		countHits();
		double multiplier = config.getMultiplier(bet.getNumbersToHit().length,hitCounter);
		winAmount =  multiplier *1.0;
		checkAndPrintResults(bet, hitCounter, winAmount, expected_pay,expected);
	}

	private void countHits() {
		for (int i = 0; i < hitNumbers.length; i++) {
			if(hitNumbers[i]==true)
				hitCounter++;
		}
	}

	private void checkAndPrintResults(KenoBet bet, int hitCounter,
			double winAmount, double expectedAmount,boolean expected) {
		if(expected)
		Assert.assertEquals( expectedAmount, winAmount,0.0);
		else
			Assert.assertNotSame(expectedAmount, winAmount);
	}
}
