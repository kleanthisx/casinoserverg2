package com.magneta.test.games.poker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PokerTestUtil {
	
	private static final Logger log = LoggerFactory.getLogger(PokerTestUtil.class);
	
	private PokerTestUtil() {
		
	}
	
	private final static PokerTestUtil instance = new PokerTestUtil();
	
	private Integer counter = Integer.valueOf(0);
	
	private final List<String> reslts = new ArrayList<String>();

	public void loadnextResult(Integer[] playerHand, Integer[] dealerHand, Integer[] commonHand){
		
		String[] ret = reslts.get(counter).split(":");
		String cards = ret[0];
		String[] cards2 = cards.split(",");
		
		for (int i = 0; i < 2; i++) {
			playerHand[i]=Integer.parseInt(cards2[i].trim());
		}
		
		for (int i = 0; i < 2; i++) {
			dealerHand[i]=Integer.parseInt(cards2[i+2].trim());
		}
		
		for (int i = 0; i < 5; i++) {
			commonHand[i]=Integer.parseInt(cards2[i+4].trim());
		}
		incrementCounter();
	}

	private void incrementCounter() {
		++counter;
		System.out.println("Incrementing counter : " + String.valueOf(counter));
	}

	public void loadnextResult(Integer[] playerHand, Integer[] dealerHand){
		
		String[] ret = reslts.get(counter).split(":");
		String cards = ret[0];
		String[] cards2 = cards.split(",");
		
		for (int i = 0; i < 5; i++) {
			playerHand[i]=Integer.parseInt(cards2[i].trim());
		}
		
		for (int i = 0; i < 5; i++) {
			dealerHand[i]=Integer.parseInt(cards2[i+5].trim());
		}
		incrementCounter();
	}

	public List<String> readFromFile(InputStream is) {
		reslts.clear();
		List<String> result = new ArrayList<String>();
		try{
			// Open the file that is the first 
			// command line parameter
//			FileInputStream fstream = new FileInputStream();
			// Get the object of DataInputStream
			InputStreamReader in = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(in);
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null)   {
				// Print the content on the console
				reslts.add(strLine);
				result.add(strLine);
				System.out.println (strLine);
			}
			//Close the input stream
			in.close();
		}catch (Exception e){
			//Catch exception if any
			throw new RuntimeException("Eror reading file ", e);
		}
		return result;
	}

	public void writeToFile(String fileName, String text) {
		BufferedWriter output;
		try {
			createFile(fileName);
			FileWriter fw = new FileWriter(fileName,true);
			output = new BufferedWriter(fw);
			output.append(text);
			output.newLine();
			output.close();
		} catch (IOException e) {
			log.error("could not write",e);
		}

	}

	public void createFile(String string) {

		try {
			File file = new File(string);
			// Create file if it does not exist
			boolean success = file.createNewFile();
			log.info(file.getAbsolutePath());
			if (success) {
				log.info("success");
			} else {
				log.info("exists");
			}
		} catch (IOException e) {
			log.error("things wend very wrong",e);
		}
	}

	public static PokerTestUtil getInstance() {
		return instance;
	}

	public int getresult() {
		System.out.println("Counter is : " + counter);
		String[] ret = reslts.get(counter-1).split(":");
		String results = ret[1];
		String[] res = results.split(",");
		return Integer.parseInt(res[res.length-1]);
	}
	
	public int getTestSize(){
		return reslts.size();
	}
}
