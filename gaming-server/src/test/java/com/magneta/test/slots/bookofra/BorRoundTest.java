package com.magneta.test.slots.bookofra;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameSettings;
import com.magneta.games.RoundOptions;
import com.magneta.games.configuration.slots.BookOfRaConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.bookofra.BookOfRaRound;
import com.magneta.test.games.Util;
import com.magneta.test.utils.sd.ObjectFactory;
import com.magneta.test.utils.sd.ServerPlayLog;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Reelstops;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Wins;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Wins.ModelWin;
import com.magneta.test.utils.xml.XmlUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { GameAction.class,GameSettings.class })
public class BorRoundTest {

	private static final Logger log = LoggerFactory.getLogger(BorRoundTest.class);
	
	GameSettings gamesettings;
	RoundOptions options;
	BookOfRaRound bookOfRaRound;
	GameAction action;
	ServerPlayLog playLog;
	GamePlayer player;
	BookOfRaConfiguration configuration;

	@Before
	public void initialise() throws ActionFailedException{
		log.info("initialising");
		gamesettings = mock(GameSettings.class);
		options = new RoundOptions(1, 0.0, 100.0, gamesettings );
		bookOfRaRound = new BookOfRaRound();
		GameBean game = new GameBean();
		GameTemplateInfo gameTemplate = new com.magneta.casino.games.templates.impl.BookOfRa();
		configuration = new BookOfRaConfiguration();
		bookOfRaRound.init(game , gameTemplate, configuration, options, 1, 1, new byte[]{'a','1','2'});

		action = mock(GameAction.class);
		player = Util.createPlayer();
		when(action.getPlayer()).thenReturn(player);
		when(action.createGeneratedAction(any(GamePlayer.class), anyInt(), anyString(), anyString(), anyDouble())).thenReturn(action);
	}

	@Test
	public void test1(){
		for (int i = 1; i < 5; i++) {
			
			String filename = "BookOfRaReelgame"+i+".xml.gz";
			log.info("BEGINNING FILE "+i);
			loadGzippedParams(getClass().getResourceAsStream("/"+filename));
			List<EntireGameOutcomes> entireGameOutcomes = playLog.getEntireGameOutcomes();
			log.info(entireGameOutcomes.size()+"");
			EntireGameOutcomes first = entireGameOutcomes.get(0);
			List<EntireGameOutcome> rounds = first.getEntireGameOutcome();
			int rountcounter = 1;

			// for evey round
			for (EntireGameOutcome entireGameOutcome : rounds) {
				log.info("BEGINNING ROUND "+(rountcounter));
				List<Reelstops> reelstops = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getReelstops();
				List<Wins> wins = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getWins();
				// get the reels
				int[] reels = getreels(reelstops);
				log.info("reels = "+Arrays.toString(reels));
				int[][] reelset = configuration.getReels(i-1);
				for (int z = 0; z < reels.length; z++) {
//					Log.info("reel length = "+reelset[z].length);
					reels[z] = (reels[z]+1+reelset[z].length)%reelset[z].length;
				}
				log.info("new reels = "+Arrays.toString(reels));
				List<ModelWin> gwins = wins.get(0).getModelWin();
				log.info(gwins.size()+"");
				Double winsum = 0.0;
				Double[] actwinsum = new Double[]{0.0};
				List<Integer> viewCombos = new ArrayList<Integer>();
				int scatterPaysCounter = 0;
				if(rountcounter==200){
					log.info("auto einai gia na kanoume track pio round prepei na kanoume debug");
				}
				try {

					int specialSumbol = -1;
					viewCombos = bookOfRaRound.evaluatePosition(action, i-1, 0.0, reels, 1, 10, specialSumbol  -1, actwinsum, true);
				} catch (ActionFailedException e) {
					e.printStackTrace();
					log.error("Something terrible happened.", e);
					throw new RuntimeException("Epic Fail", e);
				}


				SlotConfiguration config= new BookOfRaConfiguration();
				//				List<Integer> viewCombos; //= CreepyCrawlersRound.getReelViewWinCombinations(config, reelview,i);
				List<String> scatterPays = new ArrayList<String>();
				// sum the winnings of any
				for (ModelWin modelWin : gwins) {
					String pay =modelWin.getModelPrize().get(0).getValue();
					if (pay==null){
						if(modelWin.getCombination().toLowerCase().contains("book")){
							scatterPays.add(modelWin.getCombination().toLowerCase());
							continue;
						}
					}

					//String linestr = modelWin.getPaylineNames().get(0).getPaylineName();
					if (pay==null){
						log.info("here");
					}
					double wn = Double.parseDouble(pay);
					winsum = winsum + wn;
					//Mockito.verify(koa_action, VerificationModeFactory.atLeastOnce()).createGeneratedAction(player, 1, "WIN", linestr.subSequence(4,linestr.length()).toString(), wn);
				}
				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and wins : "+ winsum, winsum ,actwinsum[0]);
				if(winsum>0.0){
					log.info("kerdise tosa " + winsum);
				}

				// checking bonus pays
				scatterPaysCounter = scatterPays.size();
				for (Integer combo : viewCombos) {
					int symbol = (Integer)config.getWinCombo(combo)[0];
					if(-symbol==BookOfRaConfiguration.FREE_SPIN_SCATTER_WILD){
						int index = findIndexOf("book",scatterPays);
						scatterPays.remove(index);
						continue;
					}
//					if(-symbol==BookOfRaConfiguration.ROAD_SCATTER){
//						int index = findIndexOf("road",scatterPays);
//						scatterPays.remove(index);
//						continue;
//					}
//					if(-symbol==BookOfRaConfiguration.WELL_SCATTER){
//						int index = findIndexOf("well",scatterPays);
//						scatterPays.remove(index);
//						continue;
//					}
				}
				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and bonus : "+scatterPays.toString() , 0 ,(scatterPays.size()+viewCombos.size()-scatterPaysCounter));

				rountcounter++;
			}

		}
	}
	
	
	@Test
	public void test2(){
		for (int i = 1; i < 2; i++) {
			
			String filename = "BookOfRafreegame"+i+".xml.gz";
			log.info("BEGINNING FILE "+i);
			loadGzippedParams(getClass().getResourceAsStream("/"+filename));
			List<EntireGameOutcomes> entireGameOutcomes = playLog.getEntireGameOutcomes();
			log.info(entireGameOutcomes.size()+"");
			EntireGameOutcomes first = entireGameOutcomes.get(0);
			List<EntireGameOutcome> rounds = first.getEntireGameOutcome();
			int rountcounter = 1;

			// for evey round
			for (EntireGameOutcome entireGameOutcome : rounds) {
				log.info("BEGINNING ROUND "+(rountcounter));
				List<Reelstops> reelstops = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getReelstops();
				List<Wins> wins = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getWins();
				// get the reels
				int[] reels = getreels(reelstops);
				log.info("reels = "+Arrays.toString(reels));
				int[][] reelset = configuration.getReels(i-1);
				for (int z = 0; z < reels.length; z++) {
//					Log.info("reel length = "+reelset[z].length);
					reels[z] = (reels[z]+1+reelset[z].length)%reelset[z].length;
				}
				log.info("new reels = "+Arrays.toString(reels));
				List<ModelWin> gwins = wins.get(0).getModelWin();
				log.info(gwins.size()+"");
				Double winsum = 0.0;
				Double[] actwinsum = new Double[]{0.0};
				List<Integer> viewCombos = new ArrayList<Integer>();
				int scatterPaysCounter = 0;
				if(rountcounter==200){
					log.info("auto einai gia na kanoume track pio round prepei na kanoume debug");
				}
				try {

					int specialSumbol = 1;
					bookOfRaRound.setState(BookOfRaRound.RoundState.FREE_SPIN);
					viewCombos = bookOfRaRound.evaluatePosition(action, 4, 0.0, reels, 1, 10, specialSumbol, actwinsum,true);
				} catch (ActionFailedException e) {
					e.printStackTrace();
					log.error("Something terrible happened.", e);
					throw new RuntimeException("Epic Fail", e);
				}

				SlotConfiguration config= new BookOfRaConfiguration();
				//				List<Integer> viewCombos; //= CreepyCrawlersRound.getReelViewWinCombinations(config, reelview,i);
				List<String> scatterPays = new ArrayList<String>();
				// sum the winnings of any
				for (ModelWin modelWin : gwins) {
					String pay =modelWin.getModelPrize().get(0).getValue();
					if (pay==null){
						if(modelWin.getCombination().toLowerCase().contains("book")){
							scatterPays.add(modelWin.getCombination().toLowerCase());
							continue;
						}
					}

					//String linestr = modelWin.getPaylineNames().get(0).getPaylineName();
					if (pay==null){
						log.info("here");
					}
					double wn = Double.parseDouble(pay);
					winsum = winsum + wn;
					//Mockito.verify(koa_action, VerificationModeFactory.atLeastOnce()).createGeneratedAction(player, 1, "WIN", linestr.subSequence(4,linestr.length()).toString(), wn);
				}
				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and wins : "+ winsum, winsum ,actwinsum[0]);
				if(winsum>0.0){
					log.info("kerdise tosa " + winsum);
				}

				// checking bonus pays
				scatterPaysCounter = scatterPays.size();
				for (Integer combo : viewCombos) {
					int symbol = (Integer)config.getWinCombo(combo)[0];
					if(-symbol==BookOfRaConfiguration.FREE_SPIN_SCATTER_WILD){
						int index = findIndexOf("book",scatterPays);
						scatterPays.remove(index);
						continue;
					}
					if(symbol==1){
//					int index = findIndexOf("s_10",scatterPays);
//					scatterPays.remove(index);
						scatterPaysCounter++;
					continue;
				}
//					if(-symbol==BookOfRaConfiguration.WELL_SCATTER){
//						int index = findIndexOf("well",scatterPays);
//						scatterPays.remove(index);
//						continue;
//					}
				}
				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and bonus : "+scatterPays.toString() , 0 ,(scatterPays.size()+viewCombos.size()-scatterPaysCounter));

				rountcounter++;
			}

		}
	}

	private int findIndexOf(String bonusSymbol, List<String> scatterPays) {
		int res = -1;
		for(String sp : scatterPays){
			if(sp.toLowerCase().contains(bonusSymbol)){
				res = scatterPays.indexOf(sp);
				break;
			}
		}
		return res;
	}

	private int[] getreels(List<Reelstops> reelstops) {
		int[] res = new int[]{
				Integer.parseInt(reelstops.get(0).getStops().get(0).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(1).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(2).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(3).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(4).getValue())
		};
		return res;
	}
	
	private void loadGzippedParams(InputStream is) {
		try {
			is = new GZIPInputStream(is);
		} catch (IOException e) {
			throw new RuntimeException("file parcing problem",e);
		}
		
		playLog = new ObjectFactory().createServerPlayLog();
		try {
			playLog = XmlUtils.unmarshall(ServerPlayLog.class, is);
		} catch (JAXBException e) {
			throw new RuntimeException("file parcing problem",e);
		}

	}

}
