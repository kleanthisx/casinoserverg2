package com.magneta.test.slots.luckyladycharm;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameSettings;
import com.magneta.games.RoundOptions;
import com.magneta.games.configuration.slots.LuckyLadyCharmConfiguration;
import com.magneta.games.slots.luckyladycharm.LuckyLadyCharmRound;
import com.magneta.test.games.Util;
import com.magneta.test.utils.sd.ServerPlayLog;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { GameAction.class,GameSettings.class })
public class LLCRoundTest {

	//private static final Logger log = LoggerFactory.getLogger(LLCRoundTest.class);

	GameSettings gamesettings;
	RoundOptions options;
	LuckyLadyCharmRound luckyLadyCharmRound;
	GameAction action;
	ServerPlayLog playLog;
	GamePlayer player;
	LuckyLadyCharmConfiguration configuration;

	@Before
	public void initialise() throws ActionFailedException{
		printinfo("initialising");
		gamesettings = mock(GameSettings.class);
		options = new RoundOptions(1, 0.0, 100.0, gamesettings );
		luckyLadyCharmRound = new LuckyLadyCharmRound();
		GameBean game = new GameBean();
		GameTemplateInfo gameTemplate = new com.magneta.casino.games.templates.impl.BookOfRa();
		configuration = new LuckyLadyCharmConfiguration();
		luckyLadyCharmRound.init(game , gameTemplate, configuration, options, 1, 1, new byte[]{'a','1','2'});

		action = mock(GameAction.class);
		player = Util.createPlayer();
		when(action.getPlayer()).thenReturn(player);
		when(action.createGeneratedAction(any(GamePlayer.class), anyInt(), anyString(), anyString(), anyDouble())).thenReturn(action);
	}

	private void printinfo(String string) {
		System.out.println(string);
		
	}

	@Test
	public void test1(){

		int reelSetNumber = 0;
		int[][] reelset = configuration.getReels(0);
		int[] reelstops = new int[]{9,33,160,31,5};
		
		printinfo("new reels = "+Arrays.toString(reelstops));

		int[][] reelsView = luckyLadyCharmRound.calculateReelView(configuration.getReels(reelSetNumber), reelstops);
		for (int j = 0; j < reelsView.length; j++) {
			printinfo("new reels = "+Arrays.toString(reelsView[j]));
		}
		
		
		Double[] actwinsum = new Double[]{0.0};
		Object viewCombos;
		try {

			viewCombos = luckyLadyCharmRound.evaluatePosition(action, reelSetNumber, 0.0, reelstops, 1, 10  -1, actwinsum, true);
		} catch (ActionFailedException e) {
			e.printStackTrace();
			//log.error("Something terrible happened.", e);
			throw new RuntimeException("Epic Fail", e);
		}
		if(actwinsum[0]>0.0){
			printinfo("kerdise tosa " + actwinsum[0] + " kai eprepe na kerdisei" + 320);
		}
		Assert.assertEquals("result with reelset "+reelSetNumber+" has failed with reelstops: "+Arrays.toString(reelstops).toString()+" and wins : "+ actwinsum[0], (Double)320.0 ,actwinsum[0]);

		// checking bonus pays

//		for (Integer combo : viewCombos) {
//			int symbol = (Integer)config.getWinCombo(combo)[0];
//			if(-symbol==BookOfRaConfiguration.FREE_SPIN_SCATTER_WILD){
//				int index = findIndexOf("book",scatterPays);
//				scatterPays.remove(index);
//				continue;
//			}
//		}
//						Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reelstops).toString()+" and bonus : "+scatterPays.toString() , 0 ,(scatterPays.size()+viewCombos.size()-scatterPaysCounter));

	}
}
