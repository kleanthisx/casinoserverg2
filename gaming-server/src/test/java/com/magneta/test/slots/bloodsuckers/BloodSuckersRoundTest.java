package com.magneta.test.slots.bloodsuckers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameSettings;
import com.magneta.games.RoundOptions;
import com.magneta.games.configuration.slots.BloodSuckersConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.bloodsuckers.BloodSuckersRound;
import com.magneta.test.games.Util;
import com.magneta.test.utils.sd.ObjectFactory;
import com.magneta.test.utils.sd.ServerPlayLog;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Reelstops;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Wins;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Wins.ModelWin;
import com.magneta.test.utils.xml.XmlUtils;


@RunWith(PowerMockRunner.class)
@PrepareForTest( { GameAction.class,GameSettings.class })
public class BloodSuckersRoundTest {

	private GameSettings gamesettings;
	private RoundOptions options;
	private BloodSuckersRound bloodSuckersRound;
	private GameAction rr_action;
	private ServerPlayLog playLog;
	private GamePlayer player;
	private BloodSuckersConfiguration bloodSuckersConfiguration;

	@Before
	public void initialise() throws ActionFailedException{
		printMessage("initialising");
		gamesettings = mock(GameSettings.class);
		options = new RoundOptions(1, 0.0, 100.0, gamesettings );
		bloodSuckersRound = new BloodSuckersRound();
		GameBean game = new GameBean();
		GameTemplateInfo gameTemplate = new com.magneta.casino.games.templates.impl.BloodSuckers();
		bloodSuckersConfiguration = new BloodSuckersConfiguration();
		bloodSuckersRound.init(game , gameTemplate, bloodSuckersConfiguration, options, 1, 1, new byte[]{'a','1','2'});

		rr_action = mock(GameAction.class);
		
		UserAuthenticationBean authBean = new UserAuthenticationBean();
		authBean.setUserId(1l);
		
		player = Util.createPlayer();
		when(rr_action.getPlayer()).thenReturn(player);
		when(rr_action.createGeneratedAction(any(GamePlayer.class), anyInt(), anyString(), anyString(), anyDouble())).thenReturn(rr_action);
	}

	@Test
	public void testBase(){
		for (int i = 0; i < 1; i++) {

			String filename = "basegame.xml";
			printMessage("BEGINNING BASE GAME FILE ");
			loadParams(getClass().getResourceAsStream("/"+filename));
			List<EntireGameOutcomes> entireGameOutcomes = playLog.getEntireGameOutcomes();
			printMessage(entireGameOutcomes.size()+"");
			EntireGameOutcomes first = entireGameOutcomes.get(0);
			List<EntireGameOutcome> rounds = first.getEntireGameOutcome();
			int rountcounter = 1;

			// for evey round
			for (EntireGameOutcome entireGameOutcome : rounds) {
//				if(rountcounter<16){
//					rountcounter++;
//					continue;
//				}else{
//					printMessage("auto einai gia na kanoume track pio round prepei na kanoume debug");
//				}
				printMessage("BEGINNING ROUND {}"+ (rountcounter));
				List<Reelstops> reelstops = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getReelstops();
				List<Wins> wins = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getWins();
				// get the reels
				int[][] confreels = bloodSuckersConfiguration.getReels(0);
				int[] reels = getreels(reelstops);
				printMessage("reels = "+Arrays.toString(reels));
				for (int z = 0; z < reels.length; z++) {
					reels[z] = (reels[z]+1+confreels[z].length)%confreels[z].length;
				}
				printMessage("new reels = "+Arrays.toString(reels));
				List<ModelWin> gwins = wins.get(0).getModelWin();
				printMessage(gwins.size()+"");
				Double winsum = 0.0;
				Double[] actwinsum = new Double[]{0.0};
				List<Integer> viewCombos = new ArrayList<Integer>();
				int scatterPaysCounter = 0;
				
				try {

					viewCombos = bloodSuckersRound.evaluatePosition(rr_action, 0.0, reels,bloodSuckersConfiguration, 25, 2.0, false, false, false, actwinsum);
				} catch (ActionFailedException e) {
					throw new RuntimeException("Epic Fail", e);
				}


				SlotConfiguration config= new BloodSuckersConfiguration();
				List<String> scatterPays = new ArrayList<String>();
				// sum the winnings of any
				for (ModelWin modelWin : gwins) {
					String pay =modelWin.getModelPrize().get(0).getValue();
					if (pay==null){
						if(modelWin.getCombination().toLowerCase().contains("_scatter")||modelWin.getCombination().toLowerCase().contains("bonus")||modelWin.getCombination().toLowerCase().contains("pot")){
							scatterPays.add(modelWin.getCombination().toLowerCase());
							continue;
						}
					}

					double wn = 0.0;
					try{
						wn = Double.parseDouble(pay);
					}catch (Exception e) {
						printMessage("error");
					}
					winsum = winsum + wn;
					//Mockito.verify(rr_action, VerificationModeFactory.atLeastOnce()).createGeneratedAction(player, 1, "WIN", linestr.subSequence(4,linestr.length()).toString(), wn);
				}
				actwinsum[0] = actwinsum[0]/2;
				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and wins : "+ winsum, winsum ,actwinsum[0]);
				if(winsum>0.0){
					printMessage("kerdise tosa " + winsum);
				}

								// checking bonus pays
								scatterPaysCounter = scatterPays.size();
								for (Integer combo : viewCombos) {
									int symbol = (Integer)config.getWinCombo(combo)[0];
									if(-symbol==BloodSuckersConfiguration.FREE_SPIN_SYMBOL){
										if((Integer)config.getWinCombo(combo)[1]<=2){
											scatterPaysCounter++;
											continue;
										}
										int index = findIndexOf("_scatter",scatterPays);
										scatterPays.remove(index);
										continue;
									}
									if(-symbol==BloodSuckersConfiguration.BONUS_SYMBOL){
										int index = findIndexOf("bonus",scatterPays);
										scatterPays.remove(index);
										continue;
									}
								}
								Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and bonus : "+scatterPays.toString() , 0 ,(scatterPays.size()+viewCombos.size()-scatterPaysCounter));

				rountcounter++;
			}

		}
	}
	
	//@Test
	public void testbonus(){

		double wins = 0.0;
		Random random = new Random();
		for (int i = 0; i < 5000; i++) {
			LinkedList<Double> boxes = new LinkedList<Double>();
			
			//filling boxes
			for (int i1 = 0; i1 < 12; i1++) {
				double box_multiplier =0.0;
				if(i1<BloodSuckersConfiguration.MAXIMUM_BOXES_TO_OPEN){

					box_multiplier = bloodSuckersRound.getWeightedRandom(BloodSuckersConfiguration.BONUS_WEIGHTS, random);
				}
				boxes.add(box_multiplier);
			}
			// shifling boxes
			Collections.shuffle(boxes);
			//seleccting boxes  till first 0
			for (Iterator<Double> iterator = boxes.iterator(); iterator.hasNext();) {
				Double double1 = iterator.next();
				if(double1>0.0){
					wins+= double1;
				}else{
					break;
				}
			}
		}
		printMessage("games played " + 5000 + " and average win = " + wins/5000);
	}
	
	//@Test
	public void testFree(){
		for (int i = 0; i < 1; i++) {

			String filename = "freegame.xml";
			printMessage("BEGINNING FREE GAME FILE ");
			loadParams(getClass().getResourceAsStream("/"+filename));
			List<EntireGameOutcomes> entireGameOutcomes = playLog.getEntireGameOutcomes();
			printMessage(entireGameOutcomes.size()+"");
			EntireGameOutcomes first = entireGameOutcomes.get(0);
			List<EntireGameOutcome> rounds = first.getEntireGameOutcome();
			int rountcounter = 1;

			// for evey round
			for (EntireGameOutcome entireGameOutcome : rounds) {
//				if(rountcounter<598){
//					rountcounter++;
//					continue;
//				}else{
//					printMessage("auto einai gia na kanoume track pio round prepei na kanoume debug");
//				}
				printMessage("BEGINNING ROUND {}"+ (rountcounter));
				List<Reelstops> reelstops = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getReelstops();
				List<Wins> wins = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getWins();
				// get the reels
				int[][] confreels = bloodSuckersConfiguration.getReels(0);
				int[] reels = getreels(reelstops);
				printMessage("reels = "+Arrays.toString(reels));
				for (int z = 0; z < reels.length; z++) {
					reels[z] = (reels[z]+1+confreels[z].length)%confreels[z].length;
				}
				printMessage("new reels = "+Arrays.toString(reels));
				List<ModelWin> gwins = wins.get(0).getModelWin();
				printMessage(gwins.size()+"");
				Double winsum = 0.0;
				Double[] actwinsum = new Double[]{0.0};
				List<Integer> viewCombos = new ArrayList<Integer>();
				int scatterPaysCounter = 0;
				
				try {

					viewCombos = bloodSuckersRound.evaluatePosition(rr_action, 0.0, reels,bloodSuckersConfiguration, 24, 2.0, false, true, false, actwinsum);
				} catch (ActionFailedException e) {
					throw new RuntimeException("Epic Fail", e);
				}


				//				List<Integer> viewCombos; //= CreepyCrawlersRound.getReelViewWinCombinations(config, reelview,i);
				List<String> scatterPays = new ArrayList<String>();
				// sum the winnings of any
				for (ModelWin modelWin : gwins) {
					String pay =modelWin.getModelPrize().get(0).getValue();
					if (pay==null){
						if(/*modelWin.getCombination().toLowerCase().contains("_scatter")||*/modelWin.getCombination().toLowerCase().contains("bonus")||modelWin.getCombination().toLowerCase().contains("pot")){
							scatterPays.add(modelWin.getCombination().toLowerCase());
							continue;
						}
					}

					//String linestr = modelWin.getPaylineNames().get(0).getPaylineName();
					double wn = 0.0;
					try{
						wn = Double.parseDouble(pay);
					}catch (Exception e) {
						printMessage("error");
					}
					winsum = winsum + wn;
					//Mockito.verify(koa_action, VerificationModeFactory.atLeastOnce()).createGeneratedAction(player, 1, "WIN", linestr.subSequence(4,linestr.length()).toString(), wn);
				}
				actwinsum[0] = actwinsum[0]/2;
				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and wins : "+ winsum, winsum ,actwinsum[0]);
				if(winsum>0.0){
					printMessage("kerdise tosa " + winsum);
				}

				//				// checking bonus pays
				//				scatterPaysCounter = scatterPays.size();
				//				for (Integer combo : viewCombos) {
				//					int symbol = (Integer)config.getWinCombo(combo)[0];
				//					if(-symbol==RainbowRichesConfiguration.POT_SCATTER){
				//						int index = findIndexOf("pots",scatterPays);
				//						scatterPays.remove(index);
				//						continue;
				//					}
				//					if(-symbol==RainbowRichesConfiguration.ROAD_SCATTER){
				//						int index = findIndexOf("road",scatterPays);
				//						scatterPays.remove(index);
				//						continue;
				//					}
				//					if(-symbol==RainbowRichesConfiguration.WELL_SCATTER){
				//						int index = findIndexOf("well",scatterPays);
				//						scatterPays.remove(index);
				//						continue;
				//					}
				//				}
				//				Assert.assertEquals("result : round "+rountcounter+" with reelset "+i+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and bonus : "+scatterPays.toString() , 0 ,(scatterPays.size()+viewCombos.size()-scatterPaysCounter));

				rountcounter++;
			}

		}
	}
	
	@Test
	public void testGoIntoBonus(){

				int[] reels = new int[]{46,1,1,1,1};
				Double[] actwinsum = new Double[]{0.0};
				
				try {
					bloodSuckersRound.evaluatePosition(rr_action, 0.0, reels,bloodSuckersConfiguration, 25, 2.0, false, false, false, actwinsum);
				} catch (ActionFailedException e) {
					throw new RuntimeException("Epic Fail", e);
				}


	}
	

	private int findIndexOf(String bonusSymbol, List<String> scatterPays) {
		int res = -1;
		for(String sp : scatterPays){
			if(sp.toLowerCase().contains(bonusSymbol)){
				res = scatterPays.indexOf(sp);
				break;
			}
		}
		return res;
	}

	private int[] getreels(List<Reelstops> reelstops) {
		int[] res = new int[]{
				Integer.parseInt(reelstops.get(0).getStops().get(0).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(1).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(2).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(3).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(4).getValue())
		};
		return res;
	}

	private void loadParams(InputStream is) {
		playLog = new ObjectFactory().createServerPlayLog();
		try {
			playLog = XmlUtils.unmarshall(ServerPlayLog.class, is);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException("file parcing problem",e);
		}

	}

	private void printMessage(String mesage){
		System.out.println(mesage);
	}
}