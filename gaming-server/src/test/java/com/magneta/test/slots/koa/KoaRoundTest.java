package com.magneta.test.slots.koa;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameSettings;
import com.magneta.games.RoundOptions;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.slots.KingOfAztecConfiguration;
import com.magneta.games.slots.kingofaztec.KingOfAztecRound;
import com.magneta.test.games.Util;
import com.magneta.test.utils.sd.ObjectFactory;
import com.magneta.test.utils.sd.ServerPlayLog;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Reelstops;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Wins;
import com.magneta.test.utils.sd.ServerPlayLog.EntireGameOutcomes.EntireGameOutcome.ModelOutcomes.ModelOutcome.Wins.ModelWin;
import com.magneta.test.utils.xml.XmlUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { GameAction.class,GameSettings.class })
public class KoaRoundTest {

	private static final Logger log = LoggerFactory.getLogger(KoaRoundTest.class);
	
	GameSettings gamesettings;
	RoundOptions options;
	KingOfAztecRound kingOfAztecRound;
	GameAction koa_action;
	ServerPlayLog playLog;
	GamePlayer player;

	@Before
	public void initialise() throws ActionFailedException{
		log.info("initialising");
		gamesettings = mock(GameSettings.class);
		options = new RoundOptions(1, 0.0, 100.0, gamesettings );
		kingOfAztecRound = new KingOfAztecRound();
		GameBean game = new GameBean();
		GameTemplateInfo gameTemplate = new com.magneta.casino.games.templates.impl.KingOfAztec();
		GameConfiguration configuration = new KingOfAztecConfiguration();
		kingOfAztecRound.init(game , gameTemplate , configuration , options, 1, 1, new byte[]{'a','1','2'});

		koa_action = mock(GameAction.class);
		player = Util.createPlayer();
		when(koa_action.getPlayer()).thenReturn(player);
		when(koa_action.createGeneratedAction(any(GamePlayer.class), anyInt(), anyString(), anyString(), anyDouble())).thenReturn(koa_action);
	}

	@Test
	public void test1(){
		for (int i = 0; i < 10; i++) {
			log.info("BEGINNING FILE "+i);
			loadGzippedParams(getClass().getResourceAsStream("/koa"+i+".xml.gz"));
			List<EntireGameOutcomes> entireGameOutcomes = playLog.getEntireGameOutcomes();
			log.info(entireGameOutcomes.size()+"");
			EntireGameOutcomes first = entireGameOutcomes.get(0);
			List<EntireGameOutcome> rounds = first.getEntireGameOutcome();
			int rountcounter = 0;
			// for evey round
			for (EntireGameOutcome entireGameOutcome : rounds) {
				log.info("BEGINNING ROUND "+rountcounter);
				List<Reelstops> reelstops = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getReelstops();
				List<Wins> wins = entireGameOutcome.getModelOutcomes().get(0).getModelOutcome().get(0).getWins();
				// get the reels
				int[] reels = getreels(reelstops);
				log.info("reels = "+Arrays.toString(reels));
				for (int z = 0; z < reels.length; z++) {
					reels[z] = (reels[z]+1+64)%64;
				}
				log.info("new reels = "+Arrays.toString(reels));
				List<ModelWin> gwins = wins.get(0).getModelWin();
				log.info(gwins.size()+"");
				Double winsum = 0.0;
				Double[] actwinsum = new Double[]{0.0};
				try {
					kingOfAztecRound.evaluatePosition(koa_action, i, 0, reels,1,19,actwinsum,true);
				} catch (ActionFailedException e) {
					log.error("Something terrible happened.", e);
					throw new RuntimeException("Epic Fail", e);
				}
				// sum the winnings of any
				for (ModelWin modelWin : gwins) {
					//String linestr = modelWin.getPaylineNames().get(0).getPaylineName();
					double wn = Double.parseDouble(modelWin.getModelPrize().get(0).getValue());
					winsum = winsum + wn;
//					Mockito.verify(koa_action, VerificationModeFactory.atLeastOnce()).createGeneratedAction(player, 1, "WIN", linestr.subSequence(4,linestr.length()).toString(), wn);
				}
				Assert.assertEquals("result : "+rountcounter+" has failed with reelstops: "+Arrays.toString(reels).toString()+" and wins : "+ winsum, winsum ,actwinsum[0]);
				if(winsum>0.0){
					log.info("kerdise toea " + winsum);
				}
				rountcounter++;
			}

		}
	}

	private int[] getreels(List<Reelstops> reelstops) {
		int[] res = new int[]{
				Integer.parseInt(reelstops.get(0).getStops().get(0).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(1).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(2).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(3).getValue()),
				Integer.parseInt(reelstops.get(0).getStops().get(4).getValue())
		};
		return res;
	}

	
	
//	private void loadParams(InputStream is) {
//		playLog = new ObjectFactory().createServerPlayLog();
//		try {
//			playLog = XmlUtils.unmarshall(ServerPlayLog.class, is);
//		} catch (JAXBException e) {
//			e.printStackTrace();
//			throw new RuntimeException("file parcing problem",e);
//		}
//
//	}
	
	private void loadGzippedParams(InputStream is) {
		try {
			is = new GZIPInputStream(is);
		} catch (IOException e) {
			throw new RuntimeException("file parcing problem",e);
		}
		
		playLog = new ObjectFactory().createServerPlayLog();
		try {
			playLog = XmlUtils.unmarshall(ServerPlayLog.class, is);
		} catch (JAXBException e) {
			throw new RuntimeException("file parcing problem",e);
		}

	}

}
