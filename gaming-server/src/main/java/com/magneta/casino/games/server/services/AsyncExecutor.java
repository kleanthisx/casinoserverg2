package com.magneta.casino.games.server.services;

import java.util.concurrent.Callable;

public interface AsyncExecutor {

	Integer execute(Callable<?> command);
}
