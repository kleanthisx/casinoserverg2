/**
 * 
 */
package com.magneta.casino.games.random.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

import com.magneta.casino.games.random.RandomSeedSource;

/**
 * Seed generator from a device file.
 * 
 * @author anarxia
 * This class is not thread-safe all public method calls must
 * be serialized externally.
 */
public class FileRandomSeedSource implements RandomSeedSource {

    private static final int BUF_SIZE = 4096;
    private static final int POOL_SIZE = 8192;
    
    private ReadableByteChannel channel;
    private FileInputStream fileIo;
    private ByteBuffer buf;
    private byte[] pool;
    private int poolSize;
    private int poolPos;
    private final String devname;
    
    public FileRandomSeedSource(String devname) throws FileNotFoundException {
       this.devname = devname;
       reOpen();
       buf = ByteBuffer.allocateDirect(BUF_SIZE);
       pool = new byte[POOL_SIZE];
       poolSize = 0;
       poolPos = 0;
    }
    
    @Override
    public void reOpen() throws FileNotFoundException {
        if (channel != null) {
            try {
                channel.close();
            } catch (IOException e) {
                channel = null;
            }
        }
        
        if (fileIo != null) {
            try {
                fileIo.close();
            } catch (IOException e) {
                fileIo = null;
            }
        }
        
        fileIo = new FileInputStream(devname);
        channel = fileIo.getChannel();
    }

    /* Append the contents of the buffer to the pool */
    private void poolAppend(int bytes) {
        buf.rewind();
        
        /* Calculate write position (poolPos + poolSize) */
        int writePos = poolPos + poolSize;
        if (writePos >= pool.length) {
            writePos = poolPos + poolSize - pool.length;
        }
        
        int writeBytes1 = Math.min(pool.length - writePos, bytes);
        buf.get(pool, writePos, writeBytes1);
        
        /* Write from the start of the Pool until writePos */
        int writeBytes2 = Math.min(bytes - writeBytes1, writePos);
        if (writeBytes2 > 0) {
            buf.get(pool, 0, writeBytes2);
        }
        
        poolSize += writeBytes1 + writeBytes2;
        poolSize = Math.min(pool.length, poolSize);
    }
    
    private byte[] poolRead(int bytes) {
        
        byte[] seed = new byte[bytes];
        
        int readBytes1 = Math.min(pool.length - poolPos, bytes);
        System.arraycopy(pool, poolPos, seed, 0, readBytes1);
        poolPos += readBytes1;
        poolSize -= readBytes1;
        
        int readBytes2 = Math.min(bytes - readBytes1, poolPos);
        if (readBytes2 > 0) {
            System.arraycopy(pool, 0, seed, readBytes1, readBytes2);
            poolPos = readBytes2;
            poolSize -= readBytes2;
        }
        
        return seed;
    }

    @Override
    public byte[] getSeed(int bytes) throws IOException {

        while (poolSize < bytes) {
            int bytesRead = -1;
            buf.clear();
            if (channel == null) {
                throw new IOException(devname + " is closed.");
            }
            bytesRead = channel.read(buf);

            if (bytesRead > 0)
                poolAppend(bytesRead);
            
            if (bytesRead == -1) {
                throw new IOException(devname + " EOF.");
            }
        }

        return poolRead(bytes);
    }

}
