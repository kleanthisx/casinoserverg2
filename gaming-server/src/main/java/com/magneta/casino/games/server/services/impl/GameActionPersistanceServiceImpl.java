package com.magneta.casino.games.server.services.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.CurrentRoundService;
import com.magneta.casino.games.server.services.GameActionPersistanceService;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.GamePlayerStats;
import com.magneta.games.GameRound;
import com.magneta.games.InsufficientFundsException;

public class GameActionPersistanceServiceImpl implements GameActionPersistanceService {

	private static final Logger log = LoggerFactory.getLogger(GameActionPersistanceServiceImpl.class);
	
	private final CurrencyParser currencyParser;
	private final CurrentRoundService currentRoundService;
	private final UserDetailsService userDetailsService;
	
	public GameActionPersistanceServiceImpl(CurrencyParser currencyParser, CurrentRoundService currentRoundService,
			UserDetailsService userDetailsService) {
		this.currencyParser = currencyParser;
		this.currentRoundService = currentRoundService;
		this.userDetailsService = userDetailsService;
	}
	
	@Override
	public void commit(GameAction action) throws ActionFailedException {
        commit(action, false);
    }
	
	
	private void updateRoundStats(GameAction action, GamePlayerStats stats) {
		 if (action.getDescription().equals("WIN")){
             stats.addWins(action.getAmount());
         } else {
             stats.addBets(action.getAmount());
         }
		 
		 for (GameAction a: action.getGeneratedActions()) {
			 updateRoundStats(a, stats);
		 }
		 
		 for (GameAction a: action.getAdditionalActions()) {
			 updateRoundStats(a, stats);
		 }
	}
	
	private void markSaved(GameAction action) {
		/**
		 * Marks the current action as well as any generated or additional actions as saved.
		 */

		action.setSaved();

		for (GameAction nextAction: action.getGeneratedActions()) {
			markSaved(nextAction);
		}

		for (GameAction nextAction: action.getAdditionalActions()) {
			markSaved(nextAction);
		}

	}
	
    /**
     * Commits the current action. If the action has generated and/or additional
     * actions they are committed as well.
     * If the action is already saved only the generated actions will be saved.
     * 
     * @param roundFinish This flag indicates to the action that it is the last parent
     * action to be performed before the round finish, which is the only case this flag
     * should ever have the value of true. The correct use of this flag is vital because
     * it affects the tracking of statistical data kept by the system.
     * 
     * @throws ActionFailedException
     */
	@Override
    public void commit(GameAction action, boolean roundFinish) throws ActionFailedException {
		
		/* If the action is loaded we assume that it's already in the database including additional
		 * and generated actions
		 */
        if (action.isLoaded()) {
        	/* */
        	if (roundFinish) {
        		log.warn("Loaded actions cannot also be round finish actions. {}", action);
        	}
        	
        	GameRound round = currentRoundService.getRound();
            updateRoundStats(action, round.getRoundStats());
        	
            return;
        }
        
        Connection dbConn = ConnectionFactory.getConnection();
        if (dbConn == null) {
            log.error("Action commit error: Out of database connections. Round: " + action.getTableId() + "-" + action.getRoundId() + "." + (roundFinish?" [finish]":""));
            throw new ActionFailedException("Error while commiting action");
        }
        
        List<GamePlayer> players = new ArrayList<GamePlayer>(1);
        GamePlayerStats playerStats = new GamePlayerStats();
        
        boolean insufficientFunds = false;
        
        try {
            try {
                dbConn.setAutoCommit(false);
            } catch (SQLException e) {
            	log.error("Unable to set autocommit while commiting action", e);
                throw new ActionFailedException("Error while commiting action", e);
            }

            try {
            	saveAction(action, dbConn, playerStats, players);

                if (roundFinish) {
                	log.debug("Round {}-{} marking as finished", action.getTableId(), action.getRoundId());
                    markRoundFinished(action.getTableId(), action.getRoundId(), dbConn);
                }
            	
                dbConn.commit();
            } catch (SQLException e) {
                if (e.getMessage().toLowerCase().contains("play free credits")) {
                    throw new ActionFailedException("Must play free credits on slots first", e);
                } else if (e.getMessage().toLowerCase().contains("check_user_balance_positive")){
                	throw new InsufficientFundsException();
                } else {
                    log.warn("Error while saving action: " + this, e);
                    throw new ActionFailedException("Error while commiting action", e);
                }
            }
        } catch (InsufficientFundsException e) {
        	insufficientFunds = true;
        } catch (ActionFailedException e) {
            try {
                dbConn.rollback();
            } catch (SQLException e1) {
                log.info("Error while rolling back", e1);
            }
            throw e;
        } finally {
            DbUtil.close(dbConn);
        }

        /* Handle insufficient funds after we close the connection to avoid calling 
         * services with a database connection open.
         */
        if (insufficientFunds) {
        	log.warn("Error while saving action: {}: Insufficient funds.", action);        	

        	try {
        		UserBalanceBean balance = userDetailsService.getUserBalance(action.getPlayer().getUserId());

        		if (balance.getPlayCredits() != null && balance.getPlayCredits() > 0.009) {
        			throw new ActionFailedException("This game plays with real money only");
        		}
        	} catch (ServiceException e1) {
        		log.error("Unable to retrieve user balance", e1);
        	}

        	throw new ActionFailedException("Insufficient funds");
        }
        
        markSaved(action);
        
        for (GamePlayer p: players) {
            p.commitStats(playerStats);
        }
        
        GameRound round = currentRoundService.getRound();
        round.getRoundStats().add(playerStats);
    }
    
    /**
     * Inserts an action in the database. Please note that the database has triggers
     * to automatically create financial transactions (and update the balance). The database trigger
     * ill deduct the amount from player balance unless the action is "WIN" in which case it will be added to the balance. 
     * 
     * @param action
     * @param dbConn
     * @throws SQLException
     */
    private void insertAction(GameAction action, Connection dbConn) throws SQLException {
    	PreparedStatement statement = null;

    	String sql = "INSERT INTO game_round_actions(table_id, round_id, seat_id," +
    	"action_id, sub_action_id, user_id, action_desc, action_area, action_value,"+ 
    	"amount)";

    	if ((action.getAmountFunction() != null) && (!action.getAmountFunction().isEmpty())) {
    		sql += " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, "+ action.getAmountFunction() +") RETURNING amount";
    	} else {
    		sql += " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    	}

    	try {
    		statement = dbConn.prepareStatement(sql);

    		statement.setLong(1, action.getTableId());
    		statement.setInt(2, action.getRoundId());
    		statement.setInt(3, action.getSeatId());
    		statement.setInt(4, action.getActionId());
    		statement.setInt(5, action.getSubActionId());

    		if (action.getPlayer() == null) {
    			statement.setObject(6, null);
    		} else {
    			statement.setLong(6, action.getPlayer().getUserId());
    		}

    		statement.setString(7, action.getDescription());

    		if (action.getArea() == null || action.getArea().isEmpty()) {
    			statement.setString(8, null);
    		} else {
    			statement.setString(8, action.getArea());
    		}

    		if (action.getActionValue() == null || action.getActionValue().isEmpty()) {
    			statement.setString(9, null);
    		} else {
    			statement.setString(9, action.getActionValue());
    		}

    		if ((action.getAmountFunction() == null) || (action.getAmountFunction().isEmpty())) {
    			if (currencyParser.normalizeDouble(action.getAmount()) == 0.0d) {
    				statement.setObject(10, null);
    			} else {
    				if (action.getPlayer() == null) {
    					throw new SQLException("Action " + action + " with amount does not have a player");
    				}
    				
    				statement.setBigDecimal(10, new BigDecimal(currencyParser.normalizeDouble(action.getAmount())));
    			}

    			statement.executeUpdate();
    		} else {
    			
    			if (action.getPlayer() == null) {
					throw new SQLException("Action " + action + " with amount function does not have a player");
				}
    			
    			ResultSet rs = null;

    			try {
    				rs = statement.executeQuery();
    				
    				if (rs.next()) {
    					action.setAmount(rs.getDouble("amount"));
    				}
    			} finally {
    				DbUtil.close(rs);
    			}
    		}
    		
    		log.debug("Round: {}-{}: Saved action: {} ", new Object[] { action.getTableId(), action.getRoundId(), action});
    		
    	} finally {
    		DbUtil.close(statement);
    	}
    }
    
    private void markRoundStarted(long tableId, int roundId, Connection dbConn) throws ActionFailedException {
    	PreparedStatement myStmt = null;
        try {
            String sql = "UPDATE game_rounds SET round_started=TRUE" +
                         " WHERE table_id=? AND round_id=? AND round_started=FALSE";
           
            myStmt = dbConn.prepareStatement(sql);
            
            myStmt.setLong(1, tableId);
            myStmt.setInt(2, roundId);
            
            myStmt.executeUpdate();
            
            log.debug("Round: {}-{}: marked as started");
            
        } catch (SQLException e) {
            throw new ActionFailedException("Error while marking round started", e);
        } finally {
            DbUtil.close(myStmt);
        }
    }
    
    private void markRoundFinished(long tableId, int roundId, Connection dbConn) throws ActionFailedException {
    	PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = dbConn.prepareStatement("SELECT finish_round(?,?) FROM game_rounds WHERE table_id=? AND round_id=? AND date_closed IS NULL");
            stmt.setLong(1, tableId);
            stmt.setInt(2, roundId);
            stmt.setLong(3, tableId);
            stmt.setInt(4, roundId);
            
            rs  = stmt.executeQuery();
            if (!rs.next()) {
            	log.warn("Round {}-{} already marked as finished");
            }
        } catch (SQLException e){
            log.warn("Error while finishing round.",e);
            throw new ActionFailedException("Error while commiting action", e);
        } finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
        }
    }
    
    /**
     * Saves the currert action. And adds the player of the  action
     * to the players list.
     * @param dbConn The database connection to use for saving the action.
     * @throws ActionFailedException
     * @throws SQLException 
     */
    private void saveAction(GameAction action, Connection dbConn, GamePlayerStats stats, List<GamePlayer> players) throws ActionFailedException, SQLException {
        if (action.isSaved()) {
            for (GameAction nextAction: action.getGeneratedActions()) {
                saveAction(nextAction, dbConn, stats, players);
            }
            
            for (GameAction nextAction: action.getAdditionalActions()) {
               saveAction(nextAction, dbConn, stats, players);
            }
            return;
        }

        if ((action.getAmount() > 0.0001 || action.getAmountFunction() != null) && action.getPlayer() == null) {
            log.error("Action commit error: Amount without player. Round: " + action.getTableId() + "-" + action.getRoundId());
            throw new ActionFailedException("Error while commiting action");
        }
        
        if (action.isRoundStart()) {
        	markRoundStarted(action.getTableId(), action.getRoundId(), dbConn);
        }

        insertAction(action, dbConn);

        if (action.getPlayer() != null){
        	if(players.isEmpty()) {
        		players.add(action.getPlayer());
        	}
        	if (action.getDescription().equals("WIN")){
        		stats.addWins(action.getAmount());
        	} else {
        		stats.addBets(action.getAmount());
        	}
        }
        

        for (GameAction nextAction: action.getGeneratedActions()) {
            saveAction(nextAction, dbConn, stats, players);
        }
        
        for (GameAction nextAction: action.getAdditionalActions()) {
            saveAction(nextAction, dbConn, stats, players);
        }
    }
    
    /**
     * Load parent actions for the given round.
     *  
     * @throws ServiceException 
     */
    @Override
    public List<GameAction> loadParentActions(GamePlayer player, GameRound round) throws ServiceException { 	
    	List<GameAction> actions = new ArrayList<GameAction>();

    	String sql = "SELECT *" +
    			" FROM game_round_actions"+
    			" WHERE table_id = ? AND round_id = ? " +
    			" AND sub_action_id = 0" +
    			" ORDER BY action_id, sub_action_id";

    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new ServiceException("Out of database connections");
    	}
    	
    	PreparedStatement statement = null;
    	ResultSet rs = null;
    	
    	try {
    		statement = dbConn.prepareStatement(sql);

    		statement.setLong(1, round.getTableId());
    		statement.setInt(2, round.getId());

    		rs = statement.executeQuery();

    		while (rs.next()) {
    			GameAction action = new GameAction(true,
    					round.getTableId(),
    					round.getId(),
    					player, 
    	    			rs.getInt("action_id"),
    	    			rs.getInt("seat_id"),
    	    			rs.getString("action_desc"),
    	    			rs.getString("action_area"));
    	    	
    	    	action.setAmount(rs.getDouble("amount"));
    	    	action.setActionValue(rs.getString("action_value"));
    	    	action.setSaved();    			
    			actions.add(action);
    		}

    	} catch (SQLException e) {
    		throw new ServiceException("Database error while loading actions for round"  + round.getTableId() + "-" + round.getId(), e);
    	} finally {
    		DbUtil.close(rs);
    		DbUtil.close(statement);
    		DbUtil.close(dbConn);
    	}
    	
    	return actions;
    }
}
