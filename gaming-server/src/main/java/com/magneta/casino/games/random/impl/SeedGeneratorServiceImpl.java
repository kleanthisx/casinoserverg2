package com.magneta.casino.games.random.impl;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.casino.games.random.RandomSeedSource;
import com.magneta.casino.games.random.SeedGeneratorService;

public class SeedGeneratorServiceImpl implements SeedGeneratorService {

	private static final Logger log = LoggerFactory.getLogger(SeedGeneratorServiceImpl.class);
	
	private final RandomSeedSource seedGenerator;
    private final SecureRandomSeedSource fallbackSeedGenerator;
    private long failedTime;
	
	public SeedGeneratorServiceImpl() {
        String seedFile = Config.get("server.seed.file");
        
        if (seedFile != null) {
            RandomSeedSource fileGenerator = null;
            
            try {
                fileGenerator = new FileRandomSeedSource(seedFile);
                log.info("Using " + seedFile + " for seed generation");
            } catch (FileNotFoundException e) {
                log.warn("Seed File: " + seedFile + " not found\n"
                        + "Using default Java seed generator.", e);
                
                fileGenerator = new SecureRandomSeedSource();
            }
            
            seedGenerator = fileGenerator;
            
        } else {
            seedGenerator = new SecureRandomSeedSource();
        }
        
        fallbackSeedGenerator = new SecureRandomSeedSource();
        failedTime = 0;
    }
    
    @Override
    public byte[] getSeed(int bytes) {
        synchronized(seedGenerator) {
            try {
                return seedGenerator.getSeed(bytes);
            } catch (IOException e) {
                if (failedTime == 0) {
                    log.warn("Error while accessing RNG", e);
                }
                /* If the RNG failed retry every 30sec */
                long currTime = System.currentTimeMillis();
                if (currTime - failedTime >= 30000) {
                    failedTime = currTime;
                    try {
                        byte[] seed;
                        seedGenerator.reOpen();
                        seed = seedGenerator.getSeed(bytes);
                        failedTime = 0;
                        return seed;
                    } catch (Exception e1) { }
                }
                return fallbackSeedGenerator.getSeed(bytes);
            }
        }
    }

}
