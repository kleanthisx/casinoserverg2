package com.magneta.casino.games.server.xmlrpc.http;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.magneta.casino.games.server.xmlrpc.GameServerRequestConfig;

public class HttpXmlRpcRequestConfig extends GameServerRequestConfig {

	private final HttpServletRequest request;
	
	public HttpXmlRpcRequestConfig(HttpServletRequest request) {
		super();
		this.request = request;
	}
	
	@Override
	public HttpSession getSession(boolean create) {
		return request.getSession(create);
	}

	public HttpServletRequest getHttpServletRequest() {
		return request;
	}
	
	@Override
	public String getRemoteIp() {
		return request.getRemoteAddr();
	}
}
