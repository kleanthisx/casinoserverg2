package com.magneta.casino.games.server.xmlrpc.http;

import org.eclipse.jetty.util.log.Logger;
import org.slf4j.LoggerFactory;

class HttpGameServerLog implements org.eclipse.jetty.util.log.Logger {
    
	private final org.slf4j.Logger log;
	
	public HttpGameServerLog(org.slf4j.Logger log) {
		this.log = log;
	}
	
    @Override
    public void debug(String msg, Throwable exception) {
        log.debug("HTTP: " + msg, exception);
    }

    @Override
    public Logger getLogger(String name) {
        return new HttpGameServerLog(LoggerFactory.getLogger(name));
    }

    @Override
    public boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }

    @Override
    public void setDebugEnabled(boolean arg0) {
    	warn("setDebugEnabled not implemented",null,null);  
    }

    @Override
    public void warn(String msg, Throwable exception) {
        log.warn("HTTP: " + msg, exception);
    }

    @Override
    public String getName() {
    	return log.getName();
    }

    @Override
    public void warn(String msg, Object... args) {
        log.warn("HTTP: " + msg, args);
    }

    @Override
    public void warn(Throwable thrown) {
        log.warn("HTTP: ", thrown);
    }

    @Override
    public void info(String msg, Object... args) {
        log.info("HTTP: " + msg, args); 
    }

    @Override
    public void info(Throwable thrown) {
        log.info("HTTP: ", thrown);
    }

    @Override
    public void info(String msg, Throwable thrown) {
        log.info("HTTP: " + msg, thrown);
    }

    @Override
    public void debug(String msg, Object... args) {
        log.debug("HTTP: " + msg, args);  
    }

    @Override
    public void debug(Throwable thrown) {
        log.debug("HTTP: ", thrown);
    }

    @Override
    public void ignore(Throwable ignored) {
    }
    
    @Override
    public String toString() {
        return log.toString();
    }
}