package com.magneta.casino.games.server.jmx;

import java.lang.management.ManagementFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.apache.tapestry5.ioc.annotations.PostInjection;
import org.apache.tapestry5.ioc.services.RegistryShutdownHub;

import com.magneta.casino.games.server.mbeans.ServerStatus;

public class GameServerJmx {

	private MBeanServer mbs;
	private ObjectName name;
	
	public void setup(ServerStatus mbean) throws MalformedObjectNameException, NullPointerException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
	    	mbs = ManagementFactory.getPlatformMBeanServer();

	    	// Construct the ObjectName for the MBean we will register
	    	name = new ObjectName("com.magneta.casino.games.server:type=ServerStatus");

	    	// Register the Hello World MBean
	    	mbs.registerMBean(mbean, name);
	}
	
	@PostInjection
	public void startupService(RegistryShutdownHub shutdownHub) {
		shutdownHub.addRegistryWillShutdownListener(
			new Runnable() {
				@Override
				public void run() {
					destroy();
				}
			}
		);
	}
	
	public void destroy() {
		if (name == null) {
			return;
		}
		try {
			mbs.unregisterMBean(name);
		} catch (MBeanRegistrationException e) {
			
		} catch (InstanceNotFoundException e) {
		}
		name = null;
		mbs = null;
	}
}
