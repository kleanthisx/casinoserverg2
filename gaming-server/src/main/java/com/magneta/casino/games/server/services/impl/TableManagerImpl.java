/**
 * 
 */
package com.magneta.casino.games.server.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.random.SeedGeneratorService;
import com.magneta.casino.games.server.services.CurrentRoundService;
import com.magneta.casino.games.server.services.GameActionPersistanceService;
import com.magneta.casino.games.server.services.GameConfigService;
import com.magneta.casino.games.server.services.GameCurrentConfigurationService;
import com.magneta.casino.games.server.services.GameRoundPersistanceService;
import com.magneta.casino.games.server.services.GameRoundService;
import com.magneta.casino.games.server.services.GameServerContext;
import com.magneta.casino.games.server.services.GameSettingsService;
import com.magneta.casino.games.server.services.TableManager;
import com.magneta.casino.services.EffectiveGameSettingsService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.EffectiveGameSettingsBean;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameRoundListener;
import com.magneta.games.Table;
import com.magneta.games.configuration.GameConfigLoadingException;


/**
 * @author Nicos
 *
 */
public class TableManagerImpl implements TableManager {

	private static final Logger log = LoggerFactory.getLogger(TableManagerImpl.class);
	
	private final GameTableService gameTableService;
	private final EffectiveGameSettingsService gameSettingsService;
	private final GamesService gameService;
	private final GameServerContext gameServerContext;
	
	private final GameConfigService gameConfigService;
	private final GameCurrentConfigurationService gameCurrentConfigService;
	private final GameSettingsService realGameSettingsService;
	private final GameRoundService gameRoundService;
	private final GameRoundPersistanceService gameRoundPersistanceService;
	private final SeedGeneratorService seedGeneratorService;
	private final GameActionPersistanceService gameActionPersistanceService;
	private final CurrentRoundService currentRoundService;
	
    private Map<Long,Table> tables;
    
    public TableManagerImpl(GameTableService gameTableService,
    		EffectiveGameSettingsService gameSettingsService,
    		GamesService gameService,
    		GameServerContext gameServerContext,
    		GameConfigService gameConfigService,
    		GameCurrentConfigurationService gameCurrentConfigService,
			GameSettingsService realGameSettingsService,
			GameRoundService gameRoundService,
			GameRoundPersistanceService gameRoundPersistanceService,
			SeedGeneratorService seedGeneratorService,
			GameActionPersistanceService gameActionPersistanceService,
			CurrentRoundService currentRoundService) {
    	
    	this.gameTableService = gameTableService;
    	this.gameSettingsService = gameSettingsService;
    	this.gameService = gameService;
    	this.gameServerContext = gameServerContext;
    	
    	this.gameConfigService = gameConfigService;
    	this.gameCurrentConfigService = gameCurrentConfigService;
    	this.realGameSettingsService = realGameSettingsService;
    	this.gameRoundService = gameRoundService;
    	this.seedGeneratorService = seedGeneratorService;
    	this.gameActionPersistanceService = gameActionPersistanceService;
    	this.gameRoundPersistanceService = gameRoundPersistanceService;
    	this.currentRoundService = currentRoundService;
    	
    	tables = new ConcurrentHashMap<Long,Table>(50);
    }
    
    @Override
	public void removeTable(Table t) {
    	tables.remove(t.getTableId());
    	
    	/* If the table is closed mark it as closed in the database */
    	if (t.isClosed()) {
    		try {
				markTableClosed(t.getTableId());
			} catch (ServiceException e) {
				log.error("Unable to mark table " + t.getTableId() + " as closed", e);
			}
    	}

    	log.debug("Unloaded table: {}", t.getTableId());
    }

    /**
     * Returns the active table with the given ID.
     * @param tableID The ID of the table to reference.
     * @return The requested table or null if not found.
     */
    @Override
	public Table getActiveTable(long tableId) {
        return tables.get(tableId);
    }

    @Override
	public int getTablesCount() {
        return tables.size();
    }
    
    private Table loadTable(GamePlayer player, GameTableBean tableBean) throws ServiceException {    	
        Double minBet = null;
        Double maxBet = null;
        
        EffectiveGameSettingsBean gameSettings = this.gameSettingsService.getEffectiveGameSettings(player.getUserId(), player.getCorpId(), tableBean.getGameId());

        if (gameSettings != null) {
        	minBet = gameSettings.getMinBet();
        	maxBet = gameSettings.getMaxBet();
        }
        
        if (minBet == null || maxBet == null) {
        	throw new ServiceException("Invalid game configuration. Min/Max bet not set");
        }
        
        GameBean game = gameService.getGame(tableBean.getGameId());
        if (game == null) {
        	throw new ServiceException("Invalid gameId");
        }
        
        
        return new Table(game, player, tableBean.getTableId(), minBet, maxBet);
    }
    
    private Table createTable(GamePlayer player, Integer gameId) throws ServiceException {
        GameBean game = gameService.getGame(gameId);
        
        if (game == null) {
        	throw new EntityNotFoundException("Game not found");
        }
    	
    	Double minBet = null;
    	Double maxBet = null;

    	EffectiveGameSettingsBean gameSettings = this.gameSettingsService.getEffectiveGameSettings(player.getUserId(), player.getCorpId(), gameId);

        if (gameSettings != null) {
        	minBet = gameSettings.getMinBet();
        	maxBet = gameSettings.getMaxBet();
        }
    	
        if (minBet == null || maxBet == null) {
        	throw new ServiceException("Game configuration error");
        }
        
        long tableId;
        
    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new DatabaseException("Out of database connections");
    	}
    	    	
    	try {
    		dbConn.setAutoCommit(false);
    		
    		PreparedStatement tableStatement = null;
            ResultSet tableRes = null;
            
            try {
            	String sql = "INSERT INTO game_tables(game_type_id, table_owner, server_id)" + 
            				 " VALUES (?, ?, ?) RETURNING table_id";

            	tableStatement = dbConn.prepareStatement(sql);

                tableStatement.setInt(1, gameId);
                tableStatement.setLong(2, player.getUserId());
                tableStatement.setInt(3, gameServerContext.getServerId());
            	
                tableRes = tableStatement.executeQuery();

                if (tableRes.next()) {
                    tableId = tableRes.getLong(1);
                } else {
                	throw new DatabaseException("Unable to create table");
                }
            	
            } finally {
            	DbUtil.close(tableRes);
            	DbUtil.close(tableStatement);
            }
            
            PreparedStatement lastRoundStatement = null;
            try {
            	String sql = "INSERT INTO game_table_last_rounds(table_id, last_round_id) VALUES(?, NULL)";
            	
            	lastRoundStatement = dbConn.prepareStatement(sql);
            	lastRoundStatement.setLong(1, tableId);
            	
            	lastRoundStatement.executeUpdate();
            	
            } finally {
            	DbUtil.close(lastRoundStatement);
            }
    		
            PreparedStatement tableUsersStatement = null;
            try {
            	String sql = "INSERT INTO game_table_users(table_id, user_id, is_active)" +
           		     " VALUES (?, ?, ?)";
            	
            	tableUsersStatement = dbConn.prepareStatement(sql);
            	tableUsersStatement.setLong(1, tableId);
            	tableUsersStatement.setLong(2, player.getUserId());
            	tableUsersStatement.setBoolean(3, false);
            	
            	tableUsersStatement.executeUpdate();
            	
            } finally {
            	DbUtil.close(tableUsersStatement);
            }
    		
    		dbConn.commit();
    	} catch (SQLException e) {
    		try {
				dbConn.rollback();
			} catch (SQLException e1) {
				log.warn("Transaction rollback failed", e1);
			}
			throw new DatabaseException("Unable to create table", e);
		} finally {
    		DbUtil.close(dbConn);
    	}
    	
    	return new Table(game, player, tableId, minBet,
                maxBet);
    }
    
    /**
     * Sets whether the player is currently viewing the table.
     * @throws ServiceException 
     */
    @Override
    public void markViewerActive(Table table, GamePlayer player, boolean active) throws ServiceException {
    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new DatabaseException("Unable to join table", new RuntimeException("Out of database connections"));
    	}

    	PreparedStatement statement = null;

    	try {

    		String sql = 
    				"UPDATE game_table_users" +
    						" SET is_active = ?" +
    						" WHERE user_id = ?" +
    						" AND table_id = ?";

    		statement = dbConn.prepareStatement(sql);
    		statement.setBoolean(1, active);
    		statement.setLong(2, player.getUserId());
    		statement.setLong(3, table.getTableId());
    		statement.executeUpdate();

    	} catch (SQLException e) {
    		throw new DatabaseException("Unable to join table", e);
    	} finally {
    		DbUtil.close(statement);
    		DbUtil.close(dbConn);
    	}
    }
    
    private Integer getTableCurrentRoundId(Long tableId) throws ServiceException {
    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new DatabaseException("Out of database connections");
    	}
    	
    	PreparedStatement stmt = null;
    	ResultSet rs = null;
    	try {
    		stmt = dbConn.prepareStatement("SELECT last_round_id FROM game_table_last_rounds WHERE table_id=?");
    		stmt.setLong(1, tableId);
    		
    		rs = stmt.executeQuery();
    		
    		if (rs.next()) {
    			int roundId = rs.getInt("last_round_id");
    			if (rs.wasNull()) {
    				return null;
    			}
    			return roundId;
    		}
    		
    		return null;
    		
    	} catch (SQLException e) {
			throw new DatabaseException("Database error while retrieving current round", e);
		} finally {
    		DbUtil.close(rs);
    		DbUtil.close(stmt);
    		DbUtil.close(dbConn);
    	}
    }
    
    @Override
    public void startTable(Table table, GameRoundListener roundListener) throws ServiceException {
    	if (table.isStarted()) {
    		throw new ServiceException("Table already started");
    	}

    	/* Resume table */
    	try {
    		Integer roundId = this.getTableCurrentRoundId(table.getTableId());

    		if (roundId == null) {
    			log.debug("Starting table {}", table.getTableId());
    			table.startTable(roundListener, gameConfigService, realGameSettingsService, gameRoundService, gameRoundPersistanceService, seedGeneratorService, gameCurrentConfigService, currentRoundService);
    		} else {
    			log.debug("Resuming table {}", table.getTableId());
    			table.resumeTable(roundId, roundListener, realGameSettingsService, gameConfigService, gameCurrentConfigService, gameRoundService, gameRoundPersistanceService, gameActionPersistanceService, currentRoundService);
    		}
    	} catch (GameConfigLoadingException e) {
    		log.error("Table {}: Error while loading round", table.getTableId(), e);
    		throw new ServiceException("Invalid game configuration", e);
    	} catch (ActionFailedException e) {
    		log.error("Table {}: Error while loading round", table.getTableId(), e);
    		throw new ServiceException("Unable to replay round", e);
    	} catch (Throwable e) {
    		log.error("Table {}: Error while loading round", table.getTableId(), e);
    		throw new ServiceException("Internal server error", e);
    	}
    }
    
    /**
     * Gets a table for the player to start playing the given game.
     * @param player The player
     * @param game The game
     * @return
     * 
     * @throws ServiceException 
     */
    @Override
	public Table getTable(GamePlayer player, GameBean game) throws ServiceException {
        GameTableBean tableBean = gameTableService.getActiveTable(player.getUserId(), game.getGameId());

        Table table;
        
        /* Player does not have a table.
         * Create one now.
         */
        if (tableBean == null) {
        	table = createTable(player, game.getGameId());
        	log.debug("Created table {}", table.getTableId());
        } else {
        	if (gameServerContext.getServerId() != tableBean.getServerId()) {
            	throw new ServiceException("Table is on another server");
        	}

            table = loadTable(player, tableBean);
            log.debug("Loaded table: {}", table.getTableId());
        }

		tables.put(table.getTableId(), table);
        return table;
    }

    /**
     * Closes a table on the database.
     * @param tableId The id of the table.
     * @return 
     * @throws ServiceException 
     */
    private void markTableClosed(long tableId) throws ServiceException {
        Connection dbConn = ConnectionFactory.getConnection();
        if (dbConn == null) {
            throw new DatabaseException("Out of database connections");
        }
        
        PreparedStatement statement = null;
        try {
            String sql = "UPDATE game_tables SET date_closed = now_utc()" +
            		     " WHERE table_id = ? AND date_closed IS NULL";

            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, tableId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException("Error closing table.", e);
        } finally {
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
    }

    @Override
    public void closeTable(long tableId) throws ServiceException  {
    	Table table = tables.get(tableId);

    	if (table != null) {
    		table.setClosed();
    	} else {
    		GameTableBean tableBean = gameTableService.getTable(tableId);
    		
    		if (tableBean == null) {
    			throw new ServiceException("Table not found");
    		}
    		
    		if (tableBean.getDateClosed() != null) {
    			throw new ServiceException("Table already closed");
    		}
    		
    		if (gameServerContext.getServerId() != tableBean.getServerId()) {
    			throw new ServiceException("Table is on another server");
    		}
    		
    		markTableClosed(tableId);
    	}
    }
}
