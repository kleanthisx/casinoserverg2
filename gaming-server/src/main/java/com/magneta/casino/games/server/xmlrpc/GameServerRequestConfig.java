package com.magneta.casino.games.server.xmlrpc;

import javax.servlet.http.HttpSession;

import com.magneta.xmlrpc.server.impl.SocketXmlRpcRequestConfigImpl;

public abstract class GameServerRequestConfig extends SocketXmlRpcRequestConfigImpl {

    public abstract HttpSession getSession(boolean create);
    public abstract String getRemoteIp();
}
