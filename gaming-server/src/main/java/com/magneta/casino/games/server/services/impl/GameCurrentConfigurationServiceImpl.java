package com.magneta.casino.games.server.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.casino.games.server.services.GameCurrentConfigurationService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.CachedConfiguration;

public class GameCurrentConfigurationServiceImpl implements GameCurrentConfigurationService {

	private static final Logger log = LoggerFactory.getLogger(GameCurrentConfigurationServiceImpl.class);

	private final Map<Integer, CachedConfiguration> configs; 
	private final int refreshTime;

	public GameCurrentConfigurationServiceImpl() {
		configs = new ConcurrentHashMap<Integer, CachedConfiguration>();
		refreshTime = Config.getInt("system.config_refresh_time", 5 * 60 * 1000);
	}

	@Override
	public Long getLatestConfigId(int gameId) throws ServiceException {
		CachedConfiguration config = getLatestConfiguration(gameId);
		if (config == null)
			return null;

		return config.getConfigId();
	}

	/**
	 * Returns the latest cached configuration for the specified game. If none exists null is returned.
	 * @throws ServiceException 
	 */
	private CachedConfiguration getLatestConfiguration(int gameId) throws ServiceException {
		CachedConfiguration cachedConfig = configs.get(gameId);

		if (cachedConfig != null && ((System.currentTimeMillis() - cachedConfig.getTimeUpdated()) < refreshTime) ) {
			return cachedConfig;
		}
		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		PreparedStatement stmt = null;
		ResultSet res = null;

		try {
			stmt = dbConn.prepareStatement(
					" SELECT config_id FROM game_current_configs WHERE game_id = ?");
			stmt.setInt(1, gameId);

			res = stmt.executeQuery();

			CachedConfiguration currentConfig;
			if (res.next()) {
				currentConfig = new CachedConfiguration(res.getLong("config_id"), System.currentTimeMillis());
			} else {
				currentConfig = new CachedConfiguration(null, System.currentTimeMillis());
			}

			if (cachedConfig == null) {
				log.info("Game {}: current configuration {}", gameId, currentConfig.getConfigId());
			} else if (cachedConfig.getConfigId() != currentConfig.getConfigId()) {
				log.info("Game {}: configuration switched from {} to {}", new Object[] {gameId, cachedConfig.getConfigId(), currentConfig.getConfigId()});
			}
			configs.put(gameId, currentConfig);
			return currentConfig;
		} catch (SQLException e){
			throw new ServiceException("Error getting latest configuration for game.", e);
		} finally{
			DbUtil.close(res);
			DbUtil.close(stmt);
			DbUtil.close(dbConn);
		}
	}
}
