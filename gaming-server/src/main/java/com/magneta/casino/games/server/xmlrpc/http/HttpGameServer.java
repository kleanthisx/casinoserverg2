/**
 * 
 */
package com.magneta.casino.games.server.xmlrpc.http;

import javax.net.ssl.SSLContext;

import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.slf4j.LoggerFactory;

import com.magneta.games.server.ServerTask;
import com.magneta.xmlrpc.server.XmlRpcSocketStreamServer;

/**
 * @author anarxia
 *
 */
public class HttpGameServer implements ServerTask {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(HttpGameServer.class);

    private final Server server;
    private final String name;
    
    public HttpGameServer(String name, XmlRpcSocketStreamServer xmlrpcServer, SessionManager sessionManager,
    		String address, int port, SSLContext sslContext)  {
        
        org.eclipse.jetty.util.log.Log.setLog(new HttpGameServerLog(log));
        this.name = name;
        
        ConnectionFactory[] connectionFactories;
        
        if (sslContext == null) {
        	connectionFactories = new ConnectionFactory[1];
        } else {
        	connectionFactories = new ConnectionFactory[2];
        }
        
        server = new Server();
        
        HttpConfiguration httpConfig = new HttpConfiguration();
        
        httpConfig.setSendServerVersion(false);
    	connectionFactories[0] = new HttpConnectionFactory(httpConfig);
        
        if (sslContext != null) {
        	SslContextFactory sslContextFactory = new GameServerJettySslContextFactory(sslContext);
        	try {
                //sslContextFactory.setAllowRenegotiate(true);
				sslContextFactory.start();
			} catch (Exception e) {
				throw new RuntimeException("Unable to start SSL Context", e);
			}

        	connectionFactories[1] = 
        			new SslConnectionFactory(sslContextFactory, HttpVersion.HTTP_1_1.asString());
        }
        
        ServerConnector connector = new ServerConnector(server, connectionFactories);
        connector.setPort(port);
        if (address != null) {
            connector.setHost(address);
        }
        
        server.setConnectors(new Connector[] {connector});
      
        /* Initialize the servlet handler */
        ServletHandler handler = new ServletHandler();
        
        handler.addServletWithMapping(new ServletHolder(new HttpXmlRpcServlet(false, xmlrpcServer)), "/*");
        
        server.setHandler(new ServletContextHandler(null, "",
                            new SessionHandler(sessionManager),
                            null,
                            handler,
                            new ErrorPageErrorHandler()));
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void start() {
        
        try {
            server.start();
        } catch (Exception e) {
            log.info("HTTP: Exception", e);
        }
    }
    
    @Override
    public void join() throws InterruptedException {
    	server.join();
    }

    @Override
    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            log.debug("HTTP: Exception", e);
        }
    }

	@Override
	public String getName() {
		return name;
	}
}
