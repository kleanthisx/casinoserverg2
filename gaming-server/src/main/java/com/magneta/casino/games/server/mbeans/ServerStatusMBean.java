package com.magneta.casino.games.server.mbeans;

public interface ServerStatusMBean {

	int getNumberOfUsers();
	int getNumberOfTables();
	int getUptime();
	
	void stop();
	void disconnectUser(long userId);
	void reloadConfig();
}
