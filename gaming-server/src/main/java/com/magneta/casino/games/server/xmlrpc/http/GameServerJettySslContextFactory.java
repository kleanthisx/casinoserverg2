package com.magneta.casino.games.server.xmlrpc.http;

import javax.net.ssl.SSLContext;

import org.eclipse.jetty.util.ssl.SslContextFactory;

public class GameServerJettySslContextFactory extends SslContextFactory {
	
	public GameServerJettySslContextFactory(SSLContext sslContext) {
		super();
		setSslContext(sslContext);
	}
	
	@Override
	public void checkKeyStore() {
		if (getSslContext() == null) {
			super.checkKeyStore();
		}
	}
}
