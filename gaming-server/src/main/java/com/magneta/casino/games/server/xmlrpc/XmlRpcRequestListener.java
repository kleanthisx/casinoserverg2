package com.magneta.casino.games.server.xmlrpc;

import java.util.Map;
import java.util.Set;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.common.XmlRpcInvocationException;
import org.apache.xmlrpc.server.XmlRpcNoSuchHandlerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestService;
import com.magneta.casino.games.server.services.ServiceLocator;
import com.magneta.games.GamePlayer;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestListener;

public class XmlRpcRequestListener implements SocketXmlRpcRequestListener {
	
	private static final Logger log = LoggerFactory.getLogger(XmlRpcRequestListener.class);

	private static final boolean logRequests = Config.getBoolean("server.request.log", false);
	
	private final GameServerRequestService requestService;
	private final GameServerPrincipalService principalService;
	
	public XmlRpcRequestListener(GameServerRequestService requestService,
			GameServerPrincipalService principalService) {
		 this.requestService = requestService;
		 this.principalService = principalService;
	}
	
    private static final char[] hexChar = {'0','1','2','3','4','5','6','7','8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    
    private static void appendHexString(StringBuilder buff, byte[] bytes) {
                
        for (int i = 0; i < bytes.length; i++) {
            buff.append(hexChar[(bytes[i] >> 4) & 0x0F]);
            buff.append(hexChar[(bytes[i] & 0x0F)]);
        }
    }
	
	private void appendParameter(StringBuilder sb, Object param) {
		if (param instanceof byte[]) {
			byte[] params = (byte[])param;
			
			sb.append("(");
			
			appendHexString(sb, params);
			sb.append(")");
			
		} else if (param instanceof Object[]) {
			Object[] params = (Object[])param;
			
			sb.append("[");
			
			boolean first = true;
			for (Object p: params) {
				if (first) {
					first = false;
				} else {
					sb.append(", ");
				}
				
				appendParameter(sb, p);
			}
			
			sb.append("]");
			
		} else if (param instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, ?> m = (Map<String, ?>)param;
			
			sb.append("{");
			
			Set<String> keys = m.keySet();
			
			boolean first = true;
			for (String key : keys) {
				if (first) {
					first = false;
				} else {
					sb.append(", ");
				}
				
				sb.append(key);
				sb.append(": ");
				appendParameter(sb, m.get(key));
				
			}
			
			sb.append("}");
		} else if (param instanceof String) {	
			sb.append("\"");
			sb.append(param);
			sb.append("\"");
		} else {
			sb.append(param);
		}
	}
	
	private void appendRequest(XmlRpcRequest request, StringBuilder sb) {
		sb.append("Request: ")
		.append(request.getMethodName())
		.append('(');
		
		for (int i=0; i < request.getParameterCount(); i++) {				
			if (i > 0) {
				sb.append(", ");
			}
			appendParameter(sb, request.getParameter(i));
		}	
		
		sb.append(')');
	}
	
	@Override
	public void beforeExecute(XmlRpcRequest request) {
		if (logRequests) {
			StringBuilder sb = new StringBuilder();
			appendRequest(request, sb);
			log.info(sb.toString());
		}
		
		principalService.resetSystem();
		principalService.setPrincipal(null);
		
		requestService.startRequest(request);
		
		GameServerRequest serverRequest = requestService.getRequest();
		
        GamePlayer player = serverRequest.getPlayer();
        principalService.setPrincipal(player);
	}

	@Override
	public void afterExecute(XmlRpcRequest request, Object result,
			Throwable ex) {
		principalService.resetSystem();
		principalService.setPrincipal(null);
		requestService.endRequest();
		ServiceLocator.getRegistry().cleanupThread();
		
		if (ex != null) {
			if (!(ex instanceof XmlRpcException)) {
				StringBuilder sb = new StringBuilder();
				appendRequest(request, sb);
				log.error("Error on request: " + sb.toString() + "\n", ex);
			} else if (ex instanceof XmlRpcNoSuchHandlerException) {
				log.warn("Method not implemented: {}", request.getMethodName());
			} else if (ex instanceof XmlRpcInvocationException || ((XmlRpcException)ex).code == 500) {
				StringBuilder sb = new StringBuilder();
				appendRequest(request, sb);
				log.error("Error on request: " + sb.toString() + "\n", ex.getCause());
			}
		}
	}

}
