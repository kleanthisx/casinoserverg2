/**
 * 
 */
package com.magneta.casino.games.random.impl;

import java.io.FileNotFoundException;
import java.security.SecureRandom;

import com.magneta.casino.games.random.RandomSeedSource;


/**
 * @author anarxia
 *
 */
public class SecureRandomSeedSource implements RandomSeedSource {
    
    private final SecureRandom rand;
    
    public SecureRandomSeedSource() {
        rand = new SecureRandom();
    }
    
    @Override
    public byte[] getSeed(int bytes) {
        return rand.generateSeed(bytes);
    }

    @Override
    public void reOpen() throws FileNotFoundException {
    }
}
