/**
 * 
 */
package com.magneta.casino.games.server.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.ResourceBundle;

import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Startup;
import org.apache.tapestry5.ioc.services.PropertyShadowBuilder;
import org.apache.tapestry5.ioc.services.RegistryShutdownHub;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;
import org.reflections.Configuration;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.magneta.casino.common.services.CorporateGameSettingsServiceImpl;
import com.magneta.casino.common.services.CorporateSystemSettingsServiceImpl;
import com.magneta.casino.common.services.CorporatesServiceImpl;
import com.magneta.casino.common.services.CountryServiceImpl;
import com.magneta.casino.common.services.CurrencyParserImpl;
import com.magneta.casino.common.services.CurrencyServiceImpl;
import com.magneta.casino.common.services.DatabaseServiceImpl;
import com.magneta.casino.common.services.EffectiveGameSettingsServiceImpl;
import com.magneta.casino.common.services.GameClientServiceImpl;
import com.magneta.casino.common.services.GameConfigsServiceImpl;
import com.magneta.casino.common.services.GameTableServiceImpl;
import com.magneta.casino.common.services.GamesHistoryServiceImpl;
import com.magneta.casino.common.services.GamesServiceImpl;
import com.magneta.casino.common.services.JackpotsServiceImpl;
import com.magneta.casino.common.services.SettingsServiceImpl;
import com.magneta.casino.common.services.UserAccessServiceImpl;
import com.magneta.casino.common.services.UserAuthenticationServiceImpl;
import com.magneta.casino.common.services.UserDepositsServiceImpl;
import com.magneta.casino.common.services.UserDetailsServiceImpl;
import com.magneta.casino.common.services.UserWithdrawalsServiceImpl;
import com.magneta.casino.common.services.UsernameServiceImpl;
import com.magneta.casino.games.random.SeedGeneratorService;
import com.magneta.casino.games.random.impl.SeedGeneratorServiceImpl;
import com.magneta.casino.games.server.jmx.GameServerJmx;
import com.magneta.casino.games.server.mbeans.ServerStatus;
import com.magneta.casino.games.server.services.impl.CurrentRoundServiceImpl;
import com.magneta.casino.games.server.services.impl.GameActionPersistanceServiceImpl;
import com.magneta.casino.games.server.services.impl.GameCurrentConfigurationServiceImpl;
import com.magneta.casino.games.server.services.impl.GameRoundPersistanceServiceImpl;
import com.magneta.casino.games.server.services.impl.GameRoundServiceImpl;
import com.magneta.casino.games.server.services.impl.GameServerContextImpl;
import com.magneta.casino.games.server.services.impl.GameServerLoginServiceImpl;
import com.magneta.casino.games.server.services.impl.GameServerRequestServiceImpl;
import com.magneta.casino.games.server.services.impl.GameServerRequestValidationServiceImpl;
import com.magneta.casino.games.server.services.impl.GameServerTaskManagerImpl;
import com.magneta.casino.games.server.services.impl.GameServerUserDisconnectServiceImpl;
import com.magneta.casino.games.server.services.impl.GameSettingsServiceImpl;
import com.magneta.casino.games.server.services.impl.PrincipalServiceImpl;
import com.magneta.casino.games.server.services.impl.TableManagerImpl;
import com.magneta.casino.games.server.services.impl.XmlRpcHandlerMappingFactoryImpl;
import com.magneta.casino.games.server.session.SessionIdGenerator;
import com.magneta.casino.games.server.session.impl.SessionIdGeneratorImpl;
import com.magneta.casino.games.server.xmlrpc.GameServerRequestFactoryFactory;
import com.magneta.casino.games.server.xmlrpc.XmlRpcRequestListener;
import com.magneta.casino.games.server.xmlrpc.async.AsyncExecutorImpl;
import com.magneta.casino.games.templates.LocalConfigurationBuilder;
import com.magneta.casino.internal.services.DatabaseService;
import com.magneta.casino.reports.ReportUtil;
import com.magneta.casino.services.CorporateGameSettingsService;
import com.magneta.casino.services.CorporateSystemSettingsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.EffectiveGameSettingsService;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.GamesHistoryService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.JackpotsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.UserDepositsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserWithdrawalsService;
import com.magneta.casino.services.UsernameService;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.JackpotUpdater;
import com.magneta.games.server.LogoutDaemon;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestListener;

/**
 * @author anarxia
 *
 */
public class GameServerAppModule {

	private static final Logger log = LoggerFactory.getLogger(GameServerAppModule.class);
	
    private static void updateServerFlag(int serverId, boolean flag) throws ServiceException {
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            throw new ServiceException("Out of database connections");
        }
        
        PreparedStatement stmt = null;

        try {
            String sql=
                "UPDATE servers SET server_running = ? WHERE server_id = ?";

            stmt = conn.prepareStatement(sql);

            stmt.setBoolean(1, flag);
            stmt.setInt(2, serverId);

            stmt.executeUpdate();

        } catch (SQLException e) {
        	throw new ServiceException("Error updating server's running flag.", e);
        } finally {       
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
    }
    
    public static class GameServerShutdownListener implements Runnable {

    	private final int serverId;
    	
    	public GameServerShutdownListener(int serverId) {
    		this.serverId = serverId;
    	}

		@Override
		public void run() {
			try {
				updateServerFlag(serverId, false);
			} catch (ServiceException e) {
				log.error("Unable to mark server stopped in database.", e);
			}
		}
    }
	
    @Startup
    public static void onStartup(RegistryShutdownHub shutdownHub,
    		GameServerTaskManager taskManager,
    		SettingsService settingsService,
    		CurrencyParser currencyParser,
    		UsernameService usernameService,
    		GameServerContext serverContext,
    		GameServerJmx jmx,
    		ServerStatus mbean,
    		AsyncExecutorImpl asyncRequestExecutor) throws ServiceException {

    	ReportUtil.init(currencyParser, usernameService);
    	
    	log.info("Daemon startup.");
    	
    	try {
    		jmx.setup(mbean);
    	} catch (Throwable e) {
    		log.error("Unable to setup JMX", e);
    	}

    	taskManager.addTask("Jackpot Updater", JackpotUpdater.getInstance());
    	taskManager.addTask("Logout Daemon", LogoutDaemon.getInstance());
    	taskManager.addTask(asyncRequestExecutor);

    	updateServerFlag(serverContext.getServerId(), true);
    	shutdownHub.addRegistryShutdownListener(new GameServerShutdownListener(serverContext.getServerId()));
    }
	
	/**
	 * Binds all services to use
	 * @param binder
	 */
	public static void bind(ServiceBinder binder) {
		binder.bind(CorporatesService.class, CorporatesServiceImpl.class);
		binder.bind(CorporateGameSettingsService.class, CorporateGameSettingsServiceImpl.class);
		binder.bind(CorporateSystemSettingsService.class, CorporateSystemSettingsServiceImpl.class);
		binder.bind(CountryService.class, CountryServiceImpl.class);
		binder.bind(CurrencyService.class, CurrencyServiceImpl.class);
		binder.bind(CurrencyParser.class, CurrencyParserImpl.class);
		binder.bind(DatabaseService.class, DatabaseServiceImpl.class);
		binder.bind(EffectiveGameSettingsService.class, EffectiveGameSettingsServiceImpl.class);
		binder.bind(GameClientService.class, GameClientServiceImpl.class);
		binder.bind(GamesService.class, GamesServiceImpl.class);
		binder.bind(GameConfigsService.class, GameConfigsServiceImpl.class);
		binder.bind(GameTableService.class, GameTableServiceImpl.class);
		binder.bind(SettingsService.class, SettingsServiceImpl.class);
		binder.bind(UserAccessService.class, UserAccessServiceImpl.class);
		binder.bind(UserAuthenticationService.class, UserAuthenticationServiceImpl.class);
		binder.bind(UserDepositsService.class, UserDepositsServiceImpl.class);
		binder.bind(UserDetailsService.class, UserDetailsServiceImpl.class);
		binder.bind(UsernameService.class, UsernameServiceImpl.class);
		binder.bind(UserWithdrawalsService.class, UserWithdrawalsServiceImpl.class);

		binder.bind(CurrentRoundService.class, CurrentRoundServiceImpl.class);
		binder.bind(GameActionPersistanceService.class, GameActionPersistanceServiceImpl.class);
		binder.bind(GameCurrentConfigurationService.class, GameCurrentConfigurationServiceImpl.class);
		binder.bind(GameConfigService.class);
		binder.bind(GamesHistoryService.class, GamesHistoryServiceImpl.class);
		binder.bind(GameRoundService.class, GameRoundServiceImpl.class).eagerLoad();
		binder.bind(GameRoundPersistanceService.class, GameRoundPersistanceServiceImpl.class);
		binder.bind(GameServerContext.class, GameServerContextImpl.class);
		binder.bind(GameServerLoginService.class, GameServerLoginServiceImpl.class);
		binder.bind(GameServerPrincipalService.class, PrincipalServiceImpl.class);
		binder.bind(GameServerRequestService.class, GameServerRequestServiceImpl.class);
		binder.bind(GameServerRequestValidationService.class, GameServerRequestValidationServiceImpl.class);
		binder.bind(GameServerUserDisconnectService.class, GameServerUserDisconnectServiceImpl.class);
		binder.bind(GameSettingsService.class, GameSettingsServiceImpl.class);
		binder.bind(SeedGeneratorService.class, SeedGeneratorServiceImpl.class);
		binder.bind(SessionIdGenerator.class, SessionIdGeneratorImpl.class);
		binder.bind(TableManager.class, TableManagerImpl.class);
		binder.bind(JackpotsService.class,JackpotsServiceImpl.class);
		
		binder.bind(AsyncExecutorImpl.class);
		binder.bind(SocketXmlRpcRequestListener.class, XmlRpcRequestListener.class);
		binder.bind(XmlRpcHandlerMappingFactory.class, XmlRpcHandlerMappingFactoryImpl.class);
		binder.bind(RequestProcessorFactoryFactory.class, GameServerRequestFactoryFactory.class);
		binder.bind(GameServerTaskManager.class, GameServerTaskManagerImpl.class);
		binder.bind(GameServerJmx.class);
		binder.bind(ServerStatus.class);
	}

	public GameServerRequest buildGameServerRequest(@Inject GameServerRequestService requestService,
			@Inject PropertyShadowBuilder shadowBuilder) {
		return shadowBuilder.build(requestService, "request", GameServerRequest.class);
	}
	
	private static Reflections reflections;
	private static final String HANDLERS_PACKAGE_PREFIX = "com.magneta.casino.games.server.xmlrpc.handlers";
	
	public static Reflections buildReflections() {
		if (reflections == null) {
			Configuration config = new LocalConfigurationBuilder() {
				{
					String prefix = HANDLERS_PACKAGE_PREFIX;
					Predicate<String> filter = new FilterBuilder.Include(FilterBuilder.prefix(prefix));

					filterInputsBy(filter);
					setUrls(ClasspathHelper.forPackage(prefix));

					setScanners(
							new TypeAnnotationsScanner(),
							new SubTypesScanner());
				}
			}; 

			reflections = new Reflections(config);
		}

		return reflections;
	}
		
	public static void contributeApplicationDefaults(
			MappedConfiguration<String, String> configuration) {

		configuration.add(SymbolConstants.GAME_CLASS_PACKAGE, "com.magneta.games");
		
		ResourceBundle config = ResourceBundle.getBundle("server");
		Enumeration<String> keys = config.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			configuration.add(key, config.getString(key));
		}
	}
}
