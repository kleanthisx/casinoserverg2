/**
 * 
 */
package com.magneta.casino.games.random;

import java.io.FileNotFoundException;
import java.io.IOException;


/**
 * @author anarxia
 *
 */
public interface RandomSeedSource {
    public byte[] getSeed(int bytes) throws IOException;
    public void reOpen() throws FileNotFoundException;
}
