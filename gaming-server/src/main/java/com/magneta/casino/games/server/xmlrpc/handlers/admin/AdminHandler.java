/**
 * 
 */
package com.magneta.casino.games.server.xmlrpc.handlers.admin;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.xmlrpc.XmlRpcException;

import com.magneta.Config;
import com.magneta.casino.games.server.services.GameServerContext;
import com.magneta.casino.games.server.services.GameServerLoginService;
import com.magneta.casino.games.server.services.GameServerRequestService;
import com.magneta.casino.games.server.services.GameServerUserDisconnectService;
import com.magneta.casino.games.server.services.TableManager;
import com.magneta.casino.games.server.xmlrpc.annotations.XmlRpcHandler;
import com.magneta.casino.services.ServiceException;

/**
 * @author Nicos
 *
 */
@XmlRpcHandler(context="admin", name="Admin")
public class AdminHandler {
	
	@Inject
	private GameServerLoginService loginService;
	
	@Inject
	private GameServerUserDisconnectService disconnectService;
	
	@Inject
	private TableManager tableManager;
	
	@Inject
	private GameServerContext gameServerContext;
	
	@Inject
	private GameServerRequestService requestService;

    private void validatePassword(String password) throws XmlRpcException {

    	if (password == null) {
    		throw new XmlRpcException("Invalid admin password");
    	} 
    	if (!password.equals(Config.get("server.admin.password", null))) {
    		throw new XmlRpcException("Invalid admin password");
    	}
    }

    public boolean closeTable(String password, long tableId, int serverId) throws XmlRpcException, ServiceException {

        validatePassword(password);

        if (serverId != gameServerContext.getServerId()) {
            return false;
        }
        
        tableManager.closeTable(tableId);
        
        return true;
    }

    public boolean suspendServer(String password, String reason) throws XmlRpcException {
        validatePassword(password);
        
        gameServerContext.suspend();
        return true;
    }

    public boolean stopServer(String password) throws XmlRpcException {
        validatePassword(password);
        
        requestService.getRequest().getRequestConfig().setServerShutdown();
        return true;
    }
    
    public boolean banUser(String password, long userId) throws XmlRpcException {
        validatePassword(password);
 
        disconnectService.markForDisconnect(userId);
        return true;
    }
    
    public Map<String,Object> getSystemStatus(String password) throws XmlRpcException {
        validatePassword(password);
        
        Map<String, Object> status = new HashMap<String, Object>();
        status.put("active_users", loginService.getLoggedInUsersCount());
        status.put("active_tables", tableManager.getTablesCount());
        
    	RuntimeMXBean mx = ManagementFactory.getRuntimeMXBean();
    	int uptime = (int)(mx.getUptime() / 1000l);
        status.put("uptime", uptime);
        
        OperatingSystemMXBean mxBean = ManagementFactory.getOperatingSystemMXBean();
        status.put("load_avg", mxBean.getSystemLoadAverage());
        status.put("cpus", mxBean.getAvailableProcessors());
        
        return status;
    }
}

