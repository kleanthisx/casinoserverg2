package com.magneta.casino.games.server.services;

public interface GameServerUserDisconnectService {

	public void markForDisconnect(Long userId);
	
	public boolean shouldDisconnect(Long userId);
}
