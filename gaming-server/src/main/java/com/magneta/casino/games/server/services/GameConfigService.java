/**
 * 
 */
package com.magneta.casino.games.server.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.configuration.GameConfigLoadingException;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.GameConfigurationFactory;


/**
 * @author anarxia
 *
 */
public class GameConfigService {
    
    private Map<Long, GameConfiguration> gameConfigCache;
    private Map<String, GameConfiguration> defaultGameConfigs;
    
    public GameConfigService() {
        this.gameConfigCache = new HashMap<Long, GameConfiguration>();
        this.defaultGameConfigs = new HashMap<String, GameConfiguration>();
    }
    
    private GameConfiguration loadConfig(String gameTemplate, long configId) throws GameConfigLoadingException {
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            throw new GameConfigLoadingException("Out of database connections while loading game config");
        }
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement("SELECT config_value FROM game_configs WHERE config_id=?");
            
            stmt.setLong(1, configId);
            
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                return GameConfigurationFactory.loadConfiguration(gameTemplate, configId, rs.getString("config_value"));
            }
            
        } catch (SQLException e) {
            throw new GameConfigLoadingException("Database error while loading game config", e);
        } finally {
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
        
        throw new GameConfigLoadingException("Unable to find game config");
    }
    
    private GameConfiguration getDefaultConfiguration(String template) throws GameConfigLoadingException {
        GameConfiguration conf = defaultGameConfigs.get(template);
        if (conf != null) {
            return conf;
        }
        
        synchronized(defaultGameConfigs) {
            conf = defaultGameConfigs.get(template);
            if (conf != null) {
                return conf;
            }
            
            conf = GameConfigurationFactory.loadConfiguration(template);
            if (conf != null) {
                defaultGameConfigs.put(template, conf);
            }
        }
        
        return conf;
    }
   
    public GameConfiguration getConfiguration(String template, Long configId) throws GameConfigLoadingException {
        if (configId == null) {
            return getDefaultConfiguration(template);
        }

        GameConfiguration conf = gameConfigCache.get(configId);
        if (conf != null) {
            return conf;
        }

        synchronized(gameConfigCache) {
            conf = gameConfigCache.get(configId);
            if (conf != null) {
                return conf;
            }

            conf = loadConfig(template, configId);

            gameConfigCache.put(configId, conf);
            return conf;
        }
    }
}
