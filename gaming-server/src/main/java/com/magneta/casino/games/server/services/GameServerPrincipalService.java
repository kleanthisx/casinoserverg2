/**
 * 
 */
package com.magneta.casino.games.server.services;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;


/**
 * @author anarxia
 *
 */
public interface GameServerPrincipalService extends PrincipalService {

    public void setPrincipal(ExtendedPrincipalBean bean);
}
