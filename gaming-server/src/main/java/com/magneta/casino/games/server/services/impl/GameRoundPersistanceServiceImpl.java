package com.magneta.casino.games.server.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magneta.casino.games.server.beans.GameRoundBean;
import com.magneta.casino.games.server.services.GameRoundPersistanceService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.configuration.GameConfiguration;

public class GameRoundPersistanceServiceImpl implements GameRoundPersistanceService {
	
	public GameRoundPersistanceServiceImpl() {
	}
	
	@Override
	public GameRoundBean loadRound(long tableId, int roundId) throws ServiceException {
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new ServiceException("Out of database connections");
		}
		
		try {
			return loadRound(conn, tableId, roundId);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	private final GameRoundBean loadRound(Connection dbConn, long tableId, int roundId) throws ServiceException {
        GameRoundBean round;
        
        PreparedStatement statement = null;
        ResultSet results = null;
        try {
            String sql = 
                "SELECT random_seed, round_started, game_config_id" +
                " FROM game_rounds" +
                " WHERE table_id = ?" +
                " AND round_id = ?";

            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, tableId);
            statement.setInt(2, roundId);

            results = statement.executeQuery();

            if (results.next()) {
            	round = new GameRoundBean();
            	round.setSeed(results.getBytes("random_seed"));
                round.setConfigId((Long)results.getObject("game_config_id"));
                round.setRoundStarted(results.getBoolean("round_started"));
            } else {
            	throw new ServiceException("Round not found");
            }

        } catch (SQLException e) {
            throw new ServiceException("Database error while loading round", e);
        } finally {
            DbUtil.close(results);
            DbUtil.close(statement);
        }
        
        return round;
	}
	
	@Override
	public void roundUpdateConfig(long tableId, int roundId, Long newConfigId) throws ServiceException {
			String sql = "UPDATE game_rounds" +
            " SET game_config_id = ?" +
            " WHERE table_id = ?" +
            " AND round_id = ?";

			Connection conn = ConnectionFactory.getConnection();
			if (conn == null) {
				throw new ServiceException("Out of database connections");
			}
			
            PreparedStatement updateStmt = null;
            try {
                updateStmt = conn.prepareStatement(sql);
                updateStmt.setLong(1, newConfigId);
                updateStmt.setLong(2, tableId);
                updateStmt.setInt(3, roundId);

                updateStmt.execute();
            } catch (SQLException e) {
            	throw new ServiceException("Database error while updating round configuration", e);
            } finally {
                DbUtil.close(updateStmt);
                DbUtil.close(conn);
            }
	}
	
	@Override
	public void createRound(long tableId, int roundId, byte[] seed, GameConfiguration config) throws ServiceException {
	        Connection dbConn = ConnectionFactory.getConnection();

	        if (dbConn == null) {
	            throw new DatabaseException( "Out of database connections");
	        }

	        ResultSet results = null;
	        PreparedStatement statement = null;
	        try {
	            String sql = "SELECT create_round(?,?,?,?)";
	            statement = dbConn.prepareStatement(sql);
	            statement.setLong(1, tableId);
	            statement.setInt(2, roundId);
	            statement.setBytes(3, seed);
	            if (config != null && config.getConfigId() != null && config.getConfigId() > 0) {
	                statement.setLong(4, config.getConfigId());
	            } else {
	                statement.setObject(4, null);
	            }

	            results = statement.executeQuery();

	            if (results.next()){
	                results.getInt(1);
	            }

	        } catch (SQLException e) {
	        	throw new DatabaseException("Database error while creating round", e);
	        } finally {
	            DbUtil.close(results);
	            DbUtil.close(statement);
	            DbUtil.close(dbConn);
	        }
	    }
}
