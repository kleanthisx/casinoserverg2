package com.magneta.casino.games.server.services;

import com.magneta.games.server.ServerTask;

/**
 * Task manager for the game server.
 * 
 * @author anarxia
 *
 */
public interface GameServerTaskManager {

	void addTask(ServerTask task);
	void addTask(String name, Runnable task);
	
	void waitForTasks();
	
	void start();
	void stop();
}
