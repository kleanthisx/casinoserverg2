package com.magneta.casino.games.server.services;

import com.magneta.casino.services.ServiceException;

public interface GameCurrentConfigurationService {

	Long getLatestConfigId(int gameId) throws ServiceException;
}
