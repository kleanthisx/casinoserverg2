package com.magneta.casino.games.server.beans;

/**
 * Represents a round.
 * 
 * @author anarxia
 *
 */
public class GameRoundBean {

	private byte[] seed;
	private Long configId;
	private boolean roundStarted;

	public byte[] getSeed() {
		return seed;
	}

	public void setSeed(byte[] seed) {
		this.seed = seed;
	}

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public boolean isRoundStarted() {
		return roundStarted;
	}

	public void setRoundStarted(boolean roundStarted) {
		this.roundStarted = roundStarted;
	}
	
}
