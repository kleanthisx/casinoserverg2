package com.magneta.casino.games.server.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerContext;
import com.magneta.casino.games.server.services.GameServerLoginService;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.server.LogoutDaemon;

public class GameServerLoginServiceImpl implements GameServerLoginService {

	private static final Logger log = LoggerFactory.getLogger(GameServerLoginServiceImpl.class);
	
    private final AtomicInteger loginUsers;
    private final GameServerRequestService requestService;
    private final GameServerContext gameServerContext;
    
    public GameServerLoginServiceImpl(GameServerRequestService requestService, GameServerContext gameServerContext) {
    	this.requestService = requestService;
    	this.gameServerContext = gameServerContext;
        loginUsers = new AtomicInteger(0);

        Connection conn = ConnectionFactory.getConnection();
        try {
            /* Mark all users as disconnected */
            PreparedStatement stmt = null;

            try {
                stmt = conn.prepareStatement("UPDATE user_logins SET logout_date=now_utc() WHERE server_id=? AND logout_date IS NULL");
                stmt.setInt(1, gameServerContext.getServerId());            
                stmt.executeUpdate();

            } catch (SQLException e1) {
                log.warn("Error while deleting stale user logins", e1);
            } finally {
                DbUtil.close(stmt);
            }

            /* Mark all table users as inactive */
            try {
                stmt = conn.prepareStatement(
                        "UPDATE game_table_users SET is_active=FALSE WHERE is_active=TRUE" +
                " AND EXISTS (SELECT game_tables.table_id FROM game_tables WHERE server_id=? AND game_tables.table_id=game_table_users.table_id)");

                stmt.setInt(1, gameServerContext.getServerId());
                stmt.executeUpdate();

            } catch (SQLException e1) {
                log.warn("Error while deleting stale game_table users", e1);
            } finally {
                DbUtil.close(stmt);
            }

        } finally {
            DbUtil.close(conn);
        }
    }
    
    @Override
	public int getLoggedInUsersCount() {
    	return loginUsers.get();
    }
    
    /**
     * Records a login.
     * It will return true if the login is ok and false if the user is
     * already logged in.
     * @param userDetails
     * @return
     * @throws ServiceException 
     */
    @Override
	public boolean recordLogin(UserDetailsBean userDetails, String userAgent) throws ServiceException {    	
    	GameServerRequest request = requestService.getRequest();
    	HttpSession session = request.getRequestConfig().getSession(false);
    	
    	/* Database automatically records login for demo users */
    	if (userDetails.getUserTypeEnum() == UserTypeEnum.DEMO_USER) {
    		loginUsers.incrementAndGet();
    		log.info("User " + userDetails.getUserName() + 
    				" (UserId: " + userDetails.getId() +
    				", IP: " + request.getRequestConfig().getRemoteIp() +
    				", SessionId: " + session.getId() + 
    				") logged in.");
    		return true;
    	}

    	Connection conn = ConnectionFactory.getConnection();
    	
    	if (conn == null) {
    		throw new DatabaseException("Out of database connections");
    	}
    	
        //record login
        PreparedStatement loginInfoStmt = null;
        try {
            loginInfoStmt = conn.prepareStatement("INSERT INTO user_logins(user_id,server_id,client_ip,login_token,device_hash_code) VALUES(?,?,?,?,?)");

            loginInfoStmt.setLong(1, userDetails.getId());
            loginInfoStmt.setInt(2, gameServerContext.getServerId());
            loginInfoStmt.setString(3, request.getRequestConfig().getRemoteIp());
            loginInfoStmt.setString(4, session.getId());
            loginInfoStmt.setString(5, userAgent);

            loginInfoStmt.executeUpdate();
            loginUsers.incrementAndGet();
            
        } catch (SQLException e1) {
            if (e1.getMessage().toLowerCase().contains("\"uniquei_user_logins_user_server\"")) {
                return false;
            } 
            throw new DatabaseException("Error while recording login", e1);
        } finally {
            DbUtil.close(loginInfoStmt);
            DbUtil.close(conn);
        }
        
        log.info("User " + userDetails.getUserName() + 
        		" (UserId: " + userDetails.getId() +
        		", IP: " + request.getRequestConfig().getRemoteIp() +
        		", SessionId: " + session.getId() + ") logged in.");

        return true;
    }
    
    /**
     * Records the logout in the database. This is essential because
     * the multiple-logins check uses the database.
     * @param userId
     */
    @Override
	public void recordLogout(UserDetailsBean userDetails, String sessionId) {
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Out of database connections to record logout for UserId: {}", userDetails.getId());
            LogoutDaemon.getInstance().addLogoutUser(userDetails.getId(), sessionId);
            return;
        }
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("UPDATE user_logins SET logout_date=now_utc() WHERE server_id=? AND user_id=? AND logout_date IS NULL");
            stmt.setInt(1, gameServerContext.getServerId());
            stmt.setLong(2, userDetails.getId());

            stmt.executeUpdate();
            
            loginUsers.decrementAndGet();
            
            log.info("User " + userDetails.getUserName() + " (UserId: " + userDetails.getId() + ", SessionId: "
    				+ sessionId + ") logged out.");
            
        } catch (SQLException e1) {
            log.error("Database error while recording logout (UserId: " + userDetails.getId() + ")", e1);
            LogoutDaemon.getInstance().addLogoutUser(userDetails.getId(), sessionId);
        } finally {
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
    }
}
