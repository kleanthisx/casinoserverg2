/**
 * 
 */
package com.magneta.casino.games.server.xmlrpc;

import javax.servlet.http.HttpSession;

import com.magneta.xmlrpc.server.SocketXmlRpcClientListener;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;

/**
 * @author Nicos
 *
 */
public class XmlRpcClientListener implements SocketXmlRpcClientListener {

    @Override
    public void onClientConnected(SocketXmlRpcRequestConfig requestConfig) {        
    }

    @Override
    public void onClientDisconnected(SocketXmlRpcRequestConfig requestConfig) {
    	GameServerRequestConfig config = (GameServerRequestConfig)requestConfig;
    	
    	HttpSession session = config.getSession(false);

    	if (session != null) {
    		try {
    			session.invalidate();
    		} catch (Throwable e) {}
    	}
    }

	@Override
	public void onShutdownRequested(SocketXmlRpcRequestConfig requestConfig) {		
	}

}
