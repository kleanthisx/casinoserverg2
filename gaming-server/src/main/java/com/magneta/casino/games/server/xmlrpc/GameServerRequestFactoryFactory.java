package com.magneta.casino.games.server.xmlrpc;

import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.ioc.ServiceResources;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Request processor using tapestry-ioc. 
 * @author anarxia
 *
 */
public class GameServerRequestFactoryFactory implements RequestProcessorFactoryFactory {

	private static class RequestProcessorFactoryImpl implements RequestProcessorFactory {

		private final Object processor;
		
		public RequestProcessorFactoryImpl(Object processor) {
			this.processor = processor;
		}
		
		@Override
		public Object getRequestProcessor(XmlRpcRequest pRequest)
				throws XmlRpcException {
			return processor;
		}
	}
	
	private static final Logger log = LoggerFactory.getLogger(GameServerRequestFactoryFactory.class);
	
	private final ServiceResources serviceResources;
	
	private final Map<Class<?>, Object> requestProcessors;
	
	public GameServerRequestFactoryFactory(ServiceResources serviceResources) {
		this.serviceResources = serviceResources;
		
		requestProcessors = new HashMap<Class<?>, Object>();
	}
	
	private Object getRequestProcessor(Class<?> pClass)
			throws XmlRpcException {
		
		Object requestProcessor = requestProcessors.get(pClass);
		
		if (requestProcessor == null) {
			try {
				log.info("Autobuilding request processor with type: {}", pClass.getCanonicalName());
				
				requestProcessor = serviceResources.autobuild(pClass);
				
				requestProcessors.put(pClass, requestProcessor);
			} catch (RuntimeException e) {
				throw new XmlRpcException("Unable to create request processor", e);
			}
		}
		
		return requestProcessor;
	}

	@Override
	public RequestProcessorFactory getRequestProcessorFactory(@SuppressWarnings("rawtypes") Class pClass)
			throws XmlRpcException {
		
		return new RequestProcessorFactoryImpl(getRequestProcessor(pClass));
	}
}