/**
 * 
 */
package com.magneta.casino.games.server.services.impl;

import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.beans.SystemPrincipalBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.UserAuthenticationBean;

/**
 * @author anarxia
 *
 */
public class PrincipalServiceImpl implements GameServerPrincipalService {

    private final ThreadLocal<ExtendedPrincipalBean> principal;
    private final ThreadLocal<Integer> systemCount;
    
    private static final ExtendedPrincipalBean systemBean = new SystemPrincipalBean();
    
    public PrincipalServiceImpl() {
        super();
        principal = new ThreadLocal<ExtendedPrincipalBean>();
        systemCount = new ThreadLocal<Integer>();
    }
    
    /* (non-Javadoc)
     * @see com.magneta.casino.internal.services.PrincipalService#getPrincipal()
     */
    @Override
    public ExtendedPrincipalBean getPrincipal() {
    	Integer count = systemCount.get();
        if (count != null && count > 0) {
            return systemBean;
        }
        return principal.get();
    }

    @Override
    public void setPrincipal(ExtendedPrincipalBean bean) {
        principal.set(bean);
    }
    
    /* (non-Javadoc)
     * @see com.magneta.casino.internal.services.PrincipalService#onUserAuthenticated(com.magneta.casino.services.beans.UserAuthenticationBean)
     */
    @Override
    public void onUserAuthenticated(UserAuthenticationBean authenticatedUser) throws ServiceException {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    /* (non-Javadoc)
     * @see com.magneta.casino.internal.services.PrincipalService#onUserLogout()
     */
    @Override
    public void onUserLogout() {
        principal.remove();
    }

	@Override
	public void pushSystem() {
		Integer count = systemCount.get();
		
		if (count == null) {
			count = 0;
		}
		
		count++;
		
		systemCount.set(count);
	}

	@Override
	public void popSystem() {
		Integer count = systemCount.get();
		if (count == null || count.equals(0)) {
			throw new RuntimeException("Unbalance push/popSystem");
		}
		
		count--;
		if (count.equals(0)) {
			systemCount.remove();
			count = null;
		} else {
			systemCount.set(count);
		}
	}

	@Override
	public void resetSystem() {
		systemCount.remove();
	}

}
