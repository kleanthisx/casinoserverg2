package com.magneta.casino.games.server.session.impl;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import com.magneta.casino.games.random.SeedGeneratorService;
import com.magneta.casino.games.server.session.SessionIdGenerator;

public class SessionIdGeneratorImpl implements SessionIdGenerator {
	
	private final SeedGeneratorService seedService;
	
	public SessionIdGeneratorImpl(SeedGeneratorService seedService) {
		this.seedService = seedService;
	}

	@Override
	public Random createRandom() {
		byte[] seed = seedService.getSeed(64);

		SecureRandom rand;
		try {
			rand = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} 
		rand.setSeed(seed);

		return rand;
	}
}
