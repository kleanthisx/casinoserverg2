package com.magneta.casino.games.server.services;

import com.magneta.games.GameRound;

/**
 * Service to get game instances for a gameBean.
 * @author anarxia
 *
 */
public interface GameRoundService {
	GameRound createRound(String gameTemplate);
}
