package com.magneta.casino.games.server.services;

import com.magneta.casino.services.ServiceException;
import com.magneta.games.GameSettings;

public interface GameSettingsService {

	GameSettings getGameSettings(int gameId) throws ServiceException;
}
