package com.magneta.casino.games.server.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;
import org.apache.xmlrpc.server.XmlRpcHandlerMapping;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.XmlRpcHandlerMappingFactory;
import com.magneta.casino.games.server.xmlrpc.GameServerXmlRpcHandlerMapping;
import com.magneta.casino.games.server.xmlrpc.annotations.XmlRpcHandler;
import com.magneta.xmlrpc.server.SocketXmlRpcTypeConverterFactory;

public class XmlRpcHandlerMappingFactoryImpl implements XmlRpcHandlerMappingFactory {
	
	private static final Logger log = LoggerFactory.getLogger(XmlRpcHandlerMappingFactoryImpl.class);

	private final Reflections reflections;
	private final Map<String, XmlRpcHandlerMapping> handlerMappingCache;
	private final RequestProcessorFactoryFactory requestProccessorFactoryFactory;
	
	public XmlRpcHandlerMappingFactoryImpl(Reflections reflections,
			RequestProcessorFactoryFactory requestProccessorFactoryFactory) {
		this.reflections = reflections;
		this.requestProccessorFactoryFactory = requestProccessorFactoryFactory;
		this.handlerMappingCache = new HashMap<String, XmlRpcHandlerMapping>();
	}
	
    private void loadHandlers(GameServerXmlRpcHandlerMapping handlerMapping, String context) {
        Set<Class<?>> handlers = reflections.getTypesAnnotatedWith(XmlRpcHandler.class); 
        
        for(Class<?> handlerClass: handlers) {
        	XmlRpcHandler handler = handlerClass.getAnnotation(XmlRpcHandler.class);
        	
            if (handler == null) {
                continue;
            }
            
            if (context != null && !handler.context().equalsIgnoreCase(context)) {
            	continue;
            }
             
            log.info("XML-RPC Context: " + context + " Handler: " + handler.name() + " (" + handlerClass.getCanonicalName() + ")");
            try {
            	handlerMapping.registerHandlerClass(handler.name(), handlerClass);
			} catch (XmlRpcException e) {
				log.error("Unable to register handler", e);
			}
        }
    }
	
	@Override
	public XmlRpcHandlerMapping createMapping(String handler) {
		handler = handler.toLowerCase();
		
		XmlRpcHandlerMapping cachedMapping = this.handlerMappingCache.get(handler);
		if (cachedMapping != null) {
			return cachedMapping;
		}
				
		GameServerXmlRpcHandlerMapping gameHandlerMapping = new GameServerXmlRpcHandlerMapping();
		gameHandlerMapping.setRequestProcessorFactoryFactory(requestProccessorFactoryFactory);
		gameHandlerMapping.setTypeConverterFactory(new SocketXmlRpcTypeConverterFactory());
		loadHandlers(gameHandlerMapping, handler);
		
		handlerMappingCache.put(handler, gameHandlerMapping);
		
		return gameHandlerMapping;
	}
}
