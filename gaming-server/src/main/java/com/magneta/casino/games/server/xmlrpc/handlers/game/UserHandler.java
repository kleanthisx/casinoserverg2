package com.magneta.casino.games.server.xmlrpc.handlers.game;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.system.SettingNames;
import com.magneta.casino.games.server.services.GameServerContext;
import com.magneta.casino.games.server.services.GameServerLoginService;
import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestValidationService;
import com.magneta.casino.games.server.session.SessionProperties;
import com.magneta.casino.games.server.xmlrpc.annotations.XmlRpcHandler;
import com.magneta.casino.services.CorporateSystemSettingsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.GamePlayer;
import com.magneta.security.Password;

/**
 * UserHandler provides the necessary methods for managing the system users.
 */
@XmlRpcHandler(context="game", name="User")
public class UserHandler {
	
	private static final Logger log = LoggerFactory.getLogger(UserHandler.class);

	@Inject
	private GameServerLoginService loginService;

	@Inject
	private GameServerRequestValidationService validationService;
	
	@Inject
	private SettingsService settingsService;
	
	@Inject
	private UserDetailsService userDetailsService;
	
	@Inject
    private GameServerRequest request;
	
	@Inject
	private GameServerContext gameServerContext;
	
	@Inject
	private GameClientService gameClientService;
	
	@Inject
	private CorporateSystemSettingsService corpSettingsService;
	
	@Inject
	private CorporatesService corporateService;
	
	@Inject
	private CountryService countryService;
	
	@Inject
	private GameServerPrincipalService principalService;
	
	@Inject
	private UserAuthenticationService authenticationService;

    //user error codes
    private static final int LOGIN_SUCCESS = 0;
    private static final int INVALID_USERNAME_PASSWORD = 1;
    
    private static final int UNKNOWN_ERROR = -1;
    private static final int SYSTEM_SUSPENDED = -3;
    private static final int DATABASE_ERROR = -4;
    private static final int ALREADY_LOGGED_IN = -6;
    private static final int USERS_LIMIT_EXCEEDED = -8;

    private static class LoginInfo {
        int status;
        long userId;

        public LoginInfo() {
            userId = 0L;
            status = LOGIN_SUCCESS;
        }
    }

    private boolean checkMaxUsersLimit() throws ServiceException {
    	Integer maxUsersLimit = settingsService.getSettingValue("system.games.max_users_per_server", Integer.class);
    	
    	if (maxUsersLimit == null || maxUsersLimit <= 0) {
    		return true;
    	}

        return loginService.getLoggedInUsersCount() < maxUsersLimit;  
    }
    
    /**
     * Checks whether the client should be allowed to
     * proceed to login.
     * 
     * @return
     * @throws XmlRpcException 
     * @throws ServiceException 
     */
    private int preLoginCheck() throws XmlRpcException {
        if (gameServerContext.isSuspended()){
            return SYSTEM_SUSPENDED;
        }
        
        try {
			if (!checkMaxUsersLimit()){
			    log.debug("Max users limit exceeded!");
			    return USERS_LIMIT_EXCEEDED;
			}
		} catch (ServiceException e) {
			log.error("Unable to check max users limit", e);
			return USERS_LIMIT_EXCEEDED;
		}
        
        /* Ensure that verify version has been called */
        if (request.getClientId() == null) {
            throw new XmlRpcException("Client not verified");
        }

        return LOGIN_SUCCESS;
    }

    
    private boolean countryCheck(String countryCode) {
        if (countryCode == null)
            return true;
        
        CountryBean country = null;
        try {
            country = countryService.getCountry(countryCode);
        } catch (ServiceException e) {
            log.error("Error getting country",e);
        }
        
        if (country == null) {
            return true;
        }
        
        return country.isEnabled();
    }

    private void demoAuthorization(Connection conn, LoginInfo info) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(
                    "SELECT demo_user_login(?,?,?)");

            stmt.setInt(1, gameServerContext.getServerId());
            stmt.setString(2, request.getRemoteAddr());
            stmt.setString(3, calculateUserAgent());
            rs = stmt.executeQuery();

            if (rs.next()) {
                info.userId = rs.getLong(1);
                
                if (rs.wasNull()) {
                	info.userId = 0l;
                	info.status = USERS_LIMIT_EXCEEDED;
                } else {
                	info.status = LOGIN_SUCCESS;
                }
            } else {
            	info.status = USERS_LIMIT_EXCEEDED;
            }
        } catch (SQLException e) {
            log.error("Error while verifying password", e);
            info.status = DATABASE_ERROR;
            return;
        } finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
        }
    }

    private void passwordAuthentication(Connection conn, LoginInfo info, String username, String password) {

        //CHECK username & password
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(
                    "SELECT user_id, password_hash, password_salt" +
                    " FROM users" +
                    " WHERE LOWER(username)=?");

            stmt.setString(1, username.toLowerCase());
            rs = stmt.executeQuery();

            if (rs.next()) {
                String dbHash = rs.getString("password_hash");
                String salt = rs.getString("password_salt");

                info.userId = rs.getLong("user_id");
                
                String hash = Password.getPasswordHash(password, salt);

                if (hash.equals(dbHash)) {
                    info.status = LOGIN_SUCCESS;
                } else {
                    info.status = INVALID_USERNAME_PASSWORD;
                }
            } else {
                info.status = INVALID_USERNAME_PASSWORD;
            }
        } catch (SQLException e) {
            log.error("Error while verifying password", e);
            info.status = DATABASE_ERROR;
        } finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
        }
    }
    
    private boolean clientIdInList(String list, int clientId) {
    	String[] clientIds = list.split(" ");
    	
    	for (String cId: clientIds) {
    		try {
    			int client = Integer.parseInt(cId);
    			
    			if (client == clientId) {
    				return true;
    			}
    			
    		} catch (NumberFormatException e) {
    			
    		}
    	}
    	
    	return false;
    }
    
    /**
     * Checks whether the user is allowed to login using the given client.
     * @return
     * @throws ServiceException 
     */
    private boolean clientAuthorization(UserDetailsBean userDetails, List<Long> corporateHierarchy) throws ServiceException {    	
    	GameClientBean client = gameClientService.getGameClient(request.getClientId());
    	
    	if (client == null) {
    		log.error("Unable to retrieve client with id " + request.getClientId());
    		return false;
    	}
    	
    	String clients = null;
    	
    	if (userDetails.getUserTypeEnum() == UserTypeEnum.CORPORATE_PLAYER) {
    		for (Long corpId: corporateHierarchy) {
    			clients = corpSettingsService.getSetting(corpId, SettingNames.GAME_CLIENTS);
    			if (clients != null) {
    				break;
    			}
    		}
    	}
    	
    	if (clients == null) {
    		SettingBean setting = settingsService.getSetting(SettingNames.GAME_CLIENTS);

    		if (setting != null) {
    			clients = setting.getValue();
    		}
    	}

		if (clients != null) {
			log.debug("clients: " + clients);
			return clientIdInList(clients, request.getClientId());
		}
        return true;
    }

    private String calculateUserAgent() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(request.getClientId());
        sb.append("/");
        sb.append(request.getSession(false).getAttribute(SessionProperties.CLIENT_PROTOCOL_VERSION));
        
        Object version = request.getSession(false).getAttribute(SessionProperties.CLIENT_VERSION);
        if (version != null) {
            sb.append(" ");
            sb.append(version);
        }
       
       return sb.toString();
    }

    private int updateDemoBalance(Long userId) {
        if (!isDemoMode()) {
            return LOGIN_SUCCESS;
        }
        
        double loginBalance;
        
        try {
			SettingBean settingBean = settingsService.getSetting("system.fun.login_balance");
			if (settingBean == null || settingBean.getValue() == null) {
				return LOGIN_SUCCESS;
			}
			
			loginBalance = Double.valueOf(settingBean.getValue());
		} catch (ServiceException e) {
			log.error("Unable to retrieve demo balance settting");
			return LOGIN_SUCCESS;
		}
        
        
        if (loginBalance < 0.01) {
            return LOGIN_SUCCESS;
        }
        
        Connection conn = ConnectionFactory.getConnection();
        
        if (conn == null) {
        	return DATABASE_ERROR;
        }
        
        //Update user's balance if necessary.
        PreparedStatement adjustmentStmt = null;

        try {
        	adjustmentStmt = conn.prepareStatement(
        			"INSERT INTO user_balance_adjustments(user_id, adjustment_user, adjustment_amount)"+
        			" (SELECT user_balance.user_id, user_balance.user_id, (? - balance) AS adj_amount FROM user_balance WHERE user_id = ?" +
        	"  AND abs(? - balance) > ?)");
        	adjustmentStmt.setBigDecimal(1, new BigDecimal(loginBalance));
        	adjustmentStmt.setLong(2, userId);
        	adjustmentStmt.setBigDecimal(3, new BigDecimal(loginBalance));
        	adjustmentStmt.setBigDecimal(4, new BigDecimal(0.009));

        	adjustmentStmt.execute();
            
        } catch (SQLException ex) {
            log.warn("Error while updating user balance.", ex);
            return DATABASE_ERROR;
        } finally {
            DbUtil.close(adjustmentStmt);
            DbUtil.close(conn);
        }
        
        return LOGIN_SUCCESS;
    }

    private void logoutIfLoggedIn() {
        /* Logout the user */
        if (request.getPlayer() != null) {
            logout(request);
        }
    }

    private void updateNickname(String nickname) {
    	GamePlayer player = request.getPlayer();
    	
    	Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Out of database connections while updating nickname");
            return;
        }
    	
        /* Update nickname if send */
        PreparedStatement nickStmt = null;
        try {
                nickStmt = conn.prepareStatement("UPDATE users SET nickname=?, modifier_user = ? WHERE user_id=?");
                nickStmt.setString(1, nickname);
                nickStmt.setLong(2, player.getUserId());
                nickStmt.setLong(3, player.getUserId());

                if (nickStmt.executeUpdate() == 1) {
                    player.getDetails().setNickName(nickname);
                }
        } catch (SQLException e) {
            log.error("Error while setting nickname", e);
        } finally {
            DbUtil.close(nickStmt);
            DbUtil.close(conn);
        }
    }
    
    private boolean isDemoMode() {
    	try {
			return settingsService.getSettingValue("system.fun.enable", Boolean.class);
		} catch (ServiceException e) {
			return false;
		}    	
    }
    
    /**
     * Logs in the give user into the system but returns a response status code instead of a boolean.
     * @param username The username of the user.
     * @param password The password of the user.
     * @return A response status code.
     * @throws XmlRpcException
     */
    public int loginWithStatus(String username, String password, String nickname) throws XmlRpcException {

    	if (username == null || password == null) {
    		return INVALID_USERNAME_PASSWORD;
    	}
    	
    	nickname = nickname.trim();
    	if (nickname.isEmpty()) {
    		nickname = null;
    	}

    	boolean demoUser = username.trim().isEmpty() && password.trim().isEmpty();

    	if (demoUser && !isDemoMode()) {
    		return INVALID_USERNAME_PASSWORD;
    	}

    	LoginInfo info = new LoginInfo();

    	info.status = preLoginCheck();

    	if (info.status != LOGIN_SUCCESS) {
    		return info.status;
    	}

    	logoutIfLoggedIn();

    	Connection conn = ConnectionFactory.getConnection();
    	if (conn == null) {
    		return DATABASE_ERROR;
    	}
    	try {
    		if (demoUser) {
    			demoAuthorization(conn, info);
    		} else {
    			passwordAuthentication(conn, info, username, password);
    		}
    	} finally {
    		DbUtil.close(conn);
    	}

    	if (info.status != LOGIN_SUCCESS) {
    		Boolean passwordMatched = null;

    		if (info.status == INVALID_USERNAME_PASSWORD) {
    			passwordMatched = false;
    		}

    		recordLoginFailure(info, username, passwordMatched);
    		return info.status;
    	}
    	
    	if (info.status == LOGIN_SUCCESS) {
    		info.status = onAuthenticated(info.userId, username, demoUser);
    	}

    	if (info.status == LOGIN_SUCCESS && nickname != null) {
    		updateNickname(nickname);
    	}

    	return info.status;
    }
    
    private int onAuthenticated(long userId, String token, boolean demoUser) {    	
    	HttpSession session = request.getSession(false);

    	List<Long> corporateHierarchy = null;

    	try {
    		principalService.pushSystem();
    		corporateHierarchy = corporateService.getCorporateHierarchy(userId);

    	} catch (ServiceException e) {
    		log.error("Error during user authentication", e);
    		return DATABASE_ERROR;
    	} finally {
    		principalService.popSystem();
    	}

    	UserAuthenticationBean authBean;

    	try {
    		authBean = authenticationService.createUserAuthenticationBean(userId);
    	} catch (DatabaseException e) {
    		return DATABASE_ERROR;
    	} catch (AccessDeniedException e) {
			return INVALID_USERNAME_PASSWORD;
    	} catch (ServiceException e) {
    		return UNKNOWN_ERROR;
    	}

    	UserDetailsBean userDetails = null;
    	try {
    		principalService.pushSystem();
    		userDetails = userDetailsService.getUser(userId);
    	} catch (ServiceException e) {
    		log.error("Error during user authentication", e);
    		return DATABASE_ERROR;
		} finally {
    		principalService.popSystem();
    	}
    	
		if (userDetails == null) {
			log.error("Unable to find user with id: " + userId);
			return INVALID_USERNAME_PASSWORD;
		}
		
		/* Enforce user types */
		if (userDetails.getUserTypeEnum() != UserTypeEnum.NORMAL_USER
				&& userDetails.getUserTypeEnum() != UserTypeEnum.CORPORATE_PLAYER
				&& userDetails.getUserTypeEnum() != UserTypeEnum.DEMO_USER) {
			return INVALID_USERNAME_PASSWORD;
		}
		
    	/* Verify that the player is using an allowed client */
    	try {
    		principalService.pushSystem();
    		
    		if (!clientAuthorization(userDetails, corporateHierarchy)) {
    			return INVALID_USERNAME_PASSWORD;
    		}
    	} catch (ServiceException e) {
    		if (e instanceof DatabaseException) {
    			return DATABASE_ERROR;
    		} 
    		return INVALID_USERNAME_PASSWORD;

		} finally {
			principalService.popSystem();
		}
		
    	if (!countryCheck(userDetails.getCountryCode())) {
    		return UNKNOWN_ERROR;
    	}

    	try {
			if (!loginService.recordLogin(userDetails, calculateUserAgent())) {
				return ALREADY_LOGGED_IN;
			}
		} catch (ServiceException e) {
			log.error("Error while recording login", e);
			return UNKNOWN_ERROR;
		}

    	GamePlayer player = new GamePlayer(authBean, userDetails, corporateHierarchy);

    	if (isDemoMode()) {
    		updateDemoBalance(userId);
    	}
    	
    	session.setAttribute(SessionProperties.PLAYER, player);
    	principalService.setPrincipal(player);
    	player.updateActionTime();
    	return LOGIN_SUCCESS; 
    }

    private void recordLoginFailure(LoginInfo info, String username, Boolean credentialsOk) {
    	
    	Connection conn = ConnectionFactory.getConnection();
    	if (conn == null) {
    		log.error("Out of database connections while recording login failure");
    		return;
    	}
    	
        PreparedStatement loginInfoStmt = null;
        try {
            loginInfoStmt = conn.prepareStatement(
            "INSERT INTO user_failed_logins(server_id,username,user_id,device_hash_code,fail_reason,password_matched,ip_address) VALUES(?,?,?,?,?,?,?)");

            loginInfoStmt.setInt(1, gameServerContext.getServerId());
            loginInfoStmt.setString(2, username);

            if (info.userId > 0){
                loginInfoStmt.setLong(3, info.userId);
            } else {
                loginInfoStmt.setObject(3, null);
            }
            loginInfoStmt.setString(4, calculateUserAgent());
            loginInfoStmt.setInt(5, info.status);
            
            DbUtil.setBoolean(loginInfoStmt, 6, credentialsOk);
            loginInfoStmt.setString(7, request.getRemoteAddr());

            loginInfoStmt.executeUpdate();
        } catch (SQLException e1) {
            log.error("Error while recording login failure", e1);
        } finally {
            DbUtil.close(loginInfoStmt);
            DbUtil.close(conn);
        }
    }

    /**
     * Logs out the given user.
     *
     * @return A boolean indicating whether the logout was successful.
     * @throws XmlRpcException
     */
    public boolean logout() throws XmlRpcException {
        return logout(request);
    }
    
    private static boolean logout(GameServerRequest request) { 
    	request.getRequestConfig().setDisconnect();
        GamePlayer player = request.getPlayer();
        if (player == null) {
            return false;
        }
        
        HttpSession session = request.getSession(false);
        
        try {
            session.removeAttribute(SessionProperties.PLAYER);
        } catch (IllegalStateException e) {
            return false;
        }

        return true;
    }
    
    /**
     * Returns the nickname for the user for the current session. 
     * @throws XmlRpcException
     */
    public String getNickname() throws XmlRpcException {
    	validationService.validateRequest(request);

    	GamePlayer player = request.getPlayer();

    	if (player != null && player.getNickname() != null) {
    		return player.getNickname();
    	} 
    	return "";
    }

    /**
     * Gets the current balance for the user.
     * @throws XmlRpcException
     */
    public double getBalance() throws XmlRpcException {
    	validationService.validateRequest(request);

    	GamePlayer player = request.getPlayer();
    	UserBalanceBean balanceBean;
    	try {
    		balanceBean = userDetailsService.getUserBalance(player.getUserId());
		} catch (ServiceException e1) {
			throw new XmlRpcException(HttpURLConnection.HTTP_INTERNAL_ERROR, "Internal server error");
		}

    	return balanceBean.getBalance() + balanceBean.getPlayCredits();
    }

    /**
     * Gets the current balance for the user.
     * @throws XmlRpcException
     */
    public Object[] getBalanceNew() throws XmlRpcException {
    	validationService.validateRequest(request);

    	GamePlayer player = request.getPlayer();
    	
    	UserBalanceBean balanceBean;
    	try {
    		balanceBean = userDetailsService.getUserBalance(player.getUserId());
		} catch (ServiceException e1) {
			throw new XmlRpcException(HttpURLConnection.HTTP_INTERNAL_ERROR, "Internal server error");
		}
    	
    	
    	double balance = balanceBean.getBalance() + balanceBean.getPlayCredits();

    	Connection dbConn = ConnectionFactory.getConnection();
    	PreparedStatement statement = null;
    	ResultSet rs = null;

    	if (dbConn == null) {
    		throw new XmlRpcException(HttpURLConnection.HTTP_INTERNAL_ERROR, "Internal Server Error");
    	}

    	try {
    		String sql = 
    			"SELECT transaction_id, amount" +
    			" FROM latest_non_gaming_transactions" +
    			" WHERE user_id = ?";
    		statement = dbConn.prepareStatement(sql);
    		statement.setLong(1, player.getUserId());

    		rs = statement.executeQuery();

    		if (rs.next()){
    			return new Object[] {
    				balance,
    				rs.getLong("transaction_id"),
    				rs.getDouble("amount")
    			};
    		}
    	} catch (SQLException e) {
    		throw new XmlRpcException(HttpURLConnection.HTTP_INTERNAL_ERROR, "Internal Server Error", e);
    	} finally {
    		DbUtil.close(rs);
    		DbUtil.close(statement);
    		DbUtil.close(dbConn);
    	}
    	
    	return new Object[] {balance};
    }

    /**
     * Gives the user useful information available after login.
     *
     * @return A Map containing the following fields:<br>
     * <table border="1">
     * <tr><th>Field</th>         <th>Type</th>   <th>Description</th></tr>
     * <tr><td>license_info</td>  <td>String</td>  <td>Operator license</td></tr>
     * <tr><td>latest_version</td> <td>String</td> <td>Client latest version</td></tr>
     * </table>
     * 
     * @throws XmlRpcException
     * @throws ServiceException 
     */
    public Map<String, Object> getLoginStatus() throws XmlRpcException, ServiceException {
    	validationService.validateRequest(request);

    	Map<String,Object> loginStatus = new HashMap<String,Object>();
    	
    	GameClientBean gameClient;
    	
    	try {
    		principalService.pushSystem();
    		gameClient = gameClientService.getGameClient(request.getClientId());
    	} finally {
    		principalService.popSystem();
    	}
    	
    	if (gameClient != null && gameClient.getClientLatestVersion() != null) {
    		loginStatus.put("latest_version", gameClient.getClientLatestVersion());
    	}
    	
    	SettingBean setting = settingsService.getSetting("system.operation_license_info");
    	
    	if (setting != null) {
    		String val = setting.getValue();
    		
    		if (val != null && !val.trim().isEmpty()) {
    			loginStatus.put("license_info", val);
    		}
    	}

    	return loginStatus;
    }

    public Map<String, Object> getUserPlayStats() throws XmlRpcException {
    	validationService.validateRequest(request);

        Map<String, Object> stats = new HashMap<String, Object>();
        GamePlayer player = request.getPlayer();
        if (player != null){
            stats.put("current_bets", player.getCurrentBets());
            stats.put("current_wins", player.getCurrentWins());
        }
        return stats;
    }

    /**
     * Gets the list of favourite games for the user.
     * @param games Number of games to retrieve.
     * @return An Object array containing a the id's of the favourite games. 
     * @throws XmlRpcException
     */    
    public Object[] getUserFavouriteGames(int games) throws XmlRpcException {
    	if (games <= 0) {
    		throw new XmlRpcException("Invalid request");
    	}

    	if (games > 20) {
    		games = 20;
    	}

    	validationService.validateRequest(request);

    	int[] favourites = new int[games];
    	int favouriteCount = 0;

    	GamePlayer player = request.getPlayer();

    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new XmlRpcException("Internal server error");
    	}

    	try {
    		PreparedStatement statement = null;
    		ResultSet rs = null;

    		try {
    			statement = dbConn.prepareStatement(
    					"SELECT game_tables.game_type_id, SUM(rounds) As total_rounds" +
    					" FROM game_period_amounts" +
    					" INNER JOIN game_tables ON game_tables.table_id = game_period_amounts.table_id" +
    					" WHERE user_id= ?" +
    					" GROUP BY game_type_id" +
    					" ORDER BY total_rounds DESC" +
    			" LIMIT ?");

    			statement.setLong(1, player.getUserId());
    			statement.setLong(2, games);

    			rs = statement.executeQuery();

    			while(rs.next()) {
    				favourites[favouriteCount++] = rs.getInt("game_type_id");
    			}
    		} catch (SQLException e) {
    			throw new XmlRpcException(HttpURLConnection.HTTP_INTERNAL_ERROR, "Internal Server Error", e);
    		}
    	} finally {
    		DbUtil.close(dbConn);
    	}

    	Object[] clientGames = new Object[favouriteCount];
    	for (int i=0; i < clientGames.length; i++) { 
    		clientGames[i] = new Integer(favourites[i]);
    	}

    	return clientGames;
    }
    
    /**
     * Idle method.
     * 
     * Gets all results from asynchronous requests.
     * 
     * @return
     * @throws XmlRpcException
     */
    public String onIdle() throws XmlRpcException {
    	validationService.validateRequest(request);
    	
    	String results = request.getPlayer().getAsyncResults();
    	if (results == null) {
    		return "";
    	}
    	
    	
    	return results; 
    }
}
