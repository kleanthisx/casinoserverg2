package com.magneta.casino.games.server.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.tapestry5.ioc.annotations.PostInjection;
import org.apache.tapestry5.ioc.services.RegistryShutdownHub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerTaskManager;
import com.magneta.games.server.RunnableServerTask;
import com.magneta.games.server.ServerTask;

public class GameServerTaskManagerImpl implements GameServerTaskManager {
	
    private enum State {
        STOPPED, RUNNING;
    }
	
	private static final Logger log = LoggerFactory.getLogger(GameServerTaskManagerImpl.class);
	
	private List<ServerTask> tasks;
	
    private final AtomicReference<State> stateRef;
        
	public GameServerTaskManagerImpl() {
		stateRef = new AtomicReference<State>(State.STOPPED);
		tasks = new ArrayList<ServerTask>();
	}
	
	@PostInjection
	public void startupService(RegistryShutdownHub shutdownHub) {
		shutdownHub.addRegistryWillShutdownListener(
			new Runnable() {
				@Override
				public void run() {
					stop();
				}
			}
		);
	}
	
    @Override
	public void addTask(String name, Runnable task) {
        tasks.add(new RunnableServerTask(name, task));
    }
    
    @Override
	public void addTask(ServerTask task) {
        tasks.add(task);
    }
    
    @Override
	public void start() {
    	if (!stateRef.compareAndSet(State.STOPPED, State.RUNNING)) {
    		throw new IllegalStateException();
    	} 
    	
    	log.info("Task manager: starting all tasks");
    	
    	for (ServerTask con: tasks) {
    		log.info("Task manager: starting {}", con.getName());
    		con.start();
    	}
    	log.info("Task manager: started all tasks");
    }
    
    @Override
	public void stop() {
    	if (!stateRef.compareAndSet(State.RUNNING, State.STOPPED)) {
    		
    		/* Check if we are already stopped */
    		if (!stateRef.compareAndSet(State.STOPPED, State.STOPPED)) {
    			throw new IllegalStateException();
    		}
    		
    		/* We are already shutdown */
    		log.debug("Task manager: already stopped. You have one stop() too many.");
    		return;
    	}

    	log.info("Task manager: stopping all tasks");
    	
        for (int i=tasks.size()-1; i >=0; i--) {
            ServerTask task = tasks.get(i);
            log.info("Task manager: stopping {}", task.getName());
            task.stop();
        }
        
        log.info("Task manager: stopped all tasks");
    }
    
    @Override
	public void waitForTasks() {
        for (ServerTask con: tasks) {
            try {
                con.join();
            } catch (InterruptedException e) { Thread.interrupted(); }
        }
    }
}
