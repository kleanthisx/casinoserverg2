package com.magneta.casino.games.server.services.impl;

import com.magneta.casino.games.server.services.CurrentRoundService;
import com.magneta.games.GameRound;

public class CurrentRoundServiceImpl implements CurrentRoundService {

	private final ThreadLocal<GameRound> round;
	
	public CurrentRoundServiceImpl() {
		round = new ThreadLocal<GameRound>();
	}
	
	@Override
	public void startRequest(GameRound round) {
		this.round.set(round);
	}
	
	@Override
	public GameRound getRound() {
		return round.get();
	}
	
	@Override
	public void endRequest() {
		round.remove();
	}
}
