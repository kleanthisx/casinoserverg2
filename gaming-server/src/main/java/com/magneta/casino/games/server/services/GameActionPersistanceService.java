package com.magneta.casino.games.server.services;

import java.util.List;

import com.magneta.casino.services.ServiceException;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameRound;

public interface GameActionPersistanceService {
	
	void commit(GameAction action) throws ActionFailedException;
	
	/**
	 * Commits an action including generated and additional actions. First the action is saved, followed by
	 * generated actions and then additional actions.
	 * 
	 * @param action The action to commit.
	 * @param lastAction Whether this is the last action for the round. Every round should have exactly
	 * one action commited with this flag set. Getting this wrong can affect the accuracy of gaming information
	 * in the database.
	 * 
	 * @throws ActionFailedException
	 */
	void commit(GameAction action, boolean lastAction) throws ActionFailedException;
	
	List<GameAction> loadParentActions(GamePlayer player, GameRound round) throws ServiceException;
}
