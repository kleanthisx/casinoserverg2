/**
 * 
 */
package com.magneta.casino.games.server.xmlrpc.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.XmlRpcHandlerMapping;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerTaskManager;
import com.magneta.casino.games.server.services.ServiceLocator;
import com.magneta.casino.games.server.services.XmlRpcHandlerMappingFactory;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestListener;
import com.magneta.xmlrpc.server.XmlRpcSocketStreamServer;

/**
 * @author anarxia
 *
 */
public class HttpXmlRpcServlet extends HttpServlet implements Filter {

    private static final long serialVersionUID = 1L;
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(HttpXmlRpcServlet.class);
    
    private XmlRpcSocketStreamServer xmlrpcServer;
    private boolean strictCheck;
    private boolean accessControlHeaders;
    
    private final boolean standalone;

    public HttpXmlRpcServlet() {
    	this(true, null);
    }
    
    public HttpXmlRpcServlet(boolean standalone, XmlRpcSocketStreamServer xmlrpcServer) {
    	this.standalone = standalone;

    	strictCheck = true;
    	accessControlHeaders = true;
        this.xmlrpcServer = xmlrpcServer;
    }
    
    private XmlRpcSocketStreamServer createServer(ServletConfig config) throws ServletException {
    	String handler = config.getInitParameter("handler");
    	
    	if (handler == null) {
    		handler = "game";
    	} else {
    		handler = handler.toLowerCase();
    	}

        XmlRpcHandlerMappingFactory handlerMappingFactory = ServiceLocator.getService(XmlRpcHandlerMappingFactory.class);        
        XmlRpcHandlerMapping handlerMapping;
		try {
			handlerMapping = handlerMappingFactory.createMapping(handler);
		} catch (XmlRpcException e) {
			throw new ServletException("Unable to create XML-RPC handlers", e);
		}

        SocketXmlRpcRequestListener requestListener = ServiceLocator.getService(SocketXmlRpcRequestListener.class);
        XmlRpcSocketStreamServer server = new XmlRpcSocketStreamServer(requestListener);
        server.setHandlerMapping(handlerMapping);
        
        return server;
    }
    
    @Override
	public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	
    	String val = config.getInitParameter("xmlrpc.strict");
    	if (val != null) {
    		this.strictCheck = Boolean.parseBoolean(val);
    	}
    	
    	val = config.getInitParameter("http.accessControl");
    	if (val != null) {
    		this.accessControlHeaders = Boolean.parseBoolean(val);
    	}
    	
    	if (this.standalone) {
    		/* FIXME. Initialization/destruction should run once per VM not per servlet.
    		 */
    		ServiceLocator.init();
    		GameServerTaskManager taskManager = ServiceLocator.getService(GameServerTaskManager.class);
    		taskManager.start();
    		this.xmlrpcServer = createServer(config);
    	}
    }
    
    @Override
	public void destroy() {
    	if (this.standalone) {
    		ServiceLocator.shutdown();
    		xmlrpcServer = null;
    	}
    	super.destroy();
    }
    
    private static class FilterServletConfig implements ServletConfig {

    	private final FilterConfig filterConfig;

		public FilterServletConfig(FilterConfig filterConfig) {
			this.filterConfig = filterConfig;
		}

		@Override
		public String getServletName() {
			return filterConfig.getFilterName();
		}

		@Override
		public ServletContext getServletContext() {
			return filterConfig.getServletContext();
		}

		@Override
		public String getInitParameter(String name) {
			return filterConfig.getInitParameter(name);
		}

		@Override
		public Enumeration<String> getInitParameterNames() {
			return filterConfig.getInitParameterNames();
		}
    }
    
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
    	init(new FilterServletConfig(filterConfig));
	}
    
    private String getAccessControlOrigin(HttpServletRequest req) {
    	String referer = req.getHeader("Referer");
    	
    	if (referer == null) {
    		return null;
    	}
    	
    	URL url;
		try {
			url = new URL(referer);
		} catch (MalformedURLException e) {
			return null;
		}
    	
    	if (url.getPort() > 0) {
    		return url.getProtocol() + "://" + url.getHost() + ":" + url.getPort();
    	}
    	return url.getProtocol() + "://" + url.getHost();
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	log.debug("HTTP: Request URI: {}", req.getRequestURI());
    	if (req.getRequestURI() == null || !req.getRequestURI().equalsIgnoreCase("/disconnect")) {
    		super.doGet(req, resp);
    		return;
    	}
    	
    	/*
    	Enumeration<String> headers = req.getHeaderNames();
    	while(headers.hasMoreElements()) {
    		String header = headers.nextElement();
    		log.info(header + ":" + req.getHeader(header));
    		
    	}*/
    	
    	if (this.accessControlHeaders) {
    		resp.setHeader("Access-Control-Allow-Credentials", "true");

    		String origin = getAccessControlOrigin(req);
    		if (origin != null) {
    			log.debug("Origin is: " + origin);
    			resp.setHeader("Access-Control-Allow-Origin", origin);
    		}
    	}
    	
    	HttpSession session = req.getSession(false);
    	if (session != null) {
    		try {
    			session.invalidate();
    			log.info("HTTP: Session disconnect finished.");
    		} catch (Throwable e) {
    		}
    	}
    	
    	resp.sendError(HttpServletResponse.SC_OK, "Disconnected");
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    	if (strictCheck) {
    		if (req.getContentLength() < 1) { 
    			try {
    				resp.sendError(HttpURLConnection.HTTP_LENGTH_REQUIRED, "Content-Length required");
    			} catch (IOException e) {
    			}
    			return;
    		}

    		if (req.getContentType() == null || !req.getContentType().startsWith("text/xml")) {
    			try {
    				resp.sendError(HttpURLConnection.HTTP_BAD_REQUEST, "Invalid Content-Type");
    			} catch (IOException e) {
    			}
    			return;
    		}
    	}

    	if (accessControlHeaders) {
    		resp.setHeader("Access-Control-Allow-Credentials", "true");

    		String origin = getAccessControlOrigin(req);
    		if (origin != null) {
    			log.debug("Origin is: {}", origin);
    			resp.setHeader("Access-Control-Allow-Origin", origin);
    		}
    	}
    	
    	/*
    	Enumeration<String> headers = req.getHeaderNames();
    	while(headers.hasMoreElements()) {
    		String header = headers.nextElement();
    		log.info(header + ":" + req.getHeader(header));
    	}*/

    	
        ByteArrayOutputStream os = new ByteArrayOutputStream(512);
        HttpXmlRpcRequestConfig requestConfig = new HttpXmlRpcRequestConfig(req);

        try {
            xmlrpcServer.execute(requestConfig, req.getInputStream(), os);
        } catch (IOException e) {
            log.debug("HTTP: Exception", e);
            return;
        }
        
        try {
            resp.setContentType("text/xml");
            resp.setContentLength(os.size());
            os.writeTo(resp.getOutputStream());
            
        } catch (IOException e) {
            log.debug("HTTP: Exception", e);
        }
       
        if (requestConfig.shouldDisconnect()) {
        	HttpSession sess = req.getSession(false);
        	if (sess != null) {
        		try {
        			sess.invalidate();
        		} catch (Throwable e) {}
        	}
        }
        
    }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		service(request, response);
		
		chain.doFilter(request, response);
	}
}

