package com.magneta.casino.games.server.xmlrpc.async;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AsyncError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7824977486066219006L;
	private Integer errorCode;
	private String description;
	private Integer requestId;
	
	public AsyncError() {
		
	}
	
	public AsyncError(Integer requestId, Integer errorCode, String description) {
		this.requestId = requestId;
		this.errorCode = errorCode;
		this.description = description;
	}

	@XmlElement
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	@XmlElement
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlAttribute
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	
}
