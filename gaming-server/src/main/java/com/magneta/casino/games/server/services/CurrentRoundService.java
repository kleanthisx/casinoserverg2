package com.magneta.casino.games.server.services;

import com.magneta.games.GameRound;

public interface CurrentRoundService {

	void startRequest(GameRound round);
	void endRequest();
	
	GameRound getRound();
}
