/**
 * 
 */
package com.magneta.casino.games.server.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.tapestry5.ioc.ServiceResources;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.PostInjection;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.reflections.Configuration;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.magneta.casino.games.server.services.GameRoundService;
import com.magneta.casino.games.server.services.SymbolConstants;
import com.magneta.casino.games.templates.LocalConfigurationBuilder;
import com.magneta.games.GameRound;
import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author anarxia
 *
 */
public class GameRoundServiceImpl implements GameRoundService {
	
	private final Logger log = LoggerFactory.getLogger(GameRoundServiceImpl.class);

	private final Map<String,Class<? extends GameRound>> gameClass = new HashMap<String,Class<? extends GameRound>>();
    
    private final ServiceResources serviceResources;

    public GameRoundServiceImpl(ServiceResources serviceResources) {
    	this.serviceResources = serviceResources;
    }
    
    private static final class GameClassConfigurationBuilder extends LocalConfigurationBuilder {
    	public GameClassConfigurationBuilder(String prefix) {
    		final Predicate<String> filter = new FilterBuilder.Include(FilterBuilder.prefix(prefix));

    		{
    			filterInputsBy(filter);
    			setUrls(ClasspathHelper.forPackage(prefix));

    			setScanners(
    					new TypeAnnotationsScanner().filterResultsBy(filter),
    					new SubTypesScanner().filterResultsBy(filter));
    		}
    	}
    }
    

    @PostInjection
	public void loadGameClasses(@Inject @Symbol(SymbolConstants.GAME_CLASS_PACKAGE) String gameClassPackage) {
        Configuration config = new GameClassConfigurationBuilder(gameClassPackage);

        Reflections reflections = new Reflections(config);
    
        Set<Class<? extends GameRound>> annotated = reflections.getSubTypesOf(GameRound.class); 

        for(Class<? extends GameRound> test: annotated) {
        	if (test.isInterface()) {
        		continue;
        	}
        	
            GameTemplate gTemplate = test.getAnnotation(GameTemplate.class);
            if (gTemplate == null) {
                continue;
            }
             
            log.info("Adding game template {} ({})", gTemplate.value(), test.getCanonicalName());
          
            gameClass.put(gTemplate.value(), test);
        }
    }
    
    @Override
    public GameRound createRound(String gameTemplate) {
		Class<? extends GameRound> roundClass = gameClass.get(gameTemplate);
		return serviceResources.autobuild(roundClass);
    }
}
