package com.magneta.casino.games.server.xmlrpc.handlers.game;

import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.random.SeedGeneratorService;
import com.magneta.casino.games.server.services.CurrentRoundService;
import com.magneta.casino.games.server.services.GameRoundPersistanceService;
import com.magneta.casino.games.server.services.GameRoundService;
import com.magneta.casino.games.server.services.GameServerContext;
import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestValidationService;
import com.magneta.casino.games.server.services.TableManager;
import com.magneta.casino.games.server.xmlrpc.annotations.XmlRpcHandler;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameClientGameBean;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameRequestRoundActionListener;
import com.magneta.games.GameRound;
import com.magneta.games.Table;
import com.magneta.games.TableLogoutListener;

/**
 * TableHandler class provides the necessary methods for interacting with the game tables of the system.
 */
@XmlRpcHandler(context="game", name="Table")
public class TableHandler {

	private static final Logger log = LoggerFactory.getLogger(TableHandler.class);

	@Inject
    private GameServerRequest request;
	
	@Inject
	private GameServerContext gameServerContext;
	
	@Inject
	private GameServerRequestValidationService validationService;
	
	@Inject
	private TableManager tableManager;
	
	@Inject
	private GameTableService gameTableService;
	
	@Inject
	private GameServerPrincipalService principalService;
	
	@Inject
	private GameClientService clientService;
	
	@Inject
	private GamesService gameService;
	
	@Inject
	private SeedGeneratorService seedGeneratorService;
	
	@Inject
	private GameRoundService gameRoundService;
	
	@Inject
	private GameRoundPersistanceService gameRoundPersistanceService;
	
	@Inject
	private CurrentRoundService currentRoundService;

    /**
     * Removes a player from a table.
     * @param tableID The ID of the table to remove the user from.
     * 
     * @return Whether the user was removed sucessfully or not.
     * @throws XmlRpcException
     */
    public boolean leaveTable(long tId) throws XmlRpcException {    	
    	validationService.validateRequest(request);

    	GamePlayer player = request.getPlayer();
    	player.updateActionTime();

    	Table table = tableManager.getActiveTable(tId);

    	if (table == null || table.getPlayer() != player) {
    		return false;
    	}

    	if (table.getGamePlayerLogoutListener() != null) {
    		player.removeLogoutListener(table.getGamePlayerLogoutListener());
    	}
    	
    	try {
			tableManager.markViewerActive(table, player, false);
		} catch (ServiceException e) {
			log.error("Unable to record table leaving", e);
		}
    	tableManager.removeTable(table);
    	
    	return true;
    }

    /**
     * Joins the user to the given game.
     * @param gameId The id of the game to join.
     * @return The current status of the table.
     * @throws XmlRpcException 
     * @throws ServiceException 
     */
    public Map<String,Object> joinGame(int gameId) throws XmlRpcException, ServiceException {
    	/* Validation */       
    	if (gameServerContext.isSuspended()) {
    		throw new XmlRpcException("System is unavailable due to maintainance.");
    	}
    	validationService.validateRequest(request);

    	try {
    		if (!checkGameEnabled(gameId)) {
    			throw new XmlRpcException("Game is currently unavailable.");
    		}
    	} catch (ServiceException e1) {
    		throw new XmlRpcException(500, "Cannot join game: Internal server error.", e1);
    	}

    	GamePlayer player = request.getPlayer();
    	player.updateActionTime();

    	principalService.pushSystem();
   		try {
   			GameTableBean tableBean = this.gameTableService.getActiveTable(player.getUserId(), gameId);
   			if (tableBean != null && tableManager.getActiveTable(tableBean.getTableId()) != null) {
   				throw new XmlRpcException("Player already in game");
   			}
   			
   			Table table;
   			
   			try {
   				GameBean game = gameService.getGame(gameId);
   				if (game == null) {
   					throw new XmlRpcException("Unsupported game");
   				}
   				
   				table = tableManager.getTable(player, game);
   			} catch (ServiceException e) {
   				log.error("Error while getting table" , e);
   				throw new XmlRpcException(e.getMessage());
   			}
   			
   			try {
   				tableManager.markViewerActive(table, player, true);
   			} catch (ServiceException e) {
   				log.error("Unable to record table join", e);
   			}
   			
			Map<String, Object> results = new HashMap<String, Object>();
			GameRequestRoundActionListener roundListener = new GameRequestRoundActionListener(); 
			
			GameRound round;
			try {
				tableManager.startTable(table, roundListener);
				
				round = table.getCurrentRound();
	        	if (round.isFinished()) {
	        		log.warn("Round {}-{} was finished during resume. Player may have missed the result");
					round = table.nextRound(roundListener, seedGeneratorService, gameRoundService, gameRoundPersistanceService, currentRoundService);
	        	}

	        	round.getInfo(player, results, true);
				
			} catch (Throwable e) {
				log.error("Error while starting table", e);
				tableManager.removeTable(table);
				throw new XmlRpcException(500, "Error starting game");
			}
			
			table.setGamePlayerLogoutListener(new TableLogoutListener(tableManager, table));
   			player.addLogoutListener(table.getGamePlayerLogoutListener());	
        	
        	results.put("table_id", table.getTableId());
        	results.put("min_bet", table.getMinBet());
        	results.put("max_bet", table.getMaxBet());
                        
            if (roundListener.getActions() != null) {
                results.put("actions", roundListener.getActions());
            }

        	return results;
   			
   		} finally {
   			principalService.popSystem();
   		}
   		
    }

    private boolean checkGameEnabled(int gameId) throws ServiceException {    	
        SearchContext<GameClientGameBean> searchContext =
        	FiqlContextBuilder.create(GameClientGameBean.class, "enabled==?;gameId==?", true, gameId);
        
        try {
        	principalService.pushSystem();
        	ServiceResultSet<GameClientGameBean> gameBeans = clientService.getGameClientGames(request.getClientId(), searchContext, 0, 0, null);
        	
        	return !gameBeans.getResult().isEmpty();
		} finally {
        	principalService.popSystem();
        }
    }
}
