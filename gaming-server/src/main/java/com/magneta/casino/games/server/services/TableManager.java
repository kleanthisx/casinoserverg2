package com.magneta.casino.games.server.services;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.GamePlayer;
import com.magneta.games.GameRoundListener;
import com.magneta.games.Table;

/**
 * Game server table manager.
 *
 * @author anarxia
 *
 */
public interface TableManager {

	/**
	 * Number of active tables.
	 */
	int getTablesCount();

	Table getActiveTable(long tableId);
	void removeTable(Table t);
	void closeTable(long tableId) throws ServiceException;

	Table getTable(GamePlayer player, GameBean game) throws ServiceException;

	void startTable(Table table, GameRoundListener roundListener)
			throws ServiceException;
	
	void markViewerActive(Table table, GamePlayer player, boolean active)
			throws ServiceException;
}
