package com.magneta.casino.games.server.services.impl;

import com.magneta.Config;
import com.magneta.casino.games.server.services.GameServerContext;

public class GameServerContextImpl implements GameServerContext {

	private volatile boolean suspended;
	private final int serverId;
	
	public GameServerContextImpl() {
		this.suspended = false;
		this.serverId = Config.getInt("server.id", -1);
		
		if (this.serverId < 0) {
			throw new RuntimeException("Server id not specified in server.properties");
		}
	}
	
	@Override
	public int getServerId() {
		return serverId;
	}

	@Override
	public boolean isSuspended() {
		return suspended;
	}

	@Override
	public void suspend() {
		suspended = true;
	}
}
