/**
 * 
 */
package com.magneta.casino.games.server.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameSettingsService;
import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.CachedGameSettings;
import com.magneta.games.GameSettings;


/**
 * @author anarxia
 *
 */
public class GameSettingsServiceImpl implements GameSettingsService {
	
	private static final Logger log = LoggerFactory.getLogger(GameSettingsServiceImpl.class);

    private static final long TIMEOUT = (60 * 1000);
    
    private Map<Integer,CachedGameSettings> gameSettings = new ConcurrentHashMap<Integer,CachedGameSettings>(50);  
    
    private final PrincipalService principalService;
    private final GamesService gameService;
    
    public GameSettingsServiceImpl(PrincipalService principalService,
    		GamesService gameService ) {
    	this.principalService = principalService;
    	this.gameService = gameService;
    }
    
    private Object getSetting(GameTemplateSetting s, String dbValue) {
        switch (s.getType()) {
            case GameTemplateSetting.STRING:
            case GameTemplateSetting.COUNTRY_VALUE:
                return dbValue;
            case GameTemplateSetting.BOOLEAN:
                return Boolean.parseBoolean(dbValue);
            case GameTemplateSetting.DOUBLE:
            case GameTemplateSetting.PERCENTAGE:
                return Double.parseDouble(dbValue);
            case GameTemplateSetting.INTEGER:
                return Integer.parseInt(dbValue);
        }
        return null;
    }
    
    private GameSettings loadGameSettings(int gameId) throws ServiceException {
        Map<String,Object> settings = new HashMap<String,Object>();
        
        GameBean game = gameService.getGame(gameId);

        GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
        
        List<GameTemplateSetting> templateSettings = new ArrayList<GameTemplateSetting>();
        gameTemplate.getSettings(templateSettings);
        
        for (GameTemplateSetting s: templateSettings) {
        	Object val = s.getDefaultValue();
        	if (val != null) {
        	    settings.put(s.getKey(), s.getDefaultValue());
        	}
        }
        
        Connection dbConn = ConnectionFactory.getConnection();
        
        if (dbConn == null) {
            throw new RuntimeException("Out of db connections while loading settings");
        }
        
        PreparedStatement statement = null;
        ResultSet res = null;

        try {            
            String sql = 
                "SELECT setting_key, setting_value" +
                " FROM game_settings" +
                " WHERE game_id = ?";

            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, gameId);

            res = statement.executeQuery();

            while (res.next()){
            	String key = res.getString("setting_key");
            	if (settings.containsKey(key)) {
            	    for (GameTemplateSetting s: templateSettings) {
            	        if (s.getKey().equals(key)) {
            	            settings.put(key, getSetting(s, res.getString("setting_value")));
            	        }
            	    }
            	} else {
            		log.warn("Unsupported game setting {} in database. Ignoring", key);
            	}
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting game settings.", e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }

        return new GameSettings(settings);
    }
    
    @Override
	public GameSettings getGameSettings(int gameId) throws ServiceException {
        CachedGameSettings cachedSettings = gameSettings.get(gameId); 
        GameSettings settings = null;

        if (cachedSettings == null) {
            try {
            	principalService.pushSystem();
                settings = loadGameSettings(gameId);
                gameSettings.put(gameId, new CachedGameSettings(settings));
            } finally {
            	principalService.popSystem();
            }
        } else {
            settings = cachedSettings.getSettings();
            
            /* Check if settings are fresh */
            if (System.currentTimeMillis() - cachedSettings.getLoadedTime() >= TIMEOUT) {
                try {
                	principalService.pushSystem();
                    settings = loadGameSettings(gameId);
                    gameSettings.put(gameId, new CachedGameSettings(settings));
                } finally {
                	principalService.popSystem();
                }
            }
        }

        return settings;
    }
}
