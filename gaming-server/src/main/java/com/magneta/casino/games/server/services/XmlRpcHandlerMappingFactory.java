package com.magneta.casino.games.server.services;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.XmlRpcHandlerMapping;

public interface XmlRpcHandlerMappingFactory {

	XmlRpcHandlerMapping createMapping(String handler) throws XmlRpcException;
}
