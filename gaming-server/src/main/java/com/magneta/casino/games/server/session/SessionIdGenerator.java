package com.magneta.casino.games.server.session;

import java.util.Random;

public interface SessionIdGenerator {
	Random createRandom();
}
