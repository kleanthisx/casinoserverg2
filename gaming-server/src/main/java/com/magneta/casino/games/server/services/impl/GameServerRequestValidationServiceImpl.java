package com.magneta.casino.games.server.services.impl;

import java.net.HttpURLConnection;

import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestValidationService;
import com.magneta.casino.games.server.services.GameServerUserDisconnectService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.games.GamePlayer;

public class GameServerRequestValidationServiceImpl implements GameServerRequestValidationService {
	
	private static final Logger log = LoggerFactory.getLogger(GameServerRequestValidationServiceImpl.class);
	
	private final GameServerUserDisconnectService userDisconnectService;
	private final SettingsService settingsService;
	
	public GameServerRequestValidationServiceImpl(GameServerUserDisconnectService userDisconnectService,
			SettingsService settingsService) {
		this.userDisconnectService = userDisconnectService;
		this.settingsService = settingsService;
	}

    @Override
	public void validateRequest(GameServerRequest request) throws XmlRpcException {
        
        if (request.getClientId() == null) {
        	request.getRequestConfig().setDisconnect();
        	throw new XmlRpcException(HttpURLConnection.HTTP_FORBIDDEN, "Client not verified");
        }
    	
    	GamePlayer player = request.getPlayer();
    	if (player == null) {
    		 throw new XmlRpcException(HttpURLConnection.HTTP_FORBIDDEN, "Not logged in");
    	}
        
        /* Check if the user has been disconnected by the admin */
        if (userDisconnectService.shouldDisconnect(player.getUserId())){
            request.getRequestConfig().setDisconnect();
            throw new XmlRpcException("Disconnected by administrator");
        }
        
        Integer sessionTimeOut;
		try {
			sessionTimeOut = this.settingsService.getSettingValue("game.session.timeout", Integer.class);
		} catch (ServiceException e) {
			log.error("Unable to retrieve session timeout setting", e);
			throw new XmlRpcException(500, "Internal server error");
		}
        
        /* Check if the user disconnected due to inactivity */
        if (sessionTimeOut != null && sessionTimeOut > 0) {
        	long lastActionTime = player.getLastActionTime();

            if (System.currentTimeMillis() - lastActionTime >= sessionTimeOut * 1000l) {
                try {
                	request.getRequestConfig().setDisconnect();
                } catch (Exception e) { }

                throw new XmlRpcException("Disconnected due to inactivity");
            }
        }
    }
}
