package com.magneta.casino.games.server.mbeans;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.casino.games.server.services.GameServerLoginService;
import com.magneta.casino.games.server.services.GameServerTaskManager;
import com.magneta.casino.games.server.services.GameServerUserDisconnectService;
import com.magneta.casino.games.server.services.TableManager;

public class ServerStatus implements ServerStatusMBean {
	
	private static final Logger log = LoggerFactory.getLogger(ServerStatus.class);

	private final GameServerLoginService loginService;
	private final GameServerUserDisconnectService disconnectService;
	private final GameServerTaskManager taskManager;
	private final TableManager tableManager;
	
	public ServerStatus(GameServerLoginService loginService,
			GameServerUserDisconnectService disconnectService,
			 GameServerTaskManager taskManager,
			TableManager tableManager) {
		this.loginService = loginService;
		this.disconnectService = disconnectService;
		this.taskManager = taskManager;
		this.tableManager = tableManager;
	}
	
	@Override
	public int getNumberOfUsers() {
		return loginService.getLoggedInUsersCount();
	}

	@Override
	public int getNumberOfTables() {
		return tableManager.getTablesCount();
	}

	@Override
	public int getUptime() {
    	RuntimeMXBean mx = ManagementFactory.getRuntimeMXBean();
    	return (int)(mx.getUptime() / 1000l);
	}
	
	@Override
	public void stop() {
		try {
			taskManager.stop();
		} catch (Throwable e) {
			log.error("Error while shutting down server", e);
		}
	}
	
	@Override
	public void reloadConfig() {
		Config.reload();
	}
	
	@Override
	public void disconnectUser(long userId) {
		disconnectService.markForDisconnect(userId);
	}
}
