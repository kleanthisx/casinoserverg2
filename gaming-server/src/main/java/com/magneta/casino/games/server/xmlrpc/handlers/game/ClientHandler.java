package com.magneta.casino.games.server.xmlrpc.handlers.game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestValidationService;
import com.magneta.casino.games.server.session.SessionProperties;
import com.magneta.casino.games.server.xmlrpc.annotations.XmlRpcHandler;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.beans.GameClientGameBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.GamePlayer;

/**
 * ClientHandler provides the necessary methods for handling the clients interacting with the server.
 */
@XmlRpcHandler(context="game", name="Client")
public class ClientHandler {
	
    @Inject
    private GameServerRequest request;
    
	@InjectService("GameServerRequestValidationService")
	private GameServerRequestValidationService validationService;
	
	@InjectService("GameClientService")
	private GameClientService clientService;
	
	@InjectService("GameServerPrincipalService")
	private GameServerPrincipalService principalService;

    /**
     * Gets the list of games for the requesting client.
     * @return A List containing a Map for each game type. 
     * This Map includes the 'category'(String) field and the 'library' (String) field.
     * @throws XmlRpcException
     */
    public List<Map<String,Object>> getGameList() throws XmlRpcException {
    	validationService.validateRequest(request);

    	Integer clientId = request.getClientId();

    	SearchContext<GameClientGameBean> searchContext =
    		FiqlContextBuilder.create(GameClientGameBean.class, "enabled==?", true);

    	ServiceResultSet<GameClientGameBean> gameResult;

    	try {
    		principalService.pushSystem();
    		gameResult = clientService.getGameClientGames(clientId, searchContext, 0, 0, null);
    	} catch (ServiceException e) {
    		throw new XmlRpcException(500, "Internal Server Error.");
    	} finally {
    		principalService.popSystem();
    	}

    	List<Map<String,Object>> clientGames = new ArrayList<Map<String,Object>>();
    	List<GameClientGameBean> gameBeans = gameResult.getResult();

    	for (GameClientGameBean bean: gameBeans) {
    		Map<String,Object> currGame = new HashMap<String,Object>();

    		currGame.put("game_id", bean.getGameId());
    		currGame.put("name", bean.getName());
    		currGame.put("category", bean.getCategoryId());

    		clientGames.add(currGame);
    	}

        return clientGames;
    }
    
    public List<Map<String,Object>> getUserGamesList() throws XmlRpcException {
    	validationService.validateRequest(request);

    	Integer clientId = request.getClientId();
    	GamePlayer player = request.getPlayer();

    	SearchContext<GameClientGameBean> searchContext =
    		FiqlContextBuilder.create(GameClientGameBean.class, "enabled==?", true);

    	ServiceResultSet<GameClientGameBean> gameResult;

    	try {
    		principalService.pushSystem();
    		gameResult = clientService.getGameClientGames(clientId, searchContext, 0, 0, null);
    	} catch (ServiceException e) {
    		throw new XmlRpcException(500, "Internal Server Error.");
    	} finally {
    		principalService.popSystem();
    	}

    	List<Map<String,Object>> clientGames = new ArrayList<Map<String,Object>>();
    	List<GameClientGameBean> gameBeans = gameResult.getResult();

    	for (GameClientGameBean bean: gameBeans) {
    		Map<String,Object> currGame = new HashMap<String,Object>();

    		currGame.put("game_id", bean.getGameId());
    		currGame.put("name", bean.getName());

    		clientGames.add(currGame);
    	}

    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new XmlRpcException(500, "Internal Server Error");
    	}

    	PreparedStatement statement = null;
    	ResultSet rs = null;

    	try {
    		statement = dbConn.prepareStatement(
    				"SELECT tables.game_type_id AS game_id" +
    				" FROM game_tables As tables" +
    				" INNER JOIN game_table_last_rounds AS last_rounds ON last_rounds.table_id = tables.table_id" +
    				" INNER JOIN game_rounds ON last_rounds.table_id=game_rounds.table_id AND last_rounds.last_round_id=game_rounds.round_id" +
    				" WHERE tables.table_owner = ?" +
    				" AND game_rounds.round_started=true" +
    		" AND tables.date_closed IS NULL");

    		statement.setLong(1, player.getUserId());

    		rs = statement.executeQuery();

    		while(rs.next()) {
    			int gameId = rs.getInt("game_id");

    			for (Map<String, Object> m: clientGames) {
    				int game = (Integer)m.get("game_id");
    				if (game == gameId) {
    					m.put("unfinished", true);
    					break;
    				}
    			}
    		}
    	} catch (SQLException e) {
    		throw new XmlRpcException(500, "Internal Server Error", e);
    	} finally {
    		DbUtil.close(rs);
    		DbUtil.close(statement);
    		DbUtil.close(dbConn);
    	}

    	return clientGames;
    }
    
    /**
     * Verifies the client version. If the method returns false
     * then the client should go through the update process.
     * @param clientId The client id.
     * @param protocolVersion The protocol version of the client.
     * @param version Client specific version (for informational purposes).
     * @throws XmlRpcException 
     * @throws ServiceException 
     */
    public boolean verify(int clientId, int protocolVersion, String version) throws XmlRpcException, ServiceException {    	
    	GameClientBean client;

    	try {
    		principalService.pushSystem();
    		client = clientService.getGameClient(clientId);
    	} catch (ServiceException e) {
    		throw new XmlRpcException(500, "Internal Server Error.", e);
    	} finally {
    		principalService.popSystem();
    	}

    	if (client == null || client.getClientVersion() > protocolVersion) {
    		request.getRequestConfig().setDisconnect();
    		return false;
    	}
    	
    	HttpSession session = request.getSession(true);
    	try {
    		session.setAttribute(SessionProperties.CLIENT_ID, clientId);
    		session.setAttribute(SessionProperties.CLIENT_PROTOCOL_VERSION, protocolVersion);
    		session.setAttribute(SessionProperties.CLIENT_VERSION, version);
    	} catch (IllegalStateException e) {
    		request.getRequestConfig().setDisconnect();
    		throw new XmlRpcException("Invalid session.");
    	}
    	
    	return true;
    }

    public boolean keepAlive() throws XmlRpcException {
    	try {
    		validationService.validateRequest(request);
    	} catch (XmlRpcException e) {
    		request.getRequestConfig().setDisconnect();
    		throw e;
    	}
        return true;
    }
}
