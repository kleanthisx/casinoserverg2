/**
 * 
 */
package com.magneta.casino.games.server;

import com.magneta.games.GameActionRequest;

/**
 * Request for action on a table.
 * 
 */
public class GameActionRequestImpl implements GameActionRequest {

    private final int roundId;
    private final int seatId;
    private final String description;
    private final String area;
    private final double amount;

    /**
     * Constructs a new ActionRequest.
     * @param roundId The Id of the round.
     * @param seatId The seat that the request is about.
     * @param description The description of the action requested. 
     * @param area The action area. It should be left null when none exists.
     * @param amount The amount (money) for the action.
     * It should be set to 0.0 when the action does not have one. 
     */
    public GameActionRequestImpl(int roundId, int seatId, String description, 
            String area, double amount) {
        this.roundId = roundId;
        this.seatId = seatId;
        this.description = description;
        this.area = area;
        this.amount = amount;
    }

    /**
     * Returns the action amount.
     * @return the action amount.
     */
    @Override
    public double getAmount() {
        return amount;
    }

    /**
     * Returns the action area.
     * @return the action area.
     */
    @Override
    public String getArea() {
        return area;
    }

    /**
     * Returns the action description.
     * @return the action description.
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the ID of the round.
     * @return the ID of the round.
     */
    @Override
    public int getRoundId() {
        return roundId;
    }

    /**
     * Returns the ID of the seat.
     * @return the ID of the seat.
     */
    @Override
    public int getSeatId() {
        return seatId;
    }
    
    @Override
    public String toString() {
        String action = "{ "
        		+ " RoundId: " + this.roundId
        		+ ", Seat: " + this.seatId
        		+ ", Desc: " + this.description;
        
        if (this.area != null && this.area.length() > 0) {
            action += ", Area: " + this.area;
        }

        if (this.amount > 0.0) {
            action += ", Amount: " + this.amount;
        }

        action += " }";

        return action;
    }
}
