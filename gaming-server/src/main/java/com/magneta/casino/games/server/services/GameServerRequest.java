package com.magneta.casino.games.server.services;

import javax.servlet.http.HttpSession;

import org.apache.xmlrpc.XmlRpcRequest;

import com.magneta.casino.games.server.xmlrpc.GameServerRequestConfig;
import com.magneta.games.GamePlayer;

public interface GameServerRequest {

	public GameServerRequestConfig getRequestConfig();
	public XmlRpcRequest getRequest();
	
	public HttpSession getSession(boolean create);
	
	public String getRemoteAddr();
	public GamePlayer getPlayer();
	public Integer getClientId();
}
