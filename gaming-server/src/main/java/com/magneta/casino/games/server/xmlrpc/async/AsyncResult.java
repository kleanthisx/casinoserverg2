package com.magneta.casino.games.server.xmlrpc.async;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AsyncResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7543848129793019895L;
	private Integer requestId;
	private Object result;

	public AsyncResult() {
		
	}

	public AsyncResult(Integer requestId, Object result) {
		this.requestId = requestId;
		this.result = result;
	}
	
	@XmlAttribute
	public Integer getRequestId() {
		return requestId;
	}
	
	@XmlElement
	public Object getResult() {
		return result;
	}
	
}
