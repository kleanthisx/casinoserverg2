package com.magneta.casino.games.server.xmlrpc.async;

import java.io.StringWriter;
import java.util.concurrent.Callable;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestService;
import com.magneta.casino.games.server.services.ServiceLocator;

public class AsyncRequestExecution implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(AsyncRequestExecution.class);
	
	private final GameServerRequest request;
	private final int requestId;
	private final Callable<?> implementation;
	private final GameServerPrincipalService principalService;
	private final GameServerRequestService requestService;
	
	public AsyncRequestExecution(GameServerRequest request, int requestId, Callable<?> implementation,
			GameServerPrincipalService principalService,
			GameServerRequestService requestService) {
		this.request = request;
		this.requestId = requestId;
		this.implementation = implementation;
		
		this.principalService = principalService;
		this.requestService = requestService;
	}

	@Override
	public void run() {
		principalService.setPrincipal(request.getPlayer());
		principalService.resetSystem();
		requestService.startRequest(request.getRequest());
		try {
			doRun();
		} finally {
			principalService.setPrincipal(null);
			principalService.resetSystem();
			requestService.endRequest();
			ServiceLocator.getRegistry().cleanupThread();
		}
	}
	
	private void doRun() {
		Object result;
		try {
			result = implementation.call();
			
			try {
				Marshaller marshaller = AsyncRequestMarshaller.getMarshaller(result.getClass());

				StringWriter sw = new StringWriter();
				marshaller.marshal(new AsyncResult(requestId, result), sw);

				request.getPlayer().addAsyncResult(sw.toString());

			} catch (Throwable e) {
				log.error("Unable to marshall async result", e);
			}
			
		} catch (XmlRpcException e) {
			if (e.code == 500 && e.getCause() != null) {
				log.error("Error on async request.", e.getCause());
			}

			StringWriter sw = new StringWriter();
			try {
				Marshaller marshaller = AsyncRequestMarshaller.getMarshaller(AsyncError.class);
				marshaller.marshal(new AsyncError(requestId, e.code, e.getMessage()), sw);
				request.getPlayer().addAsyncResult(sw.toString());
			} catch (JAXBException e1) {
				log.error("Unable to marshall error", e1);
			}

			return;
		} catch (Throwable e) {
			log.error("Unable to execute asynchronous request", e);
			StringWriter sw = new StringWriter();
			try {
				Marshaller marshaller = AsyncRequestMarshaller.getMarshaller(AsyncError.class);
				marshaller.marshal(new AsyncError(requestId, 500, "Internal server error"), sw);
				request.getPlayer().addAsyncResult(sw.toString());
			} catch (JAXBException e1) {
				log.error("Unable to marshall error", e1);
			}
			return;
		}
	}
}
