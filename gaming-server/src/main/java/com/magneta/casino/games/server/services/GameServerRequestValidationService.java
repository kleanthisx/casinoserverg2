package com.magneta.casino.games.server.services;

import org.apache.xmlrpc.XmlRpcException;

public interface GameServerRequestValidationService {

    /**
     * Validates a request.
     * 
     * Checks:
     * - Whether the user is logged in.
     * - Whether the user has been disconnected due to administrator.
     * - Whether the user has been disconnected due to inactivity.
     * 
     * @throws XmlRpcException
     */
	public void validateRequest(GameServerRequest request) throws XmlRpcException;
}
