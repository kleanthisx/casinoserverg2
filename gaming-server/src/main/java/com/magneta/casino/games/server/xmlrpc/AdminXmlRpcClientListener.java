/**
 * 
 */
package com.magneta.casino.games.server.xmlrpc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerTaskManager;
import com.magneta.xmlrpc.server.SocketXmlRpcClientListener;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;

/**
 * @author Nicos
 *
 */
public class AdminXmlRpcClientListener implements SocketXmlRpcClientListener {

	private final GameServerTaskManager taskManager;
	private static final Logger log = LoggerFactory.getLogger(AdminXmlRpcClientListener.class);
	
	public AdminXmlRpcClientListener(GameServerTaskManager taskManager) {
		this.taskManager = taskManager;
	}
	
    @Override
    public void onClientConnected(SocketXmlRpcRequestConfig requestConfig) {        
    }

    @Override
    public void onClientDisconnected(SocketXmlRpcRequestConfig requestConfig) {
    }

	@Override
	public void onShutdownRequested(SocketXmlRpcRequestConfig requestConfig) {
		log.info("Client listener: shut down requested");
		try {
			taskManager.stop();
		} catch (Exception e) {
			log.error("Error while attempting shutdown", e);
		}
	}

}
