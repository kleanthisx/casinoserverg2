package com.magneta.casino.games.server.services.impl;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.magneta.casino.games.server.services.GameServerUserDisconnectService;

public class GameServerUserDisconnectServiceImpl implements GameServerUserDisconnectService {

	private final List<Long> usersToDisconnect;
	
	public GameServerUserDisconnectServiceImpl() {
		usersToDisconnect = new CopyOnWriteArrayList<Long>();
	}
	
	@Override
	public void markForDisconnect(Long userId) {
		usersToDisconnect.add(userId);
	}

	@Override
	public boolean shouldDisconnect(Long userId) {
		return usersToDisconnect.remove(userId);
	}
}
