package com.magneta.casino.games.server.xmlrpc.socket;

import java.net.InetSocketAddress;

import org.eclipse.jetty.server.SessionManager;

import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfigFactory;

public class SocketXmlRpcRequestConfigFactoryImpl implements SocketXmlRpcRequestConfigFactory {

	private final SessionManager sessionManager;
	
	public SocketXmlRpcRequestConfigFactoryImpl(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
	
	@Override
	public SocketXmlRpcRequestConfig createRequestConfig(InetSocketAddress localAddress, InetSocketAddress remoteAddress) {
		return new SocketXmlRpcRequestConfigImpl(localAddress, remoteAddress, sessionManager);
	}

}
