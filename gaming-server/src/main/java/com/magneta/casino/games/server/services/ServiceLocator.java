/**
 * 
 */
package com.magneta.casino.games.server.services;

import org.apache.tapestry5.ioc.Registry;
import org.apache.tapestry5.ioc.RegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author anarxia
 *
 */
public class ServiceLocator {
	
	private static final Logger log = LoggerFactory.getLogger(ServiceLocator.class);

    private static Registry registry;

    public static void init() {
    	log.info("ServiceLocator: initializing");

        RegistryBuilder builder = new RegistryBuilder();
        builder.add(GameServerAppModule.class);
        
        registry = builder.build();
        registry.performRegistryStartup();
    }

    public static void shutdown() {
    	registry.cleanupThread();
        registry.shutdown();
        registry = null;
        log.info("ServiceLocator: shutdown");
    }

    public static <T> T getService(Class<T> arg0) {
        if (registry == null) {
            log.error("Registry is null!!!!");
        }
        
        return registry.getService(arg0);
    }

    public static Registry getRegistry() {
    	return registry;
    }
    
    public static boolean isInit() {
    	return registry != null;
    }
}
