package com.magneta.casino.games.server.services;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.UserDetailsBean;

public interface GameServerLoginService {

	public int getLoggedInUsersCount();
	public boolean recordLogin(UserDetailsBean userDetails, String userAgent) throws ServiceException;
	public void recordLogout(UserDetailsBean userDetails, String sessionId);
}
