package com.magneta.casino.games.server.services;

import org.apache.xmlrpc.XmlRpcRequest;

public interface GameServerRequestService {
	
	public void startRequest(XmlRpcRequest request);
	public void endRequest();
	
	public GameServerRequest getRequest();
}
