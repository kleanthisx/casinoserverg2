package com.magneta.casino.games.server.session;

import com.magneta.games.GamePlayer;

/**
 * SessionListener interface listens for events caused by users sessions.
 */
public interface GamePlayerLogoutListener {

    /**
     * Handles the event of a player logging out.
     * @param The player who just logged out.
     */
    public void onLogout(GamePlayer player);
}
