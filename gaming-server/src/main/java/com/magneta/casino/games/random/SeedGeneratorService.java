package com.magneta.casino.games.random;

public interface SeedGeneratorService {

	byte[] getSeed(int noBytes);
}
