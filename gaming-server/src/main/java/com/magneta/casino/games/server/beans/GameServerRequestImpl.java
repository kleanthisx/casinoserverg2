package com.magneta.casino.games.server.beans;

import javax.servlet.http.HttpSession;

import org.apache.xmlrpc.XmlRpcRequest;

import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.session.SessionProperties;
import com.magneta.casino.games.server.xmlrpc.GameServerRequestConfig;
import com.magneta.games.GamePlayer;

public class GameServerRequestImpl implements GameServerRequest {

	private final XmlRpcRequest request;
	private final GameServerRequestConfig requestConfig;
	
	public GameServerRequestImpl(XmlRpcRequest request) {
		this.request = request;
		this.requestConfig = (GameServerRequestConfig)request.getConfig();
	}
	
	@Override
	public GameServerRequestConfig getRequestConfig() {
		return this.requestConfig;
	}
	
	@Override
	public XmlRpcRequest getRequest() {
		return this.request;
	}

	@Override
	public HttpSession getSession(boolean create) {
		return requestConfig.getSession(create);
	}

	@Override
	public String getRemoteAddr() {
		return requestConfig.getRemoteIp();
	}

	@Override
	public GamePlayer getPlayer() {
		HttpSession session = getSession(false);
		
		if (session == null) {
			return null;
		}
		
		try {
			return (GamePlayer)session.getAttribute(SessionProperties.PLAYER);
		} catch (IllegalStateException e) {
			return null;
		}
	}
	
	@Override
	public Integer getClientId() {
		HttpSession session = getSession(false);
		
		if (session == null) {
			return null;
		}
		
		try {
			return (Integer)session.getAttribute(SessionProperties.CLIENT_ID);
		} catch (IllegalStateException e) {
			return null;
		}
	}
}
