package com.magneta.casino.games.server.services.impl;

import org.apache.xmlrpc.XmlRpcRequest;

import com.magneta.casino.games.server.beans.GameServerRequestImpl;
import com.magneta.casino.games.server.services.GameServerRequest;
import com.magneta.casino.games.server.services.GameServerRequestService;

public class GameServerRequestServiceImpl implements GameServerRequestService {

	private final ThreadLocal<GameServerRequest> request;
	
	public GameServerRequestServiceImpl() {
		request = new ThreadLocal<GameServerRequest>();
	}
	
	@Override
	public void startRequest(XmlRpcRequest req) {
		request.set(new GameServerRequestImpl(req));
	}
	
	@Override
	public GameServerRequest getRequest() {
		return request.get();
	}
	
	@Override
	public void endRequest() {
		request.remove();
	}

}
