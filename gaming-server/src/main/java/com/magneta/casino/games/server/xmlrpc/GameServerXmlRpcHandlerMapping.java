package com.magneta.casino.games.server.xmlrpc;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.AbstractReflectiveHandlerMapping;

public class GameServerXmlRpcHandlerMapping extends AbstractReflectiveHandlerMapping {
	
	public GameServerXmlRpcHandlerMapping() {
		super();
	}

	public void registerHandlerClass(String pKey,
    		Class<?> pType) throws XmlRpcException {
		registerPublicMethods(pKey, pType);
	}
}
