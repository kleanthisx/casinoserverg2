package com.magneta.casino.games.server.xmlrpc.async;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class AsyncRequestMarshaller {

	private static final Map<Class<?>, JAXBContext> contextCache;
	
	static {
		contextCache = new ConcurrentHashMap<Class<?>, JAXBContext>();
		try {
			contextCache.put(AsyncError.class, JAXBContext.newInstance(AsyncError.class));
			contextCache.put(AsyncResult.class, JAXBContext.newInstance(AsyncResult.class));
		} catch (JAXBException e) {
			
		}
	}
	
	public static JAXBContext getContext(Class<?> klass) throws JAXBException {
		JAXBContext context = contextCache.get(klass);
		
		if (context == null) {
			synchronized(contextCache) {
				context = contextCache.get(klass);
				
				if (context == null) {
					context = JAXBContext.newInstance(klass,AsyncResult.class);
					contextCache.put(klass, context);
				}
			}
		}
		
		return context;
	}
	
	public static Marshaller getMarshaller(Class<?> klass) throws JAXBException {
		JAXBContext context = getContext(klass);
		
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		return marshaller;
	}
}
