package com.magneta.casino.games.server.xmlrpc.async;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.AsyncExecutor;
import com.magneta.casino.games.server.services.GameServerPrincipalService;
import com.magneta.casino.games.server.services.GameServerRequestService;
import com.magneta.games.server.ServerTask;

public class AsyncExecutorImpl implements AsyncExecutor, ServerTask {
	
	private static final Logger log = LoggerFactory.getLogger(AsyncExecutorImpl.class);
	
	private static final int POOL_SIZE = 2;
	private static final int MAX_POOL_SIZE = 5;
	private static final int MAX_QUEUE_SIZE = 50;
	private static final long KEEP_ALIVE = 10l;
	
	private ExecutorService executor;
	private boolean stopped;
	private AtomicInteger count;
	
	private final GameServerPrincipalService principalService;
	private final GameServerRequestService requestService;
	
	public AsyncExecutorImpl(GameServerPrincipalService principalService, GameServerRequestService requestService) {
		this.principalService = principalService;
		this.requestService = requestService;
		stopped = false;
		count = new AtomicInteger();
	}

	@Override
	public void start() {
		this.executor = new ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(MAX_QUEUE_SIZE));
		stopped = false;
	}

	@Override
	public void stop() {
		this.executor.shutdownNow();
		stopped = true;
	}

	@Override
	public void join() throws InterruptedException {
		while (!this.executor.awaitTermination(1, TimeUnit.HOURS)) {}
	}
	
	@Override
	public Integer execute(Callable<?> command) {
		if (this.stopped) {
			return null;
		}
		
		int requestId = count.addAndGet(1);
		AsyncRequestExecution execution = new AsyncRequestExecution(requestService.getRequest(), requestId, command, principalService, requestService); 
		
		try {
			this.executor.execute(execution);
			return requestId;
		} catch (Throwable e) {
			log.error("Unable to schedule async task", e);
			return null;
		}
	}

	@Override
	public String getName() {
		return "Async Request Executor";
	}
}
