package com.magneta.casino.games.server.services;

public interface GameServerContext {

	int getServerId();

	boolean isSuspended();
	void suspend();
}
