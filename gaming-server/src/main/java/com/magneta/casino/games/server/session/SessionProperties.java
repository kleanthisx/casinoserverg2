package com.magneta.casino.games.server.session;


/**
 * Class SessionProperties sets and gets the attributes of a users session. 
 */
public final class SessionProperties {
    
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_PROTOCOL_VERSION = "client_protocol_version";
    public static final String CLIENT_VERSION = "client_version";

    public static final String PLAYER = "player";
}
