package com.magneta.casino.games.server.services;

import com.magneta.casino.games.server.beans.GameRoundBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.games.configuration.GameConfiguration;

public interface GameRoundPersistanceService {

	void createRound(long tableId, int roundId, byte[] seed,
			GameConfiguration config) throws ServiceException;
	
	GameRoundBean loadRound(long tableId, int roundId) throws ServiceException;

	void roundUpdateConfig(long tableId, int roundId, Long newConfigId)
			throws ServiceException;
}
