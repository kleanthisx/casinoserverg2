package com.magneta.casino.games.server.xmlrpc.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.security.Principal;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.eclipse.jetty.server.SessionManager;

import com.magneta.casino.games.server.session.SessionProperties;
import com.magneta.casino.games.server.xmlrpc.GameServerRequestConfig;

public class SocketXmlRpcRequestConfigImpl extends GameServerRequestConfig implements HttpServletRequest {

	private HttpSession session;
	private final SessionManager sessionManager;
	private Map<String, Object> attributes;
	
	private final InetSocketAddress localAddress;
	private final InetSocketAddress remoteAddress;
	
	public SocketXmlRpcRequestConfigImpl(InetSocketAddress localAddress, InetSocketAddress remoteAddress, SessionManager sessionManager) {
		this.localAddress = localAddress;
		this.remoteAddress = remoteAddress;
		this.sessionManager = sessionManager;
	}
	
	@Override
	public HttpSession getSession(boolean create) {
		if (create && session == null) {
			session = sessionManager.newHttpSession(this);
			session.setMaxInactiveInterval(-1);
		}
		
		return session;
	}

	@Override
	public String getRemoteIp() {
		return remoteAddress.getAddress().getHostAddress();
	}

	@Override
	public Object getAttribute(String name) {
		if (this.attributes == null) {
			return null;
		}
		return attributes.get(name);
	}

	private static class IteratorEnumeration implements Enumeration<String> {

		private final Iterator<String> iter;
		
		public IteratorEnumeration(Iterator<String> iter) {
			this.iter = iter;
		}
		
		@Override
		public boolean hasMoreElements() {
			return iter.hasNext();
		}

		@Override
		public String nextElement() {
			return iter.next();
		}
	}
	
	@Override
	public Enumeration<String> getAttributeNames() {
		if (attributes == null) {
			return EMPTY_ENUM;
		}
		
		return new IteratorEnumeration(attributes.keySet().iterator());
	}

	@Override
	public String getCharacterEncoding() {
		return "UTF-8";
	}

	@Override
	public void setCharacterEncoding(String env)
			throws UnsupportedEncodingException {
	}

	@Override
	public int getContentLength() {
		return -1;
	}

	@Override
	public String getContentType() {
		return "text/xml";
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		throw new IOException("Unable to getInputStream for socket request");
	}

	@Override
	public String getParameter(String name) {
		return null;
	}

	@Override
	public Enumeration<String> getParameterNames() {
		return EMPTY_ENUM;
	}

	@Override
	public String[] getParameterValues(String name) {
		return null;
	}

	private static final Map<String, String[]> reqMap = new HashMap<String, String[]>();
	
	@Override
	public Map<String,String[]> getParameterMap() {
		return reqMap;
	}

	@Override
	public String getProtocol() {
		return null;
	}

	@Override
	public String getScheme() {
		return "socketxmlrpc";
	}

	@Override
	public String getServerName() {
		return "server";
	}

	@Override
	public int getServerPort() {
		return localAddress.getPort();
	}

	@Override
	public BufferedReader getReader() throws IOException {
		throw new IOException("Cannot getReader for socket request");
	}

	@Override
	public String getRemoteAddr() {
		return remoteAddress.getAddress().getHostAddress();
	}

	@Override
	public String getRemoteHost() {
		return remoteAddress.getAddress().getHostAddress();
	}

	@Override
	public void setAttribute(String name, Object o) {
		if (this.attributes == null) {
			attributes = new HashMap<String, Object>();
		}
		
		attributes.put(name, o);
	}

	@Override
	public void removeAttribute(String name) {
		if (attributes == null) {
			return;
		}

		attributes.remove(name);
	}

	@Override
	public Locale getLocale() {
		return Locale.US;
	}

	private static final Enumeration<Locale> LOCALES_ENUM;
	
	static {
		Vector<Locale> locs = new Vector<Locale>(1);
		locs.add(Locale.US);
		LOCALES_ENUM = locs.elements();
	}
	
	@Override
	public Enumeration<Locale> getLocales() {
		return LOCALES_ENUM;
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String path) {
		return null;
	}

	@Deprecated
	@Override
	public String getRealPath(String path) {
		return null;
	}

	@Override
	public int getRemotePort() {
		return remoteAddress.getPort();
	}

	@Override
	public String getLocalName() {
		return localAddress.getAddress().getHostName();
	}

	@Override
	public String getLocalAddr() {
		return localAddress.getAddress().getHostAddress();
	}

	@Override
	public int getLocalPort() {
		return localAddress.getPort();
	}

	@Override
	public String getAuthType() {
		return null;
	}

	@Override
	public Cookie[] getCookies() {
		return null;
	}

	@Override
	public long getDateHeader(String name) {
		return -1;
	}

	@Override
	public String getHeader(String name) {
		return null;
	}

	private static final Enumeration<String> EMPTY_ENUM = new Vector<String>(1).elements();
	
	@Override
	public Enumeration<String> getHeaders(String name) {
		return EMPTY_ENUM;
	}

	@Override
	public Enumeration<String> getHeaderNames() {
		return null;
	}

	@Override
	public int getIntHeader(String name) {
		return -1;
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public String getPathInfo() {
		return null;
	}

	@Override
	public String getPathTranslated() {
		return null;
	}

	@Override
	public String getContextPath() {
		return "";
	}

	@Override
	public String getQueryString() {
		return null;
	}

	@Override
	public String getRemoteUser() {
		return null;
	}

	@Override
	public boolean isUserInRole(String role) {
		return false;
	}

	@Override
	public Principal getUserPrincipal() {
		if (this.session == null) {
			return null;
		}
		
		try {
			return (Principal)session.getAttribute(SessionProperties.PLAYER);
		} catch (IllegalStateException e) {
			return null;
		}
	}

	@Override
	public String getRequestedSessionId() {
		return null;
	}

	@Override
	public String getRequestURI() {
		return null;
	}

	@Override
	public StringBuffer getRequestURL() {
		return null;
	}

	@Override
	public String getServletPath() {
		return null;
	}

	@Override
	public HttpSession getSession() {
		return getSession(true);
	}

	@Override
	public boolean isRequestedSessionIdValid() {
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromCookie() {
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromURL() {
		return false;
	}

	@Deprecated
	@Override
	public boolean isRequestedSessionIdFromUrl() {
		return false;
	}

	@Override
	public ServletContext getServletContext() {
		return null;
	}

	@Override
	public AsyncContext startAsync() throws IllegalStateException {
		return null;
	}

	@Override
	public AsyncContext startAsync(ServletRequest servletRequest,
			ServletResponse servletResponse) throws IllegalStateException {
		return null;
	}

	@Override
	public boolean isAsyncStarted() {
		return false;
	}

	@Override
	public boolean isAsyncSupported() {
		return false;
	}

	@Override
	public AsyncContext getAsyncContext() {
		return null;
	}

	@Override
	public DispatcherType getDispatcherType() {
		return null;
	}

	@Override
	public boolean authenticate(HttpServletResponse response)
			throws IOException, ServletException {
		return false;
	}

	@Override
	public void login(String username, String password) throws ServletException {
		throw new ServletException("Login not supported");
	}

	@Override
	public void logout() throws ServletException {
	}

	@Override
	public Collection<Part> getParts() throws IOException, ServletException {
		return null;
	}

	@Override
	public Part getPart(String name) throws IOException, ServletException {
		return null;
	}

}
