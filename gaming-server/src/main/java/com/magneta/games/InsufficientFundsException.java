package com.magneta.games;

public class InsufficientFundsException extends ActionFailedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2669988782041772090L;

	public InsufficientFundsException() {
		super("Insufficient Funds");
	}
}
