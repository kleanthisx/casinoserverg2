package com.magneta.games;

import java.util.Map;

public class GameSettings {

    private final Map<String,Object> settings;
    
    public GameSettings(Map<String,Object> settings) {
        this.settings = settings;
    }
    
    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> klass) {
        return (T)settings.get(key);
    }
    
    public int getInt(String key) {
        return get(key, Integer.TYPE);
    }
    
    public double getDouble(String key) {
        return get(key, Double.TYPE);
    }
    
    public boolean getBoolean(String key) {
        return get(key, Boolean.TYPE);
    }
}
