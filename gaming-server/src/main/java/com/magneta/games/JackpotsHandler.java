/**
 * 
 */
package com.magneta.games;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

/**
 * @author User
 *
 */
public class JackpotsHandler {
    
	private static final Logger log = LoggerFactory.getLogger(JackpotsHandler.class);
	
	private static final double MIN_JACKPOT_PAYOUT = 0.10;
	
	/**
	 * This method should be called after the bet action has been committed.
	 * This is because it requests information from the database in order to calculate
	 * the jackpot contribution. I the bet action is not committed, the bes is considered 0
	 * and so no contribution will be made to the jackpot.
	 * @param round
	 * @param action
	 * @return
	 */
    public static boolean contributeToJackpot(final GameRound round, final GameAction action) {
        
        if (action.isLoaded())
            return false;

        GamePlayer player = action.getPlayer();
        
        if (player == null) {
            log.error("Round {}-{}: Passed action without player to contributeToJackpot. Ignoring", round.getTableId(), round.getId());
            return false;
        }
        
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Could not get db connection to contribute to jackpot");
            return false;
        }
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            double amount = FilterBetFromBonus.getCleanMoney(conn, action);
            
            if (amount <= 0.0) {
                return false;
            }
            
            String sql = 
                "SELECT jackpot_contrib(?,?,?,?,?,?)";
            
            stmt = conn.prepareStatement(sql);
            stmt.setLong(1, player.getUserId());
            stmt.setLong(2, player.getCorpId()==null?0L:player.getCorpId());
            stmt.setInt(3, round.getGame().getGameId());
            stmt.setLong(4, round.getTableId());
            stmt.setLong(5, round.getId());
            stmt.setBigDecimal(6, new BigDecimal(amount));
            rs = stmt.executeQuery();

            if (rs.next()) {
                return rs.getBoolean(1);
            }
            
        } catch (SQLException e) {
            log.warn("Error while contributing to jackpot.", e);
        } finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }

        return false; 
    }
    
    public static boolean claimMysteryJackpot(Random random, final GameRound round, GameAction parentAction, double betAmount, double ratio){    	
        if (!round.getGameTemplate().supportsMysteryJackpot()){
            return false;
        }
        
        if (parentAction.isLoaded()) {
            return false;
        }
        
        if (parentAction.getPlayer() == null) {
            log.error("Round {}-{}: Passed action without player to claimMysteryJackpot. Ignoring", round.getTableId(), round.getId());
            return false;
        }
        
        //Check for jackpot
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Could not get connection to claim mystery jackpot");
            return false;
        }
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(
                    "SELECT jackpots.jackpot_id, draw_number, game_id, target_corporate"+
                    " FROM jackpots"+
                    " INNER JOIN jackpot_balances ON jackpots.jackpot_id = jackpot_balances.jackpot_id"+
                    " WHERE finish_date IS NULL"+
                    " AND (target_corporate = ? OR target_corporate IS NULL)"+
                    " AND (game_id IS NULL OR game_id = ?)"+
                    " AND auto=FALSE"+
                    " AND (jackpots.min_jackpot_balance IS NULL OR jackpot_balances.balance >= jackpots.min_jackpot_balance)"+
                    " AND (jackpots.min_payout IS NULL OR jackpot_balances.balance * ? >= jackpots.min_payout)"+
                    " AND (jackpot_balances.balance * ? >= ?)"+
                    " ORDER BY COALESCE(game_id,0), COALESCE(target_corporate,0)");
            
            
            stmt.setObject(1, parentAction.getPlayer().getCorpId());
            stmt.setInt(2, round.getGame().getGameId());
            stmt.setDouble(3, ratio);
            stmt.setDouble(4, ratio);
            stmt.setDouble(5, MIN_JACKPOT_PAYOUT);
            
            rs = stmt.executeQuery();

            while (rs.next()){
                long corpID = rs.getLong("target_corporate");
                int gameID = rs.getInt("game_id");      
                int drawNumber = rs.getInt("draw_number");
                long jackpotId = rs.getLong("jackpot_id");

                int medium = drawNumber / 2;
                
                /* claim jackpot
                 * If draw number is not correctly configured
                 * skip the draw
                 */
                if (drawNumber > 0 && (random.nextInt(drawNumber) == medium)){
                    GameAction winAction = parentAction.createGeneratedAction(parentAction.getPlayer(), 1, AbstractGameRound.WIN_ACTION, null, 0.0);

                    winAction.setAmountFunction("handle_mystery_jackpot_win("+ jackpotId + "," + round.getTableId() + "," + round.getId() + "," + parentAction.getPlayer().getUserId()+ ","+ ratio + ")");
                    
                    String jackpotType = "";
                    if (corpID <= 0 && gameID <=0){
                        jackpotType = "Mega ";
                    } else if (corpID > 0){
                        jackpotType = "Corporate ";
                    } else {
                        jackpotType = "Mystery ";
                    }
                    winAction.setActionValue(jackpotType+"Jackpot");
                    GameAction finishAction = parentAction.createGeneratedAction(null, 0, AbstractGameRound.FINISH_ACTION, null, 0.0);
                    try {
                        parentAction.commit(true);
                        return true;
                    } catch (ActionFailedException e){
                        parentAction.removeGeneratedAction(winAction);
                        parentAction.removeGeneratedAction(finishAction);
                    }
                }
            }
        } catch (SQLException e){
            log.error("Error checking for mystery jackpots.",e);
        } finally{
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
        
        return false;
    }

    public static boolean claimGameJackpot(final GameRound round, GameAction parentAction, Integer area, double normalWinAmount, double ratio) {
        if (!round.getGameTemplate().supportsGameJackpot()){
            return false;
        }
        
        if (parentAction.isLoaded()) {
            return false;
        }
        
        if (parentAction.getPlayer() == null) {
        	log.warn("Rounrd {}-{}: Passed action without player to claimGameJackpot. Ignoring", round.getTableId(), round.getId());
            return false;
        }
        
        //Check for jackpot
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Could not get connection to claim game jackpot");
            return false;
        }
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(
                    "SELECT jackpots.jackpot_id"+
                    " FROM jackpots"+
                    " INNER JOIN jackpot_balances ON jackpots.jackpot_id = jackpot_balances.jackpot_id"+
                    " WHERE finish_date IS NULL"+
                    " AND target_corporate IS NULL"+
                    " AND game_id = ?"+
                    " AND auto=TRUE"+
                    " AND (jackpots.min_jackpot_balance IS NULL OR jackpot_balances.balance >= jackpots.min_jackpot_balance)"+
                    " AND (jackpots.min_payout IS NULL OR jackpot_balances.balance * ? >= jackpots.min_payout)"+
                    " AND (jackpot_balances.balance * ?) >= ?" +
                    " AND (jackpot_balances.balance * ?) >= ?");

            stmt.setInt(1, round.getGame().getGameId());
            stmt.setDouble(2, ratio);
            stmt.setDouble(3, ratio);
            stmt.setDouble(4, normalWinAmount);
            stmt.setDouble(5, ratio);
            stmt.setDouble(6, MIN_JACKPOT_PAYOUT);
            rs = stmt.executeQuery();

            if (rs.next()){
                log.debug("Jackpot ratio is: "+ratio);

                GameAction winAction;

                if (area != null && area >= 0) {
                	winAction = parentAction.createGeneratedAction(parentAction.getPlayer(),
                			1, AbstractGameRound.WIN_ACTION, area.toString(), 0.0);
                } else {
                	winAction = parentAction.createGeneratedAction(parentAction.getPlayer(),
                			1, AbstractGameRound.WIN_ACTION, null, 0.0);
                }

                
                winAction.setAmountFunction(
                             "handle_ratio_jackpot_win("+parentAction.getPlayer().getUserId()+","+round.getGame().getGameId()+","+round.getTableId()+","+round.getId()+","+ratio+")");
               
                winAction.setActionValue("Jackpot");
                
                try {
                    winAction.commit();
                    return true;
                } catch (ActionFailedException e) {
                    log.debug("Error while handling jackpot win.", e);
                    parentAction.removeGeneratedAction(winAction);
                }
            }
        } catch (SQLException e){
            log.error("Error checking for mystery jackpots.",e);
        } finally{
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
        return false;
    }
    
    public static boolean claimGameJackpot(final GameRound round, GameAction parentAction, double ratio) {
        if (!round.getGameTemplate().supportsGameJackpot()) {
            return false;
        }
        
        if (parentAction.isLoaded()) {
            return false;
        }
        
        if (parentAction.getPlayer() == null) {
            log.error("Round {}-{}: Passed action without player to claimGameJackpot. Ignoring", round.getTableId(), round.getId());
            return false;
        }
                
        //Check for jackpot
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Could not get connection to claim game jackpot");
            return false;
        }
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(
                    "SELECT jackpots.jackpot_id"+
                    " FROM jackpots"+
                    " INNER JOIN jackpot_balances ON jackpots.jackpot_id = jackpot_balances.jackpot_id"+
                    " WHERE finish_date IS NULL"+
                    " AND target_corporate IS NULL"+
                    " AND game_id = ?"+
                    " AND auto=TRUE");

            stmt.setInt(1, round.getGame().getGameId());
            rs = stmt.executeQuery();

            if (rs.next()){
                log.debug("Jackpot ratio is: {}", ratio);

                GameAction winAction = parentAction.createGeneratedAction(parentAction.getPlayer(),
                			1, AbstractGameRound.WIN_ACTION, null, 0.0);

                
                winAction.setAmountFunction(
                            "handle_ratio_jackpot_win("+parentAction.getPlayer().getUserId()+","+round.getGame().getGameId()+","+round.getTableId()+","+round.getId()+","+ratio+")");
                
                winAction.setActionValue("Jackpot");
                
                try {
                    winAction.commit();
                    return true;
                } catch (ActionFailedException e) {
                    log.debug("Error while handling jackpot win.", e);
                    parentAction.removeGeneratedAction(winAction);
                }
            }
        } catch (SQLException e){
            log.error("Error checking for mystery jackpots.",e);
        } finally{
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
        return false;
    }
    
    public static boolean claimGameJackpotAmount(final GameRound round, GameAction parentAction, double amount) {
        if (!round.getGameTemplate().supportsGameJackpot()){
            return false;
        }
        
        if (parentAction.isLoaded()) {
            return false;
        }
        
        if (parentAction.getPlayer() == null) {
            log.error("Round {}-{}: Passed action without player to claimGameJackpotAmount. Ignoring", round.getTableId(), round.getId());
            return false;
        }        
        
        //Check for jackpot
        Connection conn = ConnectionFactory.getConnection();
        if (conn == null) {
            log.error("Could not get connection to claim game jackpot");
            return false;
        }
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(
                    "SELECT jackpots.jackpot_id"+
                    " FROM jackpots"+
                    " INNER JOIN jackpot_balances ON jackpots.jackpot_id = jackpot_balances.jackpot_id"+
                    " WHERE finish_date IS NULL"+
                    " AND target_corporate IS NULL"+
                    " AND game_id = ?"+
                    " AND auto=TRUE");

            stmt.setInt(1, round.getGame().getGameId());
            rs = stmt.executeQuery();

            if (rs.next()){
                log.debug("Jackpot amount is: {}", amount);

                GameAction winAction = parentAction.createGeneratedAction(parentAction.getPlayer(),
                			1, AbstractGameRound.WIN_ACTION, null, 0.0);

                
                winAction.setAmountFunction(
                            "handle_amount_jackpot_win("+parentAction.getPlayer().getUserId()+","+round.getGame().getGameId()+","+round.getTableId()+","+round.getId()+","+amount+")");
                
                winAction.setActionValue("Jackpot");
                
                try {
                    winAction.commit();
                    return true;
                } catch (ActionFailedException e) {
                    log.debug("Error while handling jackpot win.", e);
                    parentAction.removeGeneratedAction(winAction);
                }
            }
        } catch (SQLException e){
            log.error("Error checking for mystery jackpots.",e);
        } finally{
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
        return false;
    }
}