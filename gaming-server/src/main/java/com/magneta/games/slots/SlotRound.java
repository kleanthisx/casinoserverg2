/**
 * 
 */
package com.magneta.games.slots;

import java.util.Map;
import java.util.Random;

import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.slots.SlotConfiguration;

/**
 * @author anarxia
 *
 */
public abstract class SlotRound extends AbstractGameRound {

    private static final String REEL_VIEW_ACTION = "g:REEL_VIEW";    

    @Override
	public SlotConfiguration getConfig() {
    	return (SlotConfiguration)super.getConfig();
    }
    
    /**
     * Creates a g:REEL_VIEW action for the given reel stop
     * @param action The parent action.
     * @param reelView The reelView
     */
    public final GameAction createReelViewAction(GameAction action, int[][] reelView) {
        /* rolling the wheels */
        StringBuilder reelStopValueBuilder = new StringBuilder();

        for (int i=0;i<reelView[0].length;i++) {
            for (int j=0; j < reelView.length; j++) {
                reelStopValueBuilder.append(String.valueOf(reelView[j][i]));

                if (j < reelView.length - 1 || i < reelView[0].length -1) {
                    reelStopValueBuilder.append(" ");
                }
            }
        }

        GameAction reelStopAction = action.createGeneratedAction(null, 0, REEL_VIEW_ACTION, null, 0.0);
        reelStopAction.setActionValue(reelStopValueBuilder.toString());
        
        return reelStopAction;
    }

    /**
     * Spins the reels.
     * @param roundReels The reels to spin.
     * @return An array with the stop index on each reel.
     */
    protected final int[] spinReels(int[][] roundReels) {
        Random random = getRandom();
        int[] reelStops = new int[roundReels.length];

        for (int i=0;i<reelStops.length;i++){
            reelStops[i] = random.nextInt(roundReels[i].length);
        }
        
        String fixedResult = RoundFixedResultUtil.getFixedResult(getTableId(), getId());
        if (fixedResult != null) {
            String[] sReelStops = fixedResult.split(" ");
            int reelStop;
            
            for (int i=0; i < sReelStops.length && i < reelStops.length; i++) {
                try {
                    reelStop = Integer.parseInt(sReelStops[i]);
                } catch (NumberFormatException e) {
                    logError("Invalid number while parsing fixed reel slots");
                    continue;
                }
                if (reelStop >= 0 && reelStop < roundReels[i].length) {
                    reelStops[i] = reelStop;
                }
            }
        }

        return reelStops;
    }
    
    /**
     * Calculates the reel view for the given reelStop. The reel view
     * is a 2-dimensional integer array of size 3 x <number of reels> with
     * the symbols at each position of the view
     * @param roundReels
     * @param reelStops
     * @return
     */
    public final int[][] calculateReelView(int[][] roundReels, int[] reelStops) {
        //get the view
        int[][] reelsView = new int[3][roundReels.length];
        for (int i=0;i<roundReels.length;i++){
            int reelStop = reelStops[i];

            reelsView[0][i] = roundReels[i][(reelStop > 0 ? (reelStop - 1) : (roundReels[i].length-1))];
            reelsView[1][i] = roundReels[i][reelStop];
            reelsView[2][i] = roundReels[i][(reelStop+1) % roundReels[i].length];
        }

        return reelsView;
    }
    
    @Override
    protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
        super.getExtraInfo(player, roundInfo, firstTime);

        if (firstTime) {
            roundInfo.put("combos", getConfig().getWinCombos());
        }
    }
    
    protected final void getPayline(int[][] view, int line, int[] lineDest) {
    	getConfig().getPayline(view, line, lineDest);
    }
}
