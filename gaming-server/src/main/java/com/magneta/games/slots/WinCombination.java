/**
 * 
 */
package com.magneta.games.slots;


/**
 * @author anarxia
 *
 */
public class WinCombination {

    private int payline;
    private int combination;

    public WinCombination() {
        super();
    }
    
    public WinCombination(int payline, int combination) {
        this.payline = payline;
        this.combination = combination;
    }
    
    /**
     * @param payline The payline to set.
     */
    public void setPayline(int payline) {
        this.payline = payline;
    }
    
    /**
     * @return Returns the payline.
     */
    public int getPayline() {
        return payline;
    }
    
    /**
     * @param combination The combination to set.
     */
    public void setCombination(int combination) {
        this.combination = combination;
    }
    
    /**
     * @return Returns the combination.
     */
    public int getCombination() {
        return combination;
    }
    
    
}
