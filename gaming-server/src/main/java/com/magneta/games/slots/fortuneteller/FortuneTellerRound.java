package com.magneta.games.slots.fortuneteller;

import java.util.ArrayList;
import java.util.List;

import com.magneta.Config;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.FortuneTellerConfiguration;
import com.magneta.games.slots.SlotRound;
import com.magneta.games.slots.WinCombination;

@GameTemplate("FortuneTeller")
public class FortuneTellerRound extends SlotRound {
	
		private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
		
	    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";

	    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	    
	    private static final String SELECT_CARD_ACTION = "g:SELECT_CARD";
	    	    
	    private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";

	    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
	            /* Description Area Amount */
	            new GameActionRequestDefinition(BET_ACTION, ParameterType.REQUIRED,
	                    ParameterType.REQUIRED),
	            new GameActionRequestDefinition(START_FREE_SPIN_ACTION,
	                    ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
	            new GameActionRequestDefinition(SELECT_CARD_ACTION,
	            	    ParameterType.REQUIRED, ParameterType.FORBIDDEN)};

	    private static final int WILD_SYMBOL = FortuneTellerConfiguration.WILD_SYMBOL;
	    
	    private static final int NORMAL_MODE = 0;
	    private static final int BONUS_MODE = 1;

	    private int freeSpinsRemaining;

	    private boolean finished;

	    private double betAmount;

	    private int maxPayline;

	    private boolean freeSpin;

	    private boolean placedJackpotContrib;
	    
	    private int gameMode;
	    
	    private int bonusStep;
	    
	    private double baseBonusWin;
	    
	    public FortuneTellerRound() {
	        this.betAmount = 0.0;
	        this.maxPayline = -1;
	        this.freeSpinsRemaining = 0;
	        this.finished = false;
	        this.freeSpin = false;
	        this.placedJackpotContrib = false;
	        this.gameMode = NORMAL_MODE;
	        this.bonusStep = 0;
	        this.baseBonusWin = 0.0;
	    }
	    
	    @Override
		public FortuneTellerConfiguration getConfig() {
	    	return (FortuneTellerConfiguration)super.getConfig();
	    }

	    @Override
		public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
	    	FortuneTellerRound parent = (FortuneTellerRound) parentRound;
	    	
	        this.freeSpinsRemaining = parent.freeSpinsRemaining;

	        if (this.freeSpinsRemaining > 0) {
	            this.betAmount = parent.betAmount;
	            this.maxPayline = parent.maxPayline;

	            GameAction freeSpinAction = new GameAction(null, this, 0,
	                    FREE_SPIN_ACTION, maxPayline + " " + betAmount, 0.0);
	            freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
	            freeSpinAction.setRoundStart();

	            freeSpinAction.commit();
	            actionCompleted(freeSpinAction);
	            this.freeSpin = true;
	        }
	    }
	    
	    @Override
	    protected void validateGameActionRequest(GameActionRequest request)
	            throws ActionFailedException {

	        /* Check if the action is "syntacticaly" valid */
	        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

	        if (request.getSeatId() != 1) {
	            throw new ActionFailedException("Wrong seat ID (Should be 1).");
	        } else if (finished) {
	            throw new ActionFailedException("Round has finished.");
	        }
	    }

	    @Override
	    protected void loadAction(GameAction action)
	            throws ActionFailedException {
	        if (FREE_SPIN_ACTION.equals(action.getDescription())) {
	            try {
	                this.freeSpinsRemaining = Integer.parseInt(action
	                        .getActionValue());
	                this.freeSpin = true;
	                String[] tokens = action.getArea().split(" ");

	                if (tokens.length == 2) {
	                    this.maxPayline = Integer.parseInt(tokens[0]);
	                    this.betAmount = Double.parseDouble(tokens[1]);
	                } else {
	                    throw new ActionFailedException(
	                            "Free spin load failed. Values missing.");
	                }
	            } catch (NumberFormatException e) {
	                throw new ActionFailedException(
	                        "Free spin load failed. An invalid value was passed.");
	            }

	            actionCompleted(action);

	        } else {
	            doAction(action);
	        }
	    }

	    @Override
	    protected void performAction(GameAction action)
	            throws ActionFailedException {
	        if (BET_ACTION.equals(action.getDescription())) {
	            performBet(action);
	        } else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
	            performFreeSpin(action);
	        } else if (SELECT_CARD_ACTION.equals(action.getDescription())) {
	        	performCardSelect(action);
	        }
	    }
	    
	    
	    private void performCardSelect(GameAction action) throws ActionFailedException {
	    	boolean lost = false;

	        if (finished || (gameMode != BONUS_MODE)) {
	            throw new ActionFailedException("Invalid state: Card select is only allowed after round has started and while in bonus mode.");
	        }
	        	        
	        int area = -1;
	        try {
	            area = Integer.parseInt(action.getArea());
	        } catch (NumberFormatException e){
	            area = -1;
	        }
	        
	        if(area < 0 || area > bonusStep + 1) {
	            throw new ActionFailedException("Invalid area.");
	        }
	        	        
	        boolean cards[] = new boolean[bonusStep + 2];
	        cards[this.getRandom().nextInt(bonusStep + 2)] = true;
	        
	        /*User won*/
	        
	        
	        double wins = 0.0;
	        
	        /* We add the base bonus win if its the first time */
        	if(bonusStep == 0) {
        		wins += this.baseBonusWin;
        	}
	        
	        if(cards[area]) {
	        	wins += ((bonusStep + 1) * this.baseBonusWin);
	        }else{
	        	lost = true;
	        }
	        /* Log bonus boxes */
	        if (Config.getBoolean("games.log.bonus", false)) { 
	        	logInfo("Bonus Won:" + wins + " Step:" + bonusStep + "\n");
	        }
	        
	        if (wins > 0.0) {
	        	GameAction newAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, Integer.toString(bonusStep), wins);
	        	newAction.setActionValue(Integer.toString(lost?0:1));
	        }
	        bonusStep++;
	        if(bonusStep >= 5 || lost) {
	        	endRound(action);
	        }else{
	        	action.commit();
	        }
	    }

	    private void performFreeSpin(GameAction action)
	            throws ActionFailedException {
	        if (!freeSpin) {
	            throw new ActionFailedException("Not in free spin mode!");
	        } else if (gameMode != NORMAL_MODE) {
	            throw new ActionFailedException("Spin not allowed at this time.");
	        }

	        action.commit();
	        this.freeSpinsRemaining--;
	        startRound(action);
	    }

	    /**
	     * Performs the bet action.
	     * 
	     * @param action
	     *            The action request.
	     * @throws ActionFailedException
	     */
	    private void performBet(GameAction action) throws ActionFailedException {
	        if (freeSpin) {
	            throw new ActionFailedException(
	                    "Round is a free spin; No bets allowed.");
	        } else if (betAmount > 0.0) {
	            throw new ActionFailedException("Bet failed: already placed bet!");
	        } else if (gameMode != NORMAL_MODE) {
	            throw new ActionFailedException("Bet not allowed at this time.");
	        }

	        checkBet(action);

	        int area = -1;

	        try {
	            area = Integer.parseInt(action.getArea());
	        } catch (NumberFormatException e) {
	        }

	        if ((area < 0) || (area >= getConfig().getPaylines().length)) {
	            throw new ActionFailedException("Invalid action area");
	        }

	        double betUnit;
	        double totalBet;

	        if (action.isLoaded()) {
	            totalBet = action.getAmount();
	            betUnit = action.getAmount() / (area + 1);
	        } else {
	            betUnit = action.getAmount();
	            totalBet = betUnit * (area + 1);
	            action.setAmount(totalBet);
	        }

	        action.setRoundStart();
	        action.commit();

	        betAmount = betUnit;
	        maxPayline = area;

	        placedJackpotContrib = JackpotsHandler
	                .contributeToJackpot(this, action);

	        startRound(action);
	    }

	    private void startRound(GameAction action) throws ActionFailedException {
	        if (START_FREE_SPIN_ACTION.equals(action.getDescription())
	                && (freeSpinsRemaining < 0)) {
	            this.maxPayline = -1;
	            this.betAmount = 0.0;
	            this.freeSpinsRemaining = 0;
	            this.freeSpin = false;
	            throw new IllegalStateException(
	                    "No free spins remain. Round must be started normally.");
	        } else if (finished) {
	            throw new IllegalStateException(
	                    "Method startRound() already called once.");
	        }

	        double jackpotRatio = (betAmount / options.getMaxBet());
	        if (jackpotRatio > 1.0) {
	            jackpotRatio = 1.0;
	        }

	        if (claimMysteryJackpot(action, betAmount, jackpotRatio,
	                placedJackpotContrib)) {
	            this.finished = true;
	            return;
	        }

	        int[][] reels = getConfig().getReels(freeSpin ? 1: 0);
	                
	        int[] reelStops = spinReels(reels);
	        /*
	         * StringBuilder rS = new StringBuilder("Reel Stops:"); for (int
	         * reelStop: reelStops) { rS.append(" "); rS.append(reelStop); }
	         * Log.info(rS.toString());
	         */
	        int[][] reelsView = calculateReelView(reels, reelStops);

	        /* FIXED ODDS CODE */
	        // com.magneta.games.fixedodd.PharaohGuard.guardRound(game.getId(),
	        // options.getOwner(), freeSpin, roundReels, WIN_CONSTRAINTS,
	        // roundPayouts, WILD_SYMBOL, FREE_SPIN_MULTIPLIER, betAmount,
	        // maxPayline, reelStops, options.getGameSettings(), action);

	        createReelViewAction(action, reelsView);
	              

	        int[] currLine = new int[reels.length];

	        int freeSpins = 0;

	        for (int i = 0; i <= maxPayline; i++) {
	            getPayline(reelsView, i, currLine);

	            int combo = getPaylineWinCombination(currLine);
	            

	            if (combo < 0)
	                continue;

	            double multiplier = getComboPayout(combo);
	            double winAmount = multiplier * betAmount;

	            if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)
	                    && JackpotsHandler.claimGameJackpot(this, action, i,
	                            winAmount, jackpotRatio)) {
	                // stop the game from retrying to claim jackpot on other line
	                placedJackpotContrib = false;
	            } else {
	                if (winAmount > 0.0) {
	                    GameAction winAction = action.createGeneratedAction(
	                            action.getPlayer(), 1, WIN_ACTION,
	                            String.valueOf(i), winAmount);
	                    winAction.setActionValue(String.valueOf(combo));
	                }
	            }
	        }
	        
	        List<WinCombination> specialCombos = getReelViewWinCombination(reelsView);
	        if (specialCombos != null) {
	            for (WinCombination c: specialCombos) {
	                int combo = c.getCombination();
	                Object[] winCombo = getConfig().getWinCombo(combo);
	                Integer symbol = (Integer)winCombo[0];
	                
	                if (symbol.equals(-FortuneTellerConfiguration.BONUS_SYMBOL)) {
	                	this.gameMode = BONUS_MODE;
	                	this.baseBonusWin = this.betAmount * (this.maxPayline + 1) * 5.0;
	                }else if (symbol.equals(-FortuneTellerConfiguration.SCATTER_SYMBOL)) {
		                freeSpins += 5;
	                }
	            }
	        }

	        if(this.gameMode == BONUS_MODE) {
	        	action.createGeneratedAction(action.getPlayer(), 1, TREASURE_BONUS_ACTION, null, 0.0);
	        }
		       
	        if (freeSpins > 0) {
	            GameAction freeSpinsAction = action.createGeneratedAction(null, 0,
	                    FREE_SPINS_WIN_ACTION, "", 0.0);
	            freeSpinsAction.setActionValue(Integer.toString(freeSpins));
	            this.freeSpinsRemaining += freeSpins;
	        }
	        
	        if(this.gameMode != BONUS_MODE) {
		        endRound(action);
	        } else {
	        	action.commit();
	        }

	    }

	    private void endRound(GameAction action) throws ActionFailedException {
	        if (finished) {
	            throw new IllegalStateException(
	                    "Method endRound() already called once.");
	        }

	        action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
	        action.commit(true);
	        this.finished = true;
	    }
	    	    
	    /**
	     * Checks if the given payline forms any winning combinations.
	     * 
	     * @param payline
	     *            The payline to check.
	     * @return The combination the payline forms or -1 if it doesn't form any.
	     */
	    public int getPaylineWinCombination(int[] payline) {
	        Object[][] winCombos = getConfig().getWinCombos();
	        int maxPayoutCombo = -1;
	        
	        for (int i = winCombos.length - 1; i >= 0; i--) {
	            int symbol = (Integer)winCombos[i][0];
	            int quantityRequired = (Integer)winCombos[i][1];

	            if ((symbol != FortuneTellerConfiguration.SCATTER_SYMBOL) && (symbol != FortuneTellerConfiguration.BONUS_SYMBOL) && getComboPayout(i) < 0.01) {
	                continue;
	            }
	            
	            int j;

	            boolean foundSymbol = false;
	            for (j = 0; j < payline.length; j++) {
	                if (payline[j] == symbol) {
	                    foundSymbol = true;
	                } else if ((payline[j] == WILD_SYMBOL)
	                        && (symbol != FortuneTellerConfiguration.SCATTER_SYMBOL)
	                        && (symbol != FortuneTellerConfiguration.BONUS_SYMBOL)) {
	                    continue;
	                } else {
	                    break;
	                }
	            }

	            if ((j == quantityRequired) && foundSymbol) {
	                if (getComboPayout(i) > getComboPayout(maxPayoutCombo)) {
	                    maxPayoutCombo = i;
	                }
	                
	                /* Always prefer free spins and bonus over normal combinations */
	                if (symbol == FortuneTellerConfiguration.BONUS_SYMBOL ||  symbol == FortuneTellerConfiguration.SCATTER_SYMBOL)
	                    return i;
	            }
	        }

	        return maxPayoutCombo;
	    }

	    /**
	     * Checks if the given reel view forms any winning combinations.
	     * 
	     * @param reelView
	     *            The view to check.
	     * @return The combination the view forms or -1 if it doesn't form any.
	     */
	    public final List<WinCombination> getReelViewWinCombination(int[][] reelView) {
	    	List<WinCombination> combos = new ArrayList<WinCombination>();
	        List<Integer> comboSymbols = new ArrayList<Integer>();
	        Object[][] winCombos = getConfig().getWinCombos();

	        for (int i = winCombos.length - 1; i >= 0; i--) {
	            int symbol = (Integer)winCombos[i][0];
	            int quantity = (Integer)winCombos[i][1];

	            if (symbol >= 0)
	                continue;

	            symbol = symbol * -1;
	            
	            /* A combo with the symbol was already found. skip */
	            if (comboSymbols.contains(new Integer(symbol))) {
	                continue;
	            }
	            boolean foundOnLine = false;

	            for (int j = 0; j < reelView[0].length; j++) {
	                foundOnLine = false;
	                for (int k = 0; k < 3; k++) {
	                    if (reelView[k][j] == symbol) {
	                        foundOnLine = true;
	                        break;
	                    }
	                }

	                if (foundOnLine) {
	                    quantity--;
	                }

	                if (quantity <= 0) {
	                    comboSymbols.add(symbol);
	                    combos.add(new WinCombination(j, i));
	                    break;
	                }
	            }

	        }

	        return combos;
	    }

	    @Override
		public boolean isFinished() {
	        return finished;
	    }

	    public double getComboPayout(int combo) {
	        if (combo < 0) {
	            return 0.0;
	        }
	        
	        return getConfig().getPayout(combo);
	    }
}
