package com.magneta.games.slots.darktombs;

import java.util.Map;

import com.magneta.games.DefaultGameActionFilter;
import com.magneta.games.GameAction;

public class DartkTombsActionFilter extends DefaultGameActionFilter {

	public DartkTombsActionFilter() {
		super(false);
	}

	@Override
	public Map<String, Object> filterAction(GameAction action) {
		if (action.getDescription().equals(DarkTombsRound.FREE_SPIN_ACTION_CONT)) {
			return null;
		}
		
		return super.filterAction(action);
	}
}
