package com.magneta.games.slots.otto;

import static com.magneta.games.configuration.slots.OttoConfiguration.BONUS_SYMBOL;
import static com.magneta.games.configuration.slots.OttoConfiguration.WIN_CONSTRAINTS;

import java.util.ArrayList;
import java.util.List;

import com.magneta.Config;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.OttoConfiguration;
import com.magneta.games.slots.BonusBox;
import com.magneta.games.slots.SlotRound;

@GameTemplate("Otto")
public class OttoRound extends SlotRound {

    private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
    private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";
    private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";

    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description             Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
        new GameActionRequestDefinition(BOX_SELECT_ACTION,      ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
        new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
    };

    private static final int NORMAL_MODE = 0;
    private static final int TREASURE_MODE = 1;

    private static final int WILD_SYMBOL = OttoConfiguration.WILD_SYMBOL;

    private static final double FREE_SPIN_MULTIPLIER = 2.0;

    private int freeSpinsRemaining;
    private int gameMode;
    private boolean finished;
    private double betAmount;
    private int maxPayline;

    private BonusBox[][] bonusBoxes;
    private int bonusRow;

    private boolean freeSpin;
    private boolean placedJackpotContrib;

    @Override
	public OttoConfiguration getConfig() {
    	return (OttoConfiguration)super.getConfig();
    }
    
    public OttoRound() {
        this.betAmount = 0.0;
        this.maxPayline = -1;
        this.freeSpinsRemaining = 0;
        this.gameMode = NORMAL_MODE;
        this.finished = false;
        this.freeSpin = false;
        this.placedJackpotContrib = false;
        this.bonusBoxes = null;
    }

    @Override
	protected void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
    	OttoRound parent = (OttoRound)parentRound;

        this.freeSpinsRemaining = parent.freeSpinsRemaining;
        
        if (this.freeSpinsRemaining > 0) {
            this.betAmount = parent.betAmount;
            this.maxPayline = parent.maxPayline;
            
            GameAction freeSpinAction = new GameAction(null, this, 0, FREE_SPIN_ACTION, maxPayline+" "+betAmount, 0.0);
            freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
            freeSpinAction.setRoundStart();

            freeSpinAction.commit();
            actionCompleted(freeSpinAction);
            this.freeSpin = true;
        }
    }
    
    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {

        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    @Override
    protected void loadAction(GameAction action) throws ActionFailedException {
        if (FREE_SPIN_ACTION.equals(action.getDescription())){
            try{
                this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
                this.freeSpin = true;
                String[] tokens = action.getArea().split(" ");

                if (tokens.length == 2){
                    this.maxPayline = Integer.parseInt(tokens[0]);
                    this.betAmount = Double.parseDouble(tokens[1]);
                } else {
                    throw new ActionFailedException("Free spin load failed. Values missing.");
                }
            } catch (NumberFormatException e){
                throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
            }

            actionCompleted(action);

        } else {
            doAction(action);
        }
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        } else if (BOX_SELECT_ACTION.equals(action.getDescription())){
            performBoxSelect(action);
        } else if (START_FREE_SPIN_ACTION.equals(action.getDescription())){
            performFreeSpin(action);
        }
    }

    private void performBoxSelect(GameAction action) throws ActionFailedException{
        if (gameMode != TREASURE_MODE){
            throw new ActionFailedException("Not in treasure bonus mode!");
        }

        int area = -1;
        try {
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e){
            area = -1;
        }

        if ((area < 0) || (area >= bonusBoxes[bonusRow].length)){
            throw new ActionFailedException("Invalid area.");
        }

        if (bonusBoxes[bonusRow][area].isOpened()){
            throw new ActionFailedException("Box already opened!");
        }

        double winAmount = bonusBoxes[bonusRow][area].getPayout();

        if (winAmount > 0.0){
            action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, winAmount);
            if (bonusBoxes[bonusRow][area].isRowStop()) {
                action.setActionValue("1");

                if (this.bonusRow >= bonusBoxes.length - 1) {
                    endRound(action);
                } else {
                    action.commit();
                    bonusBoxes[bonusRow][area].setOpened(true);
                    this.bonusRow++;
                }


            } else {
                action.setActionValue("0");
                action.commit();
                bonusBoxes[bonusRow][area].setOpened(true);
            }

        } else {
            action.setActionValue("1");
            endRound(action);
        }
    }

    /** 
     * Shuffles a bonus box row
     * @param bonusRow
     */
    private void shuffleBonusRow(BonusBox[] bonusBoxRow) {
        BonusBox tmp;

        for (int i=0;i < bonusBoxRow.length;i++){
            int swap = getRandom().nextInt(bonusBoxRow.length);
            tmp = bonusBoxRow[swap];
            bonusBoxRow[swap] = bonusBoxRow[i];
            bonusBoxRow[i] = tmp;
        }
    }

    private static double[] rowRatio = {
        0.075,
        0.1,
        0.125,
        0.2,
        0.2,
        0.3,
    };

    private static double[][] boxRatio = {
        { 0.100, 0.100, 0.125, 0.125, 0.150, 0.150, 0.250},
        { 0.000, 0.100, 0.125, 0.125, 0.200, 0.200, 0.250},
        { 0.000, 0.000, 0.150, 0.150, 0.200, 0.250, 0.250},
        { 0.000, 0.000, 0.000, 0.125, 0.125, 0.250, 0.500},
        { 0.000, 0.000, 0.000, 0.000, 0.250, 0.250, 0.500},
        { 0.000, 0.000, 0.000, 0.000, 0.000, 0.500, 0.500}
    };

    private void initBoxes(List<Integer[]> queuedBonuses) {
        this.bonusBoxes = new BonusBox[6][7];
        this.bonusRow = 0;

        double totalPayout = 0.0;

        /* Calculate total payout for the bonus */
        for (Integer[] queuedBonus: queuedBonuses) {
            totalPayout += getConfig().getPayout(queuedBonus[1]);
        }

        if (this.freeSpin) {
            totalPayout *= FREE_SPIN_MULTIPLIER;
        }
        
        totalPayout *= this.betAmount;
        
        /* Create bonus boxes */
        for (int row=0; row < this.bonusBoxes.length; row++) {
            for (int i=0; i < this.bonusBoxes[row].length; i++) {
                bonusBoxes[row][i] = new BonusBox();
            }
        }

        /* Create n stop row boxes per row */
        for (int row=0 ; row < this.bonusBoxes.length; row++) {
            for (int i=0; i <= row; i++) {
                bonusBoxes[row][i].setRowStop(true);
            }
        }   

        for (BonusBox[] bonusBoxRow: bonusBoxes) {
            shuffleBonusRow(bonusBoxRow);
        }

        /* Distribute payouts per row */
        for (int row=0 ; row < this.bonusBoxes.length; row++) {
            double rowPayout = totalPayout * rowRatio[row];
            for (int i=0; i < this.bonusBoxes[row].length; i++) {
                bonusBoxes[row][i].setPayout(rowPayout * boxRatio[row][i]);
            }
        }   
      
        /* Shuffle boxes */
        for (BonusBox[] bonusBoxRow: bonusBoxes) {
            shuffleBonusRow(bonusBoxRow);
        }
        
        /* Log bonus boxes */
        if (Config.getBoolean("games.log.bonus", false)) { 
        	StringBuilder builder = new StringBuilder();

        	for (int row=0 ; row < this.bonusBoxes.length; row++) {
        		for (int i=0; i < this.bonusBoxes[row].length; i++) {
        			builder.append('[');
        			if (this.bonusBoxes[row][i].isRowStop()) {
        				builder.append('*');
        			}
        			builder.append(this.bonusBoxes[row][i].getPayout());
        			builder.append(']');
        		}
        		builder.append('\n');
        	}
        	logInfo("Bonus boxes:\n" + builder.toString());
        }
    }

    private void performFreeSpin(GameAction action) throws ActionFailedException {
        if (!freeSpin){
            throw new ActionFailedException("Not in free spin mode!");
        } else if (gameMode != NORMAL_MODE) {
            throw new ActionFailedException("Spin not allowed at this time.");
        }

        action.commit();
        this.freeSpinsRemaining--;
        startRound(action);
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (freeSpin) {
            throw new ActionFailedException("Round is a free spin; No bets allowed.");
        } else if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        } else if (gameMode != NORMAL_MODE){
            throw new ActionFailedException("Bet not allowed at this time.");
        } 

        checkBet(action);

        int area = -1;

        try{
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e){
        }

        if ((area < 0) || (area >= getConfig().getPaylines().length)){
            throw new ActionFailedException("Invalid action area");
        }

        double betUnit;
        double totalBet;
        
        if (action.isLoaded()) {
            totalBet = action.getAmount();
            betUnit = action.getAmount() / (area+1);
        } else {
            betUnit = action.getAmount();
            totalBet = betUnit * (area+1);
            action.setAmount(totalBet);
        }

        action.setRoundStart();
        action.commit();

        betAmount = betUnit;
        maxPayline = area;

        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    private void startRound(GameAction action) throws ActionFailedException {
        if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)){
            this.maxPayline = -1;
            this.betAmount = 0.0;
            this.freeSpinsRemaining = 0;
            this.freeSpin = false;
            throw new IllegalStateException("No free spins remain. Round must be started normally.");
        } else if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        logDebug("Starting round...");

        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
            this.finished = true;
            return;
        }

        int[][] roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);

        int[][] reelsView = calculateReelView(roundReels, reelStops);

        /*FIXED ODDS CODE*/
        //com.magneta.games.fixedodd.PharaohGuard.guardRound(game.getId(), options.getOwner(), freeSpin, roundReels, WIN_CONSTRAINTS, roundPayouts, WILD_SYMBOL, FREE_SPIN_MULTIPLIER, betAmount, maxPayline, reelStops, options.getGameSettings(), action);

        createReelViewAction(action, reelsView);

        for (int line = 1; line <= 3; line++) {
            boolean wildReplace = false;

            for (int i=0; i < reelsView.length; i++) {
                if (reelsView[i][line] == WILD_SYMBOL) {
                    wildReplace = true;
                    break;
                }
            }

            if (wildReplace) {
                for (int i=0; i < reelsView.length; i++) {
                    if (reelsView[i][line] != OttoConfiguration.SCATTER_SYMBOL
                            && reelsView[i][line] != 1) {
                        reelsView[i][line] = WILD_SYMBOL;
                    }
                }
            }
        }

        action.commit();

        //int[] lineWinCombo = new int[maxPayline+1];
        int[] currLine = new int[roundReels.length];
        List<Integer[]> queuedBonuses = new ArrayList<Integer[]>(); 

        int freeSpins = 0;

        for (int i=0;i <= maxPayline;i++){
            getPayline(reelsView, i, currLine);

            int combo = getPaylineWinCombination(currLine);

            if (combo < 0)
                continue;

            double multiplier = getConfig().getPayout(combo);
            double winAmount = multiplier * betAmount * (freeSpin ? FREE_SPIN_MULTIPLIER : 1.0);

            if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo) 
                    && JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
                //stop the game from retrying to claim jackpot on other line
                placedJackpotContrib = false;
            } else {
                GameAction winAction = action.createGeneratedAction(
                        action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                winAction.setActionValue(String.valueOf(combo));
                winAction.commit();
            }
            if (((Integer)WIN_CONSTRAINTS[combo][0]) == BONUS_SYMBOL){

                int bonusSymbolCount = (Integer)WIN_CONSTRAINTS[combo][1];
                int bonusCombo = combo;

                /* Set the bonus Combo to the real combo
                 * The combo found is for the combination we must
                 * replace it with the respective bonus win combination.
                 */
                for (int j=WIN_CONSTRAINTS.length-1; j >=0; j--) {
                    if (WIN_CONSTRAINTS[j][0].equals(0)
                            && WIN_CONSTRAINTS[j][1].equals(bonusSymbolCount)) {
                        bonusCombo = j;
                        break;
                    }
                }

                queuedBonuses.add(new Integer[]{i, bonusCombo});
            }

        }

        int viewCombo = getReelViewWinCombination(reelsView);

        if (viewCombo != -1){
            
            /* Scatter gives free spins */
            if (((Integer)WIN_CONSTRAINTS[viewCombo][0]).equals(-11)) {
                int symbolCount = (Integer)WIN_CONSTRAINTS[viewCombo][1];

                if (symbolCount == 3) {
                    freeSpins += 10;
                } else if (symbolCount == 4) {
                    freeSpins += 20;
                } else if (symbolCount == 5) {
                    freeSpins += 50;
                }
            }
            if (freeSpins > 0) {
                GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
                freeSpinsAction.setActionValue(Integer.toString(freeSpins));
                freeSpinsAction.commit();
                this.freeSpinsRemaining += freeSpins;
            }
        }

        if (!queuedBonuses.isEmpty()) {
            for (Integer[] bonus : queuedBonuses) {
                GameAction bonusAction = action.createGeneratedAction(null, 0,
                        TREASURE_BONUS_ACTION, String.valueOf(bonus[0]), 0.0);
                bonusAction.setActionValue(String.valueOf(bonus[1]));
            }

            action.commit();
            initBoxes(queuedBonuses);
            this.gameMode = TREASURE_MODE;
        } else {
            endRound(action);
        }
    }

    private void endRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method endRound() already called once.");
        }

        action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        action.commit(true);
        this.finished = true;
    }

    /**
     * Checks if the given payline forms any winning combinations.
     * @param payline The payline to check.
     * @return The combination the payline forms or -1 if it doesn't form any.
     */
    public static int getPaylineWinCombination(int[] payline) {
        Object[][] winCombos = WIN_CONSTRAINTS;

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            int j;

            boolean foundSymbol = false;
            for (j=0;j<payline.length;j++){
                if (payline[j] == symbol){
                    foundSymbol = true;
                } else if ((payline[j] == WILD_SYMBOL) && (symbol != OttoConfiguration.BONUS_SYMBOL) && (symbol != OttoConfiguration.SCATTER_SYMBOL)) {
                    continue;
                } else {
                    break;
                }
            }

            if ((j == quantityRequired) && foundSymbol){
                return i;
            }
        }        

        return -1;
    }

    /**
     * Checks if the given reel view forms any winning combinations.
     * @param reelView The view to check.
     * @return The combination the view forms or -1 if it doesn't form any.
     */
    public static final int getReelViewWinCombination(int[][] reelView){
        Object[][] winCombos = WIN_CONSTRAINTS;

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantity = (Integer)winCombos[i][1];

            if (symbol >= 0)
                continue;

            symbol = symbol * -1;
            boolean foundOnLine = false;

            for (int j=0;j<reelView[0].length;j++){
                foundOnLine = false;
                for (int k=0;k<3;k++){
                    if (reelView[k][j] == symbol){
                        foundOnLine = true;
                        break;
                    }
                }

                if (!foundOnLine) {
                    quantity = (Integer)winCombos[i][1];
                } else {
                    quantity--;
                }

                if (quantity <= 0){
                    return i;
                }
            }

        }        

        return -1;
    }

    @Override
	public boolean isFinished() {
        return finished;
    }
}
