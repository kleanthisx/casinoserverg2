package com.magneta.games.slots.kingofaztec;

import static com.magneta.games.configuration.slots.KingOfAztecConfiguration.WILD_SYMBOL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.KingOfAztecConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.KingOfAztecGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("KingOfAztec")
public class KingOfAztecRound extends SlotRound {    

	private static final Logger log = LoggerFactory.getLogger(KingOfAztecRound.class);
	
	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
	};


	private boolean finished;
	private double betAmount;
	private int maxPayline;

	private boolean placedJackpotContrib;

	List<Integer> viewCombos;

	private RoundState state;

	private enum RoundState {
		NORMAL
	}

	public KingOfAztecRound() {
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.finished = false;
		this.state = RoundState.NORMAL;
		this.placedJackpotContrib = false;
	}

	@Override
	public KingOfAztecConfiguration getConfig() {
		return (KingOfAztecConfiguration)super.getConfig();
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		performBet(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){

		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)){
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}
	
	public int[][] getWeights(){
		int[][] weights = new int[][]{ 
				{0, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_0, Integer.class) },
				{1, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_1, Integer.class) },
				{2, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_2, Integer.class) },
				{3, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_3, Integer.class) },
				{4, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_4, Integer.class) },
				{5, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_5, Integer.class) },
				{6, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_6, Integer.class) },
				{7, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_7, Integer.class) },
				{8, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_8, Integer.class) },
				{9, this.getConfig().getSetting(KingOfAztecConfiguration.REEL_WEIGHT_9, Integer.class) },
		};
		return weights;
	}

	private void startRound(GameAction action) throws ActionFailedException {
		int reelSet = 0;
		if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		int[][] weights = getWeights();

		reelSet = getReelBasedOnWeights(weights,getRandom());
		int[] reelStops = spinReels(getConfig().getReels(reelSet));
		log.debug("prin : "+Arrays.toString(reelStops));
		int rellsetChosen[] = new int[]{reelSet};
		reelStops = KingOfAztecGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount,maxPayline , rellsetChosen, reelStops, options.getGameSettings(), action, this,getGuardRandom());
		reelSet = rellsetChosen[0];
		
		log.debug("meta : "+Arrays.toString(reelStops));
		evaluatePosition(action, reelSet, jackpotRatio, reelStops,betAmount,maxPayline,new Double[]{(0.0)},true);

		this.state = RoundState.NORMAL;
		endRound(action);

	}

	public void evaluatePosition(GameAction action, int reelSet,
			double jackpotRatio, int[] reelStops,double betAmount2, int maxPayline,Double[] actwinsum, boolean isProduction) throws ActionFailedException {

		// get the reelview 
		int[][] reelsView = calculateReelView(getConfig().getReels(reelSet), reelStops);

		if(isProduction){
			createReelViewAction(action, reelsView);
			action.commit();
		}

		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		// for every line find winning combo
		for (int i=0;i <= maxPayline;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			if (combo < 0)
				continue;

			double multiplier = getRoundMultiplier() * getPaylineMultiplier(currLine) * getComboPayout(combo);
			log.debug("line "+i+ " with multiplier "+multiplier); 
			double winAmount = multiplier * betAmount2;

			if(isProduction){
				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					if (winAmount > 0.0) {
						actwinsum[0] = new Double(actwinsum[0] + winAmount);
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
						winAction.setActionValue(String.valueOf(combo));
						winAction.commit();
					} 
				}

			}else{
				actwinsum[0] = new Double(actwinsum[0] + winAmount);
			}
		}

		viewCombos = getReelViewWinCombinations(getConfig(), reelsView,reelSet);
		// Check the 
		for (Integer viewCombo: viewCombos) {
			if (viewCombo >= 0) {
				double multiplier = getComboPayout(viewCombo);

				double winAmount = multiplier * (betAmount2 * (maxPayline + 1));

				if (winAmount > 0.01) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
					if(isProduction){		
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, null, winAmount);
						winAction.setActionValue(String.valueOf(viewCombo));
					}
				}
			}
		}
	}

	public int getReelBasedOnWeights(int[][] reelset,Random random) {
		int sum = 0;

		for (int[] w: reelset) {
			sum += w[1];
		}

		int pos = random.nextInt(sum);

		for(int i=0; i < reelset.length; i++) {
			pos -= reelset[i][1];

			if (pos <= 0) {
				return reelset[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if (getComboPayout(i) < 0.01) {
				continue;
			}

			int j;

			boolean foundSymbol = false;
			for (j=0;j<payline.length;j++){
				if (payline[j] == symbol){
					foundSymbol = true;
				} else if (payline[j] == WILD_SYMBOL) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol){
				return i;
			}
		}        

		return -1;
	}

	public double getRoundMultiplier() {
		double multiplier = 1.0;
		return multiplier;
	}

	public double getPaylineMultiplier(int [] payline) {
		return 1.0;
	}

	/**
	 * Checks if the given reel view forms any scatter combinations.
	 * @param reelView The view to check.
	 * @return The combinations the view form.
	 */
	public static final List<Integer> getReelViewWinCombinations(SlotConfiguration config, int[][] reelView,int reelset){

		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			if (symbol >= 0)
				continue;

			symbol = symbol * -1;
			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}

			/* Scatter symbol wins */
			for (int j=0;j < config.getNumberOfReels(reelset);j++){
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol || reelView[k][j] == WILD_SYMBOL){
						quantity--;
						break;
					}
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}        

		return combinations;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	public Double getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}

		return getConfig().getPayout(combo);
	}

	public double  evaluatePositionWinnings(int reelSet, int[]reelStops) throws ActionFailedException{
		Double[] res = new Double[]{(0.0)};
		evaluatePosition(null, reelSet, 0.0, reelStops, betAmount, maxPayline,res, false);
		return res[0];
	}

}
