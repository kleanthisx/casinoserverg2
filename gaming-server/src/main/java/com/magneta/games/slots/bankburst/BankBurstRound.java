package com.magneta.games.slots.bankburst;

import static com.magneta.games.configuration.slots.BankBurstConfiguration.BANK_BURST_SCATTER;
import static com.magneta.games.configuration.slots.BankBurstConfiguration.CODE_SCATTER;
import static com.magneta.games.configuration.slots.BankBurstConfiguration.FREE_SPIN_SCATTER;
import static com.magneta.games.configuration.slots.BankBurstConfiguration.WILD_SYMBOL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.BankBurstConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.BankburstGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("BankBurst")
public class BankBurstRound extends SlotRound {
	

	private static final Logger log = LoggerFactory.getLogger(BankBurstRound.class);

	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";

	private static final String CODE_SCATTER_PROMPT = "g:CODE_PROMPT";
	private static final String FREE_SPIN_SCATTER_PROMPT = "g:FREE_SPIN_PROMPT";

	private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";
	private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";
	private static final String CODE_SCATTER_SELECT_ACTION = "g:CODE_SELECT";
	private static final String FREE_SPIN_SELECT_ACTION = "g:FREE_SPIN_SELECT";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(FREE_SPIN_SELECT_ACTION, ParameterType.REQUIRED, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(CODE_SCATTER_SELECT_ACTION, ParameterType.REQUIRED, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(BOX_SELECT_ACTION, ParameterType.REQUIRED, ParameterType.FORBIDDEN),
	};

	private int freeSpinsRemaining;
	private boolean finished;
	private double betAmount;
	private int maxPayline;

	private boolean freeSpin;

	private boolean placedJackpotContrib;

	List<Integer> viewCombos;
	private Integer currentSelectCombo;

	private RoundState state;

	private enum RoundState {
		NORMAL,
		CODE_SCATTER_SELECT,
		FREE_SPIN_SCATTER_SELECT,
		BONUS_MODE
	}

	public BankBurstRound() {
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.freeSpinsRemaining = 0;
		this.finished = false;
		this.state = RoundState.NORMAL;
		this.freeSpin = false;
		this.placedJackpotContrib = false;
	}

	@Override
	public BankBurstConfiguration getConfig() {
		return (BankBurstConfiguration)super.getConfig();
	}

	@Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		BankBurstRound parent = (BankBurstRound)parentRound;

		this.freeSpinsRemaining = parent.freeSpinsRemaining;

		if (this.freeSpinsRemaining > 0){
			this.betAmount = parent.betAmount;
			this.maxPayline = parent.maxPayline;

			GameAction freeSpinAction = new GameAction(null, this, 0, FREE_SPIN_ACTION, maxPayline+" "+betAmount, 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();

			freeSpinAction.commit();
			this.freeSpin = true;
			actionCompleted(freeSpinAction);
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	protected void loadAction(GameAction action) throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())){
			try{
				this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
				this.freeSpin = true;
				String[] tokens = action.getArea().split(" ");

				if (tokens.length == 2){
					this.maxPayline = Integer.parseInt(tokens[0]);
					this.betAmount = Double.parseDouble(tokens[1]);
				} else {
					throw new ActionFailedException("Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e){
				throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
			}

			actionCompleted(action);

		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())){
			performFreeSpin(action);
		} else if (CODE_SCATTER_SELECT_ACTION.equals(action.getDescription())) {
			performCodeScatterSelect(action);
		} else if (BOX_SELECT_ACTION.equals(action.getDescription())) {
			performBoxSelect(action);
		} else if (FREE_SPIN_SELECT_ACTION.equals(action.getDescription())) {
			performFreeSpinSelect(action);
		}
	}

	private void performBoxSelect(GameAction action) throws ActionFailedException {
		if (state != RoundState.BONUS_MODE) {
			throw new ActionFailedException("Not in treasure bonus mode!");
		}

		try {
			Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
			throw new ActionFailedException("Invalid option number");
		}

		double bonusWins = 0.0;


		for (int i=0; i < 3; i++) {
			int bankBurstBonusIndex = (Integer)getConfig().getWinCombo(this.currentSelectCombo)[1];

			int[][] bankBurstBonus = BankBurstConfiguration.BANK_BURST_BONUS_WEIGHTS[bankBurstBonusIndex - 3];

			bankBurstBonus = BankburstGuard.getPossibleBankBonus(bankBurstBonus,betAmount * (this.maxPayline + 1),game.getGameId(), this.options.getOwner(),this.options.getGameSettings());
			
			int multiplier = getWeightedRandom(bankBurstBonus,getRandom());
			if(multiplier >= 0) { 
				action.setActionValue(Integer.toString(multiplier));

				bonusWins += multiplier * (betAmount * (this.maxPayline + 1));
				break;
			}
			currentSelectCombo++;
		}

		if (bonusWins > 0.0) {
			GameAction winAction = action.createGeneratedAction(
					action.getPlayer(), 1, WIN_ACTION, null, bonusWins);
			winAction.setActionValue(String.valueOf(this.currentSelectCombo));
			action.commit();
		}

		proceedRound(action);
	}

	public int getWeightedRandom(int[][] freeSpin, Random random) {
		int sum = 0;

		for (int[] w: freeSpin) {
			sum += w[1];
		}

		int pos = random.nextInt(sum);

		for(int i=0; i < freeSpin.length; i++) {
			pos -= freeSpin[i][1];

			if (pos <= 0) {
				return freeSpin[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}

	private void performFreeSpinSelect(GameAction action) throws ActionFailedException {
		if (state != RoundState.FREE_SPIN_SCATTER_SELECT) {
			throw new ActionFailedException("Not in free spin select mode!");
		}

		int area = -1;
		try {
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
			area = -1;
		}

		if ((area < 0) || (area >= getConfig().getNumberOfReels(0) * 3)) {
			throw new ActionFailedException("Invalid area.");
		}

		int freeSpins = getWeightedRandom(BankBurstConfiguration.FREE_SPIN_WEIGHTS,getRandom()); 

		action.setActionValue("FREE " + freeSpins);
		GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
		freeSpinsAction.setActionValue(Integer.toString(freeSpins));
		action.commit();
		this.freeSpinsRemaining += freeSpins;

		proceedRound(action);
	}   

	private void performCodeScatterSelect(GameAction action) throws ActionFailedException {
		if (this.state != RoundState.CODE_SCATTER_SELECT) {
			throw new ActionFailedException("Not in code scatter select mode!");
		}

		try {
			Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
			throw new ActionFailedException("Invalid option number");
		}

		int codeBonusIndex = (Integer)getConfig().getWinCombo(this.currentSelectCombo)[1];

		int[][] configCodeBonus = BankBurstConfiguration.CODE_BONUS_WEIGHTS[codeBonusIndex - 3];

		int[][] codeBonus = BankburstGuard.getPossibleCodeBonus(configCodeBonus,betAmount * (this.maxPayline + 1),game.getGameId(), this.options.getOwner(),this.options.getGameSettings());
		
		int multiplier = getWeightedRandom(codeBonus,getRandom());
		action.setActionValue(Integer.toString(multiplier));


		logDebug("MULTIPLIER:" + multiplier + " BETAMOUNT:"+ betAmount + " PAYLINES:" + (maxPayline + 1));

		double winAmount = multiplier * betAmount * (maxPayline + 1);

		if (winAmount > 0.0) {
			GameAction winAction = action.createGeneratedAction(
					action.getPlayer(), 1, WIN_ACTION, null, winAmount);
			winAction.setActionValue(String.valueOf(this.currentSelectCombo));
			action.commit();
		}
		proceedRound(action);
	}

	private void performFreeSpin(GameAction action) throws ActionFailedException {
		if (!freeSpin){
			throw new ActionFailedException("Not in free spin mode!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if (freeSpin) {
			throw new ActionFailedException("Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)){
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void proceedRound(GameAction action) throws ActionFailedException {
		if (!viewCombos.isEmpty()) {
			currentSelectCombo = viewCombos.remove(0);
			Object[] winCombo = getConfig().getWinCombo(currentSelectCombo);
			Integer symbol = (Integer)winCombo[0];

			if (symbol < 0) {
				symbol = symbol * -1;
			}

			if (symbol.equals(BankBurstConfiguration.CODE_SCATTER)) {
				this.state = RoundState.CODE_SCATTER_SELECT;
			} else if (symbol.equals(BankBurstConfiguration.FREE_SPIN_SCATTER)) {
				this.state = RoundState.FREE_SPIN_SCATTER_SELECT;
			} else if (symbol.equals(BankBurstConfiguration.BANK_BURST_SCATTER)) {
				this.state = RoundState.BONUS_MODE;
			} else {
				throw new RuntimeException("Unknown bonus");
			}
		} else {
			this.state = RoundState.NORMAL;
		}

		if (this.state == RoundState.CODE_SCATTER_SELECT) {
			GameAction codeScatterAction = action.createGeneratedAction(null, 0, CODE_SCATTER_PROMPT, null, 0.0);
			codeScatterAction.setActionValue(Integer.toString(currentSelectCombo));
			action.commit();
		} else if(this.state == RoundState.FREE_SPIN_SCATTER_SELECT) {
			GameAction freeSpinScatterAction = action.createGeneratedAction(null, 0, FREE_SPIN_SCATTER_PROMPT, null, 0.0);
			freeSpinScatterAction.setActionValue(Integer.toString(currentSelectCombo));
			action.commit();
		} else if (this.state == RoundState.BONUS_MODE) {
			int bonusSymbolCount = (Integer)getConfig().getWinCombo(currentSelectCombo)[1];
			int bonusCombo = currentSelectCombo;

			/*
			 * Set the bonus Combo to the real combo. The combo found is
			 * for the combination we must replace it with the
			 * respective bonus win combination.
			 */
			Object[][] WIN_CONSTRAINTS = getConfig().getWinCombos();

			for (int j = WIN_CONSTRAINTS.length - 1; j >= 0; j--) {
				if (WIN_CONSTRAINTS[j][0].equals(0) && WIN_CONSTRAINTS[j][1]
				                                                          .equals(bonusSymbolCount)) {
					bonusCombo = j;
					break;
				}
			}

			GameAction bonusAction = action.createGeneratedAction(null, 0,
					TREASURE_BONUS_ACTION, null, 0.0);
			bonusAction.setActionValue(String.valueOf(bonusCombo));
			action.commit();
		} else {
			endRound(action);
		}
	}

	private void startRound(GameAction action) throws ActionFailedException {
		int reelSet = 0;
		if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)) {
			throw new IllegalStateException("No free spins remain. Round must be started normally.");
		} else if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		if(freeSpin) {
			reelSet = BankBurstConfiguration.FREE_SPIN_REEL_SET;
		} else {
			int[][] weights = getWeights();

			reelSet = getWeightedRandom(weights,getRandom()); 
		}

		int[] reelStops = spinReels(getConfig().getReels(reelSet));

		log.debug("prin : "+Arrays.toString(reelStops));
		int[] rellsetChosen = new int[]{reelSet};
		reelStops = BankburstGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, rellsetChosen, reelStops, maxPayline, options.getGameSettings(), action, this,getGuardRandom());
		reelSet = rellsetChosen[0];
		log.debug("meta : "+Arrays.toString(reelStops));
		evaluateposition(action, reelSet, jackpotRatio, reelStops,maxPayline ,true);

		proceedRound(action);
	}

	public int[][] getWeights() {
		int[][] weights = { 
				{0, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_0, Integer.class) },
				{1, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_1, Integer.class) },
				{2, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_2, Integer.class) },
				{3, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_3, Integer.class) },
		};
		return weights;
	}

	public double evaluateposition(GameAction action, int reelSet,
			double jackpotRatio, int[] reelStops,int maxPayline, boolean isProduction) throws ActionFailedException {

		double amountWon = 0.0;

		int[][] reelsView = calculateReelView(getConfig().getReels(reelSet), reelStops);
		
		if(isProduction){
			createReelViewAction(action, reelsView);
			action.commit();
		}
		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		for (int i=0;i <= maxPayline;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			if (combo < 0)
				continue;

			double multiplier = getRoundMultiplier() * getPaylineMultiplier(currLine) * getComboPayout(combo);

			double winAmount = multiplier * betAmount;
			if(isProduction){
				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					if (winAmount > 0.0) {
						amountWon += winAmount;
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
						winAction.setActionValue(String.valueOf(combo));
						winAction.commit();
					} 
				}
			}else{
				amountWon += winAmount;
			}
		}

		viewCombos = getReelViewWinCombinations(getConfig(), reelsView,reelSet);

		for (Integer viewCombo: viewCombos) {
			if (viewCombo >= 0) {
				double multiplier = getComboPayout(viewCombo);

				double winAmount = multiplier * (betAmount * (this.maxPayline + 1));

				if (winAmount > 0.01) {
					amountWon += winAmount;
					if(isProduction){
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, null, winAmount);
						winAction.setActionValue(String.valueOf(viewCombo));
					}
				}
			}
		}

		return amountWon;
	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if (getComboPayout(i) < 0.01) {
				continue;
			}

			int j;

			boolean foundSymbol = false;
			for (j=0;j<payline.length;j++){
				if (payline[j] == symbol){
					foundSymbol = true;
				} else if ((payline[j] == WILD_SYMBOL) && !(symbol == BANK_BURST_SCATTER || symbol == CODE_SCATTER || symbol == FREE_SPIN_SCATTER)) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol){
				return i;
			}
		}        

		return -1;
	}

	public double getRoundMultiplier() {
		double multiplier = 1.0;
		return multiplier;
	}

	public double getPaylineMultiplier(int [] payline) {
		return 1.0;
	}

	/**
	 * Checks if the given reel view forms any scatter combinations.
	 * @param reelView The view to check.
	 * @return The combinations the view form.
	 */
	public static final List<Integer> getReelViewWinCombinations(SlotConfiguration config, int[][] reelView,int reelset){

		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			if (symbol >= 0)
				continue;

			symbol = symbol * -1;
			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}
			boolean foundOnLine = false;

			/* Scatter symbol wins */
			for (int j=0;j < config.getNumberOfReels(reelset);j++){
				foundOnLine = false;
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol){
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}        

		return combinations;
	}
	@Override
	public boolean isFinished() {
		return finished;
	}

	public Double getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}

		return getConfig().getPayout(combo);
	}

	public double evaluatePositionWinnings(int reelSet, int[] reelStops,int linesPlayed) throws ActionFailedException {
		double winnings = 0.0;
		winnings = this.evaluateposition(null, reelSet, 0.0, reelStops,linesPlayed, false);
		return winnings;
	}
}
