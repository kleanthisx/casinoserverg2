package com.magneta.games.slots.minibar;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.MiniBarConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("MiniBar")
public class MiniBarRound extends SlotRound {

    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description        Area                      Amount                */
        new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED)
    };

    private double betAmount;
    private boolean finished;
    private boolean placedJackpotContrib;
    
    
    @Override
	public MiniBarConfiguration getConfig() {
    	return (MiniBarConfiguration)super.getConfig();
    }
    
    public MiniBarRound() {
        this.betAmount = 0.0;
        this.finished = false;
    }

    @Override
    protected void performAction(GameAction action)
    throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        }
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }

        checkBet(action);

        action.setRoundStart();
        action.commit();        
        betAmount = action.getAmount();

        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    /**
     * Starts and finishes the current round for this game.
     * @param action The cause action for the round start.
     * @throws ActionFailedException 
     */
    private void startRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        double userRatio = 0.0;
        if (betAmount >= (100 * 0.02)){
            userRatio = 1.0;
        } else if (betAmount >= (60 * 0.02)){
            userRatio = 0.5;
        } else {
            userRatio = 0.1;
        }
        if (claimMysteryJackpot(action, betAmount, userRatio, placedJackpotContrib)){
            this.finished = true;
            return ;
        }

        int[][] roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);

        /* FIXED ODD CODE */
        com.magneta.games.fixedodd.MiniBarGuard.guardRound(this.game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reelStops, this.options.getGameSettings(), action);

        int[][] reelsView = calculateReelView(roundReels, reelStops);
        
        createReelViewAction(action, reelsView);

        GameAction finishAction = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        finishAction.commit();

        int[] currLine = new int[3];
        for (int i=0; i < getConfig().getPaylines().length; i++) {
            getPayline(reelsView, i, currLine);

            int combo = getPaylineWinCombination(getConfig(), currLine);
            if (combo != -1){
                double multiplier = getComboPayout(combo);
                double winAmount = multiplier * betAmount;
                GameAction winAction = finishAction.createGeneratedAction(
                        action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                winAction.setActionValue(String.valueOf(combo));
                winAction.commit();
            }
        }

        action.commit(true);
        this.finished = true;   
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request)
    throws ActionFailedException {

        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    /**
     * Checks if the given line forms any winning combinations.
     * @param payline The line to check.
     * @return The combination the line forms or -1 if it doesn't form any.
     */
    public static int getPaylineWinCombination(SlotConfiguration config, int[] payline){
        Object[][] winCombos = config.getWinCombos();

        for (int i=winCombos.length-1;i>=0;i--){
            int reel1 = (Integer)winCombos[i][0];

            if (reel1 >= 0){ 
                boolean match = true;

                for (int j=0;j<3;j++){
                    if ((payline[j] != ((Integer)winCombos[i][j])) 
                            && (((Integer)winCombos[i][j]) != 0)){
                        match = false;
                        break;
                    }
                }

                if (match){
                    return i;
                }
            }
        }        

        return -1;
    }

    public double getComboPayout(int combo) {
        return getConfig().getPayout(combo);
    }
}
