package com.magneta.games.slots.chefmaster;

import java.util.ArrayList;
import java.util.List;

import com.magneta.Config;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.ChefMasterConfiguration;
import com.magneta.games.slots.BonusBox;
import com.magneta.games.slots.SlotRound;
import com.magneta.games.slots.WinCombination;

@GameTemplate("ChefMaster")
public class ChefMasterRound extends SlotRound {

	private static enum GameMode {
		NORMAL_MODE,
		TREASURE_MODE,
		FINISHED
	}
	
		private static final int BONUS_RANDOMNESS = 50;

		private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";

	    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";

	    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";

	    private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";

	    private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";
	    
		private static final String EXPANDING_WILD_ACTION = "g:EXPANDING_WILD";

	    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
	            /* Description Area Amount */
	            new GameActionRequestDefinition(BET_ACTION, ParameterType.REQUIRED,
	                    ParameterType.REQUIRED),
	            new GameActionRequestDefinition(BOX_SELECT_ACTION,
	                    ParameterType.REQUIRED, ParameterType.FORBIDDEN),
	            new GameActionRequestDefinition(START_FREE_SPIN_ACTION,
	                    ParameterType.FORBIDDEN, ParameterType.FORBIDDEN) };


	    private static final int WILD_SYMBOL = ChefMasterConfiguration.WILD_SYMBOL;

	    private static final double FREE_SPIN_MULTIPLIER = 2.0;
	    
		private final CurrencyParser currencyParser;

	    private int freeSpinsRemaining;

	    private GameMode gameMode;

	    private double betAmount;

	    private int maxPayline;

	    private BonusBox[] bonusBoxes;

	    private int bonusBoxesToOpen;

	    private boolean freeSpin;

	    private boolean placedJackpotContrib;
	    
	    public ChefMasterRound(CurrencyParser currencyParser) {
	    	this.currencyParser = currencyParser;
	    	
	        this.betAmount = 0.0;
	        this.maxPayline = -1;
	        this.freeSpinsRemaining = 0;
	        this.gameMode = GameMode.NORMAL_MODE;
	        this.freeSpin = false;
	        this.placedJackpotContrib = false;
	        this.bonusBoxes = null;
	    }

	    @Override
		public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
	    	ChefMasterRound parent = (ChefMasterRound)parentRound;
	        this.freeSpinsRemaining = parent.freeSpinsRemaining;

	        if (this.freeSpinsRemaining > 0) {
	            this.betAmount = parent.betAmount;
	            this.maxPayline = parent.maxPayline;

	            GameAction freeSpinAction = new GameAction(null, this, 0,
	                    FREE_SPIN_ACTION, maxPayline + " " + betAmount, 0.0);
	            freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
	            freeSpinAction.setRoundStart();

	            freeSpinAction.commit();
	            actionCompleted(freeSpinAction);
	            this.freeSpin = true;
	        }
	    }
	    
	    @Override
		public ChefMasterConfiguration getConfig() {
	    	return (ChefMasterConfiguration)super.getConfig();
	    }
	    
	    @Override
	    protected void validateGameActionRequest(GameActionRequest request)
	            throws ActionFailedException {

	        /* Check if the action is "syntacticaly" valid */
	        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

	        if (request.getSeatId() != 1) {
	            throw new ActionFailedException("Wrong seat ID (Should be 1).");
	        } else if (gameMode == GameMode.FINISHED) {
	            throw new ActionFailedException("Round has finished.");
	        }
	    }

	    @Override
	    protected void loadAction(GameAction action)
	            throws ActionFailedException {
	        if (FREE_SPIN_ACTION.equals(action.getDescription())) {
	            try {
	                this.freeSpinsRemaining = Integer.parseInt(action
	                        .getActionValue());
	                this.freeSpin = true;
	                String[] tokens = action.getArea().split(" ");

	                if (tokens.length == 2) {
	                    this.maxPayline = Integer.parseInt(tokens[0]);
	                    this.betAmount = Double.parseDouble(tokens[1]);
	                } else {
	                    throw new ActionFailedException(
	                            "Free spin load failed. Values missing.");
	                }
	            } catch (NumberFormatException e) {
	                throw new ActionFailedException(
	                        "Free spin load failed. An invalid value was passed.");
	            }

	            actionCompleted(action);

	        } else {
	            doAction(action);
	        }
	    }

	    @Override
	    protected void performAction(GameAction action)
	            throws ActionFailedException {
	        if (BET_ACTION.equals(action.getDescription())) {
	            performBet(action);
	        } else if (BOX_SELECT_ACTION.equals(action.getDescription())) {
	            performBoxSelect(action);
	        } else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
	            performFreeSpin(action);
	        }
	    }

	    private void performBoxSelect(GameAction action)
	            throws ActionFailedException {
	        if (gameMode != GameMode.TREASURE_MODE) {
	            throw new ActionFailedException("Not in treasure bonus mode!");
	        }

	        int area = -1;
	        try {
	            area = Integer.parseInt(action.getArea());
	        } catch (NumberFormatException e) {
	            area = -1;
	        }

	        if ((area < 0) || (area >= bonusBoxes.length)) {
	            throw new ActionFailedException("Invalid area.");
	        }

	        if (bonusBoxes[area].isOpened()) {
	            throw new ActionFailedException("Box already opened!");
	        }

	        double winAmount = bonusBoxes[area].getPayout();

            action.createGeneratedAction(action.getPlayer(), 1,
                    WIN_ACTION, null, winAmount);
            action.commit();
            bonusBoxes[area].setOpened(true);
            this.bonusBoxesToOpen--;

            if (this.bonusBoxesToOpen == 0) {
                endRound(action);
            }

	    }

	    /**
	     * Shuffles a bonus box row
	     * 
	     * @param bonusRow
	     */
	    private void shuffleBonusRow(BonusBox[] bonusBoxRow) {
	        BonusBox tmp;

	        for (int i = 0; i < bonusBoxRow.length; i++) {
	            int swap = getRandom().nextInt(bonusBoxRow.length);
	            tmp = bonusBoxRow[swap];
	            bonusBoxRow[swap] = bonusBoxRow[i];
	            bonusBoxRow[i] = tmp;
	        }
	    }

	    private void initBoxes(List<Integer> queuedBonuses) {
	        double totalPayout = 0.0;
	        int boxesToInit = 0;
	        int boxesWithFullAmount = 0;
	        
	        /* Calculate total payout for the bonus */
	        for (Integer queuedBonus : queuedBonuses) {
	            totalPayout += getComboPayout(queuedBonus);
	            boxesToInit += (Integer)getConfig().getWinCombo(queuedBonus)[4];
	            boxesWithFullAmount +=  (Integer)getConfig().getWinCombo(queuedBonus)[3];
	        }
        
	        /*Scatter combination Calculate on total bet*/
	        totalPayout *= this.betAmount * (this.maxPayline + 1);
        

	        if (boxesToInit > ChefMasterConfiguration.MAXIMUM_BOXES_TO_OPEN) {
	            boxesToInit = ChefMasterConfiguration.MAXIMUM_BOXES_TO_OPEN;
	            boxesWithFullAmount = boxesToInit;
	        }
	        
	        this.bonusBoxes = new BonusBox[boxesToInit];
	        int rates[] = new int[boxesWithFullAmount];
	        double maxAmountToGive = totalPayout;
	        double minAmountToGive = Double.MAX_VALUE;
	        double[] amounts = new double[boxesToInit];

	        for (int i = 0; i < this.bonusBoxes.length; i++) {
	            bonusBoxes[i] = new BonusBox();
	        }
	        
	        /*Calculate Rates*/
	        for(int i=0;i< BONUS_RANDOMNESS;i++) {
				int num = this.getRandom().nextInt(boxesWithFullAmount);
				rates[num]++;
	        }
	        
	        /*Allocate amount depending boxesWithFullAmount depending on the rates
	         * also calculate the amount that will be given on the other boxes*/
			for(int i=0;i< boxesWithFullAmount;i++) {
				amounts[i] = ((double)rates[i] / BONUS_RANDOMNESS) *  maxAmountToGive;	
				minAmountToGive = Math.min(minAmountToGive, amounts[i]);
			}
			
	        /*Allocate amount on the boxes remaining depending on the rates*/
			for(int i=boxesWithFullAmount;i<boxesToInit;i++) {
				amounts[i] = (minAmountToGive / 2.0) + (this.getRandom().nextDouble() * ((minAmountToGive - (minAmountToGive / 2.0))));
			}
	  
			/*Floor the values to the neares cent and assign*/
	        for (int i = 0; i < boxesToInit; i++) {
				double finalBoxAmount = currencyParser.normalizeDouble(amounts[i]);
	            bonusBoxes[i].setPayout(finalBoxAmount);
	        }
	        
	        /* Shuffle boxes */
	        shuffleBonusRow(bonusBoxes);

	        /* Log bonus boxes */
	        if (Config.getBoolean("games.log.bonus", false)) { 
	        	StringBuilder builder = new StringBuilder();

	        	for (int i=0; i < this.bonusBoxes.length; i++) {
	        		builder.append('[');
	        		if (this.bonusBoxes[i].isRowStop()) {
	        			builder.append('*');
	        		}
	        		builder.append(this.bonusBoxes[i].getPayout());
	        		builder.append(']');
	        	}

	        	logInfo("Bonus boxes:\n" + builder.toString());
	        }
	    }

	    private void performFreeSpin(GameAction action)
	            throws ActionFailedException {
	        if (!freeSpin) {
	            throw new ActionFailedException("Not in free spin mode!");
	        } else if (gameMode != GameMode.NORMAL_MODE) {
	            throw new ActionFailedException("Spin not allowed at this time.");
	        }

	        action.commit();
	        this.freeSpinsRemaining--;
	        startRound(action);
	    }

	    /**
	     * Performs the bet action.
	     * 
	     * @param action
	     *            The action request.
	     * @throws ActionFailedException
	     */
	    private void performBet(GameAction action) throws ActionFailedException {
	        if (freeSpin) {
	            throw new ActionFailedException(
	                    "Round is a free spin; No bets allowed.");
	        } else if (betAmount > 0.0) {
	            throw new ActionFailedException("Bet failed: already placed bet!");
	        } else if (gameMode != GameMode.NORMAL_MODE) {
	            throw new ActionFailedException("Bet not allowed at this time.");
	        }

	        checkBet(action);

	        int area = -1;

	        try {
	            area = Integer.parseInt(action.getArea());
	        } catch (NumberFormatException e) {
	        }

	        if ((area < 0) || (area >= getConfig().getPaylines().length)) {
	            throw new ActionFailedException("Invalid action area");
	        }

	        double betUnit;
	        double totalBet;

	        if (action.isLoaded()) {
	            totalBet = action.getAmount();
	            betUnit = action.getAmount() / (area + 1);
	        } else {
	            betUnit = action.getAmount();
	            totalBet = betUnit * (area + 1);
	            action.setAmount(totalBet);
	        }

	        action.setRoundStart();
	        action.commit();

	        betAmount = betUnit;
	        maxPayline = area;

	        placedJackpotContrib = JackpotsHandler
	                .contributeToJackpot(this, action);

	        startRound(action);
	    }

	    private void startRound(GameAction action) throws ActionFailedException {
	        if (START_FREE_SPIN_ACTION.equals(action.getDescription())
	                && (freeSpinsRemaining < 0)) {
	            this.maxPayline = -1;
	            this.betAmount = 0.0;
	            this.freeSpinsRemaining = 0;
	            this.freeSpin = false;
	            throw new IllegalStateException(
	                    "No free spins remain. Round must be started normally.");
	        } else if (gameMode == GameMode.FINISHED) {
	            throw new IllegalStateException(
	                    "Method startRound() already called once.");
	        }

	        double jackpotRatio = (betAmount / options.getMaxBet());
	        if (jackpotRatio > 1.0) {
	            jackpotRatio = 1.0;
	        }

	        if (claimMysteryJackpot(action, betAmount, jackpotRatio,
	                placedJackpotContrib)) {
	            this.gameMode = GameMode.FINISHED;
	            return;
	        }

	        int[][] reels = getConfig().getReels(freeSpin ? 1: 0);
	                
	        int[] reelStops = spinReels(reels);
	        /*
	         * StringBuilder rS = new StringBuilder("Reel Stops:"); for (int
	         * reelStop: reelStops) { rS.append(" "); rS.append(reelStop); }
	         * Log.info(rS.toString());
	         */
	        int[][] reelsView = calculateReelView(reels, reelStops);

	        /* FIXED ODDS CODE */
	        // com.magneta.games.fixedodd.PharaohGuard.guardRound(game.getId(),
	        // options.getOwner(), freeSpin, roundReels, WIN_CONSTRAINTS,
	        // roundPayouts, WILD_SYMBOL, FREE_SPIN_MULTIPLIER, betAmount,
	        // maxPayline, reelStops, options.getGameSettings(), action);
	        
	        /*Expanding wild */
	        List<Integer> wilds = new ArrayList<Integer>();
    		for (int line = 0; line < 3; line++) {
    			boolean wildReplace = false;

    			for (int i=0; i < reelsView.length; i++) {
    				if (reelsView[i][line] == WILD_SYMBOL) {
    					wildReplace = true;
    					wilds.add(line);
    					break;
    				}
    			}

    			if (wildReplace) {
    				for (int i=0; i < reelsView.length; i++) {
    					if (reelsView[i][line] != ChefMasterConfiguration.SCATTER_SYMBOL && reelsView[i][line] != ChefMasterConfiguration.BONUS_SYMBOL) {
    						reelsView[i][line] = WILD_SYMBOL;
    					}
    				}
    			}
    		}
        	
  	        createReelViewAction(action, reelsView);
	        if(!wilds.isEmpty()) {
	        	StringBuilder sbuilder = new StringBuilder();
	        	for(int i = 0;i<wilds.size();i++) {
	        		if(i != 0) {
	        			sbuilder.append(' ');
	        		}
	        		sbuilder.append(wilds.get(i));
	        	}
	        	GameAction newAction = action.createGeneratedAction(EXPANDING_WILD_ACTION, "", 0.0);
	        	newAction.setActionValue(sbuilder.toString());
	        }

	        // int[] lineWinCombo = new int[maxPayline+1];
	        int[] currLine = new int[reels.length];
	        List<Integer> queuedBonuses = new ArrayList<Integer>();

	        int freeSpins = 0;

	        for (int i = 0; i <= maxPayline; i++) {
	            getPayline(reelsView, i, currLine);

	            int combo = getPaylineWinCombination(currLine);
	            

	            if (combo < 0)
	                continue;

	            double multiplier = getComboPayout(combo);
	            double winAmount = multiplier * betAmount * (freeSpin? FREE_SPIN_MULTIPLIER : 1.0) *(wilds.size() > 0 ? wilds.size() : 1.0);

	            if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)
	                    && JackpotsHandler.claimGameJackpot(this, action, i,
	                            winAmount, jackpotRatio)) {
	                // stop the game from retrying to claim jackpot on other line
	                placedJackpotContrib = false;
	            } else {
	                if (winAmount > 0.0) {
	                    GameAction winAction = action.createGeneratedAction(
	                            action.getPlayer(), 1, WIN_ACTION,
	                            String.valueOf(i), winAmount);
	                    winAction.setActionValue(String.valueOf(combo));
	                }
	            }
	        }
	        
	        List<WinCombination> specialCombos = getReelViewWinCombination(reelsView);
	        if (specialCombos != null) {
	            for (WinCombination c: specialCombos) {
	                int combo = c.getCombination();
	                Object[] winCombo = getConfig().getWinCombo(combo);
	                Integer symbol = (Integer)winCombo[0];
	                
	                
	                if (symbol.equals(-ChefMasterConfiguration.BONUS_SYMBOL) && freeSpin) {
	                	int bonusSymbolCount = (Integer)getConfig().getWinCombo(combo)[1];
	                	int bonusCombo = combo;

	                	/*
	                	 * Set the bonus Combo to the real combo The combo found is
	                	 * for the combination we must replace it with the
	                	 * respective bonus win combination.
	                	 */
	                	Object[][] WIN_CONSTRAINTS = getConfig().getWinCombos();

	                	for (int j = WIN_CONSTRAINTS.length - 1; j >= 0; j--) {
	                		if (WIN_CONSTRAINTS[j][0].equals(0)
	                				&& WIN_CONSTRAINTS[j][1]
	                				                      .equals(bonusSymbolCount)) {
	                			bonusCombo = j;
	                			break;
	                		}
	                	}
	                	queuedBonuses.add(new Integer(bonusCombo));

	                } else if (symbol.equals(-ChefMasterConfiguration.SCATTER_SYMBOL) && !freeSpin) {
	                	freeSpins += 10;
	                }
	            }
	        }

	        if (!queuedBonuses.isEmpty()) {
	            for (Integer bonus : queuedBonuses) {
	                GameAction bonusAction = action.createGeneratedAction(null, 0,
	                        TREASURE_BONUS_ACTION, null, 0.0);
	                bonusAction.setActionValue(String.valueOf(bonus));
	                this.bonusBoxesToOpen += (Integer)getConfig().getWinCombo(bonus)[3];
	            }
	            if(this.bonusBoxesToOpen > ChefMasterConfiguration.MAXIMUM_BOXES_TO_OPEN) {
	                this.bonusBoxesToOpen = ChefMasterConfiguration.MAXIMUM_BOXES_TO_OPEN;
	            }

	            initBoxes(queuedBonuses);
	            this.gameMode = GameMode.TREASURE_MODE;
	        }
		       
	        if (freeSpins > 0) {
	            GameAction freeSpinsAction = action.createGeneratedAction(null, 0,
	                    FREE_SPINS_WIN_ACTION, "", 0.0);
	            freeSpinsAction.setActionValue(Integer.toString(freeSpins));
	            this.freeSpinsRemaining += freeSpins;
	        }
	        
	        
	        if(this.gameMode != GameMode.TREASURE_MODE) {
	            endRound(action);
	        } else {
	        	action.commit();
	        }
	    }

	    private void endRound(GameAction action) throws ActionFailedException {
	        if (gameMode == GameMode.FINISHED) {
	            throw new IllegalStateException(
	                    "Method endRound() already called once.");
	        }

	        action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
	        action.commit(true);
	        
	        gameMode = GameMode.FINISHED;
	    }
	    
	    /**
	     * Checks if the given payline forms any winning combinations.
	     * 
	     * @param payline
	     *            The payline to check.
	     * @return The combination the payline forms or -1 if it doesn't form any.
	     */
	    public int getPaylineWinCombination(int[] payline) {
	        Object[][] winCombos = getConfig().getWinCombos();
	        int maxPayoutCombo = -1;
	        
	        for (int i = winCombos.length - 1; i >= 0; i--) {
	            int symbol = (Integer)winCombos[i][0];
	            int quantityRequired = (Integer)winCombos[i][1];

	            if ((symbol != ChefMasterConfiguration.BONUS_SYMBOL && symbol != ChefMasterConfiguration.SCATTER_SYMBOL) && getComboPayout(i) < 0.01) {
	                continue;
	            }
	            
	            int j;

	            boolean foundSymbol = false;
	            for (j = 0; j < payline.length; j++) {
	                if (payline[j] == symbol) {
	                    foundSymbol = true;
	                } else if ((payline[j] == WILD_SYMBOL)
	                        && (symbol != ChefMasterConfiguration.BONUS_SYMBOL)
	                        && (symbol != ChefMasterConfiguration.SCATTER_SYMBOL)) {
	                    continue;
	                } else {
	                    break;
	                }
	            }

	            if ((j == quantityRequired) && foundSymbol) {
	                if (getComboPayout(i) > getComboPayout(maxPayoutCombo)) {
	                    maxPayoutCombo = i;
	                }
	                
	                /* Always prefer bonus and free spins over normal combinations */
	                if (symbol == ChefMasterConfiguration.BONUS_SYMBOL || symbol == ChefMasterConfiguration.SCATTER_SYMBOL)
	                    return i;
	            }
	        }

	        return maxPayoutCombo;
	    }

	    /**
	     * Checks if the given reel view forms any winning combinations.
	     * 
	     * @param reelView
	     *            The view to check.
	     * @return The combination the view forms or -1 if it doesn't form any.
	     */
	    public final List<WinCombination> getReelViewWinCombination(int[][] reelView) {
	    	List<WinCombination> combos = new ArrayList<WinCombination>();
	        List<Integer> comboSymbols = new ArrayList<Integer>();
	        Object[][] winCombos = getConfig().getWinCombos();

	        for (int i = winCombos.length - 1; i >= 0; i--) {
	            int symbol = (Integer)winCombos[i][0];
	            int quantity = (Integer)winCombos[i][1];

	            if (symbol >= 0)
	                continue;

	            symbol = symbol * -1;
	            
	            /* A combo with the symbol was already found. skip */
	            if (comboSymbols.contains(new Integer(symbol))) {
	                continue;
	            }
	            boolean foundOnLine = false;

	            for (int j = 0; j < reelView[0].length; j++) {
	                foundOnLine = false;
	                for (int k = 0; k < 3; k++) {
	                    if (reelView[k][j] == symbol) {
	                        foundOnLine = true;
	                        break;
	                    }
	                }

	                if (foundOnLine) {
	                    quantity--;
	                }

	                if (quantity <= 0) {
	                    comboSymbols.add(symbol);
	                    combos.add(new WinCombination(j, i));
	                    break;
	                }
	            }

	        }

	        return combos;
	    }

	    @Override
		public boolean isFinished() {
	        return gameMode == GameMode.FINISHED;
	    }

	    public double getComboPayout(int combo) {
	        if (combo < 0) {
	            return 0.0;
	        }
	        
	        return getConfig().getPayout(combo);
	    }
}
