package com.magneta.games.slots.reelsteel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.AbstractGameRound;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.ReelSteelConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.ReelSteelGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("ReelSteel")
public class ReelSteelRound extends SlotRound {

	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	
	private static final Logger log = LoggerFactory.getLogger(ReelSteelRound.class);

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
	};

	private int freeSpinsRemaining;
	private int freeSpinsWon;
	private boolean finished;
	private double betAmount;
	private int maxPayline;

	private boolean placedJackpotContrib;

	

	private RoundState state;

	public enum RoundState {
		NORMAL,
		FREE_SPIN
	}

	public ReelSteelRound() {
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.freeSpinsRemaining = 0;
		this.freeSpinsWon = 0;
		this.finished = false;
		this.state = RoundState.NORMAL;
		this.placedJackpotContrib = false;
	}

	@Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		ReelSteelRound parent = (ReelSteelRound)parentRound;
		this.freeSpinsRemaining = parent.freeSpinsRemaining;
		if (this.freeSpinsRemaining > 0) {
			this.freeSpinsWon = parent.freeSpinsWon;
			this.betAmount = parent.betAmount;
			this.maxPayline = parent.maxPayline;

			GameAction freeSpinAction = new GameAction(null, this, 0,
					FREE_SPIN_ACTION, maxPayline + " " + betAmount+" "+Integer.toString(this.freeSpinsWon), 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();

			freeSpinAction.commit();
			actionCompleted(freeSpinAction);
			this.state = RoundState.FREE_SPIN;
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {

		/* Check if the action is "syntacticaly" valid */
		validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	public ReelSteelConfiguration getConfig() {
		return (ReelSteelConfiguration)super.getConfig();
	}

	@Override
	protected void loadAction(GameAction action) throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())){
			try{
				this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
				this.state = RoundState.FREE_SPIN;
				String[] tokens = action.getArea().split(" ");

				if (tokens.length == 3) {
					this.maxPayline = Integer.parseInt(tokens[0]);
					this.betAmount = Double.parseDouble(tokens[1]);
					this.freeSpinsWon = Integer.parseInt(tokens[2]);
				} else {
					throw new ActionFailedException("Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e){
				throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
			}

			actionCompleted(action);

		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
			performFreeSpin(action);
		}
	}

	private void performFreeSpin(GameAction action) throws ActionFailedException {
		if (this.state!=RoundState.FREE_SPIN){
			throw new ActionFailedException("Not in free spin mode!");
		}

		if (this.state == RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if ( this.state == RoundState.FREE_SPIN) {
			throw new ActionFailedException("Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)){
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void startRound(GameAction action) throws ActionFailedException {

		if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)) {
			throw new IllegalStateException("No free spins remain. Round must be started normally.");
		} else if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		int reelSet = 0;

		if(this.state == RoundState.FREE_SPIN){
			reelSet = 2;
		}
		else {
			int[][] weights = getWeights();
			reelSet = getWeightedRandom(weights,getRandom()); 
		}
		
		int[] reelStops = spinReels(getConfig().getReels(reelSet));
		log.debug("prin : "+Arrays.toString(reelStops));
		int[] rellsetChosen = new int[]{reelSet};
		reelStops = ReelSteelGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reelSet, reelStops, options.getGameSettings(), action, this, maxPayline,rellsetChosen,getGuardRandom());
		reelSet = rellsetChosen[0];
		log.debug("meta : "+Arrays.toString(reelStops));
		
		evaluatePosition(action, reelSet, jackpotRatio, reelStops, betAmount, maxPayline ,new Double[]{(0.0)},true);

		endRound(action);

	}
	
	
	
	public List<Integer> evaluatePosition(GameAction action, int reelSet,
			double jackpotRatio, int[] reelStops, double betAmount2,
			int maxPayline2, Double[] actwinsum, boolean isProduction) throws ActionFailedException {

		int[][] reelsView = calculateReelView(getConfig().getReels(reelSet), reelStops);

		if(isProduction){
			createReelViewAction(action, reelsView);
			action.commit();
		}

		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		double roundMultiplier = 1;
		if( this.state == RoundState.FREE_SPIN){
			roundMultiplier = ReelSteelConfiguration.FREE_SPIN_MULTIPLIER;
		}
		for (int i=0;i <= maxPayline2;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			// this is to skip evaluation of the special symbol rules.

			if (combo < 0)
				continue;

			
			
			double  comboMultiplier = getComboPayout(combo);
			double wildMultiplier = getWildMultiplier(combo,currLine);
			log.debug("multiplier = "+ roundMultiplier * comboMultiplier * wildMultiplier + " for line "+ (i+1));
			//System.out.println("multiplier = "+ roundMultiplier * comboMultiplier * wildMultiplier + " for line "+ (i+1));
			double winAmount = wildMultiplier *roundMultiplier * comboMultiplier * betAmount2;
			if(isProduction){
				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					if (winAmount > 0.0) {
						actwinsum[0] = new Double(actwinsum[0] + winAmount);
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
						winAction.setActionValue(String.valueOf(combo));
						winAction.commit();
					} 
				}
			}else{
				if (winAmount > 0.0) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
				} 
			}
		}

		List<Integer> viewCombos = getReelViewWinCombinations(getConfig(), reelsView,reelSet);

		for (Integer viewCombo: viewCombos) {
			if (viewCombo >= 0) {
				double multiplier = getComboPayout(viewCombo);

				double winAmount = roundMultiplier *multiplier * (betAmount2 * (maxPayline2+1));

				int symbol = (Integer)getConfig().getWinCombo(viewCombo)[0];

				if (winAmount > 0.01) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
					if(isProduction){
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, null, winAmount);
						winAction.setActionValue(String.valueOf(viewCombo));
						action.commit();
					}
				}
				if(isProduction){
					symbol = -symbol;
					if(symbol==ReelSteelConfiguration.FREE_SPIN_SCATTER){
						int freeSpins = 0;
						if(this.state.equals(RoundState.NORMAL)&& (Integer)getConfig().getWinCombo(viewCombo)[1]>2){
							freeSpins = (Integer)getConfig().getWinCombo(viewCombo)[3];
						}else if(this.state.equals(RoundState.FREE_SPIN)){
							freeSpins = (Integer)getConfig().getWinCombo(viewCombo)[4];
						}
						
						if(freeSpins > 0) {
							action.setActionValue("FREE " + freeSpins);
							GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
							freeSpinsAction.setActionValue(Integer.toString(freeSpins));
							action.commit();
							this.freeSpinsRemaining += freeSpins;
							this.freeSpinsWon += freeSpins;
						}
					}
				}
			}
		}

		return viewCombos;

	}

	private double getWildMultiplier(int combo, int[] currLine) {
		int symbol = (Integer)getConfig().getWinCombo(combo)[0];
		if(symbol==ReelSteelConfiguration.WILD_SYMBOL){
			return 1;
		}
		for (int i = 0; i < currLine.length; i++) {
			if(currLine[i]==ReelSteelConfiguration.WILD_SYMBOL){
				return ReelSteelConfiguration.WILD_SYMBOL_MULTIPLIER;
			}
		}
		return 1;
	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();

		int topw = -1;
		double topwp = 0.0;

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if (getComboPayout(i) < 0.01) {
				continue;
			}

			if (symbol< 0) {
				continue;
			}

			int j;
			for (j=0;j<payline.length;j++){
				if (payline[j] == symbol||payline[j] ==ReelSteelConfiguration.WILD_SYMBOL ) {
					continue;
				}
				break;
			}

			if (j == quantityRequired){
				if(topwp < getConfig().getPayout(i)){
					topw =i;
					topwp = getConfig().getPayout(i);
				}
			}
		}        

		return topw;
	}

	/**
	 * Checks if the given reel view forms any scatter combinations.
	 * @param reelView The view to check.
	 * @param specialStmbol_param 
	 * @return The combinations the view form.
	 */
	public static final List<Integer> getReelViewWinCombinations(SlotConfiguration config, int[][] reelView,int reelset){

		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];
			
			if (symbol >= 0 )
				continue;
			log.debug(" symbol "+symbol+"  and quantity " + quantity );
			symbol = symbol * -1;
			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}

			boolean foundOnLine = false;

			/* Scatter symbol wins */
			for (int j=0;j < config.getNumberOfReels(reelset);j++){
				foundOnLine = false;
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol){
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}        

		return combinations;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	public Double getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}
		return this.getConfig().getPayout(combo);
	}

	public void setState(RoundState p_state) {
		this.state = p_state;
	}
	
	public int getWeightedRandom(int[][] freeSpin, Random random) {
		int sum = 0;

		for (int[] w: freeSpin) {
			sum += w[1];
		}

		int pos = random.nextInt(sum);

		for(int i=0; i < freeSpin.length; i++) {
			pos -= freeSpin[i][1];

			if (pos <= 0) {
				return freeSpin[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}
	
	public int[][] getWeights() {
		int[][] weights = { 
				{0, getConfig().getSetting(ReelSteelConfiguration.REEL_WEIGHT_0, Integer.class) },
				{1, getConfig().getSetting(ReelSteelConfiguration.REEL_WEIGHT_1, Integer.class) },
		};
		return weights;
	}
	
	public double evaluatePositionWinnings(int reelSet, int[]reelStops) throws ActionFailedException{
		Double[] res = new Double[]{(0.0)};
		evaluatePosition(null, reelSet, 0, reelStops, this.betAmount, this.maxPayline ,res,false);
		return res[0];
	}

}
