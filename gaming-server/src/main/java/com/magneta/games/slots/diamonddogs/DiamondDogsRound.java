package com.magneta.games.slots.diamonddogs;

import static com.magneta.games.configuration.slots.DiamondDogsConfiguration.BONUS_SYMBOL;
import static com.magneta.games.configuration.slots.DiamondDogsConfiguration.FREE_SPIN_SYMBOL;
import static com.magneta.games.configuration.slots.DiamondDogsConfiguration.MAXIMUM_BOXES_TO_OPEN;
import static com.magneta.games.configuration.slots.DiamondDogsConfiguration.WILD_SYMBOL;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.magneta.Config;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.BankBurstConfiguration;
import com.magneta.games.configuration.slots.DiamondDogsConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.BonusBox;
import com.magneta.games.slots.SlotRound;

@GameTemplate("DiamondDogs")
public class DiamondDogsRound extends SlotRound {

	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";
	private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";
	private static final String BOX_FINISH_ACTION = "g:BOX_FINISH";



	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/* 								Description 			Area 						Amount */
		new GameActionRequestDefinition(BET_ACTION, 			ParameterType.REQUIRED,		ParameterType.REQUIRED),
		new GameActionRequestDefinition(BOX_SELECT_ACTION,		ParameterType.REQUIRED, 	ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION,	ParameterType.FORBIDDEN,	ParameterType.FORBIDDEN) };


	private static final int NORMAL_MODE = 0;
	private static final int TREASURE_MODE = 1;

	private static final double FREE_SPIN_SCATTER_MULTIPLIER = 3.0;

	private int freeSpinsRemaining;

	private int gameMode;

	private boolean finished;

	private double betAmount;

	private int maxPayline;

	private BonusBox[] bonusBoxes;

	private int bonusBoxesToOpen;

	private boolean freeSpin;

	private boolean placedJackpotContrib;

	public DiamondDogsRound() {
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.freeSpinsRemaining = 0;
		this.gameMode = NORMAL_MODE;
		this.finished = false;
		this.freeSpin = false;
		this.placedJackpotContrib = false;
		this.bonusBoxes = null;
	}

	@Override
	public DiamondDogsConfiguration getConfig() {
		return (DiamondDogsConfiguration)super.getConfig();
	}

	@Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		DiamondDogsRound parent = (DiamondDogsRound)parentRound;

		this.freeSpinsRemaining = parent.freeSpinsRemaining;

		if (this.freeSpinsRemaining > 0) {
			this.betAmount = parent.betAmount;
			this.maxPayline = parent.maxPayline;

			GameAction freeSpinAction = new GameAction(null, this, 0,
					FREE_SPIN_ACTION, maxPayline + " " + betAmount, 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();

			freeSpinAction.commit();
			actionCompleted(freeSpinAction);
			this.freeSpin = true;
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request)
	throws ActionFailedException {

		/* Check if the action is "syntacticaly" valid */
		validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	protected void loadAction(GameAction action)
	throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())) {
			try {
				this.freeSpinsRemaining = Integer.parseInt(action
						.getActionValue());
				this.freeSpin = true;
				String[] tokens = action.getArea().split(" ");

				if (tokens.length == 2) {
					this.maxPayline = Integer.parseInt(tokens[0]);
					this.betAmount = Double.parseDouble(tokens[1]);
				} else {
					throw new ActionFailedException(
					"Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e) {
				throw new ActionFailedException(
				"Free spin load failed. An invalid value was passed.");
			}

			actionCompleted(action);

		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action)
	throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())) {
			performBet(action);
		} else if (BOX_SELECT_ACTION.equals(action.getDescription())) {
			performBoxSelect(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
			performFreeSpin(action);
		}
	}
	
	private int getOpenBoxCount() {
		int count = 0;
		
		for (BonusBox box: bonusBoxes) {
			if (box.isOpened()) {
				count++;
			}
		}
		
		return count;
	}

	private void performBoxSelect(GameAction action)
	throws ActionFailedException {
		if (gameMode != TREASURE_MODE) {
			throw new ActionFailedException("Not in treasure bonus mode!");
		}

		int area = -1;
		try {
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
			area = -1;
		}

		if ((area < 0) || (area >= bonusBoxes.length)) {
			throw new ActionFailedException("Invalid area.");
		}

		if (bonusBoxes[area].isOpened()) {
			throw new ActionFailedException("Box already opened!");
		}
		
		int openCount = getOpenBoxCount();
		
		/* First opened box must always have a payout. Select the first we find */
		if (openCount == 0 && bonusBoxes[area].getPayout() < 0.0001) {
			for (int i=0; i < bonusBoxes.length; i++) {
				if (bonusBoxes[i].getPayout() > 0.001) {
					BonusBox tmp = bonusBoxes[i];
					bonusBoxes[i] = bonusBoxes[area];
					bonusBoxes[area] = tmp;
				}
			}
		}

		com.magneta.games.fixedodd.DiamondDogsGuard.getPossibleBoxBonus(action, betAmount, game.getGameId(), options.getOwner(), options.getGameSettings(),bonusBoxes,area, openCount);
		double winAmount = bonusBoxes[area].getPayout() * this.betAmount;
		if (winAmount > 0.0) {
			action.createGeneratedAction(action.getPlayer(), 1,
					WIN_ACTION, null, winAmount);
			action.commit();
			bonusBoxes[area].setOpened(true);
			this.bonusBoxesToOpen--;

			if (this.bonusBoxesToOpen == 0) {
				endRound(action);
			}

		} else {
			String description = "";
			for(BonusBox box : bonusBoxes){
				if(!box.isOpened())
					description += box.getPayout() + " ";
			}
			GameAction boxFinishAction = action.createGeneratedAction(action.getPlayer(), 1,
					BOX_FINISH_ACTION, description, 0.0);
			boxFinishAction.setSaved();
			endRound(action);
		}
	}

	/**
	 * Shuffles a bonus box row
	 * 
	 * @param bonusRow
	 */
	private void shuffleBonusRow(BonusBox[] bonusBoxRow) {
		BonusBox tmp;

		for (int i = 0; i < bonusBoxRow.length; i++) {
			int swap = getRandom().nextInt(bonusBoxRow.length);
			tmp = bonusBoxRow[swap];
			bonusBoxRow[swap] = bonusBoxRow[i];
			bonusBoxRow[i] = tmp;
		}
	}

	private void initBoxes() {

		int boxesToInit = 12;

		//		if (boxesToInit > MAXIMUM_BOXES_TO_OPEN) {
		//			boxesToInit = MAXIMUM_BOXES_TO_OPEN;
		//		}

		this.bonusBoxes = new BonusBox[boxesToInit];

		// initialising boxes;
		for (int i = 0; i < this.bonusBoxes.length; i++) {
			bonusBoxes[i] = new BonusBox();
		}
		// filling boxes
		for (int i = 0; i < this.bonusBoxesToOpen; i++) {
			double box_multiplier =0.0;
			if(i<MAXIMUM_BOXES_TO_OPEN){
				getConfig();
				box_multiplier = getWeightedRandom(DiamondDogsConfiguration.BONUS_WEIGHTS, getRandom());
			}
			double box_amount = box_multiplier;
			bonusBoxes[i].setPayout(box_amount);

		}

		/* Shuffle boxes */
		shuffleBonusRow(bonusBoxes);

		/* Log bonus boxes */
		if (Config.getBoolean("games.log.bonus", false)) { 
			StringBuilder builder = new StringBuilder();

			for (int i=0; i < this.bonusBoxes.length; i++) {
				builder.append('[');
				if (this.bonusBoxes[i].isRowStop()) {
					builder.append('*');
				}
				builder.append(this.bonusBoxes[i].getPayout());
				builder.append(']');
			}

			logInfo("Bonus boxes:\n" + builder.toString());
		}
	}

	public int getWeightedRandom(int[][] freeSpin, Random random) {
		int sum = 0;

		for (int[] w: freeSpin) {
			sum += w[1];
		}

		int pos = random.nextInt(sum);

		for(int i=0; i < freeSpin.length; i++) {
			pos -= freeSpin[i][1];

			if (pos <= 0) {
				return freeSpin[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}

	private void performFreeSpin(GameAction action)
	throws ActionFailedException {
		if (!freeSpin) {
			throw new ActionFailedException("Not in free spin mode!");
		} else if (gameMode != NORMAL_MODE) {
			throw new ActionFailedException("Spin not allowed at this time.");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * 
	 * @param action
	 *            The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if (freeSpin) {
			throw new ActionFailedException(
			"Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0) {
			throw new ActionFailedException("Bet failed: already placed bet!");
		} else if (gameMode != NORMAL_MODE) {
			throw new ActionFailedException("Bet not allowed at this time.");
		}

		checkBet(action);

		int area = -1;

		try {
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)) {
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area + 1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area + 1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler
		.contributeToJackpot(this, action);

		startRound(action);
	}

	private void startRound(GameAction action) throws ActionFailedException {
		if (START_FREE_SPIN_ACTION.equals(action.getDescription())
				&& (freeSpinsRemaining < 0)) {
			this.maxPayline = -1;
			this.betAmount = 0.0;
			this.freeSpinsRemaining = 0;
			this.freeSpin = false;
			throw new IllegalStateException(
			"No free spins remain. Round must be started normally.");
		} else if (finished) {
			throw new IllegalStateException(
			"Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0) {
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio,
				placedJackpotContrib)) {
			this.finished = true;
			return;
		}
		
		int reelSet;
		if(freeSpin) {
			reelSet = DiamondDogsConfiguration.FREE_SPIN_REEL_SET;
		} else {
			int[][] weights = getWeights();

			reelSet = getWeightedRandom(weights,getRandom()); 
		}

		int[] reelStops = spinReels(getConfig().getReels(reelSet));
		/*
		 * StringBuilder rS = new StringBuilder("Reel Stops:"); for (int
		 * reelStop: reelStops) { rS.append(" "); rS.append(reelStop); }
		 * Log.info(rS.toString());
		 */


		/* FIXED ODDS CODE */
		reelStops = com.magneta.games.fixedodd.DiamondDogsGuard.guardRound(game.getGameId(), options.getOwner(), getConfig(), this.betAmount, reelStops, maxPayline, options.getGameSettings(), action, this, getGuardRandom(),reelSet);
		evaluatePosition(action, jackpotRatio, reelStops, getConfig(), maxPayline,betAmount,placedJackpotContrib,freeSpin,true, new Double[]{0.0},reelSet); 
		if(this.gameMode==TREASURE_MODE){
			// do nothing
		}else {
			endRound(action);
		}
	}
	
	public int[][] getWeights() {
		int[][] weights = { 
				{0, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_0, Integer.class) },
				{1, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_1, Integer.class) },
				{2, getConfig().getSetting(BankBurstConfiguration.REEL_WEIGHT_2, Integer.class) },
		};
		return weights;
	}

	public List<Integer> evaluatePosition(GameAction action, double jackpotRatio,
			int[] reelStops, SlotConfiguration config, int maxPayline, double betAmount, boolean placedJackpotContrib,boolean freeSpin,boolean isProduction, Double[] winsum,int reelset) throws ActionFailedException {


		int[][] reelsView = calculateReelView(config.getReels(reelset), reelStops);
		
//		logDebug("Printing reelview");
//		for (int i = 0; i < reelsView.length; i++) {
//			int[] row = reelsView[i];
//			logDebug(Arrays.toString(row));
//		}
		
		if(isProduction){
			createReelViewAction(action, reelsView);
		}

		int[] currLine = new int[config.getReels(0).length];

		int freeSpins = 0;

		for (int i = 0; i <= maxPayline; i++) {
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			if (combo < 0)
				continue;

			double multiplier = getComboPayout(combo);
			double winAmount = multiplier * betAmount* (freeSpin? FREE_SPIN_SCATTER_MULTIPLIER : 1.0);

			if (winAmount > 0.0) {
				winsum[0] = new Double(winsum[0]+winAmount);
			}
			
			logDebug("line "+i+" has won " + winAmount);

			if(isProduction){
				if (placedJackpotContrib && config.isJackpotCombination(i, combo)
						&& JackpotsHandler.claimGameJackpot(this, action, i,
								winAmount, jackpotRatio)) {
					// stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					if (winAmount > 0.0) {
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION,
								String.valueOf(i), winAmount);
						winAction.setActionValue(String.valueOf(combo));
					}


				}
			}
		}

		List<Integer> viewCombos = getReelViewWinCombination( config, reelsView);

		if (!viewCombos.isEmpty()) {

			if (((Integer)config.getWinCombo(viewCombos.get(0))[0]) == -FREE_SPIN_SYMBOL) {

				double scatter_amount = 0.0;
				double sctter_multiplier = (Double)config.getWinCombo(viewCombos.get(0))[2];
				scatter_amount = sctter_multiplier * betAmount * (maxPayline+1)
				* (freeSpin? FREE_SPIN_SCATTER_MULTIPLIER : 1.0);

				if (scatter_amount > 0.0) {
					logDebug("Scatter has won "+scatter_amount +" for " + config.getWinCombo(viewCombos.get(0))[1] );

					winsum[0] = new Double(winsum[0]+scatter_amount);
					if(isProduction){
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION,
								null, scatter_amount);
						winAction.setActionValue(String.valueOf(viewCombos.get(0)));
						action.commit();
					}
				}
				int symbolCount = (Integer)config.getWinCombo(viewCombos.get(0))[1];

				if (symbolCount > 2) {
					freeSpins += 10;
				}
			}
		}

		if (freeSpins > 0) {
			if(isProduction){
				GameAction freeSpinsAction = action.createGeneratedAction(null, 0,
						FREE_SPINS_WIN_ACTION, "", 0.0);
				freeSpinsAction.setActionValue(Integer.toString(freeSpins));
				freeSpinsAction.commit();
				this.freeSpinsRemaining += freeSpins;
			}
			
		}

		int[] bonus_combo  = getBonusWinCombination( config, reelsView,maxPayline);

		if (bonus_combo != null) {
			int bonusCombo = bonus_combo[0];
			viewCombos.add(bonusCombo);
			int bonusComboLine= bonus_combo[1];
			logDebug("Bonus has been won at line "+ bonusComboLine);
			if(isProduction){
				GameAction bonusAction = action.createGeneratedAction(null, 0,
						TREASURE_BONUS_ACTION, String.valueOf(bonusCombo), 0.0);
				bonusAction.setActionValue(String.valueOf(bonusComboLine));
				this.bonusBoxesToOpen = ((Double)config.getWinCombo(bonusCombo)[2]).intValue();
				if(this.bonusBoxesToOpen > MAXIMUM_BOXES_TO_OPEN) {
					this.bonusBoxesToOpen = MAXIMUM_BOXES_TO_OPEN;
				}
				action.commit();
				initBoxes();
				this.gameMode = TREASURE_MODE;
			}

		}
		return viewCombos;
	}

	private int[] getBonusWinCombination(SlotConfiguration config,
			int[][] reelsView,int maxPayline) {
		
		
		int maxPayoutCombo = -1;
		int[] result = null;
		for (int i = 0; i <= maxPayline; i++) {
			int[] currLine = new int[getConfig().getReels(0).length];
			getPayline(reelsView, i, currLine);

			Object[][] winCombos = getConfig().getWinCombos();

			for (int wc = winCombos.length - 1; wc >= 0; wc--) {

				int symbol = (Integer)winCombos[wc][0];

				if (symbol != -BONUS_SYMBOL) {
					continue;
				}
				symbol = -1*symbol;

				int quantityRequired = (Integer)winCombos[wc][1];
				boolean foundSymbol = false;
				int j;
				for (j = 0; j < currLine.length; j++) {
					if (currLine[j] == symbol) {
						foundSymbol = true;
					} else {
						break;
					}
				}

				if ((j == quantityRequired) && foundSymbol) {
					if (getComboPayout(maxPayoutCombo) < getComboPayout(wc)) {
						maxPayoutCombo = i;
						maxPayoutCombo = (Integer)winCombos[wc][1];
						result = new int[] {wc, i};
					}
					break;
				}
			}
		}
		return result;
	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished) {
			throw new IllegalStateException(
			"Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * 
	 * @param payline
	 *            The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();
		int maxPayoutCombo = -1;

		for (int i = winCombos.length - 1; i >= 0; i--) {
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if ((symbol != BONUS_SYMBOL && symbol != FREE_SPIN_SYMBOL) && getComboPayout(i) < 0.01) {
				continue;
			}

			int j;

			boolean foundSymbol = false;
			for (j = 0; j < payline.length; j++) {
				if (payline[j] == symbol) {
					foundSymbol = true;
				} else if ((payline[j] == WILD_SYMBOL)
						&& (symbol != BONUS_SYMBOL)
						&& (symbol != FREE_SPIN_SYMBOL)) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol) {
				if (getComboPayout(i) > getComboPayout(maxPayoutCombo)) {
					maxPayoutCombo = i;
				}

				/* Always prefer bonus and free spins over normal combinations */
				//				if (symbol == BONUS_SYMBOL || symbol == FREE_SPIN_SYMBOL)
				//					return i;
			}
		}

		return maxPayoutCombo;
	}

	/**
	 * Checks if the given reel view forms any winning combinations.
	 * 
	 * @param reelView
	 *            The view to check.
	 * @return The combination the view forms or -1 if it doesn't form any.
	 */
	public static final List<Integer> getReelViewWinCombination(SlotConfiguration config, int[][] reelView) {
		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i = winCombos.length - 1; i >= 0; i--) {
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			if (symbol >= 0)
				continue;
			//            log.debug(" symbol "+symbol+"  and quantity " + quantity );
			symbol = symbol * -1;

			if(symbol == BONUS_SYMBOL){
				continue;
			}
			boolean foundOnLine = false;

			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}

			for (int j = 0; j < reelView[0].length; j++) {
				foundOnLine = false;
				for (int k = 0; k < 3; k++) {
					if (reelView[k][j] == symbol) {
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}

		return combinations;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	public double getComboPayout(int combo) {
		if (combo < 0) {
			return 0.0;
		}

		return getConfig().getPayout(combo);
	}

	public double evaluatePositionWinnings(int[] reelStops, int reelset) throws ActionFailedException {
		Double[] winsum = new Double[]{0.0};
		this.evaluatePosition(null, 0.0, reelStops, getConfig(), this.maxPayline, this.betAmount, false, this.freeSpin, false, winsum,reelset);
		return winsum[0];
	}
}
