/**
 * 
 */
package com.magneta.games.slots.capodeicapi;

import java.util.Map;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.CapoDeiCapiConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("CapoDeiCapi")
public class CapoDeiCapiRound extends SlotRound {

    /**
     * The constant action description value for the take score action.  
     */
    private static final String TAKE_SCORE_ACTION = "g:TAKE_SCORE";
    /**
     * The constant action description value for the double draw action.  
     */
    private static final String DOUBLE_DRAW_ACTION = "g:DOUBLE_DRAW";
    /**
     * The constant action description value for the red card action.  
     */
    private static final String RED_ACTION = "g:RED";
    /**
     * The constant action description value for the black card action.  
     */
    private static final String BLACK_ACTION = "g:BLACK";
    /**
     * The constant action description value for the race action.  
     */
    private static final String RACE_ACTION = "g:RACE";

    private static final String WIN_LINE_ACTION = "g:WIN_LINE";

    public static final int DOUBLE_LIMIT = 5;

    private static final int NORMAL_MODE = 0;
    private static final int DOUBLE_UP_MODE = 1;

    private GameActionRequestDefinition[] allowed_actions;
    private boolean allowDoubleUp;

    private boolean placedJackpotContrib;
    private double betAmount;
    private boolean finished;
    private int gameMode;
    private double totalWins;
    private int doubleTimes;
    private Deck gameDeck;
    private double maxDoubleWinAmount;

    public CapoDeiCapiRound() {
        this.placedJackpotContrib = false;
        this.betAmount = 0.0;
        this.finished = false;
        this.gameMode = NORMAL_MODE;
        this.doubleTimes = 0;
    }
    
    @Override
	public void roundInit() {
        this.allowDoubleUp = options.getGameSettings().get("allow_double_up", Boolean.TYPE);
        this.maxDoubleWinAmount = options.getGameSettings().get("max_double_win_amount", Double.TYPE);

        if (allowDoubleUp){
            allowed_actions = new GameActionRequestDefinition[] {
                    /*                              Description        Area                     Amount                */
                    new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN, ParameterType.REQUIRED),
                    new GameActionRequestDefinition(TAKE_SCORE_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
                    new GameActionRequestDefinition(RED_ACTION,        ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
                    new GameActionRequestDefinition(BLACK_ACTION,      ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
                    new GameActionRequestDefinition(RACE_ACTION,       ParameterType.REQUIRED,  ParameterType.FORBIDDEN)
            };
        } else {
            allowed_actions = new GameActionRequestDefinition[] {
                    new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED)
            };
        }
    }
    
    @Override
	public CapoDeiCapiConfiguration getConfig() {
    	return (CapoDeiCapiConfiguration) super.getConfig();
    }

    protected void startRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        double jackpotRatio = betAmount / options.getMaxBet();
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
            this.finished = true;
            return;
        }

        /* Spin the reels */
        int[][] roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);

        /* FIXED ODD CODE */
        com.magneta.games.fixedodd.CapoDeiCapiGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reelStops, options.getGameSettings(), action);

        //get the view
        int[][] reelsView = calculateReelView(roundReels, reelStops);
        
        /* Create the REEL_STOP actions */
        createReelViewAction(action, reelsView);

        int[][][] paylines = getConfig().getPaylines();
        
        int[] lineWinCombo = new int[paylines.length];
        int[] currLine = new int[5];
        int jackpotLine = -1;
        double jackpotLineWin = 0.0;

        //check paylines for wins
        for (int i=0;i<paylines.length;i++){
            getPayline(reelsView, i, currLine);

            int combo = lineWinCombo[i] = getPaylineWinCombination(getConfig(), currLine);
            if (combo != -1){
                double multiplier = getConfig().getPayout(combo);
                double winAmount = multiplier * betAmount;

                if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)) {
                    jackpotLine = i;
                    jackpotLineWin = winAmount;
                    //stop the game from retrying to claim jackpot on other line
                    placedJackpotContrib = false;
                } else {
                    totalWins += winAmount;
                }
            }
        }

        if (totalWins <= 0.0 && jackpotLine < 0){
            //No wins - end round
            endRound(action, action.getPlayer());
        } else if (jackpotLine >= 0){
            //jackpot wins - try to claim jackpot
            if (JackpotsHandler.claimGameJackpot(this, action, jackpotLine, jackpotLineWin, jackpotRatio)){
                //create win actions for the rest of the wins & end round
                for (int i=0;i<lineWinCombo.length;i++){
                    int combo = lineWinCombo[i];
                    if ((i != jackpotLine) && (combo >= 0)){
                        double multiplier = getConfig().getPayout(combo);
                        double winAmount = multiplier * betAmount;
                        GameAction winAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                        winAction.setActionValue(String.valueOf(combo));
                    }
                }
                endRound(action, action.getPlayer());
            } else {
                totalWins += jackpotLineWin;
            }

        } else if (!allowDoubleUp || (totalWins > options.getGameSettings().get("max_double_amount", Double.TYPE)) 
                || (totalWins * 2.0 > maxDoubleWinAmount)) {
            //create win actions & end round
            for (int i=0;i<lineWinCombo.length;i++){
                int combo = lineWinCombo[i];
                if (combo >= 0){
                    double multiplier = getConfig().getPayout(combo);
                    double winAmount = multiplier * betAmount;
                    GameAction winAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                    winAction.setActionValue(String.valueOf(combo));
                }
            }
            endRound(action, action.getPlayer());
        }

        if (!this.finished){
            try {
                //create win line actions
                for (int i=0;i<lineWinCombo.length;i++){
                    int combo = lineWinCombo[i];
                    if (combo >= 0){
                        GameAction winLineAction = action.createGeneratedAction(null, 0, WIN_LINE_ACTION, String.valueOf(i), 0.0);
                        winLineAction.setActionValue(String.valueOf(combo));
                    }
                }
                action.commit(false);
                this.gameDeck = new Deck(1,0);
                this.gameMode = DOUBLE_UP_MODE;
            } catch (ActionFailedException e) {
                logError("Save failed.", e);
            }
        }
    }

    /**
     * Ends the round.
     * @param parentAction The action that caused the round to end.
     * @throws ActionFailedException 
     */
    private void endRound(GameAction parentAction, GamePlayer player) throws ActionFailedException {
        if (!finished){
            parentAction.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);

                parentAction.commit(true);
                this.finished = true;
        } else {
            throw new IllegalStateException("Method endRound() already called once.");
        }
    }

    /**
     * Ends the current round if the player is in double up mode.
     * @param action The action request.
     * @throws ActionFailedException 
     */
    private void performTakeScore(GameAction action) throws ActionFailedException{
        if (finished || (gameMode != DOUBLE_UP_MODE)) {
            throw new ActionFailedException("Invalid state: TAKE SCORE is only allowed after round has started and while in double up mode.");
        }

        action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, totalWins);
        endRound(action, action.getPlayer());
    }

    /**
     * Performs the action of the player predicting that the next card is of red or black color.
     * @param action The action request object.
     * @throws ActionFailedException
     */
    public void performColor(GameAction action, int color) throws ActionFailedException{
        if (finished || (gameMode != DOUBLE_UP_MODE)) {
            throw new ActionFailedException("Invalid state: RED or BLACK is only allowed after round has started and while in double mode.");
        }

        //reset and re-shuffle deck
        gameDeck.reset();
        gameDeck.shuffleCards(getRandom());

        /*FIXED ODD CODE*/
        com.magneta.games.fixedodd.CapoDeiCapiGuard.guardDouble(this.game.getGameId(), gameDeck, this.options.getOwner(), totalWins, color,-1, this.options.getGameSettings(), action);

        int nextCard = gameDeck.getNextCard();
        GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, String.valueOf(doubleTimes+1), 0.0);
        drawAction.setActionValue(String.valueOf(nextCard));
        action.setActionValue(String.valueOf(nextCard));
        action.commit();

        int nextCardColor = Card.getCardColour(nextCard);

        if (nextCardColor == color){
            totalWins *= 2.0;
            doubleTimes++;

            if (doubleTimes >= DOUBLE_LIMIT || (totalWins * 2.0 > maxDoubleWinAmount)) {
                action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, totalWins);
                endRound(action, action.getPlayer());
            }
        } else {
            endRound(action, action.getPlayer());
        }
    }

    /**
     * Performs the action of the player predicting that the next card is of specified race.
     * @param action The action request object.
     * @throws ActionFailedException
     */
    public void performRace(GameAction action) throws ActionFailedException{
        if (finished || (gameMode != DOUBLE_UP_MODE)) {
            throw new ActionFailedException("Invalid state: RACE is only allowed after round has started and while in double mode.");
        }

        if (totalWins * 4.0 > maxDoubleWinAmount) {
            throw new ActionFailedException("Exceeded total wins limit.");
        }

        int area = -1;
        try {
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e){
            area = -1;
        }

        if ((area != Card.CLUBS_CARD) 
                && (area != Card.DIAMONDS_CARD)
                && (area != Card.HEARTS_CARD)
                && (area != Card.SPADES_CARD)){
            throw new ActionFailedException("Invalid action area.");
        }

        //reset and re-shuffle deck
        gameDeck.reset();
        gameDeck.shuffleCards(getRandom());

        /*FIXED ODD CODE*/
        com.magneta.games.fixedodd.CapoDeiCapiGuard.guardDouble(this.game.getGameId(), gameDeck, this.options.getOwner(), totalWins, -1, area, this.options.getGameSettings(), action);

        int nextCard = gameDeck.getNextCard();
        GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, String.valueOf(doubleTimes+1), 0.0);
        drawAction.setActionValue(String.valueOf(nextCard));
        action.setActionValue(String.valueOf(nextCard));
        action.commit();

        int nextCardRace = Card.getCardRace(nextCard);

        if (nextCardRace == area){
            totalWins *= 4.0;
            doubleTimes++;

            if (doubleTimes >= DOUBLE_LIMIT || (totalWins * 2.0 > maxDoubleWinAmount)) {
                action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, totalWins);
                endRound(action, action.getPlayer());
            }
        } else {
            endRound(action, action.getPlayer());
        }
    }

    public static int getPaylineWinCombination(SlotConfiguration config, int[] payline){
        Object[][] winCombos = config.getWinCombos();

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            //try from the left 
            int j=0;
            for (;j<payline.length;j++){
                if (payline[j] != symbol){
                    break;
                }
            }

            if (j == quantityRequired){
                return i;
            }

            //try from the right
            j=payline.length-1;
            for (;j>=0;j--){
                if (payline[j] != symbol){
                    break;
                }
            }

            if (((payline.length-1) - j) == quantityRequired){
                return i;
            }
        }        

        return -1;
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        } else if (RED_ACTION.equals(action.getDescription())){
            performColor(action, Card.RED_CARD);
        } else if (BLACK_ACTION.equals(action.getDescription())){
            performColor(action, Card.BLACK_CARD);
        } else if (RACE_ACTION.equals(action.getDescription())){
            performRace(action);
        } else if (TAKE_SCORE_ACTION.equals(action.getDescription())){
            performTakeScore(action);
        }
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }

        checkBet(action);

        action.setRoundStart();
        action.commit();
        betAmount = action.getAmount();

        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {       
        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(allowed_actions, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    @Override
    protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
        super.getExtraInfo(player, roundInfo, firstTime);
        if (firstTime) {
            roundInfo.put("max_double_win", maxDoubleWinAmount);
        }
    }
}
