package com.magneta.games.slots.pharaoh;

import static com.magneta.games.configuration.slots.PharaohConfiguration.BONUS_SYMBOL;
import static com.magneta.games.configuration.slots.PharaohConfiguration.WILD_SYMBOL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.PharaohConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("Pharaoh")
public class PharaohRound extends SlotRound {

	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";
	private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(BOX_SELECT_ACTION,      ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
	};

	private static final int TREASURE_BOXES_NO = 12;
	private static final int NORMAL_MODE = 0;
	private static final int TREASURE_MODE = 1;

	private static final double FREE_SPIN_MULTIPLIER = 3.0;

	private int freeSpinsRemaining;
	private int gameMode;
	private boolean finished;
	private double betAmount;
	private int maxPayline;
	private List<Integer[]> queuedBonuses;
	private double[] bonusBoxes;
	private boolean freeSpin;
	private boolean placedJackpotContrib;

	@Override
	public PharaohConfiguration getConfig() {
		return (PharaohConfiguration)super.getConfig();
	}

	public PharaohRound() {
		this.freeSpinsRemaining = 0;
		this.gameMode = NORMAL_MODE;
		this.finished = false;
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.queuedBonuses = null;
		this.bonusBoxes = null;
		this.freeSpin = false;
		this.placedJackpotContrib = false;
	}


	public void roundInit(AbstractGameRound parentRound, int roundId, byte[] seed) throws ActionFailedException {
		PharaohRound parent = (PharaohRound) parentRound;

		this.freeSpinsRemaining = parent.freeSpinsRemaining;

		if (this.freeSpinsRemaining > 0) {
			this.betAmount = parent.betAmount;
			this.maxPayline = parent.maxPayline;

			GameAction freeSpinAction = new GameAction(null, this, 0, FREE_SPIN_ACTION, maxPayline+" "+betAmount, 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();

			freeSpinAction.commit();
			actionCompleted(freeSpinAction);
			this.freeSpin = true;
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {

		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	protected void loadAction(GameAction action) throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())){
			try{
				this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
				this.freeSpin = true;
				String[] tokens = action.getArea().split(" ");
				if (tokens.length == 2){
					this.maxPayline = Integer.parseInt(tokens[0]);
					this.betAmount = Double.parseDouble(tokens[1]);
				} else {
					throw new ActionFailedException("Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e){
				throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
			}
		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (BOX_SELECT_ACTION.equals(action.getDescription())){
			performBoxSelect(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())){
			performStartFreeSpin(action);
		}
	}

	private void performBoxSelect(GameAction action) throws ActionFailedException{
		if (gameMode != TREASURE_MODE){
			throw new ActionFailedException("Not in treasure bonus mode!");
		}

		int area = -1;
		try {
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
			area = -1;
		}

		if ((area < 0) || (area >= TREASURE_BOXES_NO)){
			throw new ActionFailedException("Invalid area.");
		}

		if (bonusBoxes == null){
			bonusBoxes = new double[TREASURE_BOXES_NO];
		}

		if (bonusBoxes[area] < 0){
			throw new ActionFailedException("Box already opened!");
		}

		/*FIXED ODD CODE*/
		com.magneta.games.fixedodd.PharaohGuard.guardBonus(this.game.getGameId(), this.options.getOwner(), bonusBoxes, area, this.options.getGameSettings(), action);

		Object[] winConstraint = getConfig().getWinCombo(queuedBonuses.get(0)[1]);
		int boxesAllowed = (Integer)winConstraint[1];

		int openCount = 1;
		for (double d: bonusBoxes){
			if (d < 0){
				openCount++;
			}
		}
		double winAmount = bonusBoxes[area];
		if (winAmount > 0.0){
			action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, winAmount);
		}

		action.setActionValue(String.valueOf(boxesAllowed-openCount));
		action.commit();
		bonusBoxes[area] = -1;

		if (openCount >= boxesAllowed){
			bonusBoxes = null;
			queuedBonuses.remove(0);

			if (queuedBonuses.isEmpty()){
				endRound(action);
			} else {
				GameAction bonusAction = action.createGeneratedAction(null, 0, TREASURE_BONUS_ACTION, String.valueOf(queuedBonuses.get(0)[0]), 0.0);
				bonusAction.setActionValue(String.valueOf(queuedBonuses.get(0)[1]));

				bonusAction.commit();
				initBoxes();
				this.gameMode = TREASURE_MODE;
			}
		}
	}

	private void initBoxes(){
		if ((bonusBoxes == null) && (!queuedBonuses.isEmpty())){
			this.bonusBoxes = new double[TREASURE_BOXES_NO];
			Object[] winConst = getConfig().getWinCombo(queuedBonuses.get(0)[1]);
			double totalPayout = getConfig().getPayout(queuedBonuses.get(0)[1]);
			int symbols = (Integer)winConst[1];

			int i = 0;
			for (;i <= symbols;i++){
				double amount = totalPayout / 2;
				totalPayout = totalPayout / 2;
				int nearest5multiple = (((int)amount + 2) / 5) * 5;
				this.bonusBoxes[i] = nearest5multiple * betAmount;
			}

			for (;i < bonusBoxes.length;i++){
				double amount = (1 + getRandom().nextInt((int)totalPayout));
				double roundedAmount;
				if (amount <= 10.0){
					roundedAmount = Math.rint(amount);
				} else {
					roundedAmount = (((int)amount + 2) / 5) * 5;
				}
				this.bonusBoxes[i] = Math.rint(roundedAmount * betAmount * 100.0) / 100.0;
			}

			for (int j=0;j < bonusBoxes.length;j++){
				int swap = getRandom().nextInt(bonusBoxes.length);
				double tmp = bonusBoxes[swap];
				bonusBoxes[swap] = bonusBoxes[j];
				bonusBoxes[j] = tmp;
			}
		}
	}

	private void performStartFreeSpin(GameAction action) throws ActionFailedException {
		if (!freeSpin){
			throw new ActionFailedException("Not in free spin mode!");
		} else if (gameMode != NORMAL_MODE) {
			throw new ActionFailedException("Spin not allowed at this time.");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if (freeSpin) {
			throw new ActionFailedException("Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		} else if (gameMode != NORMAL_MODE){
			throw new ActionFailedException("Bet not allowed at this time.");
		} 

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
		}

		if ((area < 0) || (area >= 15)){
			throw new ActionFailedException("Invalid action area");
		}


		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void startRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		int[][] roundReels = getConfig().getReels(0);
		int[] reelStops = spinReels(roundReels);

		/*FIXED ODDS CODE*/
		com.magneta.games.fixedodd.PharaohGuard.guardRound(game.getGameId(), options.getOwner(), freeSpin, getConfig(), WILD_SYMBOL, FREE_SPIN_MULTIPLIER, betAmount, maxPayline, reelStops, options.getGameSettings(), action);

		int[][] reelsView = calculateReelView(roundReels, reelStops);

		createReelViewAction(action, reelsView);

		int[] currLine = new int[roundReels.length];
		queuedBonuses = new ArrayList<Integer[]>(); 

		for (int i=0;i<= maxPayline;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(getConfig(), currLine);
			if (combo < 0) {
				continue;
			}

			if (((Integer)getConfig().getWinCombo(combo)[0]) == BONUS_SYMBOL) {
				queuedBonuses.add(new Integer[]{i, combo});
			} else {
				double multiplier = getConfig().getPayout(combo);
				double winAmount = multiplier * betAmount * (freeSpin ? FREE_SPIN_MULTIPLIER : 1.0);

				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					GameAction winAction = action.createGeneratedAction(
							action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
					winAction.setActionValue(String.valueOf(combo));
					winAction.commit();
				}
			}
		}

		int viewCombo = getReelViewWinCombination(getConfig(), reelsView);

		if (viewCombo != -1){
			double multiplier = getConfig().getPayout(viewCombo);
			double winAmount = multiplier * (betAmount * (maxPayline+1)) * (freeSpin ? FREE_SPIN_MULTIPLIER : 1.0);

			if (winAmount > 0.001) {
				GameAction winAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, winAmount);
				winAction.setActionValue(String.valueOf(viewCombo));
				winAction.commit();
			}

			if (((Integer)getConfig().getWinCombo(viewCombo)[1]) >= 3){
				GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, String.valueOf(viewCombo), 0.0);
				freeSpinsAction.setActionValue("10");
				freeSpinsAction.commit();
				this.freeSpinsRemaining += 10;
			}
		}

		if (!queuedBonuses.isEmpty()){
			GameAction bonusAction = action.createGeneratedAction(null, 0, TREASURE_BONUS_ACTION, String.valueOf(queuedBonuses.get(0)[0]), 0.0);
			bonusAction.setActionValue(String.valueOf(queuedBonuses.get(0)[1]));

			action.commit();
			initBoxes();
			this.gameMode = TREASURE_MODE;
		} else {
			endRound(action);
		}
	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public static int getPaylineWinCombination(SlotConfiguration config, int[] payline) {
		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			int j=0;
			boolean foundSymbol = false;
			for (;j<payline.length;j++){
				if (payline[j] == symbol){
					foundSymbol = true;
				} else if ((payline[j] == WILD_SYMBOL) && (symbol != PharaohConfiguration.BONUS_SYMBOL)) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol){
				return i;
			}
		}        

		return -1;
	}

	/**
	 * Checks if the given reel view forms any winning combinations.
	 * @param reelView The view to check.
	 * @return The combination the view forms or -1 if it doesn't form any.
	 */
	public static final int getReelViewWinCombination(SlotConfiguration config, int[][] reelView){
		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];

			if (symbol < 0){
				int quantity = (Integer)winCombos[i][1];

				for (int j=0;j<reelView[0].length;j++){
					for (int k=0;k<3;k++){
						if (reelView[k][j] == (-symbol)){
							quantity--;
							break;
						}
					}

					if (quantity <= 0){
						return i;
					}
				}
			}
		}        

		return -1;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
		super.getExtraInfo(player, roundInfo, firstTime);

		if (freeSpin){
			roundInfo.put("free_spins_left", this.freeSpinsRemaining - 1);
		}
	}
}
