/**
 * 
 */
package com.magneta.games.slots;


/**
 * @author anarxia
 *
 */
public class BonusBox {

    private double payout;
    private boolean rowStop;
    private boolean opened;
    
    public BonusBox() {
        this.payout = 0.0;
        this.rowStop = false;
        this.opened = false;
    }
    
    public BonusBox(double payout, boolean rowStop) {
        this.payout = payout;
        this.rowStop = rowStop;
        this.opened = false;
    }
    
    public void openBox() {
        this.setOpened(true);
    }
    
    /**
     * @return Returns the payout.
     */
    public double getPayout() {
        return payout;
    }

    public void setPayout(double payout) {
        this.payout = payout;
    }
    
    /**
     * @return Returns the rowStop.
     */
    public boolean isRowStop() {
        return rowStop;
    }

    /**
     * @return Returns the rowStop.
     */
    public void setRowStop(boolean rowStop) {
        this.rowStop = rowStop;
    }
    
    /**
     * @param opened The opened to set.
     */
    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    /**
     * @return Returns the opened.
     */
    public boolean isOpened() {
        return opened;
    }
}
