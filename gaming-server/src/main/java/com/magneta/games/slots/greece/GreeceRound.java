package com.magneta.games.slots.greece;

import static com.magneta.games.configuration.slots.GreeceConfiguration.FREE_SPIN_SYMBOL;
import static com.magneta.games.configuration.slots.GreeceConfiguration.MAXIMUM_BOXES_TO_OPEN;
import static com.magneta.games.configuration.slots.GreeceConfiguration.BONUS_SYMBOL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.magneta.Config;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.GreeceConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.BonusBox;
import com.magneta.games.slots.SlotRound;

@GameTemplate("Greece")
public class GreeceRound extends SlotRound {
	
    private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";

    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";

    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";

    private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";

    private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";

    

    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
            /* Description Area Amount */
            new GameActionRequestDefinition(BET_ACTION, ParameterType.REQUIRED,
                    ParameterType.REQUIRED),
            new GameActionRequestDefinition(BOX_SELECT_ACTION,
                    ParameterType.REQUIRED, ParameterType.FORBIDDEN),
            new GameActionRequestDefinition(START_FREE_SPIN_ACTION,
                    ParameterType.FORBIDDEN, ParameterType.FORBIDDEN) };


    private static final int NORMAL_MODE = 0;
    private static final int TREASURE_MODE = 1;

    private static final int WILD_SYMBOL = GreeceConfiguration.WILD_SYMBOL;

    private static final double FREE_SPIN_MULTIPLIER = 2.0;
   
	private final CurrencyParser currencyParser;

    private int freeSpinsRemaining;

    private int gameMode;

    private boolean finished;

    private double betAmount;

    private int maxPayline;

    private BonusBox[] bonusBoxes;

    private int bonusBoxesToOpen;

    private boolean freeSpin;

    private boolean placedJackpotContrib;

    public GreeceRound(CurrencyParser currencyParser) {
    	this.currencyParser = currencyParser;
        this.betAmount = 0.0;
        this.maxPayline = -1;
        this.freeSpinsRemaining = 0;
        this.gameMode = NORMAL_MODE;
        this.finished = false;
        this.freeSpin = false;
        this.placedJackpotContrib = false;
        this.bonusBoxes = null;
    }
    
    @Override
	public GreeceConfiguration getConfig() {
    	return (GreeceConfiguration)super.getConfig();
    }

    @Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
    	GreeceRound parent = (GreeceRound)parentRound;

        this.freeSpinsRemaining = parent.freeSpinsRemaining;

        if (this.freeSpinsRemaining > 0) {
            this.betAmount = parent.betAmount;
            this.maxPayline = parent.maxPayline;

            GameAction freeSpinAction = new GameAction(null, this, 0,
                    FREE_SPIN_ACTION, maxPayline + " " + betAmount, 0.0);
            freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
            freeSpinAction.setRoundStart();

            freeSpinAction.commit();
            actionCompleted(freeSpinAction);
            this.freeSpin = true;
        }
    }
    
    @Override
    protected void validateGameActionRequest(GameActionRequest request)
            throws ActionFailedException {

        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    @Override
    protected void loadAction(GameAction action)
            throws ActionFailedException {
        if (FREE_SPIN_ACTION.equals(action.getDescription())) {
            try {
                this.freeSpinsRemaining = Integer.parseInt(action
                        .getActionValue());
                this.freeSpin = true;
                String[] tokens = action.getArea().split(" ");

                if (tokens.length == 2) {
                    this.maxPayline = Integer.parseInt(tokens[0]);
                    this.betAmount = Double.parseDouble(tokens[1]);
                } else {
                    throw new ActionFailedException(
                            "Free spin load failed. Values missing.");
                }
            } catch (NumberFormatException e) {
                throw new ActionFailedException(
                        "Free spin load failed. An invalid value was passed.");
            }

            actionCompleted(action);

        } else {
            doAction(action);
        }
    }

    @Override
    protected void performAction(GameAction action)
            throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())) {
            performBet(action);
        } else if (BOX_SELECT_ACTION.equals(action.getDescription())) {
            performBoxSelect(action);
        } else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
            performFreeSpin(action);
        }
    }

    private void performBoxSelect(GameAction action)
            throws ActionFailedException {
        if (gameMode != TREASURE_MODE) {
            throw new ActionFailedException("Not in treasure bonus mode!");
        }

        int area = -1;
        try {
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e) {
            area = -1;
        }

        if ((area < 0) || (area >= bonusBoxes.length)) {
            throw new ActionFailedException("Invalid area.");
        }

        if (bonusBoxes[area].isOpened()) {
            throw new ActionFailedException("Box already opened!");
        }

        logDebug("After pick (Selected:" + area + ")");
		logBonusBoxes();
		
		com.magneta.games.fixedodd.GreeceGuard.getPossibleBoxBonus(action, betAmount, game.getGameId(), options.getOwner(), options.getGameSettings(),bonusBoxes,area, bonusBoxesToOpen,getGuardRandom());
		
		logDebug("After guard. (Selected:" + area + ")");
		logBonusBoxes();
		
        double winAmount = bonusBoxes[area].getPayout();

        if (winAmount > 0.0) {
            action.createGeneratedAction(action.getPlayer(), 1,
                    WIN_ACTION, null, winAmount);
            action.commit();
            bonusBoxes[area].setOpened(true);
            this.bonusBoxesToOpen--;

            if (this.bonusBoxesToOpen == 0) {
                endRound(action);
            }

        } else {
            endRound(action);
        }
    }
    
    
    private void logBonusBoxes() {
		StringBuilder builder = new StringBuilder();

		for (int i=0; i < this.bonusBoxes.length; i++) {
			builder.append('[');
			if (bonusBoxes[i].isRowStop()) {
				builder.append('*');
			}
			builder.append(bonusBoxes[i].getPayout());
			builder.append(']');
		}

		logDebug("Bonus boxes:\n" + builder.toString());
	}


    /**
     * Shuffles a bonus box row
     * 
     * @param bonusRow
     */
    private void shuffleBonusRow(BonusBox[] bonusBoxRow) {
        BonusBox tmp;

        for (int i = 0; i < bonusBoxRow.length; i++) {
            int swap = getRandom().nextInt(bonusBoxRow.length);
            tmp = bonusBoxRow[swap];
            bonusBoxRow[swap] = bonusBoxRow[i];
            bonusBoxRow[i] = tmp;
        }
    }

    private double calculateBox(double t, double b, double c, double d) {
        t = t / d;
        return c * t * t + b;
    }

    private void initBoxes(List<Integer[]> queuedBonuses) {
        double totalPayout = 0.0;
        int boxesToInit = 0;
        
        /* Calculate total payout for the bonus */
        for (Integer[] queuedBonus : queuedBonuses) {
            totalPayout += getComboPayout(queuedBonus[1]);
            boxesToInit += (Integer)getConfig().getWinCombo(queuedBonus[1])[4];
        }
        
        //Line Combination
        totalPayout *= this.betAmount;

        if (boxesToInit > MAXIMUM_BOXES_TO_OPEN) {
            boxesToInit = MAXIMUM_BOXES_TO_OPEN;
        }
        
        this.bonusBoxes = new BonusBox[boxesToInit];

        double maxAmountToGive = totalPayout;

        for (int i = 0; i < this.bonusBoxes.length; i++) {
            bonusBoxes[i] = new BonusBox();
        }

        /* Create bonus boxes */
        double maxOtherAmount = maxAmountToGive;
        double prevBoxAmount = 0.0;

        for (int i = 0; i < this.bonusBoxesToOpen; i++) {
            double boxAmount = calculateBox(i + 1, 0, (maxAmountToGive),
                    this.bonusBoxesToOpen);
            double finalBoxAmount = Math
                    .floor(((boxAmount - prevBoxAmount) + 0.05) * 10) / 10;
            bonusBoxes[i].setPayout(finalBoxAmount);
            prevBoxAmount = boxAmount;
            if (boxAmount < maxOtherAmount) {
                maxOtherAmount = boxAmount;
            }
        }

        for (int i = this.bonusBoxesToOpen; i < this.bonusBoxes.length - 1; i++) {
            /* Select a random multiplier from 0.5 to 1.0 */
            double multiplier = getRandom().nextDouble() / 2.0;
            multiplier += 0.5;

            double amount = multiplier * maxOtherAmount;
            amount = Math.floor(amount * 10.00) / 10.00;
            /* Avoid extra zeros */
            if (amount < 0.10) {
                amount = 0.10;
            }

            bonusBoxes[i].setPayout(currencyParser.normalizeDouble(amount));
        }
        
        bonusBoxes[this.bonusBoxes.length - 1].setPayout(0.00);
        
        /* Shuffle boxes */
        shuffleBonusRow(bonusBoxes);

        /* Log bonus boxes */
        if (Config.getBoolean("games.log.bonus", false)) { 
        	StringBuilder builder = new StringBuilder();

        	for (int i=0; i < this.bonusBoxes.length; i++) {
        		builder.append('[');
        		if (this.bonusBoxes[i].isRowStop()) {
        			builder.append('*');
        		}
        		builder.append(this.bonusBoxes[i].getPayout());
        		builder.append(']');
        	}

        	logInfo("Bonus boxes:\n" + builder.toString());
        }
    }

    private void performFreeSpin(GameAction action)
            throws ActionFailedException {
        if (!freeSpin) {
            throw new ActionFailedException("Not in free spin mode!");
        } else if (gameMode != NORMAL_MODE) {
            throw new ActionFailedException("Spin not allowed at this time.");
        }

        action.commit();
        this.freeSpinsRemaining--;
        startRound(action);
    }

    /**
     * Performs the bet action.
     * 
     * @param action
     *            The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (freeSpin) {
            throw new ActionFailedException(
                    "Round is a free spin; No bets allowed.");
        } else if (betAmount > 0.0) {
            throw new ActionFailedException("Bet failed: already placed bet!");
        } else if (gameMode != NORMAL_MODE) {
            throw new ActionFailedException("Bet not allowed at this time.");
        }

        checkBet(action);

        int area = -1;

        try {
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e) {
        }

        if ((area < 0) || (area >= getConfig().getPaylines().length)) {
            throw new ActionFailedException("Invalid action area");
        }

        double betUnit;
        double totalBet;

        if (action.isLoaded()) {
            totalBet = action.getAmount();
            betUnit = action.getAmount() / (area + 1);
        } else {
            betUnit = action.getAmount();
            totalBet = betUnit * (area + 1);
            action.setAmount(totalBet);
        }

        action.setRoundStart();
        action.commit();

        betAmount = betUnit;
        maxPayline = area;

        placedJackpotContrib = JackpotsHandler
                .contributeToJackpot(this, action);

        startRound(action);
    }

    private void startRound(GameAction action) throws ActionFailedException {
        if (START_FREE_SPIN_ACTION.equals(action.getDescription())
                && (freeSpinsRemaining < 0)) {
            this.maxPayline = -1;
            this.betAmount = 0.0;
            this.freeSpinsRemaining = 0;
            this.freeSpin = false;
            throw new IllegalStateException(
                    "No free spins remain. Round must be started normally.");
        } else if (finished) {
            throw new IllegalStateException(
                    "Method startRound() already called once.");
        }

        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0) {
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio,
                placedJackpotContrib)) {
            this.finished = true;
            return;
        }

        int[] reelStops = spinReels(getConfig().getReels(0));
        /*
         * StringBuilder rS = new StringBuilder("Reel Stops:"); for (int
         * reelStop: reelStops) { rS.append(" "); rS.append(reelStop); }
         * Log.info(rS.toString());
         */
        
        
        /* FIXED ODDS CODE */
		reelStops = com.magneta.games.fixedodd.GreeceGuard.guardRound(game.getGameId(), options.getOwner(), getConfig(), this.betAmount, reelStops, maxPayline, options.getGameSettings(), action, this, getGuardRandom());
        evaluatePosition(action, jackpotRatio, reelStops, getConfig(), maxPayline,betAmount,placedJackpotContrib,freeSpin,true, new Double[]{0.0}); 
		if(this.gameMode==TREASURE_MODE){
			// do nothing
		}else {
			endRound(action);
		}
    }

    public int evaluatePosition(GameAction action, double jackpotRatio,
			int[] reelStops, SlotConfiguration config, int maxPayline, double betAmount, boolean placedJackpotContrib,boolean freeSpin,boolean isProduction, Double[] winsum) throws ActionFailedException {

    	int[][] reelsView = calculateReelView(getConfig().getReels(0), reelStops);

    	logDebug("Printing reelview");
		for (int i = 0; i < reelsView.length; i++) {
			int[] row = reelsView[i];
			logDebug(Arrays.toString(row));
		}
		
		if(isProduction){
			createReelViewAction(action, reelsView);
		}
		
        int[] currLine = new int[getConfig().getReels(0).length];

        List<Integer[]> queuedBonuses = new ArrayList<Integer[]>();

        int freeSpins = 0;

        for (int i = 0; i <= maxPayline; i++) {
            getPayline(reelsView, i, currLine);

            int combo = getPaylineWinCombination(currLine);

            if (combo < 0)
                continue;

            double multiplier = getComboPayout(combo)* (freeSpin? FREE_SPIN_MULTIPLIER : 1.0);
            double winAmount = multiplier * betAmount;
            
            if (winAmount > 0.0) {
				winsum[0] = new Double(winsum[0]+winAmount);
			}
            
            logDebug("line "+i+" has won " + winAmount);

            if(isProduction){
            	if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)
            			&& JackpotsHandler.claimGameJackpot(this, action, i,
            					winAmount, jackpotRatio)) {
            		// stop the game from retrying to claim jackpot on other line
            		placedJackpotContrib = false;
            	} else {
            		if (winAmount > 0.0) {
            			GameAction winAction = action.createGeneratedAction(
            					action.getPlayer(), 1, WIN_ACTION,
            					String.valueOf(i), winAmount);
            			winAction.setActionValue(String.valueOf(combo));
            		}

            		//                replaced bu method getBonusWinCombination
	                Integer symbol = (Integer)getConfig().getWinCombo(combo)[0];
	
	                if (symbol.equals(GreeceConfiguration.BONUS_SYMBOL)) {
	                    int bonusSymbolCount = (Integer)getConfig().getWinCombo(combo)[1];
	                    int bonusCombo = combo;
	
	                    /*
	                     * Set the bonus Combo to the real combo The combo found is
	                     * for the combination we must replace it with the
	                     * respective bonus win combination.
	                     */
	                    Object[][] WIN_CONSTRAINTS = getConfig().getWinCombos();
	                    
	                    for (int j = WIN_CONSTRAINTS.length - 1; j >= 0; j--) {
	                        if (WIN_CONSTRAINTS[j][0].equals(0)
	                                && WIN_CONSTRAINTS[j][1]
	                                        .equals(bonusSymbolCount)) {
	                            bonusCombo = j;
	                            break;
	                        }
	                    }
	
	                    queuedBonuses.add(new Integer[] {i, bonusCombo});
	                }

            	}
            }
        }
        
        
        int viewCombo = getReelViewWinCombination(reelsView);

        if (viewCombo != -1) {

            if (((Integer)getConfig().getWinCombo(viewCombo)[0]) == -GreeceConfiguration.FREE_SPIN_SYMBOL) {

                int symbolCount = (Integer)getConfig().getWinCombo(viewCombo)[1];

                if (symbolCount == 3) {
                    freeSpins += 10;
                } else if (symbolCount == 4) {
                    freeSpins += 20;
                } else if (symbolCount == 5) {
                    freeSpins += 30;
                }
            }
        }

        if (freeSpins > 0) {
        	if(isProduction){
        		GameAction freeSpinsAction = action.createGeneratedAction(null, 0,
        				FREE_SPINS_WIN_ACTION, "", 0.0);
        		freeSpinsAction.setActionValue(Integer.toString(freeSpins));
        		freeSpinsAction.commit();
        		this.freeSpinsRemaining += freeSpins;
        	}
        }

        //queuedBonuses =getBonusWinCombination(config, reelsView, maxPayline);
        
        if (!queuedBonuses.isEmpty()) {
        	if(isProduction){
        		for (Integer[] bonus : queuedBonuses) {
        			GameAction bonusAction = action.createGeneratedAction(null, 0,
        					TREASURE_BONUS_ACTION, String.valueOf(bonus[0]), 0.0);
        			bonusAction.setActionValue(String.valueOf(bonus[1]));
        			this.bonusBoxesToOpen += (Integer)getConfig().getWinCombo(bonus[1])[3];
        		}
        		if(this.bonusBoxesToOpen > MAXIMUM_BOXES_TO_OPEN) {
        			this.bonusBoxesToOpen = MAXIMUM_BOXES_TO_OPEN;
        		}
        		action.commit();
        		initBoxes(queuedBonuses);
        		this.gameMode = TREASURE_MODE;
        	}else{
        		double totalPayout = 0.0;
        		/* Calculate total payout for the bonus */
                for (Integer[] queuedBonus : queuedBonuses) {
                    totalPayout += getComboPayout(queuedBonus[1]);
                }
                totalPayout*= this.betAmount;
                winsum[0] = new Double(winsum[0]+totalPayout);
        	}
        } 
        return viewCombo;
	}
    
    
//    private List<Integer[]> getBonusWinCombination(SlotConfiguration config,
//    		int[][] reelsView,int maxPayline) {
//
//    	List<Integer[]> queuedBonuses = new ArrayList<Integer[]>();
//    	
//    	for (int i = 0; i <= maxPayline; i++) {
//
//    		int[] currLine = new int[getConfig().getReels(0).length];
//    		
//    		getPayline(reelsView, i, currLine);
//
//    		int combo = getPaylineWinCombination(currLine);
//
//    		if (combo < 0)
//    			continue;
//
//    		Integer symbol = (Integer)getConfig().getWinCombo(combo)[0];
//
//    		if (symbol.equals(GreeceConfiguration.BONUS_SYMBOL)) {
//    			
//    			int bonusSymbolCount = (Integer)getConfig().getWinCombo(combo)[1];
//    			int bonusCombo = combo;
//
//    			/*
//    			 * Set the bonus Combo to the real combo The combo found is
//    			 * for the combination we must replace it with the
//    			 * respective bonus win combination.
//    			 */
//    			Object[][] WIN_CONSTRAINTS = getConfig().getWinCombos();
//
//    			for (int j = WIN_CONSTRAINTS.length - 1; j >= 0; j--) {
//    				if (WIN_CONSTRAINTS[j][0].equals(0)
//    						&& WIN_CONSTRAINTS[j][1]
//    								.equals(bonusSymbolCount)) {
//    					bonusCombo = j;
//    					break;
//    				}
//    			}
//
//    			queuedBonuses.add(new Integer[] {i, bonusCombo});
//    		}
//
//    	}
//    	return queuedBonuses;
//    }

    private void endRound(GameAction action) throws ActionFailedException {
        if (finished) {
            throw new IllegalStateException(
                    "Method endRound() already called once.");
        }

        action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        action.commit(true);
        this.finished = true;
    }
    
    /**
     * Checks if the given payline forms any winning combinations.
     * 
     * @param payline
     *            The payline to check.
     * @return The combination the payline forms or -1 if it doesn't form any.
     */
    public int getPaylineWinCombination(int[] payline) {
        Object[][] winCombos = getConfig().getWinCombos();
        int maxPayoutCombo = -1;
        
        for (int i = winCombos.length - 1; i >= 0; i--) {
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            if ((symbol != BONUS_SYMBOL && symbol != FREE_SPIN_SYMBOL) && getComboPayout(i) < 0.01) {
                continue;
            }
            
            int j;

            boolean foundSymbol = false;
            for (j = 0; j < payline.length; j++) {
                if (payline[j] == symbol) {
                    foundSymbol = true;
                } else if ((payline[j] == WILD_SYMBOL)
                        && (symbol != GreeceConfiguration.BONUS_SYMBOL)
                        && (symbol != GreeceConfiguration.FREE_SPIN_SYMBOL)) {
                    continue;
                } else {
                    break;
                }
            }

            if ((j == quantityRequired) && foundSymbol) {
                if (getComboPayout(i) > getComboPayout(maxPayoutCombo)) {
                    maxPayoutCombo = i;
                }
                
                /* Always prefer bonus and free spins over normal combinations */
                if (symbol == BONUS_SYMBOL || symbol == FREE_SPIN_SYMBOL)
                    return i;
            }
        }

        return maxPayoutCombo;
    }

    /**
     * Checks if the given reel view forms any winning combinations.
     * 
     * @param reelView
     *            The view to check.
     * @return The combination the view forms or -1 if it doesn't form any.
     */
    public final int getReelViewWinCombination(int[][] reelView) {
        Object[][] winCombos = getConfig().getWinCombos();

        for (int i = winCombos.length - 1; i >= 0; i--) {
            int symbol = (Integer)winCombos[i][0];
            int quantity = (Integer)winCombos[i][1];

            if (symbol >= 0)
                continue;

            symbol = symbol * -1;
            boolean foundOnLine = false;

            for (int j = 0; j < reelView[0].length; j++) {
                foundOnLine = false;
                for (int k = 0; k < 3; k++) {
                    if (reelView[k][j] == symbol) {
                        foundOnLine = true;
                        break;
                    }
                }

                if (foundOnLine) {
                    quantity--;
                }

                if (quantity <= 0) {
                    return i;
                }
            }

        }

        return -1;
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    public double getComboPayout(int combo) {
        if (combo < 0) {
            return 0.0;
        }
        
        return getConfig().getPayout(combo);
    }
    
    public double evaluatePositionWinnings(int[] reelStops) throws ActionFailedException {
		Double[] winsum = new Double[]{0.0};
		this.evaluatePosition(null, 0.0, reelStops, getConfig(), this.maxPayline, this.betAmount, false, this.freeSpin, false, winsum);
		return winsum[0];
	}
}
