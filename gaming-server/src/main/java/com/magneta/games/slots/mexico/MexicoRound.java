package com.magneta.games.slots.mexico;

import static com.magneta.games.configuration.slots.MexicoConfiguration.SCATTER_SYMBOL;
import static com.magneta.games.configuration.slots.MexicoConfiguration.WILD_SYMBOL;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.MexicoConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.MexicoGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("Mexico")
public class MexicoRound extends SlotRound {
	
	private static final Logger log = LoggerFactory.getLogger(MexicoRound.class);

	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	private static final String SCATTER_SELECT = "g:SCATTER_SELECT";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.FORBIDDEN,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(SCATTER_SELECT, ParameterType.REQUIRED, ParameterType.FORBIDDEN)
	};

	private static final double[] FREE_SPIN_MULTIPLIERS = {2.0,3.0,4.0};

	private int freeSpinsRemaining;
	private boolean finished;
	private double betAmount;

	private boolean freeSpin;
	private boolean placedJackpotContrib;
	private RoundState state;
	private double freeSpinMultiplier;
	private int scatterSelections;

	private enum RoundState {
		NORMAL,
		SELECT_SCATTER
	}

	@Override
	public MexicoConfiguration getConfig() {
		return (MexicoConfiguration) super.getConfig();
	}

	public MexicoRound() {
		this.betAmount = 0.0;
		this.freeSpinsRemaining = 0;
		this.finished = false;
		this.state = RoundState.NORMAL;
		this.freeSpin = false;
		this.placedJackpotContrib = false;
	}

	@Override
	protected void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		MexicoRound parent = (MexicoRound) parentRound;

		this.freeSpinsRemaining = parent.freeSpinsRemaining;

		if (this.freeSpinsRemaining > 0){
			this.betAmount = parent.betAmount;
			this.freeSpinMultiplier = parent.freeSpinMultiplier;

			GameAction freeSpinAction = new GameAction(null, this, 0, FREE_SPIN_ACTION,betAmount + " "+freeSpinMultiplier, 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();
			freeSpinAction.commit();
			this.freeSpin = true;

			actionCompleted(freeSpinAction);            
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	protected void loadAction(GameAction action) throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())){
			try{
				this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
				this.freeSpin = true;
				String[] tokens = action.getArea().split(" ");

				if (tokens.length == 2) {
					this.betAmount = Double.parseDouble(tokens[0]);
					this.freeSpinMultiplier = Double.parseDouble(tokens[1]);
				} else {
					throw new ActionFailedException("Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e){
				throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
			}

			actionCompleted(action);

		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())){
			performFreeSpin(action);
		} else if (SCATTER_SELECT.equals(action.getDescription())) {
			performScatterSelect(action);
		}
	}

	private void performScatterSelect(GameAction action) throws ActionFailedException {
		if (this.state != RoundState.SELECT_SCATTER) {
			throw new ActionFailedException("Not in scatter select mode!");
		}

		try {
			Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
			throw new ActionFailedException("Invalid option number");
		}

		this.freeSpinMultiplier = FREE_SPIN_MULTIPLIERS[getRandom().nextInt(this.scatterSelections)];
		action.setActionValue(String.valueOf(this.freeSpinMultiplier));

		endRound(action);
	}

	private void performFreeSpin(GameAction action) throws ActionFailedException {
		if (!freeSpin){
			throw new ActionFailedException("Not in free spin mode!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if (freeSpin) {
			throw new ActionFailedException("Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		action.setRoundStart();
		action.commit();

		betAmount = action.getAmount();

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void startRound(GameAction action) throws ActionFailedException {
		if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)){
			this.betAmount = 0.0;
			this.freeSpinsRemaining = 0;
			this.freeSpin = false;
			throw new IllegalStateException("No free spins remain. Round must be started normally.");
		} else if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		int[][] reels = getConfig().getReels(freeSpin ? 1: 0);
		int[] reelStops = spinReels(reels);
		
		log.debug("prin : "+Arrays.toString(reelStops));
		reelStops = MexicoGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reels, reelStops, options.getGameSettings(), action, this);
		log.debug("meta : "+Arrays.toString(reelStops));

		evaluateposition(action, jackpotRatio, reels, reelStops,true);
		if(this.state == RoundState.SELECT_SCATTER){
			return;
		}

		endRound(action);
	}

	public Double evaluateposition(GameAction action, double jackpotRatio,
			int[][] reels, int[] reelStops, boolean isProduction) throws ActionFailedException {

		Double result = new Double(0.0);
		int[][] reelsView = calculateReelView(reels, reelStops);
		if(isProduction){
			createReelViewAction(action, reelsView);
			action.commit();
		}
		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		int freeSpins = 0;
		double roundMultiplier = 1.0;

		if (freeSpin) {
			roundMultiplier *= this.freeSpinMultiplier;
		}

		for (int i=0;i < getConfig().getPaylines().length; i++) {
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			if (combo < 0)
				continue;

			double multiplier = roundMultiplier * getPaylineMultiplier(currLine) * (Double)getComboPayout(combo);
			double winAmount = multiplier * betAmount;
			if(isProduction){
				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					GameAction winAction = action.createGeneratedAction(
							action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
					winAction.setActionValue(String.valueOf(combo));
					winAction.commit();
				}
			}else{
				result = result + winAmount;
			}
		}

		int viewCombo = getReelViewWinCombination(getConfig(), reelsView);

		if (viewCombo != -1){

			double multiplier = roundMultiplier * (Double)getComboPayout(viewCombo);
			double winAmount = multiplier * betAmount;

			if (winAmount > 0.01) {
				result = result + winAmount;
				if(isProduction){
					GameAction winAction = action.createGeneratedAction(
							action.getPlayer(), 1, WIN_ACTION, null, winAmount);
					winAction.setActionValue(String.valueOf(viewCombo));
				}
			}

			/* Scatter gives free spins */
			if(isProduction){
				if (((Integer)getConfig().getWinCombo(viewCombo)[0]).equals(-SCATTER_SYMBOL)) {
					freeSpins += 15;
				}
				if (freeSpins > 0) {
					int selections = (Integer)getConfig().getWinCombo(viewCombo)[1];

					GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);

					freeSpinsAction.setActionValue(Integer.toString(freeSpins));
					action.commit();
					this.freeSpinsRemaining += freeSpins;
					this.scatterSelections = selections;
					this.state = RoundState.SELECT_SCATTER;
					
					return result;
				}
			}
		}
		return result;
	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if ((symbol != SCATTER_SYMBOL) && (Double)getComboPayout(i) < 0.01) {
				continue;
			}

			int j;
			//try from the left 
			boolean foundSymbol = false;
			for (j=0;j<payline.length;j++){
				if (payline[j] < 0) {
					continue;
				}

				if (payline[j] == symbol){
					foundSymbol = true;
				} else if ((payline[j] == WILD_SYMBOL) && (symbol != SCATTER_SYMBOL)) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol){
				return i;
			}

			int quantityFound = 0;
			//try from the right
			for (j=payline.length-1;j>=0;j--) {
				if (payline[j] < 0) {
					continue;
				}

				if (payline[j] == symbol){
					foundSymbol = true;
					quantityFound++;
				} else if ((payline[j] == WILD_SYMBOL) && (symbol != SCATTER_SYMBOL)) {
					quantityFound++;
					continue;
				} else {
					break;
				}
			}

			if (quantityFound == quantityRequired && foundSymbol){
				return i;
			}
		}        

		return -1;
	}

	public double getPaylineMultiplier(int [] payline) {
		return 1.0;
	}

	/**
	 * Checks if the given reel view forms any winning combinations.
	 * @param reelView The view to check.
	 * @return The combination the view forms or -1 if it doesn't form any.
	 */
	public static final int getReelViewWinCombination(SlotConfiguration config, int[][] reelView){
		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			if (symbol >= 0)
				continue;

			symbol = symbol * -1;
			boolean foundOnLine = false;

			/* Scatter symbol wins only on reel 2-4 */
			for (int j=2;j<=4;j++){
				foundOnLine = false;
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol){
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0){
					return i;
				}
			}

		}        

		return -1;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	public Object getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}

		return getConfig().getPayout(combo);
	}
	
	public double evaluatePositionWinnings(int[][] reels, int[]reelStops) throws ActionFailedException{
		return evaluateposition(null, 0.0, reels, reelStops, false);
		
	}
}
