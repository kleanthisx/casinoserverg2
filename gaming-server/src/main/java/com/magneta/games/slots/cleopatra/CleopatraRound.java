package com.magneta.games.slots.cleopatra;

import static com.magneta.games.configuration.slots.CleopatraConfiguration.FREE_SPIN_SCATTER_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.CleopatraConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.CleopatraGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("Cleopatra")
public class CleopatraRound extends SlotRound {
	
	private static final Logger log = LoggerFactory.getLogger(CleopatraRound.class);

	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
	};

	private int freeSpinsRemaining;
	private boolean finished;
	private double betAmount;
	private int maxPayline;

	private boolean placedJackpotContrib;

	List<Integer> viewCombos;

	private RoundState state;

	public enum RoundState {
		NORMAL,
		FREE_SPIN
	}

	public CleopatraRound() {
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.freeSpinsRemaining = 0;
		this.finished = false;
		this.state = RoundState.NORMAL;
		this.placedJackpotContrib = false;
	}

	@Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		CleopatraRound parent = (CleopatraRound)parentRound;
		this.freeSpinsRemaining = parent.freeSpinsRemaining;

		if (this.freeSpinsRemaining > 0) {
			this.betAmount = parent.betAmount;
			this.maxPayline = parent.maxPayline;

			GameAction freeSpinAction = new GameAction(null, this, 0,
					FREE_SPIN_ACTION, maxPayline + " " + betAmount, 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();

			freeSpinAction.commit();
			actionCompleted(freeSpinAction);
			this.state = RoundState.FREE_SPIN;
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	public CleopatraConfiguration getConfig() {
		return (CleopatraConfiguration)super.getConfig();
	}

	@Override
	protected void loadAction(GameAction action) throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())){
			try{
				this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
				this.state = RoundState.FREE_SPIN;
				String[] tokens = action.getArea().split(" ");

				if (tokens.length == 2){
					this.maxPayline = Integer.parseInt(tokens[0]);
					this.betAmount = Double.parseDouble(tokens[1]);
				} else {
					throw new ActionFailedException("Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e){
				throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
			}

			actionCompleted(action);

		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
			performFreeSpin(action);
		}
	}

	private int getfreespins(int symbol) {
		switch (symbol) {
		case FREE_SPIN_SCATTER_1:
			return 15;
		default:
			throw new RuntimeException("Invalid Free spin symbol : "+ symbol);
		}
	}

	private void performFreeSpin(GameAction action) throws ActionFailedException {
		if (this.state!=RoundState.FREE_SPIN){
			throw new ActionFailedException("Not in free spin mode!");
		}

		if (this.state == RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if ( this.state == RoundState.FREE_SPIN) {
			throw new ActionFailedException("Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)){
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void startRound(GameAction action) throws ActionFailedException {

		if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)) {
			throw new IllegalStateException("No free spins remain. Round must be started normally.");
		} else if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		int[] reelStops = spinReels(getConfig().getReels(0));
		log.debug("prin : "+Arrays.toString(reelStops));
		reelStops = CleopatraGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, 0, reelStops, options.getGameSettings(), action, this,maxPayline,getGuardRandom());
		log.debug("meta : "+Arrays.toString(reelStops));
		
		evaluatePosition(action, 0, jackpotRatio, reelStops, betAmount, maxPayline ,new Double[]{(0.0)},true);
		endRound(action);

	}

	public List<Integer> evaluatePosition(GameAction action, int reelSet,
			double jackpotRatio, int[] reelStops, double betAmount2,
			int maxPayline2, Double[] actwinsum, boolean isProduction) throws ActionFailedException {

		int[][] reelsView = calculateReelView(getConfig().getReels(reelSet), reelStops);

		if(isProduction){
			createReelViewAction(action, reelsView);
			action.commit();
		}

		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		for (int i=0;i <= maxPayline2;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			if (combo < 0)
				continue;

			double multiplier = getRoundMultiplier(combo) * getPaylineMultiplier(currLine,combo) * getComboPayout(combo);

			double winAmount = multiplier * betAmount2;
			if(isProduction){
				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					if (winAmount > 0.0) {
						actwinsum[0] = new Double(actwinsum[0] + winAmount);
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
						winAction.setActionValue(String.valueOf(combo));
						winAction.commit();
					} 
				}
			}else{
				actwinsum[0] = new Double(actwinsum[0] + winAmount);
			}
		}

		viewCombos = getReelViewWinCombinations(getConfig(), reelsView,reelSet);

		for (Integer viewCombo: viewCombos) {
			if (viewCombo >= 0) {
				double multiplier = getComboPayout(viewCombo);

				double winAmount = getRoundMultiplier(viewCombo) * multiplier * (betAmount2 * (maxPayline2 + 1));

				if (winAmount > 0.01) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
					if(isProduction){
					GameAction winAction = action.createGeneratedAction(
							action.getPlayer(), 1, WIN_ACTION, null, winAmount);
					winAction.setActionValue(String.valueOf(viewCombo));
					action.commit();
					}
				}

				if(isProduction){
					int symbol = (Integer)getConfig().getWinCombo(viewCombo)[0];
					symbol = -symbol;
					if(symbol==CleopatraConfiguration.FREE_SPIN_SCATTER_1 && (Integer)getConfig().getWinCombo(viewCombo)[1]>2 && isProduction){
						int freeSpins = getfreespins(symbol);

						action.setActionValue("FREE " + freeSpins);
						GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
						freeSpinsAction.setActionValue(Integer.toString(freeSpins));
						action.commit();
						this.freeSpinsRemaining += freeSpins;
					}
				}
			}
		}

		return viewCombos;

	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();

		int topw = -1;
		double topwp = 0.0;

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if (getComboPayout(i) < 0.01) {
				continue;
			}

			int j;

			boolean foundSymbol = false;
			for (j=0;j<payline.length;j++){
				if (payline[j] == symbol){
					foundSymbol = true;
				} else if (payline[j] == symbol||(payline[j] == CleopatraConfiguration.WILD && payline[j]!=CleopatraConfiguration.FREE_SPIN_SCATTER_1)) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol){
				if(topwp < getConfig().getPayout(i)){
					topw =i;
					topwp = getConfig().getPayout(i);
				}
			}
		}        

		return topw;
	}

	public double getRoundMultiplier(int combo) {
		// thw multiplier is 3 if the round is freespin.
		double multiplier = 1.0;
		if(this.state == RoundState.FREE_SPIN){
			multiplier = 3.0;
		}
		if((Integer)getConfig().getWinCombo(combo)[1]==5 && (Integer)getConfig().getWinCombo(combo)[0]==-CleopatraConfiguration.WILD ){
			multiplier = 1.0;
		}

		return multiplier;
	}

	public double getPaylineMultiplier(int [] payline, int combo) {
		// if it includes wild the multiplier is 2. 1 otherwise.
		double multiplier = 1.0;
		Integer numOfSymbols = (Integer) this.getConfig().getWinCombo(combo)[1];
		for (int i = 0; i < numOfSymbols; i++) {
			if(payline[i]==CleopatraConfiguration.WILD){
				multiplier = 2.0;
			}
		}
		// special case if the combination is a wilde only combination
		if((Integer)this.getConfig().getWinCombo(combo)[0]==CleopatraConfiguration.WILD){
			multiplier = 1.0;
		}

		return multiplier;
	}

	/**
	 * Checks if the given reel view forms any scatter combinations.
	 * @param reelView The view to check.
	 * @return The combinations the view form.
	 */
	public static final List<Integer> getReelViewWinCombinations(SlotConfiguration config, int[][] reelView,int reelset){

		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			if (symbol >= 0)
				continue;

			symbol = symbol * -1;
			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}

			boolean foundOnLine = false;

			/* Scatter symbol wins */
			for (int j=0;j < config.getNumberOfReels(reelset);j++){
				foundOnLine = false;
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol){
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}        

		return combinations;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	public Double getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}

		return this.getConfig().getPayout(combo);
	}

	public void setState(RoundState p_state) {
		this.state = p_state;
	}

	public double  evaluatePositionWinnings(int reelSet, int[]reelStops) throws ActionFailedException{
		Double[] res = new Double[]{(0.0)};
		evaluatePosition(null, reelSet, 0.0, reelStops, betAmount, maxPayline,res,false);
		return res[0];
	}

}
