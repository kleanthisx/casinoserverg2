package com.magneta.games.slots.playtime;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.PlayTimeConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("PlayTime")
public class PlayTimeRound extends SlotRound {

    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description        Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN, ParameterType.REQUIRED)
    };

    private double betAmount;
    private boolean finished;
    private boolean placedJackpotContrib;
    
    @Override
	public PlayTimeConfiguration getConfig() {
    	return (PlayTimeConfiguration)super.getConfig();
    }
    
    public PlayTimeRound() {
        this.betAmount = 0.0;
        this.finished = false;
    }
    
    protected void startRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        double userRatio = 0.0;
        if (betAmount >= (100 * 0.02)){
            userRatio = 1.0;
        } else if (betAmount >= (60 * 0.02)){
            userRatio = 0.5;
        } else {
            userRatio = 0.1;
        }
        if (claimMysteryJackpot(action, betAmount,userRatio, placedJackpotContrib)){
            this.finished = true;
            return ;
        }

        int[][] roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);

        /* FIXED ODD CODE */
        com.magneta.games.fixedodd.PlayTimeGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reelStops, options.getGameSettings(), action);

      //get the reels view
        int[][] reelsView = calculateReelView(roundReels, reelStops);

        /* rolling the wheels */
        createReelViewAction(action, reelsView);
        
        int[] lineWinCombo = new int[5];
        int[] currLine = new int[5];

        GameAction finishAction = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        
        //Check special case
        boolean bonus = true;
        int[] reqCombos = {20,23,17};
        
        for (int i=0;i<3;i++){
            getPayline(reelsView, i, currLine);
            
            if (getPaylineWinCombination(getConfig(), currLine) != reqCombos[i]){
                bonus = false;
                break;
            }
        }
        
        if (bonus){
            double multiplier = getConfig().getPayout(24);
            double winAmount = multiplier * betAmount;
            GameAction winAction = finishAction.createGeneratedAction(
                    action.getPlayer(), 1, WIN_ACTION, null, winAmount);
            winAction.setActionValue("24");
        } else {
            //check lines
            for (int i=0;i<lineWinCombo.length;i++){
                getPayline(reelsView, i, currLine);
    
                int combo = lineWinCombo[i] = getPaylineWinCombination(getConfig(), currLine);
                if (combo != -1){
                    double multiplier = getConfig().getPayout(combo);
                    double winAmount = multiplier * betAmount;
                    GameAction winAction = finishAction.createGeneratedAction(
                            action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                    winAction.setActionValue(String.valueOf(combo));
                }
            }
        }

        action.commit(true);
        this.finished = true;
    }

    public static int getPaylineWinCombination(SlotConfiguration config, int[] payline){
        Object[][] winCombos = config.getWinCombos();

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            //try from the left 
            int j=0;
            for (;j<payline.length;j++){
                if (payline[j] != symbol){
                    break;
                }
            }

            if (j == quantityRequired){
                return i;
            }

            //try from the right
            j=payline.length-1;
            for (;j>=0;j--){
                if (payline[j] != symbol){
                    break;
                }
            }

            if (((payline.length-1) - j) == quantityRequired){
                return i;
            }
        }        

        return -1;
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        }
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }

        checkBet(action);
        
        action.setRoundStart();
        action.commit();
        betAmount = action.getAmount();
        
        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action); 
        
        startRound(action);
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
        
        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }
}
