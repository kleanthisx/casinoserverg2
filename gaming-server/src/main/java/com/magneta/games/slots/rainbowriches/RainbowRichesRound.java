package com.magneta.games.slots.rainbowriches;

import static com.magneta.games.configuration.slots.RainbowRichesConfiguration.POT_SCATTER;
import static com.magneta.games.configuration.slots.RainbowRichesConfiguration.ROAD_SCATTER;
import static com.magneta.games.configuration.slots.RainbowRichesConfiguration.WELL_SCATTER;
import static com.magneta.games.configuration.slots.RainbowRichesConfiguration.WILD_SYMBOL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.RainbowRichesConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.RainbowRichesGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("RainbowRiches")
public class RainbowRichesRound extends SlotRound {
	
	private static final Logger log = LoggerFactory.getLogger(RainbowRichesRound.class);

	private static final String ROAD_SCATTER_PROMPT = "g:ROAD_PROMPT";
	private static final String POT_SCATTER_PROMPT  = "g:POT_PROMPT";
	private static final String WELL_SCATTER_PROMPT = "g:WELL_PROMPT";

	private static final String START_ROAD_SPIN_ACTION = "g:START_ROAD_SPIN";

	private static final String WELL_SELECT_ACTION = "g:WELL_SELECT";
	private static final String ROAD_SELECT_ACTION = "g:ROAD_SELECT";
	private static final String POT_SELECT_ACTION  = "g:POT_SELECT";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(START_ROAD_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(POT_SELECT_ACTION, ParameterType.REQUIRED, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(ROAD_SELECT_ACTION, ParameterType.OPTIONAL, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(WELL_SELECT_ACTION, ParameterType.REQUIRED, ParameterType.FORBIDDEN),
	};

	private double betAmount;
	private int maxPayline;

	private boolean placedJackpotContrib;

	List<Integer> viewCombos;
	private Integer currentSelectCombo;

	//	private int roadCurrentIndex;
	private Integer currentRoadIndex;
	private Integer roadDestinationIndex;
	private int roadBonusMultiplier;

	private RoundState state;

	private enum RoundState {
		NORMAL,
		WELL_SCATTER_PROMPT,		
		WELL_SCATTER_SELECT,
		ROAD_SCATTER_PROMPT,
		ROAD_SCATTER_SELECT,
		POT_SCATTER_PROMPT,
		POT_SCATTER_SELECT,
		FINISHED
	}

	public RainbowRichesRound(){
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.state = RoundState.NORMAL;
		this.placedJackpotContrib = false;
		this.currentRoadIndex = 0;
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (state==RoundState.FINISHED) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (ROAD_SELECT_ACTION.equals(action.getDescription())) {
			performRoadBonusSelect(action);
		} else if (WELL_SELECT_ACTION.equals(action.getDescription())) {
			performWellSelect(action);
		} else if (POT_SELECT_ACTION.equals(action.getDescription())) {
			performPotSelect(action);
		}
	}

	public int getWeightedRandom(int[][] weights) {
		int sum = 0;

		for (int[] w: weights) {
			sum += w[1];
		}

		int pos = getRandom().nextInt(sum);

		for(int i=0; i < weights.length; i++) {
			pos -= weights[i][1];

			if (pos <= 0) {
				return weights[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}

	private void startWellBonus(GameAction parentAction) throws ActionFailedException {		
		GameAction wellScatterAction = parentAction.createGeneratedAction(null, 0, WELL_SCATTER_PROMPT, null, 0.0);
		wellScatterAction.setActionValue(Integer.toString(currentSelectCombo));
		parentAction.commit();
		this.state = RoundState.WELL_SCATTER_SELECT;
	}

	private void performWellSelect(GameAction action) throws ActionFailedException {
		if (state != RoundState.WELL_SCATTER_SELECT) {
			throw new ActionFailedException("Not in well select mode!");
		}

		try {
			int area = Integer.parseInt(action.getArea());
			if (area < 0 || area > 2)  {
				throw new ActionFailedException("Invalid option number");
			}

		} catch (NumberFormatException e) {
			throw new ActionFailedException("Invalid option number");
		}

		double wellWins = 0.0;
		int multiplier = 0;
		int wellBonusIndex = (Integer)getConfig().getWinCombo(this.currentSelectCombo)[1];

		int[][] wellBonus = RainbowRichesConfiguration.WELL_BONUS_WEIGHTS[wellBonusIndex - 3];

		
		wellBonus = RainbowRichesGuard.getPossibleBonusWeights(wellBonus,betAmount * (this.maxPayline + 1),game.getGameId(), this.options.getOwner(),this.options.getGameSettings());
		
		multiplier = getWeightedRandom(wellBonus);

		action.setActionValue(Integer.toString(multiplier));
		wellWins = multiplier * (betAmount * (this.maxPayline + 1));

		if (wellWins > 0.0) {
			GameAction winAction = action.createGeneratedAction(
					action.getPlayer(), 1, WIN_ACTION,"WELL", wellWins);
			winAction.setActionValue(String.valueOf(this.currentSelectCombo));
			state = RoundState.NORMAL;
		}

		proceedRound(action);
	}

	private void startPotBonus(GameAction parentAction) throws ActionFailedException {		
		
		String potPays = "";
		for (int i = 0; i < RainbowRichesConfiguration.POT_WEIGHTS.length; i++) {
			potPays+= RainbowRichesConfiguration.POT_WEIGHTS[i][0];
			if(i!=RainbowRichesConfiguration.POT_WEIGHTS.length-1){
				potPays+=" ";
			}
		}
		
		GameAction wellScatterAction = parentAction.createGeneratedAction(0, POT_SCATTER_PROMPT, potPays, 0.0);
		wellScatterAction.setActionValue(Integer.toString(currentSelectCombo));
		parentAction.commit();
		this.state = RoundState.POT_SCATTER_SELECT;
	}

	private void performPotSelect(GameAction action) throws ActionFailedException {
		if (state != RoundState.POT_SCATTER_SELECT) {
			throw new ActionFailedException("Not in pot select mode!");
		}

		int area = -1;
		try {
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e) {
			area = -1;
		}

		if ((area < 0) || (area >= getConfig().getNumberOfReels(0) * 3)) {
			throw new ActionFailedException("Invalid area.");
		}

		int[][] potWeights = RainbowRichesConfiguration.POT_WEIGHTS;
		potWeights = RainbowRichesGuard.getPossibleBonusWeights(potWeights,betAmount * (this.maxPayline + 1),game.getGameId(), this.options.getOwner(),this.options.getGameSettings());
		
		double potVAlue = getWeightedRandom(RainbowRichesConfiguration.POT_WEIGHTS); 
		
		double wins = potVAlue * (betAmount * (this.maxPayline + 1));
		action.setActionValue("POT");
		GameAction potAction = action.createGeneratedAction(0, WIN_ACTION, "POT_WIN", wins);
		potAction.setActionValue(Double.toString(wins));
		state = RoundState.NORMAL;
		proceedRound(action);
	}
	
	private int getRoadIndex(int[][] roadBonus, int multiplier) {
		int tmpIndex = 0;
		for (int i = 0; i < roadBonus.length; i++) {
			if (multiplier == roadBonus[i][0]){
				tmpIndex = new Integer(i);
			}
		}
		return tmpIndex;
	}

	private void startRoadBonus(GameAction parentAction) throws ActionFailedException {		
		int roadBonusIndex = (Integer)getConfig().getWinCombo(this.currentSelectCombo)[1];

		int[][] roadBonus = RainbowRichesConfiguration.ROAD_BONUS_WEIGHTS[roadBonusIndex - 3];
		
		roadBonus = RainbowRichesGuard.getPossibleBonusWeights(roadBonus,betAmount * (this.maxPayline + 1),game.getGameId(), this.options.getOwner(),this.options.getGameSettings());
		
		String multipliers = "";
		for (int i = 0; i < roadBonus.length; i++) {
			int[] multiplier = roadBonus[i];
			Integer multi = multiplier[0];
			
			multipliers = multipliers + multi.toString();
			if(i!=roadBonus.length-1){
				multipliers+=" ";
			}
			
		}
		this.roadBonusMultiplier = getWeightedRandom(roadBonus);
		this.roadDestinationIndex = getRoadIndex(roadBonus, roadBonusMultiplier);

		GameAction roadScatterAction = parentAction.createGeneratedAction(null, 0, ROAD_SCATTER_PROMPT, multipliers, 0.0);
		roadScatterAction.setActionValue(Integer.toString(currentSelectCombo));
		parentAction.commit();
		this.state = RoundState.ROAD_SCATTER_SELECT;
	}

	private void performRoadBonusSelect(GameAction action) throws ActionFailedException {
		if (this.state != RoundState.ROAD_SCATTER_SELECT) {
			throw new ActionFailedException("Not in Road bonus mode");
		}

		int spin = 0;
		boolean atDestination = false;
		// if it's allready there then this holds true
		if(currentRoadIndex >= this.roadDestinationIndex){
			atDestination = true;
		} else {
			if (action.isLoaded()) {
				spin = Integer.valueOf(action.getActionValue());
			} else {
				spin = new Random().nextInt(6);
				spin = spin+1;
			}
			//adding steps
			int tmpIndex = this.currentRoadIndex + spin;

			//making sure we din't overstep
			if (tmpIndex > this.roadDestinationIndex && !atDestination) {
				spin = roadDestinationIndex - currentRoadIndex; 
			}

			this.currentRoadIndex = currentRoadIndex + spin;
		}
		if(atDestination){
			action.setActionValue("0");

			double winAmount = roadBonusMultiplier * (betAmount * (maxPayline+1));

			GameAction winAction = action.createGeneratedAction(
					action.getPlayer(), 1, WIN_ACTION, null, winAmount);

			winAction.setActionValue(String.valueOf(this.currentSelectCombo));
			state = RoundState.NORMAL;
		} else {
			action.setActionValue(Integer.toString(spin));
		}

		proceedRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)){
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void proceedRound(GameAction action) throws ActionFailedException {

		if (this.state == RoundState.NORMAL) {
			if (!viewCombos.isEmpty()) {
				currentSelectCombo = viewCombos.remove(0);
				roadBonusMultiplier = 0;
				roadDestinationIndex = null;

				Object[] winCombo = getConfig().getWinCombo(currentSelectCombo);
				Integer symbol = (Integer)winCombo[0];

				if (symbol < 0) {
					symbol = symbol * -1;
				}
				switch (symbol) {
				case RainbowRichesConfiguration.WELL_SCATTER:
					startWellBonus(action);
					break;
				case RainbowRichesConfiguration.ROAD_SCATTER:
					startRoadBonus(action);
					break;
				case RainbowRichesConfiguration.POT_SCATTER:
					startPotBonus(action);
					break;
				default:
					throw new RuntimeException("Unknown bonus");
				}
			} else {
				endRound(action);
			}
		} else {
			action.commit();
		}
	}

	private void startRound(GameAction action) throws ActionFailedException {
		int reelSet = 0;

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			state = RoundState.FINISHED;
			return;
		}

		int[][] weights = getWeights();

		reelSet = getWeightedRandom(weights); 

		int[] rellsetChosen = new int[]{reelSet};
		int[] reelStops = spinReels(getConfig().getReels(reelSet));
		log.debug("prin : "+Arrays.toString(reelStops));
		reelStops = RainbowRichesGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, rellsetChosen,reelStops, reelSet, options.getGameSettings(), action, this);
		reelSet = rellsetChosen[0];
		log.debug("meta : "+Arrays.toString(reelStops));
		evaluatePosition(action, reelSet, jackpotRatio, reelStops,betAmount,maxPayline,new Double[]{(0.0)},true);

		proceedRound(action);
	}
	
	public int[][] getWeights(){
		int[][] weights = new int[][]{ 
				{0, getConfig().getSetting(RainbowRichesConfiguration.REEL_WEIGHT_0, Integer.class) },
				{1, getConfig().getSetting(RainbowRichesConfiguration.REEL_WEIGHT_1, Integer.class) },
				{2, getConfig().getSetting(RainbowRichesConfiguration.REEL_WEIGHT_2, Integer.class) },
				{3, getConfig().getSetting(RainbowRichesConfiguration.REEL_WEIGHT_3, Integer.class) },
		};
		return weights;
	}

	public List<Integer> evaluatePosition(GameAction action, int reelSet,
			double jackpotRatio, int[] reelStops, double betAmount2,
			int maxPayline2, Double[] actwinsum,boolean isProduction) throws ActionFailedException {

		int[][] reelsView = calculateReelView(getConfig().getReels(reelSet), reelStops);

		if(isProduction){
		createReelViewAction(action, reelsView);
		action.commit();
		}

		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		for (int i=0;i <= maxPayline2;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine);

			if (combo < 0)
				continue;
			double multiplier = getRoundMultiplier() * getPaylineMultiplier(currLine) * getComboPayout(combo);

			double winAmount = multiplier * betAmount2;
			if(isProduction){
			if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
					&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
				//stop the game from retrying to claim jackpot on other line
				placedJackpotContrib = false;
			} else {
				if (winAmount > 0.0) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
					GameAction winAction = action.createGeneratedAction(
							action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
					winAction.setActionValue(String.valueOf(combo));
					winAction.commit();
				} 
			}
			}else{
				actwinsum[0] = new Double(actwinsum[0] + winAmount);
			}
		}

		viewCombos = getReelViewWinCombinations(getConfig(), reelsView,reelSet);

		for (Integer viewCombo: viewCombos) {

			if (viewCombo >= 0) {
				double multiplier = getComboPayout(viewCombo);

				double winAmount = multiplier * (betAmount * (this.maxPayline + 1));

				if (winAmount > 0.01) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
					if(isProduction){
					GameAction winAction = action.createGeneratedAction(
							action.getPlayer(), 1, WIN_ACTION, null, winAmount);
					winAction.setActionValue(String.valueOf(viewCombo));
					}
				}
			}
		}
		
		return viewCombos;

	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (state == RoundState.FINISHED) {
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.state = RoundState.FINISHED;
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline) {
		Object[][] winCombos = getConfig().getWinCombos();

		int topw = -1;
		double topwp = 0.0;
		
		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if (getComboPayout(i) < 0.01) {
				continue;
			}

			int j;

			boolean foundSymbol = false;
			for (j=0;j<payline.length;j++){
				if (payline[j] == symbol){
					foundSymbol = true;
				} else if ((payline[j] == WILD_SYMBOL) && !(symbol == WELL_SCATTER || symbol == ROAD_SCATTER || symbol == POT_SCATTER)) {
					continue;
				} else {
					break;
				}
			}

			if ((j == quantityRequired) && foundSymbol){
				if(topwp<getConfig().getPayout(i)){
					topw =i;
					topwp = getConfig().getPayout(i);
				}
			}
		}        

		return topw;
	}

	public double getRoundMultiplier() {
		return 1.0;
	}

	public double getPaylineMultiplier(int [] payline) {
		return 1.0;
	}

	/**
	 * Checks if the given reel view forms any scatter combinations.
	 * @param reelView The view to check.
	 * @return The combinations the view form.
	 */
	public static final List<Integer> getReelViewWinCombinations(SlotConfiguration config, int[][] reelView,int reelset){

		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			if (symbol >= 0)
				continue;

			symbol = symbol * -1;
			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}
			boolean foundOnLine = false;

			/* Scatter symbol wins */
			for (int j=0;j < config.getNumberOfReels(reelset);j++){
				foundOnLine = false;
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol){
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}        

		return combinations;
	}

	@Override
	public boolean isFinished() {
		return state == RoundState.FINISHED;
	}

	public Double getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}

		return this.getConfig().getPayout(combo);
	}

	@Override
	public RainbowRichesConfiguration getConfig() {
    	return (RainbowRichesConfiguration)super.getConfig();
    }
	
	public double  evaluatePositionWinnings(int reelSet, int[]reelStops) throws ActionFailedException{
		Double[] res = new Double[]{(0.0)};
		evaluatePosition(null, reelSet, 0.0, reelStops, betAmount, maxPayline,res, false);
		return res[0];
	}
}
