package com.magneta.games.slots.bookofra;

import static com.magneta.games.configuration.slots.BookOfRaConfiguration.FREE_SPIN_SCATTER_WILD;
import static com.magneta.games.configuration.slots.BookOfRaConfiguration.SYMBOL_WEIGHTS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.BookOfRaConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.fixedodd.BookOfRaGuard;
import com.magneta.games.slots.SlotRound;

@GameTemplate("BookOfRa")
public class BookOfRaRound extends SlotRound {

	private static final int MAX_TOTAL_FREE_SPINS = 30;
	private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	
	private static final Logger log = LoggerFactory.getLogger(BookOfRaRound.class);

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*                              Description             Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
		new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
	};

	private int freeSpinsRemaining;
	private int freeSpinsWon;
	private boolean finished;
	private double betAmount;
	private int maxPayline;
	private int specialSymbol;

	private boolean placedJackpotContrib;

	List<Integer> viewCombos;

	private RoundState state;

	public enum RoundState {
		NORMAL,
		FREE_SPIN
	}

	public BookOfRaRound() {
		this.betAmount = 0.0;
		this.maxPayline = -1;
		this.freeSpinsRemaining = 0;
		this.freeSpinsWon = 0;
		this.finished = false;
		this.state = RoundState.NORMAL;
		this.placedJackpotContrib = false;
		this.specialSymbol = -1;
	}

	@Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		BookOfRaRound parent = (BookOfRaRound)parentRound;
		this.freeSpinsRemaining = parent.freeSpinsRemaining;
		if (this.freeSpinsRemaining > 0) {
			this.freeSpinsWon = parent.freeSpinsWon;
			this.betAmount = parent.betAmount;
			this.maxPayline = parent.maxPayline;
			this.specialSymbol = parent.specialSymbol;

			GameAction freeSpinAction = new GameAction(null, this, 0,
					FREE_SPIN_ACTION, maxPayline + " " + betAmount+" "+Integer.toString(specialSymbol)+" "+Integer.toString(this.freeSpinsWon), 0.0);
			freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
			freeSpinAction.setRoundStart();

			freeSpinAction.commit();
			actionCompleted(freeSpinAction);
			this.state = RoundState.FREE_SPIN;
		}
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {

		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}

	@Override
	public BookOfRaConfiguration getConfig() {
		return (BookOfRaConfiguration)super.getConfig();
	}

	@Override
	protected void loadAction(GameAction action) throws ActionFailedException {
		if (FREE_SPIN_ACTION.equals(action.getDescription())){
			try{
				this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
				this.state = RoundState.FREE_SPIN;
				String[] tokens = action.getArea().split(" ");

				if (tokens.length == 4) {
					this.maxPayline = Integer.parseInt(tokens[0]);
					this.betAmount = Double.parseDouble(tokens[1]);
					this.specialSymbol = Integer.parseInt(tokens[2]);
					this.freeSpinsWon = Integer.parseInt(tokens[3]);
				} else {
					throw new ActionFailedException("Free spin load failed. Values missing.");
				}
			} catch (NumberFormatException e){
				throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
			}

			actionCompleted(action);

		} else {
			doAction(action);
		}
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (START_FREE_SPIN_ACTION.equals(action.getDescription())) {
			performFreeSpin(action);
		}
	}

	private int getfreespins(int symbol) {
		switch (symbol) {
		case FREE_SPIN_SCATTER_WILD:
			return 10;
		default:
			throw new RuntimeException("Invalid Free spin symbol : "+ symbol);
		}
	}

	private void performFreeSpin(GameAction action) throws ActionFailedException {
		if (this.state!=RoundState.FREE_SPIN){
			throw new ActionFailedException("Not in free spin mode!");
		}

		if (this.state == RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		action.commit();
		this.freeSpinsRemaining--;
		startRound(action);
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		if ( this.state == RoundState.FREE_SPIN) {
			throw new ActionFailedException("Round is a free spin; No bets allowed.");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		if (this.state != RoundState.NORMAL) {
			throw new ActionFailedException("Round has spinned already");
		}

		checkBet(action);

		int area = -1;

		try{
			area = Integer.parseInt(action.getArea());
		} catch (NumberFormatException e){
		}

		if ((area < 0) || (area >= getConfig().getPaylines().length)){
			throw new ActionFailedException("Invalid action area");
		}

		double betUnit;
		double totalBet;

		if (action.isLoaded()) {
			totalBet = action.getAmount();
			betUnit = action.getAmount() / (area+1);
		} else {
			betUnit = action.getAmount();
			totalBet = betUnit * (area+1);
			action.setAmount(totalBet);
		}

		action.setRoundStart();
		action.commit();

		betAmount = betUnit;
		maxPayline = area;

		placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	private void startRound(GameAction action) throws ActionFailedException {

		if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)) {
			throw new IllegalStateException("No free spins remain. Round must be started normally.");
		} else if (finished){
			throw new IllegalStateException("Method startRound() already called once.");
		}

		double jackpotRatio = (betAmount / options.getMaxBet());
		if (jackpotRatio > 1.0){
			jackpotRatio = 1.0;
		}

		if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
			this.finished = true;
			return;
		}

		int reelSet = 0;

		int[][] weights = getWeights();

		if(this.state != RoundState.FREE_SPIN){
			reelSet = getReelBasedOnWeights(weights,getRandom());
		}else{
			reelSet = 4;
		}


		int[] reelStops = spinReels(getConfig().getReels(reelSet));
		log.debug("prin : "+Arrays.toString(reelStops));
		int[] rellsetChosen = new int[]{reelSet};
		reelStops = BookOfRaGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reelSet, reelStops, options.getGameSettings(), action, this, maxPayline,rellsetChosen,getGuardRandom());
		reelSet = rellsetChosen[0];
		log.debug("meta : "+Arrays.toString(reelStops));
		
		evaluatePosition(action, reelSet, jackpotRatio, reelStops, betAmount, maxPayline , this.specialSymbol,new Double[]{(0.0)},true);

		endRound(action);

	}
	
	public int[][] getWeights() {
		int[][] weights = { 
				{0, getConfig().getSetting(BookOfRaConfiguration.REEL_WEIGHT_0, Integer.class) },
				{1, getConfig().getSetting(BookOfRaConfiguration.REEL_WEIGHT_1, Integer.class) },
				{2, getConfig().getSetting(BookOfRaConfiguration.REEL_WEIGHT_2, Integer.class) },
				{3, getConfig().getSetting(BookOfRaConfiguration.REEL_WEIGHT_3, Integer.class) },
		};
		return weights;
	}

	public List<Integer> evaluatePosition(GameAction action, int reelSet,
			double jackpotRatio, int[] reelStops, double betAmount2,
			int maxPayline2,int specialStmbol_param , Double[] actwinsum, boolean isProduction) throws ActionFailedException {

		int[][] reelsView = calculateReelView(getConfig().getReels(reelSet), reelStops);

		if(isProduction){
			createReelViewAction(action, reelsView);
			action.commit();
		}

		int[] currLine = new int[getConfig().getNumberOfReels(0)];

		for (int i=0;i <= maxPayline2;i++){
			getPayline(reelsView, i, currLine);

			int combo = getPaylineWinCombination(currLine,specialStmbol_param);

			// this is to skip evaluation of the special symbol rules.

			if (combo < 0)
				continue;

			if((Integer)(getConfig().getWinCombo(combo)[0]) == specialStmbol_param && this.state == RoundState.FREE_SPIN){
				combo = -1;
			}

			if (combo < 0)
				continue;

			double multiplier = getComboPayout(combo);
			log.debug("multiplier = "+ multiplier + " for line "+ (i+1));
			double winAmount = multiplier * betAmount2;
			if(isProduction){
				if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
						&& JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
					//stop the game from retrying to claim jackpot on other line
					placedJackpotContrib = false;
				} else {
					if (winAmount > 0.0) {
						actwinsum[0] = new Double(actwinsum[0] + winAmount);
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
						winAction.setActionValue(String.valueOf(combo));
						winAction.commit();
					} 
				}
			}else{
				if (winAmount > 0.0) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
				} 
			}
		}

		viewCombos = getReelViewWinCombinations(getConfig(), reelsView,reelSet,specialStmbol_param);

		for (Integer viewCombo: viewCombos) {
			if (viewCombo >= 0) {
				double multiplier = getComboPayout(viewCombo);

				double winAmount = multiplier * (betAmount2 * (maxPayline2+1));

				int symbol = (Integer)getConfig().getWinCombo(viewCombo)[0];

				if (winAmount > 0.01) {
					actwinsum[0] = new Double(actwinsum[0] + winAmount);
					if(isProduction){
						GameAction winAction = action.createGeneratedAction(
								action.getPlayer(), 1, WIN_ACTION, null, winAmount);
						winAction.setActionValue(String.valueOf(viewCombo));
						action.commit();
					}
				}
				if(isProduction){
					symbol = -symbol;
					if(symbol==BookOfRaConfiguration.FREE_SPIN_SCATTER_WILD && (Integer)getConfig().getWinCombo(viewCombo)[1]>2){
						int freeSpins = getfreespins(symbol);
						if (this.specialSymbol == -1){
							this.specialSymbol = getWeightedRandom(SYMBOL_WEIGHTS);
						}

						int totalFreeSpins = Math.min(this.freeSpinsWon + freeSpins, MAX_TOTAL_FREE_SPINS);
						freeSpins = totalFreeSpins - this.freeSpinsWon;
						if (freeSpins < 0) {
							freeSpins = 0;
						}
						
						if(freeSpins > 0) {
							action.setActionValue("FREE " + freeSpins);
							GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
							freeSpinsAction.setActionValue(Integer.toString(freeSpins));
							action.commit();
							this.freeSpinsRemaining += freeSpins;
							this.freeSpinsWon += freeSpins;
						}
					}
				}
			}
		}

		return viewCombos;

	}

	private void endRound(GameAction action) throws ActionFailedException {
		if (finished){
			throw new IllegalStateException("Method endRound() already called once.");
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
		this.finished = true;
	}

	private int getWeightedRandom(int[][] weights) {
		int sum = 0;

		for (int[] w: weights) {
			sum += w[1];
		}

		int pos = getRandom().nextInt(sum);

		for(int i=0; i < weights.length; i++) {
			pos -= weights[i][1];

			if (pos <= 0) {
				return weights[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}

	/**
	 * Checks if the given payline forms any winning combinations.
	 * @param payline The payline to check.
	 * @return The combination the payline forms or -1 if it doesn't form any.
	 */
	public int getPaylineWinCombination(int[] payline,int specialStmbol_param) {
		Object[][] winCombos = getConfig().getWinCombos();

		int topw = -1;
		double topwp = 0.0;

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantityRequired = (Integer)winCombos[i][1];

			if (getComboPayout(i) < 0.01) {
				continue;
			}

			if (symbol< 0) {
				continue;
			}

			int j;
			for (j=0;j<payline.length;j++){
				if (payline[j] == symbol||(payline[j] ==FREE_SPIN_SCATTER_WILD && symbol!=specialStmbol_param)) {
					continue;
				}
				break;
			}

			if (j == quantityRequired){
				if(topwp < getConfig().getPayout(i)){
					topw =i;
					topwp = getConfig().getPayout(i);
				}
			}
		}        

		return topw;
	}

	/**
	 * Checks if the given reel view forms any scatter combinations.
	 * @param reelView The view to check.
	 * @param specialStmbol_param 
	 * @return The combinations the view form.
	 */
	public static final List<Integer> getReelViewWinCombinations(SlotConfiguration config, int[][] reelView,int reelset, int specialStmbol_param){

		List<Integer> combinations = new ArrayList<Integer>();
		List<Integer> comboSymbols = new ArrayList<Integer>();

		Object[][] winCombos = config.getWinCombos();

		for (int i=winCombos.length-1;i>=0;i--){
			int symbol = (Integer)winCombos[i][0];
			int quantity = (Integer)winCombos[i][1];

			// this is to allow the symbol to be treated as special scatter symbol.
			if (symbol == specialStmbol_param )
				symbol = symbol * -1;

			if (symbol >= 0 )
				continue;

			symbol = symbol * -1;
			/* A combo with the symbol was already found. skip */
			if (comboSymbols.contains(new Integer(symbol))) {
				continue;
			}

			boolean foundOnLine = false;

			/* Scatter symbol wins */
			for (int j=0;j < config.getNumberOfReels(reelset);j++){
				foundOnLine = false;
				for (int k=0;k<3;k++){
					if (reelView[k][j] == symbol){
						foundOnLine = true;
						break;
					}
				}

				if (foundOnLine) {
					quantity--;
				}

				if (quantity <= 0) {
					comboSymbols.add(symbol);
					combinations.add(i);
					break;
				}
			}

		}        

		return combinations;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	public Double getComboPayout(int combo){
		if (combo < 0) {
			return 0.0;
		}

		return this.getConfig().getPayout(combo);
	}

	public void setState(RoundState p_state) {
		this.state = p_state;
	}

	public int getReelBasedOnWeights(int[][] freeSpin, Random random) {
		int sum = 0;

		for (int[] w: freeSpin) {
			sum += w[1];
		}

		int pos = random.nextInt(sum);

		for(int i=0; i < freeSpin.length; i++) {
			pos -= freeSpin[i][1];

			if (pos <= 0) {
				return freeSpin[i][0];
			}
		}

		throw new RuntimeException("Out of bounds!");
	}
	
	public double  evaluatePositionWinnings(int reelSet, int[]reelStops) throws ActionFailedException{
		Double[] res = new Double[]{(0.0)};
		evaluatePosition(null, reelSet, 0, reelStops, this.betAmount, this.maxPayline , this.specialSymbol,res,false);
		return res[0];
	}

}
