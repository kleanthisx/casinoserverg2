package com.magneta.games.slots.darktombs;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import com.magneta.Config;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionFilter;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.DarkTombsConfiguration;
import com.magneta.games.slots.BonusBox;
import com.magneta.games.slots.SlotRound;
import com.magneta.games.slots.WinCombination;

@GameTemplate("DarkTombs")
public class DarkTombsRound extends SlotRound {

	private static class FreeSpinSet {
		public int count;
		public int multiplier;
		
		public FreeSpinSet(int count, int multiplier) {
			this.count = count;
			this.multiplier = multiplier;
		}
		
		public FreeSpinSet(int count) {
			this.count = count;
		}
		
	}
	
	static final String FREE_SPIN_ACTION_CONT = "g:FREE_SPIN_CONT";
    private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
    private static final String TREASURE_BONUS_ACTION = "g:TREASURE_BONUS";
    private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";



    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description             Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
        new GameActionRequestDefinition(BOX_SELECT_ACTION,      ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
        new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
    };

    private static final int NORMAL_MODE = 0;
    private static final int BONUS_MODE = 1;

    private static final int MAX_BONUS_BOXES = 30;
    /*This value cannot be greater than MAX_BONUS_BOXES / 2*/
    private static final int BOXES_WITH_AMOUNT = 11;
    private static final int WILD_SYMBOL = DarkTombsConfiguration.WILD_SYMBOL;
    private static final int BONUS_SYMBOL = DarkTombsConfiguration.BONUS_SYMBOL;
    private static final int SCATTER_SYMBOL = DarkTombsConfiguration.SCATTER_SYMBOL;
    
    private static final int ACTION_DESC_SIZE = 100;

	private final CurrencyParser currencyParser;
    
    private Deque<FreeSpinSet> freeSpinStack;
    private double freeSpinMultiplier;
    
    private int gameMode;
    private boolean finished;
    private double betAmount;
    private int maxPayline;

    private BonusBox[] bonusBoxes;
    private int bonusBoxesOpened;
    private int bonusWithNoAmountFound;

    private boolean freeSpin;
    private boolean placedJackpotContrib;
    
    public DarkTombsRound(CurrencyParser currencyParser) {
    	this.currencyParser = currencyParser;
    	
        this.betAmount = 0.0;
        this.maxPayline = -1;
        this.gameMode = NORMAL_MODE;
        this.finished = false;
        this.freeSpin = false;
        this.placedJackpotContrib = false;
        this.bonusBoxes = null;
        
        this.freeSpinStack = new ArrayDeque<FreeSpinSet>();
        this.freeSpinMultiplier = 1.0;
    }    

    @Override
	public DarkTombsConfiguration getConfig() {
    	return (DarkTombsConfiguration)super.getConfig();
    }
    
	@Override
	public void roundInit(AbstractGameRound parentRound) throws ActionFailedException {
		DarkTombsRound parent = (DarkTombsRound)parentRound;
		
        this.freeSpinStack = parent.freeSpinStack;
        
        if (!this.freeSpinStack.isEmpty()) {
            this.betAmount = parent.betAmount;
            this.maxPayline = parent.maxPayline;
            
            FreeSpinSet currFreeSpin = this.freeSpinStack.peek();
            this.freeSpinMultiplier = currFreeSpin.multiplier;
            
            GameAction freeSpinAction = null;
            
            List<String> freeSpinValues = createFreeSpinActionValues();

            for (String s: freeSpinValues) {
            	if (freeSpinAction == null) {
            		freeSpinAction = new GameAction(null, this, 0, FREE_SPIN_ACTION, maxPayline+" "+betAmount, 0.0);
                    freeSpinAction.setActionValue(s);
                    freeSpinAction.setRoundStart();
                    
            	} else {
            		GameAction freeSpinCont = new GameAction(null, this, 0, FREE_SPIN_ACTION_CONT, null, 0.0); 
            		freeSpinCont.setActionValue(s);
            		freeSpinAction.addAdditionalAction(freeSpinCont);
            	}
            }

            if (freeSpinAction != null) {
            	freeSpinAction.commit();
            	actionCompleted(freeSpinAction);
            	this.freeSpin = true;
            } else {
            	throw new ActionFailedException("Free spin round information missing.");
            }
        }
    }

    private List<String> createFreeSpinActionValues() {
    	
    	List<String> values = new ArrayList<String>();
    	StringBuilder currBuilder = new StringBuilder();
    	
    	for (FreeSpinSet s: freeSpinStack) {
    		String value = String.format("%d %d", s.count, s.multiplier);
    		
    		if (currBuilder.length() + value.length() >= ACTION_DESC_SIZE) {
    			values.add(currBuilder.toString());
    			currBuilder = new StringBuilder();
    		} else {
    			if (currBuilder.length() > 1) {
    				currBuilder.append(' ');
    			}
    		}

    		currBuilder.append(value);
    	}
    	
    	if (currBuilder.length() > 0) {
    		values.add(currBuilder.toString());
    	}
    	
    	return values;
    }
    
    /**
     * Pushes a freeSpinSet to the free spin stack merging it with
     * the top element if they have the same multiplier
     * 
     * @param set
     */
    private void pushFreeSpinToStack(FreeSpinSet set) {
    	FreeSpinSet last = this.freeSpinStack.peek();
    	
    	if (last != null && last.multiplier == set.multiplier) {
			last.count += set.count;
		} else {
			this.freeSpinStack.push(set);
		}
    }
    
    /**
     * Adds a free spin / free spin continuation action to the end of the freeSpinStack.
     * @param actionValue
     */
    private void addFreeSpinToStack(String actionValue) {
    	String[] numbers = actionValue.split(" ");
    	
    	FreeSpinSet currSet = null;
    	
    	for (String n: numbers) {
    		if (currSet == null) {
    			currSet = new FreeSpinSet(Integer.valueOf(n));
    		} else {
    			currSet.multiplier = Integer.valueOf(n);
    			freeSpinStack.add(currSet);
    			currSet = null;
    		}
    	}
    }
    
    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {

        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    @Override
    protected void loadAction(GameAction action) throws ActionFailedException {
        if (FREE_SPIN_ACTION.equals(action.getDescription())){
            try{
            	addFreeSpinToStack(action.getActionValue());
                this.freeSpin = true;
                String[] tokens = action.getArea().split(" ");

                if (tokens.length == 2){
                    this.maxPayline = Integer.parseInt(tokens[0]);
                    this.betAmount = Double.parseDouble(tokens[1]);
                } else {
                    throw new ActionFailedException("Free spin load failed. Values missing.");
                }
            } catch (NumberFormatException e){
                throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
            }

            actionCompleted(action);
        } else if (FREE_SPIN_ACTION_CONT.equals(action.getDescription())) {
        	try{
        		addFreeSpinToStack(action.getActionValue());
        	} catch (NumberFormatException e){
                throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
            }	
        } else {
            doAction(action);
        }
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        } else if (BOX_SELECT_ACTION.equals(action.getDescription())){
            performBoxSelect(action);
        } else if (START_FREE_SPIN_ACTION.equals(action.getDescription())){
            performFreeSpin(action);
        }
    }
    
    private static final class BoxAmountClass {
    	double amount;
    	int count;
    }
    
    private double calculateAmountWin() {
    	int step = 0;
    	BoxAmountClass[] winsArray = new BoxAmountClass[BOXES_WITH_AMOUNT];
        for(int i=0;i<MAX_BONUS_BOXES;i++) {
        	double amount = bonusBoxes[i].getPayout();
        	if(amount > 0.001 && bonusBoxes[i].isOpened()) {
            	boolean found = false;
            	int z;
	        	for(z=0;z<winsArray.length;z++) {
	        		if(winsArray[z] == null) {
	        			continue;
	        		}
	        		if(amount >= winsArray[z].amount - 0.001 && amount <= winsArray[z].amount + 0.001) {
	        			found = true;
	        			break;
	        		}
	        	}
	        	
	        	if(!found) {
	        		winsArray[step] = new BoxAmountClass();
	        		winsArray[step].amount = amount;
	        		z = step;
	        		step++;
	        	}
	    		winsArray[z].count++;
        	}
        }
        double finalWinAmount = 0.0;
        for(int i=0;i<BOXES_WITH_AMOUNT;i++) {
    		if(winsArray[i] == null) {
    			continue;
    		}   	
    		if(winsArray[i].count >= 2) {
    			finalWinAmount += (winsArray[i].amount) * (winsArray[i].count / 2);
    		}
        }
        return finalWinAmount;
    }

    private void performBoxSelect(GameAction action) throws ActionFailedException{
        if (gameMode != BONUS_MODE){
            throw new ActionFailedException("Not in treasure bonus mode!");
        }
        
        int area = -1;
        try {
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e){
            area = -1;
        }

        if (area < 0 || area >= bonusBoxes.length) {
            throw new ActionFailedException("Invalid area.");
        }

        if (bonusBoxes[area].isOpened()){
            throw new ActionFailedException("Box already opened!");
        }
        double winAmount = bonusBoxes[area].getPayout();
        bonusBoxes[area].setOpened(true);
        action.setActionValue(Double.toString(winAmount));
        this.bonusBoxesOpened++;
        
        if(winAmount <= 0.001) {
        	this.bonusWithNoAmountFound++;
        }
        
        /*Bonus finished*/
        if(this.bonusBoxesOpened  == MAX_BONUS_BOXES || bonusWithNoAmountFound >= 2) {
        	double allWins = calculateAmountWin();
	        action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, allWins);
            endRound(action);
        }else{
            action.commit();
        }
    }

    /** 
     * Shuffles a bonus box row
     * @param bonusRow
     */
    private void shuffleBonusRow(BonusBox[] bonusBoxes) {
        BonusBox tmp;

        for (int i=0;i < bonusBoxes.length;i++){
            int swap = getRandom().nextInt(bonusBoxes.length);
            tmp = bonusBoxes[swap];
            bonusBoxes[swap] = bonusBoxes[i];
            bonusBoxes[i] = tmp;
        }
    }


    private void initBoxes(List<Integer> queuedBonuses) {
        double totalPayout = 0.0;
        int boxesRate = 0;
        int tmpRates = 0;
        int i;
        
        /* Calculate total payout for the bonus */
        for (Integer queuedBonus : queuedBonuses) {
            totalPayout += getComboPayout(queuedBonus);
        }
    
        boxesRate = (int)totalPayout;
        tmpRates = boxesRate;
        /*Scatter combination Calculate on total bet*/
        totalPayout *= this.betAmount * (this.maxPayline + 1);
        
        this.bonusBoxes = new BonusBox[MAX_BONUS_BOXES];
        for(i=0;i<MAX_BONUS_BOXES;i++) {
        	this.bonusBoxes[i] = new BonusBox();
        }
                
        int rates[] = new int[BOXES_WITH_AMOUNT];
        double amounts[] = new double[BOXES_WITH_AMOUNT];
        
        /*Allocate to all the boxes 1*/
        for (i=0;i<BOXES_WITH_AMOUNT && tmpRates > 0;i++) {
        	rates[i] = 1;
        	tmpRates--;
        }
        
        /*Calcualate Rates*/
        for(i=0;i < tmpRates;i++) {
			int num = this.getRandom().nextInt(BOXES_WITH_AMOUNT);
			rates[num]++;
        }
        
        /*Allocate amount depending BOXES_WITH_AMOUNT and the rates*/
		for(i=0;i< BOXES_WITH_AMOUNT;i++) {
			amounts[i] = ((double)rates[i] / (double)boxesRate) *  totalPayout;
			amounts[i] = currencyParser.normalizeDouble(amounts[i]);
		}
        
        for(i=0;i<BOXES_WITH_AMOUNT;i++) {
        	bonusBoxes[i * 2].setPayout(amounts[i]);
        	bonusBoxes[(i * 2) + 1].setPayout(amounts[i]);
        }
        shuffleBonusRow(bonusBoxes);
                
        /* Log bonus boxes */
        if (Config.getBoolean("games.log.bonus", false)) { 
        	StringBuilder builder = new StringBuilder();
        	for(i=0;i<MAX_BONUS_BOXES;i++) {
        		builder.append(Double.toString(this.bonusBoxes[i].getPayout())).append(' ');
        	}
    		builder.append('\n');
        	logInfo("Bonus boxes:\n" + builder.toString());
        }
    }

    private void performFreeSpin(GameAction action) throws ActionFailedException {
        if (!freeSpin){
            throw new ActionFailedException("Not in free spin mode!");
        } else if (gameMode != NORMAL_MODE) {
            throw new ActionFailedException("Spin not allowed at this time.");
        }

        action.commit();
        
        
        FreeSpinSet currFreeSpin = this.freeSpinStack.peek();
        currFreeSpin.count--;
        
        if (currFreeSpin.count < 1) {
        	this.freeSpinStack.pop();
        }
        
        startRound(action);
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (freeSpin) {
            throw new ActionFailedException("Round is a free spin; No bets allowed.");
        } else if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        } else if (gameMode != NORMAL_MODE){
            throw new ActionFailedException("Bet not allowed at this time.");
        } 

        checkBet(action);

        int area = -1;

        try{
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e){
        }

        if ((area < 0) || (area >= getConfig().getPaylines().length)){
            throw new ActionFailedException("Invalid action area");
        }

        double betUnit;
        double totalBet;
        
        if (action.isLoaded()) {
            totalBet = action.getAmount();
            betUnit = action.getAmount() / (area+1);
        } else {
            betUnit = action.getAmount();
            totalBet = betUnit * (area+1);
            action.setAmount(totalBet);
        }

        action.setRoundStart();
        action.commit();

        betAmount = betUnit;
        maxPayline = area;

        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    private void startRound(GameAction action) throws ActionFailedException {
    	if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }
    	
        if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && !freeSpin) {
            this.maxPayline = -1;
            this.betAmount = 0.0;
            throw new IllegalStateException("No free spins remain. Round must be started normally.");
        }

        logDebug("Starting round...");

        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)) {
            this.finished = true;
            return;
        }

        int[][] reels = getConfig().getReels(freeSpin ? 1: 0);
        int[] reelStops = spinReels(reels);
        /*
        StringBuilder rS = new StringBuilder("Reel Stops:");
        for (int reelStop: reelStops) {
            rS.append(" ");
            rS.append(reelStop);
        }
        Log.info(rS.toString());
        */
        int[][] reelsView = calculateReelView(reels, reelStops);

        /*FIXED ODDS CODE*/
        //com.magneta.games.fixedodd.PharaohGuard.guardRound(game.getId(), options.getOwner(), freeSpin, roundReels, WIN_CONSTRAINTS, roundPayouts, WILD_SYMBOL, FREE_SPIN_MULTIPLIER, betAmount, maxPayline, reelStops, options.getGameSettings(), action);

        createReelViewAction(action, reelsView);


        //int[] lineWinCombo = new int[maxPayline+1];
        int[] currLine = new int[reels.length];
        List<Integer> queuedBonuses = new ArrayList<Integer>();

        int freeSpinSymbolsFound = 0;

        for (int i=0;i <= maxPayline;i++){
            getPayline(reelsView, i, currLine);

            int combo = getPaylineWinCombination(currLine);
           

            if (combo < 0)
                continue;

            double multiplier = getComboPayout(combo);
            double winAmount = multiplier * betAmount  * (freeSpin? this.freeSpinMultiplier : 1.0);

            if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)
                    && JackpotsHandler.claimGameJackpot(this, action, i,
                            winAmount, jackpotRatio)) {
                // stop the game from retrying to claim jackpot on other line
                placedJackpotContrib = false;
            } else {
                if (winAmount > 0.0) {
                    GameAction winAction = action.createGeneratedAction(
                            action.getPlayer(), 1, WIN_ACTION,
                            String.valueOf(i), winAmount);
                    winAction.setActionValue(String.valueOf(combo));
                }
            }
        }

        List<WinCombination> specialCombos = getReelViewWinCombination(reelsView);
        if (specialCombos != null) {
            for (WinCombination c: specialCombos) {
                int combo = c.getCombination();
                Object[] winCombo = getConfig().getWinCombo(combo);
                Integer symbol = (Integer)winCombo[0];
               
                if (symbol.equals(-BONUS_SYMBOL)) {
                    queuedBonuses.add(new Integer(combo));
                }else if (symbol.equals(-SCATTER_SYMBOL)) {
                	freeSpinSymbolsFound += (Integer)getConfig().getWinCombo(combo)[1];
                }
            }
        }

        if (!queuedBonuses.isEmpty()) {
            action.createGeneratedAction(null, 0,TREASURE_BONUS_ACTION, null, 0.0);
            initBoxes(queuedBonuses);
            this.gameMode = BONUS_MODE;
        }
	       
        if (freeSpinSymbolsFound > 0) {
            int freeSpins = 10;
            
            GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
            freeSpinsAction.setActionValue(Integer.toString(freeSpins) + " " + Integer.toString(freeSpinSymbolsFound));
            
            pushFreeSpinToStack(new FreeSpinSet(freeSpins, freeSpinSymbolsFound));
        }
        
        if(this.gameMode != BONUS_MODE) {
	        endRound(action);
        } else {
        	action.commit();
        }
    }

    private void endRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method endRound() already called once.");
        }

        action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        action.commit(true);
        this.finished = true;
    }

    /**
     * Checks if the given payline forms any winning combinations.
     * @param payline The payline to check.
     * @return The combination the payline forms or -1 if it doesn't form any.
     */
    public int getPaylineWinCombination(int[] payline) {
        Object[][] winCombos = getConfig().getWinCombos();
        int maxPayoutCombo = -1;
        
        for (int i = winCombos.length - 1; i >= 0; i--) {
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            if ((symbol != SCATTER_SYMBOL) && (symbol != BONUS_SYMBOL) && getComboPayout(i) < 0.01) {
                continue;
            }
            
            int j;

            boolean foundSymbol = false;
            for (j = 0; j < payline.length; j++) {
                if (payline[j] == symbol) {
                    foundSymbol = true;
                } else if ((payline[j] == WILD_SYMBOL)
                        && (symbol != SCATTER_SYMBOL)
                        && (symbol != BONUS_SYMBOL)) {
                    continue;
                } else {
                    break;
                }
            }

            if ((j == quantityRequired) && foundSymbol) {
                if (getComboPayout(i) > getComboPayout(maxPayoutCombo)) {
                    maxPayoutCombo = i;
                }
                
                /* Always prefer free spins and bonus over normal combinations */
                if (symbol == BONUS_SYMBOL ||  symbol == SCATTER_SYMBOL)
                    return i;
            }
        }

        return maxPayoutCombo;
    }

    /**
     * Checks if the given reel view forms any winning combinations.
     * 
     * @param reelView
     *            The view to check.
     * @return The combination the view forms or -1 if it doesn't form any.
     */
    public final List<WinCombination> getReelViewWinCombination(int[][] reelView) {
    	List<WinCombination> combos = new ArrayList<WinCombination>();
        List<Integer> comboSymbols = new ArrayList<Integer>();
        Object[][] winCombos = getConfig().getWinCombos();

        for (int i = winCombos.length - 1; i >= 0; i--) {
            int symbol = (Integer)winCombos[i][0];
            int quantity = (Integer)winCombos[i][1];

            if (symbol >= 0)
                continue;

            symbol = symbol * -1;
            
            /* A combo with the symbol was already found. skip */
            if (comboSymbols.contains(new Integer(symbol))) {
                continue;
            }
            boolean foundOnLine = false;

            for (int j = 0; j < reelView[0].length; j++) {
                foundOnLine = false;
                for (int k = 0; k < 3; k++) {
                    if (reelView[k][j] == symbol) {
                        foundOnLine = true;
                        break;
                    }
                }

                if (foundOnLine) {
                    quantity--;
                }

                if (quantity <= 0) {
                    comboSymbols.add(symbol);
                    combos.add(new WinCombination(j, i));
                    break;
                }
            }

        }

        return combos;
    }
    
    public double getComboPayout(int combo) {
        if (combo < 0) {
            return 0.0;
        }
        
        return getConfig().getPayout(combo);
    }

    @Override
	public boolean isFinished() {
        return finished;
    }
    
    @Override
    public GameActionFilter getActionFilter() {
        return new DartkTombsActionFilter();
    }
}
