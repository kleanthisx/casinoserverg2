package com.magneta.games.slots.geiko;

import static com.magneta.games.configuration.slots.GeikoConfiguration.SCATTER_SYMBOL;
import static com.magneta.games.configuration.slots.GeikoConfiguration.WILD_SYMBOL;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.GeikoConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("Geiko")
public class GeikoRound extends SlotRound {

    private static final String FREE_SPINS_WIN_ACTION = "g:FREE_SPINS_WIN";
    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";

    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description             Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,             ParameterType.REQUIRED,  ParameterType.REQUIRED),
        new GameActionRequestDefinition(START_FREE_SPIN_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
    };

    private static final double FREE_SPIN_MULTIPLIER = 2.0;

    private int freeSpinsRemaining;
    private boolean finished;
    private double betAmount;
    private int maxPayline;

    private boolean freeSpin;
    private boolean placedJackpotContrib;
    
    public GeikoRound() {
        this.betAmount = 0.0;
        this.maxPayline = -1;
        this.freeSpinsRemaining = 0;
        this.finished = false;
        this.freeSpin = false;
        this.placedJackpotContrib = false;
    }
    
    @Override
	public GeikoConfiguration getConfig() {
    	return (GeikoConfiguration) super.getConfig();
    }

    @Override
	public void roundInit(AbstractGameRound parentRound) {
    	GeikoRound parent = (GeikoRound) parentRound;
        
        this.freeSpinsRemaining = parent.freeSpinsRemaining;
        
        if (this.freeSpinsRemaining > 0){
            this.betAmount = parent.betAmount;
            this.maxPayline = parent.maxPayline;
            
            GameAction freeSpinAction = new GameAction(null, this, 0, FREE_SPIN_ACTION, maxPayline+" "+betAmount, 0.0);
            freeSpinAction.setActionValue(String.valueOf(this.freeSpinsRemaining));
            freeSpinAction.setRoundStart();
            
            try {
                freeSpinAction.commit();
                actionCompleted(freeSpinAction);
                this.freeSpin = true;
            } catch (ActionFailedException e) {
                logError("Error saving free spin action.", e);
            }
        }
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    @Override
    protected void loadAction(GameAction action) throws ActionFailedException {
        if (FREE_SPIN_ACTION.equals(action.getDescription())){
            try{
                this.freeSpinsRemaining = Integer.parseInt(action.getActionValue());
                this.freeSpin = true;
                String[] tokens = action.getArea().split(" ");

                if (tokens.length == 2){
                    this.maxPayline = Integer.parseInt(tokens[0]);
                    this.betAmount = Double.parseDouble(tokens[1]);
                } else {
                    throw new ActionFailedException("Free spin load failed. Values missing.");
                }
            } catch (NumberFormatException e){
                throw new ActionFailedException("Free spin load failed. An invalid value was passed.");
            }

            actionCompleted(action);

        } else {
            doAction(action);
        }
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        } else if (START_FREE_SPIN_ACTION.equals(action.getDescription())){
            performFreeSpin(action);
        }
    }

    private void performFreeSpin(GameAction action) throws ActionFailedException {
        if (!freeSpin) {
            throw new ActionFailedException("Not in free spin mode!");
        }

        action.commit();
        this.freeSpinsRemaining--;
        startRound(action);
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (freeSpin) {
            throw new ActionFailedException("Round is a free spin; No bets allowed.");
        } else if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }

        checkBet(action);

        int area = -1;

        try{
            area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e){
        }

        if ((area < 0) || (area >= getConfig().getPaylines().length)){
            throw new ActionFailedException("Invalid action area");
        }

        double betUnit;
        double totalBet;
        
        if (action.isLoaded()) {
            totalBet = action.getAmount();
            betUnit = action.getAmount() / (area+1);
        } else {
            betUnit = action.getAmount();
            totalBet = betUnit * (area+1);
            action.setAmount(totalBet);
        }

        action.setRoundStart();
        action.commit();

        betAmount = betUnit;
        maxPayline = area;

        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    private void startRound(GameAction action) throws ActionFailedException {
        if (START_FREE_SPIN_ACTION.equals(action.getDescription()) && (freeSpinsRemaining < 0)){
            this.maxPayline = -1;
            this.betAmount = 0.0;
            this.freeSpinsRemaining = 0;
            this.freeSpin = false;
            throw new IllegalStateException("No free spins remain. Round must be started normally.");
        } else if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        logDebug("Starting round...");

        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
            this.finished = true;
            return;
        }

        int[] reelStops = spinReels(getConfig().getReels(0));
        int[][] reelsView = calculateReelView(getConfig().getReels(0), reelStops);
  
        createReelViewAction(action, reelsView);
        action.commit();

        //int[] lineWinCombo = new int[maxPayline+1];
        int[] currLine = new int[getConfig().getNumberOfReels(0)];

        int freeSpins = 0;
        double roundMultiplier = 1.0;

        if (freeSpin) {
            roundMultiplier *= FREE_SPIN_MULTIPLIER;
        }
        
        for (int i=0;i <= maxPayline;i++){
            getPayline(reelsView, i, currLine);

            int combo = getPaylineWinCombination(currLine);

            if (combo < 0)
                continue;

            double multiplier = roundMultiplier * getPaylineMultiplier(currLine) * (Double)getComboPayout(combo);
            double winAmount = multiplier * betAmount;

            if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
                    && JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
                //stop the game from retrying to claim jackpot on other line
                placedJackpotContrib = false;
            } else {
                GameAction winAction = action.createGeneratedAction(
                        action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                winAction.setActionValue(String.valueOf(combo));
                winAction.commit();
            }
        }

        int viewCombo = getReelViewWinCombination(getConfig(), reelsView);

        if (viewCombo != -1){
            
            double multiplier = roundMultiplier * (Double)getComboPayout(viewCombo);
            double winAmount = multiplier * betAmount * (maxPayline+1);
            
            if (winAmount > 0.01) {
                GameAction winAction = action.createGeneratedAction(
                        action.getPlayer(), 1, WIN_ACTION, null, winAmount);
                winAction.setActionValue(String.valueOf(viewCombo));
            }
            
            /* Scatter gives free spins */
            
            if (((Integer)getConfig().getWinCombo(viewCombo)[0]).equals(-SCATTER_SYMBOL)) {
                freeSpins += 15;
            }
            if (freeSpins > 0) {
                GameAction freeSpinsAction = action.createGeneratedAction(null, 0, FREE_SPINS_WIN_ACTION, "", 0.0);
                freeSpinsAction.setActionValue(Integer.toString(freeSpins));
                freeSpinsAction.commit();
                this.freeSpinsRemaining += freeSpins;
            }
        }

        endRound(action);
    }

    private void endRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method endRound() already called once.");
        }

        action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        action.commit(true);
        this.finished = true;
    }

    /**
     * Checks if the given payline forms any winning combinations.
     * @param payline The payline to check.
     * @return The combination the payline forms or -1 if it doesn't form any.
     */
    public int getPaylineWinCombination(int[] payline) {
        Object[][] winCombos = getConfig().getWinCombos();

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            if ((symbol != SCATTER_SYMBOL)&& (Double)getComboPayout(i) < 0.01) {
                continue;
            }
            
            int j;

            boolean foundSymbol = false;
            for (j=0;j<payline.length;j++){
                if (payline[j] == symbol){
                    foundSymbol = true;
                } else if ((payline[j] == WILD_SYMBOL) && (symbol != SCATTER_SYMBOL)) {
                    continue;
                } else {
                    break;
                }
            }

            if ((j == quantityRequired) && foundSymbol){
                return i;
            }
        }        

        return -1;
    }

    public double getPaylineMultiplier(int [] payline) {
        double multiplier = 1.0;
        
        for (int i=0; i < payline.length - 1; i++) {
            if (payline[i] == WILD_SYMBOL) {
                multiplier = i + 2.0;
            }
        }
        
        return multiplier;
    }
    
    /**
     * Checks if the given reel view forms any winning combinations.
     * @param reelView The view to check.
     * @return The combination the view forms or -1 if it doesn't form any.
     */
    public static final int getReelViewWinCombination(SlotConfiguration config, int[][] reelView){
        Object[][] winCombos = config.getWinCombos();

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantity = (Integer)winCombos[i][1];

            if (symbol >= 0)
                continue;

            symbol = symbol * -1;
            boolean foundOnLine = false;

            for (int j=0;j<reelView[0].length;j++){
                foundOnLine = false;
                for (int k=0;k<3;k++){
                    if (reelView[k][j] == symbol){
                        foundOnLine = true;
                        break;
                    }
                }

                if (foundOnLine) {
                    quantity--;
                }

                if (quantity <= 0){
                    return i;
                }
            }

        }        

        return -1;
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    public Object getComboPayout(int combo){
        if (combo < 0) {
            return 0.0;
        }

        return getConfig().getPayout(combo);
    }
}
