package com.magneta.games.slots.tuttifruity;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.configuration.slots.TuttiFruityConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("TuttiFruity")
public class TuttiFruityRound extends SlotRound {
    
    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description        Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,        ParameterType.REQUIRED,  ParameterType.REQUIRED)
    };
    
    private int maxPayline;
    private double betAmount;
    private boolean finished;
    private boolean placedJackpotContrib;
    
    @Override
	public TuttiFruityConfiguration getConfig() {
    	return (TuttiFruityConfiguration)super.getConfig();
    }
    
    public TuttiFruityRound() {
        this.maxPayline = -1;
        this.betAmount = 0.0;
        this.finished = false;
        this.placedJackpotContrib = false;
    }

    @Override
    protected void performAction(GameAction action)
            throws ActionFailedException {
        
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        }
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }
        
        int area = -1;

        try{
        	area = Integer.parseInt(action.getArea());
        } catch (NumberFormatException e) {
        }

        if ((area < 0) || (area >= getConfig().getPaylines().length)) {
        	throw new ActionFailedException("Invalid action area");
        }
        
        checkBet(action);

        double betUnit = action.getAmount();
        double totalBet = betUnit * (Integer.parseInt(action.getArea())+1);

        action.setAmount(totalBet);        
        action.setRoundStart();
        action.commit();
        
        betAmount = betUnit;
        maxPayline = Integer.parseInt(action.getArea());
        
        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);
        
        startRound(action);
    }

    /**
     * Starts and finishes the current round for this game.
     * @param action The cause action for the round start.
     */
    private void startRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
            this.finished = true;
            return;
        }

        int[][] roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);
        
        /* FIXED ODD CODE */
        com.magneta.games.fixedodd.TuttiFruityGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), this.betAmount, this.maxPayline, reelStops, options.getGameSettings(), action);        
        
        int[][] reelsView = calculateReelView(roundReels, reelStops);
        
        createReelViewAction(action, reelsView);

        int[] currLine = new int[roundReels.length];
        
        GameAction finishAction = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        
        for (int i=0;i<maxPayline+1;i++){
            getPayline(reelsView, i, currLine);
            
            int combo = getPaylineWinCombination(getConfig(), currLine);
            if (combo != -1) {
                double multiplier = getConfig().getPayout(combo);
                double winAmount = multiplier * betAmount;
                
                if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
                        && JackpotsHandler.claimGameJackpot(this, finishAction, i, winAmount, jackpotRatio)) {               
                    //stop the game from retrying to claim jackpot on other line
                    placedJackpotContrib = false;
                } else {
                	GameAction winAction = finishAction.createGeneratedAction(
                			action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                	winAction.setActionValue(String.valueOf(combo));
                }
            }
        }
        
        int viewCombo = getReelViewWinCombination(getConfig(), reelsView);
        
        if (viewCombo != -1){
            double multiplier = getConfig().getPayout(viewCombo);
            double winAmount = multiplier * betAmount * (maxPayline + 1);
            GameAction winAction = action.createGeneratedAction(
                    action.getPlayer(), 1, WIN_ACTION, null, winAmount);
            winAction.setActionValue(String.valueOf(viewCombo));
        }

        action.commit(true);
        this.finished = true;
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request)
            throws ActionFailedException {
        
        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);
        
        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }
    
    /**
     * Checks if the given line forms any winning combinations.
     * @param payline The line to check.
     * @return The combination the line forms or -1 if it doesn't form any.
     */
    public static int getPaylineWinCombination(SlotConfiguration config, int[] payline){
        Object[][] winCombos = config.getWinCombos();
        
        for (int i=winCombos.length-1;i>=0;i--){
            int reel1 = (Integer)winCombos[i][0];
            
            if (reel1 >= 0){ 
                boolean match = true;
                
                for (int j=0;j<payline.length;j++){
                    if ((payline[j] != ((Integer)winCombos[i][j])) 
                            && (((Integer)winCombos[i][j]) != 0)){
                        match = false;
                        break;
                    }
                }
                
                if (match){
                    return i;
                }
            }
        }        
        
        return -1;
    }
    
    /**
     * Checks if the given reel view forms any winning combinations.
     * @param reelView The view to check.
     * @return The combination the view forms or -1 if it doesn't form any.
     */
    public static int getReelViewWinCombination(SlotConfiguration config, int[][] reelView){
        Object[][] winCombos = config.getWinCombos();
        
        for (int i=winCombos.length-1;i>=0;i--){
            int reel1 = (Integer)winCombos[i][0];
            
            if (reel1 < 0){ 
                int count = 0;
                
                for (int j=0;j<reelView.length;j++){
                    for (int k=0;k<3;k++){
                        if (reelView[k][j] == ((-1) * ((Integer)winCombos[i][k]))){
                            count++;
                            break;
                        }
                    }
                    
                    if (count == reelView[j].length){
                        return i;
                    }
                }
            }
        }        
        
        return -1;
    }
}