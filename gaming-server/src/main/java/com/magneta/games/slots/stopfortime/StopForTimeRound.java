package com.magneta.games.slots.stopfortime;

import static com.magneta.games.configuration.slots.StopForTimeConfiguration.WIN_CONSTRAINTS;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.StopForTimeConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("StopForTime")
public class StopForTimeRound extends SlotRound {

    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description        Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN, ParameterType.REQUIRED)
    };

    private double betAmount;
    private boolean finished;
    private boolean placedJackpotContrib;

    @Override
	public StopForTimeConfiguration getConfig() {
    	return (StopForTimeConfiguration)super.getConfig();
    }

    public StopForTimeRound() {
        this.placedJackpotContrib = false;
        this.betAmount = 0.0;
        this.finished = false;
    }

    private void startRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }

        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }

        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
            this.finished = true;
            return;
        }

        int[][] roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);

        /* FIXED ODD CODE */
        com.magneta.games.fixedodd.StopForTimeGuard.guardRound(game.getGameId(), this.options.getOwner(), getConfig(), betAmount, reelStops, options.getGameSettings(), action);

        //get the reels view
        int[][] reelsView = calculateReelView(roundReels, reelStops);

        /* rolling the wheels */
        createReelViewAction(action, reelsView);

        int[] currLine = new int[5];

        //check lines
        for (int i=0;i<currLine.length;i++){
            getPayline(reelsView, i, currLine);

            int combo = getPaylineWinCombination(currLine);
            if (combo != -1){
                double multiplier = getConfig().getPayout(combo);
                double winAmount = multiplier * betAmount;

                if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
                        && JackpotsHandler.claimGameJackpot(this, action, i, winAmount, jackpotRatio)) {               
                    //stop the game from retrying to claim jackpot on other line
                    placedJackpotContrib = false;
                } else {
                    GameAction winAction = action.createGeneratedAction(
                            action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                    winAction.setActionValue(String.valueOf(combo));
                }
            }
        }

        endRound(action);

    }

    /**
     * Ends the round.
     * @param parentAction The action that caused the round to end.
     */
    private void endRound(GameAction parentAction) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method endRound() already called once.");
        }

        parentAction.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        parentAction.commit(true);
        this.finished = true;
    }
    
    public static int getPaylineWinCombination(int[] payline){
        Object[][] winCombos = WIN_CONSTRAINTS;

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            //try from the left 
            int j=0;
            for (;j<payline.length;j++){
                if (payline[j] != symbol){
                    break;
                }
            }

            if (j == quantityRequired){
                return i;
            }

            //try from the right
            j=payline.length-1;
            for (;j>=0;j--){
                if (payline[j] != symbol){
                    break;
                }
            }

            boolean payOnCenter = false;           
            if (winCombos[i].length > 3){
                payOnCenter = (Boolean)winCombos[i][3];
            }

            if (((payline.length-1) - j) == quantityRequired){
                return i;
            } else if ((payOnCenter) && (quantityRequired == 3)){
                //CENTER PAYLINE CHECK
                int maxCount = 0;
                int currCount = 0;
                for (int k=0;k<payline.length;k++){
                    if (payline[k] != symbol){
                        if (currCount > maxCount){
                            maxCount = currCount;
                        }
                        currCount = 0;
                    } else {
                        currCount++;
                    }
                }
                if (maxCount == quantityRequired){
                    return i;
                }
            }
        }        

        return -1;
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        }
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }

        checkBet(action);

        action.setRoundStart();
        action.commit();
        betAmount = action.getAmount();

        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {

        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }
}