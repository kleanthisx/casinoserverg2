package com.magneta.games.slots.crazycircus;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.CrazyCircusConfiguration;
import com.magneta.games.slots.SlotRound;

@GameTemplate("CrazyCircus")
public class CrazyCircusRound extends SlotRound {
    
    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description        Area                      Amount                */
        new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED)
    };
    
    private double betAmount;
    private boolean finished;
    private boolean placedJackpotContrib;
    
    public CrazyCircusRound() {
        this.betAmount = 0.0;
        this.finished = false;
        this.placedJackpotContrib = false;
    }
    
    @Override
	public CrazyCircusConfiguration getConfig() {
    	return (CrazyCircusConfiguration)super.getConfig();
    }

    @Override
    protected void performAction(GameAction action)
            throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            performBet(action);
        }
    }

    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (betAmount > 0.0){
            throw new ActionFailedException("Bet failed: already placed bet!");
        }
        
        checkBet(action);

        action.setRoundStart();
        action.commit();        
        betAmount = action.getAmount();
        
        placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

        startRound(action);
    }

    /**
     * Starts and finishes the current round for this game.
     * @param action The cause action for the round start.
     * @throws ActionFailedException 
     */
    private void startRound(GameAction action) throws ActionFailedException {
        if (finished){
            throw new IllegalStateException("Method startRound() already called once.");
        }
        
        double jackpotRatio = (betAmount / options.getMaxBet());
        if (jackpotRatio > 1.0){
            jackpotRatio = 1.0;
        }
        
        if (claimMysteryJackpot(action, betAmount, jackpotRatio, placedJackpotContrib)){
            this.finished = true;
            return;
        }
        
        int[][]roundReels = getConfig().getReels(0);
        int[] reelStops = spinReels(roundReels);
        
        /* FIXED ODD CODE */
        //com.magneta.games.fixedodd.JuicyFruityGuard.guardRound(game.getId(), this.options.getOwner(), roundReels, WIN_CONSTRAINTS, roundPayouts, this.betAmount, reelStops, options.getGameSettings(), action);        
        
        int[][] reelsView = calculateReelView(roundReels, reelStops);
        
        createReelViewAction(action, reelsView);
        
        int[] lineWinCombo = new int[5];
        int[] currLine = new int[roundReels.length];
        
        GameAction finishAction = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        finishAction.commit();

        for (int i=0;i<lineWinCombo.length;i++){
            getPayline(reelsView, i, currLine);
            
            int combo = lineWinCombo[i] = getPaylineWinCombination(currLine);
            if (combo != -1){
                double multiplier = getConfig().getPayout(combo);
                double winAmount = multiplier * betAmount;
                
                if (placedJackpotContrib && getConfig().isJackpotCombination(i, combo)  
                        && JackpotsHandler.claimGameJackpot(this, finishAction, i, winAmount, jackpotRatio)) {               
                    //stop the game from retrying to claim jackpot on other line
                    placedJackpotContrib = false;
                } else {
                    GameAction winAction = finishAction.createGeneratedAction(
                            action.getPlayer(), 1, WIN_ACTION, String.valueOf(i), winAmount);
                    winAction.setActionValue(String.valueOf(combo));
                    winAction.commit();
                }
            }
        }
        
        int viewCombo = getReelViewWinCombination(reelsView);
        
        if (viewCombo != -1){
            double multiplier = getConfig().getPayout(viewCombo);
            double winAmount = multiplier * betAmount;
            GameAction winAction = action.createGeneratedAction(
                    action.getPlayer(), 1, WIN_ACTION, null, winAmount);
            
            winAction.setActionValue(String.valueOf(viewCombo));
        }

        action.commit(true);
        this.finished = true;  
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request)
            throws ActionFailedException {
        
        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);
        
        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }
    
    private int getPaylineWinCombination(int[] payline){
        Object[][] winCombos = getConfig().getWinCombos();

        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];

            //try from the left 
            int j=0;
            for (;j<payline.length;j++){
                if (payline[j] != symbol){
                    break;
                }
            }

            if (j == quantityRequired){
                return i;
            }
        }        

        return -1;
    }
    
    /**
     * Checks if the given reel view forms any winning combinations.
     * @param reelView The view to check.
     * @return The combination the view forms or -1 if it doesn't form any.
     */
    private int getReelViewWinCombination(int[][] reelView){
        Object[][] winCombos = getConfig().getWinCombos();
        
        for (int i=winCombos.length-1;i>=0;i--){
            int symbol = (Integer)winCombos[i][0];
            int quantityRequired = (Integer)winCombos[i][1];
            
            if (symbol < 0){ 
                int count = 0;
                symbol *= -1;
                
                for (int j=0;j<reelView.length;j++){
                    for (int k=0;k<3;k++){
                        if (reelView[k][j] == symbol){
                            count++;
                            break;
                        }
                    }
                    
                    if (count == quantityRequired){
                        return i;
                    }
                }
            }
        }
        
        return -1;
    }
}
