package com.magneta.games;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.TableManager;
import com.magneta.casino.games.server.session.GamePlayerLogoutListener;
import com.magneta.casino.services.ServiceException;

public class TableLogoutListener implements GamePlayerLogoutListener {

	private static final Logger log = LoggerFactory.getLogger(TableLogoutListener.class);
	
	private final TableManager tableManager;
	private final Table table;
	
	public TableLogoutListener(TableManager tableManager, Table table) {
		this.tableManager = tableManager;
		this.table = table;
	}
	
	@Override
	public void onLogout(GamePlayer player) {
		try {
			tableManager.markViewerActive(table, player, false);
		} catch (ServiceException e) {
			log.error("Unable to mark player inactive in table", e);
		}
        tableManager.removeTable(table);
	}
}
