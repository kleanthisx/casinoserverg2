package com.magneta.games;

/**
 * ActionFailedException is thrown whenever a GameAction object fails for any reason.
 */
public class ActionFailedException extends Exception {

    private static final long serialVersionUID = -7055544999397780682L;
    
    /**
     * Constructs a new ActionFailedException with the message given.
     * @param msg The exception description message.
     */
    public ActionFailedException(String msg) {
        super(msg);
    }

    /**
     * Constructs a new ActionFailedException witn the exception that caused it.
     * @param msg The exception description message.
     * @param ex The exception that caused this exception.
     */
    public ActionFailedException(String msg, Throwable ex) {
        super(msg, ex);
    }
}
