package com.magneta.games.blackjack;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;

/**
 * BlackjackHand holds the attributes and behaviours of the blackjack player's hand.
 */
public class BlackjackHand {
   
	private static final Logger log = LoggerFactory.getLogger(BlackjackHand.class);
	
    private List<Integer> cards;
    private boolean isFinished;
    private boolean blackjack;
    private boolean busted;
    private boolean doubleBet;
    private boolean charlie;
    private boolean split;
    
    /**
     * Constructs a new BlackjackHand.
     */
    public BlackjackHand() {
        cards = new ArrayList<Integer>();
        isFinished  = false;
        blackjack = false;
        busted = false;
        doubleBet = false;
        charlie = false;
        split = false;
    }
    
    /**
     * Gets the sum of all the cards of the hand.
     * @return The sum of all the cards of the hand.
     */
    public int sumCards(){
       return BlackjackDeck.sumCards(cards);
    }
    
    /**
     * Gets a new card for this hand from the round's deck.
     * @param round The round where the action takes place.
     * @param action The action object to update.
     * @return The new card that was drawn.
     * @throws ActionFailedException 
     */
    public void hit(Deck deck, GameAction action) throws ActionFailedException {

        boolean generated = action.isGeneratedAction();
        int nextCard;
        boolean finished = false;

        nextCard = deck.getNextCard();
        action.setActionValue(String.valueOf(nextCard));
        if (!generated) {
            try {
                action.commit();
            } catch (ActionFailedException e) {
                log.error("Exception", e);
                deck.returnCard(nextCard);
                throw new ActionFailedException("Error while hitting", e);
            }
        }
        
        cards.add(nextCard);
        int handValue = sumCards();
        
        if (handValue > 21) {
            busted = true;
            finished = true;
        }
        else if ((cards.size() == 10) && (!busted)) {
            charlie = true;
            finished = true;
        }
        else if ((cards.size() == 2) && (handValue == 21)) {
            if (!split) {
                blackjack = true;
            }
            finished = true;
        }
        else if (handValue == 21){
            finished = true;
        } else if (split && cards.size() == 2) {
            /* On split Aces the player can only draw one card */
            if (BlackjackDeck.getCardValue(cards.get(0)) == 1) {
                finished = true;
            }
        }

        if (finished) {
            try {
                stand(action.createGeneratedAction(null, action.getSeatId(),
                        "g:STAND", action.getArea(), 0.0));

                if (!generated) {
                    action.commit();
                }
            }catch (ActionFailedException ex) {
                log.warn("Error while saving subaction", ex);
            }
        }
    }
    
    /**
     * Ends the players turn(i.e. performs the stand action).
     * @param action The action object to update.
     * @throws ActionFailedException 
     */
    public void stand(GameAction action) throws ActionFailedException {
	
    	if (!action.isGeneratedAction()) {
    		action.commit();
    	}
    	
        isFinished = true;
    }
    
    /**
     * Doubles the hand's bet (if possible), draws a new card and ends the hand's turn.
     * @param deck The deck to use.
     * @param action The action object to update.
     * @return The card drawn after doubling bet.
     * @throws ActionFailedException 
     */
    public void doubleBet(Deck deck, GameAction action) throws ActionFailedException {
	
		if (!isAllowedToDouble()) {
		    throw new ActionFailedException("Hand is not allowed double at this time");
		}
        
        action.commit();
        doubleBet = true;
        
        hit(deck, action.createGeneratedAction(action.getPlayer(), action.getSeatId(), "g:HIT", action.getArea(), 0.0));        
        
        if (!isFinished())
        	stand(action.createGeneratedAction(null, action.getSeatId(), "g:STAND", action.getArea(), 0.0));
        
        try {
            action.commit();
        } catch (ActionFailedException ex) {
            log.warn("Error while saving sub-actions", ex);
        }

    }
    
    /**
     * Splits the players cards into two hands.
     * @param round The round where the action takes place.
     * @param action The action object to update.
     * @return The new hand created after splitting.
     * @throws ActionFailedException 
     */
    public BlackjackHand split(Deck deck, GameAction action) throws ActionFailedException {

    	if (Card.getCardNumber(cards.get(0)) != Card.getCardNumber(cards.get(1))) {
    		throw new ActionFailedException("Cannot split different cards");
    	}

    	action.commit();

    	BlackjackHand hand = new BlackjackHand();

    	int cardRemoved = cards.remove(1);
    	hand.cards.add(cardRemoved);

    	this.split = true;
    	hand.split = true;

    	hand.hit(deck, action.createGeneratedAction(action.getPlayer(), action.getSeatId(), "g:HIT", "1", 0.0));
    	this.hit(deck, action.createGeneratedAction(action.getPlayer(), action.getSeatId(), "g:HIT", "0", 0.0));

    	try {
    		action.commit();
    	} catch (ActionFailedException e) {
    		log.warn("Error commiting split action.", e);
    	}

    	return hand;
    }
    
    /**
     * Gets the amount earned by the hand after the end of the game by comparing the hand to the dealers hand.
     * @param dealer The dealer of the round where the hand belongs to.
     * @param betAmount The anount bet by the seat.
     * @return The amount earned by the hand.
     */
    public double calculateEarnings(BlackjackDealer dealer, double betAmount, StringBuilder buf) {
        double earnings = 0.0; 
        
        /* player scores 10-card charlie */
        if (charlie){
            if (dealer.hasBlackjack()) {
                return 0.0;
            }
            
             if (doubleBet) {
                 earnings = (betAmount*4.0);
             } else {
                 earnings = (betAmount*2.0);
             }
             buf.append("10-Card Charlie");
             
             return earnings;
        }
        
        if (dealer.hasBlackjack()) {
            if (blackjack) {
                earnings = betAmount;
                buf.append("Push");
            } else {
                return 0.0;
            }
        } else if (blackjack) {
            earnings = betAmount * 2.5;
            buf.append("Blackjack");
        } else if (!busted) {
            
            //dealer busts
            if (dealer.hasBusted()) {
                earnings = betAmount * 2.0;
                buf.append("Dealer Busted");
                // beat dealer
            } else if ((sumCards() > dealer.sumCards())) {
                earnings = (betAmount*2.0);
                buf.append("Beat Dealer");
            } //push
            else if ((sumCards() == dealer.sumCards())) {
                earnings = betAmount;
                buf.append("Push");
            }
        }
        
        if (doubleBet) {
            earnings = earnings * 2.0;
        }
        
        return earnings;
    }
    
    /**
     * Retrieves whether the current hand has busted.
     * @return Whether the current hand has busted.
     */
    public boolean hasBusted(){
        return busted;
    }
    
    /**
     * Retrieves whether the current hand has finished.
     * @return Whether the current hand has finished.
     */
    public boolean isFinished(){
        return isFinished;
    }
    
    /**
     * Retrieves whether the current hand has scored a blackjack.
     * @return Whether the current hand has scored a blackjack.
     */
    public boolean hasBlackjack(){
       return blackjack;
    }
    
    /**
     * Retrieves whether the current hand has scored a ten-card charlie.
     * @return Whether the current hand has scored  a ten-card charlie.
     */
    public boolean hasTenCardCharlie(){
        return charlie;
    }
    
    /**
     * Returns the cards of the hand.
     * @return The cards of the hand.
     */
    protected List<Integer> getCards(){
        return cards;
    }
    
    /**
     * Returns whether the hand is allowed to double or not.
     * @return Whether the hand is allowed to double.
     */
    public boolean isAllowedToDouble(){
        return ((cards.size() == 2) && (!doubleBet));
    }
}
