package com.magneta.games.blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionFilter;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * BlackjackRound is the blackjack game-specific GameRound implementation.
 */
@GameTemplate("Blackjack")
public class BlackjackRound extends AbstractGameRound {

	/**
	 * The constant action description value for the deal action.  
	 */
	private static final String DEAL_ACTION = "g:DEAL";
	/**
	 * The constant action description value for the hit action.  
	 */
	private static final String HIT_ACTION = "g:HIT";
	/**
	 * The constant action description value for the stand action.  
	 */
	private static final String STAND_ACTION = "g:STAND";
	/**
	 * The constant action description value for the split action.  
	 */
	private static final String SPLIT_ACTION = "g:SPLIT";
	/**
	 * The constant action description value for the double action.  
	 */
	private static final String DOUBLE_ACTION = "g:DOUBLE";
	/**
	 * The constant action description value for the insurance action.  
	 */
	private static final String INSURANCE_ACTION = "g:INSURE";
	/**
	 * The constant action description value for the reject action.  
	 */
	private static final String REJECT_ACTION = "g:REJECT";

	/**
	 * Even money action. Dealer might have a blackjack
	 * and the player has a blackjack. Payout 1:1.  
	 */
	private static final String EVEN_MONEY_ACTION = "g:EVEN";

	/*                                  desc               area                     amount */
	private static final GameActionRequestDefinition [] ALLOWED_ACTIONS = {
		new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN, ParameterType.REQUIRED),
		new GameActionRequestDefinition(HIT_ACTION,        ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(STAND_ACTION,      ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(DOUBLE_ACTION,     ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(SPLIT_ACTION,      ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(INSURANCE_ACTION,  ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(REJECT_ACTION,     ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(EVEN_MONEY_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(DEAL_ACTION,       ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
	};

	private Deck roundDeck;
	private BlackjackSeat[] seats; 
	private BlackjackDealer dealer;

	private int currSeatId;
	private GameRoundState roundState;

	/**
	 * Constructs a new BlackjackRound.
	 * @param game The blackjack game held by the round.
	 * @param options The round's options values.
	 * @param tableId The ID of the table. 
	 * @param roundId The ID of the round to construct.
	 * @param seed The seed to used for the random generator.
	 */
	@Override
	public void roundInit() {
		int noDecks = options.getGameSettings().get("decks", Integer.TYPE);
		int noSeats = options.getGameSettings().get("seats", Integer.TYPE);

		roundDeck = new Deck(noDecks,0);
		seats = new BlackjackSeat[noSeats+1];
		dealer = new BlackjackDealer();
		currSeatId = 0;
		roundState = GameRoundState.ROUND_NOT_STARTED;
	}

	@Override
	public boolean isFinished() {
		return roundState == GameRoundState.ROUND_FINISHED;
	}

	/**
	 * Moves the current seat index to the next player, if the current seat is finished.
	 * @throws ActionFailedException 
	 */
	private void nextPlayer(GameAction parentAction) throws ActionFailedException {

		if (currSeatId > 0) {
			for (; currSeatId < seats.length; currSeatId++) {
				if ((seats[currSeatId] != null) && (seats[currSeatId].hasBet()) && 
						((!seats[currSeatId].isFinished()) || seats[currSeatId].insurancePending())) {
					return;
				}
			}
		}

		currSeatId = 0;

		boolean allBusted = true;

		for (int i=1; i < seats.length; i++) {
			if ((seats[i] != null) && (!seats[i].hasBusted())) {
				allBusted = false;
				break;
			}   
		}

		/* FIXED ODD CODE */
		com.magneta.games.fixedodd.BlackjackGuard.guardRound(game.getGameId(), roundDeck, seats, dealer, options.getGameSettings(), parentAction);
		dealer.play(this.roundDeck, parentAction, allBusted); 

		endRound(parentAction);
	}

	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());
		
		if (result == null) {
			return;
		}
		
		List<Integer> topCards = new ArrayList<Integer>();
		String[] sReelStops = result.split(" ");

		for (int i=0; i < sReelStops.length; i++) {
			try {
				topCards.add(Integer.parseInt(sReelStops[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}
		
		if (topCards.isEmpty()) {
			return;
		}
		
		roundDeck.setupDeck(topCards);
	}

	/**
	 * Deals two(2) cards to every seat and the dealer.
	 * @param dealAction The deal GameAction object to update.
	 * @throws ActionFailedException
	 */
	private void deal(GameAction dealAction) throws ActionFailedException {

		roundDeck.shuffleCards(getRandom());
		setDeck();

		for (int i=1; i <  seats.length; i++) {
			if (seats[i] != null && seats[i].hasBet()) {
				seats[i].hit(this.roundDeck, dealAction.createGeneratedAction(i, "g:HIT", "0", 0.0));
			}
		}

		dealer.hit(this.roundDeck, dealAction.createGeneratedAction(0, "g:HIT", dealAction.getArea(), 0.0));

		for (int i=1;i<seats.length;i++) {
			if (seats[i] != null && seats[i].hasBet()) {
				seats[i].hit(this.roundDeck, dealAction.createGeneratedAction(i, "g:HIT", "0", 0.0));
			}
		}

		if (dealer.shouldOfferInsurance()) {

			for (int i=1;i<seats.length;i++) {
				if (seats[i] != null) {
					seats[i].offerInsurance();
				}
			}
		}

		dealer.hit(this.roundDeck, dealAction.createGeneratedAction(0, "g:HIDDEN", dealAction.getArea(), 0.0));

		currSeatId = 1;
		nextPlayer(dealAction);

		dealAction.commit(isFinished());
	}

	/**
	 * Starts the round.
	 * @param parentAction The action which caused the START action.
	 * @throws ActionFailedException 
	 */
	private void startRound(GameAction dealAction) throws ActionFailedException {
		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Round already started");
		}

		if (this.getActiveSeatCount() < 1) {
			throw new ActionFailedException("No bets have been placed");
		}

		roundState = GameRoundState.ROUND_STARTED;
		dealAction.setRoundStart();
		deal(dealAction);
	}

	/**
	 * Ends the round.
	 * @param action The action that caused the round end action.
	 * @throws ActionFailedException 
	 */
	private void endRound(GameAction action) throws ActionFailedException {
		if (roundState == GameRoundState.ROUND_STARTED){
			roundState = GameRoundState.ROUND_FINISHED;

			GameAction roundFinish = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
			for (BlackjackSeat seat : seats) {
				if (seat != null && seat.hasBet()) {
					seat.endRound(roundFinish, dealer);
				}
			}
		}
	}

	/**
	 * Validates an action request.
	 * @param request The GameActionRequest to validate
	 * @param results A Map used to save any feedback from the validation process. 
	 * In case the validation fails, please find the cause stored as a String in the ERROR field.
	 */
	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (roundState == GameRoundState.ROUND_FINISHED) {
			throw new ActionFailedException("Round has finished.");
		}

		if ((roundState == GameRoundState.ROUND_NOT_STARTED) && (!BET_ACTION.equals(request.getDescription())) && (!DEAL_ACTION.equals(request.getDescription()))) {
			throw new ActionFailedException("Round has not started yet.");
		}

		if (request.getSeatId() < 0 || request.getSeatId() >= seats.length) {
			throw new ActionFailedException("Invalid seat number!");
		}

		if (seats[request.getSeatId()] == null && (!BET_ACTION.equals(request.getDescription()))) {
			throw new ActionFailedException("Seat not assigned to anybody!");
		}

		if ((INSURANCE_ACTION.equals(request.getDescription()) || REJECT_ACTION.equals(request.getDescription()))) {
			if (!seats[request.getSeatId()].insurancePending()) {
				throw new ActionFailedException("Insurance was rejected or not offered at all.");
			}
			return;
		}

		if (!BET_ACTION.equals(request.getDescription()) && !DEAL_ACTION.equals(request.getDescription()) && request.getSeatId() != currSeatId) {
			throw new ActionFailedException("It's not players turn!");
		} 
	}

	/**
	 * Performs the requested action.
	 * @param player The player that requested the action.
	 * @param action The action to execute.
	 * @param results A Map used to save any feedback from the validation process. 
	 * In case the request fails, please find the cause, stored as a String, in the ERROR field. 
	 * Also find the card(s) drawn from the HIT, SPLIT or DOUBLE action in the cards_0 and cards_1 
	 * (OPTIONAL) fields.
	 */
	@Override
	protected void performAction(GameAction action) throws ActionFailedException {

		if (action.getSeatId() <= 0 || action.getSeatId() > seats.length) {
			throw new ActionFailedException("Invalid seat id");
		}

		if (BET_ACTION.equals(action.getDescription())) {
			performBet(action);
			return;
		}

		if (seats[action.getSeatId()] == null) {
			throw new ActionFailedException("Seat is not active for this round");
		}

		if (DEAL_ACTION.equals(action.getDescription())) {
			startRound(action);
		} else if (HIT_ACTION.equals(action.getDescription())){
			performHit(action);
		} else if (STAND_ACTION.equals(action.getDescription())){
			performStand(action);
		} else if (DOUBLE_ACTION.equals(action.getDescription())){
			performDouble(action);
		} else if (SPLIT_ACTION.equals(action.getDescription())){
			performSplit(action);
		} else if (INSURANCE_ACTION.equals(action.getDescription())){
			performInsurance(action);
		} else if (REJECT_ACTION.equals(action.getDescription())){
			performReject(action);
		} else if (EVEN_MONEY_ACTION.equals(action.getDescription())){
			performEvenMoney(action);
		}
	}

	/**
	 * Performs the BET action.
	 * @param action The bet action GameAction to update.
	 * @param results The Map to save any errors in the ERROR field.
	 */
	private void performBet(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
		}

		checkBet(action);

		if (action.isLoaded()){
			seats[action.getSeatId()] = new BlackjackSeat(action.getPlayer(), action.getSeatId());    
		} else {
			joinSeat(action.getPlayer(), action.getSeatId());
		}

		BlackjackSeat seat = seats[action.getSeatId()];
		seat.bet(action);

	}

	public boolean joinSeat(GamePlayer player, int seatId) throws ActionFailedException{
		if (seatId <= 0 || seatId >= seats.length || seats[seatId] != null) {
			throw new ActionFailedException("Cannot join this seat.");
		}

		seats[seatId] = new BlackjackSeat(player, seatId);
		return true;
	}

	/**
	 * Performs the HIT action.
	 * @param action The hit action GameAction to update.
	 * @param results The Map to save any feedback from the execution of the action. 
	 * If the action was successful, then a cards_0 field will exist holding the card drawn. 
	 * If there was an error it will be placed in the ERROR field.
	 */
	private void performHit(GameAction action) throws ActionFailedException {

		seats[action.getSeatId()].hit(this.roundDeck, action);

		if (seats[action.getSeatId()].isFinished()) {
			nextPlayer(action);
		}
		
		action.commit(isFinished());
	}

	/**
	 * Performs the STAND action.
	 * @param action The stand action GameAction to update.
	 * @param results The Map to save any errors in the ERROR field.
	 */
	private void performStand(GameAction action) throws ActionFailedException {

		seats[action.getSeatId()].stand(action);

		if (seats[action.getSeatId()].isFinished()){
			nextPlayer(action);
		}
		
		action.commit(isFinished());
	}

	/**
	 * Performs the DOUBLE action.
	 * @param action The double action GameAction to update.
	 * @param results The Map to save any feedback from the execution of the action. 
	 * If the action was successful, then a cards_0 field will exist holding the card drawn. 
	 * If there was an error it will be placed in the ERROR field.
	 */
	private void performDouble(GameAction action) throws ActionFailedException {

		seats[action.getSeatId()].doubleBet(this.roundDeck, action);

		if (seats[action.getSeatId()].isFinished()) {
			nextPlayer(action);
		}
		
		action.commit(isFinished());
	}

	/**
	 * Performs the SPLIT action.
	 * @param action The split action GameAction to update.
	 * @throws ActionFailedException 
	 */
	private void performSplit(GameAction action) throws ActionFailedException {

		seats[action.getSeatId()].split(this.roundDeck, action);

		if (seats[action.getSeatId()].isFinished()) {
			nextPlayer(action);
		}
		
		action.commit(isFinished());
	}

	/**
	 * Performs the INSURANCE action.
	 * @param action The insurance action GameAction to update.
	 * @throws ActionFailedException 
	 */
	private void performEvenMoney(GameAction action) throws ActionFailedException {
		seats[action.getSeatId()].evenMoney(action);
		nextPlayer(action);
		action.commit(isFinished());
	}

	/**
	 * Performs the INSURANCE action.
	 * @param action The insurance action GameAction to update.
	 * @param results The Map to save any feedback from the execution of the action.  
	 * If there was an error it will be placed in the ERROR field.
	 * @throws ActionFailedException 
	 */
	private void performInsurance(GameAction action) throws ActionFailedException{

		seats[action.getSeatId()].insurance(action);

		if (action.getSeatId() == currSeatId && seats[action.getSeatId()].isFinished()) {
			nextPlayer(action);
		}
		action.commit(isFinished());
	}

	/**
	 * Performs the REJECT action. REJECT is a fake action used only by the players who
	 * cannot participate in the round and should have the option of insurance.
	 * @param action The reject action GameAction to update.
	 * @throws ActionFailedException 
	 */
	private void performReject(GameAction action) throws ActionFailedException {
		action.setSaved();
		if (action.getSeatId() == currSeatId && seats[action.getSeatId()].insurancePending()) {
			seats[action.getSeatId()].rejectInsurance();
			nextPlayer(action);
			action.commit(isFinished());
		}
	}

	/**
	 * Returns the number of active seats in the round.
	 */
	private int getActiveSeatCount(){
		int count = 0;

		for (int i=1;i<seats.length;i++){
			BlackjackSeat seat = seats[i];
			if (seat != null)
				count++;
		}

		return count;
	}

	/**
	 * Gets the filter to use for filtering this round GameAction's. 
	 * @Return The GameActionFilter to use. 
	 */    
	@Override
	public GameActionFilter getActionFilter() {
		return new BlackjackActionFilter();
	}

	/**
	 * Stores any round-type-specific info into the Map provided.
	 */
	@Override
	protected void getExtraInfo(GamePlayer player, Map<String,Object> roundInfo, boolean firstTime) {
		if (roundInfo != null) {
			super.getExtraInfo(player, roundInfo, firstTime);

			roundInfo.put("curr_seat_id", currSeatId);
			if (seats[currSeatId] != null) {
				roundInfo.put("curr_hand", seats[currSeatId].getCurrentHand());
			}
		}
	}
}
