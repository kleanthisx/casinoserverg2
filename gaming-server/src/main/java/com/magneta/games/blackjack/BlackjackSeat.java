package com.magneta.games.blackjack;

import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;

/**
 * The BlackjackSeat holds the state and behaviors of any seat of a BlackjackRound.
 */
public class BlackjackSeat {

    private final GamePlayer player;
    private final int seatId;
    
    public final BlackjackHand[] hands;
    private int currHand;
    
    private double betAmount;

    private boolean insurancePending = false;
    private boolean insured = false;
    private boolean evenMoney = false;

    private boolean roundEnded = false;
        
    /**
     * Constructs a new BlackjackSeat
     * @param player The player that will occupy the seat.
     */
    public BlackjackSeat(GamePlayer player, int seatId) {
        this.seatId = seatId;
        this.player = player;
        hands =  new BlackjackHand[2];
        hands[0] = new BlackjackHand();
        
        betAmount = 0.0;
        currHand = 0;
    }
    
    public int getSeatId() {
        return seatId;
    }
    
    /**
     * Performs the bet action of the seat.
     * @param action The GameAction object to update.
     * @throws ActionFailedException
     */
    public void bet(GameAction action) throws ActionFailedException {
        if (this.betAmount > 0.0) {
            throw new ActionFailedException("User has already put a bet.");
        }

        if (action.getAmount() <= 0.001) {
            throw new ActionFailedException("Invalid bet amount");
        }

        action.commit();

        betAmount = action.getAmount();
    }
    
    private int getHand(GameAction action) throws ActionFailedException {	
    	try {
            int hand = Integer.parseInt(action.getArea());

            if (hand < 0  || hand >= hands.length) {
                throw new ActionFailedException("Invalid hand number");
            }

            return hand;
        } catch (NumberFormatException e) {
            throw new ActionFailedException("Invalid hand number.");
        }
    }

    /**
     * Performs the hit action of the seat.
     * @param round The round were the action will take place.
     * @param action The GameAction object to update.
     * @return The card drawn by the action.
     * @throws ActionFailedException
     */
    public void hit(Deck deck, GameAction action) throws ActionFailedException {
    	int handIndex = getHand(action);
    	
    	if (handIndex != currHand) {
    		throw new ActionFailedException("Invalid hand: Not the current hand, it should be "+currHand+"!");
    	}
    	
        BlackjackHand hand = hands[handIndex];
        if (hand == null) {
        	throw new ActionFailedException("Hand is not participating");
        }


        if (action.getPlayer() != null) {
            insurancePending = false;
        }
        
        hand.hit(deck, action);
        
        if (hand.isFinished()) {
        	nextHand();
        }
    }

    private boolean nextHand() {
        if (currHand < hands.length -1 && hands[currHand + 1] != null) {
        	currHand++;
        	return true;
        }
        
        return false;
    }
    
    /**
     * Performs the stand action of the seat.
     * @param action The GameAction object to update.
     * @throws ActionFailedException
     */
    public void stand(GameAction action) throws ActionFailedException {
    	int handIndex = getHand(action);
    	
    	if (handIndex != currHand) {
    		throw new ActionFailedException("Invalid hand: Not the current hand, it should be "+currHand+"!");
    	}
    	
        BlackjackHand hand = hands[handIndex];
        if (hand == null) {
        	throw new ActionFailedException("Hand is not participating");
        }
        
        hand.stand(action);
        insurancePending = false;

        nextHand();
    }
  
    public GameAction autoPlay(AbstractGameRound round) throws ActionFailedException {
    	GameAction standAction = null;
    	
    	for(;;) {
    		if (!hands[currHand].isFinished()) {
    			insurancePending = false;
    			hands[currHand].stand(standAction);
    			GameAction action = new GameAction(null, round, this.seatId, "g:STAND", Integer.toString(currHand), 0.0);
    			
    			if (standAction == null) {
    				standAction = action; 
    			} else {
    				standAction.addAdditionalAction(action);
    			}
    		}
    		
    		if (!nextHand()) {
    			break;
    		}
    	}
        
        return standAction;
    }
    
    /**
     * Performs the double action of the seat.
     * @param round The round were the action will take place.
     * @param action The GameAction object to update.
     * @return The card drawn by the action.
     * @throws ActionFailedException
     */
    public void doubleBet(Deck deck, GameAction action) throws ActionFailedException {
    	int handIndex = getHand(action);
    	
    	if (handIndex != currHand) {
    		throw new ActionFailedException("Invalid hand: Not the current hand, it should be "+currHand+"!");
    	}
    	
        BlackjackHand hand = hands[handIndex];
        if (hand == null) {
        	throw new ActionFailedException("Hand is not participating");
        }

        insurancePending = false;
        action.setAmount(betAmount);
        hand.doubleBet(deck, action);

        nextHand();
    }

    /**
     * Performs the split action of the seat.
     * @param round The round were the action will take place.
     * @param action The GameAction object to update.
     * @throws ActionFailedException.
     */
    public void split(Deck deck, GameAction action) throws ActionFailedException {
        insurancePending = false;

        if (hands[hands.length - 1] != null) {
            throw new ActionFailedException("Hand has already split");
        }

        action.setAmount(betAmount);
       
        hands[currHand+1] = hands[currHand].split(deck, action);
        
        if (hands[currHand].isFinished()) {
        	nextHand();
        }
    }

    /**
     * Performs the insurance action of the seat.
     * @param action The GameAction object to update.
     * @throws ActionFailedException.
     */
    public void insurance(GameAction action) throws ActionFailedException {
        
    	if (insured || evenMoney) {
    		 throw new ActionFailedException("Seat already insured.");
    	}
    	
        if (insurancePending == false) {
            throw new ActionFailedException("Dealer has not offered insurance.");
        }
        
        if ((insured == true) || (hands[0].getCards().size() > 2)) {
            throw new ActionFailedException("Seat is already insured or is not allowed.");
        }

        action.setAmount(this.betAmount / 2.0);        
        action.commit();
        insured = true;
        insurancePending = false;
    }

    /**
     * Performs the "even money" action of the seat.
     * @param action The GameAction object to update.
     * @throws ActionFailedException.
     */
    public void evenMoney(GameAction action) throws ActionFailedException {
        if (insured || evenMoney) {
            throw new ActionFailedException("Seat already insured");
        }

        if (!insurancePending) {
        	throw new ActionFailedException("You can only take even money if insurance is pending.");
        }

        if (!hands[0].hasBlackjack()) {
            throw new ActionFailedException("You can only take even money if you have blackjack.");
        }
        
        action.commit();
        evenMoney = true;
        insurancePending = false;
    }
    
    /** 
     * Ends the round for this seat and returns its earnings
     * @param dealer The round dealer.
     * @return The amount earned by this seat.
     */
    public void endRound(GameAction parentAction, BlackjackDealer dealer) {
        if (roundEnded){
            throw new RuntimeException("endRound() called multiple times");
        }
        roundEnded = true;
        
        if (this.evenMoney) {
            GameAction winAction = parentAction.createGeneratedAction(this.player, this.seatId, "WIN", null, betAmount * 2.0);
            winAction.setActionValue("EvenMoney");
            return;
        }
        
        if (insured && dealer.hasBlackjack()) {
        	GameAction winAction = parentAction.createGeneratedAction(this.player, this.seatId, "WIN", null, betAmount * 1.5);
            winAction.setActionValue("Insurance");
        }
 
        for (int i=0; i < hands.length; i++) {
        	if (hands[i] != null) {
        		StringBuilder buf = new StringBuilder();
        		double handWins = hands[i].calculateEarnings(dealer, betAmount, buf);
        		
        		if (handWins > 0.0) {
        			GameAction winAction = parentAction.createGeneratedAction(this.player, this.seatId, "WIN", Integer.toString(i), handWins);
                    winAction.setActionValue(buf.toString().trim());
        		}
        	}
        }
    }

    /**
     * Returns whether the seat has finished playing.
     * @return Whether the seat has finished playing.
     */
    public boolean isFinished() {
    	if (evenMoney) {
    		return true;
    	}
    	
    	for (int i=0; i < hands.length; i++) {
    		if (hands[i] != null && !hands[i].isFinished()) {
    			return false;
    		}
    	}
    	return true;
    }

    /**
     * Checks whether the seat has a bet.
     */
    public boolean hasBet(){
        return betAmount > 0.0;
    }

    /**
     * Gets the bet for this seat.
     */
    public double getBet(){
        return betAmount;
    }
    
    /**
     * Gets the player occupying this seat.
     * @return The player occupying this seat.
     */
    public GamePlayer getPlayer() {
        return player;
    }

    /**
     * Gets the currentHand index.
     * @return The currentHand index.
     */
    public int getCurrentHand(){
        return currHand;
    }
    
    /**
     * Returns whether both the seats hands have busted.
     * @return Whether both the seats hands have busted.
     */
    public boolean hasBusted(){
    	for (int i=0; i < hands.length; i++) {
    		if (hands[i] != null && !hands[i].hasBusted()) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    public void offerInsurance(){
        insurancePending = true;
    }
    
    public void rejectInsurance(){
        insurancePending = false;
    }
    
    public boolean insurancePending(){
        return insurancePending;
    }
}
