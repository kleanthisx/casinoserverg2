package com.magneta.games.blackjack;

import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;

/**
 * BlackjackDealer simulates the behaviour of the dealer of the blackjack game.
 */
public class BlackjackDealer {
    
    private List<Integer> cards;
    private boolean busted = false;
    private boolean finished = false;
    
    /**
     * Constructs a new BlackjackDealer. 
     */
    public BlackjackDealer()
    {
        cards = new ArrayList<Integer>();
    }
    
    /**
     * Performs the hit action of the Blackjack dealer.
     * @param round The round where the action takes place.
     * @param action The action object to update.
     */
    public void hit(Deck deck, GameAction action) {
    
        int nextCard = deck.getNextCard();
        cards.add(nextCard);
            
        action.setActionValue(String.valueOf(nextCard));
        
        if (sumCards() > 21){
            busted = true;   
        }
    }
    
    /**
     * Performs the stand action of the Blackjack dealer.
     * @param round The round where the action takes place.
     * @param action The action object to update.
     */
    private void stand(GameAction action) {
        finished = true;
         
        action.createGeneratedAction(null, 0, "g:STAND", null, 0.0);
    }
    
    /**
     * Plays the dealer.
     * @param round The round where the action takes place.
     * @param action The action object to update.
     * @param allBusted Whether all the rounds players have busted.
     */
    public void play(Deck deck, GameAction action, boolean allBusted){

        if (!allBusted)
        {
            GameAction flipAction = action.createGeneratedAction(null, 0, "g:FLIP", null, 0.0);
            flipAction.setActionValue(String.valueOf(this.cards.get(1)));

            while (sumCards() < 17){
                GameAction newHit = action.createGeneratedAction(null, 0,
                        "g:HIT", null, 0.0);
                hit(deck, newHit);
            }
        } else if (Card.getCardNumber(this.cards.get(0)) == Card.ACE) {
            GameAction flipAction = action.createGeneratedAction(null, 0, "g:FLIP", null, 0.0);
            flipAction.setActionValue(String.valueOf(this.cards.get(1)));
        }
        stand(action);
    }
    
    /**
     * Sums the dealers cards.
     * @return The sum of the dealers cards.
     */
    public int sumCards(){
        return BlackjackDeck.sumCards(cards);
    }
    
    /**
     * Returns whether the dealer has finished playing or not.
     * @return A boolean indicating whether the dealer has finished playing.
     */
    public boolean isFinished(){
        return finished;
    }
    
    /**
     * Returns whether the dealer has blackjack or not.
     * @return A boolean indicating whether the dealer has blackjack.
     */
    public boolean hasBlackjack(){
        return (cards.size() == 2 && sumCards() == 21);
    }
    
    /**
     * Returns whether the dealer has busted or not.
     * @return A boolean indicating whether the dealer has busted.
     */
    public boolean hasBusted(){
        return busted;
    }
    
    /**
     * Returns the dealer card at the given index.
     * @return The dealer card at the given index.
     */
    public boolean shouldOfferInsurance() {
        return Card.getCardNumber(cards.get(0)) == Card.ACE;
    }
}
