package com.magneta.games.blackjack;

import java.util.HashMap;
import java.util.Map;

import com.magneta.games.GameAction;
import com.magneta.games.GameActionFilter;

/**
 * BlackjackActionFilter implements the filtering of GameAction objects for the blackjack game.
 */
public class BlackjackActionFilter implements GameActionFilter {

    /**
     * Filters the action showing only the info that are necessary and applicable for the blackjack game.
     * @param The GameAction to filter.
     * @return Returns a Hashtable containing the following fields:<br>
     * 
     * <table border="1">
     * <tr><th>Field</th>       <th>Type</th>   <th>Description</th></tr>
     * <tr><td>round</td>       <td>int</td>    <td>The ID of the round</td></tr>
     * <tr><td>seat_id</td>     <td>int</td>    <td>The ID of the seat which performed the action.</td></tr>
     * <tr><td>action_desc</td> <td>String</td> <td>The description of the action.</td></tr>
     * <tr><td>action_area</td> <td>String</td> <td>The area of the action (OPTIONAL).</td></tr>
     * <tr><td>amount</td>      <td>double</td> <td>The amount the action may have returned or received (OPTIONAL).</td></tr>
     * <tr><td>action_value</td><td>String</td> <td>The outcome of the action when there is one (OPTIONAL).</td></tr>
     * </table>
     */
    @Override
    public Map<String, Object> filterAction(GameAction action) {
        
        if (action.getDescription().equals("GUARD")) {
            return null;
        }
        
        Map<String, Object> currAction = new HashMap<String, Object>();

        currAction.put(ROUND, action.getRoundId());
        currAction.put(SEAT_ID, action.getSeatId());
        currAction.put(ACTION, action.getDescription());
        if (action.getArea() != null) {
            currAction.put(ACTION_AREA, action.getArea());
        }
        
        if (action.getAmount() > 0.0){
            currAction.put(AMOUNT, action.getAmount());
        }
        
        if (action.getActionValue() != null) {

            if (action.getDescription().equals("g:HIDDEN"))
                currAction.put(ACTION_VALUE, "?");
            else
                currAction.put(ACTION_VALUE, action.getActionValue());
        }

        return currAction;
    }

}
