package com.magneta.games;

/**
 * DeckOutOfCardsException is thrown when a Deck object has no more cards.
 */
public class DeckOutOfCardsException extends RuntimeException {

    private static final long serialVersionUID = -8312271480683348964L;

    /**
     * Constructs a new DeckOutOfCardsException
     * @param msg The exception message to show
     */
    public DeckOutOfCardsException(String msg) {
        super(msg);
    }
}
