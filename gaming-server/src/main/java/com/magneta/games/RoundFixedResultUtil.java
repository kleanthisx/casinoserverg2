package com.magneta.games;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class RoundFixedResultUtil {

	private static final Logger log = LoggerFactory.getLogger(RoundFixedResultUtil.class);
	
	public static String getFixedResult(long tableId, int roundId) {
		if (!Config.getBoolean("server.game.fixed.allow", false)) {
			return null;
		}
		
		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null) {
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = dbConn.prepareStatement("SELECT result FROM game_fixed_results" +
					" WHERE table_id=? AND round_id=?");

			
			stmt.setLong(1, tableId);
			stmt.setInt(2, roundId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				return rs.getString("result");
			}
			
		} catch (SQLException e) {
			log.warn("Error while getting game fixed results", e);
			return null;
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(dbConn);
		}
		
		return null;
	}
}
