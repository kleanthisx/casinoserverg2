/**
 * 
 */
package com.magneta.games.virtualracing.dogs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.ActionFailedException;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.virtualracing.Runner;
import com.magneta.games.virtualracing.VirtualRacingRound;

@GameTemplate("VirtualDogRacing")
public class VirtualDogRacingRound extends VirtualRacingRound {

    protected static final int RACES_PER_SESSION = 10;
    protected static final int RUNNERS_PER_RACE = 6;
    
    private static final int[] BET_TYPES = {WIN_BET_TYPE, QUINELLA_BET_TYPE};

    @Override
	protected int getRacesPerSession() {
    	return RACES_PER_SESSION;
    }
    
    @Override
	protected int getRunnersPerRace() {
    	return RUNNERS_PER_RACE;
    }
    
    @Override
	protected int[] getEnabledBetTypes() {
    	return BET_TYPES;
    }
    
    @Override
	public String getRunnerType() {
    	return "dog";
    }
    
    @Override
    protected List<Runner> findRunners() throws ActionFailedException {
    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new ActionFailedException("Out of database connections");
    	}

    	List<Runner> dogs = new ArrayList<Runner>(RUNNERS_PER_RACE);

    	try {
    		PreparedStatement stmt = null;
    		ResultSet res = null;

    		String sql = 
    				"SELECT dog_id, dog_name"+
    						" FROM dogs" +
    						" ORDER BY random()" +
    						" LIMIT ?";
    		try {
    			stmt = dbConn.prepareStatement(sql);
    			stmt.setInt(1, RUNNERS_PER_RACE);

    			res = stmt.executeQuery();

    			while (res.next()) {
    				dogs.add(new Runner(res.getInt("dog_id"), res.getString("dog_name")));
    			}

    		} catch (SQLException e) {
    			throw new ActionFailedException("Unable to load runner data",e);
    		} finally {
    			DbUtil.close(res);
    			DbUtil.close(stmt);
    		}


    		for (Runner r: dogs) {
    			loadRunnerData(dbConn, r);
    		}

    	} finally {
    		DbUtil.close(dbConn);
    	}

    	return dogs;
    }

    @Override
    protected Runner loadRunner(int id) throws ActionFailedException {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try
        {            
            String sql = 
                "SELECT dog_name" +
                " FROM dogs" +
                " WHERE dog_id = ?";

            statement = dbConn.prepareStatement(sql);
            statement.setInt(1, id);

            res = statement.executeQuery();

            String name = null;
            if (res.next()){
                name = res.getString("dog_name");
            }
            Runner runner = new Runner(id, name);
            loadRunnerData(dbConn, runner);
            
            return runner;
        } catch (SQLException e) {
            logError("Error while loading dog.",e);
            throw new ActionFailedException("Error while loading runner");
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
    }
}
