/**
 * 
 */
package com.magneta.games.virtualracing;


/**
 * @author User
 *
 */
public class BetDescription {
        private final int betType;
        private final int[] betEntities;
        private final double betOdds;

        public BetDescription(final int betType, final int[] betHorses, final double betOdds) {
            this.betType = betType;
            this.betEntities = betHorses;
            this.betOdds = betOdds;
        }

        public int[] getBetEntities() {
            return betEntities;
        }

        public double getBetOdds() {
            return betOdds;
        }

        public String getOddsString() {
            int div = gcd(100, (int)((this.betOdds - 1.0) * 100.0));
            return (((int)((this.betOdds - 1.0) * 100.0)) / div)+" - "+(100/div);
        }
        
        private int gcd(int x, int y) {
            if (y != 0) 
                return gcd (y, x % y);
            
            return Math.abs(x);
        }
        
        public int getBetType() {
            return betType;
        }
}
