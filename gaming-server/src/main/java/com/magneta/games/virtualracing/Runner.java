package com.magneta.games.virtualracing;

public class Runner {

	private int id;
	private String name;
	
	private double score;
	private int[][] historicalData;
	
	public Runner(int id, String name) {
        this.id = id;
        this.name = name;
    }
	
	public int[][] getHistoricalData() {
        return historicalData;
    }
   
    public void setHistoricalData(int[][] historicalData) {
    	this.historicalData = historicalData;
    	int totalRunners = 0;
    	
    	score = 0.0;
    	for (int i=0;i<historicalData.length;i++){
    		score += ((historicalData[i][1]+1) - historicalData[i][0]);
    		totalRunners += historicalData[i][1];
    	}
    	score /= totalRunners;
    }
    
    public double getScore() {
    	return score;
    }
    
    public String getLastFiveResultsString() {
        String str = "";
        if (historicalData != null){
            for (int i=0;i<5;i++){
                if (i == 0){
                    str += historicalData[i][0];
                } else {
                    str += "-"+historicalData[i][0];
                }
            }
        }
        return str;
    }
    
    public int getId() {
    	return id;
    }
    
    public String getName() {
    	return name;
    }
    
    @Override
    public String toString(){
        return
        "\nId:     "+this.id+
        "\nName:   "+this.name;
    }
}
