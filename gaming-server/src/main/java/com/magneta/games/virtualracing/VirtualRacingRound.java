/**
 * 
 */
package com.magneta.games.virtualracing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionFilter;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;

public abstract class VirtualRacingRound extends AbstractGameRound {

    private static final String START_RACE_ACTION = "g:START_RACE";
    private static final String RUNNER_JOIN_ACTION = "g:RUNNER_JOIN"; 
    private static final String RUNNER_RANK_ACTION = "g:RUNNER_RANK";
    private static final String BETS_ACTION = "g:BETS";
    
    protected static final int WIN_BET_TYPE = 0;
    protected static final int QUINELLA_BET_TYPE = 1;
    protected static final int EXACTA_BET_TYPE = 2;

    private static final double MIN_ODDS = 1.05;
    private static final double MAX_ODDS = 999.00;
    
    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
        /*                              Description        Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,        ParameterType.REQUIRED,  ParameterType.REQUIRED),
        new GameActionRequestDefinition(BETS_ACTION,       ParameterType.REQUIRED,  ParameterType.FORBIDDEN),
        new GameActionRequestDefinition(START_RACE_ACTION, ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
    };
    
    private int raceNo;
    private final Runner[] runners;
    private double odds_prorit_rate;
    
    private BetDescription[] betsOdds;
    private GameRoundState roundState;
    private double[] bets;
    private boolean placedBet;
    
    public VirtualRacingRound() {
        this.runners = new Runner[getRunnersPerRace()];
        this.roundState = GameRoundState.ROUND_NOT_STARTED;
        this.bets = null;
        this.betsOdds = null;
        this.placedBet = false;
    }
    
    @Override
	protected void roundInit() throws ActionFailedException {
    	raceNo = getId() % getRacesPerSession();
        odds_prorit_rate = options.getGameSettings().get("odds_gain_rate", Double.TYPE);       
        
		initRace();
    }

    protected abstract int getRacesPerSession();
    protected abstract int getRunnersPerRace();
    protected abstract int[] getEnabledBetTypes();
    protected abstract List<Runner> findRunners() throws ActionFailedException;
    protected abstract Runner loadRunner(int id) throws ActionFailedException;
    protected abstract String getRunnerType();

    private boolean isBetEnabled(int betType){
    	int[] betTypes = getEnabledBetTypes();
        for (int bet: betTypes){
            if (bet == betType){
                return true;
            }
        }
        
        return false;
    }
    
    private void initRace() throws ActionFailedException {

    	GameAction firstJoin = null;

    	try {            
    		//STEP 1: Find the available horses
    		List<Runner> runnerList = findRunners();

    		if (runnerList.size() < this.getRunnersPerRace()) {
    			throw new ActionFailedException("Not enough runners for the race.");
    		}
    		for (int i=0; i < getRunnersPerRace(); i++) {
    			runners[i] = runnerList.get(i);

    			GameAction dogJoin = new GameAction(null, this, 0, RUNNER_JOIN_ACTION, String.valueOf(i), 0.0);
    			dogJoin.setActionValue(String.valueOf(runners[i].getId()));

    			if (firstJoin == null){
    				firstJoin = dogJoin;
    			} else {
    				firstJoin.addAdditionalAction(dogJoin);
    			}
    		}
    	} catch (ActionFailedException e) {
    		throw new ActionFailedException("Error while initializing race.", e);
    	}
    	
    	if (firstJoin == null) {
    		throw new ActionFailedException("Error while initializing race.");
    	}

    	firstJoin.commit();
    	actionCompleted(firstJoin);
    	calculateOdds();
    }
    
    private void calculateOdds() {
        //calculate length of all possible combinations
        int numBets = 0;
        
        boolean winTypeEnabled = isBetEnabled(WIN_BET_TYPE);
        boolean quinellaTypeEnabled = isBetEnabled(QUINELLA_BET_TYPE);
        boolean exactaTypeEnabled = isBetEnabled(EXACTA_BET_TYPE);
        
        if (winTypeEnabled){
            numBets += runners.length;
        }
        
        if (quinellaTypeEnabled){
            for (int i=1;i<runners.length;i++){
                numBets += i;
            }
        }
        
        if (exactaTypeEnabled){
            numBets += (runners.length * (runners.length -1));
        }
        
        betsOdds = new BetDescription[numBets];
        bets = new double[numBets]; 

        double scoresSum = 0.0;
        for (Runner entity: runners){
            scoresSum += entity.getScore();
        }
     
        double probabilities[] = new double[runners.length];
        
        for (int i=0;i<runners.length;i++){
            probabilities[i] = (runners[i].getScore() / scoresSum);
            logDebug("Entity "+i+" probability: "+probabilities[i]);
        }        
        
        int k=0;
        if (winTypeEnabled){
            for (int i=0;i<runners.length;i++,k++){
                double winOdds = (1.0/probabilities[i]) * (1.0 - odds_prorit_rate);
                
                if (winOdds < MIN_ODDS){
                    winOdds = MIN_ODDS;
                } else if (winOdds > MAX_ODDS){
                    winOdds = MAX_ODDS;
                }
                
                /* round odds*/
                winOdds = Math.floor(winOdds);
                
                betsOdds[i] = new BetDescription(WIN_BET_TYPE, new int[]{i}, winOdds);
                logDebug("Entity "+i+" odds: "+betsOdds[i].getBetOdds());
            }
        }
        
        if (quinellaTypeEnabled){
            for (int i=0;i<runners.length;i++){
                for (int j=i+1;j<runners.length;j++,k++){
                    double props = ((probabilities[i] * probabilities[j]) / (1 - probabilities[j])) + ((probabilities[j] * probabilities[i]) / (1 - probabilities[i]));
                    logDebug("Quinella combination "+i+"-"+j+" probabilities: "+props);
                    
                    double finalOdds = (1 / props) * (1 - odds_prorit_rate);
                    
                    if (finalOdds < MIN_ODDS){
                        finalOdds = MIN_ODDS;
                    } else if (finalOdds > MAX_ODDS){
                        finalOdds = MAX_ODDS;
                    }
                    
                    /*round odds*/
                    finalOdds = Math.floor(finalOdds);
                    
                    betsOdds[k] = new BetDescription(QUINELLA_BET_TYPE, new int[]{i,j}, finalOdds);
                    logDebug("Quinella combination "+i+"-"+j+" odds: "+betsOdds[k].getBetOdds());
                }
            }
        }
        
        if (exactaTypeEnabled){
            for (int i=0;i<runners.length;i++){
                for (int j=0;j<runners.length;j++){
                    if (i == j){
                        continue;
                    }
                    
                    double props = ((probabilities[i] * probabilities[j]) / (1 - probabilities[j]));
                    logDebug("Exacta combination "+i+"-"+j+" probabilities: "+props);
                    
                    double finalOdds = (1 / props) * (1 - odds_prorit_rate);
                    
                    if (finalOdds < MIN_ODDS){
                        finalOdds = MIN_ODDS;
                    } else if (finalOdds > MAX_ODDS){
                        finalOdds = MAX_ODDS;
                    }
                    
                    /*round odds*/
                    finalOdds = Math.floor(finalOdds);
                    
                    betsOdds[k] = new BetDescription(EXACTA_BET_TYPE, new int[]{i,j}, finalOdds);
                    logDebug("Exacta combination "+i+"-"+j+" odds: "+betsOdds[k].getBetOdds());
                    k++;
                }
            }
        }
    }

    @Override
    public boolean isFinished() {
        return (roundState == GameRoundState.ROUND_FINISHED);
    }
    
    @Override
    protected final void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            if (action.isLoaded() && (betsOdds == null)){
                calculateOdds();
            }
            performBet(action);
        } if (START_RACE_ACTION.equals(action.getDescription())){
            action.setRoundStart();
            startRound(action);
        } else if (BETS_ACTION.equals(action.getDescription())){
            performBets(action);
        }
    }

    @Override
    protected final void loadAction(GameAction action) throws ActionFailedException {
        if (RUNNER_JOIN_ACTION.equals(action.getDescription())){
            try{
                int i = Integer.parseInt(action.getArea());
                int id = Integer.parseInt(action.getActionValue());
                logDebug("Loading entity with ID="+id+" to index="+i);
                
                runners[i] = loadRunner(id);

                logDebug("Entities "+runners[i]+" was loaded.");
            } catch (NumberFormatException e){
                logError("Loaded action area or action value is invalid.",e);
                throw new ActionFailedException("Loaded action area or action value is invalid.",e);
            }
        } else {
            doAction(action);
        }
    }
    
    public static final void calculateRace(Random random, Runner[] entities, int[] raceResults) {
        double scoresSum = 0.0;
        for (int i=0;i<entities.length;i++){
            scoresSum += entities[i].getScore();
        }

        double probability;
        int horseScores[][] = new int[entities.length][2];

        for (int i=0;i<entities.length;i++){
            probability = (entities[i].getScore() / scoresSum);
            
            horseScores[i][0] = i;
            
            double boost = Math.log((random.nextDouble() * (Math.E-1)) + 1);
            double antiBoost = Math.log((random.nextDouble() * (Math.E-1)) + 1);
            probability *= boost/antiBoost;
            
            horseScores[i][1] += (int)(probability * 1000000.0);
        } 

        /* Sort the scores */
        for (int i=0;i< horseScores.length;i++){
            for (int j=i+1; j < horseScores.length; j++) {
                if (horseScores[j][1] > horseScores[i][1]) {
                    int tmp[] = horseScores[i];
                     
                    horseScores[i] = horseScores[j];
                    horseScores[j] = tmp;
                }
            }
        }
        
        for (int i=0; i < horseScores.length; i++) {
            raceResults[i] = horseScores[i][0];
            //logDebug("Position " + i + ": " + raceResults[i] + " Score: " + horseScores[i][1]);
        }
    }
    
    private void updateRunnerHistory(Runner runner, Connection dbConn, int currPos) throws SQLException {
        PreparedStatement statement2 = null;
        try {
            statement2 = dbConn.prepareStatement(
                    "SELECT update_"+getRunnerType()+"_history(?,?,?,?)");

            statement2.setInt(1, runner.getId());
            statement2.setLong(2, getTableId());
            statement2.setInt(3, currPos);
            statement2.setInt(4, getRunnersPerRace());
            statement2.executeQuery();
        } finally {
            DbUtil.close(statement2);
        }
    }
    
    private int[][] loadRunnerBootHistoricalData(Runner runner, Connection dbConn) throws SQLException {
        PreparedStatement statement4 = null;
        ResultSet res3 = null;
        
        try {
            statement4 = dbConn.prepareStatement(
                    "SELECT pos, contestants" +
                    " FROM boot_"+getRunnerType()+"_races" +
                    " WHERE table_id = ?" +
                    " AND "+getRunnerType()+"_id = ?" +
                    " ORDER BY race_no DESC");            
            statement4.setLong(1, getTableId());
            statement4.setInt(2, runner.getId());
            
            res3 = statement4.executeQuery();
    
            int[][] data = null;
            int i = 0;
            
            while (res3.next()){
                if (data == null){
                    data = new int[10][2];
                }
                data[i][0] = res3.getInt("pos");
                data[i][1] = res3.getInt("contestants");
                i++;
            }
            return data;
        } finally {
            DbUtil.close(res3);
            DbUtil.close(statement4);
        }
    }
    
    protected void loadRunnerData(Connection conn, Runner runner) throws ActionFailedException {
    	
    	int[][] historicalData;
    	
    	try {  
    		historicalData = loadRunnerBootHistoricalData(runner, conn);
    		if (historicalData == null){
    			historicalData = generateRunnerBootRaces(conn, runner);
    		}
    	} catch (SQLException e) {
    		logError("Error while getting runner statistics.",e);
    		throw new ActionFailedException("Error while loading statistics");
    	}

    	for (int pass=1; pass < historicalData.length; pass++) {
    		for (int i=0; i < historicalData.length-pass; i++) {
    			if ((historicalData[i][0] / historicalData[i][1]) > 
    			(historicalData[i+1][0] / historicalData[i+1][1])) {

    				int tmpRank = historicalData[i][0];
    				int tmpHorses = historicalData[i][1];
    				historicalData[i][0] = historicalData[i+1][0];
    				historicalData[i][1] = historicalData[i+1][1];
    				historicalData[i+1][0] = tmpRank;
    				historicalData[i+1][1] = tmpHorses;
    			}
    		}
    	}

    	runner.setHistoricalData(historicalData);
    }
    
    private int[][] generateRunnerBootRaces(Connection dbConn, Runner runner) throws SQLException {
    	int runnersPerRace = getRunnersPerRace();
    	
        int avgPos = (1 + getRandom().nextInt(runnersPerRace));
        
        PreparedStatement statement3 = null;
        try {
           
            statement3 = dbConn.prepareStatement(
                "INSERT INTO boot_"+getRunnerType()+"_races(table_id, "+getRunnerType()+"_id, race_no, pos, contestants)" +
                " VALUES (?,?,?,?,?)");
    
            int[][] data = new int[10][2];
            
            for (int j =0;j<data.length;j++){
                int lowest = (avgPos > 2 ? avgPos-2 : 1);
                int variation = ((lowest + 4) > runnersPerRace ? (runnersPerRace - lowest) : 4);
    
                data[j][0] = lowest + getRandom().nextInt(variation);
                data[j][1] = runnersPerRace;
    
                statement3.setLong(1, getTableId());
                statement3.setInt(2, runner.getId());
                statement3.setInt(3, j);
                statement3.setInt(4, data[j][0]);
                statement3.setInt(5, data[j][1]);
    
                statement3.execute();
            }

            return data;
        } finally {
            DbUtil.close(statement3);
        }
    }
    
    private void startRound(GameAction action) throws ActionFailedException {
    	if (!placedBet) {
    		throw new ActionFailedException("Invalid state: No bets placed yet!");
    	}

    	if (roundState != GameRoundState.ROUND_NOT_STARTED) {
    		throw new ActionFailedException("Round already started or has finished.");
    	}

    	this.roundState = GameRoundState.ROUND_STARTED;

    	int[] raceResults = new int[runners.length];
    	calculateRace(getRandom(), runners, raceResults);

    	/*FIXED ODD CODE*/
    	com.magneta.games.fixedodd.VirtualRacingGuard.guardRound(game.getGameId(), options.getOwner(), runners, raceResults, bets, betsOdds, getRandom(), options.getGameSettings(), action);


    	for (int i=0;i<raceResults.length;i++) {
    		GameAction runnerRank = action.createGeneratedAction(null, 0, RUNNER_RANK_ACTION, String.valueOf(i+1), 0.0);
    		runnerRank.setActionValue(String.valueOf(raceResults[i]));
    	}

    	for (int j = 0; j< betsOdds.length;j++){
    		switch(betsOdds[j].getBetType()){
    		case WIN_BET_TYPE:
    			if ((bets[j] > 0.0) && (raceResults[0] == betsOdds[j].getBetEntities()[0])){
    				double winAmount = betsOdds[j].getBetOdds() * bets[j];
    				GameAction winAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, WIN_BET_TYPE+" "+betsOdds[j].getBetEntities()[0], winAmount);
    				winAction.setActionValue(String.valueOf(betsOdds[j].getBetOdds()));
    			}
    			break;

    		case QUINELLA_BET_TYPE:
    			if ((bets[j] > 0.0) && ((raceResults[0] == betsOdds[j].getBetEntities()[0] && raceResults[1] == betsOdds[j].getBetEntities()[1]) ||
    					(raceResults[0] == betsOdds[j].getBetEntities()[1] && raceResults[1] == betsOdds[j].getBetEntities()[0]))){
    				double winAmount = betsOdds[j].getBetOdds() * bets[j];
    				GameAction winAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, QUINELLA_BET_TYPE+" "+betsOdds[j].getBetEntities()[0]+"-"+betsOdds[j].getBetEntities()[1], winAmount);
    				winAction.setActionValue(String.valueOf(betsOdds[j].getBetOdds()));
    			}
    			break;
    		case EXACTA_BET_TYPE:
    			if ((bets[j] > 0.0) && ((raceResults[0] == betsOdds[j].getBetEntities()[0] && raceResults[1] == betsOdds[j].getBetEntities()[1]))){
    				double winAmount = betsOdds[j].getBetOdds() * bets[j];
    				GameAction winAction = action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, EXACTA_BET_TYPE+" "+betsOdds[j].getBetEntities()[0]+"-"+betsOdds[j].getBetEntities()[1], winAmount);
    				winAction.setActionValue(String.valueOf(betsOdds[j].getBetOdds()));
    			}
    			break;
    		}
    	}


    	action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);

    	action.commit(true);
    	
    	
    	Connection conn = ConnectionFactory.getConnection();
    	try {
    		conn.setAutoCommit(false);
    		for (int i=0;i < raceResults.length;i++){
    			updateRunnerHistory(runners[raceResults[i]], conn, i+1);
    		}
    		conn.commit();
    	} catch (SQLException e) {
    		try {
    			conn.rollback();
    		} catch (SQLException e1) {
    			logError("Error rolling back.", e1);
    		}
    		this.logError("Error updating entity history.", e);
    		throw new ActionFailedException("Error while updating statistics", e);
    	} finally {
    		DbUtil.close(conn);
    	}
    	
    	this.roundState = GameRoundState.ROUND_FINISHED;
    }

    /**
     * Performs the action of the player betting on a particular horse to win.
     * @param action The game action request.
     * @throws ActionFailedException 
     */
    private void performBet(GameAction action) throws ActionFailedException {
        if (roundState != GameRoundState.ROUND_NOT_STARTED) {
            throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
        }
        
        checkBet(action);

        int betIndex = parseArea(action.getArea());

        if (bets[betIndex] > 0.0) {
            throw new ActionFailedException("Already placed bet on entity.");
        }

        action.commit();
        bets[betIndex] = action.getAmount();
        this.placedBet = true;
    }

    /**
     * Performs the action of the player betting on a number of entities.
     * @param action The game action request.
     * @throws ActionFailedException 
     */
    private void performBets(GameAction action) throws ActionFailedException {
        if (roundState != GameRoundState.ROUND_NOT_STARTED) {
            throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
        }

        String[] betsArea = action.getArea().split(";");
        
        if (betsArea.length == 0) {
            throw new ActionFailedException("Invalid action format");
        }
        
        action.setSaved();
        double[] tmpBets = new double[bets.length];
        
        for (String bet: betsArea){
            if (bet.matches("\\d+ \\d+[[_]\\d+]* \\d+(\\.\\d+)?")){
                String area = bet.substring(0,bet.lastIndexOf(" ")).replaceFirst("_", "-");
                double amount = Double.parseDouble(bet.substring(bet.lastIndexOf(" ")+1));
                
                if ((!action.isLoaded()) && 
                        ((amount < options.getMinBet()) || (amount > options.getMaxBet()))) {
                    throw new ActionFailedException("Amount not within min/max bet");
                }
                
                int betIndex = parseArea(area);
                if (tmpBets[betIndex] > 0.0 || bets[betIndex] > 0.0){
                    throw new ActionFailedException("Already placed bet on entity.");
                }

                action.addAdditionalAction(new GameAction(action.getPlayer(), this,1, BET_ACTION, area, amount));
                tmpBets[betIndex] = amount;
                
            } else {
                throw new ActionFailedException("Invalid area format.");
            }
        }
        
        GameAction startRace = new GameAction(action.getPlayer(),this,1,START_RACE_ACTION, "", 0.0);
        startRace.setRoundStart();
        action.addAdditionalAction(startRace);
    
        action.commit();
        
        for (int i=0;i<bets.length;i++){
            if (tmpBets[i] > 0.0){
                bets[i] = tmpBets[i];
            }
        }
        this.placedBet = true;
        
        startRound(startRace);
    }
    
    private int parseArea(String area) throws ActionFailedException {
    	if (!area.matches("\\d+ \\d+[[-]\\d+]*")){
    		throw new ActionFailedException("Area not formatted correctly.");
    	}

    	int betType = Integer.parseInt(area.substring(0, area.indexOf(" ")));
    	String areaCpy = area.substring(area.indexOf(" ")+1);

    	switch (betType) {
    		case WIN_BET_TYPE: 
    			if (areaCpy.indexOf("-") > 0) {
    				throw new ActionFailedException("Invalid area: more than one entity specified.");
    			}

    			int horseIndex = Integer.parseInt(areaCpy);

    			if (horseIndex < 0 || horseIndex > (runners.length-1)) {
    				throw new ActionFailedException("Invalid area: entity index out of range.");
    			}
    			
    			return horseIndex;

    		case QUINELLA_BET_TYPE: {
    			
    			StringTokenizer tokenizer = new StringTokenizer(areaCpy, "-");

    			int tokens = tokenizer.countTokens();

    			if (tokens != 2) {
    				throw new ActionFailedException("Invalid area: two entities must be specified.");
    			} 

    			int h1Index = Integer.parseInt(tokenizer.nextToken());
    			int h2Index = Integer.parseInt(tokenizer.nextToken());

    			if (h1Index > h2Index) {
    				int tmp = h1Index;
    				h1Index = h2Index;
    				h2Index = tmp;
    			}

    			if (h1Index < 0 || h1Index > (runners.length-2) || h2Index < 0 || h2Index > (runners.length-1)) {
    				throw new ActionFailedException("Invalid area: entity index out of range.");
    			} else if (h1Index == h2Index) {
    				throw new ActionFailedException("Invalid area: the same entity specified twice.");
    			}

    			int shift = 0;
    			if (isBetEnabled(WIN_BET_TYPE)) {
    				shift += runners.length;
    			}

    			for (int i=h1Index-1;i>=0;i--) {
    				shift += ((runners.length-1) - i);
    			}
    			return (shift + (h2Index-h1Index-1)); 
    			

    		}
    		case EXACTA_BET_TYPE:{
    			StringTokenizer tokenizer = new StringTokenizer(areaCpy, "-");

    			int tokens = tokenizer.countTokens();

    			if (tokens != 2) {
    				throw new ActionFailedException("Invalid area: two entities must be specified.");
    			} 

    			int h1Index = Integer.parseInt(tokenizer.nextToken());
    			int h2Index = Integer.parseInt(tokenizer.nextToken());

    			if (h1Index < 0 || h1Index > (runners.length-1) || h2Index < 0 || h2Index > (runners.length-1)){
    				throw new ActionFailedException("Invalid area: entity index out of range.");
    			} else if (h1Index == h2Index) {
    				throw new ActionFailedException("Invalid area: the same entity specified twice.");
    			}

    			int shift = 0;
    			if (isBetEnabled(WIN_BET_TYPE)){
    				shift += runners.length;
    			}
    			if (isBetEnabled(QUINELLA_BET_TYPE)){
    				for (int i=1;i<runners.length;i++){
    					shift += i;
    				}
    			}

    			return (shift + (h1Index * (runners.length-1)) + h2Index +(h2Index > h1Index? -1: 0)); 
    		}
    	default: 
    		throw new ActionFailedException("Invalid area: invalid bet type.");
    	}
    }


    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {       
        /* Check if the action is "syntacticaly" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (roundState == GameRoundState.ROUND_FINISHED) {
            throw new ActionFailedException("Round has finished.");
        }
    }

    @Override
    protected void loadFinished(GamePlayer player) {
        if ((betsOdds == null) && (runners != null)){
            calculateOdds();
        }
    }
    
    @Override
    protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
    	super.getExtraInfo(player, roundInfo, firstTime);

    	String raceHorses = "";

    	for (int i=0;i<runners.length;i++){
    		raceHorses += runners[i].getName()+":"+runners[i].getLastFiveResultsString()+";";
    	}

    	DecimalFormat f = new DecimalFormat("0.00");
    	DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    	symbols.setDecimalSeparator('.');
    	symbols.setGroupingSeparator(',');
    	f.setDecimalFormatSymbols(symbols);

    	StringBuilder oddsBuf = new StringBuilder();
    	
    	for (BetDescription betOdd: betsOdds) {
    		oddsBuf
    			.append(betOdd.getBetType())
    			.append(' ');

    		for (int j=0; j< betOdd.getBetEntities().length; j++) {
    			if (j > 0) {
    				oddsBuf.append("_").append(betOdd.getBetEntities()[j]);
    			}
    			oddsBuf.append(betOdd.getBetEntities()[j]);
    		}

    		oddsBuf
    			.append(' ')
    			.append(f.format(betOdd.getBetOdds()))
    			.append(";");
    	}

    	if (firstTime && getId() > 0) {
    		Connection dbConn = ConnectionFactory.getConnection();
    		PreparedStatement statement = null;
    		ResultSet res = null;
    		String lastResStr = null;

    		try{
    			String sql = 
    					"SELECT action_area::integer as pos, action_value::integer as h_index"+
    							" FROM game_round_actions" +
    							" WHERE table_id = ?" +
    							" AND round_id = ?"+
    							" AND action_desc = ?" +
    							" ORDER BY pos" +
    							" LIMIT 2";

    			statement = dbConn.prepareStatement(sql);
    			statement.setLong(1, getTableId());
    			statement.setInt(2, getId() - 1);
    			statement.setString(3, RUNNER_RANK_ACTION);

    			res = statement.executeQuery();
    			boolean first = true;

    			while (res.next()){
    				if (first){
    					lastResStr = String.valueOf(res.getInt("h_index") + 1);
    					first = false;
    				} else {
    					lastResStr += "-"+(res.getInt("h_index") + 1);
    				}
    			}

    			if (lastResStr != null){
    				roundInfo.put("last_results", lastResStr);
    			}
    		} catch (SQLException e) {
    			logError("Error while getting last round results.", e);
    		} finally{
    			DbUtil.close(res);
    			DbUtil.close(statement);
    			DbUtil.close(dbConn);
    		}
    	}

    	roundInfo.put("race_no", (this.raceNo+1));            
    	roundInfo.put("runners", raceHorses);
    	roundInfo.put("odds", oddsBuf.toString());
    }
    
    @Override
    public GameActionFilter getActionFilter() {
        return new VirtualRacingActionFilter();
    }
}
