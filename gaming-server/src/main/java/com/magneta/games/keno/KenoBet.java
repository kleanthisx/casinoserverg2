package com.magneta.games.keno;

public class KenoBet {

	private double amount;
	private int[] numbersToHit;

	public KenoBet(double p_amount,int[] p_numbersToHit) {
		this.amount = p_amount;
		this.numbersToHit = p_numbersToHit;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAmount() {
		return amount;
	}

	public void setNumbersToHit(int[] numbersToHit) {
		this.numbersToHit = numbersToHit;
	}
	public int[] getNumbersToHit() {
		return numbersToHit;
	}
	
	public String getNumbersToHitToString() {
		String result ="";
		boolean first = true;
		for (int i = 0; i < numbersToHit.length; i++) {
			if(!first){
				result+=" ";
			}
			first = false;
			result+=Integer.toString(numbersToHit[i]);
		}
		return result;
	}

	public static KenoBet parse(String betStr) {

		String[] betPieces = betStr.split(":");
		double p_amount = Double.parseDouble(betPieces[1]);
		String[] _numbers = betPieces[0].split(" ");
		int[] p_numbers = new int[_numbers.length];
		for (int i = 0; i < p_numbers.length; i++) {
			p_numbers[i] = Integer.parseInt(_numbers[i]);
		}

		KenoBet bet = new KenoBet(p_amount, p_numbers);

		return bet;
	}

	public boolean[] hasWon(int[] picks) {
		boolean[] result = new boolean[numbersToHit.length];
		for (int j = 0; j < numbersToHit.length; j++) {
			for (int i = 0; i < picks.length; i++) {
				if(numbersToHit[j]==picks[i]){
					result[j]=true;
					break;
				}
				result[j]=false;
			}
		}
		return result;
	}

}
