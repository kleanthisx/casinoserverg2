package com.magneta.games.keno;

/**
 * DeckOutOfCardsException is thrown when a pot object has no more balls.
 */
public class PotOutOfBallsException extends Exception {

	private static final long serialVersionUID = -5880858145264949488L;

		/**
	     * Constructs a new DeckOutOfCardsException
	     * @param msg The exception message to show
	     */
	    public PotOutOfBallsException(String msg) {
	        super(msg);
	    }
	}