package com.magneta.games.keno;

import java.util.List;
import java.util.Random;

/**
 * The Deck class represents a deck of cards for any card-based game.
 */
public class Pot {

	private final int[] balls;
	private int currBall;

	/**
	 * The constant number of cards per race in the deck.
	 */
	public static final int CARDS_PER_RACE = 13;
	/**
	 * The constant number of cards per deck.
	 */
	public static final int CARDS_PER_DECK = CARDS_PER_RACE * 4;

	/**
	 * Constructs a new Deck of cards. 
	 * @param numOfBalls The number of stacks to have in the deck.
	 * @param jokers A flag indicating whether the deck should or should not have jokers.
	 */
	public Pot(int numOfBalls) {

		balls = new int[numOfBalls];
		currBall = 0;
		
		for (int i=0; i < numOfBalls; i++) {
			balls[i] = new Integer(i+1);
		}
	}

	/**
	 * Shuffles the balls in the pot.
	 * @param sr The Random object to be used to shuffle the deck's cards. 
	 */
	public void shuffleBalls(Random sr) {

		long[] ballRank = new long[balls.length];

		for (int rank=0; rank < ballRank.length; rank++) {
			ballRank[rank] = sr.nextLong();
		}

		for (int i=0; i < balls.length; i++) {
			for (int j=i+1; j < balls.length; j++) {
				if (ballRank[i] < ballRank[j]) {
					long tmp = ballRank[i];
					ballRank[i] = ballRank[j];
					ballRank[j] = tmp;

					int tmp_ball = balls[i];
					balls[i] = balls[j];
					balls[j] = tmp_ball;
				}
			}
		}

		currBall = 0;
	}

	/**
	 * Returns the next (top) card from the deck.
	 * @return The next (top) card from the deck.
	 * @throws PotOutOfBallsException 
	 */
	public int getNextBall() throws PotOutOfBallsException{

		if (currBall >= balls.length) {
			throw new PotOutOfBallsException("Out of numbers");
		}
		return balls[currBall++];
	}

	/**
	 * Returns a card already taken from the deck back to the deck.
	 * @param card The card to return to the deck. 
	 * @return Returns whether the card was returned successfully or not.
	 */
	public boolean returnBall(int ball) {
		if (balls[currBall - 1] == ball) {
			currBall--;
			return true;
		}
		return false;
	}

	/**
	 * Returns the number of cards this deck instance has.
	 * @return The number of cards this deck instance has.
	 */
	public int getBallsCount(){
		return balls.length;
	}

	public void reset(){
		this.currBall = 0;
	}

	/**
	 * Returns the number of cards remaining in the deck.
	 */
	public int getBallsRemaining(){
		return (balls.length - (currBall+1));
	}

	/**
	 * Sets the pto so that the given balls are on the top.
	 * This is only used for testing and if you need this for any
	 * purpose other than testing you are doing it wrong.
	 * 
	 * @param topBalls
	 */
	public void setupPot(List<Integer> topBalls) {
		int ballsMoved = 0;

		for (int card: topBalls) {
			for (int i=ballsMoved; i < balls.length; i++) {
				if (balls[i] == card) {
					int tmp = balls[i];
					balls[i] = balls[ballsMoved];
					balls[ballsMoved] = tmp;
					ballsMoved++;
					break;
				}
			}
		}
	}
}