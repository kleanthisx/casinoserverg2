package com.magneta.games.keno;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.keno.KenoConfiguration;

@GameTemplate("Keno")
public class KenoRound extends AbstractGameRound {

	private static final String DRAW_ACTION = "g:DRAW";
	private static final String START_DRAW_ACTION = "g:START_DRAW";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*Description  Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,  ParameterType.REQUIRED,  ParameterType.REQUIRED),
	};

	private Pot pot;
	private final List<KenoBet> bets;
	private boolean finished;

	public KenoRound() {
		this.bets = new ArrayList<KenoBet>();
	}

	@Override
	public void roundInit() {        
		this.pot = new Pot(80);
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			if (!action.isLoaded()){
				performBet(action);
			} else{
				loadBet(action);
			}
		}
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		action.setRoundStart();
		String[] betsArray = action.getArea().split(";");

		if (betsArray.length == 0) {
			throw new ActionFailedException("Invalid area format");
		}

		List<KenoBet> tmpBets = new ArrayList<KenoBet>();
		for (String betStr: betsArray){

			KenoBet bet;
			try {
				bet = KenoBet.parse(betStr);
			} catch (IllegalArgumentException e) {
				throw new ActionFailedException("Invalid bet: " + e.getMessage());
			}
				if (tmpBets.contains(bet)){
					throw new ActionFailedException("Bet already placed!");
				} else if (bet.getAmount() < (this.options.getMinBet() - 0.001)) {
					throw new ActionFailedException("Bet is lower than min bet.");
				} else if (bet.getAmount() > (this.options.getMaxBet() + 0.001)){
					throw new ActionFailedException("Bet is higher than max bet.");    
				}

			tmpBets.add(bet);
		}

		List<GameAction> betActions = new ArrayList<GameAction>();
		
		
		for (KenoBet bet: tmpBets) {
			betActions.add(action.createGeneratedAction( BET_ACTION, bet.getNumbersToHitToString(), bet.getAmount()));
		}

		action.commit();
		
		for (GameAction betAction: betActions) {
			JackpotsHandler.contributeToJackpot(this, betAction);
		}

		bets.addAll(tmpBets);
		performPicks(action);
	}

	private void loadBet(GameAction action) throws ActionFailedException {
		DecimalFormat f = new DecimalFormat("0.00");
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		f.setDecimalFormatSymbols(symbols);

		try {
			KenoBet bet = KenoBet.parse(action.getArea()+":"+f.format(action.getAmount()));
			bets.add(bet);
		} catch (Throwable e) {
			throw new ActionFailedException("Invalid bet");
		}
	}

	@Override
	protected void loadFinished(GamePlayer player) throws ActionFailedException {
		if (bets != null && bets.size() > 0){
			GameAction loadedAction = new GameAction(player, this, 1, DRAW_ACTION, null, 0.0);
			performPicks(loadedAction);
		}
	}

	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());

		if (result == null) {
			return;
		}

		List<Integer> topBalls = new ArrayList<Integer>();
		String[] ballsSelected = result.split(" ");

		for (int i=0; i < ballsSelected.length; i++) {
			try {
				topBalls.add(Integer.parseInt(ballsSelected[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}

		if (topBalls.isEmpty()) {
			return;
		}

		pot.setupPot(topBalls);
	}

	/**
	 * @param action
	 * @throws ActionFailedException
	 */
	private void performPicks(GameAction action) throws ActionFailedException {
		GameAction drawAction = action.createGeneratedAction(null, 1, DRAW_ACTION, null, 0.0);

		/* Shuffle */
		pot.shuffleBalls(getRandom());
		setDeck();

		int[] picks = new int[20];
		for (int i = 0; i < picks.length; i++) {
			try {
				picks[i] = pot.getNextBall();
			} catch (PotOutOfBallsException e) {
				logError("Pot out of Balls. this should not have happened",e);
				throw new RuntimeException("Pot out of Balls. this should not have happened. round = "+this.getId(),e);
			}
		}

		String picks_str = "";
		for (int i = 0; i < picks.length; i++) {
			picks_str = picks_str + Integer.toString(picks[i]);
			if(i<picks.length-1){
				picks_str = picks_str +",";
			}
		}

		/* Send actions for balls drawn */
		GameAction handAction = drawAction.createGeneratedAction(null, 1, START_DRAW_ACTION, "0", 0.0);
		handAction.setActionValue(picks_str);

		/* Check for wins */
		for (KenoBet bet: bets) {

			boolean[] hitNumbers = bet.hasWon(picks);
			int hitCounter = 0;
			for (int i = 0; i < hitNumbers.length; i++) {
				if(hitNumbers[i]==true)
					hitCounter++;
			}
			boolean isJackpotCombo = getConfig().isJackpotCombination(bet.getNumbersToHit().length, hitCounter);
			boolean hasWonJackpot = false;
			double winAmount =0.0;
			double multiplier = getConfig().getMultiplier(bet.getNumbersToHit().length,hitCounter);
			if(isJackpotCombo){
				double jpRatio = getConfig().getJackpotPercentage(bet.getNumbersToHit().length,hitCounter);
				// checking for jackpot bonus win.
				double moneyFromPayout =  multiplier*bet.getAmount();
				hasWonJackpot = JackpotsHandler.claimGameJackpot(this, action, 0, moneyFromPayout, jpRatio);

			}

			if(multiplier >0.0 && !hasWonJackpot){
				winAmount = multiplier * bet.getAmount();
				if (-1<hitCounter) {
					GameAction winAction = drawAction.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, bet.getNumbersToHitToString(), winAmount);
					String foundOrder = "";
					for (int i = 0; i < hitNumbers.length; i++) {
						foundOrder = foundOrder + (hitNumbers[i]?"1":"0");
						if(i<picks.length-1){
							picks_str = picks_str +",";
						}
					}
					winAction.setActionValue(foundOrder);
				}
			}
		}

		drawAction.createGeneratedAction(null, 1, FINISH_ACTION, null, 0.0);
		drawAction.commit(true);
		this.finished = true;
	}

	@Override
	public KenoConfiguration getConfig(){
		return (KenoConfiguration)super.getConfig();
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {      
		/* Check if the action is "syntactically" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}
}
