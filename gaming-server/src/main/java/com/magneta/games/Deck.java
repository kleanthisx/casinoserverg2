package com.magneta.games;

import java.util.List;
import java.util.Random;

import static com.magneta.casino.common.utils.Card.*;

/**
 * The Deck class represents a deck of cards for any card-based game.
 */
public class Deck {

    private final int[] cards;
    private int currCard;
    
    /**
     * The constant number of cards per race in the deck.
     */
    public static final int CARDS_PER_RACE = 13;
    /**
     * The constant number of cards per deck.
     */
    public static final int CARDS_PER_DECK = CARDS_PER_RACE * 4;
    
    /**
     * Constructs a new Deck of cards. 
     * @param stacks The number of stacks to have in the deck.
     * @param jokers A flag indicating whether the deck should or should not have jokers.
     */
    public Deck(int stacks, int jokers) {

        cards = new int[(stacks * CARDS_PER_DECK) + (jokers)];
        currCard = 0;

        int card_index;
        
        for (int i=0; i < stacks; i++) {
            for (int race = 0; race < 4; race++) {
                card_index = (i*CARDS_PER_DECK) + (race*CARDS_PER_RACE);
                
                for (int j = 0; j < CARDS_PER_RACE; j++) {
                    cards[card_index + j] = (race << 4) | (j + 1);
                }
            }
        }
        
        for (int i=0;i<jokers;i++){
            cards[(stacks*CARDS_PER_DECK)+i] = (NO_TYPE_CARD << 4) | JOKER;
        }
    }

    /**
     * Shuffles the cards of the deck.
     * @param sr The Random object to be used to shuffle the deck's cards. 
     */
    public void shuffleCards(Random sr) {
 
        long[] cardRank = new long[cards.length];
        
        for (int rank=0; rank < cardRank.length; rank++) {
            cardRank[rank] = sr.nextLong();
        }
        
        for (int i=0; i < cards.length; i++) {
            for (int j=i+1; j < cards.length; j++) {
                if (cardRank[i] < cardRank[j]) {
                    long tmp = cardRank[i];
                    cardRank[i] = cardRank[j];
                    cardRank[j] = tmp;
                    
                    int tmp_card = cards[i];
                    cards[i] = cards[j];
                    cards[j] = tmp_card;
                }
            }
        }

        currCard = 0;
    }
    
    /**
     * Returns the next (top) card from the deck.
     * @return The next (top) card from the deck.
     */
    public int getNextCard(){
        
        if (currCard >= cards.length) {
            throw new DeckOutOfCardsException("Out of cards");
        }
        return cards[currCard++];
    }

    /**
     * Returns a card already taken from the deck back to the deck.
     * @param card The card to return to the deck. 
     * @return Returns whether the card was returned successfully or not.
     */
    public boolean returnCard(int card) {
        if (cards[currCard - 1] == card) {
            currCard--;
            return true;
        }
        return false;
    }
    
    /**
     * Returns the number of cards this deck instance has.
     * @return The number of cards this deck instance has.
     */
    public int getCardsCount(){
        return cards.length;
    }
    
    public void reset(){
        this.currCard = 0;
    }
    
    /**
     * Returns the number of cards remaining in the deck.
     */
    public int getCardsRemaining(){
        return (cards.length - (currCard+1));
    }
    
    /**
     * Sets the deck so that the given cards are on the top.
     * This is only used for testing and if you need this for any
     * purpose other than testing you are doing it wrong.
     * 
     * @param topCards
     */
    public void setupDeck(List<Integer> topCards) {
    	int cardsMoved = 0;
    	
    	for (int card: topCards) {
    		for (int i=cardsMoved; i < cards.length; i++) {
    			if (cards[i] == card) {
    				int tmp = cards[i];
    				cards[i] = cards[cardsMoved];
    				cards[cardsMoved] = tmp;
    				cardsMoved++;
    				break;
    			}
    		}
    	}
    }
}