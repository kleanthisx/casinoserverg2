/**
 * 
 */
package com.magneta.games;

/**
 * Request for action.
 * All requests are valid for the current round only.
 */
public interface GameActionRequest {

	/**
	 * The round for this request. Even though there is only
	 * one current round this is used to make sure that the client
	 * state is in sync with the server.
	 * 
	 * @return
	 */
    int getRoundId();
    
    /**
     * The seat for this request.
     * This is used by multi-seat games. For single seat games
     * the seat id must be 1.
     * @return
     */
    int getSeatId();
    
    /**
     * The type of the request. Examples: BET
     * @return
     */
    String getDescription();
    
    /**
     * The area of the request. This is request-specific.
     * @return
     */
    String getArea();
    
    /**
     * The amount for the request.
     * @return
     */
    double getAmount();    
}
