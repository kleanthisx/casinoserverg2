/**
 * 
 */
package com.magneta.games.poker;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.configuration.poker.PokerConstants;

import static com.magneta.games.configuration.poker.PokerConstants.*;

/**
 * @author Nicos
 *
 */
public final class PokerUtil {

	/**
	 * Returns the card value applicable to the video poker game.
	 * @param card The card of whose value is to be determined.
	 * @return The card value applicable to the video poker game.
	 */
	public static int getCardValue(int card) {
		int cardNo = Card.getCardNumber(card);

		if (cardNo == Card.ACE){
			return Card.KING+1;
		} else if (cardNo == Card.JOKER) {
			return Card.KING+2;
		} else{
			return cardNo;
		}
	}

	public static class ComboState {
		private List<Integer> cardIndexUsed;
		private int freezeIndex;

		public ComboState() {
			this.cardIndexUsed = new ArrayList<Integer>();
		}

		public boolean usedCard(int cardIndex) {
			return cardIndexUsed.contains(cardIndex);
		}

		public void addCard(int cardIndex) {
			this.cardIndexUsed.add(cardIndex);
		}

		public void markCardsUsed(boolean[] keepCard) {
			for (Integer i: cardIndexUsed) {
				keepCard[i] = true;
			}
		}

		public int getComboCardsUsed() {
			return cardIndexUsed.size() - freezeIndex;
		}

		public int getTotalCardsUsed() {
			return cardIndexUsed.size();
		}

		public void freeze() {
			this.freezeIndex = cardIndexUsed.size();
		}

		public ComboState reset() {
			if (this.freezeIndex > 0) {
				for (int i=cardIndexUsed.size()-1; i >= freezeIndex; i--) {
					cardIndexUsed.remove(i);
				}
			} else {
				cardIndexUsed.clear();
			}
			return this;
		}
	}

	public static ComboState findCombo(ComboState state, Integer[] cards, CardMatcher[] cardsToFind) {
		for (int i=0; i < cardsToFind.length; i++) {

			boolean foundCard = false;
			for (int j=0; j < cards.length; j++) {
				if (state.usedCard(j)) {
					continue;
				}

				if (cardsToFind[i].cardMatches(cards[j])) {
					state.addCard(j);
					foundCard = true;
					break;
				}
			}

			/* Replace missing cards with jokers */
			if (!foundCard) {
				for (int j=0; j < cards.length; j++) {
					if (Card.getCardNumber(cards[j]) != Card.JOKER || state.usedCard(j)) {
						continue;
					}

					state.addCard(j);
					break;
				}
			}
		}

		return state;
	}

	public static interface CardMatcher {
		boolean cardMatches(int card);
	}

	public static final class SingleCardMatcher implements CardMatcher {
		private final int card;

		public SingleCardMatcher(int card) {
			this.card = card;
		}

		@Override
		public boolean cardMatches(int card) {
			return this.card == card;
		}
	}

	public static final class CardNumberMatcher implements CardMatcher {
		private final int number;

		public CardNumberMatcher(int number) {
			this.number = number;
		}

		@Override
		public boolean cardMatches(int card) {
			return this.number == Card.getCardNumber(card);
		}
	}

	public static boolean checkKind(ComboState state, Integer[] cards, int count) {
		CardMatcher[] cardsToFind;

		/* Check aces first */
		{
			cardsToFind = new CardMatcher[count];

			for (int i=0; i < count; i++) {
				cardsToFind[i] = new CardNumberMatcher(Card.ACE);
			}

			findCombo(state.reset(), cards, cardsToFind);

			if (state.getComboCardsUsed() == cardsToFind.length) {
				return true;
			}
		}

		for (int number=Card.KING; number > Card.ACE; number--) {
			cardsToFind = new CardMatcher[count];

			for (int i=0; i < count; i++) {
				cardsToFind[i] = new CardNumberMatcher(number);
			}

			findCombo(state.reset(), cards, cardsToFind);

			if (state.getComboCardsUsed() == cardsToFind.length) {
				return true;
			}
		}

		return false;
	}

	public static boolean checkPair(ComboState state, Integer[] cards, int minPair) {
		CardMatcher[] cardsToFind;

		/* Check aces first */
		{
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(Card.ACE),
					new CardNumberMatcher(Card.ACE),
			};

			findCombo(state.reset(), cards, cardsToFind);

			if (state.getComboCardsUsed() == cardsToFind.length) {
				return true;
			}
		}

		for (int number = Card.KING; number >= minPair && number > Card.ACE; number --) {
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(number),
					new CardNumberMatcher(number),
			};

			findCombo(state.reset(), cards, cardsToFind);

			if (state.getComboCardsUsed() == cardsToFind.length) {
				return true;
			}
		}

		return false;
	}

	public static int getWinningCombination(Integer[] cards, boolean[] keepCard, int minPair) {

		CardMatcher[] cardsToFind;
		ComboState state = new ComboState();

		/* ROYAL FLUSH */
		for (int race=0; race < 4; race++) {

			cardsToFind = new CardMatcher[] {
					new SingleCardMatcher(Card.getCard(10, race)),
					new SingleCardMatcher(Card.getCard(Card.JACK, race)),
					new SingleCardMatcher(Card.getCard(Card.QUEEN, race)),
					new SingleCardMatcher(Card.getCard(Card.KING, race)),
					new SingleCardMatcher(Card.getCard(Card.ACE, race))
			};

			findCombo(state.reset(), cards, cardsToFind);
			if (state.getComboCardsUsed() == 5) {
				state.markCardsUsed(keepCard);
				return ROYAL_FLUSH;
			}
		}

		/* STRAIGHT FLUSH */
		for (int race=0; race < 4; race++) {
			for (int i=Card.KING; i >= Card.ACE + 4; i--) {
				cardsToFind = new CardMatcher[] {
						new SingleCardMatcher(Card.getCard(i, race)),
						new SingleCardMatcher(Card.getCard(i-1, race)),
						new SingleCardMatcher(Card.getCard(i-2, race)),
						new SingleCardMatcher(Card.getCard(i-3, race)),
						new SingleCardMatcher(Card.getCard(i-4, race))
				};

				findCombo(state.reset(), cards, cardsToFind);
				if (state.getComboCardsUsed() == 5) {
					state.markCardsUsed(keepCard);
					return STRAIGHT_FLUSH;
				}
			}
		}

		/* FOUR OF A KIND */
		if (checkKind(state.reset(), cards, 4)) {
			state.markCardsUsed(keepCard);
			return FOUR_OF_A_KIND;
		}

		/* FULL HOUSE */
		if (checkKind(state.reset(), cards, 3)) {
			state.freeze();

			if (checkPair(state.reset(), cards, 0)) {
				state.markCardsUsed(keepCard);
				return FULL_HOUSE;
			}
		}

		/* FLUSH */
		for (int race=0; race < 4; race++) {
			state = new ComboState();
			boolean[] tmpKeepCard = Arrays.copyOfRange(keepCard, 0, keepCard.length);
			int cardCcounter = 0;
			// look for ace
			cardsToFind = new CardMatcher[] {
					new SingleCardMatcher(Card.getCard(Card.ACE, race)),
			};
			state.freeze();
			findCombo(state, cards, cardsToFind);			
			if (state.getComboCardsUsed() == cardsToFind.length) {
				state.markCardsUsed(tmpKeepCard);
				cardCcounter++;
			}
			// look for rest
			for (int i=Card.KING; i > Card.ACE; i--) {
				cardsToFind = new CardMatcher[] {
						new SingleCardMatcher(Card.getCard(i, race)),
				};
				state.freeze();
				findCombo(state, cards, cardsToFind);			
				if (state.getComboCardsUsed() == cardsToFind.length) {
					state.markCardsUsed(tmpKeepCard);
					cardCcounter++;
					if(cardCcounter == 5) {
						for(int z = 0; z < tmpKeepCard.length;z++) {
							keepCard[z] = tmpKeepCard[z];
						}
						return FLUSH;
					}
				}
			}
		}

		state= new ComboState();

		/* STRAIGHT */
		{
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(Card.ACE),
					new CardNumberMatcher(Card.KING),
					new CardNumberMatcher(Card.QUEEN),
					new CardNumberMatcher(Card.JACK),
					new CardNumberMatcher(10)
			};

			findCombo(state.reset(), cards, cardsToFind);
			if (state.getComboCardsUsed() == cardsToFind.length) {
				state.markCardsUsed(keepCard);
				return STRAIGHT;
			}
		}

		for (int i=Card.KING; i >= 5; i--) {
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(i),
					new CardNumberMatcher(i-1),
					new CardNumberMatcher(i-2),
					new CardNumberMatcher(i-3),
					new CardNumberMatcher(i-4),
			};

			findCombo(state.reset(), cards, cardsToFind);
			if (state.getComboCardsUsed() == cardsToFind.length) {
				state.markCardsUsed(keepCard);
				return STRAIGHT;
			}
		}

		/* THREE OF A KIND */
		if (checkKind(state, cards, 3)) {
			state.markCardsUsed(keepCard);
			return THREE_OF_A_KIND;
		}

		/* TWO PAIR */
		if (checkPair(state.reset(), cards, 0)) {
			state.freeze();

			if (checkPair(state.reset(), cards, 0)) {
				state.markCardsUsed(keepCard);
				return TWO_PAIRS;
			}			
			state = new ComboState();
		}

		if (checkPair(state.reset(), cards, minPair)) {
			state.markCardsUsed(keepCard);
			return JACKS_OR_BETTER;
		}

		return NONE;
	}

	/**
	 * Checks if the hand of the player forms any possible winning combination.
	 * @param cardsState The CardsState object describing the state of the player hand.
	 * @param keepCard The array to place the hold suggestions in. Should always be passed with all values set to false.
	 * @return The winning combination constant value representing the combination.
	 */
	public static final int getPossibleCombination(Integer[] cards, boolean[] keepCard, int minPair) {

		Integer[] fakeCards = new Integer[cards.length+1];
		boolean[] fakeKeepCard = new boolean[keepCard.length+1];

		for (int i=0; i < cards.length; i++) {
			fakeCards[i] = cards[i];
		}

		fakeCards[cards.length] = Card.getCard(Card.JOKER, Card.NO_TYPE_CARD);

		int combo = getWinningCombination(fakeCards, fakeKeepCard, minPair);

		if (combo < PokerConstants.STRAIGHT) {
			combo = NONE;
		}

		switch(combo) {
		case ROYAL_FLUSH:
			combo = POSSIBLE_ROYAL_FLUSH;
			break;
		case STRAIGHT_FLUSH:
			combo = POSSIBLE_STRAIGHT_FLUSH;
			break;
		case FLUSH:
			combo = POSSIBLE_FLUSH;
			break;
		case STRAIGHT:
			combo = POSSIBLE_STRAIGHT;
			break;
		default:
			return NONE;
		}


		for (int i=0; i < keepCard.length; i++) {
			keepCard[i] = fakeKeepCard[i];
		}

		return combo;
	}
}
