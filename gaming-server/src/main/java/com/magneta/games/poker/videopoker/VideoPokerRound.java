package com.magneta.games.poker.videopoker;

import static com.magneta.games.configuration.poker.PokerConstants.FOUR_OF_A_KIND;
import static com.magneta.games.configuration.poker.PokerConstants.NONE;
import static com.magneta.games.poker.PokerUtil.getPossibleCombination;
import static com.magneta.games.poker.PokerUtil.getWinningCombination;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.videopoker.VideoPokerConfiguration;
import com.magneta.games.poker.PokerUtil;

@GameTemplate("VideoPoker")
public class VideoPokerRound extends AbstractGameRound {

	/**
	 * The constant action description value for the draw action.  
	 */
	private static final String DRAW_ACTION = "g:DRAW";
	/**
	 * The constant action description value for the hold action.  
	 */
	private static final String HOLD_ACTION = "g:HOLD";
	/**
	 * The constant action description value for the double up action.  
	 */
	private static final String DOUBLE_UP_ACTION = "g:DOUBLE_UP";
	/**
	 * The constant action description value for the end hold action.  
	 */
	private static final String END_HOLD_ACTION = "g:END_HOLD";
	/**
	 * The constant action description value for the take score action.  
	 */
	private static final String TAKE_SCORE_ACTION = "g:TAKE_SCORE";
	/**
	 * The constant action description value for the double half action.  
	 */
	private static final String DOUBLE_HALF_ACTION = "g:DOUBLE_HALF";
	/**
	 * The constant action description value for the double draw action.  
	 */
	private static final String DOUBLE_DRAW_ACTION = "g:DOUBLE_DRAW";
	/**
	 * The constant action description value for the hold suggest action.  
	 */
	private static final String HOLD_SUGGEST_ACTION = "g:HOLD_SUGGEST";
	/**
	 * The constant action description value for the win card action.  
	 */
	private static final String WIN_CARD_ACTION = "g:WIN_CARD";
	/**
	 * The constant action description value for the hold reason action.  
	 */
	private static final String HOLD_REASON_ACTION = "g:HOLD_REASON";
	/**
	 * The constant action description value for the possible action.  
	 */
	private static final String POSSIBLE_ACTION = "g:POSSIBLE";
	/**
	 * The constant action description value for the higher action.  
	 */
	private static final String HIGHER_ACTION = "g:HIGHER";


	private static final int NORMAL_MODE = 0;
	private static final int DOUBLE_UP_MODE = 1;

	//private static final int JACKPOT_COMBO = ROYAL_FLUSH;
	private static final int JOKERS = 0;
	private static final int DOUBLE_LIMIT = 5;

	private boolean allowDoubleUp;
	private GameActionRequestDefinition[] allowed_actions; 

	private Deck gameDeck;
	private Integer[] cards;
	private boolean[] holdCard;
	private GameRoundState roundState;
	private int gameMode;
	private double betAmount;
	private double multiplier;
	private int finalWinCombo;
	private int doubleUpCard;
	private int doubleTimes;

	public VideoPokerRound() {
		this.roundState = GameRoundState.ROUND_NOT_STARTED;
		this.gameMode = NORMAL_MODE;
		this.cards = new Integer[5];
		this.holdCard = new boolean[5];
		this.multiplier = 0.0;
		this.betAmount = 0.0;
		this.doubleUpCard = -1;
		this.finalWinCombo = NONE;
		this.doubleTimes = 0;
	}
	
	@Override
	public VideoPokerConfiguration getConfig() {
		return (VideoPokerConfiguration)super.getConfig();
	}
	
	@Override
	public void roundInit() {
		allowDoubleUp = options.getGameSettings().get("allow_double_up", Boolean.TYPE);

		if (allowDoubleUp){
			allowed_actions = new GameActionRequestDefinition[] {
					/*Description      Area                      Amount*/ 
					new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED),
					new GameActionRequestDefinition(HOLD_ACTION,       ParameterType.REQUIRED,   ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(END_HOLD_ACTION,   ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(DOUBLE_UP_ACTION,  ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(TAKE_SCORE_ACTION, ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(DOUBLE_HALF_ACTION,ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(HIGHER_ACTION,     ParameterType.REQUIRED,   ParameterType.FORBIDDEN)
			};
		} else {
			allowed_actions = new GameActionRequestDefinition[] {
					new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED),
					new GameActionRequestDefinition(HOLD_ACTION,       ParameterType.REQUIRED,   ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(END_HOLD_ACTION,   ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN)
			};
		}
	}

	@Override
	public boolean isFinished() {
		return (roundState == GameRoundState.ROUND_FINISHED);
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (HOLD_ACTION.equals(action.getDescription())){
			performHold(action);
		} else if (END_HOLD_ACTION.equals(action.getDescription())){
			performEndHold(action);
		} else if (DOUBLE_UP_ACTION.equals(action.getDescription())){
			performDoubleUp(action); 
		} else if (TAKE_SCORE_ACTION.equals(action.getDescription())){
			performTakeScore(action);
		} else if (DOUBLE_HALF_ACTION.equals(action.getDescription())){
			performDoubleHalf(action);
		} else if (HIGHER_ACTION.equals(action.getDescription())){
			performHigher(action);
		}
	}

	/**
	 * Performs the action of the player selecting a card from the 4 hidden which he believes may be of higher value than the double up card.
	 * @param action The action request object.
	 * @throws ActionFailedException
	 */
	public void performHigher(GameAction action) throws ActionFailedException{
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: HIGHER is only allowed after round has started and while in double mode.");
		} else if (doubleUpCard == -1){
			throw new ActionFailedException("Invalid state: must double to select higher!");
		}

		int[] doubleCards = new int[4];

		int area = Integer.parseInt(action.getArea());

		if ((area < 1) || (area >= (doubleCards.length+1))) {
			throw new ActionFailedException("Invalid area for higher.");
		}

		/*FIXED ODD CODE*/
		com.magneta.games.fixedodd.VideoPokerGuard.guardDouble(game.getGameId(), gameDeck, this.options.getOwner(), betAmount, multiplier, doubleUpCard, area, options.getGameSettings(), action);

		for (int i=0;i<doubleCards.length;i++){
			int nextCard = gameDeck.getNextCard();
			GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, String.valueOf(i+1), 0.0);
			drawAction.setActionValue(String.valueOf(nextCard));

			doubleCards[i] = nextCard;
		}

		action.setActionValue(String.valueOf(doubleCards[area-1]));
		action.commit();

		if (PokerUtil.getCardValue(doubleCards[area-1]) >PokerUtil.getCardValue(doubleUpCard)) {
			multiplier *= 2.0;
			doubleUpCard = -1;
			doubleTimes++;

			if (doubleTimes >= DOUBLE_LIMIT){
				endRound(action);
			}
		} else {
			multiplier = 0.0;
			doubleUpCard = -1;
			endRound(action);
		}
	}

	/**
	 * Perform the double half action (user risks only 1/2 of his points - takes half of his winnings).
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performDoubleHalf(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: DOUBLE HALF is only allowed after round has started and in double up mode once.");
		} else if (doubleUpCard != -1){
			throw new ActionFailedException("Already doubled half!");
		}

		double half = (multiplier / 2.0) * betAmount;

		action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, half);
		action.commit();

		multiplier /= 2.0;

		GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, "0", 0.0);

		//reset and re-shuffle deck
		gameDeck.reset();
		gameDeck.shuffleCards(getRandom());

		int nextCard = gameDeck.getNextCard();
		drawAction.setActionValue(String.valueOf(nextCard));

		try {
			drawAction.commit();

			doubleUpCard = nextCard;
		} catch (ActionFailedException e) {
			gameDeck.returnCard(nextCard);
			doubleUpCard = -1;
			logError("Error while doubling up.",e);
			throw e;
		}
	}

	/**
	 * Performs the double up action.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performDoubleUp(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: DOUBLE UP is only allowed after round has started.");
		} else if (doubleUpCard != -1){
			throw new ActionFailedException("Already double up");
		}

		action.commit();

		GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, "0", 0.0);

		//reset and re-shuffle deck
		gameDeck.reset();
		gameDeck.shuffleCards(getRandom());

		int nextCard = gameDeck.getNextCard();
		drawAction.setActionValue(String.valueOf(nextCard));

		try {
			drawAction.commit();

			doubleUpCard = nextCard;
		} catch (ActionFailedException e) {
			gameDeck.returnCard(nextCard);
			doubleUpCard = -1;
			logError("Error while doubling up.",e);
			throw e;
		}
	}


	/**
	 * Ends the current round if the player is in double up mode.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performTakeScore(GameAction action) throws ActionFailedException{
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: TAKE SCORE is only allowed after round has started and while in double up mode.");
		} else if (doubleUpCard != -1){
			throw new ActionFailedException("Double up in progress; Cannot take score at this time.");
		}

		try {
			action.commit();
			endRound(action);
		} catch (ActionFailedException e) {
			logError("Error while taking score.",e);
			throw new ActionFailedException("Could not take score: "+e.getMessage());
		}
	}

	/**
	 * Performs the end hold action ending the process of choosing the cards to hold.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performEndHold(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != NORMAL_MODE)) {
			throw new ActionFailedException("Invalid state: END HOLD is only allowed after round has started.");
		}

		/* FIXED ODD CODE */
		com.magneta.games.fixedodd.VideoPokerGuard.guardSecondHand(game.getGameId(), this.gameDeck, this.options.getOwner(), getConfig().getPaytable(), this.betAmount, this.cards, this.holdCard, options.getGameSettings(), action);        

		for (int i=0;i<cards.length;i++){
			if (!holdCard[i]){
				int nextCard = gameDeck.getNextCard();

				GameAction drawAction = action.createGeneratedAction(null, 1, DRAW_ACTION, String.valueOf(i), 0.0);
				drawAction.setActionValue(String.valueOf(nextCard));

				cards[i] = nextCard;
			}
		}

		action.commit();

		boolean[] keepCard = new boolean[cards.length];

		finalWinCombo = getWinningCombination(cards, keepCard, Card.JACK);

		if (finalWinCombo == NONE){
			multiplier = 0.0;
			endRound(action);
		} else {
			multiplier = getMultiplier(finalWinCombo);

			for (int i=0;i<keepCard.length;i++){
				if (keepCard[i]){
					GameAction winCard = action.createGeneratedAction(null, 0, WIN_CARD_ACTION, String.valueOf(i), 0.0);
					winCard.setActionValue(String.valueOf(finalWinCombo));
				}
			}

			action.commit();

			/*if ((finalWinCombo == JACKPOT_COMBO) && (placedJackpotContrib)){
                GameAction roundFinish = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);

                GameAction winAction = roundFinish.createGeneratedAction(1, this.options.getOwner(), WIN_ACTION, null, 0.0);
                winAction.setAmountFunction(
                        "handle_jackpot_win("+this.options.getOwner()+","+this.game.getId()+","+this.tableId+","+this.roundId+")");
                winAction.setActionValue("Jackpot");

                try {
                    roundFinish.commit();
                    this.onPlayerRoundFinish(action.getPlayer());
                    roundState = GameRoundState.ROUND_FINISHED;
                } catch (ActionFailedException e) {
                    // The jackpot does not exist
                    Log.debug("Jackpot conditions met but no jackpot currently exists.");
                    action.removeGeneratedAction(roundFinish);
                    if ((finalWinCombo >= FOUR_OF_A_KIND) || (!allowDoubleUp)){
                        endRound(action);
                    } else {
                        gameMode = DOUBLE_UP_MODE;
                    }
                }
            } else*/ if ((finalWinCombo >= FOUR_OF_A_KIND) || (!allowDoubleUp)){
            	endRound(action);
            } else {
            	gameMode = DOUBLE_UP_MODE;
            }
		}
	}

	/**
	 * Ends the round.
	 * @param parentAction The action that caused the round to end.
	 */
	private void endRound(GameAction parentAction) {
		if (roundState != GameRoundState.ROUND_FINISHED){

			logDebug("endRound() was called for round "+getId()+" caused by: "+parentAction);

			GameAction roundFinish = parentAction.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);

			if (multiplier > 0.0){
				double winAmount = multiplier * betAmount;
				roundFinish.createGeneratedAction(parentAction.getPlayer(), 1, WIN_ACTION, null, winAmount);
			}

			try {
				parentAction.commit(true);
				roundState = GameRoundState.ROUND_FINISHED;
			} catch (ActionFailedException e) {
				logError("Error while finishing round.", e);
			}
		} else {
			throw new IllegalStateException("Method endRound() already called once.");
		}
	}

	/**
	 * Performs the hold action, telling the round that the card in the given area should not be replaced.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performHold(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != NORMAL_MODE)) {
			throw new ActionFailedException("Invalid state: Hold is only allowed after round has started.");
		}

		int area = Integer.parseInt(action.getArea());

		if ((area < 0) || (area >= holdCard.length)) {
			throw new ActionFailedException("Invalid area for hold.");
		}

		if (!holdCard[area]){
			action.commit();
			holdCard[area] = true;
		}
	}

	/**
	 * Performs the BET action.
	 * @param action The bet GameAction to update.
	 */
	private void performBet(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		checkBet(action);
		action.setRoundStart();
		action.commit();
		betAmount = action.getAmount();

		//this.placedJackpotContrib = JackpotsHandler.contributeToJackpot(this.options.getOwner(), this.options.getOwnerSuperCorpId(), this.game.getId(), betAmount);

		startRound(action);
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(allowed_actions, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (roundState == GameRoundState.ROUND_FINISHED) {
			throw new ActionFailedException("Round has finished.");
		} else if ((roundState == GameRoundState.ROUND_NOT_STARTED) && (!BET_ACTION.equals(request.getDescription()))) {
			throw new ActionFailedException("Round has not started yet.");
		} else if ((roundState == GameRoundState.ROUND_NOT_STARTED) && (BET_ACTION.equals(request.getDescription()))) {
			/* Avoid the last check */
		}  else if ((gameMode == NORMAL_MODE) && 
				(!HOLD_ACTION.equals(request.getDescription())) &&
				(!END_HOLD_ACTION.equals(request.getDescription())) && 
				(!DOUBLE_UP_ACTION.equals(request.getDescription()))) {
			throw new ActionFailedException("Cannot perform action at this time.");
		}
	}

	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());
		
		if (result == null) {
			return;
		}
		
		List<Integer> topCards = new ArrayList<Integer>();
		String[] sReelStops = result.split(" ");

		for (int i=0; i < sReelStops.length; i++) {
			try {
				topCards.add(Integer.parseInt(sReelStops[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}

		if (topCards.isEmpty()) {
			return;
		}
		
		gameDeck.setupDeck(topCards);
	}
	
	/**
	 * Starts the round.
	 * @param parentAction The action that caused the round start.
	 * @throws ActionFailedException
	 */
	private void startRound(GameAction parentAction) throws ActionFailedException {
		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Round has already started");
		}

		gameDeck = new Deck(1,JOKERS);
		gameDeck.shuffleCards(getRandom());
		
		/*FIXED ODD CODE*/
		com.magneta.games.fixedodd.VideoPokerGuard.guardFirstHand(game.getGameId(), gameDeck, this.options.getOwner(), getConfig(), betAmount, options.getGameSettings(), parentAction);
		setDeck();
		
		roundState = GameRoundState.ROUND_STARTED;


		for (int i=0;i<cards.length;i++){
			int nextCard = gameDeck.getNextCard();
			GameAction drawAction = parentAction.createGeneratedAction(null, 1, DRAW_ACTION, String.valueOf(i), 0.0);
			drawAction.setActionValue(String.valueOf(nextCard));

			cards[i] = nextCard;
		}

		boolean[] keepCard = new boolean[cards.length];

		int combo = getWinningCombination(cards, keepCard, Card.JACK);

		if (combo != NONE){
			for (int i=0;i<keepCard.length;i++){
				if (keepCard[i]){
					parentAction.createGeneratedAction(null, 0, HOLD_SUGGEST_ACTION, String.valueOf(i), 0.0);
				}
			}

			GameAction holdReason = parentAction.createGeneratedAction(null, 0, HOLD_REASON_ACTION, null, 0.0);
			holdReason.setActionValue(String.valueOf(combo));
		} else {
			boolean[] possibleHold = new boolean[cards.length];
			int possibleCombo = getPossibleCombination(cards, possibleHold, Card.JACK);
			
			for (int i=0;i<possibleHold.length;i++){
				if (possibleHold[i]){
					parentAction.createGeneratedAction(null, 0, HOLD_SUGGEST_ACTION, String.valueOf(i), 0.0);
				}
			}

			if (possibleCombo != NONE) {
				GameAction possibleAction = parentAction.createGeneratedAction(null, 0, POSSIBLE_ACTION, null, 0.0);
				possibleAction.setActionValue(String.valueOf(possibleCombo));
			
			}
		}

		parentAction.commit();
	}

	private double getMultiplier(int combo){
		return getConfig().getMultiplier(combo);
	}

	@Override
	protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
		super.getExtraInfo(player, roundInfo, firstTime);

		if (roundInfo != null && firstTime) {
			double[] paytable = getConfig().getPaytable();
			for (int i=1; i<paytable.length; i++){
				roundInfo.put("multiplier_"+i, paytable[i]);
			}
		}
	}
}
