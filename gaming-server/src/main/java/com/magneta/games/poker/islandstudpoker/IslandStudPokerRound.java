package com.magneta.games.poker.islandstudpoker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.DefaultGameActionFilter;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionFilter;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.PokerConstants;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;
import com.magneta.games.configuration.poker.islandstudpoker.WinCombination;
import com.magneta.games.configuration.poker.islandstudpoker.WinType;
import com.magneta.games.fixedodd.IslandStudHandGuardParams;
import com.magneta.games.fixedodd.IslandStudPokerGuard;
import com.magneta.games.poker.PokerUtil;
import com.magneta.games.poker.PokerUtil.ComboState;

/**
 * BlackjackRound is the blackjack game-specific GameRound implementation.
 */
@GameTemplate("IslandStudPoker")
public class IslandStudPokerRound extends AbstractGameRound {
	
	/**
	 * The constant action description value for the fold action.  
	 */
	private static final String FOLD_ACTION = "g:FOLD";
	
	/**
	 * The constant action description value for the fold action.  
	 */
	private static final String RAISE_ACTION = "g:RAISE";
	
	private static final String JACKPOT_BET_ACTION = "g:JACKPOT_BET";

	/*                                  desc           area                     amount */
	private static final GameActionRequestDefinition [] ALLOWED_ACTIONS = {
		new GameActionRequestDefinition(BET_ACTION,    ParameterType.FORBIDDEN, ParameterType.REQUIRED),
		new GameActionRequestDefinition(JACKPOT_BET_ACTION,    ParameterType.FORBIDDEN, ParameterType.REQUIRED),
		new GameActionRequestDefinition(FOLD_ACTION,   ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(RAISE_ACTION,  ParameterType.FORBIDDEN, ParameterType.FORBIDDEN)
	};

	private final Deck roundDeck;
	private GameRoundState roundState;
	
	private Integer[] dealerHand;
	
	private double playerBet;
	private double jackpotBet;
	private Integer[] playerHand;

	/**
	 * Constructs a new BlackjackRound.
	 * @param game The blackjack game held by the round.
	 * @param options The round's options values.
	 * @param tableId The ID of the table. 
	 * @param roundId The ID of the round to construct.
	 * @param seed The seed to used for the random generator.
	 */
	public IslandStudPokerRound() {
		roundDeck = new Deck(1,0);
		dealerHand = new Integer[5];
		playerHand = new Integer[5];		
		roundState = GameRoundState.ROUND_NOT_STARTED;
	}

	@Override
	public IslandStudPokerConfiguration getConfig() {
		return (IslandStudPokerConfiguration)super.getConfig();
	}

	@Override
	public boolean isFinished() {
		return roundState == GameRoundState.ROUND_FINISHED;
	}
	
    @Override
	public GameActionFilter getActionFilter() {
        return new DefaultGameActionFilter(true);
    }

	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());
		
		if (result == null) {
			return;
		}
		
		List<Integer> topCards = new ArrayList<Integer>();
		String[] sReelStops = result.split(" ");

		for (int i=0; i < sReelStops.length; i++) {
			try {
				topCards.add(Integer.parseInt(sReelStops[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}

		
		if (topCards.isEmpty()) {
			return;
		}
		
		roundDeck.setupDeck(topCards);
	}

	/**
	 * Starts the round.
	 * @param parentAction The action which caused the START action.
	 * @throws ActionFailedException 
	 */
	private void startRound(GameAction dealAction) throws ActionFailedException {
		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Round already started");
		}

		if (this.playerBet < 0.01) {
			throw new ActionFailedException("No bets have been placed");
		}

		roundState = GameRoundState.ROUND_STARTED;

		/* Shuffle cards */
		roundDeck.shuffleCards(getRandom());
		setDeck();
		
		for (int i= 0; i < playerHand.length; i++) {
			playerHand[i] = roundDeck.getNextCard();
			dealerHand[i] = roundDeck.getNextCard();
		}

		// guard must be here
		IslandStudHandGuardParams params = new IslandStudHandGuardParams();
		params.setGameID(game.getGameId());
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setOwnerID(this.options.getOwner());
		params.setConfig(getConfig());
		params.setBetAmount(playerBet);
		params.setJackpotBet(jackpotBet);
		params.setSettings(options.getGameSettings());
		params.setParentAction(dealAction);
		
		IslandStudPokerGuard.guardFirstHand(params);
		
		dealAction.createGeneratedAction(0, "g:HIT", null, 0.0).setActionValue(String.valueOf(dealerHand[0]));

		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		
		for (Integer card: playerHand) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(' ');
			}
			sb.append(card.toString());
		}
		
		boolean[] playerWinningCards = new boolean[playerHand.length];
		int combination = getWinningCombination(playerHand, playerWinningCards);
		
		dealAction.createGeneratedAction(1, "g:HIT", getActionArea(combination, playerWinningCards), 0.0).setActionValue(sb.toString());
		dealAction.commit();
	}

	/**
	 * Ends the round.
	 * @param action The action that caused the round end action.
	 * @throws ActionFailedException 
	 */
	private void endRound(GameAction action) throws ActionFailedException {
		if (roundState != GameRoundState.ROUND_STARTED) {
			throw new ActionFailedException("Round already finished");
		}
		
		roundState = GameRoundState.ROUND_FINISHED;
		
		
		/* Check for royal/straight flush for jackpot */
		if (this.jackpotBet > 0.001) {			
			int combination = PokerUtil.getWinningCombination(this.playerHand, new boolean[playerHand.length], 0);
			
			WinCombination winCombo = getConfig().getJackpotWinCombination(combination);
			
			if (winCombo != null && winCombo.getAmount() > 0.001) {
				if (winCombo.getType() == WinType.JACKPOT_POOL_RATIO) {
					JackpotsHandler.claimGameJackpot(this, action, winCombo.getAmount());
				} else {
					JackpotsHandler.claimGameJackpotAmount(this, action, winCombo.getAmount() * jackpotBet);
				}
			}
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
	}

	/**
	 * Validates an action request.
	 * @param request The GameActionRequest to validate
	 * @param results A Map used to save any feedback from the validation process. 
	 * In case the validation fails, please find the cause stored as a String in the ERROR field.
	 */
	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (roundState == GameRoundState.ROUND_FINISHED) {
			throw new ActionFailedException("Round has finished.");
		}

		if ((roundState == GameRoundState.ROUND_NOT_STARTED) 
				&& (!BET_ACTION.equals(request.getDescription()))
				&& (!JACKPOT_BET_ACTION.equals(request.getDescription())) ) {
			throw new ActionFailedException("Round has not started yet.");
		}

		if (request.getSeatId() != 1) {
			throw new ActionFailedException("Invalid seat number!");
		}
	}

	/**
	 * Performs the requested action.
	 * @param player The player that requested the action.
	 * @param action The action to execute.
	 * @param results A Map used to save any feedback from the validation process. 
	 * In case the request fails, please find the cause, stored as a String, in the ERROR field. 
	 * Also find the card(s) drawn from the HIT, SPLIT or DOUBLE action in the cards_0 and cards_1 
	 * (OPTIONAL) fields.
	 */
	@Override
	protected void performAction(GameAction action) throws ActionFailedException {

		if (action.getSeatId() != 1) {
			throw new ActionFailedException("Invalid seat id");
		}

		if (BET_ACTION.equals(action.getDescription())) {
			performBet(action);
		} else if (JACKPOT_BET_ACTION.equals(action.getDescription())) {
			performJackpotBet(action);
		} else if (FOLD_ACTION.equals(action.getDescription())) {
			performFold(action);
		} else if (RAISE_ACTION.equals(action.getDescription())) {
			performRaise(action);
		}
	}

	/**
	 * Performs the BET action.
	 * @param action The bet action GameAction to update.
	 * @param results The Map to save any errors in the ERROR field.
	 */
	private void performBet(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
		}

		checkBet(action);
		
		if (this.playerBet > 0.0) {
			throw new ActionFailedException("Bet already placed");
		}

		action.setRoundStart();
		action.commit();
		playerBet = action.getAmount();
		
		startRound(action);
	}
	
	private void performFold(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_STARTED) {
			throw new ActionFailedException("Invalid state: Fold is only allowed after the round has started");
		}
		
		/* Send the remaining 4 cards the dealer received */
		StringBuilder sb = new StringBuilder();
		
		for (int i=1; i < dealerHand.length; i++) {
			if (i > 1) {
				sb.append(' ');
			}
			sb.append(dealerHand[i].toString());
		}

		
		boolean[] dealerWinningCards = new boolean[dealerHand.length];
		int dealerCombo = getWinningCombination(dealerHand, dealerWinningCards);
		
		action.createGeneratedAction(0, "g:HIT", getActionArea(dealerCombo, dealerWinningCards), 0.0).setActionValue(sb.toString());
		
		endRound(action);
	}
	
	private static int handCardNumberCount(Integer[] hand, int number) {
		int count = 0;

		for (int card: hand) {
			int no = Card.getCardNumber(card);
			
			if (number == no) {
				count++;
			}
		}
		
		return count;
	}
	
	private static class CardComparator implements Comparator<Integer> {

		private final Integer[] hand;
		private final boolean[] cardsInvolved;
		private final boolean involved;
		
		public CardComparator(Integer[] hand, boolean[] cardsInvolved, boolean involved) {
			this.hand = hand;
			this.cardsInvolved = cardsInvolved;
			this.involved = involved;
		}
		
		private int countCardNumber(int number) {
			int count = 0;
			for (int i=0; i < hand.length; i++) {
				if (!cardsInvolved[i] == involved) {
					continue;
				}
				
				if (Card.getCardNumber(hand[i]) == number) {
					count++;
				}
			}

			return count;
		}
		
		@Override
		public int compare(Integer card0, Integer card1) {
			if (involved) {
				int frequencyDiff = countCardNumber(Card.getCardNumber(card1)) - countCardNumber(Card.getCardNumber(card0));

				if (frequencyDiff != 0) {
					return frequencyDiff;
				}
			}
			
			return PokerUtil.getCardValue(card1) - PokerUtil.getCardValue(card0);
		}
		
	}
	
	/**
	 * Sorts hands as follows:
	 * - Cards involved in the winning combination are first non-involved follow.
	 * - Cards which appear more frequently have priortity.
	 * - Highest value cards have priority over lowest value.
	 * @param hand
	 * @param cardsInvolved
	 * @param handState
	 */
	private Integer[] sortHand(Integer[] origHand, boolean[] cardsInvolved) {		
		Integer[] hand = new Integer[origHand.length];
		
		int involvedCount = 0;
		
		/* Add involved cards on top */
		for (int i=0; i < origHand.length; i++) {
			if (!cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount] = origHand[i];
			involvedCount++;
		}
		
		/* Add the remainingCards */
		int currIndex = 0;
		for (int i=0; i < origHand.length; i++) {
			if (cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount + currIndex] = origHand[i];
			currIndex++;
		}
		
		for (int i = 0; i < involvedCount; i++) {
			cardsInvolved[i]=true;
		}
		for (int i = involvedCount; i < cardsInvolved.length; i++) {
			cardsInvolved[i]=false;
		}

		
		if (involvedCount > 0) {
			Arrays.sort(hand, 0, involvedCount, new CardComparator(hand, cardsInvolved, true));
		}
		
		if (involvedCount < hand.length) {
			Arrays.sort(hand, involvedCount, hand.length, new CardComparator(hand, cardsInvolved, false));
		}
		
		return hand;
	}
	
	public int getWinningCombination(Integer[] cards, boolean[] cardsInvolved) {
		return PokerUtil.getWinningCombination(cards, cardsInvolved, 0);
	}
	
	public int compareHands(Integer[] hand0, Integer[] hand1) {
		boolean[] hand0CardsInvolved = new boolean[hand0.length];
		boolean[] hand1CardsInvolved = new boolean[hand1.length];


		int hand0Combo = getWinningCombination(hand0, hand0CardsInvolved);
		int hand1Combo = getWinningCombination(hand1, hand1CardsInvolved);

		if (hand0Combo != hand1Combo) {
			return hand0Combo - hand1Combo;
		}
		
		if (hand0Combo == PokerConstants.FULL_HOUSE) {
			boolean[] nHand0CardsInvolved = new boolean[hand0.length];
			boolean[] nHand1CardsInvolved = new boolean[hand1.length];
			ComboState state0 = new ComboState();
			PokerUtil.checkKind(state0, hand0, 3);
			
			ComboState state1 = new ComboState();
			PokerUtil.checkKind(state1, hand1, 3);
			
			state0.markCardsUsed(nHand0CardsInvolved);
			state1.markCardsUsed(nHand1CardsInvolved);
			
			Integer[] nHand0 = sortHand(hand0, nHand0CardsInvolved);
			Integer[] nHand1 = sortHand(hand1, nHand1CardsInvolved);
			
			for (int i=0; i < nHand0.length; i++) {
				int cardDiff = PokerUtil.getCardValue(nHand0[i]) - PokerUtil.getCardValue(nHand1[i]);

				if (cardDiff != 0) {
					return cardDiff;
				}
			}
		}

		Integer[] newHand0 = sortHand(hand0, hand0CardsInvolved);
		Integer[] newHand1 = sortHand(hand1, hand1CardsInvolved);
		int tmpdif = 0;
		switch (hand0Combo) {
			case PokerConstants.STRAIGHT_FLUSH:
			case PokerConstants.STRAIGHT:
				if (PokerUtil.getCardValue(newHand0[1]) == PokerUtil.getCardValue(newHand1[1])) {
					tmpdif =  (PokerUtil.getCardValue(newHand0[0]) % 14) - (PokerUtil.getCardValue(newHand1[0]) % 14);
				}
				if(tmpdif!=0)
				return tmpdif;
		}
		
		for (int i=0; i < newHand0.length; i++) {
			int cardDiff = PokerUtil.getCardValue(newHand0[i]) - PokerUtil.getCardValue(newHand1[i]);

			if (cardDiff != 0) {
				return cardDiff;
			}
		}
		return 0;
	}
	
	private String getActionArea(int combo, boolean[] involvedCards) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(combo);
		
		for (int i=0; i < involvedCards.length; i++) {
			if (involvedCards[i]) {
				sb.append(' ');
				sb.append(i);
			}
		}
		return sb.toString();
	}
	
	private void performRaise(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_STARTED) {
			throw new ActionFailedException("Invalid state: Fold is only allowed after the round has started");
		}
		
		action.setAmount(playerBet * 2.0);
		action.commit();
		
		/* Send the remaining 4 cards the dealer received */
		StringBuilder sb = new StringBuilder();
		
		for (int i=1; i < dealerHand.length; i++) {
			if (i > 1) {
				sb.append(' ');
			}
			sb.append(dealerHand[i].toString());
		}
		
		boolean[] dealerWinningCards = new boolean[dealerHand.length];
		int dealerCombo = getWinningCombination(dealerHand, dealerWinningCards);		

		action.createGeneratedAction(0, "g:HIT", getActionArea(dealerCombo, dealerWinningCards), 0.0).setActionValue(sb.toString());
		
		boolean dealerQualified = checkIfDealerQualifies(dealerCombo, dealerHand);
		
		if (!dealerQualified) {
			action.createGeneratedAction(1, "WIN", "0", this.playerBet*2.0).setActionValue("NOT QUALIFIED");
			action.createGeneratedAction(1, "WIN", "1", this.playerBet*2.0).setActionValue("NOT QUALIFIED");
		} else {
			
			int handDiff = compareHands(dealerHand, playerHand);

			/* Player won */
			if (handDiff < 0) {
				
				boolean[] playerWinningCards = new boolean[playerHand.length];
				int playerCombo = getWinningCombination(playerHand, playerWinningCards);
				
				WinCombination combo = getConfig().getWinCombination(playerCombo);
				
				double multiplier = combo.getAmount();

				action.createGeneratedAction(1, "WIN", "0", this.playerBet*2.0).setActionValue(getActionArea(playerCombo, playerWinningCards));
				action.createGeneratedAction(1, "WIN", "1", (this.playerBet * 2.0) * (multiplier + 1.0)).setActionValue(getActionArea(playerCombo, playerWinningCards));
			/* Push */
			} else if (handDiff == 0) {
				action.createGeneratedAction(1, "WIN", "0", this.playerBet).setActionValue("PUSH");
				action.createGeneratedAction(1, "WIN", "1", this.playerBet*2.0).setActionValue("PUSH");
			}
		}
		
		endRound(action);
	}

	public static boolean checkIfDealerQualifies(int dealerCombo,Integer[] dealerHand) {
		boolean dealerQualified = dealerCombo > 0 ||
				(handCardNumberCount(dealerHand, Card.ACE) > 0
					&& handCardNumberCount(dealerHand, Card.KING) > 0);
		return dealerQualified;
	}
	
	private void performJackpotBet(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Invalid state: Jackpot bet is only allowed before the round has started");
		}
		
		if (this.jackpotBet > 0.0) {
			throw new ActionFailedException("Jackpot bet already placed");
		}

		Double jackpotBet = this.options.getGameSettings().get("jackpot_bet", Double.class);
		
		if (jackpotBet == null) {
			throw new ActionFailedException("Game jackpot disabled");
		}
		
		if (Math.abs(jackpotBet - action.getAmount()) > 0.001) {
			throw new ActionFailedException("Invalid jackpot bet");
		}
		action.setAmount(jackpotBet);
		
		action.commit();
		this.jackpotBet = action.getAmount();
		
		JackpotsHandler.contributeToJackpot(this, action);
	}

	/**
	 * Stores any round-type-specific info into the Map provided.
	 */
	@Override
	protected void getExtraInfo(GamePlayer player, Map<String,Object> roundInfo, boolean firstTime) {
		if (roundInfo != null) {
			super.getExtraInfo(player, roundInfo, firstTime);
			
			if (firstTime) {
				Double jackpotBet = this.options.getGameSettings().get("jackpot_bet", Double.class);
				if (jackpotBet != null) {
					roundInfo.put("jackpot_bet", jackpotBet);
				}
			}
		}
	}
}
