package com.magneta.games.poker.videopoker;

import static com.magneta.games.configuration.poker.PokerConstants.FOUR_OF_A_KIND;
import static com.magneta.games.configuration.poker.PokerConstants.NONE;
import static com.magneta.games.configuration.poker.PokerConstants.ROYAL_FLUSH;
import static com.magneta.games.poker.PokerUtil.getPossibleCombination;
import static com.magneta.games.poker.PokerUtil.getWinningCombination;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.videopoker.VideoPokerPlusConfiguration;

@GameTemplate("VideoPokerPlus")
public class VideoPokerPlusRound extends AbstractGameRound {

	/**
	 * The constant action description value for the draw action.  
	 */
	private static final String DRAW_ACTION = "g:DRAW";
	/**
	 * The constant action description value for the hold action.  
	 */
	private static final String HOLD_ACTION = "g:HOLD";
	/**
	 * The constant action description value for the double up action.  
	 */
	private static final String DOUBLE_UP_ACTION = "g:DOUBLE_UP";
	/**
	 * The constant action description value for the end hold action.  
	 */
	private static final String END_HOLD_ACTION = "g:END_HOLD";
	/**
	 * The constant action description value for the take score action.  
	 */
	private static final String TAKE_SCORE_ACTION = "g:TAKE_SCORE";
	/**
	 * The constant action description value for the double half action.  
	 */
	private static final String DOUBLE_HALF_ACTION = "g:DOUBLE_HALF";
	/**
	 * The constant action description value for the double draw action.  
	 */
	private static final String DOUBLE_DRAW_ACTION = "g:DOUBLE_DRAW";
	/**
	 * The constant action description value for the hold suggest action.  
	 */
	private static final String HOLD_SUGGEST_ACTION = "g:HOLD_SUGGEST";
	/**
	 * The constant action description value for the win card action.  
	 */
	private static final String WIN_CARD_ACTION = "g:WIN_CARD";
	/**
	 * The constant action description value for the hold reason action.  
	 */
	private static final String HOLD_REASON_ACTION = "g:HOLD_REASON";
	/**
	 * The constant action description value for the possible action.  
	 */
	private static final String POSSIBLE_ACTION = "g:POSSIBLE";
	/**
	 * The constant action description value for the higher action.  
	 */
	/*private static final String HIGHER_ACTION = "g:HIGHER";*/
	/**
	 * The constant action description value for the higher action.  
	 */
	/*private static final String LOWER_ACTION = "g:LOWER";*/
	/**
	 * The constant action description value for the change card action.  
	 */
	/*private static final String CHANGE_CARD_ACTION = "g:CHANGE_CARD";*/
	/**
	 * The constant action description value for the red card action.  
	 */
	private static final String RED_ACTION = "g:RED";
	/**
	 * The constant action description value for the red card action.  
	 */
	private static final String BLACK_ACTION = "g:BLACK";

	private static final int NORMAL_MODE = 0;
	private static final int DOUBLE_UP_MODE = 1;

	private static final int JACKPOT_COMBO = ROYAL_FLUSH;
	public static final int DOUBLE_LIMIT = 4;
	private boolean allowDoubleUp;

	private GameActionRequestDefinition [] allowed_actions; 

	private Deck gameDeck;
	private Integer[] cards;
	private boolean[] holdCard;
	private GameRoundState roundState;
	private int gameMode;
	private double betAmount;
	private double multiplier;
	private int finalWinCombo;
	private int doubleTimes;
	private Integer[] doubleCards;
	private boolean doubleHalf;
	private boolean placedJackpotContrib;

	public VideoPokerPlusRound() {
		this.roundState = GameRoundState.ROUND_NOT_STARTED;
		this.gameMode = NORMAL_MODE;
		this.cards = new Integer[5];
		this.holdCard = new boolean[5];
		this.multiplier = 0.0;
		this.betAmount = 0.0;
		this.doubleCards = new Integer[DOUBLE_LIMIT+1];
		for (int i=0;i<doubleCards.length;i++){
			doubleCards[i] = -1;
		}
		this.finalWinCombo = NONE;
		this.doubleTimes = 0;
		this.doubleHalf = false;
		this.placedJackpotContrib = false;
	}
	
	@Override
	public VideoPokerPlusConfiguration getConfig() {
		return (VideoPokerPlusConfiguration)super.getConfig();
	}
	
	@Override
	public void roundInit() {
		this.allowDoubleUp = options.getGameSettings().get("allow_double_up", Boolean.TYPE);

		if (allowDoubleUp){
			allowed_actions = new GameActionRequestDefinition[] {
					/*Description        Area                      Amount*/ 
					new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED),
					new GameActionRequestDefinition(HOLD_ACTION,       ParameterType.REQUIRED,   ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(END_HOLD_ACTION,   ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(DOUBLE_UP_ACTION,  ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(TAKE_SCORE_ACTION, ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(RED_ACTION,        ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(BLACK_ACTION,      ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN)
			};
		} else {
			allowed_actions = new GameActionRequestDefinition[] {
					new GameActionRequestDefinition(BET_ACTION,        ParameterType.FORBIDDEN,  ParameterType.REQUIRED),
					new GameActionRequestDefinition(HOLD_ACTION,       ParameterType.REQUIRED,   ParameterType.FORBIDDEN),
					new GameActionRequestDefinition(END_HOLD_ACTION,   ParameterType.FORBIDDEN,  ParameterType.FORBIDDEN)
			};
		}
	}

	@Override
	public boolean isFinished() {
		return (roundState == GameRoundState.ROUND_FINISHED);
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			performBet(action);
		} else if (HOLD_ACTION.equals(action.getDescription())){
			performHold(action);
		} else if (END_HOLD_ACTION.equals(action.getDescription())){
			performEndHold(action);
		} else if (DOUBLE_UP_ACTION.equals(action.getDescription())){
			performDoubleUp(action); 
		} else if (TAKE_SCORE_ACTION.equals(action.getDescription())){
			performTakeScore(action);
		} else if (DOUBLE_HALF_ACTION.equals(action.getDescription())){
			performDoubleHalf(action);
			/*} else if (HIGHER_ACTION.equals(action.getDescription())){
            performHigher(action);
        } else if (LOWER_ACTION.equals(action.getDescription())){
            performLower(action);
        } else if (CHANGE_CARD_ACTION.equals(action.getDescription())){
            performChangeCard(action);*/
		} else if (RED_ACTION.equals(action.getDescription())){
			performColor(action, Card.RED_CARD);
		} else if (BLACK_ACTION.equals(action.getDescription())){
			performColor(action, Card.BLACK_CARD);
		}
	}

	/**
	 * Performs the action of the player predicting that the next card is higher valued that the current double card.
	 * @param action The action request object.
	 * @throws ActionFailedException
	 */
	/*public void performHigher(GameAction action) throws ActionFailedException{
        if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
            throw new ActionFailedException("Invalid state: HIGHER is only allowed after round has started and while in double mode.");
        } else if (doubleCards[doubleTimes] == -1){
            throw new ActionFailedException("Invalid state: must double to select higher!");
        }

        doubleHalf = true;

        /*FIXED ODD CODE*/
	/*com.magneta.games.fixedodd.VideoPokerPlusGuard.guardDouble(game.getId(), gameDeck, this.options.getOwner(), roundMultipliers, betAmount, multiplier, doubleCards, doubleTimes, true, options.getGameSettings(), action);

        int nextCard = gameDeck.getNextCard();
        GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, String.valueOf(doubleTimes+1), 0.0);
        drawAction.setActionValue(String.valueOf(nextCard));
        action.setActionValue(String.valueOf(nextCard));
        action.commit();

        int nextCardValue = VideoPokerDeck.getCardValue(nextCard);
        int doubleUpCardValue = VideoPokerDeck.getCardValue(doubleCards[doubleTimes]);

        if (nextCardValue >= doubleUpCardValue){
            if (nextCardValue > doubleUpCardValue){
                multiplier *= 2.0;
            }
            doubleTimes++;

            if ((doubleTimes >= DOUBLE_LIMIT) || (nextCardValue == doubleUpCardValue)){
                endRound(action);
            } else {
                doubleCards[doubleTimes] = nextCard;
                doubleHalf = false;
            }
        } else {
            multiplier = 0.0;
            doubleCards[doubleTimes] = -1;
            endRound(action);
        }
    }*/

	/**
	 * Performs the action of the player predicting that the next card is higher valued that the current double card.
	 * @param action The action request object.
	 * @throws ActionFailedException
	 */
	public void performColor(GameAction action, int color) throws ActionFailedException{
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: RED or BLACK is only allowed after round has started and while in double mode.");
		} else if (doubleCards[doubleTimes] == -1){
			throw new ActionFailedException("Invalid state: must double to select higher!");
		}

		doubleHalf = true;

		/*FIXED ODD CODE*/
		com.magneta.games.fixedodd.VideoPokerPlusGuard.guardDouble(game.getGameId(), gameDeck, this.options.getOwner(), getConfig().getPaytable(), betAmount, multiplier, doubleCards, doubleTimes, /*true,*/color, options.getGameSettings(), action);

		int nextCard = gameDeck.getNextCard();
		GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, String.valueOf(doubleTimes+1), 0.0);
		drawAction.setActionValue(String.valueOf(nextCard));
		action.setActionValue(String.valueOf(nextCard));
		action.commit();

		int nextCardColor = Card.getCardColour(nextCard);

		if (nextCardColor == color){
			multiplier *= 2.0;
			doubleTimes++;

			if (doubleTimes >= DOUBLE_LIMIT) {
				endRound(action);
			} else {
				doubleCards[doubleTimes] = nextCard;
				doubleHalf = false;
			}
		} else {
			multiplier = 0.0;
			doubleCards[doubleTimes] = -1;
			endRound(action);
		}
	}

	/**
	 * Performs the action of the player predicting that the next card is lower valued that the current double card..
	 * @param action The action request object.
	 * @throws ActionFailedException
	 */
	/*public void performLower(GameAction action) throws ActionFailedException{
        if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
            throw new ActionFailedException("Invalid state: HIGHER is only allowed after round has started and while in double mode.");
        } else if (doubleCards[doubleTimes] == -1){
            throw new ActionFailedException("Invalid state: must double to select lower!");
        }

        doubleHalf = true;

        /*FIXED ODD CODE*//*
        com.magneta.games.fixedodd.VideoPokerPlusGuard.guardDouble(game.getId(), gameDeck, this.options.getOwner(), roundMultipliers, betAmount, multiplier, doubleCards, doubleTimes, false, options.getGameSettings(), action);

        int nextCard = gameDeck.getNextCard();
        GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, String.valueOf(doubleTimes+1), 0.0);
        drawAction.setActionValue(String.valueOf(nextCard));
        action.setActionValue(String.valueOf(nextCard));
        action.commit();

        int nextCardValue = VideoPokerDeck.getCardValue(nextCard);
        int doubleUpCardValue = VideoPokerDeck.getCardValue(doubleCards[doubleTimes]);

        if (nextCardValue <= doubleUpCardValue){
            if (nextCardValue < doubleUpCardValue){
                multiplier *= 2.0;
            }
            doubleTimes++;

            if ((doubleTimes >= DOUBLE_LIMIT) || (nextCardValue == doubleUpCardValue)){
                endRound(action);
            } else {
                doubleCards[doubleTimes] = nextCard;
                doubleHalf = false;
            }
        } else {
            multiplier = 0.0;
            doubleCards[doubleTimes] = -1;
            endRound(action);
        }
    }*/

	/**
	 * Perform the double half action (user risks only 1/2 of his points - takes half of his winnings).
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performDoubleHalf(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: DOUBLE HALF is only allowed after round has started and in double up mode once.");
		} else if (doubleHalf){
			throw new ActionFailedException("Already doubled half!");
		}

		double half = (multiplier / 2.0) * betAmount;
		action.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, half);

		if ((doubleTimes == 0) && (doubleCards[doubleTimes] == -1)){
			action.commit();

			//reset and re-shuffle deck
			gameDeck = new Deck(1,0);
			gameDeck.shuffleCards(getRandom());

			com.magneta.games.fixedodd.VideoPokerPlusGuard.guardDoubleStart(game.getGameId(), gameDeck, options.getOwner(), betAmount, multiplier, doubleCards.length, options.getGameSettings(), action);

			GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, "0", 0.0);

			int nextCard = gameDeck.getNextCard();
			drawAction.setActionValue(String.valueOf(nextCard));

			try {
				drawAction.commit();

				doubleCards[doubleTimes] = nextCard;
			} catch (ActionFailedException e) {
				gameDeck.returnCard(nextCard);
				doubleCards[doubleTimes] = -1;
				logError("Error while doubling up.",e);
				throw e;
			}
		}

		action.commit();

		multiplier /= 2.0;
		doubleHalf = true;
	}

	/*
    public void performChangeCard(GameAction action) throws ActionFailedException {
        if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
            throw new ActionFailedException("Invalid state: CHANGE CARD is only allowed after round has started and in double up mode.");
        } else if (doubleTimes != 0 || doubleCards[doubleTimes] == -1){
            throw new ActionFailedException("Card can only be changed the first time and only once.");
        }

        com.magneta.games.fixedodd.VideoPokerPlusGuard.guardDoubleStart(game.getId(), gameDeck, options.getOwner(), betAmount, multiplier, doubleCards.length, options.getGameSettings(), action);

        GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, "0", 0.0);

        int nextCard = gameDeck.getNextCard();
        int previousCard = doubleCards[doubleTimes];
        drawAction.setActionValue(String.valueOf(nextCard));

        try {
            action.commit();
            doubleCards[doubleTimes] = nextCard;
        } catch (ActionFailedException e) {
            gameDeck.returnCard(nextCard);
            doubleCards[doubleTimes] = previousCard;
            Log.error("Error while changing double card.",e);
            throw e;
        }
    }*/

	/**
	 * Performs the double up action.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performDoubleUp(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: DOUBLE UP is only allowed after round has started.");
		} else if (doubleCards[doubleTimes] != -1 || doubleTimes != 0){
			throw new ActionFailedException("Already doubled up.");
		}

		action.commit();

		//reset and re-shuffle deck
		gameDeck = new Deck(1,0);
		gameDeck.shuffleCards(getRandom());

		com.magneta.games.fixedodd.VideoPokerPlusGuard.guardDoubleStart(game.getGameId(), gameDeck, options.getOwner(), betAmount, multiplier, doubleCards.length, options.getGameSettings(), action);

		GameAction drawAction = action.createGeneratedAction(null, 1, DOUBLE_DRAW_ACTION, "0", 0.0);

		int nextCard = gameDeck.getNextCard();
		drawAction.setActionValue(String.valueOf(nextCard));

		try {
			action.commit();

			doubleCards[doubleTimes] = nextCard;
		} catch (ActionFailedException e) {
			gameDeck.returnCard(nextCard);
			doubleCards[doubleTimes] = -1;
			logError("Error while doubling up.",e);
			throw e;
		}
	}

	/**
	 * Ends the current round if the player is in double up mode.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performTakeScore(GameAction action) throws ActionFailedException{
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != DOUBLE_UP_MODE)) {
			throw new ActionFailedException("Invalid state: TAKE SCORE is only allowed after round has started and while in double up mode.");
		}

		try {
			action.commit();
			endRound(action);
		} catch (ActionFailedException e) {
			logError("Error while taking score.",e);
			throw new ActionFailedException("Could not take score: "+e.getMessage());
		}
	}

	/**
	 * Performs the end hold action ending the process of choosing the cards to hold.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performEndHold(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != NORMAL_MODE)) {
			throw new ActionFailedException("Invalid state: END HOLD is only allowed after round has started.");
		}

		/* FIXED ODD CODE */
		com.magneta.games.fixedodd.VideoPokerPlusGuard.guardSecondHand(game.getGameId(), this.gameDeck, this.options.getOwner(), getConfig(), this.betAmount, this.cards, this.holdCard, options.getGameSettings(), action);        

		for (int i=0;i<cards.length;i++){
			if (!holdCard[i]){
				int nextCard = gameDeck.getNextCard();

				GameAction drawAction = action.createGeneratedAction(null, 1, DRAW_ACTION, String.valueOf(i), 0.0);
				drawAction.setActionValue(String.valueOf(nextCard));

				cards[i] = nextCard;
			}
		}

		action.commit();

		boolean[] keepCard = new boolean[cards.length];

		finalWinCombo = getWinningCombination(cards, keepCard, Card.JACK);

		if (finalWinCombo == NONE){
			multiplier = 0.0;
			endRound(action);
		} else {
			multiplier = getMultiplier(finalWinCombo);

			for (int i=0;i<keepCard.length;i++){
				if (keepCard[i]){
					GameAction winCard = action.createGeneratedAction(null, 0, WIN_CARD_ACTION, String.valueOf(i), 0.0);
					winCard.setActionValue(String.valueOf(finalWinCombo));
				}
			}

			action.commit();

			if ((finalWinCombo == JACKPOT_COMBO) && (placedJackpotContrib)){
				GameAction roundFinish = action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);

				GameAction winAction = roundFinish.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, null, 0.0);
				winAction.setAmountFunction(
						"handle_jackpot_win("+this.options.getOwner()+","+this.game.getGameId()+","+getTableId()+","+getId()+")");
				winAction.setActionValue("Jackpot");

				try {
					roundFinish.commit();
					roundState = GameRoundState.ROUND_FINISHED;
				} catch (ActionFailedException e) {
					/* The jackpot does not exist */
					logDebug("Jackpot conditions met but no jackpot currently exists.");
					action.removeGeneratedAction(roundFinish);
					if ((finalWinCombo >= FOUR_OF_A_KIND) || (!allowDoubleUp)){
						endRound(action);
					} else {
						gameMode = DOUBLE_UP_MODE;
					}
				}
			} else if ((finalWinCombo >= FOUR_OF_A_KIND) || (!allowDoubleUp)){
				endRound(action);
			} else {
				gameMode = DOUBLE_UP_MODE;
			}
		}
	}

	/**
	 * Ends the round.
	 * @param parentAction The action that caused the round to end.
	 * @throws ActionFailedException 
	 */
	private void endRound(GameAction parentAction) throws ActionFailedException {
		if (roundState != GameRoundState.ROUND_FINISHED){

			logDebug("endRound() was called for round "+getId()+" caused by: "+parentAction);

			GameAction roundFinish = parentAction.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);

			if (multiplier > 0.0){
				/*if (doubleCards[DOUBLE_LIMIT] != -1){
                    CardsState state = CardsState.getCardsState(doubleCards);
                    boolean winCard[] = new boolean[doubleCards.length];
                    int doubleCombo = getWinningCombination(state,winCard);

                    if (doubleCombo >= STRAIGHT){
                        multiplier += roundMultipliers[doubleCombo];
                        for (int i=0;i<winCard.length;i++){
                            if (winCard[i]){
                                GameAction winCardAction = roundFinish.createGeneratedAction(null, 0, WIN_CARD_ACTION, String.valueOf(i), 0.0);
                                winCardAction.setActionValue(String.valueOf(doubleCombo));
                            }
                        }
                    }
                }*/

				double winAmount = multiplier * betAmount;
				roundFinish.createGeneratedAction(parentAction.getPlayer(), 1, WIN_ACTION, null, winAmount);
			}

				parentAction.commit(true);
				roundState = GameRoundState.ROUND_FINISHED;

		} else {
			throw new IllegalStateException("Method endRound() already called once.");
		}
	}

	/**
	 * Performs the hold action, telling the round that the card in the given area should not be replaced.
	 * @param action The action request.
	 * @throws ActionFailedException 
	 */
	private void performHold(GameAction action) throws ActionFailedException {
		if ((roundState != GameRoundState.ROUND_STARTED) || (gameMode != NORMAL_MODE)) {
			throw new ActionFailedException("Invalid state: Hold is only allowed after round has started.");
		}

		int area = Integer.parseInt(action.getArea());

		if ((area < 0) || (area >= holdCard.length)) {
			throw new ActionFailedException("Invalid area for hold.");
		}

		if (!holdCard[area]){
			action.commit();
			holdCard[area] = true;
		}
	}

	/**
	 * Performs the BET action.
	 * @param action The bet GameAction to update.
	 */
	private void performBet(GameAction action) throws ActionFailedException {

		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
		} else if (betAmount > 0.0){
			throw new ActionFailedException("Bet failed: already placed bet!");
		}

		checkBet(action);

		action.setRoundStart();
		action.commit();
		betAmount = action.getAmount();

		this.placedJackpotContrib = JackpotsHandler.contributeToJackpot(this, action);

		startRound(action);
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException { 
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(allowed_actions, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (roundState == GameRoundState.ROUND_FINISHED) {
			throw new ActionFailedException("Round has finished.");
		} else if ((roundState == GameRoundState.ROUND_NOT_STARTED) && (!BET_ACTION.equals(request.getDescription()))) {
			throw new ActionFailedException("Round has not started yet.");
		} else if ((roundState == GameRoundState.ROUND_NOT_STARTED) && (BET_ACTION.equals(request.getDescription()))) {
			/* Avoid the last check */
		}  else if ((gameMode == NORMAL_MODE) && 
				(!HOLD_ACTION.equals(request.getDescription())) &&
				(!END_HOLD_ACTION.equals(request.getDescription())) && 
				(!DOUBLE_UP_ACTION.equals(request.getDescription()))) {
			throw new ActionFailedException("Cannot perform action at this time.");
		}
	}
	
	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());
		
		if (result == null) {
			return;
		}
		
		List<Integer> topCards = new ArrayList<Integer>();
		String[] sReelStops = result.split(" ");

		for (int i=0; i < sReelStops.length; i++) {
			try {
				topCards.add(Integer.parseInt(sReelStops[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}
		
		if (topCards.isEmpty()) {
			return;
		}
		
		gameDeck.setupDeck(topCards);
	}

	/**
	 * Starts the round.
	 * @param parentAction The action that caused the round start.
	 * @throws ActionFailedException
	 */
	private void startRound(GameAction parentAction) throws ActionFailedException {
		if (roundState != GameRoundState.ROUND_NOT_STARTED) {
			throw new ActionFailedException("Round already started");
		}

		gameDeck = new Deck(1,0);

		gameDeck.shuffleCards(getRandom());
		/*FIXED ODD CODE*/
		com.magneta.games.fixedodd.VideoPokerPlusGuard.guardFirstHand(game.getGameId(), gameDeck, this.options.getOwner(), getConfig(), betAmount, options.getGameSettings(), parentAction);
		setDeck();
		roundState = GameRoundState.ROUND_STARTED;
		
		for (int i=0;i<cards.length;i++){
			int nextCard = gameDeck.getNextCard();
			GameAction drawAction = parentAction.createGeneratedAction(null, 1, DRAW_ACTION, String.valueOf(i), 0.0);
			drawAction.setActionValue(String.valueOf(nextCard));

			cards[i] = nextCard;
		}

		boolean[] keepCard = new boolean[cards.length];

		int combo = getWinningCombination(cards, keepCard, Card.JACK);

		if (combo != NONE){
			for (int i=0;i<keepCard.length;i++){
				if (keepCard[i]){
					parentAction.createGeneratedAction(null, 0, HOLD_SUGGEST_ACTION, String.valueOf(i), 0.0);
				}
			}

			GameAction holdReason = parentAction.createGeneratedAction(null, 0, HOLD_REASON_ACTION, null, 0.0);
			holdReason.setActionValue(String.valueOf(combo));
		} else {
			boolean[] possibleHold = new boolean[cards.length];
			int possibleCombo = getPossibleCombination(cards, possibleHold, Card.JACK);
			
			for (int i=0;i<possibleHold.length;i++){
				if (possibleHold[i]){
					parentAction.createGeneratedAction(null, 0, HOLD_SUGGEST_ACTION, String.valueOf(i), 0.0);
				}
			}
			
			if (possibleCombo != NONE) {
				GameAction possibleAction = parentAction.createGeneratedAction(null, 0, POSSIBLE_ACTION, null, 0.0);
				possibleAction.setActionValue(String.valueOf(possibleCombo));
			}
			
		}

		parentAction.commit();
	}

	private double getMultiplier(int combo){
		return getConfig().getMultiplier(combo);
	}

	@Override
	protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
		super.getExtraInfo(player, roundInfo, firstTime);

		if ((roundInfo != null) && firstTime) {
			double[] paytable = getConfig().getPaytable();
			for (int i=1; i<paytable.length; i++){
				roundInfo.put("multiplier_"+i, paytable[i]);
			}
		}
	}
}
