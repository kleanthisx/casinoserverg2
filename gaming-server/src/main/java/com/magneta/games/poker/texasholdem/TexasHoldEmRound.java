package com.magneta.games.poker.texasholdem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.DefaultGameActionFilter;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionFilter;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.PokerConstants;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration.TexasPokerSideGame;
import com.magneta.games.configuration.poker.texasholdem.WinCombination;
import com.magneta.games.fixedodd.HoldEmHandGuardParams;
import com.magneta.games.fixedodd.HoldEmPokerGuard;
import com.magneta.games.poker.PokerUtil;
import com.magneta.games.poker.PokerUtil.CardMatcher;
import com.magneta.games.poker.PokerUtil.CardNumberMatcher;
import com.magneta.games.poker.PokerUtil.ComboState;
import com.magneta.games.poker.PokerUtil.SingleCardMatcher;

/**
 * BlackjackRound is the blackjack game-specific GameRound implementation.
 */
@GameTemplate("TexasHoldEm")
public class TexasHoldEmRound extends AbstractGameRound {

	/**
	 * The constant action description value for the fold action.  
	 */
	private static final String FOLD_ACTION = "g:FOLD";

	/**
	 * The constant action description value for the hit action.  
	 */
	private static final String HIT_ACTION = "g:HIT";

	/**
	 * The constant action description value for the fold action.  
	 */
	private static final String RAISE_ACTION = "g:RAISE";

	/**
	 * The constant action description value for the turn check action.  
	 */
	private static final String TURN_ACTION = "g:TURN";

	/**
	 * The constant action description value for the turn bet action.  
	 */
	private static final String RIVER_ACTION = "g:RIVER";

	private static final String SIDE_BET_ACTION = "g:SIDE_BET";

	private static final int PLAYER_SEAT = 1;
	private static final int DEALER_SEAT = 0;
	private static final int COMMON_SEAT = 2;
	
	private static final Logger log = LoggerFactory.getLogger(TexasHoldEmRound.class);

	private static enum TexasState {

		INITIAL,
		DEAL,
		FLOP,
		TURN,
		RIVER,
		FINISHED;

	}
	/*                                  	  desc         		area                     amount */
	private static final GameActionRequestDefinition [] ALLOWED_ACTIONS = {
		new GameActionRequestDefinition(BET_ACTION		,  ParameterType.FORBIDDEN, ParameterType.REQUIRED),
		new GameActionRequestDefinition(SIDE_BET_ACTION	,  ParameterType.FORBIDDEN, ParameterType.REQUIRED),
		new GameActionRequestDefinition(RAISE_ACTION	,  ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(FOLD_ACTION		,  ParameterType.FORBIDDEN, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(TURN_ACTION		,  ParameterType.REQUIRED, ParameterType.FORBIDDEN),
		new GameActionRequestDefinition(RIVER_ACTION	,  ParameterType.REQUIRED, ParameterType.FORBIDDEN)
	};

	private final Deck roundDeck;
	private TexasState roundState;

	private Integer[] dealerHand;
	private Integer[] playerHand;
	private Integer[] commonHand;
	private double playerBet;
	private double sideGameBet;
	boolean betTurn = false;
	boolean betRiver = false;



	/**
	 * Constructs a new BlackjackRound.
	 * @param game The blackjack game held by the round.
	 * @param options The round's options values.
	 * @param tableId The ID of the table. 
	 * @param roundId The ID of the round to construct.
	 * @param seed The seed to used for the random generator.
	 */
	public TexasHoldEmRound() {
		roundDeck = new Deck(1,0);

		dealerHand = new Integer[2];
		playerHand = new Integer[2];
		commonHand = new Integer[5];

		roundState = TexasState.INITIAL;
	}
	
	@Override
	public TexasHoldemConfiguration getConfig() {
		return (TexasHoldemConfiguration)super.getConfig();
	}

	@Override
	public boolean isFinished() {
		return roundState == TexasState.FINISHED;
	}

	@Override
	public GameActionFilter getActionFilter() {
		return new DefaultGameActionFilter(true);
	}

	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());

		if (result == null) {
			return;
		}

		List<Integer> topCards = new ArrayList<Integer>();
		String[] sReelStops = result.split(" ");

		for (int i=0; i < sReelStops.length; i++) {
			try {
				topCards.add(Integer.parseInt(sReelStops[i]));
			} catch (NumberFormatException e) {
				log.error("Invalid number while parsing fixed cards");
				continue;
			}
		}



		if (topCards.isEmpty()) {
			return;
		}

		roundDeck.setupDeck(topCards);
	}

	/**
	 * Starts the round.
	 * @param parentAction The action which caused the START action.
	 * @throws ActionFailedException 
	 */
	private void startRound(GameAction dealAction) throws ActionFailedException {
		if (roundState != TexasState.INITIAL) {
			throw new ActionFailedException("Round already started");
		}

		if (this.playerBet < 0.01) {
			throw new ActionFailedException("No bets have been placed");
		}


		/* Shuffle cards */
		roundDeck.shuffleCards(getRandom());
		setDeck();
		
		/*FIXED ODD CODE*/
		
		for (int i= 0; i < playerHand.length; i++) {
			playerHand[i] = roundDeck.getNextCard();	
		}

		for (int i= 0; i < dealerHand.length; i++) {
			dealerHand[i] = roundDeck.getNextCard();			
		}

		for (int i= 0; i < commonHand.length; i++) {
			commonHand[i]= roundDeck.getNextCard();
		}		
		
		
		HoldEmHandGuardParams params = new HoldEmHandGuardParams();
		params.setGameID(game.getGameId());
		params.setPlayerhand(playerHand);
		params.setDealerhand(dealerHand);
		params.setCommonhand(commonHand);
		params.setOwnerID(this.options.getOwner());
		params.setConfig(getConfig());
		params.setBetAmount(playerBet);
		params.setSidebet(sideGameBet);
		params.setSettings(options.getGameSettings());
		params.setParentAction(dealAction);
		
		HoldEmPokerGuard.guardFirstHand(params);
		
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		// the player cards to stringbuilder
		for (Integer card: playerHand) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(' ');
			}
			sb.append(card.toString());
		}

		Integer[] tmpcommonhand = new Integer[]{};
		boolean[] cardsUsed = new boolean[(playerHand.length+tmpcommonhand.length)];
		Integer combo = evaluateSeatFullHand(playerHand,tmpcommonhand,cardsUsed);
		String comb = combo.toString();
		dealAction.createGeneratedAction(PLAYER_SEAT, HIT_ACTION, comb, 0.0).setActionValue(sb.toString());
		dealAction.commit();
		roundState = TexasState.DEAL;
	}


	/**
	 * Ends the round.
	 * @param action The action that caused the round end action.
	 * @throws ActionFailedException 
	 */
	private void endRound(GameAction action) throws ActionFailedException {
		if (roundState == TexasState.FINISHED) {
			throw new ActionFailedException("Round already finished");
		}

		roundState = TexasState.FINISHED;

		/* Check for sidegame combinations */
		if (this.sideGameBet > 0.001) {	
			TexasPokerSideGame combination = checkForSidegameCombination(playerHand, dealerHand);
			if(combination != null) {
				WinCombination winCombo = getConfig().getSideWinCombination(combination);
				action.createGeneratedAction(1, "WIN", "4", this.sideGameBet*(winCombo.getAmount() + 1.0)).setActionValue(Integer.toString(combination.getCode()));
			}
		}

		action.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
		action.commit(true);
	}

	/**
	 * Validates an action request.
	 * @param request The GameActionRequest to validate
	 */
	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {
		/* Check if the action is "syntacticaly" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (roundState == TexasState.FINISHED) {
			throw new ActionFailedException("Round has finished.");
		}

		if ((roundState == TexasState.INITIAL) 
				&& (!BET_ACTION.equals(request.getDescription()))
				&& (!SIDE_BET_ACTION.equals(request.getDescription())) ) {
			throw new ActionFailedException("Round has not started yet.");
		}

		if (request.getSeatId() != 1) {
			throw new ActionFailedException("Invalid seat number!");
		}
	}

	/**
	 * Performs the requested action.
	 * @param player The player that requested the action.
	 * @param action The action to execute.
	 * @param results A Map used to save any feedback from the validation process. 
	 * In case the request fails, please find the cause, stored as a String, in the ERROR field. 
	 * Also find the card(s) drawn from the HIT, SPLIT or DOUBLE action in the cards_0 and cards_1 
	 * (OPTIONAL) fields.
	 */
	@Override
	protected void performAction(GameAction action) throws ActionFailedException {

		if (action.getSeatId() != 1) {
			throw new ActionFailedException("Invalid seat id");
		}

		if (BET_ACTION.equals(action.getDescription())) {
			performBet(action);
		} else if (SIDE_BET_ACTION.equals(action.getDescription())) {
			performSideBet(action);
		} else if (FOLD_ACTION.equals(action.getDescription())) {
			performFold(action);
		} else if (RAISE_ACTION.equals(action.getDescription())) {
			performRaise(action);
		} else if (TURN_ACTION.equals(action.getDescription())) {
			performTurn(action);
		} else if (RIVER_ACTION.equals(action.getDescription())) {
			performRiver(action);
		}
	}

	private void performSideBet(GameAction action) throws ActionFailedException {

		if (roundState != TexasState.INITIAL) {
			throw new ActionFailedException("Invalid state: Side bet is only allowed before the round has started");
		}

		if (this.sideGameBet > 0.0) {
			throw new ActionFailedException("Side bet already placed");
		}

		if (!action.isLoaded()) {
			Double minBet = this.options.getGameSettings().get("min_side_bet", Double.class);
			Double maxBet = this.options.getGameSettings().get("max_side_bet", Double.class);

			if (minBet == null || maxBet == null) {
				throw new ActionFailedException("Side bet disabled");
			}

			if (action.getAmount() < minBet - 0.001 || action.getAmount() > maxBet + 0.001) {
				throw new ActionFailedException("Invalid side bet amount");
			}
		}

		action.commit();
		sideGameBet = action.getAmount();		
	}

	/**
	 * Performs the BET action.
	 * @param action The bet action GameAction to update.
	 * @param results The Map to save any errors in the ERROR field.
	 */
	private void performBet(GameAction action) throws ActionFailedException {

		if (roundState != TexasState.INITIAL) {
			throw new ActionFailedException("Invalid state: Bet is only allowed before the round has started");
		}

		checkBet(action);

		if (this.playerBet > 0.0) {
			throw new ActionFailedException("Bet already placed");
		}

		action.setRoundStart();
		action.commit();
		playerBet = action.getAmount();
		startRound(action);
	}

	private void performRaise(GameAction action) throws ActionFailedException {

		if (roundState != TexasState.DEAL) {
			throw new ActionFailedException("Invalid state: Fold is only allowed after the round has started");
		}

		action.setAmount(playerBet * 2.0);
		action.commit();

		Integer[] tmpcommonhand = Arrays.copyOfRange(commonHand, 0, 3);

		/* Send the first 3 flop cards */
		StringBuilder sb = new StringBuilder();
		
		boolean[] cardsUsed = new boolean[(playerHand.length+tmpcommonhand.length)];
		Integer combo = evaluateSeatFullHand(playerHand,tmpcommonhand,cardsUsed);
		String comb = combo.toString();
		boolean isFirst=true;
		// the player cards to stringbuilder
		for (int i=0; i < 3; i++) {
			if(isFirst){
				isFirst=false;
			}else{
				sb.append(' ');
			}
			sb.append(commonHand[i]);
		}

		action.createGeneratedAction(COMMON_SEAT, HIT_ACTION, comb,0.0).setActionValue(sb.toString());
		action.commit();
		roundState = TexasState.FLOP;
	}

	public int evaluateSeatFullHand(Integer[] playerhand_prm,
			Integer[] commonhand_prm,boolean[] cardsUsed) {

		Integer[] mergedHand = new Integer[(playerhand_prm.length+commonhand_prm.length)];
		//merging hands
		for(int i=0;i<playerhand_prm.length;i++){
			mergedHand[i] = playerhand_prm[i];
		}
		for(int i=playerhand_prm.length;i<(playerhand_prm.length+commonhand_prm.length);i++){
			mergedHand[i] = new Integer(commonhand_prm[i-playerhand_prm.length]);
		}
		//evaluating hands
		int playerCombo = getWinningCombination(mergedHand, cardsUsed);
		return playerCombo;
	}

	private void performFold(GameAction action) throws ActionFailedException {

		if (roundState != TexasState.DEAL) {
			throw new ActionFailedException("Invalid state: Fold is only allowed after the round has started");
		}

		boolean isFirst = true;
		/* Send the 2 cards of the DEALER */
		StringBuilder sb = new StringBuilder();
		for (Integer card:dealerHand) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(' ');
			}
			sb.append(card.toString());
		}
		action.createGeneratedAction(DEALER_SEAT, HIT_ACTION, "0", 0.0).setActionValue(sb.toString());
		endRound(action);
	}

	private void performTurn(GameAction action) throws ActionFailedException {
		if (roundState != TexasState.FLOP) {
			throw new ActionFailedException("Invalid state: Turn bet is only allowed after the round has started");
		}

		if ("1".equals(action.getArea())) {
			action.setAmount(playerBet * 1.0);
			action.commit();
			betTurn = true;
		} else if (!"0".equals(action.getArea())) {
			throw new ActionFailedException("Invalid area");
		}

		/* Send the 4th flop card the dealer received */
		StringBuilder sb = new StringBuilder();
		sb.append(commonHand[3]);
		Integer[] tmpcommonhand = Arrays.copyOfRange(commonHand, 0, 4);
		
		boolean[] cardsUsed = new boolean[(playerHand.length+tmpcommonhand.length)];
		Integer combo = evaluateSeatFullHand(playerHand,tmpcommonhand,cardsUsed);
		String comb = combo.toString();
		action.createGeneratedAction(COMMON_SEAT, HIT_ACTION, comb, 0.0).setActionValue(sb.toString());
		action.commit();

		roundState = TexasState.TURN;
	}

	private void performRiver(GameAction action) throws ActionFailedException {
		if (roundState != TexasState.TURN) {
			throw new ActionFailedException("Invalid state: River bet is only allowed after the round has started");
		}

		if ("1".equals(action.getArea())) {
			action.setAmount(playerBet * 1.0);
			action.commit();
			betRiver = true;
		} else if (!"0".equals(action.getArea())) {
			throw new ActionFailedException("Invalid area");
		}

		/* Send the 5th flop card */
		boolean[] cardsUsed = new boolean[(playerHand.length+commonHand.length)];
		Integer combo = evaluateSeatFullHand(playerHand,commonHand,cardsUsed);
		String comb = combo.toString();
		StringBuilder sb = new StringBuilder();
		sb.append(commonHand[4]);
		action.createGeneratedAction(COMMON_SEAT, HIT_ACTION, comb, 0.0).setActionValue(sb.toString());

		/* Send the 2 cards of the DEALER */
		boolean isFirst = true;
		sb = new StringBuilder();
		for (Integer card:dealerHand) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(' ');
			}
			sb.append(card.toString());
		}

		// evaluating dealer hand
		Integer[] dealerFullHand = new Integer[7];
		for(int i=0;i<5;i++){
			dealerFullHand[i] = commonHand[i];
		}
		for(int i=0;i<2;i++){
			dealerFullHand[i+5] = dealerHand[i];
		}
		boolean[] dealerWinningCards = new boolean[dealerFullHand.length];
		Integer dealerCombo = PokerUtil.getWinningCombination(dealerFullHand, dealerWinningCards, 1);

		action.createGeneratedAction(DEALER_SEAT, HIT_ACTION, dealerCombo.toString(), 0.0).setActionValue(sb.toString());
		evaluateHands(action); 
		endRound(action);
	}

	private void evaluateHands(GameAction action) {

		Integer[] dealerFullHand = new Integer[7];
		Integer[] playerFullHand = new Integer[7];

		for(int i=0;i<5;i++){
			dealerFullHand[i] = commonHand[i];
			playerFullHand[i] = commonHand[i];
		}

		for(int i=0;i<2;i++){
			dealerFullHand[i+5] = dealerHand[i];
			playerFullHand[i+5] = playerHand[i];
		}
		// evaluating player hand
		boolean[] playerWinningCards = new boolean[playerFullHand.length];
		int playerCombo = getWinningCombination(playerFullHand, playerWinningCards);

		WinCombination combo = getConfig().getWinCombination(playerCombo);

		double multiplier = combo.getAmount();
		
		StringBuilder indexes = new StringBuilder("");
		int handDiff = compareHands(dealerFullHand, playerFullHand,indexes);

		/* Player won */
		if (handDiff < 0) {
			//here we handle the ante
			if(playerCombo > PokerConstants.THREE_OF_A_KIND ) {
				action.createGeneratedAction(PLAYER_SEAT, "WIN", "0"+indexes, this.playerBet*2.0).setActionValue(getActionArea(playerCombo, playerWinningCards));
			}else{
				action.createGeneratedAction(PLAYER_SEAT, "WIN", "0"+indexes, this.playerBet).setActionValue("PUSH");
			}
			// raise/call
			action.createGeneratedAction(PLAYER_SEAT, "WIN", "1", (this.playerBet * 2.0) * (multiplier + 1.0)).setActionValue(getActionArea(playerCombo, playerWinningCards));
			//turn
			if(this.betTurn)
				action.createGeneratedAction(PLAYER_SEAT, "WIN", "2", (this.playerBet) * (multiplier + 1.0)).setActionValue(getActionArea(playerCombo, playerWinningCards));
			//river
			if(this.betRiver)
				action.createGeneratedAction(PLAYER_SEAT, "WIN", "3", (this.playerBet) * (multiplier + 1.0)).setActionValue(getActionArea(playerCombo, playerWinningCards));
		} 
		/* Push */
		else if (handDiff == 0) {
			action.createGeneratedAction(PLAYER_SEAT, "WIN", "0"+indexes, this.playerBet).setActionValue("PUSH");
			action.createGeneratedAction(PLAYER_SEAT, "WIN", "1", this.playerBet*2.0).setActionValue("PUSH");
			if(this.betTurn)
			action.createGeneratedAction(PLAYER_SEAT, "WIN", "2", this.playerBet).setActionValue("PUSH");
			if(this.betRiver)
				action.createGeneratedAction(PLAYER_SEAT, "WIN", "3", this.playerBet).setActionValue("PUSH");
		}
	}

	private static class CardComparator implements Comparator<Integer> {

		private final Integer[] hand;
		private final boolean[] cardsInvolved;
		private final boolean involved;

		public CardComparator(Integer[] hand, boolean[] cardsInvolved, boolean involved) {
			this.hand = hand;
			this.cardsInvolved = cardsInvolved;
			this.involved = involved;
		}

		private int countCardNumber(int number) {
			int count = 0;
			for (int i=0; i < hand.length; i++) {
				if (!cardsInvolved[i] == involved) {
					continue;
				}

				if (Card.getCardNumber(hand[i]) == number) {
					count++;
				}
			}

			return count;
		}

		@Override
		public int compare(Integer card0, Integer card1) {
			if (involved) {
				int frequencyDiff = countCardNumber(Card.getCardNumber(card1)) - countCardNumber(Card.getCardNumber(card0));

				if (frequencyDiff != 0) {
					return frequencyDiff;
				}
			}

			return PokerUtil.getCardValue(card1) - PokerUtil.getCardValue(card0);
		}

	}

	/**
	 * Sorts hands as follows:
	 * - Cards involved in the winning combination are first non-involved follow.
	 * - Cards which appear more frequently have priortity.
	 * - Highest value cards have priority over lowest value.
	 * @param hand
	 * @param cardsInvolved
	 * @param handState
	 */
	private Integer[] sortHand(Integer[] origHand, boolean[] cardsInvolved) {		
		Integer[] hand = new Integer[origHand.length];

		int involvedCount = 0;
		boolean[] cardsInvolvedTmp = new boolean[cardsInvolved.length]; 

		/* Add involved cards on top */
		for (int i=0; i < origHand.length; i++) {
			if (!cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount] = origHand[i];
			cardsInvolvedTmp[involvedCount]=true;
			involvedCount++;
		}

		/* Add the remainingCards */
		int currIndex = 0;
		for (int i=0; i < origHand.length; i++) {
			if (cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount + currIndex] = origHand[i];
			cardsInvolvedTmp[currIndex+involvedCount]=false;
			currIndex++;
		}


		if (involvedCount > 0) {
			Arrays.sort(hand, 0, involvedCount, new CardComparator(hand, cardsInvolvedTmp, true));
		}

		if (involvedCount < hand.length) {
			Arrays.sort(hand, involvedCount, hand.length, new CardComparator(hand, cardsInvolvedTmp, false));
		}
		
		hand = Arrays.copyOfRange(hand, 0, 5);
		return hand;
	}

	private int getWinningCombination(Integer[] cards, boolean[] cardsInvolved) {
		return PokerUtil.getWinningCombination(cards, cardsInvolved, 0);
	}

	public int compareHands(Integer[] dealerHand, Integer[] playerHand,StringBuilder indexes) {
		boolean[] dealerCardsInvolved = new boolean[dealerHand.length];
		boolean[] playerCardsInvolved = new boolean[playerHand.length];


		int dealerCombo = getWinningCombination(dealerHand, dealerCardsInvolved);
		int playerCombo = getWinningCombination(playerHand, playerCardsInvolved);

		Integer[] dealerNewHand = sortHand(dealerHand, dealerCardsInvolved);
		Integer[] playerNewHand = sortHand(playerHand, playerCardsInvolved);

		for (int i=0;i<playerHand.length;i++) {
			for (Integer x :playerNewHand){
				if (playerHand[i].intValue()==x.intValue()){
					indexes.append(" "+i);
				}
			}
		}
		
		if (dealerCombo != playerCombo) {
			return dealerCombo - playerCombo;
		}

		
		
		int tmpdif = 0;
		switch (playerCombo) {
		case PokerConstants.STRAIGHT_FLUSH:
		case PokerConstants.STRAIGHT:
			if (PokerUtil.getCardValue(dealerNewHand[1]) == PokerUtil.getCardValue(playerNewHand[1])) {
				tmpdif =  (PokerUtil.getCardValue(dealerNewHand[0]) % 14) - (PokerUtil.getCardValue(playerNewHand[0]) % 14);
			}

			if(tmpdif!=0)
				return tmpdif;
		}

		for (int i=0; i < dealerNewHand.length; i++) {

			int cardDiff = PokerUtil.getCardValue(dealerNewHand[i]) - PokerUtil.getCardValue(playerNewHand[i]);

			if (cardDiff != 0) {
				return cardDiff;
			}
		}

		return 0;
	}

	private String getActionArea(int combo, boolean[] involvedCards) {
		StringBuilder sb = new StringBuilder();

		sb.append(combo);

		for (int i=0; i < involvedCards.length; i++) {
			if (involvedCards[i]) {
				sb.append(' ');
				sb.append(i);
			}
		}
		return sb.toString();
	}

	/**
	 * Stores any round-type-specific info into the Map provided.
	 */
	@Override
	protected void getExtraInfo(GamePlayer player, Map<String,Object> roundInfo, boolean firstTime) {
		super.getExtraInfo(player, roundInfo, firstTime);

		if (firstTime) {
			Double minSideBet = this.options.getGameSettings().get("min_side_bet", Double.class);
			if (minSideBet != null) {
				roundInfo.put("min_side_bet", minSideBet);
			}

			Double maxSideBet = this.options.getGameSettings().get("max_side_bet", Double.class);
			if (maxSideBet != null) {
				roundInfo.put("max_side_bet", maxSideBet);
			}
		}
	}

	/**
	 * Checks if the cardsToFind are present in the given cardHand. 
	 * Returns 0 if all the cards are resent,otherwise returns the 
	 * mumber of cards that are mising from the hand to comlpete 
	 * the cards to find. The check by race as well enforces that
	 * the cards are  race as well.
	 * 
	 * @param cardHand
	 * @param cardsToFind
	 * @param cardsUsed
	 * @param cardsFound
	 * @param checkByRaceAsWell
	 * @return
	 */
	public static TexasPokerSideGame checkForSidegameCombination(Integer[] playerHand,Integer[] dealerHand) {

		ComboState state = new ComboState();
		PokerUtil.CardMatcher[] cardsToFind;
		int cardsUsed=2;

		boolean playerHasAces =PokerUtil.checkPair(state,playerHand, 15);
		boolean dealerHasAces =PokerUtil.checkPair(state,dealerHand, 15);

		//check if both have aces
		if(playerHasAces && dealerHasAces)
			return TexasPokerSideGame.AA_AA;

		if(playerHasAces) 
			return TexasPokerSideGame.AA;

		/* A K suited */
		for (int race=0; race < 4; race++) {
			cardsToFind = new CardMatcher[] {
					new PokerUtil.SingleCardMatcher(Card.getCard(Card.KING, race)),
					new SingleCardMatcher(Card.getCard(Card.ACE, race))
			};
			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.AK_SUITED;
			}
		}

		/* A Q or A J suited */
		for (int race=0; race < 4; race++) {
			for(int c =Card.JACK; c<Card.KING; c++){
				cardsToFind = new CardMatcher[] {
						new SingleCardMatcher(Card.getCard(c, race)),
						new SingleCardMatcher(Card.getCard(Card.ACE, race))
				};

				PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
				if (state.getComboCardsUsed()==cardsUsed) {
					return TexasPokerSideGame.AQ_AJ_SUITED;
				}
			}
		}


		/* A K unsuited */
		cardsToFind = new CardMatcher[] {
				new CardNumberMatcher(Card.KING),
				new CardNumberMatcher(Card.ACE)
		};
		PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
		if (state.getComboCardsUsed()==cardsUsed) {
			return TexasPokerSideGame.AK_UNSUITED;
		}

		/* K K or Q Q or J J*/
		for(int c =Card.JACK; c<=Card.KING; c++){
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(c),
					new CardNumberMatcher(c)
			};
			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.KK_QQ_JJ;
			}
		}

		/* A Q or A J suited */
		for(int c =Card.JACK; c<Card.KING; c++){
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(c),
					new CardNumberMatcher(Card.ACE)
			};

			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.AQ_AJ_UNSUITED;
			}
		}

		/* 10 10 to 2 2*/
		for(int c =2; c<11; c++){
			cardsToFind = new CardMatcher[] {
					new CardNumberMatcher(c),
					new CardNumberMatcher(c)
			};

			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.TEN_TO_TWO_PAIR;
			}
		}

		return null;

	}
	
}
