package com.magneta.games;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.random.SeedGeneratorService;
import com.magneta.casino.games.server.beans.GameRoundBean;
import com.magneta.casino.games.server.services.CurrentRoundService;
import com.magneta.casino.games.server.services.GameActionPersistanceService;
import com.magneta.casino.games.server.services.GameConfigService;
import com.magneta.casino.games.server.services.GameCurrentConfigurationService;
import com.magneta.casino.games.server.services.GameRoundPersistanceService;
import com.magneta.casino.games.server.services.GameRoundService;
import com.magneta.casino.games.server.services.GameSettingsService;
import com.magneta.casino.games.server.session.GamePlayerLogoutListener;
import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.configuration.GameConfigLoadingException;
import com.magneta.games.configuration.GameConfiguration;

/**
 *
 */
public class Table {

	private static final Logger log = LoggerFactory.getLogger(Table.class);

    private final long tableId;
    private final GameBean game;
    private final GamePlayer player;
    
    private final double minBet;
    private final double maxBet;

    private boolean closed;
    private boolean tableStarted;

    private GameRound currGameRound;
    
    private GamePlayerLogoutListener logoutListener;

    /**
     * Constructs a new game table.
     * @param tableID The ID of the table to construct.
     * @param gameType The game type ID of the table game.
     * @param minBet The minimum bet for the new table.
     * @param maxBet The maximum bet for the new table.
     * @param privateTable A flag indicating whether the table is private.
     */
    public Table(GameBean game, GamePlayer player, long tableId, double minBet, double maxBet) {	
    	this.game = game;
    	this.player = player;
        this.tableId = tableId;
        this.minBet = minBet;
        this.maxBet = maxBet;
        
        this.closed = false;
        this.tableStarted = false;
    }
    
    /**
     * Sets a listener for when the player logs out. This is not used by the table
     * itself but the server code.
     * 
     * @param logoutListener
     */
    public void setGamePlayerLogoutListener(GamePlayerLogoutListener logoutListener) {
    	this.logoutListener = logoutListener;
    }
    
    public GamePlayerLogoutListener getGamePlayerLogoutListener() {
    	return this.logoutListener;
    }

    /**
     * Returns the ID of the table.
     * @return The ID of the table.
     */
    public long getTableId() {
        return tableId;
    }

    /**
     * Returns the minimum bet amount for this table.
     * @return The minimum bet amount for this table.
     */
    public double getMinBet() {
        return minBet;
    }

    /**
     * Returns the maximum bet amount for this table.
     * @return The maximum bet amount for this table.
     */
    public double getMaxBet() {
        return maxBet;
    }

    /**
     * Sends an action request to the current round.
     * @param request The ActionRequest of the action.
     */
    public void doAction(GameActionRequest request, GameRoundListener listener, 
    		GameRoundPersistanceService gameRoundPersistanceService,
    		CurrentRoundService currentRoundService) throws ActionFailedException {
        GameRound round = currGameRound;

        if (round == null) {
            throw new ActionFailedException("No round in progress");
        }

        if (round.getId() != request.getRoundId()) {
            throw new ActionFailedException("Invalid roundId.");
        }
        
        if (round.isFinished()) {
        	throw new ActionFailedException("Round finished!");
        }

        round.setRoundListener(listener);
        currentRoundService.startRequest(round);
        try {
        	round.doRequest(getPlayer(), request);        	
        } finally {
        	currentRoundService.endRequest();
        	round.setRoundListener(null);
        }
    }
    
    public GameRound nextRound(GameRoundListener listener, 
    		SeedGeneratorService seedGeneratorService,
    		GameRoundService gameRoundService,
    		GameRoundPersistanceService gameRoundPersistanceService,
    		CurrentRoundService currentRoundService) throws ServiceException, ActionFailedException {
    	if (currGameRound == null || !currGameRound.isFinished()) {
    		throw new IllegalStateException("");
    	}

    	GameConfiguration config = currGameRound.getConfig();

    	int roundId = currGameRound.getId() + 1;
    	byte[] seed = seedGeneratorService.getSeed(64);
    	gameRoundPersistanceService.createRound(getTableId(), roundId, seed, config);

    	GameRound prevRound = currGameRound;

    	currGameRound = gameRoundService.createRound(game.getGameTemplate());
    	currGameRound.setRoundListener(listener);
    	currentRoundService.startRequest(currGameRound);
    	try {
    		currGameRound.init(prevRound, seed);
    	} finally {
    		currentRoundService.endRequest();
    		currGameRound.setRoundListener(null);
    	}

    	return currGameRound;
    }
    
    public GameRound getCurrentRound() {
    	return currGameRound;
    }

    /**
     * Starts the current table and creates a new round.
     * 
     * @throws GameConfigLoadingException 
     * @throws ServiceException 
     * @throws ActionFailedException 
     */
    public void startTable(GameRoundListener roundListener, GameConfigService configService,
    		GameSettingsService gameSettingsService,
    		GameRoundService gameRoundService,
    		GameRoundPersistanceService gameRoundPersistanceService,
    		SeedGeneratorService seedGeneratorService,
    		GameCurrentConfigurationService currentConfigService,
    		CurrentRoundService currentRoundService) throws GameConfigLoadingException, ServiceException, ActionFailedException {
        if (this.tableStarted) {
            throw new IllegalStateException("Table " + tableId + " has already started");
        }
        tableStarted = true;
    	
        Long configId = currentConfigService.getLatestConfigId(game.getGameId());

        GameConfiguration config = configService.getConfiguration(game.getGameTemplate(), configId);
        
        RoundOptions options = new RoundOptions(player.getUserId(), this.minBet, this.maxBet, gameSettingsService.getGameSettings(game.getGameId()));

        int roundId = 0;
        byte[] seed = seedGeneratorService.getSeed(64);

        gameRoundPersistanceService.createRound(getTableId(), roundId, seed, config);        

        GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
        
        currGameRound = gameRoundService.createRound(gameTemplate.getTemplate());
        
        try {
        	currGameRound.setRoundListener(roundListener);
        	currentRoundService.startRequest(currGameRound);
        	currGameRound.init(game, gameTemplate, config, options, getTableId(), roundId, seed);
        } finally {
        	currentRoundService.endRequest();
        	currGameRound.setRoundListener(null);
        }
    }
    
    /**
     * Starts the table by loading the latest unfinished round.
     * @param roundId The ID of the latest unfinished round of this table.
     * @throws ServiceException 
     * @throws ActionFailedException 
     */
    public void resumeTable(int roundId, GameRoundListener roundListener,
    		GameSettingsService gameSettingsService,
    		GameConfigService gameConfigService,
    		GameCurrentConfigurationService gameCurrentConfigService,
    		GameRoundService gameRoundService,
    		GameRoundPersistanceService gameRoundPersistanceService,
    		GameActionPersistanceService gameActionPersistance,
    		CurrentRoundService currentRoundService) throws ServiceException, ActionFailedException {
    	
    	if (this.tableStarted) {
    		throw new IllegalStateException("Table " + tableId + " has already started");
    	}
    	
    	GameRoundBean roundBean = gameRoundPersistanceService.loadRound(getTableId(), roundId);
    	
    	/* If the round has not stared switch to the latest config */
    	if (!roundBean.isRoundStarted()) {
    		Long currentConfigId = gameCurrentConfigService.getLatestConfigId(game.getGameId());
    		
    		/* Configuration need to be updated */
    		if (currentConfigId != null && !currentConfigId.equals(roundBean.getConfigId())) {
    			gameRoundPersistanceService.roundUpdateConfig(getTableId(), roundId, currentConfigId);
    			roundBean.setConfigId(currentConfigId);
    		}
    	}

    	RoundOptions opt = new RoundOptions(player.getUserId(), this.minBet, this.maxBet, gameSettingsService.getGameSettings(this.game.getGameId()));

    	GameConfiguration config;
        try {
        	config = gameConfigService.getConfiguration(game.getGameTemplate(), roundBean.getConfigId());
        } catch (GameConfigLoadingException e) {
        	throw new ServiceException("Unable to load configuration for round " + this.tableId + "-" + roundId, e);
        }
        
        GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());    	
        this.tableStarted = true;
        
    	currGameRound = gameRoundService.createRound(game.getGameTemplate());

    	try {
        	currGameRound.setRoundListener(roundListener);
        	currentRoundService.startRequest(currGameRound);
        	currGameRound.init(game, gameTemplate, config, opt, getTableId(), roundId, roundBean.getSeed());
        	
        	Iterable<GameAction> loadedActions = gameActionPersistance.loadParentActions(player, currGameRound);
        	
        	try {
        		currGameRound.loadActions(player, loadedActions);
            } catch (ActionFailedException e) {
            	throw new ServiceException("Unable to finish loading round", e);
            }
        	
        } finally {
        	currentRoundService.endRequest();
        	currGameRound.setRoundListener(null);
        }
    }

    /**
     * Marks the table as closed.
     */
    public void setClosed() {
        log.info("Table {}: marked closed", this.tableId);
        this.closed = true;
    }
    
    /**
     * Whether the table is marked closed.
     */
    public boolean isClosed() {
    	return this.closed;
    }
    
    public boolean isStarted() {
    	return this.tableStarted;
    }
    
    public GamePlayer getPlayer() {
    	return this.player;
    }
}
