/**
 * 
 */
package com.magneta.games;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.DbUtil;

/**
 * @author User
 *
 */
public class FilterBetFromBonus {

	private static final Logger log = LoggerFactory.getLogger(FilterBetFromBonus.class);
	
    public static double getCleanMoney(Connection conn, final GameAction action) {        
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            String sql=
                "SELECT amount FROM user_transactions" +
                " WHERE user_id = ?" +
                " AND action_table_id = ?" +
                " AND action_round_id = ?" +
                " AND action_action_id = ?" +
                " AND action_sub_action_id = ?";

            stmt = conn.prepareStatement(sql);

            int i = 1;
            
            stmt.setLong(i++, action.getPlayer().getUserId());
            stmt.setLong(i++, action.getTableId());
            stmt.setInt(i++, action.getRoundId());
            stmt.setInt(i++, action.getActionId());
            stmt.setInt(i, action.getSubActionId());
            
            rs = stmt.executeQuery();

            if(rs.next()) {
                return rs.getDouble("amount") * -1.0;
            }

        } catch (SQLException e) {
            log.error("Error getting amount.",e);
        } finally {       
            DbUtil.close(rs);
            DbUtil.close(stmt);
        }
        return 0.0;
        
    }
}
