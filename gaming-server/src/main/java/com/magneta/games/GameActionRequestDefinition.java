/**
 * 
 */
package com.magneta.games;

import java.io.Serializable;


/**
 * Game Action definitions.
 * Used to validate requests against expected parameters.
 * @author Nicos
 *
 */
public final class GameActionRequestDefinition implements Serializable {

    /**
     * Type for each request parameter
     * @author Nicos
     *
     */
    public enum ParameterType {
        /** The parameter is optional (ie it may be specified or not) */
        OPTIONAL,
        /** The parameter is required (it MUST be specified) */
        REQUIRED,
        /** The parameter is forbidden (it MUST NOT be specified) */
        FORBIDDEN
    }
    
    private static final long serialVersionUID = 8755628946560935337L;

    public final String description;
    public final ParameterType area;
    public final ParameterType amount;  
    
    public GameActionRequestDefinition(String description, ParameterType area, ParameterType amount) {
        this.description = description;
        this.area = area;
        this.amount = amount;
    }
    
}
