package com.magneta.games;

public class CachedConfiguration {
    private final Long configId;
    private final long timeUpdated;

    public CachedConfiguration(Long configId, long timeUpdated) {
        super();
        this.configId = configId;
        this.timeUpdated = timeUpdated;
    }

    public Long getConfigId() {
        return configId;
    }

    public long getTimeUpdated() {
        return timeUpdated;
    }
}