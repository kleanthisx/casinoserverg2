/**
 * 
 */
package com.magneta.games.frenchroulette;

import static com.magneta.games.frenchroulette.FrenchRouletteRound.*;

/**
 * @author User
 *
 */
public class FrenchRouletteBet {

    private static final int [][] OUTSIDE_BETS = {
        //Column bets
        /*0*/{1,4,7,10,13,16,19,22,25,28,31,34},
        /*1*/{2,5,8,11,14,17,20,23,26,29,32,35},
        /*2*/{3,6,9,12,15,18,21,24,27,30,33,36},
        
        //Dozen bets
        /*0*/{1,2,3,4,5,6,7,8,9,10,11,12},
        /*1*/{13,14,15,16,17,18,19,20,21,22,23,24},
        /*2*/{25,26,27,28,29,30,31,32,33,34,35,36},
        
        //Color bets
        /*0 - RED  */{1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36},
        /*1 - BLACK*/{2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35},
        
        //Odd-even bets
        /*0 - odd */{1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35},
        /*1 - even*/{2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36},
        
        //High-low bets
        /*0 - low */{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18},
        /*1 - high*/{19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36},
    };
    
    final int betType;
    final int[] args;
    final double betAmount;
    final int[] numbers;
    
    public FrenchRouletteBet(int betType, int[] args, double betAmount){
        this.betType = betType;
        this.args = args;
        this.betAmount = betAmount;

        switch (betType){
            case NUMBER_BET:
                this.numbers = args;
                break;
            case COLUMN_BET:
                this.numbers = OUTSIDE_BETS[args[0]];
                break;
            case DOZEN_BET:
                this.numbers = OUTSIDE_BETS[3 + args[0]];
                break;
            case COLOR_BET:
                this.numbers = OUTSIDE_BETS[3 + 3 + args[0]];
                break;
            case ODD_EVEN_BET:
                this.numbers = OUTSIDE_BETS[3 + 3 + 2 + args[0]];
                break;
            case HIGH_LOW_BET:
                this.numbers = OUTSIDE_BETS[3 + 3 + 2 + 2 + args[0]];
                break;
            default:
                throw new RuntimeException("Invalid Bet Type");
        }
    }
    
    public double getBetAmount() {
        return betAmount;
    }

    public int getBetType() {
        return betType;
    }
    
    public int[] getNumbers() {
        return numbers;
    }
    
    @Override
    public boolean equals(Object o){
        if (o instanceof FrenchRouletteBet){
            FrenchRouletteBet bet = (FrenchRouletteBet)o;
            if (bet.betType == this.betType && bet.numbers.length == this.numbers.length){
                for (int i=0;i<numbers.length;i++){
                    if (bet.numbers[i] != this.numbers[i]){
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return toString().hashCode();
    }
    
    @Override
    public String toString(){
        String nums = "";
        for (int i=0;i<args.length;i++){
            nums += args[i];
            if (i < (args.length-1)){
                nums += ",";
            }
        }
        return betType+"_"+nums;
    }

    public double getWinAmount(){
        return ((Math.rint(36 / numbers.length) * betAmount));
    }
}
