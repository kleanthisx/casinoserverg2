package com.magneta.games.frenchroulette;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.magneta.casino.services.CurrencyParser;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("FrenchRoulette")
public class FrenchRouletteRound extends AbstractGameRound {
    
    private static final String SPIN_ACTION = "g:SPIN";
    
    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
                                      /*Description  Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,  ParameterType.REQUIRED,  ParameterType.REQUIRED),
    };
    
    protected static final int NUMBER_BET = 0;
    protected static final int COLUMN_BET = 1;
    protected static final int DOZEN_BET = 2;
    protected static final int COLOR_BET = 3;
    protected static final int ODD_EVEN_BET = 4;
    protected static final int HIGH_LOW_BET = 5;
    
    private static final int[][] NUMBERS = {
         {1,2,3},
         {4,5,6},
         {7,8,9},
         {10,11,12},
         {13,14,15},
         {16,17,18},
         {19,20,21},
         {22,23,24},
         {25,26,27},
         {28,29,30},
         {31,32,33},
         {34,35,36}
        };

   
    private boolean finished;
    private final List<FrenchRouletteBet> bets;
    private double totalMaxBet;
    private double totalMinBet;
    
    private final CurrencyParser currencyParser;
    
    public FrenchRouletteRound(CurrencyParser currencyParser) {
    	this.currencyParser = currencyParser;
    	this.bets = new ArrayList<FrenchRouletteBet>();
        this.finished = false;
    }
    
    @Override
	public void roundInit() {
        totalMaxBet = options.getGameSettings().get("total_max_bet", Double.TYPE);
        totalMinBet = options.getGameSettings().get("total_min_bet", Double.TYPE);
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            if (!action.isLoaded()){
                performBet(action);
            } else {
                loadBet(action);
            }
        }
    }
    
    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        String[] betsArea = action.getArea().split(";");
        
        if (betsArea.length == 0) {
            throw new ActionFailedException("Invalid area format");
        }
        
        action.setSaved();
        GameAction lastBet = null;
        
        List<FrenchRouletteBet> tmpBets = new ArrayList<FrenchRouletteBet>();
        for (String betStr: betsArea){

            FrenchRouletteBet bet = parseBet(betStr);
            if (!action.isLoaded()){
                if (tmpBets.contains(bet)){
                    throw new ActionFailedException("Bet already placed!");
                } else if ((bet.betAmount > (this.options.getMaxBet() + 0.001)) || (bet.betAmount < (this.options.getMinBet() - 0.001))){
                    throw new ActionFailedException("Bet not within min/max individual bet.");    
                }
            }
            lastBet = new GameAction(action.getPlayer(), this,1, BET_ACTION, bet.toString(), bet.betAmount);
            action.addAdditionalAction(lastBet);
            tmpBets.add(bet);
        }

        double total = 0.0;
        for (FrenchRouletteBet bet: tmpBets){
            total += bet.betAmount;
        }
        
        if ((!action.isLoaded()) && ((totalMaxBet < 0 || total > (totalMaxBet + 0.001)) || (total < (totalMinBet - 0.001)))){
            throw new ActionFailedException("Bet total not within min/max total bet.");
        }
        
        action.commit();
        bets.addAll(tmpBets);
        performSpin(lastBet);
    }
    
    private void loadBet(GameAction action) throws ActionFailedException {
        FrenchRouletteBet bet = parseBet(action.getArea()+" " + currencyParser.formatDouble(action.getAmount()));
        bets.add(bet);
    }
    
    @Override
    protected void loadFinished(GamePlayer player) throws ActionFailedException {
    	if (bets != null && !bets.isEmpty()) {
    		performSpin(new GameAction(player, this, 1, SPIN_ACTION, null, 0.0));
    	}
    }
    
    private static final int[] WHEEL = {
        0, 32, 15, 19,  4, 21,  2, 25, 17, 34,  6, 27, 13, 36, 11, 30,  8, 23, 10,
        5, 24, 16, 33,  1, 20, 14, 31,  9, 22, 18, 29,  7, 28, 12, 35,  3, 26
    };
    
    /**
     * @param action
     * @throws ActionFailedException
     */
    private void performSpin(GameAction action) throws ActionFailedException {
        GameAction spinAction = action.createGeneratedAction(null, 1, SPIN_ACTION, null, 0.0);
        spinAction.setRoundStart();
        
        int stop = WHEEL[getRandom().nextInt(WHEEL.length * 100) % WHEEL.length];
        
        //int stop = getRandom().nextInt(37);

        stop = com.magneta.games.fixedodd.FrenchRouletteGuard.guardRound(game.getGameId(), this.options.getOwner(), stop, bets, getRandom(), this.options.getGameSettings(), spinAction);
        
        spinAction.setActionValue(String.valueOf(stop));
        
        for (FrenchRouletteBet bet: bets){
            for (int num: bet.numbers){
                if (num == stop){
                    spinAction.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, bet.toString(), bet.getWinAmount());
                    break;
                }
            }
        }
        
        spinAction.createGeneratedAction(null, 0, FINISH_ACTION, null, 0.0);
        spinAction.commit(true);
        Connection dbConn = ConnectionFactory.getConnection();
        if (dbConn != null){
            PreparedStatement statement = null;
            try 
            {
                statement = dbConn.prepareStatement("SELECT update_roulette_result(?,?,?)");
    
                statement.setLong(1, getTableId());
                statement.setInt(2, getId());
                statement.setInt(3, stop);
                statement.executeQuery();
            } catch (SQLException e) {
                logError("Error saving roulette result.",e);
            } finally {
                DbUtil.close(statement);
                DbUtil.close(dbConn);
            }
        }
        this.finished = true;
    }

    @Override
	public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {      
        /* Check if the action is "syntactically" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);
        
        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }
    
    private FrenchRouletteBet parseBet(String bet) throws ActionFailedException{
        if (!bet.matches("\\d+[[_]\\d+[,\\d+]]* \\d+(\\.\\d+)?")){
            throw new ActionFailedException("Invalid area format.");
        }
        
        String area = bet.substring(0,bet.lastIndexOf(" "));
        double amount = Double.parseDouble(bet.substring(bet.lastIndexOf(" ")+1));
        
        String[] split = area.split("_");
        String[] args = split[1].split(",");       
        int betType = Integer.parseInt(split[0]);
        int[] betArgs = new int[args.length];
        for (int i=0;i<args.length;i++){
            betArgs[i] = Integer.parseInt(args[i]);
        }
        
        switch (betType){
            case NUMBER_BET:
                Arrays.sort(betArgs);
                for (int arg: betArgs){
                    if (arg < 0 || arg > 36){
                        throw new ActionFailedException("Invalid bet arguments.");
                    }
                }
                if (betArgs.length > 0 && betArgs.length <= 6){
                    switch (betArgs.length){
                        case 1:
                            return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                        case 2:
                            if ((betArgs[0] == 0) && (betArgs[1] >= 1) && (betArgs[1] <= 3)){                                   
                                return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                            }

                            for (int i=0;i < NUMBERS.length;i++) {
                            	for (int j=0;j < 3;j++){

                            		if ((NUMBERS[i][j] == betArgs[0])) {
                            			if (  (j < 2 && NUMBERS[i][j+1] == betArgs[1])
                            					|| (i < NUMBERS.length -1 && NUMBERS[i+1][j] == betArgs[1])){
                            				return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                            			}
                            			break;
                            		}
                            	}
                            }
                            throw new ActionFailedException("Invalid bet arguments.");

                        case 3:
                            if ((betArgs[0] == 0) && (((betArgs[1] == 1) && (betArgs[2] == 2)) || ((betArgs[1] == 2) && (betArgs[2] == 3)))){
                                return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                            }
                            for (int i=1;i<betArgs.length;i++){
                            	if (betArgs[i] != (betArgs[i-1]+1)){
                            		throw new ActionFailedException("Invalid bet arguments.");
                            	}
                            }
                            return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                        case 4:
                            if (betArgs[0] == 0){
                                for (int i=1;i<betArgs.length;i++){
                                    if (betArgs[i] != (betArgs[i-1]+1)){
                                        throw new ActionFailedException("Invalid bet arguments.");
                                    }
                                }
                                return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                            }

                            for (int i=0;i < NUMBERS.length-1;i++) {
                            	for (int j=0;j<2;j++){
                            		if ((NUMBERS[i][j] == betArgs[0]) && (NUMBERS[i][j+1] == betArgs[1])
                            				&& (NUMBERS[i+1][j] == betArgs[2]) && (NUMBERS[i+1][j+1] == betArgs[3])){
                            			return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                            		}
                            	}
                            }
                            throw new ActionFailedException("Invalid bet arguments.");

                        case 6:
                            for (int i=0;i<NUMBERS.length-1;i++) {
                                if (NUMBERS[i][0] == betArgs[0]){
                                    int last = betArgs[0];
                                    for (int j=1;j<3;j++){
                                        if (NUMBERS[i][j] == (last+1)){
                                            last++;
                                        } else {
                                            throw new ActionFailedException("Invalid bet arguments.");
                                        }
                                    }
                                    for (int j=0;j<3;j++){
                                        if (NUMBERS[i+1][j] == (last+1)){
                                            last++;
                                        } else {
                                            throw new ActionFailedException("Invalid bet arguments.");
                                        }
                                    }
                                    return new FrenchRouletteBet(NUMBER_BET, betArgs, amount);
                                }
                            }
                            break;
                        default:
                            throw new ActionFailedException("Invalid bet arguments.");
                    }
                } else {
                    throw new ActionFailedException("Invalid bet arguments.");
                }
                break;
            case COLUMN_BET:
            case DOZEN_BET:
                if (betArgs[0] < 0 || betArgs[0] > 2){ 
                    throw new ActionFailedException("Invalid bet arguments.");
                }
                
                return new FrenchRouletteBet(betType, betArgs, amount);
            case COLOR_BET:
            case ODD_EVEN_BET:
            case HIGH_LOW_BET:
                if (betArgs[0] < 0 || betArgs[0] > 1){ 
                    throw new ActionFailedException("Invalid bet arguments.");
                }
                return new FrenchRouletteBet(betType, betArgs, amount);
            default:
                throw new ActionFailedException("Invalid bet type.");
        }
        
        throw new ActionFailedException("Invalid area.");
    }
    
    @Override
    protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
        super.getExtraInfo(player, roundInfo, firstTime);
        if (firstTime){
            Connection dbConn = ConnectionFactory.getConnection();
            PreparedStatement stmt = null;
            ResultSet res = null;
            
            List<Integer> lastRes = new ArrayList<Integer>();
            
            try {
                stmt = dbConn.prepareStatement(
                        "SELECT result" +
                        " FROM roulette_results" +
                        " WHERE table_id = ?" +
                        " ORDER BY round_id DESC");
                stmt.setLong(1, getTableId());
                res = stmt.executeQuery();

                while (res.next()){
                    lastRes.add(res.getInt(1));
                }
            } catch (SQLException e){
                logError("Error getting the 12 last results.");
            } finally {
                DbUtil.close(res);
                DbUtil.close(stmt);
                DbUtil.close(dbConn);
            }
            
            if (lastRes.size() > 0){
                roundInfo.put("last_results", lastRes);
            }
            
            roundInfo.put("total_min_bet", totalMinBet);
            roundInfo.put("total_max_bet", totalMaxBet);
        }
    }
}
