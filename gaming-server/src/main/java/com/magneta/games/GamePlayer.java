package com.magneta.games;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerLoginService;
import com.magneta.casino.games.server.services.ServiceLocator;
import com.magneta.casino.games.server.session.GamePlayerLogoutListener;
import com.magneta.casino.internal.beans.ExtendedPrincipalBeanImpl;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.beans.UserDetailsBean;

/**
 * GamePlayer class holds information for a logged-in player. 
 */
public final class GamePlayer extends ExtendedPrincipalBeanImpl implements HttpSessionBindingListener {

	private static final Logger log = LoggerFactory.getLogger(GamePlayer.class);
    /**
     * 
     */
    private static final long serialVersionUID = 5735363554275580344L;

    private UserDetailsBean userDetails;    
    private List<Long> corporateHierarchy;

    private GamePlayerStats sessionStats; 
    private long sessionStart;
    private long lastActionTime;

    private List<GamePlayerLogoutListener> logoutListeners;
    private StringBuilder asyncResults;

    public GamePlayer(UserAuthenticationBean authBean, UserDetailsBean userDetails, List<Long> corporateHierarchy) {
    	super(authBean);
    	
    	this.userDetails = userDetails;
    	this.corporateHierarchy = corporateHierarchy;
        this.sessionStats = new GamePlayerStats();
        this.sessionStart = System.currentTimeMillis();
        this.lastActionTime = this.sessionStart;
    }

    public synchronized void commitStats(GamePlayerStats stats) {  
    	sessionStats.add(stats);
        log.debug("ADDED BETS: {}, WINS: {}", stats.getBets(), stats.getWins());
    }

    /**
     * Returns the player's nickname.
     * @return The player's nickname.
     */
    public String getNickname() {
    	String nickname = null;
    	
    	if (this.userDetails != null) {
    		nickname = userDetails.getNickName();
    	}
    	
        if (nickname == null) {
            return "P" + this.getUserId();
        }
        return nickname;
    }

    @Override
    public boolean equals(Object o) {

        if (o == null) {
            return false;
        }

        if (o instanceof GamePlayer) {
            return (((GamePlayer)o).getUserId().equals(this.getUserId()));
        }

        return false;
    }

    /**
     * @return The amount of bets since the user logged in
     */
    public double getCurrentBets() {
        return sessionStats.getBets();
    }

    /**
     * @return The amount of wins since the user logged in
     */
    public double getCurrentWins() {
        return sessionStats.getWins();
    }

    public String getUsername() {
    	if (userDetails == null) {
    		return null;
    	}
    	return userDetails.getUserName();
    }

    public UserDetailsBean getDetails() {
    	return userDetails;
    }

    public List<Long> getCorporateHierarchy() {
        return this.corporateHierarchy;
    }

    public Long getCorpId() {
    	if (this.corporateHierarchy.isEmpty()) {
    		return null;
    	}
    	
    	return this.corporateHierarchy.get(0);
    }

    public synchronized void addLogoutListener(GamePlayerLogoutListener listener) {
    	if (logoutListeners == null) {
    		logoutListeners = new ArrayList<GamePlayerLogoutListener>();
    	}
    	
    	logoutListeners.add(listener);
    }
    
    public synchronized void removeLogoutListener(GamePlayerLogoutListener listener) {
    	if (logoutListeners == null) {
    		return;
    	}
    	
    	logoutListeners.remove(listener);
    }
    
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
	}

	/**
	 * User unbound from session. Notify all listeners.
	 */
	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		if (logoutListeners != null) {
			int listenersSize = logoutListeners.size();
			
			for (int i=listenersSize-1; i >=0 ; i--) {
				logoutListeners.get(i).onLogout(this);
			}
		}
		this.logoutListeners = null;
		
		ServiceLocator.getService(GameServerLoginService.class).recordLogout(getDetails(), event.getSession().getId());
	}
	
	public long getLastActionTime() {
		return this.lastActionTime;
	}
	
	public void updateActionTime() {
		this.lastActionTime = System.currentTimeMillis();
	}
	
	public synchronized void addAsyncResult(String result) {
		if (this.asyncResults == null) {
			this.asyncResults = new StringBuilder();
		} else {
			this.asyncResults.append("\n");
		}
		this.asyncResults.append(result);
	}
	
	public synchronized String getAsyncResults() {
		if (this.asyncResults == null) {
			return null;
		}
		
		String res = this.asyncResults.toString();
		this.asyncResults = null;
		return "<asyncResults>\n" + res + "\n</asyncResults>";
	}
}
