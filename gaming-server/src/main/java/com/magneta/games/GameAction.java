package com.magneta.games;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameActionPersistanceService;
import com.magneta.casino.games.server.services.ServiceLocator;

/**
 * GameAction class holds a GameAction that is to be performed.
 * 
 * Actions can be nested as generated actions when the parent action
 * triggers the generated actions. Actions can be nested upto 4 levels
 * with each level having 100 actions.
 */
public final class GameAction implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(GameAction.class);
	
    private static final long serialVersionUID = 1168428403204487586L;
    
    private static final int MAX_SUB_ACTIONS = 100;
    private static final int MAX_SUB_LEVELS = 4;
    
    private final long tableId;
    private final int roundId;
    
    private final int seatId;
    private final GamePlayer player;

    private final String actionDesc;
    private final String actionArea;
    
    private String actionValue;
    private String amountFunction;
    private double amount;
    
    private final int actionId;
    private final int subActionId;

    private int level;

    private final List<GameAction>generatedActions;
    private final List<GameAction>additionalActions;

    private boolean savedAction;
    private boolean loadedAction;
    private boolean additionalAction;
    private boolean isRoundStartAction;
    
    /*
     * Multiplier per level
     */
    private static final int[] level_multiplier = { 0, 1000000, 10000, 100, 1};
    
    private static int calculateSubActionId(int parentSubActionId, int level, int counter) {
        int prefix;
        
        if (level > MAX_SUB_LEVELS) {
            throw new RuntimeException("Maximum number of levels reached");
        }
        
        if (counter >= MAX_SUB_ACTIONS) {
            throw new RuntimeException("Maximum number of sub-actions reached");
        }
        
        prefix = parentSubActionId;
        
        if (level == 1) {
            prefix = 1000000000;
        }
        
        return prefix + (counter * level_multiplier[level]);
    }
    
    /**
     * Constructs a new GameAction which depends on another action.
     * @param parent The GameAction on which the new action will depend upon.
     * @param seatID The ID of the seat performing the action.
     * @param userId The ID of the user performing the action.
     * @param desc The description of the action.
     * @param area The area of the action.
     * @param actionAmount The amount the action invests or wins.
     */
    private GameAction(GameAction parent, GamePlayer player, int seatId, String desc, String area, double actionAmount) {
        this.tableId = parent.tableId;
        this.roundId = parent.roundId;
        this.seatId = seatId;
        this.player = player;
        this.actionId = parent.actionId;
        this.actionDesc = desc;
        this.actionArea = area;
        this.amount = actionAmount;
        this.generatedActions = new ArrayList<GameAction>();
        this.additionalActions = new ArrayList<GameAction>();
        
        this.actionValue = null;
        this.amountFunction = null;
        this.savedAction = false;
        
        this.loadedAction = parent.loadedAction;
        this.level = parent.level + 1;
        this.subActionId = calculateSubActionId(parent.subActionId, level, parent.generatedActions.size() + 1);
        parent.generatedActions.add(this);
        
        log.debug("Round {}-{}: Created action: {}", new Object[] {tableId, roundId, this});
    }

    /**
     * Constructs a new independent GameAction.
     * @param round The round where the action takes place.
     * @param seatId The ID of the seat performing the action.
     * @param userId The ID of the user performing the action.
     * @param actionDesc The description of the action
     * @param actionArea The area of the action.
     * @param amount The amount the action invests or wins.
     */
    public GameAction(GamePlayer player, AbstractGameRound round, int seatId, String actionDesc, String actionArea, double amount) {
    	this(false, round.getTableId(), round.getId(), player, round.nextActionId(), seatId, actionDesc, actionArea);
    	
    	this.setAmount(amount);
    	log.debug("Round {}-{}: Created action: {}", new Object[] {tableId, roundId, this});
    }

    public GameAction(boolean loaded, long tableId, int roundId, GamePlayer player, int actionId, int seatId, String actionDesc, String actionArea) {
        this.tableId = tableId;
        this.roundId = roundId;
        this.seatId = seatId;
        this.player = player;
        
        this.actionDesc = actionDesc;
        this.actionArea = actionArea;
        this.generatedActions = new ArrayList<GameAction>();
        this.additionalActions = new ArrayList<GameAction>();
        
        this.actionValue = null;
        this.amountFunction = null;
        this.savedAction = false;
        this.loadedAction = loaded;
        this.isRoundStartAction = false;
        
        this.actionId = actionId;
        this.subActionId = 0;
    }

    /**
     * Adds another action to this action.
     * @param action The GameAction to add to this action.
     */
    public void addAdditionalAction(GameAction action) {
    	action.additionalAction = true;
        this.additionalActions.add(action);
    }
    
    public void setRoundStart() {
        this.isRoundStartAction = true;
    }
    
    public void commit() throws ActionFailedException {
        commit(false);
    }
    
    /**
     * Commits the current action. If the action has generated and/or additional
     * actions they are committed as well.
     * If the action is already saved only the generated actions will be saved.
     * 
     * @param lastAction This flag indicates to the action that it is the last parent
     * action to be performed before the round finish, which is the only case this flag
     * should ever have the value of true. The correct use of this flag is vital because
     * it affects the tracking of statistical data kept by the system.
     * 
     * @throws ActionFailedException
     */
    public void commit(boolean lastAction) throws ActionFailedException {	
        if (this.loadedAction) {
            return;
        }
        
        ServiceLocator.getService(GameActionPersistanceService.class).commit(this, lastAction);
    }

    /**
     * Creates a new GameAction under the current action as a resulting action of the current. 
     * <br>NOTE: Use this overload of createGeneratedAction when the generator of the action is NOT the same seat as the parent action  and the affected user is NOT the same. 
     * @param seatID The ID of the seat generating the action.
     * @param desc The action description.
     * @param area The action area.
     * @param actionAmount The amount invested/won by this action.
     * @return The new GameAction that was created.
     */
    public GameAction createGeneratedAction(GamePlayer newPlayer, int seatID, String desc, String area, double actionAmount) {
        return new GameAction(this, newPlayer, seatID, desc, area, actionAmount);
    }

    /**
     * Creates a new GameAction under the current action as a resulting action of the current.      * @param seatID The ID of the seat generating the action.
     * @param desc The action description.
     * @param area The action area.
     * @param actionAmount The amount invested/won by this action.
     * @return The new GameAction that was created.
     */
    public GameAction createGeneratedAction(int seatId, String desc, String area, double actionAmount) {
        return createGeneratedAction(this.player, seatId, desc, area, actionAmount);
    }

    /**
     * Creates a new GameAction under the current action as a resulting action of the current. 
     * @param desc The action description.
     * @param area The action area.
     * @param actionAmount The amount invested/won by this action.
     * @return The new GameAction that was created.
     */
    public GameAction createGeneratedAction(String desc, String area, double actionAmount) {
        return createGeneratedAction(this.player, this.seatId, desc, area, actionAmount);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof GameAction) {
            GameAction other = (GameAction)o;
            return (this.tableId == other.tableId) &&
                   (this.roundId == other.roundId) &&
                   (this.actionId == other.actionId) &&
                   (this.subActionId == other.subActionId);
        }
        return false;       
    }

    /**
     * Returns the ID of the action.
     * @return The ID of the action.
     */
    public int getActionId() {
        return actionId;
    }

    /**
     * Returns the outcome value of the action.
     * @return the outcome value of the action.
     */
    public String getActionValue() {
        return actionValue;
    }

    /**
     * The List containing all the additional actions of the current action.
     * @return List containing all the additional actions of the current action.
     */
    public List<GameAction> getAdditionalActions() {
        return additionalActions;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    /**
     * Returns the amount invested or won by the action.
     * @return The amount invested or won by the action.
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Returns the action area.
     * @return The action area.
     */
    public String getArea() {
        return actionArea;
    }

    /**
     * Returns the action description.
     * @return The action description.
     */
    public String getDescription() {
        return actionDesc;
    }
    
    /**
     * Returns the List containing all the generated actions of the current action.
     * @return List containing all the generated actions of the current action.
     */
    public List<GameAction> getGeneratedActions() {
        return generatedActions;
    }

    /**
     * Returns the ID of the round to which this action belongs.
     * @return The ID of the round to which this action belongs.
     */
    public int getRoundId() {
        return roundId;
    }

    /**
     * Returns the ID of the seat where the action took place.
     * @return The ID of the seat where the action took place.
     */
    public int getSeatId() {
        return seatId;
    }
    
    /**
     * Returns the sub-ID of the current action.
     * @return The sub-ID of the action. For parent actions this is always 0.
     */
    public int getSubActionId() {
        return this.subActionId;
    }

    /**
     * Returns the ID of the table where the action took place.
     * @return The ID of the table where the action took place.
     */
    public long getTableId() {
        return tableId;
    }

    @Override
    public int hashCode() {
        return (((int)this.tableId) * 10000000) + (this.roundId * 100000) +  (this.actionId * 10000) + this.subActionId;
    }

    /**
     * Returns whether this action is an additional action (i.e. It goes together with another action).
     * @return Whether this action is an additional action.
     */
    public boolean isAdditionalAction() {
        return additionalAction;
    }

    /**
     * Returns whether the action is a generated one.
     * @return Whether the action is a generated one.
     */
    public boolean isGeneratedAction() {
        return this.subActionId > 0;
    }
    
    /**
     * Returns whether the action has been saved into the system database.
     * @return Whether the action has been saved into the system database.
     */
    public boolean isSaved(){
        return savedAction;
    }

    public boolean isLoaded() {
        return loadedAction;
    }
    
    public boolean isRoundStart() {
    	return this.isRoundStartAction;
    }
 
    /**
     * Marks the action as saved.
     *
     */
    public void setSaved() {
        this.savedAction = true;
    }
    
    /**
     * Sets the outcome value of the action.
     * @param value The value to assign.
     */
    public void setActionValue(String value) {
        this.actionValue = value;
    }
    
    
    public String getAmountFunction() {
    	return this.amountFunction;
    }
    
    /**
     * Sets a DB stored function call to be used for the action amount instead of a numeric amount.
     * @param amountFunction The function call string to use (which should include invokation values, if applicable).
     */
    public void setAmountFunction(String amountFunction) {
        this.amountFunction = amountFunction;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	
        sb 	.append("{ Id: ")
        	.append(this.actionId)
        	.append(", SubId: ")
        	.append(this.subActionId)
        	.append(", SeatId: ")
        	.append(this.seatId);
        
        
        if (this.player != null) {
        	
            sb 	.append(", Player: ")
            	.append(this.player.getUsername())
            	.append(" (Id: ")
            	.append(this.player.getUserId())
            	.append(")");
        }

        sb .append(", Desc: ")
           .append(this.actionDesc);

        if (this.actionArea != null) {
            sb  .append(", Area: ")
            	.append(this.actionArea);
        }

        if (this.actionValue != null) {
            sb.append(", Value: ")
              .append(this.actionValue);
        }

        if (this.amount > 0.0) {
            sb.append(", Amount: ")
              .append(this.amount);
        }

        sb.append(" }");

        return sb.toString();
    }
    
    public boolean removeGeneratedAction(GameAction action) {
        return generatedActions.remove(action);
    }

    public GamePlayer getPlayer() {
        return player;
    }
}
