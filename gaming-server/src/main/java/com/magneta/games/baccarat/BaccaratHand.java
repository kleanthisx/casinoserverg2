package com.magneta.games.baccarat;

import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.baccarat.BaccaratBet.BetType;

public class BaccaratHand {

	private final List<Integer> cards;

	public BaccaratHand() {
		this.cards = new ArrayList<Integer>(3);
	}
	
	public void addCard(int card) {
		this.cards.add(card);
	}
	
	public int getValue() { 
		int val = 0;
		
		for (int card: cards) {
			val += getCardPoints(card);
		}
		
		return val % 10;
	}
    
    private int getCardPoints(int card) {
    	int no = Card.getCardNumber(card);

    	switch(no) {
    	case Card.ACE:
    		return 1;
    	case 10:
    	case Card.JACK:
    	case Card.QUEEN:
    	case Card.KING:
    		return 0;
    	default:
    		return no;
    	}
    }
    
    public int getLastCardValue() {
    	return getCardPoints(this.cards.get(this.cards.size() - 1));
    }
    
    public boolean handSatisfiesBet(BetType betType) {
    	switch (betType) {
    	case ACE:
    		for (int card: cards) {
    			if (Card.getCardNumber(card) == Card.ACE) {
    				return true;
    			}
    		}
    		break;
    	case FACE:
    		for (int card: cards) {
    			if (Card.getCardNumber(card) >= Card.JACK
    				&& Card.getCardNumber(card) <= Card.KING) {
    				return true;
    			}
    		}
    		break;
    	case ACE_FACE:
    		boolean foundAce = false;
    		boolean foundFace = false;
    		
    		for (int card: cards) {
    			if (Card.getCardNumber(card) >= Card.JACK
    				&& Card.getCardNumber(card) <= Card.KING) {
    				foundFace = true;
    			} else if (Card.getCardNumber(card) == Card.ACE) {
    				foundAce = true;
    			}
    		}
    		
    		return foundAce && foundFace;
    	default:
    		return false;
    	}
    	
    	return false;
    }
    
    @Override
	public String toString() {
    	StringBuilder sb = new StringBuilder();
    	
    	boolean first = true;
    	
    	for (int card: cards) {
    		if (first) {
    			first = false;
    		} else {
    			sb.append(" ");
    		}
    		sb.append(card);
    	}
    	
    	return sb.toString();
    }
}
