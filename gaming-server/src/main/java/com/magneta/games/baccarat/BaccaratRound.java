package com.magneta.games.baccarat;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.magneta.casino.services.CurrencyParser;
import com.magneta.games.ActionFailedException;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.baccarat.BaccaratBet.Actor;
import com.magneta.games.baccarat.BaccaratBet.BetType;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.fixedodd.BaccaratGuard;

@GameTemplate("Baccarat")
public class BaccaratRound extends AbstractGameRound {

    private final CurrencyParser currencyParser;
    
    private static final String DEAL_ACTION = "g:DEAL";
    private static final String HAND_ACTION = "g:HAND";
    protected static final String PUSH = "PUSH";
    
    private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
                                      /*Description  Area                     Amount                */
        new GameActionRequestDefinition(BET_ACTION,  ParameterType.REQUIRED,  ParameterType.REQUIRED),
    };
    
    private Deck deck;
    private double totalMinBet;
    private double totalMaxBet;
    private final List<BaccaratBet> bets;
    private boolean finished;
    
    public BaccaratRound(CurrencyParser currencyParser) {
    	this.currencyParser = currencyParser;
    	this.bets = new ArrayList<BaccaratBet>();
    }
    
    @Override
	public void roundInit() {        
        this.deck = new Deck(options.getGameSettings().getInt("decks"), 0);
        this.totalMinBet = options.getGameSettings().get("total_min_bet", Double.TYPE);
        this.totalMaxBet = options.getGameSettings().get("total_max_bet", Double.TYPE);
    }

    @Override
    protected void performAction(GameAction action) throws ActionFailedException {
        if (BET_ACTION.equals(action.getDescription())){
            if (!action.isLoaded()){
                performBet(action);
            } else {
                loadBet(action);
            }
        }
    }
    
    private boolean checkMaxBet(BaccaratBet bet) {
    	Double betMultiplier = 20.0d;
    	
    	
    	if (bet.getBetType() == BetType.ACE_FACE) {
    		if (bet.getActor() == Actor.BOTH) {
    			betMultiplier = 1.0d;
    		} else {
    			betMultiplier = 10.0d;
    		}
    	} else if (bet.getActor() == Actor.BOTH  && bet.getBetType() == BetType.ACE) {
    		betMultiplier = 10.0d;
    	}
    	
    	return bet.getBetAmount() < ((this.options.getMaxBet() * betMultiplier) + 0.001);
    }
    
    /**
     * Performs the bet action.
     * @param action The action request.
     * @throws ActionFailedException
     */
    private void performBet(GameAction action) throws ActionFailedException {
        String[] betsArea = action.getArea().split(";");
        
        if (betsArea.length == 0) {
            throw new ActionFailedException("Invalid area format");
        }
        
        action.setSaved();
        
        
        List<BaccaratBet> tmpBets = new ArrayList<BaccaratBet>();
        for (String betStr: betsArea){

        	BaccaratBet bet;
        	try {
        		bet = BaccaratBet.parse(currencyParser, betStr);
        	} catch (IllegalArgumentException e) {
        		throw new ActionFailedException("Invalid bet: " + e.getMessage());
        	}
            if (!action.isLoaded()){
                if (tmpBets.contains(bet)){
                    throw new ActionFailedException("Bet already placed!");
                } else if (bet.getBetAmount() < (this.options.getMinBet() - 0.001)) {
                	throw new ActionFailedException("Bet is lower than min bet.");
                } else if (!checkMaxBet(bet)){
                    throw new ActionFailedException("Bet is higher than max bet.");    
                }
            }
            
            tmpBets.add(bet);
        }

        double total = 0.0;
        for (BaccaratBet bet: tmpBets){
            total += bet.getBetAmount();
        }
        
        if ((!action.isLoaded()) && ((totalMaxBet < 0 || total > (totalMaxBet + 0.001)) || (total < (totalMinBet - 0.001)))){
            throw new ActionFailedException("Total bet not within total min/max bet.");
        }
        
        GameAction lastBet = null;
        
        for (BaccaratBet bet: tmpBets) {
        	lastBet = new GameAction(action.getPlayer(), this,1, BET_ACTION, bet.toString(), bet.getBetAmount());
        	action.addAdditionalAction(lastBet);
        }
        
        action.commit();
        bets.addAll(tmpBets);
        performDeal(lastBet);
    }
    
    private void loadBet(GameAction action) throws ActionFailedException {
        DecimalFormat f = new DecimalFormat("0.00");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        f.setDecimalFormatSymbols(symbols);
        
        try {
        	BaccaratBet bet = BaccaratBet.parse(currencyParser, action.getArea()+" "+f.format(action.getAmount()));
        	bets.add(bet);
        } catch (Throwable e) {
        	throw new ActionFailedException("Invalid bet");
        }
    }
    
    @Override
    protected void loadFinished(GamePlayer player) throws ActionFailedException {
    	if (bets != null && !bets.isEmpty()) {
    		performDeal(new GameAction(player, this, 1, DEAL_ACTION, null, 0.0));
    	}
    }

    private static final boolean[][] DRAWING_RULES = {
    	
        /* PLAYER      0     1     2     3     4     5     6     7     8      9 */
        /* BANKER */
    	/* 0 */    { true, true, true, true, true, true, true, true, true , true },
    	/* 1 */    { true, true, true, true, true, true, true, true, true , true },
    	/* 2 */    { true, true, true, true, true, true, true, true, true , true },
    	/* 3 */    { true, true, true, true, true, true, true, true, false, true },
    	/* 4 */    {false,false, true, true, true, true, true, true, false, false},
    	/* 5 */    {false,false,false,false, true, true, true, true, false, false},
    	/* 6 */    {false,false,false,false,false,false, true, true, false, false},
    	/* 7 */    {false,false,false,false,false,false,false,false, false, false},
    };
    
    public static boolean dealerShouldHit(BaccaratHand[] hands) {
    	return DRAWING_RULES[hands[0].getValue()][hands[1].getLastCardValue()];
    }
    
	private void setDeck() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());
		
		if (result == null) {
			return;
		}
		
		List<Integer> topCards = new ArrayList<Integer>();
		String[] sReelStops = result.split(" ");

		for (int i=0; i < sReelStops.length; i++) {
			try {
				topCards.add(Integer.parseInt(sReelStops[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}
		

		
		if (topCards.isEmpty()) {
			return;
		}
		
		deck.setupDeck(topCards);
	}
    
    /**
     * @param action
     * @throws ActionFailedException
     */
    private void performDeal(GameAction action) throws ActionFailedException {
        GameAction dealAction = action.createGeneratedAction(null, 1, DEAL_ACTION, null, 0.0);
        dealAction.setRoundStart();
        
        /* Shuffle */
        deck.shuffleCards(getRandom());
        setDeck();
        
        /* Deal */
        BaccaratHand[] hands = new BaccaratHand[] {
        		new BaccaratHand(), /* Banker */
        		new BaccaratHand()  /* Player */
        };
        
        for (BaccaratHand hand: hands) {
        	hand.addCard(deck.getNextCard());
        	hand.addCard(deck.getNextCard());
        }
        
        boolean shouldStand = false; 
        
        /* Check for 8 or 9 */
        for (BaccaratHand hand: hands) {
        	int val = hand.getValue();
        	
        	if (val == 8 || val == 9) {
        		shouldStand = true;
        		break;
        	}
        }
        
        if (!shouldStand) {
        	if (hands[1].getValue() <= 5) {
        		hands[1].addCard(deck.getNextCard());
        		
        		if (dealerShouldHit(hands)) {
        			hands[0].addCard(deck.getNextCard());
        		}
        		
        	} else {
        		if (hands[0].getValue() <= 5) {
        			hands[0].addCard(deck.getNextCard());
        		}
        	}
        }
        
        BaccaratGuard.guardHand(hands,bets,this.options.getOwner(),game.getGameId(),options.getGameSettings());
        
        /* Send actions for cards drawn */
        GameAction handAction = dealAction.createGeneratedAction(null, 1, HAND_ACTION, "0", 0.0);
        handAction.setActionValue(hands[0].toString());
        
        handAction = dealAction.createGeneratedAction(null, 1, HAND_ACTION, "1", 0.0);
        handAction.setActionValue(hands[1].toString());

        /* Check for wins */
        for (BaccaratBet bet: bets) {
        	if (bet.hasWon(hands)) {
        		dealAction.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, bet.toString(), bet.getWinMultiplier() * bet.getBetAmount());
        	} else {
        		/* If player has bet on somebody winning but we have a tie then push */
        		if(bet.getBetType() == BaccaratBet.BetType.WIN && hands[0].getValue() == hands[1].getValue()) {
        			dealAction.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, bet.toString(), 1 * bet.getBetAmount()).setActionValue(PUSH);
        		}
        	}
        }
        
        dealAction.createGeneratedAction(null, 1, FINISH_ACTION, null, 0.0);
        dealAction.commit(true);
        this.finished = true;
    }

    @Override
    public boolean isFinished() {
        return finished;
    }

    @Override
    protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {      
        /* Check if the action is "syntactically" valid */
        AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);
        
        if (request.getSeatId() != 1) {   
            throw new ActionFailedException("Wrong seat ID (Should be 1).");
        } else if (finished) {
            throw new ActionFailedException("Round has finished.");
        }
    }
    
    @Override
    protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
        super.getExtraInfo(player, roundInfo, firstTime);
        if (firstTime){            
            roundInfo.put("total_min_bet", totalMinBet);
            roundInfo.put("total_max_bet", totalMaxBet);
        }
    }
}
