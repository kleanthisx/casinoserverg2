package com.magneta.games.baccarat;

import com.magneta.casino.services.CurrencyParser;

public class BaccaratBet {

	public enum Actor {
		PLAYER(1),
		BANKER(2),
		BOTH(3);
		
		private final int id;
		
		private Actor(int id) {
			this.id = id;
		}
		
		public int getId() {
			return this.id;
		}
		
		public static Actor valueOf(int val) {
			
			for (Actor a: values()) {
				if (a.getId() == val) {
					return a;
				}
			}
			throw new IllegalArgumentException("Invalid actor");
		}
	}
	
	public enum BetType {
		WIN(1),
		TIE(2),
		ACE(3),
		FACE(4),
		ACE_FACE(5);
		
		private final int id;
		
		private BetType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return this.id;
		}
		
		public static BetType valueOf(int val) {
			
			for (BetType a: values()) {
				if (a.getId() == val) {
					return a;
				}
			}
			throw new IllegalArgumentException("Invalid BetType");
		}
	}
	
	private final Actor actor;
	private final BetType betType;
	private final double betAmount;
	
	public BaccaratBet(Actor actor, BetType betType, double betAmount) {
		this.actor = actor;
		this.betType = betType;
		this.betAmount = betAmount;
	}
	
	public double getBetAmount() {
		return this.betAmount;
	}
	
	public BetType getBetType() {
		return this.betType;
	}
	
	public Actor getActor() {
		return this.actor;
	}
	
	public static BaccaratBet parse(CurrencyParser parser, String val) {
		String[] parts = val.split(" ");
		
		Actor actor = Actor.valueOf(Integer.valueOf(parts[0]));
		BetType betType = BetType.valueOf(Integer.valueOf(parts[1]));
        
		Double betAmount = parser.parseDouble(parts[2]);
		
		if (betType == BetType.WIN && actor == Actor.BOTH) {
			throw new IllegalArgumentException("Invalid bet");
		}
		
		if (betType == BetType.TIE && actor != Actor.BOTH) {
			throw new IllegalArgumentException("Invalid bet");
		}
		
		if (betAmount == null) {
			throw new IllegalArgumentException("Invalid bet amount");
		}
		
		return new BaccaratBet(actor, betType, betAmount);
	}
	
	@Override
	public String toString() {
		return actor.getId() + " " + betType.getId();
	}
	
	public boolean hasWon(BaccaratHand[] hands) {
		switch (this.betType) {
		case WIN:
			return actor == Actor.PLAYER? 
					hands[1].getValue() > hands[0].getValue()
				  : hands[0].getValue() > hands[1].getValue();
		case TIE:
			return hands[0].getValue() == hands[1].getValue();
		case ACE:
		case FACE:
		case ACE_FACE:
			if (actor == Actor.BOTH) {
				return hands[0].handSatisfiesBet(betType)
						&& hands[1].handSatisfiesBet(betType);
			} else if (actor == Actor.BANKER) {
				return hands[0].handSatisfiesBet(betType);
			} else if (actor == Actor.PLAYER) {
				return hands[1].handSatisfiesBet(betType);
			}
			
		default:
			throw new IllegalArgumentException("Invalid bet");
		}
	}
	
	public double getWinMultiplier() {
		switch (this.betType) {
			case WIN:
				return actor == Actor.PLAYER? 2.0d: 1.95d;
			case TIE:
				return 9.0d;
			case ACE:
				return actor == Actor.BOTH? 26.0d : 5.0d;
			case FACE:
				return actor == Actor.BOTH? 4.0d : 2.0d;
			case ACE_FACE:
				return actor == Actor.BOTH? 201.0d : 15.0d;
			default:
				throw new IllegalArgumentException("Invalid bet");
		}
	}
}
