package com.magneta.games;

import java.util.Map;

/**
 * The GameActionFilter interface defines the necessary behaviours of any action filter.
 */
public interface GameActionFilter {

    /**
     * The constant field key(name) value for the round ID.  
     */
    public static final String ROUND = "round";
    /**
     * The constant field key(name) value for the seat ID.  
     */
    public static final String SEAT_ID = "seat_id";
    /**
     * The constant field key(name) value for the action description.  
     */
    public static final String ACTION = "action_desc";
    /**
     * The constant field key(name) value for the action ID.  
     */
    public static final String ACTION_ID = "action_id";
    /**
     * The constant field key(name) value for the action sub-ID.  
     */
    public static final String SUB_ACTION_ID = "sub_action_id";
    /**
     * The constant field key(name) value for the action area.  
     */
    public static final String ACTION_AREA = "action_area";
    /**
     * The constant field key(name) value for the action amount.  
     */
    public static final String AMOUNT = "amount";
    /**
     * The constant field key(name) value for the action value.  
     */
    public static final String ACTION_VALUE = "action_value";
    
    /**
     * Filters the given GameAction object. 
     * 
     * @param action The GameAction to be filtered.
     * 
     * @return Returns null or a Map containing the following fields:
     * 
     * <table border="1"><br>
     * <tr><th>Field</th>       <th>Type</th>   <th>Description</th></tr>
     * <tr><td>round</td>       <td>int</td>    <td>The ID of the round</td></tr>
     * <tr><td>action_id</td>   <td>int</td>    <td>The action ID.</td></tr>
     * <tr><td>sub_action</td>  <td>int</td>    <td>The sub-ID of the action.</td></tr>
     * <tr><td>seat_id</td>     <td>int</td>    <td>The ID of the seat which performed the action.</td></tr>
     * <tr><td>action_desc</td> <td>String</td> <td>The description of the action.</td></tr>
     * <tr><td>action_area</td> <td>String</td> <td>The area of the action (OPTIONAL).</td></tr>
     * <tr><td>amount</td>      <td>double</td> <td>The amount the action may have returned or received (OPTIONAL).</td></tr>
     * <tr><td>action_value</td><td>String</td> <td>The outcome of the action when there is one (OPTIONAL).</td></tr>
     * </table>
     *  
     */
    Map<String,Object> filterAction(GameAction action);
}
