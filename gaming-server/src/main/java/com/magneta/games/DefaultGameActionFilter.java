package com.magneta.games;

import java.util.HashMap;
import java.util.Map;

/**
 * The DefaultGameActionFilter is the default filter for GameAction objects sent to the users. 
 */
public class DefaultGameActionFilter implements GameActionFilter {
    
	private boolean sendSeatId = false;

	public DefaultGameActionFilter(boolean sendSeatId) {
		this.sendSeatId = sendSeatId;
	}
	
    /**
     * Filters the given action.
     * @param action The action to filter.
     * @return Returns a Map containing the following fields:<br>
     * 
     * <table border="1">
     * <tr><th>Field</th>       <th>Type</th>   <th>Description</th></tr>
     * <tr><td>round</td>       <td>int</td>    <td>The ID of the round</td></tr>
     * <tr><td>action_desc</td> <td>String</td> <td>The description of the action.</td></tr>
     * <tr><td>action_area</td> <td>String</td> <td>The area of the action (OPTIONAL).</td></tr>
     * <tr><td>amount</td>      <td>double</td> <td>The amount the action may have returned or received (OPTIONAL).</td></tr>
     * <tr><td>action_value</td><td>String</td> <td>The outcome of the action when there is one (OPTIONAL).</td></tr>
     * </table>
     *  
     */
    @Override
    public Map<String, Object> filterAction(GameAction action) {
        if (("GUARD").equals(action.getDescription())){
            return null;
        }
        
        Map<String, Object> currAction = new HashMap<String, Object>();
        
        currAction.put(ROUND, action.getRoundId());
        currAction.put(ACTION, action.getDescription());
        
        if (this.sendSeatId) {
        	currAction.put(SEAT_ID, action.getSeatId());
        }
        
        if (action.getArea() != null) {
            currAction.put(ACTION_AREA, action.getArea());
        }
        
        if (action.getAmount() > 0.0){
            currAction.put(AMOUNT, action.getAmount());
        }
        
        if (action.getActionValue() != null) {
            currAction.put(ACTION_VALUE, action.getActionValue());
        }
        
        return currAction;
    }
}
