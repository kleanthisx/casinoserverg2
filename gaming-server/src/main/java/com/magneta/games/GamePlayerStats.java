package com.magneta.games;

public class GamePlayerStats {

	private double bets;
	private double wins;
	
	public double getBets() {
		return bets;
	}
	
	public double getWins() {
		return wins;
	}
	
	public void addBets(double bets) {
		this.bets += bets;
	}
	
	public void addWins(double wins) {
		this.wins += wins;
	}
	
	public void add(GamePlayerStats stats) {
		this.bets += stats.getBets();
		this.wins += stats.getWins();
	}
}
