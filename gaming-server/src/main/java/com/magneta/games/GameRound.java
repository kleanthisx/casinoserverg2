package com.magneta.games;

import java.util.Map;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.configuration.GameConfiguration;

/**
 * Game round. This is the interface implemented for each game.
 * Most games extend AbstractGameRound.
 *  
 * @author anarxia
 *
 */
public interface GameRound {

	/**
	 * The Id of the table for this round.
	 */
	long getTableId();
	
	/**
	 * The id for this round.
	 */
	int getId();

	/**
	 * The game template for the round.
	 * @return
	 */
	GameTemplateInfo getGameTemplate();

	/**
	 * The game running on the round.
	 */
	GameBean getGame();
	
	/**
	 * The configuration used.
	 */
	GameConfiguration getConfig();
	
	/**
	 * The statistics of the player for this round.
	 * The statistics are updated after each action is commited.
	 */
	GamePlayerStats getRoundStats();
	
	/**
	 * Checks whether the round has finished.
	 * @return
	 */
	boolean isFinished();

	/**
	 * Initialize round.
	 * 
	 * @param game
	 * @param gameTemplate
	 * @param configuration
	 * @param options
	 * @param tableId
	 * @param roundId
	 * @param seed
	 * @throws ActionFailedException
	 */
	void init(GameBean game, GameTemplateInfo gameTemplate,
			GameConfiguration configuration, RoundOptions options,
			long tableId, int roundId, byte[] seed)
			throws ActionFailedException;
	
	/**
	 * Initialize round from parent round.
	 * 
	 * @param parentRound
	 * @param seed
	 * @throws ActionFailedException
	 */
	void init(GameRound parentRound, byte[] seed) throws ActionFailedException;
	
	void setRoundListener(GameRoundListener roundListener);

	/**
	 * Loads actions into the round. This method must be called after init and before
	 * any other method that modifies the round state.
	 * @param player The player for the round.
	 * @param actions The actions to load.
	 * 
	 * @throws ActionFailedException
	 */
	void loadActions(GamePlayer player, Iterable<GameAction> actions)
			throws ActionFailedException;
	
	void doRequest(GamePlayer player, GameActionRequest request)
			throws ActionFailedException;

	void getInfo(GamePlayer player, Map<String, Object> info, boolean firstTime);
}
