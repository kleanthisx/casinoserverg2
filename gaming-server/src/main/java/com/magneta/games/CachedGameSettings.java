/**
 * 
 */
package com.magneta.games;


/**
 * @author anarxia
 *
 */
public class CachedGameSettings {
    private final GameSettings settings;
    private final long loadedTime;
    
    public CachedGameSettings(GameSettings settings) {
        this.settings = settings;
        this.loadedTime = System.currentTimeMillis();
    }
    
    public GameSettings getSettings() {
        return this.settings;
    }
    
    public long getLoadedTime() {
        return this.loadedTime;
    }
}
