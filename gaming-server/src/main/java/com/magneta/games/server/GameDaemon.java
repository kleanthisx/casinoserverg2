/**
 * 
 */
package com.magneta.games.server;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.XmlRpcHandlerMapping;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.casino.games.server.services.GameServerTaskManager;
import com.magneta.casino.games.server.services.ServiceLocator;
import com.magneta.casino.games.server.services.XmlRpcHandlerMappingFactory;
import com.magneta.casino.games.server.session.SessionIdGenerator;
import com.magneta.casino.games.server.xmlrpc.AdminXmlRpcClientListener;
import com.magneta.casino.games.server.xmlrpc.XmlRpcClientListener;
import com.magneta.casino.games.server.xmlrpc.http.HttpGameServer;
import com.magneta.casino.games.server.xmlrpc.socket.SocketXmlRpcRequestConfigFactoryImpl;
import com.magneta.xmlrpc.server.SocketXmlRpcClientListener;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestListener;
import com.magneta.xmlrpc.server.SocketXmlRpcServer;
import com.magneta.xmlrpc.server.XmlRpcSocketStreamServer;
import com.magneta.xmlrpc.server.classic.SocketXmlRpcServerImpl;
import com.magneta.xmlrpc.server.netty.NettySocketXmlRpcServer;
import com.magneta.xmlrpc.server.ssl.SSLContextFactory;

/**
 * @author Nicos
 *
 */
public class GameDaemon {
	
	private static final Logger log = LoggerFactory.getLogger(GameDaemon.class);
    private static final int SERVER_TIMEOUT = 60;
	
    private GameServerTaskManager taskManager;
    private final boolean standalone;

    private GameDaemon() {
    	standalone = true;
    }
    
    public GameDaemon(boolean standalone) {
    	this.standalone = standalone;
    }

    public void init() throws Exception {
    	if (standalone) {
    		ServiceLocator.init();
    	}
    	
        taskManager = ServiceLocator.getService(GameServerTaskManager.class);

        List<String> servers = parseServers();
        Collections.sort(servers);
        
        if (!servers.isEmpty()) {
        	SessionIdGenerator sessionIdGenerator = ServiceLocator.getService(SessionIdGenerator.class);
        	
        	HashSessionManager sessionManager = new HashSessionManager();
        	sessionManager.setMaxInactiveInterval(SERVER_TIMEOUT);
        	sessionManager.setSessionIdManager(new HashSessionIdManager(sessionIdGenerator.createRandom()));

        	for (String server: servers) {
        		addServer(server, sessionManager);
        	}
        }
    }

    public void destroy() {
        if (standalone) {
    		ServiceLocator.shutdown();
    	}
        this.taskManager = null;
    }

    /**
     * Get a list of all servers to start.
     * Finds all config items: server.<name>.enable = true
     * @return
     */
    private List<String> parseServers() {
        List<String> servers = new ArrayList<String>();

        String serversString = getString("server.connectors");
        if (serversString == null || serversString.isEmpty()) {
        	return servers;
        }
        
        String[] serverArray = serversString.split("\\s*(\\s|,)\\s*");
        
        for (String server: serverArray) {
            servers.add(server);
        }
        
        return servers;
    }
    
    private enum ServerProtocol {
        SERVER_PROTOCOL_NONE,
        SERVER_PROTOCOL_TCP,
        SERVER_PROTOCOL_HTTP
    }
    
    private String getString(String key, String def) {
        String value = Config.get(key);
        if (value != null) {
            value = value.trim();
            
            if (value.isEmpty())
                value = null;
        }
        
        if (value == null)
            return def;
        
        return value;
    }
    
    private String getString(String key) {
        return getString(key, null);
    }
    
    private XmlRpcSocketStreamServer createStreamServer(String handler) throws XmlRpcException {
        XmlRpcHandlerMapping handlerMapping;

        XmlRpcHandlerMappingFactory handlerMappingFactory = ServiceLocator.getService(XmlRpcHandlerMappingFactory.class);        
        handlerMapping = handlerMappingFactory.createMapping(handler);

        SocketXmlRpcRequestListener requestListener = ServiceLocator.getService(SocketXmlRpcRequestListener.class);
        XmlRpcSocketStreamServer xmlrpcServer = new XmlRpcSocketStreamServer(requestListener);
        xmlrpcServer.setHandlerMapping(handlerMapping);
        
        return xmlrpcServer;
    }
    
    private void addServer(String serverName, SessionManager sessionManager) throws XmlRpcException {
        String keyPrefix = serverName;

        ServerProtocol protocol = ServerProtocol.SERVER_PROTOCOL_NONE;
        String keystore = getString("ssl.keystore");
        String keystorePass = getString("ssl.password");
        
        String proto = getString(keyPrefix + ".protocol");
        if (proto != null) {
            proto = proto.toLowerCase();
            
            if ("tcp".equals(proto)) {
                protocol = ServerProtocol.SERVER_PROTOCOL_TCP;
            } else if ("http".equals(proto)) {
                protocol = ServerProtocol.SERVER_PROTOCOL_HTTP;
            } else {
            	throw new XmlRpcException("Unsupported protocol " + proto);
            }
        }
        
        String address = getString(keyPrefix + ".address");
        int port = Config.getInt(keyPrefix + ".port", 0);
        boolean ssl = Config.getBoolean(keyPrefix + ".ssl.enable", false);        
        keystore = getString(keyPrefix + ".ssl.keystore", keystore);
        keystorePass = getString(keyPrefix + ".ssl.password", keystorePass);
        String handler = getString(keyPrefix + ".handler", "game");
        String description = getString(keyPrefix + ".description", serverName);
        int timeout = Config.getInt(keyPrefix + ".idle.timeout", 0);
        int maxRequestSize = Config.getInt(keyPrefix + "request.maxSize", 0);
        
        String implementation = getString(keyPrefix + ".implementation", null);

        XmlRpcSocketStreamServer xmlrpcServer = createStreamServer(handler);
        
        switch (protocol) {
            case SERVER_PROTOCOL_HTTP:
            	SSLContext sslContext = null;
            	
            	if (implementation != null && !implementation.equalsIgnoreCase("jetty")) {
            		throw new XmlRpcException("Unknown HTTP server implementation: " + implementation);
            	}
            	
            	if (ssl) {
            		try {
						sslContext = SSLContextFactory.create(this.getClass().getClassLoader(), keystore, keystorePass);
					} catch (Exception e) {
						throw new XmlRpcException("Unable to creare SSL context for server " + serverName, e);
					}
            	}
            	
            	taskManager.addTask(new HttpGameServer(description, xmlrpcServer, sessionManager, address, port, sslContext));
                break;
            case SERVER_PROTOCOL_TCP:
            	SocketXmlRpcServer server;
            	SocketAddress localAddress = null;
            	if (address != null) {
            		try {
						localAddress = new InetSocketAddress(InetAddress.getByName(address), port);
					} catch (UnknownHostException e) {
						throw new XmlRpcException("Unable to create local address",e);
					}
            	} else {
            		localAddress = new InetSocketAddress(port);
            	}
            	
            	SocketXmlRpcClientListener clientListener = null;
            	
            	if ("game".equals(handler)) {
                    clientListener = new XmlRpcClientListener();
            	} else if ("admin".equals(handler)) {
            		clientListener = new AdminXmlRpcClientListener(taskManager);
            	}

        		if (implementation == null || "classic".equalsIgnoreCase(implementation)) {
        			server = new SocketXmlRpcServerImpl(xmlrpcServer, clientListener, localAddress);
        		} else if ("netty".equalsIgnoreCase(implementation)) {
        			server = new NettySocketXmlRpcServer(xmlrpcServer, clientListener, localAddress);
        		} else {
        			throw new XmlRpcException("Unknown TCP server implementation: " + implementation);
        		}

                if (ssl) {
                	try {
						server.setSslContext(SSLContextFactory.create(this.getClass().getClassLoader(), keystore, keystorePass));
					} catch (Exception e) {
						throw new XmlRpcException("Unable to creare SSL context for server " + serverName, e);
					}
                }
 
                if (timeout > 0) {
                	server.setIdleTimeout(timeout);
                }
                
                if (maxRequestSize > 0) {
                	server.setMaxRequestSize(maxRequestSize);
                }
                
                server.setSocketXmlRpcRequestConfigFactory(new SocketXmlRpcRequestConfigFactoryImpl(sessionManager));
                
                
                taskManager.addTask(description, server);
                break;
            case SERVER_PROTOCOL_NONE:
                log.error("Server " + serverName + ": no protocol specified. Ignoring");
                break;
            
        }
        
    }
    
    public void start() throws Exception {        
        taskManager.start();
    }

    public void stop() throws Exception {
    	taskManager.stop();
    }
    
    void waitForShutDown() {
    	taskManager.waitForTasks();
    }
}
