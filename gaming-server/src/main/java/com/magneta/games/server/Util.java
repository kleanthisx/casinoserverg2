package com.magneta.games.server;



/**
 * Util class contains utility methods necessary for the operation of some of 
 * the XML-RPC methods/classes.
 *
 */
public class Util {

    /**
     * Gets a long number (64-bit integer) from an array of 8 bytes.
     * @param bytesNumber An array of 8 bytes representing a long.
     * @return The long number created.
     */
    public static long getLong(byte[] bytesNumber)
    {
       long num;
       
       num = 	(  ((long)bytesNumber[0]) << 56) | 
       			( (((long)bytesNumber[1]) << 48) & 0x00FF000000000000L) |
       			( (((long)bytesNumber[2]) << 40) & 0x0000FF0000000000L) |
       			( (((long)bytesNumber[3]) << 32) & 0x000000FF00000000L) | 
       			( (((long)bytesNumber[4]) << 24) & 0x00000000FF000000L) |
       			( (((long)bytesNumber[5]) << 16) & 0x0000000000FF0000L) |
       			( (((long)bytesNumber[6]) <<  8) & 0x000000000000FF00L) |
       			( bytesNumber[7] & 0x00000000000000FFL); 
        
        return num;
        
    }
}
