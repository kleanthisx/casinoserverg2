/**
 * 
 */
package com.magneta.games.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.server.services.GameServerContext;
import com.magneta.casino.games.server.services.ServiceLocator;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;


/**
 * Logout daemon.
 * Handles database errors preventing logout update in the database
 * by periodically recording logouts that failed.
 * @author anarxia
 *
 */
public class LogoutDaemon implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(LogoutDaemon.class);
	
    private static LogoutDaemon _instance = null;
    private boolean stopped;
    private final int serverId;
    
    public static LogoutDaemon getInstance() {
        if (_instance == null) {
            _instance = new LogoutDaemon();
        }
        return _instance;
    }
    
    private List<UserToken> userLogoutList;
    
    private LogoutDaemon() {
        userLogoutList = new ArrayList<UserToken>();
        serverId = ServiceLocator.getService(GameServerContext.class).getServerId();
    }
    
    public void addLogoutUser(long userId, String sessionId) {
        if (userId < 1) {
            return;
        }
        synchronized(userLogoutList) {
            userLogoutList.add(new UserToken(sessionId, userId));
        }
    }
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {

        while(!stopped) {
            
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            	Thread.interrupted();
                if (stopped) {
                    break;
                }
            }

            Connection conn = null;
            PreparedStatement stmt = null;

            try {
                synchronized(userLogoutList) {
                    while (!userLogoutList.isEmpty()) {

                        UserToken token = userLogoutList.get(0);
                       
                        if (stmt == null) {
                        	if (conn == null) {
                        		conn = ConnectionFactory.getConnection();
                        		if (conn == null) {
                        			break;
                        		}
                        	}
                        	stmt = conn.prepareStatement("UPDATE user_logins SET logout_date=now_utc() WHERE server_id=? AND user_id=? AND logout_date IS NULL AND login_token=?");
                        }

                        stmt.setInt(1, serverId);
                        stmt.setLong(2, token.getUserId());
                        stmt.setString(3, token.getToken());
                        
                        stmt.executeUpdate();
                        userLogoutList.remove(0);
                        log.info("Logout Daemon: Logged out userId: {}", token.getUserId());
                    }

                }
            } catch (SQLException e) {
                log.error("Logout Daemon: Error while updating user logouts", e);
                continue;
            } finally {
                DbUtil.close(stmt);
                DbUtil.close(conn);
            }

        }
    }

    public void stop() {
        stopped = true;
    }
}
