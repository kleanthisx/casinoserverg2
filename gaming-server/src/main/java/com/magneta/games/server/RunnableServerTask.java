/**
 * 
 */
package com.magneta.games.server;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author anarxia
 *
 */
public class RunnableServerTask  implements ServerTask {
	
	private static final Logger log = LoggerFactory.getLogger(RunnableServerTask.class);
	
    private Runnable task;
    private Thread thread;
    private String name;
    
    public RunnableServerTask(String name, Runnable task) {
        this.task = task;
        this.thread = new Thread(task);
        this.name = name;
    }

    /** 
     * Starts the connector
     */
    @Override
    public void start() {
       this.thread.start(); 
    }
    
    @Override
    public void join() throws InterruptedException {
        this.thread.join();
    }
    
    @Override
    public void stop()  {
        try {
        	Method stop = this.task.getClass().getMethod("stop");
            stop.invoke(task);
        } catch (Throwable e) { log.info("Could not call stop method", e); }
        
        try {
            this.thread.interrupt();
        } catch (Throwable e) {}
    }
    
    @Override
    public String getName() {
        return this.name;
    }
}
