/**
 * 
 */
package com.magneta.games.server;


/**
 * @author anarxia
 *
 */
public class UserToken {

    private final String token;
    private final long userId;
    private final long createdTime;

    public UserToken(String token, long userId) {
        this.userId = userId;
        this.token = token;
        this.createdTime = System.currentTimeMillis();
    }
    
    public String getToken() {
        return this.token;
    }
    
    public long getUserId() {
        return this.userId;
    }
    
    public long getCreationTime() {
        return this.createdTime;
    }
}
