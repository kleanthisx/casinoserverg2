/**
 * 
 */
package com.magneta.games.server;

import com.magneta.casino.games.server.services.ServiceLocator;

/**
 * @author anarxia
 *
 */
public class DaemonShutdownHook extends Thread {

    @Override
    public void run() { 
        if (ServiceLocator.isInit()) {
        	ServiceLocator.shutdown();
        }
    }
}
