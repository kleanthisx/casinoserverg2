package com.magneta.games.server;

public interface ServerTask {

	void start();
	void stop();
	
	void join() throws InterruptedException;
	String getName();
}