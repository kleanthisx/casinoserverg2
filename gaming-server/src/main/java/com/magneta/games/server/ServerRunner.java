/**
 * 
 */
package com.magneta.games.server;

import com.magneta.casino.games.server.services.ServiceLocator;

/**
 * @author Nicos
 *
 */
public class ServerRunner {

    public static void main(String[] args) throws Exception {
        ServiceLocator.init();
        
        GameDaemon daemon = new GameDaemon(false); 
        daemon.init();
        daemon.start();
        
        Thread shutdownHook = new DaemonShutdownHook();
        Runtime.getRuntime().addShutdownHook(shutdownHook);
        
        daemon.waitForShutDown();
        try {
            Runtime.getRuntime().removeShutdownHook(shutdownHook);
        } catch (IllegalStateException e) {
        	
        } catch (Throwable e) {}

        ServiceLocator.shutdown();
    }
}
