package com.magneta.games;

/**
 * Class RoundOptions holds the values of certain parameters of any GameRound.
 */
public final class RoundOptions {
    
    private final double minBet;
    private final double maxBet;
    private final long owner;
    private final GameSettings gameSettings;
    
    /**
     * Constructs a new RoundOptions object.
     * @param minBet The minumum bet amount option value.
     * @param maxBet The maximum bet amount option value.
     */
    public RoundOptions(long owner, double minBet, double maxBet, GameSettings gameSettings) {
        this.minBet = minBet;
        this.maxBet = maxBet;
        this.owner = owner;
        this.gameSettings = gameSettings;
    }

    /**
     * Returns the maximum bet amount parameter value for this set of options.
     * @return The maximum bet amount parameter value for this set of options. 
     */
    public double getMaxBet() {
        return maxBet;
    }

    /**
     * Returns the minimum bet amount parameter value for this set of options.
     * @return the minimum bet amount parameter value for this set of options.
     */
    public double getMinBet() {
        return minBet;
    }
    
    public long getOwner() {
        return owner;
    }
    
    public GameSettings getGameSettings() {
        return gameSettings;
    }
}
