package com.magneta.games.bingo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.google.common.primitives.Ints;


public class BingoCardFactory {
	
	 public static final int[][] CARD_NUMBERS ={
		 /*1-9*/{1,2,3,4,5,6,7,8,9},
		 /*10-19*/{10,11,12,13,14,15,16,17,18,19},
		 /*20-29*/{20,21,22,23,24,25,26,27,28,29},
		 /*30-39*/{30,31,32,33,34,35,36,37,38,39},
		 /*40-49*/{40,41,42,43,44,45,46,47,48,49},
		 /*50-59*/{50,51,52,53,54,55,56,57,58,59},
		 /*60-69*/{60,61,62,63,64,65,66,67,68,69},
		 /*70-79*/{70,71,72,73,74,75,76,77,78,79},
		 /*80-90*/{80,81,82,83,84,85,86,87,88,89,90}
	 };
	
	 public static final int[][][][] CARD_SET_TEMPLATES = {
		 {
		    /* 1 */ {
		    			{ 6,11,22, 0, 0,55, 0, 0,80},//5
		    			{ 0,12, 0,33, 0, 0,67,72,84},//5
		    			{ 9,15, 0, 0,47, 0, 0,76,87}//5
		    		},
		    /* 2 */ {
		    			{ 4, 0, 0,31,40, 0,61, 0,83},//5
		    			{ 7,10,27, 0, 0,51,69, 0, 0},//5
		    			{ 8, 0, 0,32,41,58, 0,70, 0}//5
		    		},
		    /* 3 */ {
		    			{ 2, 0,20, 0,42,52, 0, 0,85},//5
		    			{ 0, 0,24,37,48, 0,64, 0,88},//5
		    			{ 0,13, 0,39, 0,59, 0,78,89}//5
		    		},
		    /* 4 */ {
		    			{ 0,17,28, 0, 0, 0,60,71,81},//5
		    			{ 1, 0, 0,30,46,50, 0,73, 0},//5
		    			{ 0, 0,29,38, 0,54,66, 0,82}
		    		},
		    /* 5 */ {
		    			{ 0,18,21, 0,43, 0,62, 0,86},//5
		    			{ 3, 0,23,36, 0, 0,65,75, 0},//5
		    			{ 0,19,25, 0,49,56,68, 0, 0}//5
		    		},
		    /* 6 */ {
		    			{ 0,14,26, 0,44, 0,63,74, 0},//5
		    			{ 5, 0, 0,34, 0,53, 0,77,90},//5
		    			{ 0,16, 0,35,45,57, 0,79, 0}//4
		    		}
		 },{
		 /* 1 */ {
	    			{ 6,11,22, 0, 0,55, 0, 0,80},
	    			{ 0,12, 0,33, 0, 0,67,72,84},
	    			{ 9,15, 0, 0,47, 0, 0,76,87}
	    		},
	    /* 2 */ {
	    			{ 4, 0, 0,31,40, 0,61, 0,83},
	    			{ 7,10,27, 0, 0,51,69, 0, 0},
	    			{ 8, 0, 0,32,41,58, 0,70, 0}
	    		},
	    /* 3 */ {
	    			{ 2, 0,20, 0,42,52, 0, 0,85},
	    			{ 0, 0,24,37,48, 0,64, 0,88},
	    			{ 0,13, 0,39, 0,59, 0,78,89}
	    		},
	    /* 4 */ {
	    			{ 0,17,28, 0, 0, 0,60,71,81},
	    			{ 1, 0, 0,30,46,50, 0,73, 0},
	    			{ 0, 0,29,38, 0,54,66, 0,82}
	    		},
	    /* 5 */ {
	    			{ 0,18,21, 0,43, 0,62, 0,86},
	    			{ 3, 0,23,36, 0, 0,65,75, 0},
	    			{ 0,19,25, 0,49,56,68, 0, 0}
	    		},
	    /* 6 */ {
	    			{ 0,14,26, 0,44, 0,63,74, 0},
	    			{ 5, 0, 0,34, 0,53, 0,77,90},
	    			{ 0,16, 0,35,45,57, 0,79, 0}
	    		}
		 }
	 };
	 
	 
	public static List<BingoCard> getBingoSetOfSix(Random random) {
		List<BingoCard> resultSet = new ArrayList<BingoCard>();
		int templatenumber =  0;
		int[][][] cardsTemplate = getTemplate(templatenumber);
		List<int[][]> cards = Arrays.asList(cardsTemplate);
		Collections.shuffle(cards,random);
		for (int i = 0; i < cards.size(); i++) {
			BingoCard bingoCard = new BingoCard();
			//setting the template
			bingoCard.setNumbers(cards.get(i));
			resultSet.add(bingoCard);
		}
		//Shufling the numbers
		int shufledNumbers[][] = getShufledNumbers(CARD_NUMBERS,random);
		
		//for every comumn
		for (int j = 0; j <9; j++) {
			//we make a list of the numbers for the column
			List<Integer> listedNumbers = new ArrayList<Integer>();
		    for (int index = 0; index < shufledNumbers[j].length; index++)
		    {
		    	listedNumbers.add(shufledNumbers[j][index]);
		    }
		
		// for every card we get the template and add appropriate numbers to the column where appropriate.
		for (BingoCard card:resultSet) {		
			    // we see hoe many numbers are in a column and we get them from the top of the list
				int numbersInColumn = card.getNumbersInColumn(j);
				List<Integer> tmpNumbers = new ArrayList<Integer>();
				if(numbersInColumn>0){
					for(int n=0; n<numbersInColumn;n++){
					tmpNumbers.add(listedNumbers.get(0));
					listedNumbers.remove(0);
					}
				}
				Collections.sort(tmpNumbers);
				card.setNumbersInColumn(j,tmpNumbers);
			}
			
		}
		return resultSet;
	}

	public static int[][] getShufledNumbers(int[][] cardNumbers, Random random) {
		int result[][] = cardNumbers.clone();
		for (int i = 0; i < cardNumbers.length; i++) {
			int[] tmpNumbers = result[i] ;
			 List<Integer> intList = new ArrayList<Integer>();
			    for (int index = 0; index < tmpNumbers.length; index++)
			    {
			        intList.add(tmpNumbers[index]);
			    }
			Collections.shuffle(intList, random);
			result[i]= Ints.toArray(intList);
		}
		return result;
	}

	private static int[][][] getTemplate(int templatenumber) {
		return CARD_SET_TEMPLATES[templatenumber];
	}
	
}
