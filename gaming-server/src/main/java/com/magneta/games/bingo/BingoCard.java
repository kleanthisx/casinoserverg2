package com.magneta.games.bingo;

import java.util.List;

public class BingoCard {
	
	private int numbers[][] = new int[3][9];
	
	public BingoCard() {
		for (int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[i].length; j++) {
				numbers[i][j]=0;
			}
		}
	}
	
	
	public int numOfBallsToWin(int[] picks) {
		int ballscounter = 0;
		for (int i = 0; i < picks.length; i++) {
			
			if(hasNumber(picks[i])){
				ballscounter++;
			}
			if(ballscounter==15){
				return i+1;
			}
		}
		return -1;
	}
	
	public int numOfBallsToWinOneLine(int[] picks,int line) {
		int ballscounter = 0;
		int[] row = getRow(line);
		for (int i = 0; i < row.length; i++) {
			for (int j = 0; j < picks.length; j++) {
				
				if(row[i]==picks[j]){
					ballscounter++;
				}
				if(ballscounter==5){
					return j+1;
				}
			}
		}
		return -1;
	}

	public boolean hasWon(int[] picks) {
		int ballscounter = 0;
		for (int i = 0; i < picks.length; i++) {
			
			if(hasNumber(picks[i])){
				ballscounter++;
			}
			if(ballscounter==15){
				return true;
			}
		}
		return false;
	}

	public static BingoCard parse(String string) {
		BingoCard result = new BingoCard();
		String tmpString[] = string.split(",");
		int[][] tmpnumbers = result.getNumbers();
		int index=0;
		for (int i = 0; i < tmpnumbers.length; i++) {
			for (int j = 0; j < tmpnumbers[i].length; j++) {
				tmpnumbers[i][j]=Integer.parseInt(tmpString[index]);
				index++;
			}
		}
		result.setNumbers(tmpnumbers);
		return result;
	}
	
	@Override
	public String toString(){
		String result = "";
		for (int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[i].length; j++) {
				result += numbers[i][j]+",";
			}
		}
		result = result.substring(0, result.length()-1);
		return result;
	}


	public int[][] getNumbers() {
		return numbers;
	}


	public void setNumbers(int numbers[][]) {
		if(numbers.length!=3 || numbers[0].length!=9){
			return ;
		}
		this.numbers = numbers;
	}
	
	public int[] getRow(int row){
		
		if(row<0 || row>2){
			return null;
		}
		
		return this.numbers[row];
	}
	
	public int[] getColumn(int column){
		
		if(column<0 || column>8){
			return null;
		}
		int result[] = new int[3];
		for (int i = 0; i < result.length; i++) {
			result[i] = this.numbers[i][column];
		}
		return  result;
	}
	
	public boolean hasNumber(int number){
		for (int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[i].length; j++) {
				if(numbers[i][j] == number)
					return true;
			}
		}
		return false;
	}


	public int getNumbersInColumn(int column) {
		
		int[] template = getColumn(column);
		int numbersCounter = 0;
		for (int i = 0; i < template.length; i++) {
			if(template[i]>0){
				numbersCounter++;
			}
		}
		return numbersCounter;
	}


	public void setNumbersInColumn(int column,List<Integer> tmpNumbers) {
		for (int i = 0; i < 3; i++) {
			if(numbers[i][column]>0){
				numbers[i][column]=tmpNumbers.get(0);
				tmpNumbers.remove(0);
			}
		}
	}
	
}
