package com.magneta.games.bingo;

import java.util.List;
import java.util.Random;

/**
 * The Deck class represents a deck of cards for any card-based game.
 */
public class Pot {

	private final int[] balls;

	/**
	 * Constructs a new Deck of cards. 
	 * @param numOfBalls The number of stacks to have in the deck.
	 * @param jokers A flag indicating whether the deck should or should not have jokers.
	 */
	public Pot(int numOfBalls) {

		balls = new int[numOfBalls];
		
		for (int i=0; i < numOfBalls; i++) {
			balls[i] = new Integer(i+1);
		}
	}

	/**
	 * Shuffles the balls in the pot.
	 * @param sr The Random object to be used to shuffle the deck's cards. 
	 */
	public void shuffleBalls(Random sr) {

		long[] ballRank = new long[balls.length];

		for (int rank=0; rank < ballRank.length; rank++) {
			ballRank[rank] = sr.nextLong();
		}

		for (int i=0; i < balls.length; i++) {
			for (int j=i+1; j < balls.length; j++) {
				if (ballRank[i] < ballRank[j]) {
					long tmp = ballRank[i];
					ballRank[i] = ballRank[j];
					ballRank[j] = tmp;

					int tmp_ball = balls[i];
					balls[i] = balls[j];
					balls[j] = tmp_ball;
				}
			}
		}
	}
	
	public int[] getBalls() {
		return this.balls;
	}
	

	/**
	 * Returns the number of cards this deck instance has.
	 * @return The number of cards this deck instance has.
	 */
	public int getBallsCount(){
		return balls.length;
	}

	/**
	 * Sets the deck so that the given cards are on the top.
	 * This is only used for testing and if you need this for any
	 * purpose other than testing you are doing it wrong.
	 * 
	 * @param topCards
	 */
	public void setupPot(List<Integer> topBalls) {
		int ballsMoved = 0;

		for (int card: topBalls) {
			for (int i=ballsMoved; i < balls.length; i++) {
				if (balls[i] == card) {
					int tmp = balls[i];
					balls[i] = balls[ballsMoved];
					balls[ballsMoved] = tmp;
					ballsMoved++;
					break;
				}
			}
		}
	}
}