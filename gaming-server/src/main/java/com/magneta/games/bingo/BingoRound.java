package com.magneta.games.bingo;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameActionRequest;
import com.magneta.games.GameActionRequestDefinition;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.GamePlayer;
import com.magneta.games.AbstractGameRound;
import com.magneta.games.JackpotsHandler;
import com.magneta.games.RoundFixedResultUtil;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.bingo.BingoConfiguration;

@GameTemplate("Bingo")
public class BingoRound extends AbstractGameRound {

	private static final String DRAW_ACTION = "g:DRAW";

	private static final GameActionRequestDefinition[] ALLOWED_ACTIONS = {
		/*Description  Area                     Amount                */
		new GameActionRequestDefinition(BET_ACTION,  ParameterType.REQUIRED,  ParameterType.REQUIRED),
	};

	private Pot pot;
	private List<BingoCard> bingoCards;
	private int numberOfCards;
	private double betAmount;
	private boolean finished;

	@Override
	public void roundInit() {
		this.bingoCards = BingoCardFactory.getBingoSetOfSix(getRandom());
		this.pot = new Pot(90);
		this.betAmount = 0.0;
		this.numberOfCards =6;
	}

	@Override
	protected void performAction(GameAction action) throws ActionFailedException {
		if (BET_ACTION.equals(action.getDescription())){
			if (!action.isLoaded()){
				performBet(action);
			} else{
				loadBet(action);
			}
		}
	}

	/**
	 * Performs the bet action.
	 * @param action The action request.
	 * @throws ActionFailedException
	 */
	private void performBet(GameAction action) throws ActionFailedException {
		// parcing valit format
		try{
			this.numberOfCards = Integer.parseInt(action.getArea());
			this.betAmount = Double.parseDouble(action.getActionValue());
		}catch (NumberFormatException e){
			throw new ActionFailedException("Invalid values parced");
		}

		// parcing valid area value
		if (this.numberOfCards <= 0 || numberOfCards >6) {
			throw new ActionFailedException("Invalid area format");
		}

		this.checkBet(action);

		String betArea = "";
		for (BingoCard card : bingoCards) {
			betArea += card.toString() +";";
		}

		action.setActionValue(betArea);
		action.setRoundStart();
		action.commit();
		if(!action.isLoaded()){
			JackpotsHandler.contributeToJackpot(this, action);
		}
		performPicks(action);
	}

	private void loadBet(GameAction action) throws ActionFailedException {
		DecimalFormat f = new DecimalFormat("0.00");
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		f.setDecimalFormatSymbols(symbols);



		String cardsPlayedAllInOne = action.getActionValue();
		String [] cardsPlayed = cardsPlayedAllInOne.split(";");
		for (int i = 0; i < cardsPlayed.length; i++) {
			try {
				BingoCard bet = BingoCard.parse(cardsPlayed[i]);
				bingoCards.add(bet);
			} catch (Throwable e) {
				throw new ActionFailedException("Invalid bet");
			}
		}
		this.betAmount = Double.parseDouble(f.format(action.getAmount()));
	}

	@Override
	protected void loadFinished(GamePlayer player) throws ActionFailedException {
		if (bingoCards != null && bingoCards.size() > 0 && bingoCards.size() < 7){
			GameAction loadedAction = new GameAction(player, this, 1, DRAW_ACTION, null, 0.0);
			performPicks(loadedAction);
		}
	}

	private void setPot() {
		String result = RoundFixedResultUtil.getFixedResult(getTableId(), getId());

		if (result == null) {
			return;
		}

		List<Integer> topBalls = new ArrayList<Integer>();
		String[] ballsSelected = result.split(" ");

		for (int i=0; i < ballsSelected.length; i++) {
			try {
				topBalls.add(Integer.parseInt(ballsSelected[i]));
			} catch (NumberFormatException e) {
				logError("Invalid number while parsing fixed reel slots");
				continue;
			}
		}



		if (topBalls.isEmpty()) {
			return;
		}

		pot.setupPot(topBalls);
	}

	/**
	 * @param action
	 * @throws ActionFailedException
	 */
	private void performPicks(GameAction action) throws ActionFailedException {
		GameAction drawAction = action.createGeneratedAction(null, 1, DRAW_ACTION, null, 0.0);
		
		/* Shuffle */
		pot.shuffleBalls(getRandom());
		// make function init opt to setup results
		setPot();

		int[] picks = pot.getBalls();

		/* Send actions for balls drawn */
		String picks_str = "";
		for (int pick: picks) {
			if (!picks_str.isEmpty()) {
				picks_str += ",";
			}
			picks_str += Integer.toString(pick);
		}

		drawAction.setActionValue(picks_str);

		/* Check for Line wins */
		ArrayList<Integer> numberOfBallsToLineWin = new ArrayList<Integer>();
		for (BingoCard card: bingoCards) {
			for (int i = 0; i < card.getColumn(0).length; i++) {
				numberOfBallsToLineWin.add(card.numOfBallsToWin(picks));			
			}
		}
		int minBallsIndex =  getMinValueIndex(numberOfBallsToLineWin);
		int minBallsToLinewin = numberOfBallsToLineWin.get(minBallsIndex);
		int lineWinCard = minBallsIndex/bingoCards.get(0).getColumn(0).length;
		int lineWinLine = minBallsIndex%bingoCards.get(0).getColumn(0).length;
		
		double maxMultiplier = this.getConfig().getMultiplier(numberOfBallsToLineWin.get(minBallsIndex), BingoConfiguration.LINE_NUMBERS);
		
		if(maxMultiplier>0){
			double winAmount = maxMultiplier*this.betAmount;
			//generating action for line win
			GameAction lineWinAction = drawAction.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, ""+lineWinCard+","+lineWinLine+","+Double.toString(maxMultiplier), winAmount);
			// TODO just send the index of win combo
			lineWinAction.setActionValue(Integer.toString(minBallsToLinewin));
		}	
		
		/* Check for wins */
		ArrayList<Integer> numberOfBallsToWin = new ArrayList<Integer>();
		for (BingoCard card2: bingoCards) {
			numberOfBallsToWin.add(card2.numOfBallsToWin(picks));
		}
		
		
		boolean hasWonJackpot = false;
		int minBallIndex =  getMinValueIndex(numberOfBallsToWin);
		int ballsToWin =  numberOfBallsToWin.get(minBallIndex);
		boolean isJackpotCombo = getConfig().isJackpotCombination(ballsToWin,15);
		double winAmount =0.0;
		double multiplier = getConfig().getMultiplier(ballsToWin,15);
		if(isJackpotCombo){
			double moneyFromPayout =  multiplier*this.betAmount;
			hasWonJackpot = JackpotsHandler.claimGameJackpot(this, action, 0, moneyFromPayout, 100);
		}

		if(multiplier >0.0 && !hasWonJackpot){
			winAmount = multiplier *this.betAmount;
			GameAction winAction = drawAction.createGeneratedAction(action.getPlayer(), 1, WIN_ACTION, Double.toString(multiplier), winAmount);
			String ballsToShow = "";
			for (int i = 0; i < minBallIndex; i++) {
				ballsToShow = ballsToShow + picks[i];
			}
			winAction.setActionValue(""+minBallIndex+","+ballsToShow);
		}

		drawAction.createGeneratedAction(null, 1, FINISH_ACTION, null, 0.0);
		drawAction.commit(true);
		this.finished = true;
	}

	
	private int getMinValueIndex(ArrayList<Integer> numberOfBallsToWin) {
		int minIndex = 0;
		int min = numberOfBallsToWin.get(0);
		for (int i = 0; i < numberOfBallsToWin.size(); i++) {
			if (numberOfBallsToWin.get(i) < min) {
				min = numberOfBallsToWin.get(i);
				minIndex = new Integer(i);
			}
		}
		return minIndex;
	}

	@Override
	public BingoConfiguration getConfig(){
		return (BingoConfiguration)super.getConfig();
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	protected void validateGameActionRequest(GameActionRequest request) throws ActionFailedException {      
		/* Check if the action is "syntactically" valid */
		AbstractGameRound.validateRequest(ALLOWED_ACTIONS, request);

		if (request.getSeatId() != 1) {   
			throw new ActionFailedException("Wrong seat ID (Should be 1).");
		} else if (finished) {
			throw new ActionFailedException("Round has finished.");
		}
	}
}
