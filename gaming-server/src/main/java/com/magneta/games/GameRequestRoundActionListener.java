package com.magneta.games;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameRequestRoundActionListener implements GameRoundListener {

	private static final Logger log = LoggerFactory.getLogger(GameRequestRoundActionListener.class);
	
	private List<Map<String,Object>> actions = null;
	
	public GameRequestRoundActionListener() {
	}

    /**
     * Gets the actions performed so far in the request.
     */
    public List<Map<String,Object>> getActions() {
        return actions;
    }
	
    /**
     * Adds the give action and any sub-actions it may have to all the viewer's sessions.
     * @param session The HttpSession of the viewer.
     * @param action The GameAction to be added to the viewers session.
     * @param filter The GameActionFilter to be used to filter the actions.
     */
    private void addAction(GameAction action, GameActionFilter filter) {

        Map<String,Object> filteredAction = filter.filterAction(action);
        if (filteredAction != null) {
        	if (actions == null) {
                actions = new ArrayList<Map<String,Object>>();
            }

            actions.add(filteredAction);
        }

        if (!action.isSaved() && !action.isLoaded()){
            log.warn("Action has not been saved. {}", action);
        }

        List<GameAction> genActions = action.getGeneratedActions();
        for (GameAction genAction: genActions) {
            addAction(genAction, filter);
        }

        List<GameAction> addActions = action.getAdditionalActions();
        for (GameAction next: addActions) {
            addAction(next, filter);
        }
    }

    /**
     * Called automatically when an action has been performed.
     */
    @Override
    public void onActionPerformed(AbstractGameRound round, GameAction action) {
    	GameActionFilter filter = round.getActionFilter();
    	addAction(action, filter);
    }
}
