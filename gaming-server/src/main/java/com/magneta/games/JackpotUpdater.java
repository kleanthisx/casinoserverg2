package com.magneta.games;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class JackpotUpdater implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(JackpotUpdater.class);
	
    /**
     * Jackpot update interval in milliseconds
     */
    private static final int JACKPOT_UPDATE_INTERVAL = 2500;
    
    private static JackpotUpdater _instance = null;
    
    private boolean stopped;
    private Map<Integer, Double> gameJackpots;
    private Map<Integer, Double> mysteryJackpots;
    private Map<Long, Double> corporateJackpots;
    
    private JackpotUpdater() {
        gameJackpots = new HashMap<Integer, Double>();
        mysteryJackpots = new HashMap<Integer, Double>();
        corporateJackpots = new HashMap<Long, Double>();
    }
    
    public static JackpotUpdater getInstance() {
        if (_instance == null) {
            _instance = new JackpotUpdater();
        }
        return _instance;
    }
    

    @Override
    public void run() {
        
        while(!stopped) {
            Connection conn = ConnectionFactory.getConnection();
            Statement stmt = null;
            ResultSet rs = null;
            if (conn != null) {
                try {
                    String sql = 
                        "SELECT jackpots.target_corporate, jackpots.game_id, jackpots.auto, jackpot_balances.balance AS jackpot_amount" + 
                        " FROM jackpots" +
                        " INNER JOIN jackpot_balances ON jackpots.jackpot_id = jackpot_balances.jackpot_id" +
                        " WHERE jackpots.finish_date IS NULL";

                    stmt = conn.createStatement();
                    rs = stmt.executeQuery(sql);

                    Map<Integer, Double> newGameJackpots = new HashMap<Integer, Double>();
                    Map<Long, Double> newCorporateJackpots = new HashMap<Long, Double>();
                    Map<Integer, Double> newMysteryJackpots = new HashMap<Integer, Double>();

                    while (rs.next()){
                        Integer gameId = (Integer)rs.getObject("game_id");
                        Long corpId = (Long)rs.getObject("target_corporate");
                        double amount = rs.getDouble("jackpot_amount");

                        if ((gameId != null) && (corpId != null)){
                            continue;
                        } else if ((gameId == null) && (corpId == null)){
                            newCorporateJackpots.put(new Long(0), amount);
                        } else if (gameId != null){
                            if (rs.getBoolean("auto")){
                                newGameJackpots.put(gameId, amount);
                            } else {
                                newMysteryJackpots.put(gameId, amount);
                            }
                        } else if (corpId != null) {
                            newCorporateJackpots.put(corpId, amount);
                        }
                    }

                    this.gameJackpots = newGameJackpots;
                    this.mysteryJackpots = newMysteryJackpots;
                    this.corporateJackpots = newCorporateJackpots;
                } catch (SQLException e) {
                    log.warn("Error retrieving jackpot amounts.", e);
                } finally {
                    DbUtil.close(rs);
                    DbUtil.close(stmt);
                    DbUtil.close(conn);
                }
            }
            
            try {
                Thread.sleep(JACKPOT_UPDATE_INTERVAL);
            } catch (InterruptedException e) {
                if (stopped) {
                    break;
                }
            }
        }
    }
    
    public void stop() {
        stopped = true;
    }
    
    public double getGameJackpot(int gameID){
        Double amt = gameJackpots.get(gameID);
        if (amt == null){
            return 0.0;
        } 
        return amt;
    }
    
    public double getMysteryJackpot(int gameID) {
    	Double amt = mysteryJackpots.get(gameID);
    	if (amt == null){
    		return 0.0;
    	} 

    	return amt;
    }
    
    public double getCorporateJackpot(Long corpId) {
    	if (corpId == null) {
    		return 0.0;
    	}
    	
        Double amt = corporateJackpots.get(corpId);
        if (amt == null){
            return 0.0;
        } 
        return amt;
    }
    
    public double getMegaJackpot() {
    	Double amt = corporateJackpots.get(0L);
        if (amt == null){
            return 0.0;
        } 
        
        return amt;
    }
}
