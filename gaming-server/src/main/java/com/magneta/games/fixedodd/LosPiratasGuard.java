/**
 * 
 */
package com.magneta.games.fixedodd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.lospiratas.LosPiratasRound;

/**
 * @author User
 *
 */
public class LosPiratasGuard {

private static boolean showedMsg = false;
    
	private static final Logger log = LoggerFactory.getLogger(LosPiratasGuard.class);

    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int reelStops[], GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        Double maxAllowedPayout = settings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}
		
        log.debug("Los Piratas called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Los Piratas Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        log.debug("ORIGINAL REEL STOPS");
        for (int i: reelStops){
            log.debug("Reel stop: {}", i);
        }
        log.debug("*******************");
        
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        log.debug("Player allowed wins: {}", allowedWins);
        if (allowedWins < 0.0) {
            allowedWins = 0.0;
        }
        
        double playerWins = getWins(config, reelStops, betUnit);
        
        double totalBet = betUnit *(config.getPaylines().length);
		
		if (playerWins<totalBet) {
			return;
		}
        
        log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
        
        if (playerWins <= allowedWins && playerWins <= maxAllowedPayout){
            return;
        }
        
        log.info("Los Piratas guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int[][] REELS = config.getReels(0);
        for (int changes = 0; changes < REELS[2].length; changes++) {
            reelStops[2]++;
            if (reelStops[2] >= REELS[2].length) {
                reelStops[2] = 0;
            }
            
            for (int changes2 = 0; changes2 < REELS[6].length; changes2++) {
            
                reelStops[6]++;
                if (reelStops[6] >= REELS[6].length) {
                    reelStops[6] = 0;
                }
            
                playerWins = getWins(config, reelStops, betUnit);
            
                if (playerWins <= allowedWins && playerWins <= maxAllowedPayout) {
                    log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
                    guardAction.setActionValue("1");
                    log.debug("FINAL REEL STOPS");
                    for (int i: reelStops){
                        log.debug("Reel stop: "+i);
                    }
                    log.debug("*******************");
                    return;
                }
            }
        }
        guardAction.setActionValue("0");
    }
    
    public static double getWins(SlotConfiguration config, int[] reelStops, double betUnit) {
    	
    	int[][] reels = config.getReels(0);

    	
    	int[][] reelsView = new int[3][reels.length];
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];

            reelsView[0][i] = reels[i][(reelStop > 0 ? (reelStop - 1) : (reels[i].length-1))];
            reelsView[1][i] = reels[i][reelStop];
            reelsView[2][i] = reels[i][(reelStop+1) % reels[i].length];
        }
        
        int[] currLine = new int[5];
        double playerWins = 0.0;
        
        for (int i=0;i<config.getPaylines().length;i++) {
            //check line from the LEFT view
            config.getPayline(reelsView, i, currLine);
            
            int combo = LosPiratasRound.getPaylineWinCombination(config, currLine);
            if (combo != -1){
                 playerWins += config.getPayout(combo) * betUnit;
            }
        }
        
        return playerWins;
    }
}
