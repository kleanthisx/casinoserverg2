/**
 * 
 */
package com.magneta.games.fixedodd;

import java.util.Arrays;
import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.GameAction;
import com.magneta.games.configuration.poker.PokerConstants;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration.TexasPokerSideGame;
import com.magneta.games.configuration.poker.texasholdem.WinCombination;
import com.magneta.games.poker.PokerUtil;
import com.magneta.games.poker.PokerUtil.ComboState;
/**
 * 
 * @author kleanthis
 *
 * This guard works as follows :
 * When the player makes a bet it assumes that the player will win 
 * maximum bet or loose only the basic bet. Therefore if the cards are not in favor of a tie or loss, it redeals untill it does.
 */
public class HoldEmPokerGuard {
	
	private static final Logger log = LoggerFactory.getLogger(HoldEmPokerGuard.class);

	private static boolean showedMsg = false;
	
	public static void guardFirstHand(HoldEmHandGuardParams params){
		boolean guard = params.getSettings().get("guard", Boolean.TYPE);
		double payoutRatio = params.getSettings().get("payout_ratio", Double.TYPE);

		log.debug("Texas Hold Em Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("exas Hold Em Guard enabled");
		}

		if (!guard) {
			return;
		}
		
		double allowedWins = FixedOddUtil.calculateRemainingWins(params.getOwnerID(), params.getGameID(), payoutRatio);

		// find the win combo 
		Integer[] dealerFullHand = new Integer[7];
		Integer[] playerFullHand = new Integer[7];

		Integer[] commonhand = params.getCommonhand();
		for(int i=0;i<5;i++){
			dealerFullHand[i] = commonhand[i];
			playerFullHand[i] = commonhand[i];
		}

		Integer[] dealerhand = params.getDealerhand();
		Integer[] playerhand = params.getPlayerhand();
		for(int i=0;i<2;i++){
			dealerFullHand[i+5] = dealerhand[i];
			playerFullHand[i+5] = playerhand[i];
		}

		// evaluate hand.
		StringBuilder indexes = new StringBuilder("");
		int handDiff = compareHands(dealerFullHand, playerFullHand,indexes);
		double amountToPay = 0.0;
		log.info("handDiff "+handDiff);
		if (handDiff < 0) {
			amountToPay = (params.getBetAmount() * 5);
		}
		log.info("amountToPay "+amountToPay);
		if(params.getSidebet() > 0.0){
			TexasPokerSideGame combo = checkForSidegameCombination(playerhand, dealerhand);
			if(combo!=null){
				WinCombination winCombo = params.getConfig().getSideWinCombination(combo);
				amountToPay = amountToPay+((winCombo.getAmount() + 1.0) * params.getSidebet());
			}
		}
		// evaluate sidebet hand
		log.info("amountToPay "+amountToPay);
		//		check the win amount...
		if(amountToPay<=allowedWins) {
			return;
		}

		//we don't fix anything but swap the cards.
		log.info("Texas Hold Em poker guard: swaping cards and evaluating...");

		GameAction guardAction = null;
		guardAction = params.getParentAction().createGeneratedAction(null, 0, "GUARD", null, 0.0);
		// swap the cards
		Integer[] tmpHand = Arrays.copyOf(dealerhand, dealerhand.length);
		for (int i = 0; i < tmpHand.length; i++) {
			dealerhand[i] = playerhand[i];
			playerhand[i]=tmpHand[i];	
		}
		

		// setting the amount..
		double amountToPay1 = 0.0;
		handDiff = compareHands(playerFullHand, dealerFullHand, indexes);
		if (handDiff < 0) {
			amountToPay1 =  params.getBetAmount() *5;
		}
		// evaluate sidebet hand
		if(params.getSidebet() > 0.0){
			TexasPokerSideGame combo = checkForSidegameCombination(playerhand, dealerhand);
			if(combo!=null){
				WinCombination winCombo = params.getConfig().getSideWinCombination(combo);
				amountToPay1 = amountToPay1+((winCombo.getAmount()+1.0)*params.getSidebet());
			}
		}
		log.info("amountToPay1 "+amountToPay1);
		//		compare the win amount... and find out what to do
		if(amountToPay1 < amountToPay) {
			log.info("swaped");
			guardAction.setActionValue("1");
			return;
		}
		log.info("not swaped");
		guardAction.setActionValue("0");
		tmpHand = Arrays.copyOf(dealerhand, dealerhand.length);
		for (int i = 0; i < tmpHand.length; i++) {
			dealerhand[i] = playerhand[i];
			playerhand[i]=tmpHand[i];	
		}
		
	}

	private static int compareHands(Integer[] dealerHand, Integer[] playerHand,StringBuilder indexes) {
		boolean[] dealerCardsInvolved = new boolean[dealerHand.length];
		boolean[] playerCardsInvolved = new boolean[playerHand.length];


		int dealerCombo = getWinningCombination(dealerHand, dealerCardsInvolved);
		int playerCombo = getWinningCombination(playerHand, playerCardsInvolved);

		Integer[] dealerNewHand = sortHand(dealerHand, dealerCardsInvolved);
		Integer[] playerNewHand = sortHand(playerHand, playerCardsInvolved);

		for (int i=0;i<playerHand.length;i++) {
			for (Integer x :playerNewHand){
				if (playerHand[i].intValue()==x.intValue()){
					indexes.append(" "+i);
				}
			}
		}

		if (dealerCombo != playerCombo) {
			return dealerCombo - playerCombo;
		}



		int tmpdif = 0;
		switch (playerCombo) {
		case PokerConstants.STRAIGHT_FLUSH:
		case PokerConstants.STRAIGHT:
			if (PokerUtil.getCardValue(dealerNewHand[1]) == PokerUtil.getCardValue(playerNewHand[1])) {
				tmpdif =  (PokerUtil.getCardValue(dealerNewHand[0]) % 14) - (PokerUtil.getCardValue(playerNewHand[0]) % 14);
			}
			//			else{
			//			tmpdif = PokerUtil.getCardValue(dealerNewHand[1]) - PokerUtil.getCardValue(playerNewHand[1]);
			//		}

			if(tmpdif!=0)
				return tmpdif;
		}

		for (int i=0; i < dealerNewHand.length; i++) {

			int cardDiff = PokerUtil.getCardValue(dealerNewHand[i]) - PokerUtil.getCardValue(playerNewHand[i]);

			if (cardDiff != 0) {
				return cardDiff;
			}
		}

		return 0;
	}

	private static int getWinningCombination(Integer[] cards, boolean[] cardsInvolved) {
		return PokerUtil.getWinningCombination(cards, cardsInvolved, 0);
	}

	private static Integer[] sortHand(Integer[] origHand, boolean[] cardsInvolved) {		
		Integer[] hand = new Integer[origHand.length];

		int involvedCount = 0;
		boolean[] cardsInvolvedTmp = new boolean[cardsInvolved.length]; 

		/* Add involved cards on top */
		for (int i=0; i < origHand.length; i++) {
			if (!cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount] = origHand[i];
			cardsInvolvedTmp[involvedCount]=true;
			involvedCount++;
		}

		/* Add the remainingCards */
		int currIndex = 0;
		for (int i=0; i < origHand.length; i++) {
			if (cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount + currIndex] = origHand[i];
			cardsInvolvedTmp[currIndex+involvedCount]=false;
			currIndex++;
		}


		if (involvedCount > 0) {
			Arrays.sort(hand, 0, involvedCount, new CardComparator(hand, cardsInvolvedTmp, true));
		}

		if (involvedCount < hand.length) {
			Arrays.sort(hand, involvedCount, hand.length, new CardComparator(hand, cardsInvolvedTmp, false));
		}

		hand = Arrays.copyOfRange(hand, 0, 5);
		return hand;
	}

	private static class CardComparator implements Comparator<Integer> {

		private final Integer[] hand;
		private final boolean[] cardsInvolved;
		private final boolean involved;

		public CardComparator(Integer[] hand, boolean[] cardsInvolved, boolean involved) {
			this.hand = hand;
			this.cardsInvolved = cardsInvolved;
			this.involved = involved;
		}

		private int countCardNumber(int number) {
			int count = 0;
			for (int i=0; i < hand.length; i++) {
				if (!cardsInvolved[i] == involved) {
					continue;
				}

				if (Card.getCardNumber(hand[i]) == number) {
					count++;
				}
			}

			return count;
		}

		@Override
		public int compare(Integer card0, Integer card1) {
			if (involved) {
				int frequencyDiff = countCardNumber(Card.getCardNumber(card1)) - countCardNumber(Card.getCardNumber(card0));

				if (frequencyDiff != 0) {
					return frequencyDiff;
				}
			}

			return PokerUtil.getCardValue(card1) - PokerUtil.getCardValue(card0);
		}

	}

	private static TexasPokerSideGame checkForSidegameCombination(Integer[] playerHand,Integer[] dealerHand) {

		ComboState state = new ComboState();
		PokerUtil.CardMatcher[] cardsToFind;
		int cardsUsed=2;

		boolean playerHasAces =PokerUtil.checkPair(state,playerHand, 15);
		boolean dealerHasAces =PokerUtil.checkPair(state,dealerHand, 15);

		//check if both have aces
		if(playerHasAces && dealerHasAces)
			return TexasPokerSideGame.AA_AA;

		if(playerHasAces) 
			return TexasPokerSideGame.AA;

		/* A K suited */
		for (int race=0; race < 4; race++) {
			cardsToFind = new PokerUtil.CardMatcher[] {
					new PokerUtil.SingleCardMatcher(Card.getCard(Card.KING, race)),
					new PokerUtil.SingleCardMatcher(Card.getCard(Card.ACE, race))
			};
			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.AK_SUITED;
			}
		}

		/* A Q or A J suited */
		for (int race=0; race < 4; race++) {
			for(int c =Card.JACK; c<Card.KING; c++){
				cardsToFind = new PokerUtil.CardMatcher[] {
						new PokerUtil.SingleCardMatcher(Card.getCard(c, race)),
						new PokerUtil.SingleCardMatcher(Card.getCard(Card.ACE, race))
				};

				PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
				if (state.getComboCardsUsed()==cardsUsed) {
					return TexasPokerSideGame.AQ_AJ_SUITED;
				}
			}
		}


		/* A K unsuited */
		cardsToFind = new PokerUtil.CardMatcher[] {
				new PokerUtil.CardNumberMatcher(Card.KING),
				new PokerUtil.CardNumberMatcher(Card.ACE)
		};
		PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
		if (state.getComboCardsUsed()==cardsUsed) {
			return TexasPokerSideGame.AK_UNSUITED;
		}

		/* K K or Q Q or J J*/
		for(int c =Card.JACK; c<=Card.KING; c++){
			cardsToFind = new PokerUtil.CardMatcher[] {
					new PokerUtil.CardNumberMatcher(c),
					new PokerUtil.CardNumberMatcher(c)
			};
			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.KK_QQ_JJ;
			}
		}

		/* A Q or A J suited */
		for(int c =Card.JACK; c<Card.KING; c++){
			cardsToFind = new PokerUtil.CardMatcher[] {
					new PokerUtil.CardNumberMatcher(c),
					new PokerUtil.CardNumberMatcher(Card.ACE)
			};

			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.AQ_AJ_UNSUITED;
			}
		}

		/* 10 10 to 2 2*/
		for(int c =2; c<11; c++){
			cardsToFind = new PokerUtil.CardMatcher[] {
					new PokerUtil.CardNumberMatcher(c),
					new PokerUtil.CardNumberMatcher(c)
			};

			PokerUtil.findCombo(state.reset(), playerHand, cardsToFind);
			if (state.getComboCardsUsed()==cardsUsed) {
				return TexasPokerSideGame.TEN_TO_TWO_PAIR;
			}
		}

		return null;

	}

}