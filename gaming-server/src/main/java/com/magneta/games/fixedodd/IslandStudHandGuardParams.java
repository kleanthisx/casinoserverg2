package com.magneta.games.fixedodd;

import java.io.Serializable;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;

public class IslandStudHandGuardParams implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//
	private int gameID;
	
	//
	private Integer[] playerhand;
	
	//
	private Integer[] dealerhand;
	
	//
	private long ownerID;
	
	//
	private IslandStudPokerConfiguration config;
	
	//
	private double betAmount;
	
	//
	private double jackpotBet;
	
	//
	private GameSettings settings;
	
	//
	private GameAction parentAction;
	
	public int getGameID() {
		return gameID;
	}
	public void setGameID(int gameID) {
		this.gameID = gameID;
	}
	public Integer[] getPlayerhand() {
		return playerhand;
	}
	public void setPlayerhand(Integer[] playerhand) {
		this.playerhand = playerhand;
	}
	public Integer[] getDealerhand() {
		return dealerhand;
	}
	public void setDealerhand(Integer[] dealerhand) {
		this.dealerhand = dealerhand;
	}

	public long getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(long ownerID) {
		this.ownerID = ownerID;
	}
	public IslandStudPokerConfiguration getConfig() {
		return config;
	}
	public void setConfig(IslandStudPokerConfiguration config2) {
		this.config = config2;
	}
	public double getBetAmount() {
		return betAmount;
	}
	public void setBetAmount(double betAmount) {
		this.betAmount = betAmount;
	}
	public void setJackpotBet(double jackpotBet) {
		this.jackpotBet = jackpotBet;
	}
	public double getJackpotBet() {
		return jackpotBet;
	}
	public GameSettings getSettings() {
		return settings;
	}
	public void setSettings(GameSettings settings) {
		this.settings = settings;
	}
	public GameAction getParentAction() {
		return parentAction;
	}
	public void setParentAction(GameAction parentAction) {
		this.parentAction = parentAction;
	}
}
