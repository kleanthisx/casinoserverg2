package com.magneta.games.fixedodd;

import java.util.Arrays;
import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.GameAction;
import com.magneta.games.configuration.poker.PokerConstants;
import com.magneta.games.poker.PokerUtil;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : When the player makes a bet it assumes
 *         that the player will win maximum bet or loose only the basic bet.
 *         Therefore if the cards are not in favor of a tie or loss, it redeals
 *         untill it does.
 */
public final class IslandStudPokerGuard {

	private static final Logger log = LoggerFactory.getLogger(IslandStudPokerGuard.class);
	
	/**
	 * private constructor for utility class
	 */
	private IslandStudPokerGuard() {
	}

	/**
	 * flag about whether to log the use of guard.
	 */
	private static boolean showedMsg = false;

	/**
	 * This method is called to apply a guard filter on a game. This
	 * 
	 * @param params a parameters object, 
	 * due to the big number of parameters otherwise.
	 */
	public static void guardFirstHand(IslandStudHandGuardParams params) {
		boolean guard = params.getSettings().get("guard", Boolean.TYPE);
		
		if (!guard) {
			return;
		}
		
		double payoutRatio = params.getSettings().get("payout_ratio", Double.TYPE);

		log.debug("Island Stud Guard called with settings: guard = " + guard
				+ ", payout_ratio = " + payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Island Stud Em Guard enabled");
		}

		double allowedWins = FixedOddUtil.calculateRemainingWins(
						params.getOwnerID(), params.getGameID(), payoutRatio);

		// find the win combo
		Integer[] dealerFullHand = new Integer[params.getDealerhand().length];
		Integer[] playerFullHand = new Integer[params.getPlayerhand().length];

		Integer[] dealerhand = params.getDealerhand();
		Integer[] playerhand = params.getPlayerhand();
		for (int i = 0; i < params.getDealerhand().length; i++) {
			dealerFullHand[i] = dealerhand[i];
			playerFullHand[i] = playerhand[i];
		}

		//		check if dealer is qualified
		int dealerCombo = PokerUtil.getWinningCombination(dealerhand,new boolean[dealerhand.length], 0);
		boolean dealerQualified = dealerCombo > 0 || (handCardNumberCount(dealerhand, Card.ACE) > 0
				&& handCardNumberCount(dealerhand, Card.KING) > 0);

		// evaluate hand.
		StringBuilder indexes = new StringBuilder("");
		int handDiff = compareHands(dealerFullHand, playerFullHand, indexes);
		double amountToPay = 0.0;
		log.info("handDiff " + handDiff);
		if (handDiff < 0 || !dealerQualified) {
			amountToPay = (params.getBetAmount() * 3);
		}
		log.info("amountToPay " + amountToPay);
//		if (params.getJackpotBet() > 0.0) {
//			int combination = PokerUtil.getWinningCombination(playerhand,new boolean[playerhand.length], 0);
//			WinCombination winCombo = params.getConfig().getJackpotWinCombination(combination);
//			if (winCombo != null && winCombo.getAmount() > 0.001) {if (winCombo.getType() == WinType.JACKPOT_POOL_RATIO) {
//				amountToPay = amountToPay + (winCombo.getAmount() * 10000.0 * params.getJackpotBet());
//			} else {
//				amountToPay = amountToPay
//				+ (winCombo.getAmount() + 1.00 * params.getJackpotBet());
//			}
//			}
//		}

		log.info("amountToPay " + amountToPay);
		// check the win amount...
		if (amountToPay <= allowedWins) {
			return;
		}

		// we don't fix anything but swap the cards.
		log.info("Island Stud guard: swaping cards and evaluating...");

		GameAction guardAction = params.getParentAction().createGeneratedAction(null, 0,
				"GUARD", null, 0.0);
		// swap the cards
		Integer[] tmpHand = Arrays.copyOf(dealerhand, dealerhand.length);
		for (int i = 0; i < tmpHand.length; i++) {
			dealerhand[i] = playerhand[i];
			playerhand[i] = tmpHand[i];
		}

		//		check if dealer is qualified
		dealerCombo = PokerUtil.getWinningCombination(dealerhand,new boolean[dealerhand.length], 0);
		dealerQualified = dealerCombo > 0 || (handCardNumberCount(dealerhand, Card.ACE) > 0
				&& handCardNumberCount(dealerhand, Card.KING) > 0);

		// evaluate hand.
		indexes = new StringBuilder("");
		handDiff = -1*handDiff;

		// setting the amount..
		double amountToPay1 = 0.0;
		log.info("handDiff " + handDiff);
		if (handDiff < 0 || !dealerQualified) {
			amountToPay1 = (params.getBetAmount() * 3);
		}
		
		log.info("amountToPay " + amountToPay1);
//		this is commented out because jackpot in not concidered part of the game
//		if (params.getJackpotBet() > 0.0) {
//			int combination = PokerUtil.getWinningCombination(playerhand,new boolean[playerhand.length], 0);
//			WinCombination winCombo = params.getConfig().getJackpotWinCombination(combination);
//			if (winCombo != null && winCombo.getAmount() > 0.001) {if (winCombo.getType() == WinType.JACKPOT_POOL_RATIO) {
//				amountToPay1 = amountToPay1 + (winCombo.getAmount() * 10000.0 * params.getJackpotBet());
//			} else {
//				amountToPay1 = amountToPay1
//				+ (winCombo.getAmount() * 1.00 * params.getJackpotBet());
//			}
//			}
//		}

		log.info("amountToPay1 " + amountToPay1);
		// compare the win amount... and find out what to do
		if (amountToPay1 < amountToPay) {
			log.info("swaped");
			guardAction.setActionValue("1");
			return;
		}


		log.info("not swaped");
		guardAction.setActionValue("0");
		tmpHand = Arrays.copyOf(dealerhand, dealerhand.length);
		for (int i = 0; i < tmpHand.length; i++) {
			dealerhand[i] = playerhand[i];
			playerhand[i] = tmpHand[i];
		}

	}

	private static int compareHands(Integer[] dealerHand, Integer[] playerHand,
			StringBuilder indexes) {
		boolean[] dealerCardsInvolved = new boolean[dealerHand.length];
		boolean[] playerCardsInvolved = new boolean[playerHand.length];

		int dealerCombo = getWinningCombination(dealerHand, dealerCardsInvolved);
		int playerCombo = getWinningCombination(playerHand, playerCardsInvolved);

		Integer[] dealerNewHand = sortHand(dealerHand, dealerCardsInvolved);
		Integer[] playerNewHand = sortHand(playerHand, playerCardsInvolved);

		for (int i = 0; i < playerHand.length; i++) {
			for (Integer x : playerNewHand) {
				if (playerHand[i].intValue() == x.intValue()) {
					indexes.append(" " + i);
				}
			}
		}

		if (dealerCombo != playerCombo) {
			return dealerCombo - playerCombo;
		}

		int tmpdif = 0;
		switch (playerCombo) {
		case PokerConstants.STRAIGHT_FLUSH:
		case PokerConstants.STRAIGHT:
			if (PokerUtil.getCardValue(dealerNewHand[1]) == PokerUtil
					.getCardValue(playerNewHand[1])) {
				tmpdif = (PokerUtil.getCardValue(dealerNewHand[0]) % 14)
				- (PokerUtil.getCardValue(playerNewHand[0]) % 14);
			}

			if (tmpdif != 0)
				return tmpdif;
		}

		for (int i = 0; i < dealerNewHand.length; i++) {

			int cardDiff = PokerUtil.getCardValue(dealerNewHand[i])
			- PokerUtil.getCardValue(playerNewHand[i]);

			if (cardDiff != 0) {
				return cardDiff;
			}
		}

		return 0;
	}

	private static int getWinningCombination(Integer[] cards,
			final boolean[] cardsInvolved) {
		return PokerUtil.getWinningCombination(cards, cardsInvolved, 0);
	}

	private static Integer[] sortHand(Integer[] origHand,
			final boolean[] cardsInvolved) {
		Integer[] hand = new Integer[origHand.length];

		int involvedCount = 0;
		boolean[] cardsInvolvedTmp = new boolean[cardsInvolved.length];

		/* Add involved cards on top */
		for (int i = 0; i < origHand.length; i++) {
			if (!cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount] = origHand[i];
			cardsInvolvedTmp[involvedCount] = true;
			involvedCount++;
		}

		/* Add the remainingCards */
		int currIndex = 0;
		for (int i = 0; i < origHand.length; i++) {
			if (cardsInvolved[i]) {
				continue;
			}
			hand[involvedCount + currIndex] = origHand[i];
			cardsInvolvedTmp[currIndex + involvedCount] = false;
			currIndex++;
		}

		if (involvedCount > 0) {
			Arrays.sort(hand, 0, involvedCount, new CardComparator(hand,
					cardsInvolvedTmp, true));
		}

		if (involvedCount < hand.length) {
			Arrays.sort(hand, involvedCount, hand.length, new CardComparator(
					hand, cardsInvolvedTmp, false));
		}

		hand = Arrays.copyOfRange(hand, 0, 5);
		return hand;
	}

	private static class CardComparator implements Comparator<Integer> {

		private final Integer[] hand;
		private final boolean[] cardsInvolved;
		private final boolean involved;

		public CardComparator(Integer[] hand, boolean[] cardsInvolved,
				boolean involved) {
			this.hand = hand;
			this.cardsInvolved = cardsInvolved;
			this.involved = involved;
		}

		private int countCardNumber(int number) {
			int count = 0;
			for (int i = 0; i < hand.length; i++) {
				if (!cardsInvolved[i] == involved) {
					continue;
				}

				if (Card.getCardNumber(hand[i]) == number) {
					count++;
				}
			}

			return count;
		}

		@Override
		public int compare(Integer card0, Integer card1) {
			if (involved) {
				int frequencyDiff = countCardNumber(Card.getCardNumber(card1))
				- countCardNumber(Card.getCardNumber(card0));

				if (frequencyDiff != 0) {
					return frequencyDiff;
				}
			}

			return PokerUtil.getCardValue(card1)
			- PokerUtil.getCardValue(card0);
		}

	}

	private static int handCardNumberCount(Integer[] hand, int number) {
		int count = 0;

		for (int card: hand) {
			int no = Card.getCardNumber(card);

			if (number == no) {
				count++;
			}
		}

		return count;
	}

}