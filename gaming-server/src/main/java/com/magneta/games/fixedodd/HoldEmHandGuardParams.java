package com.magneta.games.fixedodd;

import java.io.Serializable;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;

public class HoldEmHandGuardParams implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//
	private int gameID;
	
	//
	private Integer[] playerhand;
	
	//
	private Integer[] dealerhand;
	
	//
	private Integer[] commonhand;
	
	//
	private long ownerID;
	
	//
	private TexasHoldemConfiguration config;
	
	//
	private double betAmount;
	
	//
	private double sidebet;
	
	//
	private GameSettings settings;
	
	//
	private GameAction parentAction;
	
	public int getGameID() {
		return gameID;
	}
	public void setGameID(int gameID) {
		this.gameID = gameID;
	}
	public Integer[] getPlayerhand() {
		return playerhand;
	}
	public void setPlayerhand(Integer[] playerhand) {
		this.playerhand = playerhand;
	}
	public Integer[] getDealerhand() {
		return dealerhand;
	}
	public void setDealerhand(Integer[] dealerhand) {
		this.dealerhand = dealerhand;
	}
	public Integer[] getCommonhand() {
		return commonhand;
	}
	public void setCommonhand(Integer[] commonhand) {
		this.commonhand = commonhand;
	}
	public long getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(long ownerID) {
		this.ownerID = ownerID;
	}
	public TexasHoldemConfiguration getConfig() {
		return config;
	}
	public void setConfig(TexasHoldemConfiguration config) {
		this.config = config;
	}
	public double getBetAmount() {
		return betAmount;
	}
	public void setBetAmount(double betAmount) {
		this.betAmount = betAmount;
	}
	public double getSidebet() {
		return sidebet;
	}
	public void setSidebet(double sidebet) {
		this.sidebet = sidebet;
	}
	public GameSettings getSettings() {
		return settings;
	}
	public void setSettings(GameSettings settings) {
		this.settings = settings;
	}
	public GameAction getParentAction() {
		return parentAction;
	}
	public void setParentAction(GameAction parentAction) {
		this.parentAction = parentAction;
	}
}
