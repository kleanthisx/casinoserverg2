package com.magneta.games.fixedodd;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.BonusBox;
import com.magneta.games.slots.hiddengarden.HiddenGardenRound;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : If the amount won is greater than the amount
 *          allowed to win the guard respinds in a hope to win less. this is repeated
 *           five times. Bonus stages are not taken into consideration.
 */
public class HiddenGardenGuard {

	private static final Logger log = LoggerFactory.getLogger(HiddenGardenGuard.class);

	private static boolean showedMsg = false;

	public static int[] guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int[] reelStops, int maxPayline, GameSettings settings, GameAction parentAction, HiddenGardenRound round,Random random) throws ActionFailedException{
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);
		Double maxAllowedPayout = settings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}
		log.debug("Hidden Garden called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Hidden Garden Guard enabled");
		}

		if (!guard) {
			return reelStops;
		}

		log.debug("ORIGINAL REEL STOPS");
		for (int i: reelStops){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");


		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		log.debug("Player allowed wins: "+allowedWins);
		if (allowedWins < 0.0) {
			allowedWins = 0.0;
		}

		double playerWins1 =round.evaluatePositionWinnings(reelStops);

		log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins1);

		if (playerWins1 > maxAllowedPayout){
			log.debug("watch this ");
		}

		if ((playerWins1 <= maxPayline+1*betUnit)){
			return reelStops;
		}

		// if both of the conditions are true.
		if ((playerWins1 <= allowedWins) && (playerWins1 <= maxAllowedPayout)){
			return reelStops;
		}

		log.info("Hidden Garden guard: attempt to fix odds");

		double minPayoutSoFar = playerWins1;
		int[] minReelStopsSoFar = Arrays.copyOf(reelStops, reelStops.length);
		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
		guardAction.setActionValue("0");

		for(int t=0;t<5;t++){

			int[] reelstmp = spinReels(config.getReels(0),random);
			double playerWins2 = round.evaluatePositionWinnings(reelstmp);


			if (playerWins2 < minPayoutSoFar){
				minPayoutSoFar = playerWins1;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
			}

			if ((playerWins2 <= maxPayline+1*betUnit)){
				minPayoutSoFar = playerWins1;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
				break;
			}

			if(minPayoutSoFar <= allowedWins && minPayoutSoFar<= maxAllowedPayout){
				break;
			}

		}
		log.debug("Found acceptable wins of: "+minPayoutSoFar+" VS allowed of: "+allowedWins);

		log.debug("FINAL REEL STOPS");
		for (int i: minReelStopsSoFar){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");
		return minReelStopsSoFar;
	}

	//	public static void getPossibleBoxBonus(int[][] codeBonus, double totalBetAmount, int gameID, long ownerID, GameSettings gameSettings) {
	//		double payoutRatio = gameSettings.get("payout_ratio", Double.TYPE);
	//		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
	//
	//		int maxMulltiplier = new Double(Math.floor(allowedWins)/(totalBetAmount)).intValue();
	//		if(maxMulltiplier<0){
	//			maxMulltiplier = 0;
	//		}
	//
	//	}

	protected final static int[] spinReels(int[][] roundReels,Random random) {
		int[] reelStops = new int[roundReels.length];

		for (int i=0;i<reelStops.length;i++){
			reelStops[i] = random.nextInt(roundReels[i].length);
		}
		return reelStops;
	}

	public static void getPossibleBoxBonus(GameAction parentAction, double betAmount, Integer gameId,
			long owner, GameSettings gameSettings, BonusBox[] bonusBoxes,
			int area, int boxCountToOpen,Random random) {
		boolean guard = gameSettings.get("guard", Boolean.TYPE);

		if (!guard) {
			return;
		}

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Hidden Garden Guard enabled");
		}

		double payoutRatio = gameSettings.get("payout_ratio", Double.TYPE);
		double allowedWins = FixedOddUtil.calculateRemainingWins(owner, gameId, payoutRatio);
		Double maxAllowedPayout = gameSettings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}

		maxAllowedPayout = Math.min(maxAllowedPayout, allowedWins);
		maxAllowedPayout = Math.max(maxAllowedPayout, 0d);

		if (bonusBoxes[area].getPayout() * betAmount <= maxAllowedPayout) {
			return;
		}

		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
		guardAction.setActionValue("0");

		/*
		 * Swap the selected box with one within the payout limits.
		 * For area 0 always swap with a box that has payout
		 */
		boolean found = false;

		/* Try to find any box within the max allowed payout but not zero */
		for (int i = 0; i < bonusBoxes.length; i++) {
			if (bonusBoxes[i].isOpened() || bonusBoxes[i].getPayout() > maxAllowedPayout || bonusBoxes[i].getPayout() > allowedWins ) {
				continue;
			}
			
			BonusBox tmp = bonusBoxes[area];
			bonusBoxes[area] = bonusBoxes[i];
			bonusBoxes[i] = tmp;
			guardAction.setActionValue("1");
			found = true;
			break;
		}

		if (!found) {
			
			List<Double> prizes = new ArrayList<Double>();
			for (int i=0; i<bonusBoxes.length; ++i){
				if(!bonusBoxes[i].isOpened())
					prizes.add(bonusBoxes[i].getPayout());
			}

			Collections.sort(prizes);
			
			double[] smaller_prizes = new double[boxCountToOpen];
			for (int i = 0; i < smaller_prizes.length; i++) {
				smaller_prizes[i] = prizes.get(i);
			}
			
			int selectedBox = random.nextInt(boxCountToOpen);

			int minBox = area;

			for (int i = 0; i < bonusBoxes.length; i++) {
				if (!bonusBoxes[i].isOpened() && bonusBoxes[i].getPayout()==smaller_prizes[selectedBox]) {
					minBox = i;
					break;
				}					
			}

			BonusBox tmp = bonusBoxes[area];
			bonusBoxes[area] = bonusBoxes[minBox];
			bonusBoxes[minBox] = tmp;
			guardAction.setActionValue("1");

			log.warn("Round {}-{}: Giving {} payout more than max due to game rules.", new Object[] {
					parentAction.getTableId(), 
					parentAction.getRoundId(), (bonusBoxes[area].getPayout() * betAmount) - maxAllowedPayout});

		} 
	}
}
