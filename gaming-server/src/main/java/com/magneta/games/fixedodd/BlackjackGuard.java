/**
 * 
 */
package com.magneta.games.fixedodd;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.DeckOutOfCardsException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.blackjack.BlackjackDealer;
import com.magneta.games.blackjack.BlackjackDeck;
import com.magneta.games.blackjack.BlackjackSeat;


/**
 * @author Nicos
 *
 */
public class BlackjackGuard {

	private static final Logger log = LoggerFactory.getLogger(BlackjackGuard.class);
	
    private static boolean showedMsg = false;

    public static void guardRound(int gameID, Deck deck, BlackjackSeat seats[], BlackjackDealer dealer, GameSettings settings, GameAction parentAction) {
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Blackjack Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        int targetDealerHand = -1;
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Blackjack Guard enabled");
        }
        
        if (dealer.hasBlackjack() || !guard) {
            return;
        }
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        for (BlackjackSeat seat: seats) {
            if (seat != null && seat.hasBet() && !seat.hasBusted()) {
                double allowedWins = FixedOddUtil.calculateRemainingWins(seat.getPlayer().getUserId(), gameID, payoutRatio);
                
                if (allowedWins < seat.getBet() * 2.0) {
                    
                    if (seat.hands[0] != null && !seat.hands[0].hasBlackjack() && !seat.hands[0].hasBusted()) {
                        targetDealerHand = Math.max(targetDealerHand, seat.hands[0].sumCards());
                    }
                    
                    if (seat.hands[1] != null && !seat.hands[1].hasBlackjack() && !seat.hands[1].hasBusted()) {
                        targetDealerHand = Math.max(targetDealerHand, seat.hands[1].sumCards());
                    }
                }
            }
        }
        
        int dealerHand = dealer.sumCards();
        
        if (dealerHand < 17 && targetDealerHand > 0) {
            
             
            int cardsTried = 0;
            List<Integer> cardsTaken = new ArrayList<Integer>();
            boolean foundCombo = false;
            
            do {
                cardsTried++;
                try {
                    int card;
                    int cardValue;
                    int newDealerHand = dealerHand;
                    int attemptedCards = 0;
                    boolean aceAsEleven = false;
                    boolean shouldStop = false;
                    
                    do {
                        attemptedCards ++;
                        card = deck.getNextCard();
                        cardsTaken.add(card);

                        cardValue = BlackjackDeck.getCardValue(card);

                        if (cardValue == Card.ACE && newDealerHand < 11) {
                            newDealerHand += 11;
                            aceAsEleven = true;
                        } else {
                            newDealerHand += cardValue;
                        }

                        if (newDealerHand > 21 && aceAsEleven) {
                            aceAsEleven = false;
                            newDealerHand -= 10;
                        }

                        if (newDealerHand >= 17) {
                            shouldStop = true;
                        }
                        
                    } while(!shouldStop && ((newDealerHand < targetDealerHand || (targetDealerHand == 21 && newDealerHand != 21)) || newDealerHand > 21));

                    for (int i=attemptedCards - 1; i > 0; i--) {
                        deck.returnCard(cardsTaken.remove(cardsTaken.size() -1));
                    }

                    if ((newDealerHand == 21 || newDealerHand >= targetDealerHand) && newDealerHand < 22) {
                        foundCombo = true;
                        deck.returnCard(cardsTaken.remove(cardsTaken.size() -1));
                        guardAction.setActionValue("1");
                    }


                } catch (DeckOutOfCardsException e) {
                    log.info("Out of cards while guarding round");
                    for (int i=cardsTaken.size()-1; i>=0; i--) {
                        deck.returnCard(cardsTaken.get(i));
                    }
                    guardAction.setActionValue("0");
                    return;
                }
            } while (!foundCombo);
            
            if (cardsTried > 1) {
                int nextCard = deck.getNextCard();
                log.info("Blackjack Guard: Dealer Card " + Card.getCardNumber(nextCard) + " Cards changed: " + (cardsTried - 1));
                deck.returnCard(nextCard);
            }
        } else {
            guardAction.setActionValue("0");
        }
    }
}
