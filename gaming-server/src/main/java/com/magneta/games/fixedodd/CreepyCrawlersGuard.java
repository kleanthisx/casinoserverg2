package com.magneta.games.fixedodd;


import java.util.Arrays;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.creepycrawlers.CreepyCrawlersRound;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : If the amount won is greater than the amount
 *          allowed to win the guard respinds in a hope to win less. this is repeated
 *           five times. Bonus stages are not taken into consideration.
 */
public class CreepyCrawlersGuard {

	private static final Logger log = LoggerFactory.getLogger(CreepyCrawlersGuard.class);
	
	private static boolean showedMsg = false;

	public static int[] guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int reelSet, int[] reelStops, GameSettings settings, GameAction parentAction, CreepyCrawlersRound round) throws ActionFailedException{
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);

		log.debug("CreepyCrawlers called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("CreepyCrawlers Guard enabled");
		}

		if (!guard) {
			return reelStops;
		}

		log.debug("ORIGINAL REEL STOPS");
		for (int i: reelStops){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");

		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		log.debug("Player allowed wins: {}", allowedWins);
		if (allowedWins < 0.0) {
			allowedWins = 0.0;
		}

		double playerWins =round.evaluatePositionWinnings(reelSet, reelStops);

		log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);

		if ((playerWins <= allowedWins)/* || (multiplier <= 10.0)*/){
			return reelStops;
		}

		log.info("CreepyCrawlers guard: attempt to fix odds");
		
		int[] reelstmp = Arrays.copyOf(reelStops, reelStops.length);
		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

		for(int t=0;t<5;t++){

			reelstmp = spinReels(config.getReels(reelSet));
			playerWins =round.evaluatePositionWinnings(reelSet, reelstmp);

			if (playerWins <= allowedWins) {
				log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
				guardAction.setActionValue("1");
				log.debug("FINAL REEL STOPS");
				for (int i: reelstmp){
					log.debug("Reel stop: {}", i);
				}
				log.debug("*******************");
				
				return reelstmp;
			}
		}
		guardAction.setActionValue("0");
		return reelStops;
	}

	protected final static int[] spinReels(int[][] roundReels) {
		Random random = new Random();
		int[] reelStops = new int[roundReels.length];

		for (int i=0;i<reelStops.length;i++){
			reelStops[i] = random.nextInt(roundReels[i].length);
		}
		return reelStops;
	}

}
