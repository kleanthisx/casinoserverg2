package com.magneta.games.fixedodd;


import java.util.Arrays;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.kingofaztec.KingOfAztecRound;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : If the amount won is greater than the amount
 *          allowed to win the guard respinds in a hope to win less. this is repeated
 *           five times. Bonus stages are not taken into consideration.
 */
public class KingOfAztecGuard {
	
	private static final Logger log = LoggerFactory.getLogger(KingOfAztecGuard.class);

	private static boolean showedMsg = false;

	public static int[] guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int maxPayline, int[] rellsetChosen, int[] reelStops, GameSettings settings, GameAction parentAction, KingOfAztecRound round, Random random) throws ActionFailedException{
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);
		Double maxAllowedPayout = settings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}
		
		log.debug("KingOfAztec called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("KingOfAztec Guard enabled");
		}

		if (!guard) {
			return reelStops;
		}

		log.debug("ORIGINAL REEL STOPS");
		for (int i: reelStops){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");


		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		log.debug("Player allowed wins: "+allowedWins);
		if (allowedWins < 0.0) {
			allowedWins = 0.0;
		}
		int reelSet = rellsetChosen[0];
		double playerWins =round.evaluatePositionWinnings(reelSet, reelStops);

		double totalBet = betUnit *(maxPayline+1);
		
		if (playerWins<totalBet) {
			return reelStops;
		}
		
		log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
		
		if (playerWins > maxAllowedPayout){
			log.debug("watch this");
		}
		
		// if both of the conditions are true.
		if ((playerWins <= allowedWins) && (playerWins <= maxAllowedPayout)){
			return reelStops;
		}

		log.info("KingOfAztec guard: attempt to fix odds");
		int minRelsetSoFar = reelSet;
		double minPayoutSoFar = playerWins;
		int[] minReelStopsSoFar = Arrays.copyOf(reelStops, reelStops.length);
		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
		int[][] weights = round.getWeights();
		
		for(int t=0;t<10;t++){
			int reelSetTmp = round.getReelBasedOnWeights(weights,random);
			int[] reelstmp = spinReels(config.getReels(reelSetTmp),random);
			playerWins =round.evaluatePositionWinnings(reelSetTmp, reelstmp);

			if (playerWins < minPayoutSoFar){
				minRelsetSoFar = reelSetTmp;
				minPayoutSoFar = playerWins;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
			}
			
			if (playerWins <= (maxPayline+1)*betUnit){
				minRelsetSoFar = reelSetTmp;
				minPayoutSoFar = playerWins;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
				break;
			}
			
			if(minPayoutSoFar <= allowedWins && minPayoutSoFar<= maxAllowedPayout){
				break;
			}
			
			
		}
		log.debug("Found acceptable wins of: "+minPayoutSoFar+" VS allowed of: "+allowedWins);
		guardAction.setActionValue("1");
		log.debug("FINAL REEL STOPS");
		for (int i: minReelStopsSoFar){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");
		rellsetChosen[0] = new Integer(minRelsetSoFar);
		return minReelStopsSoFar;
	}

	protected final static int[] spinReels(int[][] roundReels,Random random) {
		int[] reelStops = new int[roundReels.length];

		for (int i=0;i<reelStops.length;i++){
			reelStops[i] = random.nextInt(roundReels[i].length);
		}
		return reelStops;
	}

}
