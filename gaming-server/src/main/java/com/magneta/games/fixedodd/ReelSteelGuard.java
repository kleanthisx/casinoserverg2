package com.magneta.games.fixedodd;


import java.util.Arrays;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.reelsteel.ReelSteelRound;

/**
 * @author kleanthis
 * 
 * This guard works as follows : If the amount won is greater than the amount
 * allowed to win the guard respins in a hope to win less. this is repeated
 * five times. Bonus stages are not taken into consideration.
 */
public class ReelSteelGuard {
	
	private static final Logger log = LoggerFactory.getLogger(ReelSteelGuard.class);

	private static boolean showedMsg = false;

	public static int[] guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int reelSet, int[] reelStops, GameSettings settings, GameAction parentAction, ReelSteelRound reelSteelRound,int maxLine, int[] rellsetChosen, Random random) throws ActionFailedException{
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);
		Double maxAllowedPayout = settings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}
		
		log.debug("Reel Steel guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio+", max_payout = "+maxAllowedPayout);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Reel Steel guard enabled");
		}

		if (!guard) {
			return reelStops;
		}

		log.debug("ORIGINAL REEL STOPS");
		for (int i: reelStops){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");


		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		log.debug("Player allowed wins: "+allowedWins);
		if (allowedWins < 0.0) {
			allowedWins = 0.0;
		}

		// calculated in real money.
		double playerWins =reelSteelRound.evaluatePositionWinnings(reelSet, reelStops);

		log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
		
		if (playerWins > maxAllowedPayout){
			log.debug("watch this");
		}
		
		if (playerWins <= (maxLine+1)*betUnit){
			return reelStops;
		}
		
		// if both of the conditions are true.
		if ((playerWins <= allowedWins) && (playerWins <= maxAllowedPayout)){
			return reelStops;
		}

		log.info("Reel Steel guard: attempt to fix odds");
		int minReelsetSoFar = rellsetChosen[0];
		double minPayoutSoFar = playerWins;
		int[] minReelStopsSoFar = Arrays.copyOf(reelStops, reelStops.length);
		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

		for(int i = 0;i<10;i++){
			int reelsetTmp = reelSet;
			int[] reelstmp = spinReels(config.getReels(reelsetTmp),random);
			playerWins =reelSteelRound.evaluatePositionWinnings(reelsetTmp, reelstmp);

			if (playerWins < minPayoutSoFar){
				minReelsetSoFar = reelsetTmp;
				minPayoutSoFar = playerWins;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
			}
			
			if (playerWins <= (maxLine+1)*betUnit){
				minReelsetSoFar = reelsetTmp;
				minPayoutSoFar = playerWins;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
				break;
			}
			
			if(minPayoutSoFar <= allowedWins && minPayoutSoFar<= maxAllowedPayout){
				break;
			}
		}
		// logging the guard action.
		log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
		log.debug("FINAL REEL STOPS");
		for (int i: minReelStopsSoFar){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");
		rellsetChosen[0]= minReelsetSoFar;
		return minReelStopsSoFar;

	}

	protected final static int[] spinReels(int[][] roundReels, Random random) {
		
		int[] reelStops = new int[roundReels.length];

		for (int i=0;i<reelStops.length;i++){
			reelStops[i] = random.nextInt(roundReels[i].length);
		}
		
		return reelStops;
	}

}
