/**
 * 
 */
package com.magneta.games.fixedodd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.juicyfruity.JuicyFruityRound;

/**
 * @author User
 *
 */
public class JuicyFruityGuard {
    
	private static final Logger log = LoggerFactory.getLogger(JuicyFruityGuard.class);
	
    private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int reelStops[], GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Juicy Fruity Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Pirates Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        log.debug("ORIGINAL REEL STOPS");
        for (int i: reelStops){
            log.debug("Reel stop: "+i);
        }
        log.debug("*******************");
        
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        log.debug("Player allowed wins: {}", allowedWins);
        if (allowedWins < 0.0) {
            allowedWins = 0.0;
        }
        
        double playerWins = getWins(config, reelStops, betUnit);
        
        log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
        
        if ((playerWins <= allowedWins)/* || (multiplier <= 10.0)*/){
            return;
        }
        
        log.info("Juicy Fruity guard: attempt to fix odds");
        int[][] REELS = config.getReels(0);
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        for (int changes = 0; changes < REELS[2].length; changes++) {
            reelStops[1]++;
            if (reelStops[1] >= REELS[2].length) {
                reelStops[1] = 0;
            }
            playerWins = getWins(config, reelStops, betUnit);

            if (playerWins <= allowedWins) {
            	guardAction.setActionValue("1");
            	
                log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
                log.debug("FINAL REEL STOPS");
                for (int i: reelStops){
                    log.debug("Reel stop: {}", i);
                }
                log.debug("*******************");
                return;
            }
        }
        guardAction.setActionValue("0");
    }
    
    public static double getWins(SlotConfiguration config, int[] reelStops, double betUnit){
        //get the reel view
        int[][] REELS = config.getReels(0);
        int[][] reelsView = new int[3][REELS.length];
        
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];

            reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
            reelsView[1][i] = REELS[i][reelStop];
            reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
        }
        
        int[] currLine = new int[REELS.length];
        double playerWins = 0.0;
        
        for (int i=0;i<currLine.length;i++){
            config.getPayline(reelsView, i, currLine);
            
            int combo = JuicyFruityRound.getPaylineWinCombination(config, currLine);
            if (combo != -1){
                 playerWins += config.getPayout(combo) * betUnit;
            }
        }
        
        int combo = JuicyFruityRound.getReelViewWinCombination(config, reelsView);
        if (combo != -1){
            playerWins += config.getPayout(combo) * betUnit;
        }
        
        return playerWins;
    }
}
