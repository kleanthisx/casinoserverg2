package com.magneta.games.fixedodd;


import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.RainbowRichesConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.rainbowriches.RainbowRichesRound;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : It calculates the regular payouts from the bet lines.
 *         If the amount won is greater than the allowed winnings then it will respin the reels
 *         and re-evaluate untill either the result is acceptable or the iterations run out.
 */
public class RainbowRichesGuard {
	
	private static final Logger log = LoggerFactory.getLogger(RainbowRichesGuard.class);

	private static boolean showedMsg = false;

	public static int[] guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int[] rellsetChosen, int[] reelStops, int maxPayline, GameSettings settings, GameAction parentAction, RainbowRichesRound round) throws ActionFailedException{
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);
		Double maxAllowedPayout = settings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}
		
		log.debug("Rainbow Riches called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Rainbow Riches Guard enabled");
		}

		if (!guard) {
			return reelStops;
		}

		log.debug("ORIGINAL REEL STOPS");
		for (int i: reelStops){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");


		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		log.debug("Player allowed wins: "+allowedWins);
		if (allowedWins < 0.0) {
			allowedWins = 0.0;
		}

		int reelSet =  rellsetChosen[0];
		double playerWins =round.evaluatePositionWinnings(reelSet , reelStops);

		int[][] reelsViewIn = round.calculateReelView(config.getReels(reelSet), reelStops);
		List<Integer> viewCombosIn = RainbowRichesRound.getReelViewWinCombinations(config, reelsViewIn,reelSet);

		for (Integer viewcombo : viewCombosIn) {
			Integer currentSelectCombo = viewcombo;
			Object[] winCombo = config.getWinCombo(currentSelectCombo);

			Integer symbol = (Integer)winCombo[0];
			int bonusIndex = (Integer)winCombo[1];

			if (symbol < 0) {
				symbol = symbol * -1;
			}

			if (symbol.equals(RainbowRichesConfiguration.POT_SCATTER)) {	
				int[][] potBonus = RainbowRichesConfiguration.POT_WEIGHTS;
				int maxMulltiplier=0;
				maxMulltiplier = new Double(Math.floor(allowedWins - playerWins)/(maxPayline+1*betUnit)).intValue();
				if(maxMulltiplier<0){
					maxMulltiplier = 0;
				}
				int[][] possiblePotBonus = getPossibleBonusWeights(potBonus,betUnit * (maxPayline+1), gameID, ownerID, settings);
				if(possiblePotBonus!=null){
					//						because this ordered we get the last/max and add it to the payout.
					playerWins += possiblePotBonus[possiblePotBonus.length-1][0] * (betUnit * (maxPayline+1));
				}else{
					playerWins += potBonus[0][0] * (betUnit * (maxPayline+1));
				}
			} else if (symbol.equals(RainbowRichesConfiguration.ROAD_SCATTER)) {

				int[][] roadBonus = RainbowRichesConfiguration.ROAD_BONUS_WEIGHTS[bonusIndex - 3];
				int maxMulltiplier=0;
				maxMulltiplier = new Double(Math.floor(allowedWins - playerWins)/(maxPayline+1*betUnit)).intValue();
				int[][] possibleroadBonus = getPossiblePotBonus(roadBonus, maxMulltiplier);
				// summing the multipliers by groups of 3
				if(possibleroadBonus!=null){
					//						because this ordered we get the last/max and add it to the payout.
					playerWins += possibleroadBonus[possibleroadBonus.length-1][0] * (betUnit * (maxPayline+1));
				}else{
					playerWins += roadBonus[0][0] * (betUnit * (maxPayline+1));
				}
			}else if (symbol.equals(RainbowRichesConfiguration.WELL_SCATTER)) {

				int[][] wellBonus = RainbowRichesConfiguration.WELL_BONUS_WEIGHTS[bonusIndex - 3];
				int maxMulltiplier=0;
				maxMulltiplier = new Double(Math.floor(allowedWins - playerWins)/(maxPayline+1*betUnit)).intValue();
				int[][] possibleWellBonus = getPossiblePotBonus(wellBonus, maxMulltiplier);
				// summing the multipliers by groups of 3
				if(possibleWellBonus!=null){
					//						because this ordered we get the last/max and add it to the payout.
					playerWins += possibleWellBonus[possibleWellBonus.length-1][0] * (betUnit * (maxPayline+1));
				}else{
					playerWins += wellBonus[0][0] * (betUnit * (maxPayline+1));
				}
			}
		}
		
		log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
		
		if ((playerWins <= maxPayline+1*betUnit)){
			return reelStops;
		}
		
		if (playerWins > maxAllowedPayout){
			log.debug("watch this");
		}
		
		// if both of the conditions are true.
		if ((playerWins <= allowedWins) && (playerWins <= maxAllowedPayout)){
			return reelStops;
		}

		log.info("Rainbow Riches guard: attempt to fix odds");
		
		int minRelsetSoFar = reelSet;
		double minPayoutSoFar = playerWins;
		int[] minReelStopsSoFar = Arrays.copyOf(reelStops, reelStops.length);
		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
		int[][] weights = round.getWeights();
		
		for(int t=0;t<10;t++){
			
			int reelSetTmp = round.getWeightedRandom(weights);
			int[] reelstmp = spinReels(config.getReels(reelSetTmp));
			playerWins =round.evaluatePositionWinnings(reelSetTmp, reelstmp);

			int[][] reelsView = round.calculateReelView(config.getReels(reelSet), reelstmp);
			List<Integer> viewCombos = RainbowRichesRound.getReelViewWinCombinations(config, reelsView,reelSet);

			for (Integer viewcombo : viewCombos) {
				Integer currentSelectCombo = viewcombo;
				Object[] winCombo = config.getWinCombo(currentSelectCombo);

				Integer symbol = (Integer)winCombo[0];
				int bonusIndex = (Integer)winCombo[1];

				if (symbol < 0) {
					symbol = symbol * -1;
				}

				if (symbol.equals(RainbowRichesConfiguration.POT_SCATTER)) {	
					int[][] potBonus = RainbowRichesConfiguration.POT_WEIGHTS;
					int maxMulltiplier=0;
					maxMulltiplier = new Double(Math.floor(allowedWins - playerWins)/(maxPayline+1*betUnit)).intValue();
					if(maxMulltiplier<0){
						maxMulltiplier = 0;
					}
					int[][] possiblePotBonus = getPossibleBonusWeights(potBonus,betUnit, gameID, ownerID, settings);
					if(possiblePotBonus!=null){
						//						because this ordered we get the last/max and add it to the payout.
						playerWins += possiblePotBonus[possiblePotBonus.length-1][0] * (betUnit * (maxPayline+1));
					}else{
						playerWins += potBonus[0][0] * (betUnit * (maxPayline+1));
					}
				} else if (symbol.equals(RainbowRichesConfiguration.ROAD_SCATTER)) {

					int[][] roadBonus = RainbowRichesConfiguration.ROAD_BONUS_WEIGHTS[bonusIndex - 3];
					int maxMulltiplier=0;
					maxMulltiplier = new Double(Math.floor(allowedWins - playerWins)/(maxPayline+1*betUnit)).intValue();
					int[][] possibleroadBonus = getPossiblePotBonus(roadBonus, maxMulltiplier);
					// summing the multipliers by groups of 3
					if(possibleroadBonus!=null){
						//						because this ordered we get the last/max and add it to the payout.
						playerWins += possibleroadBonus[possibleroadBonus.length-1][0] * (betUnit * (maxPayline+1));
					}else{
						playerWins += roadBonus[0][0] * (betUnit * (maxPayline+1));
					}
				}else if (symbol.equals(RainbowRichesConfiguration.WELL_SCATTER)) {

					int[][] wellBonus = RainbowRichesConfiguration.WELL_BONUS_WEIGHTS[bonusIndex - 3];
					int[][] possibleWellBonus = getPossibleBonusWeights(wellBonus,betUnit, gameID, ownerID, settings);
					// summing the multipliers by groups of 3
					if(possibleWellBonus!=null){
						//						because this ordered we get the last/max and add it to the payout.
						playerWins += possibleWellBonus[possibleWellBonus.length-1][0] * (betUnit * (maxPayline+1));
					}else{
						playerWins += wellBonus[0][0] * (betUnit * (maxPayline+1));
					}
				}
			}
			
			if (playerWins < minPayoutSoFar){
				minRelsetSoFar = reelSetTmp;
				minPayoutSoFar = playerWins;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
			}
			
			if ((playerWins <= maxPayline+1*betUnit)){
				minRelsetSoFar = reelSetTmp;
				minPayoutSoFar = playerWins;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
				break;
			}
			
			if(minPayoutSoFar <= allowedWins && minPayoutSoFar<= maxAllowedPayout){
				break;
			}
		
		}
		log.debug("Found acceptable wins of: "+minPayoutSoFar+" VS allowed of: "+allowedWins);
		
		log.debug("FINAL REEL STOPS");
		for (int i: minReelStopsSoFar){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");
		rellsetChosen[0] = new Integer(minRelsetSoFar);
		return minReelStopsSoFar;
	}

	protected final static int[] spinReels(int[][] roundReels) {
		Random random = new Random();
		int[] reelStops = new int[roundReels.length];

		for (int i=0;i<reelStops.length;i++){
			reelStops[i] = random.nextInt(roundReels[i].length);
		}
		return reelStops;
	}

	public static int[][] getPossibleBonusWeights(int[][] potWeights,
			double totalBetAmount, Integer gameId, long owner,
			GameSettings gameSettings) {
		
		double payoutRatio = gameSettings.get("payout_ratio", Double.TYPE);
		double allowedWins = FixedOddUtil.calculateRemainingWins(owner, gameId, payoutRatio);
		int maxMulltiplier = new Double(Math.floor(allowedWins)/totalBetAmount).intValue();
		if(maxMulltiplier<0){
			maxMulltiplier = 0;
		}
		int[][] result = getPossiblePotBonus(potWeights, maxMulltiplier);
		
		// ordering the table
		java.util.Arrays.sort(potWeights, new java.util.Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});
		
		if(result==null){
			result = Arrays.copyOf(potWeights, 1);
		}
		return result;

		
	
	}

	private static int[][] getPossiblePotBonus(int[][] potWeights,
			int maxMulltiplier) {
		int[][] orderedMultipliers = potWeights.clone();

		// ordering the table
		java.util.Arrays.sort(orderedMultipliers, new java.util.Comparator<int[]>() {
			
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});

		// counting which is acceptable results.
		int counter = 0;
		for (int[] w: orderedMultipliers) {
			if(w[0]<maxMulltiplier){
				counter++;
			}else{
				break;
			}
		}
		// making sure we return only the accepted ones.		
		if(counter>0){
			return Arrays.copyOf(orderedMultipliers, counter);
		}
		return null;
	}

}
