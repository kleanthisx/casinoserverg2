/**
 * 
 */
package com.magneta.games.fixedodd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.tuttifruity.TuttiFruityRound;

/**
 * @author User
 *
 */
public class TuttiFruityGuard {
	
	private static final Logger log = LoggerFactory.getLogger(TuttiFruityGuard.class);
    private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int maxPayline, int reelStops[], GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Tutti Fruity Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Tutti Fruity Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        
        double playerWins = getWins(config, reelStops, betUnit, maxPayline);
        
        if (playerWins <= allowedWins) {
            return;
        }

        log.info("Tutti Fruity guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int changes = 0;
        int bestStopIndex = reelStops[0];
        double bestWins = playerWins;
        
        int[][] REELS = config.getReels(0);
              
        while ((playerWins >= allowedWins) && (changes < REELS[0].length)){
            changes++;
            int nextStop = (reelStops[0]+1) % REELS[0].length;
            reelStops[0] = nextStop;
            
            playerWins = getWins(config, reelStops, betUnit, maxPayline);
            
            if (playerWins < bestWins){
                bestWins = playerWins;
                bestStopIndex = nextStop;
            }
        }
        
        if (bestStopIndex != reelStops[0]){
            guardAction.setActionValue("1");
        } else {
            guardAction.setActionValue("0");
        }
        reelStops[0] = bestStopIndex;
    }
    
    public static double getWins(SlotConfiguration config, int[] reelStops, double betUnit, int lines){

        int[][] REELS = config.getReels(0);
        int[][] reelsView = new int[3][3];
        
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];
            reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
            reelsView[1][i] = REELS[i][reelStop];
            reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
        }
    
        int[] currLine = new int[REELS.length];
        double playerWins = 0.0;
        
        for (int i=0; i < currLine.length && i < lines;i++){
            config.getPayline(reelsView, i, currLine);
            
            int combo = TuttiFruityRound.getPaylineWinCombination(config, currLine);
            if (combo != -1){
                playerWins += config.getPayout(combo) * betUnit;
            }
        }

        int viewCombo = TuttiFruityRound.getReelViewWinCombination(config, reelsView);
        if (viewCombo != -1){
            playerWins += config.getPayout(viewCombo) * (betUnit * lines);
        }
        
        return playerWins;
    }
}
