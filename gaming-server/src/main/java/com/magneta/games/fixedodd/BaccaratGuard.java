package com.magneta.games.fixedodd;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.Deck;
import com.magneta.games.GameSettings;
import com.magneta.games.baccarat.BaccaratBet;
import com.magneta.games.baccarat.BaccaratHand;
import com.magneta.games.baccarat.BaccaratRound;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : When the player makes a bet it assumes
 *         that the player will win maximum bet or loose only the basic bet.
 *         Therefore if the cards are not in favor of a tie or loss, it redeals
 *         untill it does.
 */
public final class BaccaratGuard {

	private static final Logger log = LoggerFactory.getLogger(BaccaratGuard.class);
	
	/**
	 * private constructor for utility class
	 */
	private BaccaratGuard() {
	}

	/**
	 * flag about whether to log the use of guard.
	 */
	private static boolean showedMsg = false;

	/**
	 * This method is called to apply a guard filter on a game. This
	 * @param integer 
	 * @param ownerId 
	 * @param gameSettings 
	 * 
	 * @param params a parameters object, 
	 * due to the big number of parameters otherwise.
	 */
	public static void guardHand(BaccaratHand[] hands, List<BaccaratBet> bets, long ownerId, Integer gameId, GameSettings gameSettings) {
		boolean guard = gameSettings.get("guard", Boolean.TYPE);

		if (!guard) {
			return;
		}
		
		double payoutRatio = gameSettings.get("payout_ratio", Double.TYPE);

		log.debug("Baccarat called with settings: guard = " + guard
				+ ", payout_ratio = " + payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Baccarat Guard enabled");
		}

		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerId, gameId, payoutRatio);
		double roundWins = 0.0;
		
		//		checking whether the cards dealt are acceptable.
		for (BaccaratBet bet: bets) {
			if (bet.hasWon(hands)) {
				roundWins = roundWins + ((bet.getWinMultiplier()-1) * bet.getBetAmount());
			}
		}

		log.debug("original payout {} cards {}", roundWins, hands[0].toString() + hands[1].toString());
		if(roundWins<=allowedWins){
			return;
		}

		log.info("Baccarat guard activated");
		for(int i=0;i<1;i++){
			
			Deck deck = new Deck(gameSettings.getInt("decks"), 0);
			deck.shuffleCards(new Random());
			BaccaratHand[] tmpHands = new BaccaratHand[] {
					new BaccaratHand(), /* Banker */
					new BaccaratHand()  /* Player */
			};
			
			for (BaccaratHand hand: tmpHands) {
	        	hand.addCard(deck.getNextCard());
	        	hand.addCard(deck.getNextCard());
	        }
	        
	        boolean shouldStand = false; 
	        
	        /* Check for 8 or 9 */
	        for (BaccaratHand hand: tmpHands) {
	        	int val = hand.getValue();
	        	
	        	if (val == 8 || val == 9) {
	        		shouldStand = true;
	        		break;
	        	}
	        }
	        
	        if (!shouldStand) {
	        	if (tmpHands[1].getValue() <= 5) {
	        		tmpHands[1].addCard(deck.getNextCard());
	        		
	        		if (BaccaratRound.dealerShouldHit(tmpHands)) {
	        			tmpHands[0].addCard(deck.getNextCard());
	        		}
	        		
	        	} else {
	        		if (tmpHands[0].getValue() <= 5) {
	        			tmpHands[0].addCard(deck.getNextCard());
	        		}
	        	}
	        }
	        
	        double guardedRoundWins = 0.0;
			//		checking whether the cards dealt are acceptable.

			for (BaccaratBet bet: bets) {
				if (bet.hasWon(tmpHands)) {
					guardedRoundWins = guardedRoundWins + ((bet.getWinMultiplier()-1) * bet.getBetAmount());
				}
			}

			
			if(guardedRoundWins<=allowedWins){
				hands[0] = tmpHands[0];
				hands[1] = tmpHands[1];
				return;
			}else if(guardedRoundWins<roundWins){
				roundWins = new Double(guardedRoundWins);
				hands[0] = tmpHands[0];
				hands[1] = tmpHands[1];
			}
	        
		}// end for

		return;

	}

}