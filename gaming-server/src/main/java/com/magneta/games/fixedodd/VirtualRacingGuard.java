package com.magneta.games.fixedodd;

import static com.magneta.games.virtualracing.VirtualRacingRound.calculateRace;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.virtualracing.BetDescription;
import com.magneta.games.virtualracing.Runner;

public class VirtualRacingGuard {

	private static final Logger log = LoggerFactory.getLogger(VirtualRacingGuard.class);
	
    private static boolean showedMsg = false;
    private static final int WIN_BET_TYPE = 0;
    private static final int QUINELLA_BET_TYPE = 1;
    private static final int EXACTA_BET_TYPE = 2;
    private static final int REGENERATED_RACES_MAX = 10;

    public static void guardRound(int gameID, long ownerID, Runner[] entities, int[] raceResults, double[] bets, BetDescription[] betsOdds, Random random, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Virtual Racing Guard enabled");
        }

        if (!guard) {
            return;
        }

        double totalBet = 0.0;
        
        for (int i=0; i < bets.length; i++) {
            totalBet += bets[i];
        }
        
        /* Don't do anything if the player has a net lost in the race */
        if (calculateWins(entities.length, raceResults, bets, betsOdds) <= totalBet){
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);

        if (calculateWins(entities.length, raceResults, bets, betsOdds) <= allowedWins){
            return;
        }

        log.info("Virtual Racing Guard: Attempting to fix odds...");

        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int[] raceResultsTmp = new int[raceResults.length]; 
        System.arraycopy(raceResults, 0, raceResultsTmp, 0, raceResults.length);
        
        for (int i =0;i<REGENERATED_RACES_MAX;i++){
            calculateRace(random,entities,raceResultsTmp);
            if (calculateWins(entities.length, raceResultsTmp, bets, betsOdds) <= allowedWins){
                System.arraycopy(raceResultsTmp, 0, raceResults, 0, raceResults.length);
                log.info("Virtual Racing Guard: After "+(i+1)+" race re-generations found acceptable result.");
                guardAction.setActionValue("1");
                return;
            }
        }
        
        System.arraycopy(raceResults, 0, raceResultsTmp, 0, raceResults.length);
        int half = entities.length / 2;
        
        for (int i=0;i<half;i++){
            for (int j=0;j<raceResultsTmp.length;j++){
                int tmp = raceResultsTmp[i];
                raceResultsTmp[i] = raceResultsTmp[j];
                raceResultsTmp[j] = tmp;

                if (calculateWins(entities.length, raceResultsTmp, bets, betsOdds) <= allowedWins){
                    System.arraycopy(raceResultsTmp, 0, raceResults, 0, raceResults.length);
                    guardAction.setActionValue("1");
                    return;
                }
            }
        }
        
        guardAction.setActionValue("0");
    }

    private static double calculateWins(int horses, int[] raceResults, double[] bets, BetDescription[] betsOdds){
        double wins = 0.0;

        for (int j = 0; j< betsOdds.length;j++){
            switch(betsOdds[j].getBetType()){
                case WIN_BET_TYPE:
                    if ((bets[j] > 0.0) && (raceResults[0] == betsOdds[j].getBetEntities()[0])){
                        wins += betsOdds[j].getBetOdds() * bets[j];
                    }
                    break;

                case QUINELLA_BET_TYPE:
                    if ((bets[j] > 0.0) && ((raceResults[0] == betsOdds[j].getBetEntities()[0] && raceResults[1] == betsOdds[j].getBetEntities()[1]) ||
                            (raceResults[0] == betsOdds[j].getBetEntities()[1] && raceResults[1] == betsOdds[j].getBetEntities()[0]))){
                        wins += betsOdds[j].getBetOdds() * bets[j];
                    }
                    break;
                case EXACTA_BET_TYPE:
                    if ((bets[j] > 0.0) && ((raceResults[0] == betsOdds[j].getBetEntities()[0] && raceResults[1] == betsOdds[j].getBetEntities()[1]))){
                        wins += betsOdds[j].getBetOdds() * bets[j];
                    }
                    break;
            }
        }

        return wins;
    }
}
