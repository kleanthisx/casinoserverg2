/**
 * 
 */
package com.magneta.games.fixedodd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.stopfortime.StopForTimeRound;

/**
 * @author User
 *
 */
public class StopForTimeGuard {

	private static final Logger log = LoggerFactory.getLogger(StopForTimeGuard.class);
	private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int[] reelStops, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Stop For Time Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Stop For Time Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        log.debug("ORIGINAL REEL STOPS");
        for (int i: reelStops){
            log.debug("Reel stop: "+i);
        }
        log.debug("*******************");
        
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        log.debug("Player allowed wins: "+allowedWins);
        if (allowedWins < 0.0) {
            allowedWins = 0.0;
        }
        
        double playerWins = getWins(config, reelStops, betUnit);
        
        log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
        
        if ((playerWins <= allowedWins)/* || (multiplier <= 10.0)*/){
            return;
        }
        
        log.info("Stop For Time guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int[][] REELS = config.getReels(0);
        
        for (int changes = 0; changes < REELS[2].length; changes++) {
            reelStops[2]++;
            if (reelStops[2] >= REELS[2].length) {
                reelStops[2] = 0;
            }

            playerWins = getWins(config, reelStops, betUnit);

            if (playerWins <= allowedWins) {
                guardAction.setActionValue("1");
                return;
            }

        }
        guardAction.setActionValue("0");
    }
    
    public static double getWins(SlotConfiguration config, int[] reelStops, double betUnit){
        int[][] REELS = config.getReels(0);
        //get the reels view
        int[][] reelsView = new int[3][5];
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];

            reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
            reelsView[1][i] = REELS[i][reelStop];
            reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
        }

        //FOR DEBUGGING REASONS
        if (log.isDebugEnabled()){
            for (int[] line: reelsView){
                String reel = "REEL: ";
                for (int fruit: line){
                    reel += fruit+" ";
                }
                log.debug(reel);
            }
        }

        int[] currLine = new int[5];
        double playerWins = 0.0;
        
        //check lines
        for (int i=0;i<config.getPaylines().length;i++) {
            config.getPayline(reelsView, i, currLine);

            int combo = StopForTimeRound.getPaylineWinCombination(currLine);
            if (combo != -1){
                double multiplier = config.getPayout(combo);
                playerWins += multiplier * betUnit;
            }
        }

        return playerWins;
    }    
}
