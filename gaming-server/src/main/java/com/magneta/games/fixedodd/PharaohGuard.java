package com.magneta.games.fixedodd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.pharaoh.PharaohRound;

public class PharaohGuard {

	private static final Logger log = LoggerFactory.getLogger(PharaohGuard.class);
	
    private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, boolean freeSpin, SlotConfiguration config, int wildSymbol, double freeSpinMultiplier, double betUnit, int maxPayline, int reelStops[], GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Pharaoh Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Pharaoh Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        double allowedMultiplier = 0.0;
        
        if (allowedWins > 0.0){
            allowedMultiplier = allowedWins / betUnit;
        }
        
        int[][] REELS = config.getReels(0);
        int[][] reelsView = new int[3][REELS.length];

        for (int i=0;i<REELS.length;i++){
            int reelStop = reelStops[i];
            reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
            reelsView[1][i] = REELS[i][reelStop];
            reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
        }
        
        int[] currLine = new int[REELS.length];
        double multiplier = 0.0;
        
        for (int i=0;i<config.getPaylines().length;i++){
            config.getPayline(reelsView, i, currLine);

            int combo = PharaohRound.getPaylineWinCombination(config, currLine);
            if (combo != -1){
                multiplier += config.getPayout(combo) * (freeSpin ? freeSpinMultiplier : 1.0);
            }
        }

        int viewCombo = PharaohRound.getReelViewWinCombination(config, reelsView);
        if (viewCombo != -1){
            multiplier += config.getPayout(viewCombo) * (freeSpin ? freeSpinMultiplier : 1.0);
        }
            
        if ((multiplier <= allowedMultiplier) || (multiplier <= 1.0)){
            return;
        }
        
        log.info("Pharaoh guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int changes = 0;
        double bestMultiplier = multiplier;
        int bestStopIndex = reelStops[2];
        
        while ((multiplier > allowedMultiplier) && (changes < REELS[0].length)){
            changes++;
            int nextStop = (reelStops[2]+1) % REELS[0].length;
            reelStops[2] = nextStop;
            
            for (int i=0;i<REELS.length;i++){
                int reelStop = reelStops[i];
                reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
                reelsView[1][i] = REELS[i][reelStop];
                reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
            }
            
            multiplier = 0.0;
            
            for (int i=0;i<config.getPaylines().length;i++){
                config.getPayline(reelsView, i, currLine);

                int combo = PharaohRound.getPaylineWinCombination(config, currLine);
                if (combo != -1){
                    multiplier += config.getPayout(combo) * (freeSpin ? freeSpinMultiplier : 1.0);
                }
            }

            viewCombo = PharaohRound.getReelViewWinCombination(config, reelsView);
            if (viewCombo != -1){
                multiplier += config.getPayout(viewCombo) * (freeSpin ? freeSpinMultiplier : 1.0);
            }
            
            if (multiplier < bestMultiplier){
                bestMultiplier = multiplier;
                bestStopIndex = nextStop;
            }
        }
        
        if ((changes > 0) && (changes < REELS[0].length)){
            guardAction.setActionValue("1");
        } else {
            guardAction.setActionValue("0");
        }
        
        reelStops[2] = bestStopIndex;
    }
    
    public static void guardBonus(int gameID, long ownerID, double[] bonusBoxes, int area, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Pharaoh Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Pharaoh Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        
        if (allowedWins > 0.0){
            allowedWins = 0.0;
        }
        
        double winAmount = bonusBoxes[area];
        
        if (winAmount <= allowedWins){
            return;
        }
        
        log.info("Pharaoh guard: attempt to fix bonus odds");
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        for (int i=0;i<bonusBoxes.length;i++){
            if ((bonusBoxes[i] >= 0.0) && (bonusBoxes[i] <= allowedWins)){
                double amount = bonusBoxes[area];
                bonusBoxes[area] = bonusBoxes[i];
                bonusBoxes[i] = amount;
                guardAction.setActionValue("1");
                return;
            }
        }
        
        guardAction.setActionValue("0");
    }
}
