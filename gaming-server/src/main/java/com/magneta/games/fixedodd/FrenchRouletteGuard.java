/**
 * 
 */
package com.magneta.games.fixedodd;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.frenchroulette.FrenchRouletteBet;

/**
 * @author User
 *
 */

public class FrenchRouletteGuard {
	
	private static final Logger log = LoggerFactory.getLogger(FrenchRouletteGuard.class);
	
    private static boolean showedMsg = false;

    public static int guardRound(int gameID, long ownerID, int number, final List<FrenchRouletteBet> bets, Random random, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);

        log.debug("French Roulette Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("French Roulette Guard enabled");
        }

        if (!guard) {
            return number;
        }

        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        if (allowedWins < 0.0){
            allowedWins = 0.0;
        }

        double totalBet = 0.0;
        for (FrenchRouletteBet bet: bets){
            totalBet += bet.getBetAmount();
        }

        int testNum = number;
        double wins = 0.0;

        for (FrenchRouletteBet bet: bets){
            for (int num: bet.getNumbers()){
                if (num == testNum){
                    wins += bet.getWinAmount();
                    break;
                }
            }
        }

        if ((wins <= totalBet) || ((wins - totalBet) <= allowedWins)){
            return number;
        }

        log.info("French Roulette Guard: attempt to fix odds...");

        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

        for (int i=0; i < 100; i++) {
            wins = 0.0;
            testNum = random.nextInt(37);

            for (FrenchRouletteBet bet: bets){
                for (int num: bet.getNumbers()){
                    if (num == testNum){
                        wins += bet.getWinAmount();
                        break;
                    }
                }
            }

            if ((wins <= totalBet) || ((wins - totalBet) <= allowedWins)){
                guardAction.setActionValue("1");
                return testNum;
            }
        }

        guardAction.setActionValue("0");
        return number;
    }
}

