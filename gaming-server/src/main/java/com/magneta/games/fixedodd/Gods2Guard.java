/**
 * 
 */
package com.magneta.games.fixedodd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.gods2.Gods2Round;

public class Gods2Guard {

	private static final Logger log = LoggerFactory.getLogger(Gods2Guard.class);
	
    private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int reelStops[], GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Oh my gods II Guard called with settings: guard = {}, payout_ratio = {}", guard, payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Oh my gods II Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        log.debug("ORIGINAL REEL STOPS");
        for (int i: reelStops){
            log.debug("Reel stop: {}", i);
        }
        log.debug("*******************");
        
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        log.debug("Player allowed wins: {}", allowedWins);
        if (allowedWins < 0.0) {
            allowedWins = 0.0;
        }
        
        double playerWins = getWins(config, reelStops, betUnit);
        
        log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
        
        if ((playerWins <= allowedWins)/* || (multiplier <= 10.0)*/){
            return;
        }
        
        log.info("Oh my gods II guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        int[][] REELS = config.getReels(0);
        
        for (int changes2 = 0; changes2 < REELS[2].length; changes2++) {

            reelStops[2]++;
            if (reelStops[2] >= REELS[2].length) {
                reelStops[2] = 0;
            }

            playerWins = getWins(config, reelStops, betUnit);

            if (playerWins <= allowedWins) {
                log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
                guardAction.setActionValue("1");
                log.debug("FINAL REEL STOPS");
                for (int i: reelStops){
                    log.debug("Reel stop: {}", i);
                }
                log.debug("*******************");
                return;
            }
        }
        
        guardAction.setActionValue("0");
    }
    
    public static void guardDouble(int gameID, Deck deck, long ownerID, double currWins, int color, int race, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Oh my gods II Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Oh my gods II Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        
        if (allowedWins >= (currWins * (race >= 0 ? 4.0 : 2.0))){
            return;
        }
        
        log.info("Oh my gods II guard: attempt to fix double odds");
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int card = deck.getNextCard();
        while(deck.getCardsRemaining() > 1 && (race >= 0 ? (Card.getCardRace(card) == race) : (Card.getCardColour(card) == color))){
            card = deck.getNextCard();
        }
        
        if ((race >= 0 && (Card.getCardRace(card) != race)) || (Card.getCardColour(card) != color)){
            deck.returnCard(card);
            guardAction.setActionValue("1");
        } else {
            deck.reset();
            guardAction.setActionValue("0");
        }
    }
    
    public static double getWins(SlotConfiguration config, int[] reelStops, double betUnit){
      //get the view
        int[][] reelsView = new int[3][6];
        int[][] REELS = config.getReels(0);
        
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];
            
            reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
            reelsView[1][i] = REELS[i][reelStop];
            reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
        }

        int[] currLine = new int[5];
        double playerWins = 0.0;
        
        //check lines
        for (int i=0;i<config.getPaylines().length;i++){
            
            config.getPayline(reelsView, i, currLine);
            int combo = Gods2Round.getPaylineWinCombination(currLine);
            if (combo != -1){
                playerWins += config.getPayout(combo) * betUnit;
            }
        }
        
        return playerWins;
    }
}
