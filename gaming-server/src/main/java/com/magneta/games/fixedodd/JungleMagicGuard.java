/**
 * 
 */
package com.magneta.games.fixedodd;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.junglemagic.JungleMagicRound;


public class JungleMagicGuard {

	private static final Logger log = LoggerFactory.getLogger(JungleMagicGuard.class);
	
    private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int reelStops[], GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Jungle Magic Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Jungle Magic Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        log.debug("Player allowed wins: {}", allowedWins);
        if (allowedWins < 0.0) {
            allowedWins = 0.0;
        }
        
        int[][] reels = config.getReels(0);
        
        double playerWins = getWins(reelStops, config, betUnit);
        
        log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
        
        if ((playerWins <= allowedWins)/* || (multiplier <= 10.0)*/){
            return;
        }
        
        log.info("Jungle Magic guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

        for (int changes2 = 0; changes2 < reels[2].length; changes2++) {

            reelStops[2]++;
            if (reelStops[2] >= reels[2].length) {
                reelStops[2] = 0;
            }

            playerWins = getWins(reelStops, config, betUnit);

            if (playerWins <= allowedWins) {
                log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
                guardAction.setActionValue("1");
                log.debug("FINAL REEL STOPS");
                for (int i: reelStops){
                    log.debug("Reel stop: "+i);
                }
                log.debug("*******************");
                return;
            }
        }
        
        guardAction.setActionValue("0");
    }
    
    public static void guardDouble(int gameID, Deck deck, long ownerID, double currWins, int color, int race, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Jungle Magic Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Win 7 Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        
        if (allowedWins >= (currWins * (race >= 0 ? 4.0 : 2.0))){
            return;
        }
        
        log.info("Jungle Magic guard: attempt to fix double odds");
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int card = deck.getNextCard();
        while(deck.getCardsRemaining() > 1 && (race >= 0 ? (Card.getCardRace(card) == race) : (Card.getCardColour(card) == color))){
            card = deck.getNextCard();
        }
        
        if ((race >= 0 && (Card.getCardRace(card) != race)) || (Card.getCardColour(card) != color)){
            deck.returnCard(card);
            guardAction.setActionValue("1");
        } else {
            deck.reset();
            guardAction.setActionValue("0");
        }
    }
    
    public static double getWins(int[] reelStops, SlotConfiguration config, double betUnit){
        //get the LEFT side view
        int[][]reels = config.getReels(0);
        
        int[][] reelsView = new int[3][config.getPaylines().length];
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];

            reelsView[0][i] = reels[i][(reelStop > 0 ? (reelStop - 1) : (reels[i].length-1))];
            reelsView[1][i] = reels[i][reelStop];
            reelsView[2][i] = reels[i][(reelStop+1) % reels[i].length];
        }
        
        int[] currLine = new int[5];
        double playerWins = 0.0;
        
        for (int i=0;i<currLine.length;i++){
            //check line from the view
            config.getPayline(reelsView, i, currLine);
            
            int combo = JungleMagicRound.getPaylineWinCombination(currLine);
            if (combo != -1){
                 playerWins += config.getPayout(combo) * betUnit;
            }
        }
        
        return playerWins;
    }
}

