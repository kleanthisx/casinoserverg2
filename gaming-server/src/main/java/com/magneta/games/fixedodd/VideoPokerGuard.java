/**
 * 
 */
package com.magneta.games.fixedodd;

import static com.magneta.games.poker.PokerUtil.getWinningCombination;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.DeckOutOfCardsException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.poker.PokerConfiguration;
import com.magneta.games.poker.PokerUtil;

/**
 * @author Nicos
 *
 */
public class VideoPokerGuard {

	private static final Logger log = LoggerFactory.getLogger(VideoPokerGuard.class);
	
    private static boolean showedMsg = false;
    
    public static void guardFirstHand(int gameID, Deck deck, long ownerID, PokerConfiguration config, double betAmount, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Video Poker Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Video Poker Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        List<Integer> cardsTaken = new ArrayList<Integer>();
        int cardsCount = deck.getCardsCount();
        
        for (int i=0;i<5;i++){
            int nextCard = deck.getNextCard();
            cardsTaken.add(nextCard);
            cardsCount--;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        
        double allowedMultiplier = 0.0;
        
        if (allowedWins > 0.0){
            allowedMultiplier = allowedWins / betAmount;
        }
        
        Integer[] testHand = new Integer[5];
        boolean[] dummy = new boolean[5];
        for (int i=0;i<cardsTaken.size();i++){
            testHand[i] = cardsTaken.get(i);
        }

        int combo = getWinningCombination(testHand, dummy, Card.JACK);
        int bestCombo = combo;
        int bestComboIndex = 0;
                
        if ((config.getMultiplier(combo) <= allowedMultiplier) || (config.getMultiplier(combo) <= 1.0)){
            for (int i=cardsTaken.size()-1;i>=0;i--){
                deck.returnCard(cardsTaken.get(i));
            }
            return;
        }
        
        log.info("Video poker guard: attempt to fix first hand odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        while (cardsCount > 5) {
        	if (config.getMultiplier(combo) <= allowedMultiplier) {
        		for (int i=(cardsTaken.size()-1);i>=(cardsTaken.size()-5);i--){
        			deck.returnCard(cardsTaken.get(i));
        		}
        		guardAction.setActionValue("1");
        		return;
        	}
        	cardsTaken.add(deck.getNextCard());
        	cardsCount--;

        	int tmp = 4;
        	for (int i=cardsTaken.size()-1;i>=cardsTaken.size()-5;i--){
        		testHand[tmp] = cardsTaken.get(i);
        		tmp--;
        	}

        	combo = getWinningCombination(testHand, dummy, Card.JACK);

        	if (combo < bestCombo){
        		bestCombo = combo;
        		bestComboIndex = cardsTaken.size() - 5;
        	}
        }
        
        if (bestComboIndex > 0){
            guardAction.setActionValue("1");
        } else {
            guardAction.setActionValue("0");
        }
        
        for (int i=cardsTaken.size()-1;i>=bestComboIndex;i--){
            deck.returnCard(cardsTaken.get(i));
        }
    }
    
    public static void guardSecondHand(int gameID, Deck deck, long ownerID, final double multipliers[], double betAmount, Integer[] currHand, boolean[] holdCard, GameSettings settings, GameAction parentAction){
    	boolean guard = settings.get("guard", Boolean.TYPE);
    	double payoutRatio = settings.get("payout_ratio", Double.TYPE);

    	log.debug("Video Poker Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

    	if (!showedMsg && guard) {
    		showedMsg = true;
    		log.info("Video Poker Guard enabled");
    	}

    	if (!guard) {
    		return;
    	}

    	int holdCardsCount = 0;
    	for (int i=0;i<holdCard.length;i++){
    		if (holdCard[i]){
    			holdCardsCount++;
    		}
    	}

    	if (holdCardsCount == holdCard.length){
    		return;
    	}

    	Integer[] holdCards = new Integer[holdCardsCount];
    	int tmp=0;
    	for (int i=0;i<holdCard.length;i++){
    		if (holdCard[i]){
    			holdCards[tmp] = currHand[i];
    			tmp++;
    		}
    	}

    	double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
    	double allowedMultiplier = 0.0;

    	if (allowedWins > 0.0){
    		allowedMultiplier = allowedWins / betAmount;
    	}

    	List<Integer> cardsTaken = new ArrayList<Integer>();
    	Integer[] testHand = new Integer[5];
    	boolean dummy[] = new boolean[5];
    	System.arraycopy(holdCards, 0, testHand, 0, holdCards.length);

    	for (int i=holdCardsCount;i<testHand.length;i++){
    		int nextCard = deck.getNextCard();
    		cardsTaken.add(nextCard);
    		testHand[i] = nextCard;
    	}

    	int combo = getWinningCombination(testHand, dummy, Card.JACK);
    	int bestCombo = combo;
    	int bestComboIndex = 0;

    	if ((multipliers[combo] <= allowedMultiplier) || (multipliers[combo] <= 1.0)){
    		for (int i=cardsTaken.size()-1;i>=0;i--){
    			deck.returnCard(cardsTaken.get(i));
    		}
    		return;
    	}

    	log.info("Video poker guard: attempt to fix second hand odds");

    	GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

    	while (true){
    		if (multipliers[combo] <= allowedMultiplier){
    			for (int i=(cardsTaken.size()-1);i>=(cardsTaken.size()-(5-holdCardsCount));i--){
    				deck.returnCard(cardsTaken.get(i));
    			}
    			guardAction.setActionValue("1");
    			return;
    		} 
    		try{
    			cardsTaken.add(deck.getNextCard());

    			tmp = 4;
    			for (int i=cardsTaken.size()-1;i>=(cardsTaken.size()-(5-holdCardsCount));i--){
    				testHand[tmp] = cardsTaken.get(i);
    				tmp--;
    			}

    			combo = getWinningCombination(testHand, dummy, Card.JACK);

    			if (combo < bestCombo){
    				bestCombo = combo;
    				bestComboIndex = cardsTaken.size() - (5-holdCardsCount);
    			}
    		} catch (DeckOutOfCardsException e){
    			for (int i=cardsTaken.size()-1;i>=bestComboIndex;i--){
    				deck.returnCard(cardsTaken.get(i));
    			}

    			if (bestComboIndex > 0){
    				guardAction.setActionValue("1");
    			} else {
    				guardAction.setActionValue("0");
    			}
    			return;
    		}

    	}
    }
    
    public static void guardDouble(int gameID, Deck deck, long ownerID, double betAmount, double currMulti, int doubleCard, int area, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Video Poker Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Video Poker Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        double allowedMultiplier = 0.0;
        
        if (allowedWins > 0.0){
            allowedMultiplier = allowedWins / betAmount;
        }
        
        if ((currMulti * 2.0) <= allowedMultiplier){
            return;
        }
        
        log.info("Video poker guard: attempt to fix double odds...");
        
        List<Integer> cardsTaken = new ArrayList<Integer>();
        
        for (int i=0;i<4;i++){
            cardsTaken.add(deck.getNextCard());
        }
        
        log.debug("ORIGINAL CARD SELECTED: {}", cardsTaken.get(area-1));
        
        int selectedCardVal = PokerUtil.getCardValue(cardsTaken.get(area-1));
        int doubleCardVal = PokerUtil.getCardValue(doubleCard);
        int allowedCards = deck.getCardsCount()-1;
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        while ((selectedCardVal >= doubleCardVal) && (cardsTaken.size() < allowedCards)){
            if ((selectedCardVal == doubleCardVal) && (currMulti <= allowedMultiplier)){
                guardAction.setActionValue("1");
                for (int i=cardsTaken.size()-1;i>=cardsTaken.size()-4;i--){
                    deck.returnCard(cardsTaken.get(i));
                }
                return;
            }
            
            try{
                cardsTaken.add(deck.getNextCard());
                selectedCardVal = PokerUtil.getCardValue(cardsTaken.get(area-1));
            } catch (DeckOutOfCardsException e){
                log.warn("Out of cards while guarding round double.",e);
                for (int i=cardsTaken.size()-1;i>=0;i--){
                    deck.returnCard(cardsTaken.get(i));
                }
                guardAction.setActionValue("0");
                return;
            }
        }
        
        if (selectedCardVal >= doubleCardVal){
            guardAction.setActionValue("1");
            for (int i=cardsTaken.size()-1;i>=cardsTaken.size()-4;i--){
                deck.returnCard(cardsTaken.get(i));
            }
        } else {
            guardAction.setActionValue("0");
            for (int i=cardsTaken.size()-1;i>=0;i--){
                deck.returnCard(cardsTaken.get(i));
            }
        }
    }
}
