/**
 * 
 */
package com.magneta.games.fixedodd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

/**
 * @author Nicos
 *
 */
public class FixedOddUtil {

	private static final Logger log = LoggerFactory.getLogger(FixedOddUtil.class);
	
    /**
     * Calculates the remaining allowed wins for the user.
     * 
     * @param user_id The user_id for the user
     * @param game_id The game_id for the game.
     * 
     * @return
     */
    static double calculateRemainingWins(long userId, int gameId, double payoutRatio) {
        
        double in_balance = 0.0;
        double out_balance = 0.0;
        
        String sql = 
            "SELECT SUM(real_wins) AS out_balance, SUM(real_bets) AS in_balance"+
            " FROM game_period_amounts"+
            " INNER JOIN game_tables ON game_period_amounts.table_id = game_tables.table_id"+
            " AND game_tables.game_type_id = ?"+
            " WHERE game_period_amounts.user_id = ?"+
            " AND age(period_date AT TIME ZONE 'UTC') < INTERVAL '2 months'";
        
        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setLong(1, gameId);
            stmt.setLong(2, userId);
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                in_balance = rs.getDouble("in_balance");
                out_balance = rs.getDouble("out_balance");
            }
            
        } catch (SQLException e) {
            log.warn("Error while getting user balance", e);
        } finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
        
        
        double allowedWins = in_balance * payoutRatio;
        allowedWins -= out_balance;
        return allowedWins;
    }
}
