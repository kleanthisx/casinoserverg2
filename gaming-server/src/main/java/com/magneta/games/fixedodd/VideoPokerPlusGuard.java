/**
 * 
 */
package com.magneta.games.fixedodd;

import static com.magneta.games.configuration.poker.PokerConstants.ROYAL_FLUSH;
import static com.magneta.games.poker.PokerUtil.getWinningCombination;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.DeckOutOfCardsException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.poker.PokerConfiguration;
import com.magneta.games.poker.PokerUtil;
import com.magneta.games.poker.videopoker.VideoPokerPlusRound;
/**
 * @author Nicos
 *
 */
public class VideoPokerPlusGuard {

	private static final Logger log = LoggerFactory.getLogger(VideoPokerPlusGuard.class);
	private static boolean showedMsg = false;

	public static void guardFirstHand(int gameID, Deck deck, long ownerID, PokerConfiguration config, double betAmount, GameSettings settings, GameAction parentAction){
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);

		log.debug("Video Poker Plus Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Video Poker Plus Guard enabled");
		}

		if (!guard) {
			return;
		}

		List<Integer> cardsTaken = new ArrayList<Integer>();
		int cardsCount = deck.getCardsCount();

		for (int i=0;i<5;i++){
			int nextCard = deck.getNextCard();
			cardsTaken.add(nextCard);
			cardsCount--;
		}

		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);

		double allowedMultiplier = 0.0;

		if (allowedWins > 0.0){
			allowedMultiplier = allowedWins / betAmount;
		}

		Integer[] testHand = new Integer[5];
		for (int i=0;i<cardsTaken.size();i++){
			testHand[i] = cardsTaken.get(i);
		}
		boolean[] dummy = new boolean[testHand.length];

		int combo = getWinningCombination(testHand, dummy, Card.JACK);
		int bestCombo = combo;
		int bestComboIndex = 0;

		if ((config.getMultiplier(combo) <= allowedMultiplier) || (config.getMultiplier(combo) <= 1.0)){
			for (int i=cardsTaken.size()-1;i>=0;i--){
				deck.returnCard(cardsTaken.get(i));
			}
			return;
		}

		log.info("Video poker guard: attempt to fix first hand odds");

		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

		while (cardsCount > 5) {
			if (config.getMultiplier(combo) <= allowedMultiplier){
				for (int i=(cardsTaken.size()-1);i>=(cardsTaken.size()-5);i--){
					deck.returnCard(cardsTaken.get(i));
				}

				guardAction.setActionValue("1");

				return;
			}

			cardsTaken.add(deck.getNextCard());
			cardsCount--;

			int tmp = 4;
			for (int i=cardsTaken.size()-1;i>=cardsTaken.size()-5;i--){
				testHand[tmp] = cardsTaken.get(i);
				tmp--;
			}

			combo = getWinningCombination(testHand, dummy, Card.JACK);

			if (combo < bestCombo){
				bestCombo = combo;
				bestComboIndex = cardsTaken.size() - 5;
			}
		}

		if (bestComboIndex != 0){
			guardAction.setActionValue("1");
		} else {
			guardAction.setActionValue("0");
		}

		for (int i=cardsTaken.size()-1;i>=bestComboIndex;i--){
			deck.returnCard(cardsTaken.get(i));
		}
	}

	public static void guardSecondHand(int gameID, Deck deck, long ownerID, PokerConfiguration config, double betAmount, Integer[] currHand, boolean holdCard[], GameSettings settings, GameAction parentAction){
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);

		log.debug("Video Poker Plus Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Video Poker Plus Guard enabled");
		}

		if (!guard) {
			return;
		}

		int holdCardsCount = 0;
		for (int i=0;i<holdCard.length;i++){
			if (holdCard[i]){
				holdCardsCount++;
			}
		}

		if (holdCardsCount == holdCard.length){
			return;
		}

		Integer[] holdCards = new Integer[holdCardsCount];
		int tmp=0;
		for (int i=0;i<holdCard.length;i++){
			if (holdCard[i]){
				holdCards[tmp] = currHand[i];
				tmp++;
			}
		}

		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		double allowedMultiplier = 0.0;

		if (allowedWins > 0.0){
			allowedMultiplier = allowedWins / betAmount;
		}

		List<Integer> cardsTaken = new ArrayList<Integer>();
		Integer[] testHand = new Integer[5];
		System.arraycopy(holdCards, 0, testHand, 0, holdCards.length);
		boolean dummy[] = new boolean[testHand.length];

		for (int i=holdCardsCount;i<testHand.length;i++){
			int nextCard = deck.getNextCard();
			cardsTaken.add(nextCard);
			testHand[i] = nextCard;
		}

		int combo = getWinningCombination(testHand, dummy, Card.JACK);
		int bestCombo = combo;
		int bestComboIndex = 0;

		if ((config.getMultiplier(combo) <= allowedMultiplier) || (config.getMultiplier(combo) <= 1.0)){
			for (int i=cardsTaken.size()-1;i>=0;i--){
				deck.returnCard(cardsTaken.get(i));
			}
			return;
		}

		log.info("Video poker guard: attempt to fix second hand odds");

		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

		while (true){
			if (config.getMultiplier(combo) <= allowedMultiplier){
				for (int i=(cardsTaken.size()-1);i>=(cardsTaken.size()-(5-holdCardsCount));i--){
					deck.returnCard(cardsTaken.get(i));
				}

				guardAction.setActionValue("1");
				return;
			} 
			try{
				cardsTaken.add(deck.getNextCard());

				tmp = 4;
				for (int i=cardsTaken.size()-1;i>=(cardsTaken.size()-(5-holdCardsCount));i--){
					testHand[tmp] = cardsTaken.get(i);
					tmp--;
				}

				combo = getWinningCombination(testHand, dummy, Card.JACK);

				if (combo < bestCombo){
					bestCombo = combo;
					bestComboIndex = cardsTaken.size() - (5-holdCardsCount);
				}
			} catch (DeckOutOfCardsException e){
				for (int i=cardsTaken.size()-1;i>=bestComboIndex;i--){
					deck.returnCard(cardsTaken.get(i));
				}

				if (bestComboIndex != 0){
					guardAction.setActionValue("1");
				} else {
					guardAction.setActionValue("0");
				}

				return;
			}
		}
	}

	public static void guardDoubleStart(int gameID, Deck deck, long ownerID, double betAmount, double currMulti, int doubleCardsLen, GameSettings settings, GameAction parentAction){
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);

		log.debug("Video Poker Plus Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Video Poker Plus Guard enabled");
		}

		if (!guard) {
			return;
		}

		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		double allowedMultiplier = 0.0;

		if (allowedWins > 0.0){
			allowedMultiplier = allowedWins / betAmount;
		}

		int cardsAllowed = deck.getCardsRemaining() - doubleCardsLen;

		double maxPossible = currMulti * 2.0;
		if (maxPossible <= allowedMultiplier){
			return;
		} else if (cardsAllowed <= 0){
			return;
		}

		List<Integer> cardsTaken = new ArrayList<Integer>();
		int selectedCard = deck.getNextCard();
		cardsTaken.add(selectedCard);
		int selectedCardVal = PokerUtil.getCardValue(selectedCard);

		if ((selectedCardVal >= 6) && (selectedCardVal <= 9)){
			deck.returnCard(selectedCard);
			return;
		}

		log.debug("ORIGINAL CARD SELECTED: "+selectedCardVal);
		log.info("Video poker plus guard: attempt to fix double start odds...");

		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

		while (true){
			if (cardsTaken.size() >= cardsAllowed){
				for (int i=cardsTaken.size()-1;i>=0;i--){
					deck.returnCard(cardsTaken.get(i));
				}
				guardAction.setActionValue("0");
				return;
			} else if ((selectedCardVal >= 6) && (selectedCardVal <= 9)){
				deck.returnCard(cardsTaken.get(cardsTaken.size()-1));
				guardAction.setActionValue("1");
				return;
			}

			try {
				selectedCard = deck.getNextCard();
				cardsTaken.add(selectedCard);
				selectedCardVal = PokerUtil.getCardValue(selectedCard);
			} catch (DeckOutOfCardsException e){
				log.error("Out of cards while guarding double start.");
				for (int i=cardsTaken.size()-1;i>=0;i--){
					deck.returnCard(cardsTaken.get(i));
				}
				guardAction.setActionValue("0");
				return;
			}
		}
	}

	public static void guardDouble(int gameID, Deck deck, long ownerID, final double multipliers[], double betAmount, double currMulti, Integer[] doubleCards, int doubleCount, /*boolean higher,*/int color, GameSettings settings, GameAction parentAction){
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);

		log.debug("Video Poker Plus Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Video Poker Plus Guard enabled");
		}

		if (!guard) {
			return;
		}

		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		double allowedMultiplier = 0.0;

		if (allowedWins > 0.0){
			allowedMultiplier = allowedWins / betAmount;
		}

		int cardsAllowed = deck.getCardsRemaining() - (doubleCards.length - doubleCount);

		double maxPossible = ((currMulti * 2) + ((doubleCount + 1) == doubleCards.length ? multipliers[ROYAL_FLUSH] : 0.0));
		if (maxPossible <= allowedMultiplier){
			return;
		} else if (cardsAllowed <= 0){
			return;
		}

		log.info("Video poker plus guard: attempt to fix double odds...");

		Integer[] doubleCardsCpy = new Integer[doubleCards.length];
		System.arraycopy(doubleCards, 0, doubleCardsCpy, 0, doubleCards.length);

		List<Integer> cardsTaken = new ArrayList<Integer>();
		boolean[] dummy = new boolean[doubleCardsCpy.length];
		int selectedCard = deck.getNextCard();
		cardsTaken.add(selectedCard);
		int selectedCardVal = PokerUtil.getCardValue(selectedCard);

		log.debug("ORIGINAL CARD SELECTED: "+selectedCardVal);
		//int doubleCardVal = VideoPokerDeck.getCardValue(doubleCardsCpy[doubleCount]);
		doubleCardsCpy[doubleCount+1] = selectedCard;
		double multi = 0.0;

		log.debug("ALLOWED WINS: "+allowedWins);
		log.debug("BET AMOUNT: "+betAmount);
		log.debug("ALLOWED MULTIPLIER: "+allowedMultiplier);

		/*if (selectedCardVal == doubleCardVal){
            multi = currMulti;

            if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
                multi += getWinningCombination(state,dummy);
            }
        } else if (selectedCardVal > doubleCardVal){
            if (higher){
                multi = currMulti * 2.0;

                if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
                    multi += getWinningCombination(state,dummy);
                }
            } else {
                multi = 0.0;
            }
        } else if (selectedCardVal < doubleCardVal){
            if (!higher){
                multi = currMulti * 2.0;
            } else {
                multi = 0.0;
            }

            if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
                multi += getWinningCombination(state,dummy);
            }
        }*/

		if ((Card.getCardColour(selectedCardVal) == color)){
			multi = currMulti * 2.0;

			if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
				multi += getWinningCombination(doubleCardsCpy, dummy, Card.JACK);
			}
		} else {
			multi = 0.0;
		}

		while ((cardsTaken.size() < cardsAllowed) && (multi > allowedMultiplier)){
			try{
				selectedCard = deck.getNextCard();
				doubleCardsCpy[doubleCount+1] = selectedCard;
				cardsTaken.add(selectedCard);
				selectedCardVal = PokerUtil.getCardValue(selectedCard);
				multi = 0.0;

				if ((Card.getCardColour(selectedCardVal) == color)){
					multi = currMulti * 2.0;

					if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
						multi += getWinningCombination(doubleCardsCpy, dummy, Card.JACK);
					}
				} else {
					multi = 0.0;
				}

				/* if (selectedCardVal == doubleCardVal){
                    multi = currMulti;

                    if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
                        multi += getWinningCombination(state,dummy);
                    }
                } else if (selectedCardVal > doubleCardVal){
                    if (higher){
                        multi = currMulti * 2.0;

                        if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
                            multi += getWinningCombination(state,dummy);
                        }
                    } else {
                        multi = 0.0;
                    }
                } else if (selectedCardVal < doubleCardVal){
                    if (!higher){
                        multi = currMulti * 2.0;
                    } else {
                        multi = 0.0;
                    }

                    if ((doubleCount+1) >= VideoPokerPlusRound.DOUBLE_LIMIT){
                        multi += getWinningCombination(state,dummy);
                    }
                }*/
			} catch (DeckOutOfCardsException e){
				log.warn("Out of cards while guarding round double.",e);
				for (int i=cardsTaken.size()-1;i>=0;i--){
					deck.returnCard(cardsTaken.get(i));
				}
				break;
			}
		}

		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

		if (multi <= allowedMultiplier){
			guardAction.setActionValue("1");
		} else {
			guardAction.setActionValue("0");
		}

		deck.returnCard(cardsTaken.get(cardsTaken.size()-1));
	}
}
