/**
 * 
 */
package com.magneta.games.fixedodd;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.utils.Card;
import com.magneta.games.Deck;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.winner.WinnerRound;

public class WinnerGuard {

	private static final Logger log = LoggerFactory.getLogger(WinnerGuard.class);
	
    private static boolean showedMsg = false;
    
    public static void guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int[] reelStops, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Winner Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Winner Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        log.debug("ORIGINAL REEL STOPS");
        for (int i: reelStops){
            log.debug("Reel stop: "+i);
        }
        log.debug("*******************");
        
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        log.debug("Player allowed wins: "+allowedWins);
        if (allowedWins < 0.0) {
            allowedWins = 0.0;
        }
        
        double playerWins = getWins(config, reelStops, betUnit);
        
        log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins);
        
        if ((playerWins <= allowedWins)/* || (multiplier <= 10.0)*/){
            return;
        }
        
        log.info("Winner guard: attempt to fix odds");
        
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);

        int[][] REELS = config.getReels(0);
        
        for (int changes2 = 0; changes2 < REELS[2].length; changes2++) {

            reelStops[2]++;
            if (reelStops[2] >= REELS[2].length) {
                reelStops[2] = 0;
            }

            playerWins = getWins(config, reelStops, betUnit);

            if (playerWins <= allowedWins) {
                log.debug("Found acceptable wins of: "+playerWins+" VS allowed of: "+allowedWins);
                guardAction.setActionValue("1");
                log.debug("FINAL REEL STOPS");
                for (int i: reelStops){
                    log.debug("Reel stop: "+i);
                }
                log.debug("*******************");
                return;
            }
        }
        
        guardAction.setActionValue("0");
    }
    
    public static void guardDouble(int gameID, Deck deck, long ownerID, double currWins, int color, int race, GameSettings settings, GameAction parentAction){
        boolean guard = settings.get("guard", Boolean.TYPE);
        double payoutRatio = settings.get("payout_ratio", Double.TYPE);
        
        log.debug("Winner Guard called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);
        
        if (!showedMsg && guard) {
            showedMsg = true;
            log.info("Winner  Guard enabled");
        }
        
        if (!guard) {
            return;
        }
        
        double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
        
        if (allowedWins >= (currWins * (race >= 0 ? 4.0 : 2.0))){
            return;
        }
        
        log.info("Winner  guard: attempt to fix double odds");
        GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
        
        int card = deck.getNextCard();
        while(deck.getCardsRemaining() > 1 && (race >= 0 ? (Card.getCardRace(card) == race) : (Card.getCardColour(card) == color))){
            card = deck.getNextCard();
        }
        
        if ((race >= 0 && (Card.getCardRace(card) != race)) || (Card.getCardColour(card) != color)){
            deck.returnCard(card);
            guardAction.setActionValue("1");
        } else {
            deck.reset();
            guardAction.setActionValue("0");
        }
    }
    
    public static double getWins(SlotConfiguration config, int[] reelStops, double betUnit){
        
        int[][] REELS = config.getReels(0);
        //get the LEFT side view
        int[][] reelsView = new int[3][5];
        for (int i=0;i<reelsView[0].length;i++){
            int reelStop = reelStops[i];

            reelsView[0][i] = REELS[i][(reelStop > 0 ? (reelStop - 1) : (REELS[i].length-1))];
            reelsView[1][i] = REELS[i][reelStop];
            reelsView[2][i] = REELS[i][(reelStop+1) % REELS[i].length];
        }

        //FOR DEBUGGING REASONS
        if (log.isDebugEnabled()){
            for (int[] line: reelsView){
                String reel = "GUARD REEL: ";
                for (int fruit: line){
                    reel += fruit+" ";
                }
                log.debug(reel);
            }
        }
        
        int[] currLine = new int[5];
        double playerWins = 0.0;
        
        for (int i=0;i<currLine.length;i++){
            //check line from the view
            config.getPayline(reelsView, i, currLine);
            
            int combo = WinnerRound.getPaylineWinCombination(config, currLine);
            if (combo != -1){
                 playerWins += config.getPayout(combo) * betUnit;
            }
        }
        
        return playerWins;
    }
}

