package com.magneta.games.fixedodd;


import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.games.ActionFailedException;
import com.magneta.games.GameAction;
import com.magneta.games.GameSettings;
import com.magneta.games.configuration.slots.BankBurstConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.games.slots.bankburst.BankBurstRound;

/**
 * @author kleanthis
 * 
 *         This guard works as follows : If the amount won is greater than the amount
 *          allowed to win the guard respinds in a hope to win less. this is repeated
 *           five times. Bonus stages are not taken into consideration.
 */
public class BankburstGuard {

	private static final Logger log = LoggerFactory.getLogger(BankburstGuard.class);

	private static boolean showedMsg = false;

	public static int[] guardRound(int gameID, long ownerID, SlotConfiguration config, double betUnit, int[] rellsetChosen, int[] reelStops, int maxPayline, GameSettings settings, GameAction parentAction, BankBurstRound round,Random random) throws ActionFailedException{
		boolean guard = settings.get("guard", Boolean.TYPE);
		double payoutRatio = settings.get("payout_ratio", Double.TYPE);
		Double maxAllowedPayout = settings.get("max_payout", Double.TYPE);
		if (maxAllowedPayout == null){
			maxAllowedPayout = new Double(Double.MAX_VALUE);
		}
		log.debug("Bankburst called with settings: guard = "+guard+", payout_ratio = "+payoutRatio);

		if (!showedMsg && guard) {
			showedMsg = true;
			log.info("Bankburst Guard enabled");
		}

		if (!guard) {
			return reelStops;
		}

		log.debug("ORIGINAL REEL STOPS");
		for (int i: reelStops){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");


		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		log.debug("Player allowed wins: "+allowedWins);
		if (allowedWins < 0.0) {
			allowedWins = 0.0;
		}

		int reelSet = rellsetChosen[0];
		double playerWins1 =round.evaluatePositionWinnings(reelSet , reelStops,maxPayline);

		int[][] reelsViewIn = round.calculateReelView(config.getReels(reelSet), reelStops);
		List<Integer> viewCombosIn = BankBurstRound.getReelViewWinCombinations(config, reelsViewIn,reelSet);

		for (Integer viewcombo : viewCombosIn) {
			Integer currentSelectCombo = viewcombo;
			Object[] winCombo = config.getWinCombo(currentSelectCombo);

			Integer symbol = (Integer)winCombo[0];
			int bonusIndex = (Integer)winCombo[1];

			if (symbol < 0) {
				symbol = symbol * -1;
			}

			if (symbol.equals(BankBurstConfiguration.CODE_SCATTER)) {	
				int[][] codeBonus = BankBurstConfiguration.CODE_BONUS_WEIGHTS[bonusIndex - 3];
				int maxMulltiplier=0;
				maxMulltiplier = new Double(Math.floor(allowedWins - playerWins1)/((maxPayline+1)*betUnit)).intValue();
				if(maxMulltiplier<0){
					maxMulltiplier = 0;
				}
				int[][] possibleCodeBonus = getPossibleCodeBonus(codeBonus,maxMulltiplier);
				if(possibleCodeBonus!=null){
					//						because this ordered we get the last/max and add it to the payout.
					playerWins1 += possibleCodeBonus[possibleCodeBonus.length-1][0] * (betUnit * (maxPayline+1));
				}else{
					playerWins1 += codeBonus[0][0] * (betUnit * (maxPayline+1));
				}
			} else if (symbol.equals(BankBurstConfiguration.BANK_BURST_SCATTER)) {

				int[][] bankBurstBonus = BankBurstConfiguration.BANK_BURST_BONUS_WEIGHTS[bonusIndex - 3];
				int maxMulltiplier=0;
				maxMulltiplier = new Double(Math.floor(allowedWins - playerWins1)/((maxPayline+1)*betUnit)).intValue();
				int[][] possibleBankBonus = getPossibleBankBonus(bankBurstBonus,maxMulltiplier);
				// summing the multipliers by groups of 3
				if(possibleBankBonus!=null){

					//						because this ordered we get the last/max and add it to the payout.
					playerWins1 += possibleBankBonus[possibleBankBonus.length-1][0] * (betUnit * (maxPayline+1));
					playerWins1 += possibleBankBonus[possibleBankBonus.length-2][0] * (betUnit * (maxPayline+1));
					playerWins1 += possibleBankBonus[possibleBankBonus.length-3][0] * (betUnit * (maxPayline+1));
				}else {
					playerWins1 += bankBurstBonus[0][0] * (betUnit * (maxPayline+1));
					playerWins1 += bankBurstBonus[1][0] * (betUnit * (maxPayline+1));
					playerWins1 += bankBurstBonus[2][0] * (betUnit * (maxPayline+1));
				}
			}
		}

		log.debug("Allowed Wins: "+allowedWins+", player wins: " +  playerWins1);

		if (playerWins1 > maxAllowedPayout){
			log.debug("watch this ");
		}

		if ((playerWins1 <= maxPayline+1*betUnit)){
			return reelStops;
		}

		// if both of the conditions are true.
		if ((playerWins1 <= allowedWins) && (playerWins1 <= maxAllowedPayout)){
			return reelStops;
		}

		log.info("Bankburst guard: attempt to fix odds");

		int minRelsetSoFar = reelSet;
		double minPayoutSoFar = playerWins1;
		int[] minReelStopsSoFar = Arrays.copyOf(reelStops, reelStops.length);
		GameAction guardAction = parentAction.createGeneratedAction(null, 0, "GUARD", null, 0.0);
		guardAction.setActionValue("0");
		int[][] weights = round.getWeights();

		for(int t=0;t<5;t++){

			int reelSetTmp = round.getWeightedRandom(weights,random);
			int[] reelstmp = spinReels(config.getReels(reelSetTmp),random);
			double playerWins2 = round.evaluatePositionWinnings(reelSetTmp, reelstmp,maxPayline);
			int[][] reelsView = round.calculateReelView(config.getReels(reelSetTmp), reelstmp);
			List<Integer> viewCombos = BankBurstRound.getReelViewWinCombinations(config, reelsView,reelSetTmp);

			for (Integer viewcombo : viewCombos) {
				Integer currentSelectCombo = viewcombo;
				Object[] winCombo = config.getWinCombo(currentSelectCombo);

				Integer symbol = (Integer)winCombo[0];
				int bonusIndex = (Integer)winCombo[1];

				if (symbol < 0) {
					symbol = symbol * -1;
				}

				if (symbol.equals(BankBurstConfiguration.CODE_SCATTER)) {	
					int[][] codeBonus = BankBurstConfiguration.CODE_BONUS_WEIGHTS[bonusIndex - 3];
					int maxMulltiplier=0;
					maxMulltiplier = new Double(Math.floor(allowedWins - playerWins1)/((maxPayline+1)*betUnit)).intValue();
					if(maxMulltiplier<0){
						maxMulltiplier = 0;
					}
					int[][] possibleCodeBonus = getPossibleCodeBonus(codeBonus,maxMulltiplier);
					if(possibleCodeBonus!=null){
						//						because this ordered we get the last/max and add it to the payout.
						playerWins2 += possibleCodeBonus[possibleCodeBonus.length-1][1];
					}else {
						playerWins2 += codeBonus[0][0];
					}
				} else if (symbol.equals(BankBurstConfiguration.BANK_BURST_SCATTER)) {

					int[][] bankBurstBonus = BankBurstConfiguration.BANK_BURST_BONUS_WEIGHTS[bonusIndex - 3];
					int maxMulltiplier=0;
					maxMulltiplier = new Double(Math.floor(allowedWins - playerWins1)/((maxPayline+1)*betUnit)).intValue();
					int[][] possibleBankBonus = getPossibleBankBonus(bankBurstBonus,maxMulltiplier);
					// summing the multipliers by groups of 3
					if(possibleBankBonus!=null) {
						for (int i=possibleBankBonus.length -1; i >= 0 & i >= possibleBankBonus.length - 3; i--) {
							playerWins2 += possibleBankBonus[i][0]* (betUnit * (maxPayline+1));
						}
					}else{
						playerWins2 += bankBurstBonus[0][0]* (betUnit * (maxPayline+1));
						playerWins2 += bankBurstBonus[1][0]* (betUnit * (maxPayline+1));
						playerWins2 += bankBurstBonus[2][0]* (betUnit * (maxPayline+1));
					}
				}
			}

			if (playerWins2 < minPayoutSoFar){
				minRelsetSoFar = reelSetTmp;
				minPayoutSoFar = playerWins1;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
			}

			if ((playerWins2 <= maxPayline+1*betUnit)){
				minRelsetSoFar = reelSetTmp;
				minPayoutSoFar = playerWins1;
				minReelStopsSoFar = Arrays.copyOf(reelstmp, reelstmp.length);
				guardAction.setActionValue("1");
				break;
			}

			if(minPayoutSoFar <= allowedWins && minPayoutSoFar<= maxAllowedPayout){
				break;
			}

		}
		log.debug("Found acceptable wins of: "+minPayoutSoFar+" VS allowed of: "+allowedWins);

		log.debug("FINAL REEL STOPS");
		for (int i: minReelStopsSoFar){
			log.debug("Reel stop: "+i);
		}
		log.debug("*******************");

		rellsetChosen[0] = new Integer(minRelsetSoFar);
		return minReelStopsSoFar;
	}

	public static int[][] getPossibleBankBonus(int[][] bankBurstBonus,
			int maxMulltiplier) {
		int[][] orderedMultipliers = bankBurstBonus.clone();
		// ordering the table
		java.util.Arrays.sort(orderedMultipliers, new java.util.Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});

		int length = 0;
		length = orderedMultipliers.length;

		if(length<3){
			return null;
		}
		// counting which is acceptable results.
		int counter = 0;
		int summedMultiplier =0;
		boolean hasSpecialCase = false;
		for (int i = 0; i < orderedMultipliers.length-2; i++) {

			if(orderedMultipliers[i][0]<=0){
				hasSpecialCase = true;
				counter++;
				continue;
			}

			for(int r =0; r<3;r++){
				summedMultiplier=+orderedMultipliers[i+r][0];
			}
			if(summedMultiplier<=maxMulltiplier){
				counter++;
			}else{
				break;
			}
		}

		if(counter>1 && hasSpecialCase){
			return Arrays.copyOfRange(orderedMultipliers, 1, 2+counter);
		}else if(counter>0){
			return Arrays.copyOf(orderedMultipliers, 2+counter);
		}else{
			return null;
		}
	}

	public static int[][] getPossibleBankBonus(int[][] codeBonus, double totalBetAmount, int gameID, long ownerID, GameSettings gameSettings) {
		double payoutRatio = gameSettings.get("payout_ratio", Double.TYPE);
		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);

		int maxMulltiplier = new Double(Math.floor(allowedWins)/(totalBetAmount)).intValue();
		if(maxMulltiplier<0){
			maxMulltiplier = 0;
		}
		int[][] result = getPossibleBankBonus(codeBonus, maxMulltiplier);

		// ordering the table
		java.util.Arrays.sort(codeBonus, new java.util.Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});

		if(result==null){
			result = Arrays.copyOf(codeBonus, 3);
		}
		return result;
	}

	public static int[][] getPossibleCodeBonus(int[][] codeBonus,int maxMulltiplier) {
		int[][] orderedMultipliers = codeBonus.clone();

		// ordering the table
		java.util.Arrays.sort(orderedMultipliers, new java.util.Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});

		// counting which is acceptable results.
		int counter = 0;
		for (int[] w: orderedMultipliers) {
			if(w[0]<maxMulltiplier){
				counter++;
			}else{
				break;
			}
		}
		// making sure we return only the accepted ones.		
		if(counter <= 0) {
			return null;
		}
		return Arrays.copyOf(orderedMultipliers, counter);
	}

	public static int[][] getPossibleCodeBonus(int[][] codeBonus, double totalBetAmount, int gameID, long ownerID, GameSettings gameSettings) {
		double payoutRatio = gameSettings.get("payout_ratio", Double.TYPE);
		double allowedWins = FixedOddUtil.calculateRemainingWins(ownerID, gameID, payoutRatio);
		int maxMulltiplier = new Double(Math.floor(allowedWins)/(totalBetAmount)).intValue();
		if(maxMulltiplier<0){
			maxMulltiplier = 0;
		}
		int[][] result = getPossibleCodeBonus(codeBonus, maxMulltiplier);

		// ordering the table
		java.util.Arrays.sort(codeBonus, new java.util.Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});

		if(result==null){
			result = Arrays.copyOf(codeBonus, 1);
		}
		return result;
	}

	protected final static int[] spinReels(int[][] roundReels,Random random) {
		int[] reelStops = new int[roundReels.length];

		for (int i=0;i<reelStops.length;i++){
			reelStops[i] = random.nextInt(roundReels[i].length);
		}
		return reelStops;
	}



}
