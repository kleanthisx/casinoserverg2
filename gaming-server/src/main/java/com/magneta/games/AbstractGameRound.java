package com.magneta.games;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.GameActionRequestDefinition.ParameterType;
import com.magneta.games.configuration.GameConfiguration;

/**
 * GameRound class holds the information related to a game round and defines general game round operations.
 */
public abstract class AbstractGameRound implements GameRound {

	private static final Logger log = LoggerFactory.getLogger(AbstractGameRound.class);
	
    protected static final String START_ACTION = "START";
    protected static final String FINISH_ACTION = "FINISH";
    protected static final String BET_ACTION = "BET";
    protected static final String WIN_ACTION = "WIN";

    public enum GameRoundState {
        ROUND_NOT_STARTED, ROUND_STARTED, ROUND_FINISHED;
    }
    
    protected GameBean game;
    private GameTemplateInfo gameTemplate;
    private GameConfiguration configuration;

    protected RoundOptions options;

    private Random jackpotRandom;
    private Random random;
    private Random guardRandom;
    
    private long tableId;
    private int roundId;
    
    private GameRoundListener roundListener;
    private int currActionId;
    private final GamePlayerStats roundStats;
   
    public AbstractGameRound() {
    	this.roundStats = new GamePlayerStats();
    }
    
    @Override
    public GamePlayerStats getRoundStats() { 
    	return this.roundStats;
    }
    
    @Override
    public GameBean getGame() {
    	return this.game;
    }
    
    @Override
    public void setRoundListener(GameRoundListener roundListener) {
    	this.roundListener = roundListener;
    }

    @Override
	public final void init(GameBean game, GameTemplateInfo gameTemplate, GameConfiguration configuration, RoundOptions options, long tableId, int roundId, byte[] seed) throws ActionFailedException {
    	this.game = game;
        this.gameTemplate = gameTemplate;
        this.configuration = configuration;
        
        this.tableId = tableId;
        this.roundId = roundId;
        this.options = options;
        this.currActionId = 0;
        
        try {
            this.random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } 
        ((SecureRandom)this.random).setSeed(seed);
        this.jackpotRandom = createJackpotRandom();
        this.guardRandom = createGuardRandom();
        roundInit();
    }
    
	@Override
    public final void init(GameRound parentRound, byte[] seed) throws ActionFailedException {
		AbstractGameRound parent = (AbstractGameRound)parentRound;
		
    	init(parent.getGame(), parent.getGameTemplate(), parent.getConfig(), parent.options, parent.getTableId(), parent.getId()+1, seed);
    	roundInit(parent);
    }

    /**
     * May be implemented by sub-classes to
     * do initial state based on configuration and options
     * @throws ActionFailedException 
     */
    protected void roundInit() throws ActionFailedException {
    	
    }
    
    /**
     * May be implemented by sub-classes to
     * do initial state based on the previous round.
     * roundInit() will be called prior to this.
     * @throws ActionFailedException 
     *
     * @see roundInit()
     */
    protected void roundInit(AbstractGameRound previousRound) throws ActionFailedException {
    }
    
    private final Random createJackpotRandom() {
        byte[] seed = new byte[64]; 
        this.getRandom().nextBytes(seed);

        SecureRandom rand;
        try {
            rand = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } 
        rand.setSeed(seed);

        return rand;
    }
    
    private final Random createGuardRandom() {
        byte[] seed = new byte[64]; 
        this.jackpotRandom.nextBytes(seed);

        SecureRandom rand;
        try {
            rand = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } 
        rand.setSeed(seed);

        return rand;
    }
    
    public final Random getGuardRandom() {
    	return this.guardRandom;
    }

    @Override
    public final GameTemplateInfo getGameTemplate() {
    	return this.gameTemplate;
    }

    /** 
     * Returns the next available action ID.
     * Action ID's are never reused so make sure you do some validation before
     * creating an action.
     * @return The next available action ID.
     */ 
    public final int nextActionId() {
        int actionId;

        actionId = currActionId;
        currActionId++;

        return actionId;
    }

    public final void setCurrActionId(int value) {
        this.currActionId = value;
    }

    @Override
    public GameConfiguration getConfig() {
    	return configuration;
    }

    /**
     * Returns the Random object for this round.
     * @return The Random object for this round.
     */
    protected final Random getRandom() {
        return this.random;
    }

    /**
     * Gets the GameActionFilter for this game round.
     * @param session The HttpSession of the user requesting the filter.
     * @return The GameActionFilter for the given user.
     */
    public GameActionFilter getActionFilter() {
        return new DefaultGameActionFilter(false);
    }

    /**
     * Puts information about the round in the info map.
     *  
     */
    @Override
    public final void getInfo(GamePlayer player, Map<String,Object> info, boolean firstTime) {
        info.put("curr_round_id", this.roundId);
        
        if (gameTemplate.supportsGameJackpot()) {
            double gJackpot = JackpotUpdater.getInstance().getGameJackpot(game.getGameId());
            if (gJackpot > 0.0){
                info.put("jackpot", gJackpot);
            }
        }

        if (gameTemplate.supportsMysteryJackpot()) {
            double mJackpot = JackpotUpdater.getInstance().getMysteryJackpot(game.getGameId());
            if (mJackpot > 0.0){
            	info.put("mystery_jackpot", mJackpot);
            }

            Long corpId = player.getCorpId();

            if (corpId != null) {
                double sJackpot = JackpotUpdater.getInstance().getCorporateJackpot(corpId);
                if (sJackpot > 0.0){
                	info.put("corporate_jackpot", sJackpot);
                }
            }

            double megaJackpot = JackpotUpdater.getInstance().getMegaJackpot();
            if (megaJackpot > 0.0){
            	info.put("mega_jackpot", megaJackpot);
            }
        }
        
        getExtraInfo(player, info, firstTime);
    }
    

    /**
     * Gets any subclass specific round information if available.
     * @param roundInfo The Map to save the extra information.
     */
    protected void getExtraInfo(GamePlayer player, Map<String, Object> roundInfo, boolean firstTime) {
    }

    /**
     * Notifies all listeners that an action has completed.
     * Generated and additional actions are ignored. Only the parent should
     * be passed.
     * 
     * @note Parent actions going through doAction() and doRequest() do not 
     * need to call this method. Only parent actions originating inside the round
     * class need to call this.
     * 
     * @param action The action to notify its completion.
     */
    protected final void actionCompleted(GameAction action) {
        if ((!action.isAdditionalAction()) && (!action.isGeneratedAction())) {
        	if (this.roundListener != null) {
        		roundListener.onActionPerformed(this, action);
        	}
        }
    }

    /**
     * Returns the round ID for this round.
     * @return The round ID for this round.
     */
    @Override
    public final int getId() {
        return roundId;
    }
    
    @Override
    public final long getTableId() {
    	return tableId;
    }

    /**
     * Validates the given GameActionRequest
     * @param request The GameActionRequest to validate
     * @param results The Map containg the results of the action request.
     * @return True if the request is valid, false otherwise.
     */
    protected abstract void validateGameActionRequest(GameActionRequest request) throws ActionFailedException;

    /**
     * Performs the given action.
     * @note The action has already been validated with validateGameActionRequest.
     * @param action The action to perform
     * @param results The Map to save the results of the action
     * 
     * @return Whether the round has finished.
     */
    protected abstract void performAction(GameAction action) throws ActionFailedException;

    @Override
    public abstract boolean isFinished();
    
    /**
     * Performs the given request.
     * @param request The ActionRequest to perform.
     */ 
    @Override
    public final void doRequest(GamePlayer player, GameActionRequest request) throws ActionFailedException {    	
        validateGameActionRequest(request);

        GameAction action = new GameAction(player, this, request.getSeatId(), request.getDescription(), request.getArea(), request.getAmount());
        
        performAction(action);
        actionCompleted(action);
    }

    /**
     * Validates a request against the given definition array.
     * @param requestDefinitions
     * @param request
     * @param results
     * @return
     */
    protected static final void validateRequest(GameActionRequestDefinition[] requestDefinitions, GameActionRequest request)  throws ActionFailedException {

        boolean foundAction = false;

        /* Check if the action is valid */
        for (GameActionRequestDefinition s: requestDefinitions) {

            if (s == null) {
                continue;
            }

            if (s.description.equals(request.getDescription())) {

                foundAction = true;

                if (s.area == ParameterType.FORBIDDEN && request.getArea() != null) {
                    throw new ActionFailedException("Invalid action: " + request.getDescription() + ", Area is not accepted");
                } else if (s.area == ParameterType.REQUIRED && request.getArea() == null) {
                    throw new ActionFailedException("Invalid action: " + request.getDescription() + ", Area is required");
                }

                if (s.amount == ParameterType.FORBIDDEN && request.getAmount() != 0.0) {
                    throw new ActionFailedException("Invalid action: " + request.getDescription() + ", Amount is not accepted");
                }

                break;
            }
        }

        if (!foundAction) {
            throw new ActionFailedException("Invalid action: " + request.getDescription());
        }
    }

    /**
     * Performs the given action.
     * @param action The action to perform.
     */
    protected final void doAction(GameAction action) throws ActionFailedException { 
        performAction(action);
        actionCompleted(action);
    }

    /**
     * Loads actions for this round.
     * @param player
     * @param actions
     * @throws ActionFailedException
     */
    @Override
    public final void loadActions(GamePlayer player, Iterable<GameAction> actions) throws ActionFailedException {
    	for (GameAction action: actions) {
    		log.debug("Loading action {}", action);
    		setCurrActionId(action.getActionId()+1);
    		loadAction(action);
    	}
    	
    	loadFinished(player);
    }
    
    /**
     * Loads an action.
	 *
     * @param action The action to load.
     * @throws ActionFailedException
     */    
    protected void loadAction(GameAction action) throws ActionFailedException {
        doAction(action);
    }
    
    /**
     * Called when the round has finished loading.
     * @throws ActionFailedException 
     */
    protected void loadFinished(GamePlayer player) throws ActionFailedException {
    }

    /**
     * Used by all games, EXCEPT Roulette and VirtualRacing(performBets), for checking that bet amount is within min/max.
     * @param action The bet action to check.
     * @throws ActionFailedException when amount is not within min/max bet.
     */
    protected void checkBet(GameAction action) throws ActionFailedException {
    	if (action.isLoaded()) {
    		return;
    	}

    	if (action.getAmount() < (options.getMinBet() - 0.001)){
    		throw new ActionFailedException("Amount is below the minimum bet!");
    	} else if (action.getAmount() > (options.getMaxBet() + 0.001)){
    		throw new ActionFailedException("Amount is over the maximum bet!");
    	}

    	if (action.getPlayer() == null && action.getAmount() > 0.001) {
    		logWarn("Amount action without player");
    	}
    }

    protected final void logError(String msg, Throwable e) {
        log.error("Round " + this.tableId + "-" + this.roundId + ": " + msg, e);
    }

    protected final void logError(String msg) {
        log.error("Round " + this.tableId + "-" + this.roundId + ": " + msg);
    }

    protected final void logWarn(String msg, Throwable e) {
        log.warn("Round " + this.tableId + "-" + this.roundId + ": " + msg, e);
    }

    protected final void logWarn(String msg) {
        log.warn("Round " + this.tableId + "-" + this.roundId + ": " + msg);
    }
    
    protected final void logInfo(String msg, Throwable e) {
        log.info("Round " + this.tableId + "-" + this.roundId + ": " + msg, e);
    }

    protected final void logInfo(String msg) {
        log.info("Round " + this.tableId + "-" + this.roundId + ": " + msg);
    }
    
    protected final void logDebug(String msg, Throwable e) {
        log.debug("Round " + this.tableId + "-" + this.roundId + ": " + msg, e);
    }

    protected final void logDebug(String msg) {
        log.debug("Round " + this.tableId + "-" + this.roundId + ": " + msg);
    }

    protected final boolean claimMysteryJackpot(GameAction parentAction, double betAmount, double ratio, boolean placedJackpotContrib){
        if (!placedJackpotContrib || parentAction.isLoaded())
            return false;

        return JackpotsHandler.claimMysteryJackpot(jackpotRandom, this, parentAction, betAmount, ratio);
    }
}