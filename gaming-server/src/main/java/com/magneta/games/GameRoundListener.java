package com.magneta.games;

/**
 * GameRoundListener listens for events that take place in a certain GameRound.
 */
public interface GameRoundListener {

    /**
     * Notifies the listener that a GameAction was performed in the given GameRound.
     * @param round The round where the action took place.
     * @param action The action that was completed.
     */
    public void onActionPerformed(AbstractGameRound round, GameAction action);
}
