package com.magneta.test.gamingserver;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import com.magneta.Log;
import com.magneta.casino.common.utils.Card;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.Deck;
import com.magneta.games.DeckOutOfCardsException;

import junit.framework.TestCase;

public class TestDeck extends TestCase {

    public void testGetNextCard() throws DeckOutOfCardsException
    {
        Deck deck = new Deck(1,2);

        for (int i=0;i<52;i++){
            assertTrue(compareCardValue(deck.getNextCard(), ((i%13)+1)));
        }

        //JOKERS
        assertTrue(compareCardValue(deck.getNextCard(),14));
        assertTrue(compareCardValue(deck.getNextCard(),14));
    }

    public boolean compareCardValue(int card, int cardVal){
        return (Card.getCardNumber(card) == cardVal);
    }

    public void testShuffle() throws NoSuchAlgorithmException {
        Deck deck = null;
        
        int[][] instances = new int[13][20];
        
        for (int j=0;j<1000000;j++){
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(sr.generateSeed(128));
            deck = new Deck(6, 0);
            deck.shuffleCards(sr);
            
            for (int i=0;i<20;i++){
                int card = deck.getNextCard();
                instances[Card.getCardNumber(card)-1][i]++;
            }
        }
        
        for (int i=0;i<instances.length;i++){
            if (i < 9){
                System.out.print(" ");
            }
            System.out.print("  "+(i+1)+": ");
            for (int j=0;j<instances[i].length;j++){
                System.out.print(instances[i][j]+" | ");
            }
            System.out.print("\n");
        }
    }
}
