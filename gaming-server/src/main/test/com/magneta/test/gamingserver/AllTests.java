package com.magneta.test.gamingserver;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.magneta");
        //$JUnit-BEGIN$
        /*suite.addTestSuite(TestXmlRpc.class);
        suite.addTestSuite(TestSessionProperties.class);
        suite.addTestSuite(TestTable.class);
        suite.addTestSuite(TestRoundGame.class);
        suite.addTestSuite(TestRoundSimple.class);
        suite.addTestSuite(TestLogin.class);
        suite.addTestSuite(TestTableHandler.class);
        suite.addTestSuite(TestPaymentServlets.class);
        suite.addTestSuite(TestGameFactory.class);
        suite.addTestSuite(TestGuid.class);
        suite.addTestSuite(TestUtil.class);
        suite.addTestSuite(TestUserHandler.class);
        suite.addTestSuite(TestDeck.class);
        suite.addTestSuite(TestLobbyHandler.class);
        suite.addTestSuite(TestVideoPoker.class);
        suite.addTestSuite(TestPassword.class);
        suite.addTestSuite(TestFileSeedGenerator.class);*/
        //$JUnit-END$
        return suite;
    }

}
