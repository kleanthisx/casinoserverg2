/**
 * 
 */
package com.magneta.test.gamingserver;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

import com.magneta.games.FileSeedGeneratorImpl;

import junit.framework.TestCase;


/**
 * @author anarxia
 *
 */
public class TestFileSeedGenerator extends TestCase {

    private static final String TEST_FILE = "C:\\PDOXUSRS.NET";

    public void testSeed() throws Exception {
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(TEST_FILE));
        FileSeedGeneratorImpl seedGen = new FileSeedGeneratorImpl(TEST_FILE);
        int totalRead = 0;
        
        byte[] buf = new byte[64];
        int bufSize;
        
        while ((bufSize = in.read(buf)) > 0) {
            totalRead += bufSize;
            byte[] seed = seedGen.getSeed(bufSize);
            
            for (int i=0; i < bufSize; i++) {
                assertTrue(seed[i] == buf[i]);
            }
        }
    }
}
