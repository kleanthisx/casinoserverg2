package com.magneta.test.gamingserver;

//import org.apache.xmlrpc.XmlRpcException;
import com.magneta.Log;
import com.magneta.casino.common.utils.Card;
import com.magneta.games.videopoker.CardsState;
//import com.magneta.test.gamingserver.tools.clients.ClientInterface;
//import com.magneta.test.gamingserver.tools.clients.RemoteClient;
//import com.magneta.test.gamingserver.tools.players.videopoker.VirtualVPPlayer;
import junit.framework.TestCase;
import static com.magneta.games.configuration.videopoker.VideoPokerConstants.*;
import static com.magneta.games.videopoker.VideoPokerUtil.*;

public class TestVideoPoker extends TestCase {
  
    public void testWinMethod(){
        
        Object[][] tests = {
                {
                    new int[]{29,
                            28,
                            57,
                            42,
                            17}, NONE
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(7, Card.SPADES_CARD),
                              Card.getCard(9, Card.CLUBS_CARD)}, NONE
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(7, Card.SPADES_CARD),
                              Card.getCard(Card.JOKER, Card.NO_TYPE_CARD)}, NONE
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(Card.JACK, Card.SPADES_CARD),
                              Card.getCard(Card.JACK, Card.CLUBS_CARD)}, JACKS_OR_BETTER
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                              Card.getCard(Card.KING, Card.SPADES_CARD)}, JACKS_OR_BETTER
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                              Card.getCard(Card.ACE, Card.SPADES_CARD)}, JACKS_OR_BETTER
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                              Card.getCard(Card.JACK, Card.SPADES_CARD)}, JACKS_OR_BETTER
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                              Card.getCard(3, Card.DIAMONDS_CARD),
                              Card.getCard(5, Card.HEARTS_CARD),
                              Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                              Card.getCard(Card.QUEEN, Card.SPADES_CARD)}, JACKS_OR_BETTER
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                            Card.getCard(8, Card.DIAMONDS_CARD),
                            Card.getCard(6, Card.HEARTS_CARD),
                            Card.getCard(6, Card.NO_TYPE_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD)}, TWO_PAIRS
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                            Card.getCard(8,Card.DIAMONDS_CARD),
                            Card.getCard(8,Card.HEARTS_CARD),
                            Card.getCard(6, Card.CLUBS_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD)}, THREE_OF_A_KIND
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                            Card.getCard(8, Card.DIAMONDS_CARD),
                            Card.getCard(5, Card.HEARTS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD)}, THREE_OF_A_KIND
                },
                {
                    new int[]{Card.getCard(8, Card.CLUBS_CARD),
                            Card.getCard(8, Card.DIAMONDS_CARD),
                            Card.getCard(5, Card.HEARTS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD)}, THREE_OF_A_KIND
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(3, Card.DIAMONDS_CARD),
                            Card.getCard(4, Card.HEARTS_CARD),
                            Card.getCard(5, Card.CLUBS_CARD),
                            Card.getCard(6, Card.SPADES_CARD)}, STRAIGHT
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(3, Card.DIAMONDS_CARD),
                            Card.getCard(4, Card.HEARTS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(6, Card.SPADES_CARD)}, STRAIGHT
                },
                {
                    new int[]{Card.getCard(10,Card.CLUBS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(Card.JACK, Card.DIAMONDS_CARD),
                            Card.getCard(Card.ACE, Card.HEARTS_CARD),
                            Card.getCard(Card.QUEEN, Card.SPADES_CARD)}, STRAIGHT
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(5, Card.CLUBS_CARD),
                            Card.getCard(7, Card.CLUBS_CARD),
                            Card.getCard(9, Card.CLUBS_CARD),
                            Card.getCard(8, Card.CLUBS_CARD)}, FLUSH
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(5, Card.CLUBS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(9, Card.CLUBS_CARD),
                            Card.getCard(8, Card.CLUBS_CARD)}, FLUSH
                },
                {
                    new int[]{Card.getCard(2,Card.CLUBS_CARD),
                            Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(3, Card.HEARTS_CARD),
                            Card.getCard(3, Card.HEARTS_CARD),
                            Card.getCard(3, Card.HEARTS_CARD)}, FULL_HOUSE
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(3, Card.HEARTS_CARD),
                            Card.getCard(3, Card.HEARTS_CARD)}, FULL_HOUSE
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(2, Card.HEARTS_CARD),
                            Card.getCard(8, Card.HEARTS_CARD)}, FOUR_OF_A_KIND
                },
                {
                    new int[]{Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(2, Card.CLUBS_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(2, Card.HEARTS_CARD),
                            Card.getCard(8, Card.HEARTS_CARD)}, FOUR_OF_A_KIND
                },
                {
                    new int[]{Card.getCard(2,Card.SPADES_CARD),
                            Card.getCard(3, Card.SPADES_CARD),
                            Card.getCard(4, Card.SPADES_CARD),
                            Card.getCard(5, Card.SPADES_CARD),
                            Card.getCard(6, Card.SPADES_CARD)}, STRAIGHT_FLUSH
                },
                {
                    new int[]{Card.getCard(2,Card.SPADES_CARD),
                            Card.getCard(3, Card.SPADES_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(5, Card.SPADES_CARD),
                            Card.getCard(6, Card.SPADES_CARD)}, STRAIGHT_FLUSH
                },
                {
                    new int[]{Card.getCard(10,Card.SPADES_CARD),
                            Card.getCard(Card.JACK, Card.SPADES_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD),
                            Card.getCard(Card.QUEEN, Card.SPADES_CARD),
                            Card.getCard(Card.ACE, Card.SPADES_CARD)}, ROYAL_FLUSH
                },
                {
                    new int[]{Card.getCard(10,Card.SPADES_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD),
                            Card.getCard(Card.QUEEN, Card.SPADES_CARD),
                            Card.getCard(Card.ACE, Card.SPADES_CARD)}, ROYAL_FLUSH
                },
                {
                    new int[]{Card.getCard(10,Card.SPADES_CARD),
                            Card.getCard(Card.JACK, Card.SPADES_CARD),
                            Card.getCard(Card.KING, Card.SPADES_CARD),
                            Card.getCard(Card.QUEEN, Card.SPADES_CARD),
                            Card.getCard(Card.JOKER, Card.NO_TYPE_CARD)}, ROYAL_FLUSH
                }
        };
        
        for (int j=0;j<tests.length;j++){
            Object[] o = tests[j];
            
            boolean keepCard[] = new boolean[5];          
            int[] cards = (int[])o[0];
            
            CardsState state = CardsState.getCardsState(cards);
            
            Log.info(" **"+j+") Cards state:\n"+state);
            int winCombo = getWinningCombination(state,keepCard);
            Log.info("Combo: "+getDescription(winCombo));
            
            assertTrue(winCombo == ((Integer)o[1]));
            
            for (int i=0;i<keepCard.length;i++){
                if (keepCard[i]){
                    Log.info("keep card at "+i);
                }
            }
        }
    }
    
    private String getDescription(int combo){
        switch (combo){
            case NONE : return "none";
            case JACKS_OR_BETTER : return "jacks or better";
            case TWO_PAIRS : return "two pairs";
            case THREE_OF_A_KIND : return "three of a kind";
            case STRAIGHT : return "straight";
            case FLUSH : return "flush";
            case FULL_HOUSE : return "full house";
            case FOUR_OF_A_KIND : return "four of a kind";
            case STRAIGHT_FLUSH : return "straight flush";
            case ROYAL_FLUSH : return "royal flush";
            default: return "";
        }
    }
}