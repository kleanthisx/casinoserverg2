package com.magneta.test.gamingserver;

import com.magneta.Log;
import com.magneta.games.server.Util;
import junit.framework.TestCase;

public class TestUtil extends TestCase {

	public boolean parseLong(long num) {
		
		byte[] bytes = Util.getBytes(num);
        
		return Util.getLong(bytes) == num;
	}
	
    public void testParseLong() {
       assertTrue(parseLong(0));
       assertTrue(parseLong(-1));
       assertTrue(parseLong(1));
       assertTrue(parseLong(255));
       assertTrue(parseLong(1024));
       assertTrue(parseLong(Long.MAX_VALUE));
       assertTrue(parseLong(Long.MAX_VALUE - 1));
       
       assertTrue(parseLong(Long.MIN_VALUE));
       assertTrue(parseLong(Long.MIN_VALUE + 1));
       
    }
    
    public void testPattern(){
        System.out.println(System.getProperty("java.library.path"));
        
        String s = "11,2,3,4,5|\n11,2,3,4,5|\n11,2,3,4,5|\n11,2,3,4,5|\n12,3,4,5";
        assertTrue(s.matches("[[0-9]{1}[,[0-9]{1}]*\\|||\n?]+"));
        
        String[] split = s.split("\\|");
        Log.info("Pieces: "+split.length);
        for (String str: split){
            Log.info("str: "+str);
        }
    }
}
