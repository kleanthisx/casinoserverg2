package com.magneta.test.gamingserver;

import com.magneta.Log;
import com.magneta.games.Game;
import com.magneta.games.GameFactory;

import junit.framework.TestCase;

public class TestGameFactory extends TestCase {

    @Override
    public void setUp() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver").newInstance();
    }
    
    public void testGetGame() {
        
        Game game = GameFactory.getGame(1);
        
        assertTrue(game != null);
        
        Log.debug(game.getName());
        
        assertTrue(("Blackjack").equals(game.getName()));
        
        assertTrue(game.getCategory() > 0);
        
        assertTrue(game.getMaxPlayers() == 5);
        
        game = GameFactory.getGame(-1);
        
        assertTrue(game == null);
        
        game = GameFactory.getGame(9);
        
        assertTrue(game == null);
    }
}
