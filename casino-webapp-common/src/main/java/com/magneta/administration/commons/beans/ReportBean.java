package com.magneta.administration.commons.beans;

import java.io.Serializable;

public class ReportBean implements Serializable {

	private static final long serialVersionUID = 3006183033124309353L;

	private String reportName;
	private String reportDesc;
	private String optionsPage;
	private String report;
	private boolean queueReport;
	private String category;
	
	public ReportBean(String report, String reportName, String reportDesc, String optionsPage, String category, boolean queueReport) {
	    this.report = report;
		this.reportName = reportName;
		this.reportDesc = reportDesc;
		this.optionsPage = optionsPage;
		this.queueReport = queueReport;
		this.category = category;
	}
	
	public ReportBean(String report, String reportName, String reportDesc, String optionsPage, String category){
		this(report, reportName, reportDesc, optionsPage, category, false);
	}

	public String getOptionsPage() {
		return optionsPage;
	}

	public String getReportDesc() {
		return reportDesc;
	}

	public String getReportName() {
		return reportName;
	}
	
	public String getReport() {
		return report;
	}

	public boolean isQueueReport() {
		return queueReport;
	}

    
    public String getCategory() {
        return category;
    }

    
    public void setCategory(String category) {
        this.category = category;
    }
	
}