package com.magneta.administration.commons.models;

import org.apache.tapestry.form.IPropertySelectionModel;

public class ReportFormatsSelectionModel implements IPropertySelectionModel {

	private static final String[][] FORMATS = new String[][]{
		{"HTML","HTML Document"},
		{"PDF", "Adobe PDF"},
		{"XLS", "Microsoft Excel"},
		{"ODT", "OpenDocument Text"},
		{"RTF", "Rich Text Format"},
		{"DOCX", "Microsoft Word 2007+"}};
    
    @Override
    public String getLabel(int arg0) {
        return FORMATS[arg0][1];
    }

    @Override
    public Object getOption(int arg0) {
    	return FORMATS[arg0][0];
    }

    @Override
    public int getOptionCount() {
        return FORMATS.length;
    }

    @Override
    public String getValue(int arg0) {
        return FORMATS[arg0][0];
    }

    @Override
    public Object translateValue(String arg0) {
        return arg0;
    }
    
    @Override
    public boolean isDisabled(int arg0) {
		return false;
	}
}
