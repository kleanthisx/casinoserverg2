/**
 * 
 */
package com.magneta.administration.commons.services;

import org.apache.hivemind.Registry;

/**
 * @author anarxia
 *
 * ServiceLocator for Tapestry4 applications.
 * It works using hivemind.
 */
public class ServiceLocator {

    private static Registry registry;
    
    /**
     * This must be set by the application.
     * @param reg
     */
    public static void setRegistry(Registry reg) {
        registry = reg;
    }

    public static Registry getRegistry() {
    	return registry;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T getService(Class<T> arg0) {
        return (T)registry.getService(arg0);
    }
}
