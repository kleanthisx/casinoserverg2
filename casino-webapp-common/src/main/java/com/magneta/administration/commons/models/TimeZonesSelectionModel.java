package com.magneta.administration.commons.models;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import org.apache.tapestry.form.IPropertySelectionModel;

public class TimeZonesSelectionModel implements IPropertySelectionModel {

    String[] zones;
    
    private static final String[] invalidPrefix = {
        "Etc",
        "SystemV"
    };
    
    private boolean isValidId(String id) {
        if (id.length() < 3)
            return false;
        
        if (!id.contains("/")) {
            return false;
        }
        
        String[] parts = id.split("/");
    
        for (String prefix: invalidPrefix) {
            if (parts[0].equalsIgnoreCase(prefix)) {
                return false;
            }
        }
        
        return true;
    }
    
    
    public TimeZonesSelectionModel(){
    	String[] zoneIds = TimeZone.getAvailableIDs();
        List<String> tmpIds = new ArrayList<String>();
        
        for (String id: zoneIds){
            if (isValidId(id)) {
                tmpIds.add(id);
            }
        }
        
        
        zones = new String[tmpIds.size()];
        
        for (int i=0;i<tmpIds.size();i++){
            zones[i] = tmpIds.get(i);
        }
        
        Arrays.sort(zones, 0, zones.length);
    }
    
    
    public TimeZonesSelectionModel(TimeZone timeZone){
        String[] zoneIds = TimeZone.getAvailableIDs();
        List<String> tmpIds = new ArrayList<String>();
        tmpIds.add("");
        
        for (String id: zoneIds){
            if (isValidId(id)) {
                tmpIds.add(id);
            }
        }
        
        zones = new String[tmpIds.size()];
        zones[0] = "System ("+timeZone.getID()+")";
        
        for (int i=1;i<tmpIds.size();i++){
            zones[i] = tmpIds.get(i);
        }
        
        Arrays.sort(zones, 1, zones.length);
    }
    
    @Override
    public String getLabel(int arg0) {
        return zones[arg0];
    }

    @Override
    public Object getOption(int arg0) {
        if (arg0 == 0){
        	int i = zones[0].indexOf('(');
        	int j = zones[0].indexOf(')');
        	
        	if ((i > 0) && (j>0)){
        		return zones[0].substring(i+1, j);
        	} 
        	return zones[arg0];
        }
        
        return zones[arg0];
    }
    
    @Override
    public int getOptionCount() {
        return zones.length;
    }

    @Override
    public String getValue(int arg0) {
    	if (arg0 == 0){
        	int i = zones[0].indexOf('(');
        	int j = zones[0].indexOf(')');
        	
        	if ((i > 0) && (j>0)){
        		return zones[0].substring(i+1, j);
        	} 
        	return zones[arg0];
        }
        return zones[arg0];
    }

    @Override
    public Object translateValue(String arg0) {
        return arg0;
    }
    
    @Override
    public boolean isDisabled(int arg0) {
        return false;
    }
}
