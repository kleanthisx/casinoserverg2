package com.magneta.administration.commons.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CountryBean;

public class CountrySelectionModel implements IPropertySelectionModel {

    private List<CountryBean> countries;

    public CountrySelectionModel(boolean showOnlyAcceptable) throws ServiceException {

        CountryService service = ServiceLocator.getService(CountryService.class);

        List<CountryBean> countryList = service.getCountries();

        if(countryList != null) {
            if (showOnlyAcceptable) {
                this.countries = new ArrayList<CountryBean>(100);

                for (CountryBean c: countryList) {

                    if (c.isEnabled()) {
                        this.countries.add(c);
                    }
                }

            } else {
                this.countries = countryList;
            }
        }
    }

    public CountrySelectionModel() throws ServiceException {
        this(false);
    }

    @Override
    public int getOptionCount() {
        if(countries == null) {
            return 0;
        }
        return countries.size();
    }

    @Override
    public Object getOption(int arg0) {
        if(countries == null) {
            return null;
        }
        CountryBean c = countries.get(arg0);

        return c.getCode();
    }

    @Override
    public String getLabel(int arg0) {
        if(countries == null) {
            return null;
        }
        CountryBean c = countries.get(arg0);

        return c.getName();
    }

    @Override
    public String getValue(int arg0) {
        if(countries == null) {
            return null;
        }
        CountryBean c = countries.get(arg0);

        return c.getCode();
    }

    @Override
    public Object translateValue(String value) {
        if ((value != null) && value.isEmpty()) {
            return null;
        }
        return value;
    }

    @Override
    public boolean isDisabled(int arg0) {
        if(countries == null) {
            return true;
        }
        CountryBean c = countries.get(arg0);

        return !c.isEnabled();
    }
}
