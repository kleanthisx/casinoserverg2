package com.magneta.tapestry4.callback;

import java.io.Serializable;

import org.apache.tapestry.IExternalPage;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.engine.ExternalServiceParameter;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;

/**
 * Defines a callback to an IExternalPage that is to be executed with
 * redirect-after-post
 * @author anarxia
 *
 */
public class RedirectExternalCallback implements Serializable, ICallback {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2551332685305352841L;
	private final String pageName;
	private final Object[] parameters;
	
	public RedirectExternalCallback(IRequestCycle cycle) {
		this(cycle.getPage().getPageName(), cycle.getListenerParameters());
	}
	
	public RedirectExternalCallback(IExternalPage page, Object[] parameters) {
		this(page.getPageName(), parameters);
	}

	public RedirectExternalCallback(String pageName, Object[] parameters) {
		this.pageName = pageName;
		this.parameters = parameters;
	}

	/**
	 * Performs the callback using a client-side redirect. This can be used to implement
	 * redirect-after-post.
	 * @param cycle
	 */
	@Override
	public void performCallback(IRequestCycle cycle) {
		IEngineService service =
				cycle.getInfrastructure().getServiceMap().getService(Tapestry.EXTERNAL_SERVICE);

		ILink link = service.getLink(false, new ExternalServiceParameter(pageName,
				parameters));
		
		throw new RedirectException(link.getURL());
	}
}
