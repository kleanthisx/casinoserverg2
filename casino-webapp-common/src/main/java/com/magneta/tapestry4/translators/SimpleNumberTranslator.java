/**
 * 
 */
package com.magneta.tapestry4.translators;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.apache.tapestry.form.translator.NumberTranslator;

/**
 * @author User
 *
 */
public class SimpleNumberTranslator extends NumberTranslator {
    
    public SimpleNumberTranslator() {
        super();
    }
    
    public SimpleNumberTranslator(String initializer) {
        super(initializer);
    }

    @Override
	public DecimalFormat getDecimalFormat(Locale locale) {
    	DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator('\0');
    	
        DecimalFormat df = new DecimalFormat(getPattern(), dfs);
        return df;
    }
    
    @Override
    protected Object getValueForEmptyInput() {
        return null;
    }
    
    @Override
	protected String defaultPattern() {
        return "#.##";
    }
}