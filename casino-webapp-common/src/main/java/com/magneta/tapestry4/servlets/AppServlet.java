/**
 * 
 */
package com.magneta.tapestry4.servlets;

import javax.servlet.ServletConfig;

import org.apache.hivemind.Registry;
import org.apache.tapestry.ApplicationServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;


/**
 * @author anarxia
 *
 */
public class AppServlet extends ApplicationServlet {

	private static final Logger log = LoggerFactory.getLogger(AppServlet.class);
	
    /**
     * 
     */
    private static final long serialVersionUID = -7466108712576854468L;
    
    @Override
    protected Registry constructRegistry(ServletConfig config) {
    	log.info("T4 AppServlet initialized");
        Registry registry = super.constructRegistry(config);
        ServiceLocator.setRegistry(registry);
        return registry;
    }
}
