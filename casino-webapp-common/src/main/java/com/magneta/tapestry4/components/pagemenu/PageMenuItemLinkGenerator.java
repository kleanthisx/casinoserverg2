package com.magneta.tapestry4.components.pagemenu;

import org.apache.tapestry.IDirect;
import org.apache.tapestry.IRequestCycle;

public interface PageMenuItemLinkGenerator {
	String getUrl(IRequestCycle cycle, IDirect menuComponent, PageMenuItem item);
}
