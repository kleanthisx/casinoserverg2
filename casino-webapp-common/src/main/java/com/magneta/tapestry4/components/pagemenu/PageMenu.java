/**
 * 
 */
package com.magneta.tapestry4.components.pagemenu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tapestry.AbstractComponent;
import org.apache.tapestry.IActionListener;
import org.apache.tapestry.IDirect;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.IScript;
import org.apache.tapestry.TapestryUtils;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectScript;

/**
 * @author User
 *
 */
public abstract class PageMenu extends AbstractComponent implements IDirect {

    @InjectScript("PageMenu.script")
    public abstract IScript getPageMenu();

    @InjectObject("service:t4.common.PageMenuItemLinkGenerator")
    public abstract PageMenuItemLinkGenerator getPageMenuItemLinkGenerator();

    public abstract List<PageMenuItem> getItems();

    public abstract int getMaxLevel();

    public abstract String getCssClass();

    public abstract String getBehaviorCssClass();
    
    public abstract String getActiveLinkCssClass();

    public abstract String getExpandedItemCssClass();

    public abstract String getCollapsedItemCssClass();
    
    public abstract String getSeperatorCssClass();

    @Override
    protected void renderComponent(IMarkupWriter writer, IRequestCycle cycle) {
        Map<String, Object> symbols = new HashMap<String, Object>();
        symbols.put("css", getBehaviorCssClass());
        symbols.put("id", getClientId());
        getPageMenu().execute(this, cycle, TapestryUtils.getPageRenderSupport(cycle, this), symbols);
        builtMenu(writer, cycle, null, getItems(), 0);
    }

    private void builtMenu(IMarkupWriter writer, IRequestCycle cycle, PageMenuItem parent, List<PageMenuItem> items, int level) {
        writer.begin("ul");

        String id = this.getClientId();
        if (parent != null){
            id += "_mnu_"+parent.getTargetPage();
        }
        writer.attribute("id",id);

        if (getCssClass() != null) {
            writer.attribute("class", getCssClass() + (level > 0 ? "_sub"+level : ""));
        }
        if (items != null) {
            for (PageMenuItem item: items) {
                writer.begin("li");

                if ("@seperator@".equals(item.getOption())){
                    if (getSeperatorCssClass() == null){
                        writer.attribute("class", "menu_seperator");
                    } else {
                        writer.attribute("class", getSeperatorCssClass());
                    }
                    if (item.getDescription() != null){
                        writer.print(item.getDescription());
                    } else {
                        writer.begin("hr");
                        writer.end("hr");
                    }
                } else if (item.getTargetPage() != null || item.getTargetURL() != null || item.getSpecialFunction() != null){
                    writer.attribute("id", id+"_mni_"+(item.getOption() != null ? item.getOption().replaceAll("\\p{Punct}|\\s", "_") : ""));

                    writer.begin("a");

                    if (item.getTargetURL() != null) {
                    	writer.attribute("href", item.getTargetURL());
                    } else {
                    	writer.attribute("href", getPageMenuItemLinkGenerator().getUrl(cycle, this, item));
                    }

                    if (item.isActive(cycle)){
                        writer.attribute("class", (getActiveLinkCssClass() == null ? "activeLink" : getActiveLinkCssClass()));
                    }

                    if (item.getOptionTitle() != null){
                        writer.attribute("title", item.getOptionTitle());
                    }
                    writer.print(item.getOption());

                    writer.end("a");
                } else {
                    writer.attribute("id", id+"_mni_"+(item.getOption() != null ? item.getOption().replaceAll("\\p{Punct}|\\s", "_") : ""));
                    writer.print(item.getOption());
                }

                if (item.getSubItems() != null && (getMaxLevel() < 0 || level < getMaxLevel())){
                    builtMenu(writer, cycle, item, item.getSubItems(), ++level);
                }

                level = 0;

                writer.end("li");
                writer.println();
            }
        }
        writer.end("ul");
    }

    @Override
    public void trigger(IRequestCycle cycle) {
        String function = (String)cycle.getListenerParameters()[0];
        IActionListener listener = cycle.getPage().getListeners().getListener(function);
        listener.actionTriggered(this, cycle);
    }
}
