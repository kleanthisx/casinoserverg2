/**
 * 
 */
package com.magneta.tapestry4.components.pagemenu;

/**
 * @author User
 *
 */
public interface PageMenuItemAccessValidator {
    
    public boolean checkAccess(String page);
    
    public boolean checkSpecial(String special);

	public boolean checkWriteAccess(String pageName);
}
