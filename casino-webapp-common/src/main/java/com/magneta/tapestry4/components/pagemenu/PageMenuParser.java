package com.magneta.tapestry4.components.pagemenu;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PageMenuParser {

	public static List<PageMenuItem> parseFile(InputStream is) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(is);

		if (doc.getFirstChild() == null){
			return null;
		}
		List<PageMenuItem> items = new ArrayList<PageMenuItem>();

		NodeList nodes = doc.getFirstChild().getChildNodes();

		for (int i = 0; i < nodes.getLength(); i++){
			Node item = nodes.item(i);

			if (!item.getNodeName().toLowerCase().equals("menuitem")){
				if (item.getNodeName().toLowerCase().equals("seperator")){
					PageMenuItem mnuItem = new PageMenuItem("@seperator@", null, null, null);
					mnuItem.setDescription(item.getTextContent());
					if (item.getAttributes() != null){
						Node n = item.getAttributes().getNamedItem("forpage");
						if (n != null){
							mnuItem.setTargetPage(n.getTextContent());
						}
					}
					items.add(mnuItem);
				} else {
					continue;
				}
			} else {
				Element menuItem = (Element)item;
				PageMenuItem mnuItem = new PageMenuItem(null, null, null, null);
				items.add(mnuItem);

				NodeList children = menuItem.getChildNodes();

				for (int j = 0; j < children.getLength(); j++){
					Node child = children.item(j);
					if (child.getNodeName().toLowerCase().equals("seperator")){
						PageMenuItem subItem = new PageMenuItem("@seperator@", child.getTextContent(), null, null);
						if (child.getAttributes() != null){
							Node n = child.getAttributes().getNamedItem("forpage");
							if (n != null){
								subItem.setTargetPage(n.getTextContent());
							}
						}
						mnuItem.addSubItem(subItem);
					} else if (child.getNodeName().toLowerCase().equals("description")){
						mnuItem.setDescription(child.getTextContent());
					} else if (child.getNodeName().toLowerCase().equals("page")){
						mnuItem.setTargetPage(child.getTextContent());
					} else if (child.getNodeName().toLowerCase().equals("special")){
						mnuItem.setSpecialFunction(child.getTextContent());
					} else if (child.getNodeName().toLowerCase().equals("message")){
						mnuItem.setOption(child.getTextContent());
					} else if (child.getNodeName().toLowerCase().equals("url")){
						mnuItem.setTargetURL(child.getTextContent());
					} else if (child.getNodeName().toLowerCase().equals("tooltip")){
						mnuItem.setOptionTitle(child.getTextContent());
					} else if (child.getNodeName().toLowerCase().equals("menuitem")){
						createItem((Element)child, mnuItem);
					} else if (child.getNodeName().toLowerCase().equals("params")){
						NodeList params = child.getChildNodes();
						String len = child.getAttributes().getNamedItem("length").getTextContent();

						if (len != null && len.matches("\\d")){
							Object[] vals = new Object[Integer.parseInt(len)];
							for (int k = 0; k < params.getLength(); k++){
								Node param = params.item(k);
								if (param.getNodeName().toLowerCase().equals("param")){

									String type = param.getAttributes().getNamedItem("type").getTextContent();
									String val = param.getTextContent();
									String index = param.getAttributes().getNamedItem("index").getTextContent();
									if (index != null && index.matches("\\d")){
										int pos = Integer.parseInt(index);
										if (pos >= 0 && pos < vals.length && val != null && val.length() > 0){
											if ("string".equals(type) || type == null){
												vals[pos] = new String(val);
											} else if ("integer".equals(type)){
												vals[pos] = Integer.parseInt(val);
											} else if ("double".equals(type)){
												vals[pos] = Double.parseDouble(val);
											} else if ("long".equals(type)){
												vals[pos] = Long.parseLong(val);
											}
										}
									}
								}
							}
							mnuItem.setParams(vals);
						}
					}
				}
			}
		}

		return items;
	}

	private static void createItem(Element menuItem, PageMenuItem parentMenu){
		PageMenuItem item = new PageMenuItem(null, null, null, null);
		parentMenu.addSubItem(item);
		NodeList children = menuItem.getChildNodes();

		for (int j = 0; j < children.getLength(); j++){
			Node child = children.item(j);
			if (child.getNodeName().toLowerCase().equals("seperator")){
				PageMenuItem subItem = new PageMenuItem("@seperator@", child.getTextContent(), null, null);
				if (child.getAttributes() != null){
					Node n = child.getAttributes().getNamedItem("forpage");
					if (n != null){
						subItem.setTargetPage(n.getTextContent());
					}
				}
				item.addSubItem(subItem);
			} else if (child.getNodeName().toLowerCase().equals("description")){
				item.setDescription(child.getTextContent());
			} else if (child.getNodeName().toLowerCase().equals("page")){
				item.setTargetPage(child.getTextContent());
			} else if (child.getNodeName().toLowerCase().equals("special")){
				item.setSpecialFunction(child.getTextContent());
			} else if (child.getNodeName().toLowerCase().equals("message")){
				item.setOption(child.getTextContent());
			} else if (child.getNodeName().toLowerCase().equals("url")){
				item.setTargetURL(child.getTextContent());
			} else if (child.getNodeName().toLowerCase().equals("tooltip")){
				item.setOptionTitle(child.getTextContent());
			} else if (child.getNodeName().toLowerCase().equals("menuitem")){
				createItem((Element)child, item);
			} else if (child.getNodeName().toLowerCase().equals("params")){
				NodeList params = child.getChildNodes();
				String len = child.getAttributes().getNamedItem("length").getTextContent();

				if (len != null && len.matches("\\d")){
					Object[] vals = new Object[Integer.parseInt(len)];
					for (int k = 0; k < params.getLength(); k++){
						Node param = params.item(k);
						if (param.getNodeName().toLowerCase().equals("param")){
							String type = param.getAttributes().getNamedItem("type").getTextContent();
							String val = param.getTextContent();
							String index = param.getAttributes().getNamedItem("index").getTextContent();
							if (index != null && index.matches("\\d")){
								int pos = Integer.parseInt(index);
								if (pos >= 0 && pos < vals.length && val != null && val.length() > 0){
									if ("string".equals(type) || type == null){
										vals[pos] = new String(val);
									} else if ("integer".equals(type)){
										vals[pos] = Integer.parseInt(val);
									} else if ("double".equals(type)){
										vals[pos] = Double.parseDouble(val);
									} else if ("long".equals(type)){
										vals[pos] = Long.parseLong(val);
									}
								}
							}
						}
					}
					item.setParams(vals);
				}
			}
		}
	}
}
