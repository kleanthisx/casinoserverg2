package com.magneta.tapestry4.components;

import org.apache.tapestry.IBinding;
import org.apache.tapestry.IComponent;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.binding.ExpressionBinding;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.ValidatorsBinding;
import org.apache.tapestry.valid.FieldLabel;

public abstract class ExFieldLabel extends FieldLabel {

    @Override
    protected void renderComponent(IMarkupWriter writer, IRequestCycle cycle)
    {
        super.renderComponent(writer, cycle);
        if (getShowReqIndicator() && isRequiredField(getField())){
            writer.begin("span");
            writer.attribute("class", "reqFieldIndicator");
            writer.print("*");
            writer.end();
        }
    }
    
    public abstract boolean getShowReqIndicator();

    private boolean isRequiredField(IComponent comp) {
    	if (comp == null) {
    		return false;
    	}

    	IBinding val = comp.getBinding("validators");
    	if (val == null) {
    		return false;
    	}

    	if(val instanceof ExpressionBinding) {
    		ExpressionBinding expressionBinding = (ExpressionBinding)val;
    		return expressionBinding.getObject() instanceof Required;                                                              
    	} else if (val instanceof ValidatorsBinding) {
    		ValidatorsBinding vb = (ValidatorsBinding)val;
    		Iterable<?> collection = (Iterable<?>)vb.getObject();
    		for(Object v: collection) {
    			if(v instanceof Required) {
    				return true;
    			}
    		}
    	}

    	return false;
    }
}
