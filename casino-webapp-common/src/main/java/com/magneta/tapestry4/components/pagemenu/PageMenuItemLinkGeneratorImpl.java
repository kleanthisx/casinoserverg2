package com.magneta.tapestry4.components.pagemenu;

import org.apache.tapestry.IDirect;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.engine.DirectServiceParameter;
import org.apache.tapestry.engine.ExternalServiceParameter;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;

public class PageMenuItemLinkGeneratorImpl implements PageMenuItemLinkGenerator {

	@Override
	public String getUrl(IRequestCycle cycle, IDirect menuComponent, PageMenuItem item) {
    	if (item.getTargetPage() != null) {
            String url = null;
            if (item.getParams() == null){
            	IEngineService s = cycle.getInfrastructure().getServiceMap().getService(Tapestry.EXTERNAL_SERVICE);
                ExternalServiceParameter parameter = new ExternalServiceParameter(item.getTargetPage());
                ILink link = s.getLink(false, parameter);
                url = link.getURL();
            } else {
                IEngineService s = cycle.getInfrastructure().getServiceMap().getService(Tapestry.EXTERNAL_SERVICE);
                ExternalServiceParameter parameter = new ExternalServiceParameter(item.getTargetPage(), item.getParams());
                ILink link = s.getLink(false, parameter);
                url = link.getURL();
            }
            return url;
        } else if ("logout".equals(item.getSpecialFunction())) {
            IEngineService s = cycle.getInfrastructure().getServiceMap().getService(Tapestry.DIRECT_SERVICE);
            return s.getLink(true, new DirectServiceParameter(menuComponent, new Object[]{"logout"})).getURL();
        }
    	
    	return null;
	}
}
