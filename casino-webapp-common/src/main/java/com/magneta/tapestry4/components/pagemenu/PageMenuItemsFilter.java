/**
 * 
 */
package com.magneta.tapestry4.components.pagemenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author User
 *
 */
public class PageMenuItemsFilter {

    private PageMenuItemAccessValidator validator;
    
    public PageMenuItemsFilter(PageMenuItemAccessValidator validator){
        this.validator = validator;
    }
    
    private boolean allowAccess(PageMenuItem item) {
        if (validator == null)
            return true;
        
        if (item.getTargetPage() != null && !item.getTargetPage().trim().isEmpty()) {
            return validator.checkAccess(item.getTargetPage());
        } else if (item.getSpecialFunction() != null && !item.getSpecialFunction().trim().isEmpty()) {
            return validator.checkSpecial(item.getSpecialFunction());
        }
        
        return true;
    }
    
    public List<PageMenuItem> filterItems(List<PageMenuItem> srcMenuItems, Locale locale, String msgsBaseFileName){
        List<PageMenuItem> items = new ArrayList<PageMenuItem>();
        
        ResourceBundle msgs = ResourceBundle.getBundle(msgsBaseFileName, locale);

        for (PageMenuItem item: srcMenuItems){

            if (!allowAccess(item))
                continue;
            
            if ("@seperator@".equals(item.getOption())) {

                PageMenuItem newItem = new PageMenuItem(item);
                items.add(newItem);

            } else {
                PageMenuItem newItem = new PageMenuItem(item);

                items.add(newItem);

                if (msgs != null && newItem.getOption() != null){
                    if (msgs.containsKey(newItem.getOption())){
                        newItem.setOption(msgs.getString(newItem.getOption()));
                    } else if (newItem.getDescription() != null){
                        newItem.setOption(newItem.getDescription());
                    } else {
                        newItem.setOption("["+newItem.getOption()+"]");
                    }
                } else {
                    newItem.setOption(newItem.getDescription());
                }

                if (item.getSubItems() != null){
                    filterMenu(newItem, item.getSubItems(), msgs);
                }
            }
        }

        return items;
    }

    
    
    private void filterMenu(PageMenuItem parent, List<PageMenuItem> items, ResourceBundle msgs){
        for (PageMenuItem item: items){

            if (!allowAccess(item))
                continue;
            
            if ("@seperator@".equals(item.getOption())) {
                PageMenuItem newItem = new PageMenuItem(item);
                parent.addSubItem(newItem);
              
            } else {
                PageMenuItem newItem = new PageMenuItem(item);

                parent.addSubItem(newItem);

                if (msgs != null && newItem.getOption() != null){
                    if (msgs.containsKey(newItem.getOption())){
                        newItem.setOption(msgs.getString(newItem.getOption()));
                    } else if (newItem.getDescription() != null){
                        newItem.setOption(newItem.getDescription());
                    } else {
                        newItem.setOption("["+newItem.getOption()+"]");
                    }
                } else {
                    newItem.setOption(newItem.getDescription());
                }

                if (item.getSubItems() != null){
                    filterMenu(newItem, item.getSubItems(), msgs);
                }
            }
        }
    }
}
