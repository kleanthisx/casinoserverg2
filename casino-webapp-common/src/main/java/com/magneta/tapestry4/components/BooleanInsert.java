/**
 * 
 */
package com.magneta.tapestry4.components;

import org.apache.tapestry.AbstractComponent;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;

/**
 * @author User
 *
 */
public abstract class BooleanInsert extends AbstractComponent {
 
    @Override
    protected void renderComponent(IMarkupWriter writer, IRequestCycle cycle) {
        Boolean value = getValue();
        
        if (value == null)
            return;
        
        
        if (value) {
            writer.print(this.getMessages().getMessage("Yes"));
        } else {
            writer.print(this.getMessages().getMessage("No"));
        }
    }

    public abstract Boolean getValue();
}
