package com.magneta.tapestry4.components.pagemenu;

import java.util.ArrayList;
import java.util.List;
import org.apache.tapestry.IPage;
import org.apache.tapestry.IRequestCycle;

public class PageMenuItem {
    
    private String option;
    private String targetPage;
    private String optionTitle;
    private String targetURL;
    private String specialFunction;
    private String description;
    private List<PageMenuItem> subItems;
    private Object[] params;
    
    public PageMenuItem(PageMenuItem item) {
        this.option = item.option;
        this.targetPage = item.targetPage;
        this.targetURL = item.targetURL;
        this.specialFunction = item.specialFunction;
        this.optionTitle = item.optionTitle;
        this.description = item.description;
        this.params = item.params;
        this.subItems = null;
    }
    
    public PageMenuItem(String option, String targetPage, String specialFunction, String targetURL) {
        this(option, targetPage, targetURL, specialFunction, null);
    }
    

    public PageMenuItem(String option, String targetPage, String specialFunction, String targetURL, String optionTitle) {
        this.option = option;
        this.targetPage = targetPage;
        this.specialFunction = specialFunction;
        this.optionTitle = optionTitle;
        this.description = null;
        this.subItems = null;
    }
    
    public String getSpecialFunction() {
        return specialFunction;
    }
    
    public void setSpecialFunction(String specialFunction) {
        this.specialFunction = specialFunction;
    }

    public String getTargetURL() {
        return targetURL;
    }
    
    public void setTargetURL(String targetURL) {
        this.targetURL = targetURL;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
        
    public String getTargetPage() {
        return targetPage;
    }

    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
    
    public void addSubItem(PageMenuItem item){
        if (subItems == null){
            subItems = new ArrayList<PageMenuItem>();
        }
        subItems.add(item);
    }

    public List<PageMenuItem> getSubItems() {
        return subItems;
    }
    
    public boolean checkExpand(IRequestCycle cycle){
    	String currPageName = cycle.getPage().getPageName();

    	if (currPageName.equals(targetPage)) {
    		return true;
    	} 
    	
    	if (subItems != null) {
    		for (PageMenuItem item: subItems) {
    			if (currPageName.equals(item.getTargetPage())) {
    				return true;
    			}
    		}
    	}

    	return false;
    }
    
    public boolean isActive(IRequestCycle cycle){
        IPage currPage = cycle.getPage();
        return (this.params == null) && (currPage.getPageName().equals(targetPage));
    }
    
    public String getOptionTitle() {
        return optionTitle;
    }
    
    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}