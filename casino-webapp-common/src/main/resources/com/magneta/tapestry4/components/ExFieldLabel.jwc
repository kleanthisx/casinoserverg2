<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE component-specification PUBLIC
  "-//Apache Software Foundation//Tapestry Specification 4.0//EN" 
  "http://tapestry.apache.org/dtd/Tapestry_4_0.dtd">
	
<component-specification class="com.magneta.tapestry4.components.ExFieldLabel" allow-body="no" allow-informal-parameters="yes">
  
  <description>
  	An extended field label which also renders a required field indicator (*).
  </description>

  <parameter name="field" required="yes"/>

  <parameter name="displayName">
    <description>
      Optional.  Defaults to the displayName of the associated field.
      Used to override the field's displayName, or when there is no
      field.
    </description>
  </parameter>

  <parameter name="raw">
    <description>
      If false (the default), then HTML characters in the value are escaped.  If
      true, then value is emitted exactly as is.
    </description>
  </parameter>
  
  <parameter name="prerender" default-value="true">
    <description>
      If true (the default), then the field (if any) will be pre-rendered by the FieldLabel.
      This is useful when the FieldLabel and field are inside a loop and the FieldLabel
      precedes the field.  This parameter should be set to false if the FieldLabel occurs
      after the field.
    </description>
  </parameter>
  
  <parameter name="showReqIndicator" default-value="true">
    <description>
      If true (the default), then the required field indicator is rendered if the field is required.
    </description>
  </parameter>
  
  <reserved-parameter name="for"/>

</component-specification>
