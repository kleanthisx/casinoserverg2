package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.RoleService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.RoleBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class RoleServiceImpl implements RoleService {

	private final PrincipalService principalService;
	
	public RoleServiceImpl(PrincipalService principalService) {
		this.principalService = principalService;
	}
	
	@Override
	public RoleBean getRole(Integer roleId) throws ServiceException {
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, false)) {
			throw new AccessDeniedException("Access denied");
		}
		
		SearchContext<RoleBean> searchContext = FiqlContextBuilder.create(RoleBean.class, "roleId==?", roleId); 
		
		return new SqlBuilder<RoleBean>(RoleBean.class).findBean(searchContext, null);
	}

	@Override
	public ServiceResultSet<RoleBean> getRoles(
			SearchContext<RoleBean> searchContext, int limit, int offset,
			SortContext<RoleBean> sortContext) throws ServiceException {
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, false)) {
			throw new AccessDeniedException();
		}
		
		return new SqlBuilder<RoleBean>(RoleBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public void createRole(RoleBean roleBean) throws ServiceException {
		if (roleBean == null) {
			throw new InvalidParameterException("RoleBean is null");
		}

		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException();
		}
		
		new SqlBuilder<RoleBean>(RoleBean.class).insertBean(roleBean);
	}

	@Override
	public void updateRole(RoleBean roleBean) throws ServiceException {
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException();
		}

		if (roleBean == null) {
			throw new InvalidParameterException("RoleBean is null");
		}
		
		if (roleBean.getRoleId() == null) {
			throw new InvalidParameterException("RoleId is null");
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		PreparedStatement stmt = null;
		
		try {
			stmt = conn.prepareStatement("UPDATE roles SET role_desc=? WHERE role_id=?");
			
			stmt.setString(1, roleBean.getDescription());
			stmt.setInt(2, roleBean.getRoleId());
			
		} catch (SQLException e) {
			throw new DatabaseException("Database error while updateing role", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
	}

}
