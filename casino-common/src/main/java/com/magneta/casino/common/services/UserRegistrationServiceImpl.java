package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.TimeZone;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.UsersBlacklistService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.UserBlacklistBean;
import com.magneta.casino.services.beans.UserRegistrationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserAffiliationEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.AccountLevelViolation;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateNicknameException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidAuthenticationCredentialsException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.exceptions.InvalidUserInformation;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.security.Password;

public class UserRegistrationServiceImpl implements UserRegistrationService {


	private final UsersBlacklistService blackListsService;
	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	private final CorporatesService corporatesService;

	public UserRegistrationServiceImpl(PrincipalService principalService,
			UsersBlacklistService blackListsService,
			UserAccessService userAccessService,
			CorporatesService corporatesService) {

		this.principalService = principalService;
		this.blackListsService = blackListsService;
		this.userAccessService = userAccessService;
		this.corporatesService = corporatesService;
	}

	@Override
	public void updatePassword(Long userId,String newPassword) throws ServiceException {
		if (newPassword == null || newPassword.length() < 6 ) {
			throw new InvalidParameterException("new password size must be greater that 6 characters");
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		if (!userPrincipal.isSystem()) {
			if (!userPrincipal.getUserId().equals(userId)) {
				switch (userPrincipal.getType()) {
				case STAFF_USER:
					if (!userPrincipal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
						throw new AccessDeniedException();
					}
					break;
				case CORPORATE_ACCOUNT:
					break;
				default:
					throw new AccessDeniedException();
				}
			}
		}

		final String salt = Password.getSalt();
		String passHash = Password.getPasswordHash(newPassword, salt);
		if(passHash == null || passHash.isEmpty()) {
			throw new ServiceException("Passhash not generated");
		}

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;

		if (conn == null){
			throw new DatabaseException("Database Error");
		}

		try {
			String sql=
					"UPDATE users SET password_hash=?, password_salt=?" +
							" WHERE user_id=?";

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, passHash);
			stmt.setString(2, salt);
			stmt.setLong(3, userId);


			if(stmt.executeUpdate() != 1) {
				throw new EntityNotFoundException("User not found");
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL Query Error", e);
		} finally {         
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}


	private boolean allowedUserType(ExtendedPrincipalBean userPrincipal, UserTypeEnum type) {
		if (userPrincipal.isSystem()) {
			return true;
		}

		switch(userPrincipal.getType()) {
		case STAFF_USER:
			if (!userPrincipal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
				return false;
			}
			return true;
		case CORPORATE_ACCOUNT:
			return type == UserTypeEnum.CORPORATE_ACCOUNT || type == UserTypeEnum.CORPORATE_PLAYER;
		default:
			return false;
		}
	}

	@Override
	public Long registerUser(UserRegistrationBean user) throws ServiceException {
		if (user == null || user.getUserName() == null) {
			throw new InvalidParameterException("Username is required");
		}

		registerUser(user, null);

		return user.getId();
	}

	@Override
	public void registerCorporate(UserRegistrationBean user, CorporateBean corporateInfo) throws ServiceException {
		if (user == null || user.getUserName() == null) {
			throw new InvalidParameterException("Username is required");
		}

		registerUser(user, corporateInfo);		
	}
	
	
	private void validateCorporateRegistration(UserRegistrationBean user, CorporateBean corporateInfo, ExtendedPrincipalBean principal) throws ServiceException {
		CorporateBean creator = null;
		if (principal.getType() == UserTypeEnum.CORPORATE_ACCOUNT) {
			creator = corporatesService.getCorporate(user.getCorporateId());
		}

		/* Enforce allowed sub-levels. The creator can create corporate sub accounts upto sub-levels.
		 * null means unlimited.
		 */
		if (creator != null) {
			Integer subLevels = creator.getSubLevels();
			if (subLevels != null) {
				if (subLevels < 1 || (subLevels == 1 && !user.getCorporateId().equals(creator))) {
					throw new AccountLevelViolation("Can only create accounts up to "  + subLevels + " levels");
				}

				if (user.getCorporateId().equals(creator)) {
					
					/* Make sure sublevels are not greater than what the creator is allowed */
					if (corporateInfo.getSubLevels() == null || corporateInfo.getSubLevels() > subLevels - 1) {
						corporateInfo.setSubLevels(subLevels - 1);
					}
					
					if (corporateInfo.getAffiliateRate() > creator.getAffiliateRate()) {
						throw new InvalidParameterException("Commission rate cannot be higher than master account");
					}
					
					if (creator.getAllowFreeCredits() == false && corporateInfo.getAllowFreeCredits() == true) {
						corporateInfo.setAllowFreeCredits(false);
					}
					
					if (creator.getAllowFullFreeCredits() == false && corporateInfo.getAllowFullFreeCredits() == true) {
						corporateInfo.setAllowFullFreeCredits(false);
					}
					
				} else {
					int levelsRemaining = subLevels - 1;
					List<Long> corporates = corporatesService.getCorporateHierarchy(user.getCorporateId());
					boolean found = false;

					for (int i=0; i < levelsRemaining && i < corporates.size(); i++) {
						if (corporates.get(i).equals(creator)) {
							found = true;
							break;
						}
					}

					if (!found) {
						throw new AccountLevelViolation("Can only create accounts up to "  + subLevels + " levels");
					}	
				}
			}
		}
		
		/* Set approve flag */
		boolean approved = false;

		if (principal.hasPrivilege(PrivilegeEnum.CORPORATE, true)) {
			approved = true;
		} else if (creator != null) {
			approved = !creator.getRequiresApproval() && !creator.getRequiredSuperApproval();
			
			if (creator.getRequiresApproval()) {
				corporateInfo.setRequiresApproval(true);
			}
			
			if (creator.getRequiredSuperApproval()) {
				corporateInfo.setRequiredSuperApproval(true);
			}
		}

		corporateInfo.setApproved(approved);
	}
	

	private void registerUser(UserRegistrationBean user, CorporateBean corporateInfo) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		/* Trim the username from leading and trailing spaces */
		user.setUserName(user.getUserName().trim());

		if (user.getUserName().isEmpty()) {
			throw new InvalidParameterException("Username is required");
		}

		if (user.getUserName().contains("*")) {
			throw new InvalidUserInformation("Invalid character * in username");
		}

		if (user.getUserType() == null) {
			throw new InvalidParameterException("User type is required");
		}

		if (corporateInfo != null && user.getUserTypeEnum() != UserTypeEnum.CORPORATE_ACCOUNT
				|| corporateInfo == null && user.getUserTypeEnum() == UserTypeEnum.CORPORATE_ACCOUNT) {
			throw new InvalidParameterException("Invalid user type");
		}

		if (!allowedUserType(userPrincipal, user.getUserTypeEnum())) {
			throw new AccessDeniedException();
		}

		final String salt = Password.getSalt();
		final String passHash = Password.getPasswordHash(user.getPassword(), salt);	

		/* Validation for corporate hierarchy */
		if(userPrincipal.getType() == UserTypeEnum.CORPORATE_ACCOUNT) {
			if (user.getCorporateId() == null) {
				throw new InvalidParameterException("Corporate account but corporate is not set");
			}
		}

		if (user.getUserTypeEnum() == UserTypeEnum.CORPORATE_PLAYER && user.getCorporateId() == null) {
			throw new InvalidParameterException("Corporate account but corporate is not set");
		}

		if (user.getCorporateId() != null && !userAccessService.canAccess(user.getCorporateId())) {
			throw new AccessDeniedException();
		}

		//set default timezone if not given
		if(user.getTimezone() == null) {
			user.setTimezone(TimeZone.getDefault().getID());			
		}

		UserBlacklistBean blacklistBean = blackListsService.isUserBlackList(user.getUserName(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getCountryCode());
		if(blacklistBean != null) {
			throw new InvalidUserInformation();
		}

		if (corporateInfo != null) {
			validateCorporateRegistration(user, corporateInfo, userPrincipal);
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Unable to get database connection");
		}

		/* Check blacklist */
		try { 
			conn.setAutoCommit(false);

			PreparedStatement stmt = null;
			ResultSet rs = null;
			try {
				stmt = conn.prepareStatement(
						"INSERT INTO users (username, password_hash, password_salt, email, first_name,"+
								" middle_name, last_name, postal_address, country, phone, region, town, postal_code, nickname, time_zone,"+
								" user_type,is_active)"+
								" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
						" RETURNING user_id" );

				stmt.setString(1, user.getUserName());
				stmt.setString(2, passHash);
				stmt.setString(3, salt);
				stmt.setString(4, user.getEmail());
				stmt.setString(5, user.getFirstName());
				stmt.setString(6, user.getMiddleName());
				stmt.setString(7, user.getLastName());
				stmt.setString(8, user.getPostalAddress());
				stmt.setString(9, user.getCountryCode());
				stmt.setString(10, user.getPhone());
				stmt.setString(11, user.getRegion());
				stmt.setString(12, user.getTown());
				stmt.setString(13, user.getPostalCode());
				stmt.setString(14, user.getNickName());
				stmt.setString(15, user.getTimezone());
				stmt.setInt(16, user.getUserType());
				stmt.setBoolean(17, true);

				rs = stmt.executeQuery();

				if (rs.next()) {
					user.setId(rs.getLong(1));
				}
			} finally {
				DbUtil.close(rs);
				DbUtil.close(stmt);
			}

			if (corporateInfo != null) {
				PreparedStatement stmt4 = null;
				try {
					stmt4 = conn.prepareStatement(
							"INSERT INTO affiliates(affiliate_id, affiliate_rate," +
									" payment_period, approved, allow_free_credits, free_credits_rate," +
									" allow_full_free_credits, require_approval, sub_levels, requires_super_approval)" +
							" VALUES (?,?,CAST (? AS INTERVAL),?,?,?,?,?,?,?)");

					stmt4.setLong(1, user.getId());

					stmt4.setDouble(2, corporateInfo.getAffiliateRate());
					stmt4.setString(3, corporateInfo.getPaymentPeriod());
					stmt4.setBoolean(4, corporateInfo.getApproved());

					DbUtil.setBoolean(stmt4, 5, corporateInfo.getAllowFreeCredits());
					DbUtil.setDouble(stmt4, 6, corporateInfo.getFreeCreditsRate()); //also zero instead of null
					DbUtil.setBoolean(stmt4, 7, corporateInfo.getAllowFullFreeCredits());
					DbUtil.setBoolean(stmt4, 8, corporateInfo.getRequiresApproval());
					DbUtil.setInteger(stmt4, 9, corporateInfo.getSubLevels());
					DbUtil.setBoolean(stmt4, 10, corporateInfo.getRequiredSuperApproval());

					stmt4.execute();

				} finally {
					DbUtil.close(stmt4);
				}
			}

			/* Corporate hierarchy */
			if (user.getCorporateId() != null) {
				PreparedStatement stmt4 = null;
				try {
					stmt4 = conn.prepareStatement(
							"INSERT INTO affiliate_users(affiliate_id, user_id, affiliation)" +
							" VALUES (?,?,?)");
					stmt4.setLong(1, user.getCorporateId());
					stmt4.setLong(2, user.getId());

					if (user.getUserTypeEnum() == UserTypeEnum.CORPORATE_PLAYER) {
						stmt4.setInt(3, UserAffiliationEnum.USER_AFFILIATION.getId()); /* User Affiliation */
					} else {
						stmt4.setInt(3, UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId());
					}

					stmt4.execute();
				} finally {
					DbUtil.close(stmt4);
				}
			}

			conn.commit();
		} catch (SQLException e) {
			user.setId(null);
			try {
				conn.rollback();
			} catch (SQLException e1) {

			}
			/* FIXME: Hack. Find proper way to detect constraint */
			if (e.getMessage().contains("_username")) {
				throw new DuplicateUsernameException();
			} else if (e.getMessage().contains("_nickname")) {
				throw new DuplicateNicknameException();
			} else if (e.getMessage().contains("_email")) {
				throw new DuplicateEmailException();
			} else {
				throw new DatabaseException("Database Error", e);
			}

		} finally {
			DbUtil.close(conn);
		}
	}

	@Override
	public void resetPassword(Long userId, String oldPassword,String newPassword) throws ServiceException  {
		if(userId == null) {
			throw new InvalidParameterException("Userid is required");
		}
		if(oldPassword == null || oldPassword.isEmpty()) {
			throw new InvalidParameterException("Old password cannot be empty");
		}
		if(newPassword == null || newPassword.isEmpty()) {
			throw new InvalidParameterException("new password cannot be empty");
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		//Old password is wrong;
		if(!this.verifyPassword(userId, oldPassword)) {
			throw new InvalidAuthenticationCredentialsException("Old password is wrong");
		}

		this.updatePassword(userId, newPassword);
	}

	@Override
	public boolean verifyPassword(Long userId, String password) throws ServiceException {
		if(userId == null || password == null || password.isEmpty()) {
			throw new InvalidParameterException("Parameters passed not correct");
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get a connection to database to verify password");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT password_hash, password_salt FROM users WHERE user_id = ?");
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, userId);
			rs = stmt.executeQuery();
			if(!rs.next()) {
				return false;
			}
			String dbHash = rs.getString("password_hash");
			String salt = rs.getString("password_salt");
			String hash = Password.getPasswordHash(password, salt);
			if (hash.equals(dbHash)) {
				return true;
			}

		} catch (Exception e) {
			throw new DatabaseException("SQL query error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return false;
	}
}
