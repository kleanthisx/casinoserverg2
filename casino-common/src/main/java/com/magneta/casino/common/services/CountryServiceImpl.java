package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchCondition;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.CollectionUtils;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class CountryServiceImpl implements CountryService {

	private static final long TIMEOUT = 5 * 60 * 1000;

	private final PrincipalService principalService;
	
	private List<CountryBean> countries;
	private Map<String,CountryBean> countryTable;
	private long lastUpdate;

	public CountryServiceImpl(PrincipalService principalService) {
		countries = null;
		countryTable = null;
		lastUpdate = 0;
		this.principalService = principalService;
	}

	/* Check and refresh list */
	private synchronized void checkRefresh() throws ServiceException {
		if (System.currentTimeMillis() - lastUpdate < TIMEOUT) {
			return;
		}
		
		List<CountryBean> countries = new SqlBuilder<CountryBean>(CountryBean.class).findBeans(null, null, new SortContext<CountryBean>("name"));
		Map<String, CountryBean> countryTable = new HashMap<String, CountryBean>();

		if (countries != null) {
			this.lastUpdate = System.currentTimeMillis();
			this.countries = countries;

			for (CountryBean c: countries) {
				countryTable.put(c.getCode(), c);
			}
			this.countryTable = countryTable;
		}
	}

	@Override
	public List<CountryBean> getCountries() throws ServiceException {
		checkRefresh();

		List<CountryBean> origList = this.countries;
		if (origList == null) {
			throw new ServiceException("countries are null");
		}
		
		List<CountryBean> newList = new ArrayList<CountryBean>(origList.size());
		
		for (CountryBean c: origList) {
			newList.add((CountryBean)c.clone());
		}
		
		return newList;
	}

	@Override
	public CountryBean getCountry(String code) throws ServiceException  {
		checkRefresh();

		if (this.countryTable == null) {
			throw new ServiceException("countryTable is null");
		}

		CountryBean c = this.countryTable.get(code);
		
		if (c == null) {
			return null;
		}
		
		return (CountryBean)c.clone();
	}

	@Override
	public void update(CountryBean country) throws ServiceException {
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;

		if (dbConn == null){
			throw new DatabaseException("Error connecting to database");
		}

		try
		{   
			String sql = 
				"UPDATE countries SET accept = ?, country_name = ? WHERE country_code = ?";

			statement = dbConn.prepareStatement(sql);
			statement.setBoolean(1, country.isEnabled());
			statement.setString(2, country.getName());
			statement.setString(3, country.getCode());

			if(statement.executeUpdate() != 1) {
				throw new EntityNotFoundException("Could not update country with countryCode:" + country.getCode());
			}
			
			/* Update country in our cache */
			CountryBean orig = getCountry(country.getCode());
			orig.setEnabled(country.isEnabled());
			orig.setName(country.getName());
			
		} catch (SQLException e) {
			throw new DatabaseException("SQL Exception", e);
		} finally{
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
	}

	@Override
	public ServiceResultSet<CountryBean> getCountries(int limit, int offset)  throws ServiceException {
		return this.getCountries(null,null,limit,offset);
	}

	@Override
	public ServiceResultSet<CountryBean> getCountries(SearchContext<CountryBean> searchContext,SortContext<CountryBean> sortContext, int limit, int offset)throws ServiceException {
		checkRefresh();
		List<CountryBean> copyList = CollectionUtils.copyList(countries);
		
		if(offset > copyList.size())  {
			throw new InvalidParameterException("offset cannot be bigger than winners size:" + Long.toString(copyList.size()));
		}
		
		if(searchContext != null) {
			SearchCondition<CountryBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}
		
		if(sortContext != null) {
			sortContext.sortList(CountryBean.class, copyList);
		}		
		
		List<CountryBean> beans = new ArrayList<CountryBean>();
		
		ListIterator<CountryBean> iter = copyList.listIterator(offset);
		int myLimit = limit;
		
		if(myLimit != 0) {
			myLimit++;
		}
		while (iter.hasNext() && (myLimit == 0 || beans.size() < myLimit)) {
			beans.add((CountryBean)iter.next().clone());
		}
		
		ServiceResultSet<CountryBean> res = new ServiceResultSet<CountryBean>();
		res.setResult(beans);
		
		if (offset > 0) {
			res.setHasPrev(true);
		}
		
		if (myLimit > 0 && beans.size() > limit) {
			res.setHasNext(true);
			beans.remove(beans.size() - 1);
		}
		
		return res;
	}

	@Override
	public int getCountriesSize(SearchContext<CountryBean> searchContext)throws ServiceException {
		checkRefresh();
		List<CountryBean> copyList = CollectionUtils.copyList(countries);
		
		
		if(searchContext != null) {
			SearchCondition<CountryBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}
		return copyList.size();
	}

}
