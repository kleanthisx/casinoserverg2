package com.magneta.casino.common.utils;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.magneta.casino.reports.ReportUtil;

public class FormatUtils {

    /**
     * Formats the given double amount into a string which truncates all but the two last decimals.
     * @param amount The amount to format.
     * @return The formatted amount.
     */
	@Deprecated
	public static String getFormattedAmount(Double amount) {		
		return ReportUtil.getFormattedAmount(amount, Locale.getDefault());
    }
    
    /**
     * Formats the given date with the given time zone into a string with the date for the given time zone and locale. 
     * @param date The date to format.
     * @param sourceZone The time zone of the date.
     * @param destZone The time zone to format the date in.
     * @param locale The locale for the formatted date.
     * @return A string with the formatted date.
     */
	@Deprecated
    public static String getFormattedDate(Date date, TimeZone destZone, Locale locale){     
        return ReportUtil.getFormattedDate(date, destZone, locale);
    }
    
    public static String getDuration(Integer secs) {
    	if (secs == null) {
    		return "";
    	}
        StringBuilder buf = new StringBuilder();
        
        int days = secs / (60 * 60 * 24);
        secs -= days * (60 * 60 * 24);
        
        if (days > 0) {
            buf.append(Integer.valueOf(days));
            buf.append(" days ");
        }
        
        int hours = secs / (60 * 60);
        secs -= hours * (60 * 60);
        
        if (hours > 0 || days > 0) {
            buf.append(Integer.valueOf(hours));
            buf.append(" hours ");
        }
        
        int mins = secs / 60;
        secs -= mins * 60;
        
        buf.append(Integer.valueOf(mins));
        buf.append(" minutes "); 
        
        buf.append(Integer.valueOf(secs));
        buf.append(" secs ");     
        
        return buf.toString().trim();
    }
}

