package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchCondition;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.configuration.GameConfigLoadingException;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.GameConfigurationFactory;

public class GameConfigsServiceImpl implements GameConfigsService {
	
	private static final Logger log = LoggerFactory.getLogger(GameConfigsServiceImpl.class);
	
	private static final long TIMEOUT = 5 * 60 * 1000;

	private long lastUpdate;
	private Map<Long,GameConfigBean> configs;
	private Map<Integer,GameConfigBean> defaultConfigs;
	private Map<Integer,List<Long>> gameConfigs;
	
	private final GamesService gamesService;
	private final PrincipalService principalService;
	
	public GameConfigsServiceImpl(PrincipalService principalService, GamesService gamesService) {
		configs = new HashMap<Long,GameConfigBean>();
		gameConfigs = new HashMap<Integer,List<Long>>();
		defaultConfigs = new HashMap<Integer,GameConfigBean>();
		
		this.gamesService = gamesService;
		this.principalService = principalService;
		lastUpdate = 0;
	}

	private synchronized void checkUpdate() throws ServiceException {
		long now = System.currentTimeMillis();

		if (now - lastUpdate < TIMEOUT)
			return;

		List<GameBean> games = this.gamesService.getGames();
		for(GameBean game:games) {
			if (defaultConfigs.get(game.getGameId()) != null) {
				continue;
			}

			GameConfiguration config = null;
			try {
				config = GameConfigurationFactory.loadConfiguration(game.getGameTemplate());
			} catch (GameConfigLoadingException e) {
				log.warn("Unable to load default configuration for game " + game.getGameId());
				continue;
			}
			
			if (config == null)
				continue;
			
			String value;
			try {
				value = config.serialize();
			} catch (JAXBException e) {
				log.error("Unable to serialize default config", e);
				continue;
			}
				
			GameConfigBean bean = new GameConfigBean();
			bean.setConfigValue(value);
			bean.setConfigId(0L);
			bean.setConfigComment("Default Configuration");
			bean.setGameId(game.getGameId());		
			defaultConfigs.put(game.getGameId(), bean);
		}

		Connection conn = ConnectionFactory.getReportsConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get connection to get games");
		}

		String sql =
			"SELECT * FROM game_configs";

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.prepareStatement(sql);

			rs = stmt.executeQuery();
			Map<Integer,List<Long>> tmpGameConfigs = new HashMap<Integer,List<Long>>();
			
			while(rs.next()) {
				GameConfigBean bean = new GameConfigBean();
				bean.setConfigId(DbUtil.getLong(rs, "config_id"));
				bean.setGameId(DbUtil.getInteger(rs, "game_id"));
				bean.setConfigValue(rs.getString("config_value"));
				bean.setConfigComment(rs.getString("config_comment"));
				bean.setConfigDate(DbUtil.getTimestamp(rs, "config_date"));
				bean.setCreatorId(DbUtil.getLong(rs, "modified_by"));
				configs.put(bean.getConfigId(),bean);

				List<Long> list = tmpGameConfigs.get(bean.getGameId());
				if(list == null) {
					list = new ArrayList<Long>();
				}
				list.add(bean.getConfigId());
				tmpGameConfigs.put(bean.getGameId(), list);
			}

			this.gameConfigs = tmpGameConfigs;
			
		} catch (SQLException e) {
			throw new DatabaseException("SQL Error to get game configs", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

		if (configs != null) {
			this.lastUpdate = System.currentTimeMillis();
		}

	}
	
	@Override
	public GameConfigBean getGameConfig(Integer gameId,Long configId) throws ServiceException {
		if(configId == null) {
			throw new InvalidParameterException("configId cannot be null");
		}
		
		principalService.pushSystem();
		try {
			checkUpdate();
		} finally {
			principalService.popSystem();
		}

		GameConfigBean bean = null;
		if(configId != 0) {
			bean = configs.get(configId);
			
			if (!bean.getGameId().equals(gameId)) {
				return null;
			}
			
		} else {
			bean = defaultConfigs.get(gameId);
		}
 
		return bean;
	}

	@Override
	public ServiceResultSet<GameConfigBean> getGameConfigs(Integer gameId, int limit, int offset)throws ServiceException {
		return this.getGameConfigs(gameId, null, null, limit, offset);
	}

	@Override
	public ServiceResultSet<GameConfigBean> getGameConfigs(Integer gameId,SearchContext<GameConfigBean> searchContext,SortContext<GameConfigBean> sortContext, int limit, int offset)throws ServiceException {
		if(gameId == null) {
			throw new InvalidParameterException("gameId cannot be null ");
		}
		
		principalService.pushSystem();
		try {
			checkUpdate();
		} finally {
			principalService.popSystem();
		}
		
		List<Long> longConfigs = gameConfigs.get(gameId);
		

		List<GameConfigBean> copyList = new ArrayList<GameConfigBean>();
		
		if(longConfigs != null) {
			for(Long configId:longConfigs) {
				copyList.add(configs.get(configId));
			}
		}
		copyList.add(defaultConfigs.get(gameId));

		if(offset > copyList.size())  {
			throw new InvalidParameterException("offset cannot be bigger than categories size:" + Long.toString(copyList.size()));
		}

		if(searchContext != null) {
			SearchCondition<GameConfigBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}

		if(sortContext != null) {
			sortContext.sortList(GameConfigBean.class, copyList);
		}		

		List<GameConfigBean> beans = new ArrayList<GameConfigBean>();

		ListIterator<GameConfigBean> iter = copyList.listIterator(offset);

		int myLimit = limit;

		if(myLimit != 0) {
			myLimit++;
		}

		while (iter.hasNext() && (myLimit == 0 || beans.size() < myLimit)) {
			beans.add(iter.next());
		}

		ServiceResultSet<GameConfigBean> res = new ServiceResultSet<GameConfigBean>();
		res.setResult(beans);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (myLimit > 0 && beans.size() > limit) {
			res.setHasNext(true);
			beans.remove(beans.size() - 1);
		}
		return res;
	}

	@Override
	public int getGameConfigsSize(Integer gameId,SearchContext<GameConfigBean> searchContext) throws ServiceException {
		if(gameId == null) {
			throw new InvalidParameterException("gameId cannot be null ");
		}

		principalService.pushSystem();
		try {
			checkUpdate();
		} finally {
			principalService.popSystem();
		}
		
		int size = 0;
		GameConfigBean defaultConfig = defaultConfigs.get(gameId);
		
		if (defaultConfig != null) {
			size = 1;
		}
		
		List<Long> longConfigs = gameConfigs.get(gameId);
		if(longConfigs == null) {
			return size;
		}

		if (searchContext == null) {
			return longConfigs.size() + size;
		}
		
		List<GameConfigBean> copyList = new ArrayList<GameConfigBean>();
		for(Long configId:longConfigs) {
			copyList.add(configs.get(configId));
		}

		SearchCondition<GameConfigBean> searchCondition = searchContext.getCondition();
		copyList = searchCondition.findAll(copyList);

		return copyList.size() + size;
	}
	
	@Override
	public GameConfiguration getFilteredConfiguration(Integer gameId,Long gameConfigId) throws ServiceException {
		if(gameId == null || gameConfigId == null || gameConfigId < 0) {
			throw new InvalidParameterException("Error with parameters");
		}
		
		GameBean game = this.gamesService.getGame(gameId);
		if (game == null) {
			throw new EntityNotFoundException("Game " + gameId +" not found");
		}
		
		GameConfigBean configBean = this.getGameConfig(gameId,gameConfigId);
		if (configBean == null) {
			try {
				return GameConfigurationFactory.loadConfiguration(game.getGameTemplate());
			} catch (GameConfigLoadingException e) {
				throw new ServiceException("Configuration exists but is invalid");
			}
		}
		
		GameConfiguration config;
		try {
			config = GameConfigurationFactory.loadConfiguration(game.getGameTemplate(), gameConfigId, configBean.getConfigValue());
		} catch (GameConfigLoadingException e) {
			throw new ServiceException("Configuration exists but is invalid", e);
		}

		return config;
	}
	
	@Override
	public void insertConfig(GameConfigBean bean) throws ServiceException {	
		
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		bean.setCreatorId(principal.getUserId());

		String sql =  "INSERT INTO game_configs(game_id, config_value, config_comment, modified_by) VALUES(?,?,?,?) RETURNING config_id, config_date";
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null)
			throw new DatabaseException("Unable to get DB connection for inserting config");
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);
			
			stmt.setInt(1, bean.getGameId());
			stmt.setString(2, bean.getConfigValue());
			stmt.setString(3, bean.getConfigComment());			
			DbUtil.setLong(stmt, 4, bean.getCreatorId());
						
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				bean.setConfigId(rs.getLong("config_id"));
				bean.setConfigDate(DbUtil.getDate(DbUtil.getTimestamp(rs, "config_date")));
				/* Invalidate config cache */
				this.lastUpdate = 0;
			}
			
		} catch (SQLException e) {
			throw new DatabaseException("Database error while inserting config", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
	}
	
	@Override
	public void updateConfig(GameConfigBean bean) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null)
			throw new DatabaseException("Unable to get DB connection to update config");
		
		GameConfigBean oldBean = this.getGameConfig(bean.getGameId(), bean.getConfigId());
		
		if (oldBean == null) {
			throw new DatabaseException("Unable to update non-existant bean");
		}

		String sql = "UPDATE game_configs SET config_comment=? WHERE config_id=?";
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, bean.getConfigComment());
			stmt.setLong(2, oldBean.getConfigId());
			
			stmt.executeUpdate();
			
			/* Update oldBean in cache */
			oldBean.setConfigComment(bean.getConfigComment());
		} catch (SQLException e) {
			throw new DatabaseException("Database error while updating config", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}
}
