package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.JackpotsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.JackpotBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.SortUtil;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class JackpotsServiceImpl implements JackpotsService {

	private final PrincipalService principalService;
	private final CorporatesService corporatesService;

	public JackpotsServiceImpl(PrincipalService principalService, CorporatesService corporatesService) {
		this.principalService = principalService;
		this.corporatesService = corporatesService;

		if(this.principalService == null) {
			throw new RuntimeException("Principal service is null");
		}
	}

	@Override
	public ServiceResultSet<JackpotBean> getJackpots(SearchContext<JackpotBean> searchContext,SortContext<JackpotBean> sortContext, int limit, int offset) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		Long corpId;

		switch (userPrincipal.getType()) {
			case STAFF_USER:
			case SYSTEM:
				corpId = null;
				break;
			case CORPORATE_PLAYER:
				CorporateBean corp = corporatesService.getSuperCorporate(userPrincipal.getUserId());

				if (corp != null) {
					corpId = corp.getAffiliateId();
				} else {
					throw new AccessDeniedException();
				}

				break;
			case CORPORATE_ACCOUNT:
				corpId = userPrincipal.getUserId();
				break;
			default:
				corpId = 0L;
				break;
		}


		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;

		List<JackpotBean> jackpots = new ArrayList<JackpotBean>();
		try {
			StringBuilder sql = new StringBuilder("SELECT jackpots.jackpot_id,jackpots.game_id," +
					"jackpots.finish_date,jackpots.pay_count,jackpots.initial_amount,jackpots.bet_contrib," +
					"jackpots.bet_amount,jackpots.start_date,jackpots.maximum_amount,jackpots.auto," +
					"jackpots.target_corporate,jackpots.config_id,jackpots.draw_number," +
					"jackpots.min_payout,jackpots.min_jackpot_balance," +
					"jackpot_balances.balance,jackpot_balances.winners " +
					"FROM jackpots INNER JOIN jackpot_balances ON jackpot_balances.jackpot_id = jackpots.jackpot_id");

			if (corpId == null) {
				sql.append(" WHERE 1=1");
			} else if (corpId.equals(0L)) {
				sql.append(" WHERE jackpots.target_corporate IS NULL");
			} else {
				sql.append(" WHERE (jackpots.target_corporate IS NULL OR jackpots.target_corporate = ?)");
			}

			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}
			int myLimit = limit;

			if (limit > 0){
				myLimit++;
			}

			if(sortContext != null) {
				SortUtil.appendSqlEpilogue(sql, sortContext.getConditions(JackpotBean.class), offset, myLimit);
			}else{
				SortUtil.appendSqlEpilogue(sql, null, offset, myLimit);
			}
			stmt = conn.prepareStatement(sql.toString());

			int num = 1;
			if(corpId != null && corpId > 0L) {
				stmt.setLong(num, corpId);
				num++;
			}
			if(searchContext != null) { 
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			rs = stmt.executeQuery();
			while (rs.next()){
				JackpotBean bean = new JackpotBean();
				bean.setJackpotId(DbUtil.getInteger(rs,"jackpot_id"));
				bean.setGameId(DbUtil.getInteger(rs,"game_id"));
				bean.setFinishDate(DbUtil.getTimestamp(rs,"finish_date"));
				bean.setPayCount(DbUtil.getInteger(rs,"pay_count"));
				bean.setInitialAmount(DbUtil.getDouble(rs,"initial_amount"));
				bean.setBetContribution(DbUtil.getDouble(rs,"bet_contrib"));
				bean.setTargetCorporate(DbUtil.getLong(rs,"target_corporate"));
				bean.setConfigId(DbUtil.getInteger(rs,"config_id"));
				bean.setDrawNumber(DbUtil.getInteger(rs,"draw_number"));
				bean.setMinimumPayout(DbUtil.getDouble(rs,"min_payout"));
				bean.setMinimumJackpotBalance(DbUtil.getDouble(rs,"min_jackpot_balance"));
				bean.setBalance(DbUtil.getDouble(rs,"balance"));
				bean.setWinners(DbUtil.getInteger(rs,"winners"));

				filterBean(userPrincipal, bean);
				jackpots.add(bean);
			}

		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		ServiceResultSet<JackpotBean> res = new ServiceResultSet<JackpotBean>();
		res.setResult(jackpots);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (limit > 0 && jackpots.size() > limit) {
			res.setHasNext(true);
			jackpots.remove(limit);
		}
		return res;
	}

	@Override
	public ServiceResultSet<JackpotBean> getJackpots(int limit, int offset) throws ServiceException {
		return this.getJackpots(null, null, limit, offset);
	}

	@Override
	public ServiceResultSet<JackpotBean> getActiveJackpots(SearchContext<JackpotBean> searchContext,SortContext<JackpotBean> sortContext, int limit, int offset)throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		Long corpId;

		switch (userPrincipal.getType()) {
			case NORMAL_USER:
			case DEMO_USER:
				corpId = 0L;
				break;

			case CORPORATE_PLAYER:
				CorporateBean corp = corporatesService.getSuperCorporate(userPrincipal.getUserId());

				if (corp != null) {
					corpId = corp.getAffiliateId();
				} else {
					throw new AccessDeniedException();
				}

				break;
			case CORPORATE_ACCOUNT:
				corpId = userPrincipal.getUserId();
				break;
			default:
				corpId = null;
				break;
		}

		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;


		List<JackpotBean> jackpots = new ArrayList<JackpotBean>();
		try {
			StringBuilder sql = new StringBuilder("SELECT jackpots.jackpot_id,jackpots.game_id," +
					"jackpots.finish_date,jackpots.pay_count,jackpots.initial_amount,jackpots.bet_contrib," +
					"jackpots.bet_amount,jackpots.start_date,jackpots.maximum_amount,jackpots.auto," +
					"jackpots.target_corporate,jackpots.config_id,jackpots.draw_number," +
					"jackpots.min_payout,jackpots.min_jackpot_balance," +
					"jackpot_balances.balance,jackpot_balances.winners " +
					"FROM jackpots INNER JOIN jackpot_balances ON jackpot_balances.jackpot_id = jackpots.jackpot_id " +
					"WHERE (jackpots.finish_date IS NULL) ");

			if (corpId == null) {

			} else if (corpId.equals(0L)) {
				sql.append(" AND jackpots.target_corporate IS NULL");
			} else {
				sql.append(" AND (jackpots.target_corporate IS NULL OR jackpots.target_corporate = ?)");
			}

			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}

			int myLimit = limit;

			if (limit > 0){
				myLimit++;
			}
			if(sortContext != null) {
				SortUtil.appendSqlEpilogue(sql, sortContext.getConditions(JackpotBean.class), offset, myLimit);
			}else{
				SortUtil.appendSqlEpilogue(sql, null, offset, myLimit);
			}
			stmt = conn.prepareStatement(sql.toString());
			int num = 1;

			if(corpId != null && corpId > 0L) {
				stmt.setLong(num, corpId);
				num++;
			}

			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			rs = stmt.executeQuery();
			while (rs.next()){
				JackpotBean bean = new JackpotBean();
				bean.setJackpotId(DbUtil.getInteger(rs,"jackpot_id"));
				bean.setGameId(DbUtil.getInteger(rs,"game_id"));
				bean.setFinishDate(DbUtil.getTimestamp(rs,"finish_date"));
				bean.setPayCount(DbUtil.getInteger(rs,"pay_count"));
				bean.setInitialAmount(DbUtil.getDouble(rs,"initial_amount"));
				bean.setBetContribution(DbUtil.getDouble(rs,"bet_contrib"));
				bean.setTargetCorporate(DbUtil.getLong(rs,"target_corporate"));
				bean.setConfigId(DbUtil.getInteger(rs,"config_id"));
				bean.setDrawNumber(DbUtil.getInteger(rs,"draw_number"));
				bean.setMinimumPayout(DbUtil.getDouble(rs,"min_payout"));
				bean.setMinimumJackpotBalance(DbUtil.getDouble(rs,"min_jackpot_balance"));
				bean.setBalance(DbUtil.getDouble(rs,"balance"));
				bean.setWinners(DbUtil.getInteger(rs,"winners"));

				filterBean(userPrincipal, bean);
				jackpots.add(bean);
			}

		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		ServiceResultSet<JackpotBean> res = new ServiceResultSet<JackpotBean>();
		res.setResult(jackpots);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (limit > 0 && jackpots.size() > limit) {
			res.setHasNext(true);
			jackpots.remove(limit);
		}
		return res;
	}

	private void filterBean(ExtendedPrincipalBean userPrincipal, JackpotBean bean) {
		if (userPrincipal.getType() == UserTypeEnum.SYSTEM
				|| userPrincipal.getType() == UserTypeEnum.STAFF_USER) {
			return;
		}

		bean.setBetContribution(null);
		bean.setDrawNumber(null);
		bean.setInitialAmount(null);
		bean.setMaximumAmount(null);
		bean.setMinimumJackpotBalance(null);
		bean.setMinimumPayout(null);
		bean.setPayCount(null);
	}

	@Override
	public JackpotBean getJackpot(Integer jackpotId) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("Not authenticated");
		}
		if(jackpotId == null) {
			throw new InvalidParameterException("Jackpot id is required");
		}
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;

		JackpotBean bean = null;
		try {
			StringBuilder sql = new StringBuilder("SELECT jackpots.jackpot_id,jackpots.game_id," +
					"jackpots.finish_date,jackpots.pay_count,jackpots.initial_amount,jackpots.bet_contrib," +
					"jackpots.bet_amount,jackpots.start_date,jackpots.maximum_amount,jackpots.auto," +
					"jackpots.target_corporate,jackpots.config_id,jackpots.draw_number," +
					"jackpots.min_payout,jackpots.min_jackpot_balance," +
					"jackpot_balances.balance,jackpot_balances.winners " +
					"FROM jackpots INNER JOIN jackpot_balances ON jackpot_balances.jackpot_id = jackpots.jackpot_id " +
					"WHERE jackpots.jackpot_id = ?");
			stmt = conn.prepareStatement(sql.toString());
			stmt.setInt(1, jackpotId);
			rs = stmt.executeQuery();
			if(rs.next()) {
				bean = new JackpotBean();
				bean.setJackpotId(DbUtil.getInteger(rs,"jackpot_id"));
				bean.setGameId(DbUtil.getInteger(rs,"game_id"));
				bean.setFinishDate(DbUtil.getTimestamp(rs,"finish_date"));
				bean.setPayCount(DbUtil.getInteger(rs,"pay_count"));
				bean.setInitialAmount(DbUtil.getDouble(rs,"initial_amount"));
				bean.setBetContribution(DbUtil.getDouble(rs,"bet_contrib"));
				bean.setTargetCorporate(DbUtil.getLong(rs,"target_corporate"));
				bean.setConfigId(DbUtil.getInteger(rs,"config_id"));
				bean.setDrawNumber(DbUtil.getInteger(rs,"draw_number"));
				bean.setMinimumPayout(DbUtil.getDouble(rs,"min_payout"));
				bean.setMinimumJackpotBalance(DbUtil.getDouble(rs,"min_jackpot_balance"));
				bean.setBalance(DbUtil.getDouble(rs,"balance"));
				bean.setWinners(DbUtil.getInteger(rs,"winners"));	
			}
		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

		if (bean == null) {
			return null;
		}

		filterBean(userPrincipal, bean);

		switch (userPrincipal.getType()) {
			case NORMAL_USER:
			case DEMO_USER:
				if (bean.getTargetCorporate() != null) {
					return null;
				}
				break;

			case CORPORATE_PLAYER:
				CorporateBean corp = corporatesService.getSuperCorporate(userPrincipal.getUserId());

				if (corp != null) {
					Long corpId = corp.getAffiliateId();

					if (bean.getTargetCorporate() != null && !corpId.equals(bean.getTargetCorporate())) {
						return null;
					}
				}

				break;
			case CORPORATE_ACCOUNT:
				if (bean.getTargetCorporate() != null && !userPrincipal.getUserId().equals(bean.getTargetCorporate())) {
					return null;
				}
				break;
			default:
				break;
		}

		return bean;
	}

	@Override
	public ServiceResultSet<JackpotBean> getActiveJackpots(int limit, int offset)throws ServiceException {
		return this.getActiveJackpots(null, null, limit, offset);
	}

	@Override
	public int getActiveJackpotsSize(SearchContext<JackpotBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		Long corpId;

		switch (userPrincipal.getType()) {
			case NORMAL_USER:
			case DEMO_USER:
				corpId = 0L;
				break;

			case CORPORATE_PLAYER:
				CorporateBean corp = corporatesService.getSuperCorporate(userPrincipal.getUserId());

				if (corp != null) {
					corpId = corp.getAffiliateId();
				} else {
					throw new AccessDeniedException();
				}

				break;
			case CORPORATE_ACCOUNT:
				corpId = userPrincipal.getUserId();
				break;
			default:
				corpId = null;
				break;
		}

		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT count(*)" +
					"FROM jackpots INNER JOIN jackpot_balances ON jackpot_balances.jackpot_id = jackpots.jackpot_id " +
					"WHERE (jackpots.finish_date IS NULL) ");

			if (corpId == null) {

			} else if (corpId.equals(0L)) {
				sql.append(" AND jackpots.target_corporate IS NULL");
			} else {
				sql.append(" AND (jackpots.target_corporate IS NULL OR jackpots.target_corporate = ?)");
			}

			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}

			stmt = conn.prepareStatement(sql.toString());
			int num = 1;

			if(corpId != null && corpId > 0L) {
				stmt.setLong(num, corpId);
				num++;
			}

			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			rs = stmt.executeQuery();
			if (rs.next()){
				return rs.getInt("count");
			}

		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return 0;
	}

	@Override
	public int getJackpotsSize(SearchContext<JackpotBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		Long corpId;

		switch (userPrincipal.getType()) {
			case NORMAL_USER:
			case DEMO_USER:
				corpId = 0L;
				break;

			case CORPORATE_PLAYER:
				CorporateBean corp = corporatesService.getSuperCorporate(userPrincipal.getUserId());

				if (corp != null) {
					corpId = corp.getAffiliateId();
				} else {
					throw new AccessDeniedException();
				}

				break;
			case CORPORATE_ACCOUNT:
				corpId = userPrincipal.getUserId();
				break;
			default:
				corpId = null;
				break;
		}

		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT count(*)" +
					"FROM jackpots INNER JOIN jackpot_balances ON jackpot_balances.jackpot_id = jackpots.jackpot_id");


			if (corpId == null) {
				sql.append(" WHERE 1=1");
			} else if (corpId.equals(0L)) {
				sql.append(" WHERE jackpots.target_corporate IS NULL");
			} else {
				sql.append(" WHERE (jackpots.target_corporate IS NULL OR jackpots.target_corporate = ?)");
			}

			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}

			stmt = conn.prepareStatement(sql.toString());

			int num = 1;

			if (corpId != null && corpId > 0L) {
				stmt.setLong(num, corpId);
				num++;
			}

			if(searchContext != null) { 
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			rs = stmt.executeQuery();
			if (rs.next()){
				return rs.getInt("count");
			}

		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return 0;
	}

}
