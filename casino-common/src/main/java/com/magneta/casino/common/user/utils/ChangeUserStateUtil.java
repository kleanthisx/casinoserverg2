package com.magneta.casino.common.user.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class ChangeUserStateUtil {

	private static final Logger log = LoggerFactory.getLogger(ChangeUserStateUtil.class);
	
	public static boolean changeUserState(long userId, long modifierId, boolean active) {
		Connection conn = ConnectionFactory.getConnection();
		try {
			return changeUserState(conn, userId, modifierId, active);		
		} finally {
			DbUtil.close(conn);
		}
	}
	
	public static boolean changeUserState(Connection conn, long userId, long modifierId, boolean active) {

		PreparedStatement statement = null;

		if (conn == null){
			return false;
		}

		try {
			String sql = 
				"UPDATE users " +
				" SET is_active = ?, modifier_user = ?" +
				" WHERE user_id = ?" +
				" AND closed = FALSE";

			statement = conn.prepareStatement(sql);
			statement.setBoolean(1, active);
			
			if(modifierId > 0) {
				statement.setLong(2, modifierId);
			} else {
				statement.setObject(2, null);
			}
			
			statement.setLong(3, userId);

			if(statement.executeUpdate() == 1) {
				return true;
			}

		} catch (SQLException e) {
			log.error("Error updating user state.",e);
		} finally{
			DbUtil.close(statement);
		}
		return false;
	}
}
