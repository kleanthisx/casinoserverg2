package com.magneta.casino.common.services;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserCommentService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserCommentBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class UserCommentServiceImpl implements UserCommentService {

	private final UserAccessService userAccessService;
	private final PrincipalService principalService;

	public UserCommentServiceImpl(UserAccessService userAccessService,
			PrincipalService principalService) {
		this.userAccessService = userAccessService;
		this.principalService = principalService;
	}

	@Override
	public UserCommentBean getComment(Long userId, Long commentId) throws ServiceException {
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.COMMENTS, false)) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		SearchContext<UserCommentBean> searchContext = FiqlContextBuilder.create(UserCommentBean.class, "userId==?;messageId==?", userId, commentId);
		return new SqlBuilder<UserCommentBean>(UserCommentBean.class).findBean(searchContext);
	}

	@Override
	public ServiceResultSet<UserCommentBean> getComments(Long userId, SearchContext<UserCommentBean> searchContext, int limit, int offset, SortContext<UserCommentBean> sortContext) throws ServiceException{
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.COMMENTS, false)) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		if (searchContext == null) {
			searchContext = FiqlContextBuilder.create(UserCommentBean.class, "userId==?", userId);
		} else {
			searchContext = FiqlContextBuilder.create(UserCommentBean.class, "userId==?;(?)", userId, searchContext);
		}

		return new SqlBuilder<UserCommentBean>(UserCommentBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public int getCommentsSize(Long userId, SearchContext<UserCommentBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.COMMENTS, false)) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		if (searchContext == null) {
			searchContext = FiqlContextBuilder.create(UserCommentBean.class, "userId==?", userId);
		} else {
			searchContext = FiqlContextBuilder.create(UserCommentBean.class, "userId==?;(?)", userId, searchContext);
		}

		return new SqlBuilder<UserCommentBean>(UserCommentBean.class).countBeans(searchContext, null);
	}

	@Override
	public void createComment(UserCommentBean comment) throws ServiceException {
		if (comment.getUserId() == null || comment.getComment() == null) {
			throw new InvalidParameterException("Invalid comment");
		}
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.COMMENTS, true)) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(comment.getUserId())) {
			throw new AccessDeniedException();
		}
		
		comment.setDateCreated(null);
		comment.setCommenterId(principal.getUserId());
		
		new SqlBuilder<UserCommentBean>(UserCommentBean.class).insertBean(comment);
	}
	
	@Override
	public void editComment(UserCommentBean comment) throws ServiceException {
		if (comment.getMessageId() == null || comment.getUserId() == null || comment.getComment() == null) {
			throw new InvalidParameterException("Invalid comment");
		}
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.COMMENTS, true)) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(comment.getUserId())) {
			throw new AccessDeniedException();
		}
		
		UserCommentBean orig = getComment(comment.getUserId(), comment.getMessageId());
		if (orig == null || !orig.getUserId().equals(comment.getUserId())) {
			throw new InvalidParameterException("Invalid comment");
		}
		
		if (orig.getComment().equals(comment.getComment())) {
			return;
		}
		
		comment.setCommenterId(principal.getUserId());
		
		new SqlBuilder<UserCommentBean>(UserCommentBean.class).updateBean(comment);
	}
	
	@Override
	public void deleteComment(Long userId, Long commentId) throws ServiceException {
		if (userId == null || commentId == null) {
			throw new InvalidParameterException("Invalid comment");
		}
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.COMMENTS, true)) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SearchContext<UserCommentBean> searchContext = 
				FiqlContextBuilder.create(UserCommentBean.class, "userId==?;messageId==?", userId, commentId);
		
		new SqlBuilder<UserCommentBean>(UserCommentBean.class).deleteBeans(searchContext);
	}
}
