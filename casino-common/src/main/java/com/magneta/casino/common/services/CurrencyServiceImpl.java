package com.magneta.casino.common.services;

import java.util.Currency;

import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;

public class CurrencyServiceImpl implements CurrencyService {

	private final Currency currentCurrency;

	public CurrencyServiceImpl(SettingsService settingsService) throws ServiceException {
		String currency = settingsService.getSettingValue("system.currency", String.class);
		
		this.currentCurrency = Currency.getInstance(currency);
	}

	@Override
	public Currency getActiveCurrency() {
		return this.currentCurrency;
	}	
}
