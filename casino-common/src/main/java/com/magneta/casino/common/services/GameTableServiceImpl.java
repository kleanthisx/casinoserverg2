package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GameTableServiceImpl implements GameTableService {

	private final UserAccessService userAccessService;
	private final PrincipalService principalService;
	
	public GameTableServiceImpl(UserAccessService userAccessService, PrincipalService principalService) {
		this.userAccessService = userAccessService;
		this.principalService = principalService;
	}
	
	@Override
	public GameTableBean getActiveTable(Long userId, Integer gameId) throws ServiceException {
		if (!this.userAccessService.canAccess(userId)) {
			throw new AccessDeniedException("Access denied");
		}
		
		SearchContext<GameTableBean> searchContext = FiqlContextBuilder.create(GameTableBean.class, "owner==?;gameId==?;dateClosed==?", userId, gameId, null);
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		try {
			return new SqlBuilder<GameTableBean>(GameTableBean.class).findBean(conn, searchContext, null);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	@Override
	public GameTableBean getTable(Long tableId) throws ServiceException {		
		SearchContext<GameTableBean> searchContext = FiqlContextBuilder.create(GameTableBean.class, "tableId==?", tableId);
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		GameTableBean table;
		try {
			table = new SqlBuilder<GameTableBean>(GameTableBean.class).findBean(conn, searchContext, null);
		} finally {
			DbUtil.close(conn);
		}
		
		if (table == null) {
			return null;
		}
		
		if (table.getOwner() != null) {
			if (!userAccessService.canAccess(table.getOwner())) {
				return null;
			}
		}
		
		return table;
	}
	
	@Override
	public Integer getCurrentRoundId(Long tableId) throws ServiceException {
    	Connection dbConn = ConnectionFactory.getConnection();
    	if (dbConn == null) {
    		throw new DatabaseException("Out of database connections");
    	}
    	
    	PreparedStatement stmt = null;
    	ResultSet rs = null;
    	try {
    		stmt = dbConn.prepareStatement("SELECT last_round_id FROM game_table_last_rounds WHERE table_id=?");
    		stmt.setLong(1, tableId);
    		
    		rs = stmt.executeQuery();
    		
    		if (rs.next()) {
    			int roundId = rs.getInt("last_round_id");
    			if (rs.wasNull()) {
    				return null;
    			}
    			return roundId;
    		}
    		
    	} catch (SQLException e) {
			throw new DatabaseException("Database error while retrieving current round", e);
		} finally {
    		DbUtil.close(rs);
    		DbUtil.close(stmt);
    		DbUtil.close(dbConn);
    	}
    	
    	throw new EntityNotFoundException("Round not found");
    }

	@Override
	public void moveTable(Long tableId, Integer serverId)
			throws ServiceException {
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.TABLES, true)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		if (tableId == null || serverId == null) {
			throw new InvalidParameterException("Invalid tableId/serverId");
		}
		
		SearchContext<GameTableBean> searchContext = FiqlContextBuilder.create(GameTableBean.class, "tableId==?", tableId);
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		GameTableBean table;
		try {
			table = new SqlBuilder<GameTableBean>(GameTableBean.class).findBean(conn, searchContext, null);
		} finally {
			DbUtil.close(conn);
		}
		
		if (table == null) {
			throw new EntityNotFoundException("Table not found");
		}
		
		if (table.getOwner() != null) {
			if (!userAccessService.canAccess(table.getOwner())) {
				throw new EntityNotFoundException("Table not found");
			}
		}
		
		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		PreparedStatement statement = null;

		try {
			String sql = 
					"UPDATE game_tables" +
							" SET server_id = ?" +
							" WHERE table_id = ?";

			statement = dbConn.prepareStatement(sql);

			statement.setInt(1, serverId);
			statement.setLong(2, tableId);

			statement.execute();

		} catch (SQLException e) {
			throw new DatabaseException("Database error while moving table", e);
		} finally{
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
		
	}
}
