package com.magneta.casino.common.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.magneta.casino.common.system.SettingNames.*;
import static com.magneta.casino.services.enums.SettingTypeEnum.*;

public class SettingDefinitions {

	private static final Map<String, SettingDefinition> settingDefinitions;

	static { 
		settingDefinitions = new HashMap<String, SettingDefinition>();
		
		s(new SettingDefinition(SYSTEM_NAME, STRING, null, "Casino Name", "System", null));
		s(new SettingDefinition("system.operation_license_info", STRING, null, "Operation Licence", "System", null));
		s(new SettingDefinition("system.currency", STRING, "EUR", "System Currency", "System", null));
		s(new SettingDefinition("system.single_country", COUNTRY, null, "Single Country", "System", null));
		
		s(new SettingDefinition("system.backoffice.hosts_whitelist_enabled", BOOLEAN, false, "Backoffice IP Whitelist", "Backoffice", null));
		
		/* Fun Mode */
		s(new SettingDefinition("system.fun.enable", BOOLEAN, false, "Fun Mode", "System", "Fun Mode"));
		s(new SettingDefinition("system.fun.login_balance", DOUBLE, 100.0, "Fun mode login balance", "System", "Fun Mode"));

		/* Free Credits */
		s(new SettingDefinition(DEFAULT_PROMO_RATE, PERCENTAGE, 0.1, "Free credits percentage", "Free Credits", null));
		s(new SettingDefinition("system.promo.free_secs_time_allowed", INTEGER, 60 * 60, "Free credit time (s)", "Free Credits", null));
		s(new SettingDefinition("system.promo.free_max_balance", DOUBLE, 0.1999, "Free credit max balance", "Free Credits", null));

		/* Jackpots */
		s(new SettingDefinition("system.jackpots.corporate_jackpots_enabled", BOOLEAN, false, "Enable corporate jackpots", "Jackpots", "Corporate"));
		s(new SettingDefinition("system.global_corporate_jackpot", BOOLEAN, false, "Global corporate jackpot", "Jackpots", "Corporate"));
		
		/* Games: Server */
		s(new SettingDefinition("system.games.max_users_per_server", INTEGER, null, "Max players per server", "Games", "Server"));
		s(new SettingDefinition("game.session.timeout", INTEGER, null, "Idle session timeout (s)", "Games", "Server"));
		s(new SettingDefinition(GAME_CLIENTS, STRING, null, "Game clients (empty for all)", "Games", "Server"));
		
		/* Games: Virtual Races */
		s(new SettingDefinition("system.games.virtualracing.video.url", STRING, null, "URL prefix for videos", "Games", "Virtual Races"));
		s(new SettingDefinition("system.games.virtualracing.video.sub_url", STRING, null, "URL prefix for videos", "Games", "Virtual Races"));
		s(new SettingDefinition("system.games.virtualracing.video_ftp.update_interval", INTEGER, 10 * 60, "Video List refresh interval (seconds)", "Games", "Virtual Races"));
		s(new SettingDefinition("system.games.virtualracing.video_ftp.host", STRING, null, "Video FTP host", "Games", "Virtual Races"));
		s(new SettingDefinition("system.games.virtualracing.video_ftp.username", STRING, null, "Video FTP username", "Games", "Virtual Races"));
		s(new SettingDefinition("system.games.virtualracing.video_ftp.password", STRING, null, "Video FTP Password", "Games", "Virtual Races"));

		/* Reporting */
		s(new SettingDefinition("system.log_reports_generation", BOOLEAN, false, "Log reports", "Reports", null));
		s(new SettingDefinition("system.reports.use_report_queue", BOOLEAN, false, "Report Queue", "Reports", null));
		s(new SettingDefinition("system.reports.queue_size", INTEGER, 100, "Report Queue size", "Reports", null));
		s(new SettingDefinition("system.reports.max_repo_size_per_user", INTEGER, 50, "Reports to save per user", "Reports", null));
	}
	
	private static void s(SettingDefinition s) {
		settingDefinitions.put(s.getKey(), s);
	}
	
	public static SettingDefinition get(String key) {
		return settingDefinitions.get(key);
	}
	
	public static List<SettingDefinition> getDefinitions() {
		Collection<SettingDefinition> defs = settingDefinitions.values();
		
		List<SettingDefinition> d = new ArrayList<SettingDefinition>();
		
		d.addAll(defs);
		
		return d;
	}
}
