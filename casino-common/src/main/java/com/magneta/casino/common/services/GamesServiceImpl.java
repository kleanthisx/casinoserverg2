package com.magneta.casino.common.services;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.beans.GameDescriptorBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchCondition;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.CollectionUtils;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GamesServiceImpl implements GamesService {
	private static final long TIMEOUT = 5 * 60 * 1000;
	private static final String gameImages = "com/magneta/games/images/game";

	private long lastUpdate;
	private List<GameBean> games;
	private Map<Integer, GameBean> gamesTable;
	
	private final GameConfigsService gameConfigs;
	private final PrincipalService principalService;
	
	public GamesServiceImpl(PrincipalService principalService, GameConfigsService gameConfigs) {
		lastUpdate = 0;
		this.principalService = principalService;
		this.gameConfigs = gameConfigs;
	}

	private synchronized void checkUpdate() throws ServiceException {
		long now = System.currentTimeMillis();

		if (now - lastUpdate < TIMEOUT)
			return;

		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		List<GameBean> games;
		try {
			games = new SqlBuilder<GameBean>(GameBean.class).findBeans(null, null, null);
		} finally {
			DbUtil.close(dbConn);
		}
		
		Map<Integer, GameBean> gamesTable = new HashMap<Integer, GameBean>();
		
		for (GameBean g: games) {
			gamesTable.put(g.getGameId(), g);
		}

		this.lastUpdate = System.currentTimeMillis();
		this.games = games;
		this.gamesTable = gamesTable;
	}

	@Override
	public GameBean getGame(Integer gameId) throws ServiceException {
		if(gameId == null) {
			throw new InvalidParameterException("gameId is required");
		}
		checkUpdate();
		Map<Integer, GameBean> games = this.gamesTable;

		if (games == null) {
			throw new ServiceException("gamesTable is null");
		}

		GameBean game = games.get(gameId);
		
		if (game == null) {
			return null;
		}
		
		/* Do not send the template to users not in casino staff */
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || (principal.getType() != UserTypeEnum.SYSTEM
			&& principal.getType() != UserTypeEnum.STAFF_USER)) {
			
			GameBean newGame = new GameBean(game);
			newGame.setGameTemplate(null);
			return newGame;
			
		}
		return (GameBean)game.clone();
	}

	@Override
	public List<GameBean> getGames() throws ServiceException {
		checkUpdate();
		List<GameBean> gameList = new ArrayList<GameBean>(games.size());
		
		for (GameBean game: games) {
			gameList.add(new GameBean(game));
		}
		
		return gameList;
	}

	@Override
	public ServiceResultSet<GameBean> getGames(int limit, int offset) throws ServiceException {
		return this.getGames(null,null,limit,offset);
	}

	@Override
	public ServiceResultSet<GameBean> getGames(SearchContext<GameBean> searchContext,SortContext<GameBean> sortContext, int limit, int offset) throws ServiceException {
		List<GameBean> copyList = getGames();

		if(offset > copyList.size())  {
			throw new InvalidParameterException("offset cannot be bigger than winners size:" + Long.toString(copyList.size()));
		}

		if(searchContext != null) {
			SearchCondition<GameBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}

		if(sortContext != null) {
			sortContext.sortList(GameBean.class, copyList);
		}		

		List<GameBean> beans = new ArrayList<GameBean>();

		ListIterator<GameBean> iter = copyList.listIterator(offset);

		int myLimit = limit;

		if(myLimit != 0) {
			myLimit++;

		}

		while (iter.hasNext() && (myLimit == 0 || beans.size() < myLimit)) {
			beans.add(new GameBean(iter.next()));
		}

		ServiceResultSet<GameBean> res = new ServiceResultSet<GameBean>();
		res.setResult(beans);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (myLimit > 0 && beans.size() > limit) {
			res.setHasNext(true);
			beans.remove(beans.size() - 1);
		}
		return res;

	}

	private URL getGameImage(String filename) {
		String urlString = gameImages + filename;
		return this.getClass().getClassLoader().getResource(urlString);
	}

	@Override
	public GameDescriptorBean getGameDescriptor(Integer gameId) throws ServiceException {

		if (gameId == null) {
			throw new InvalidParameterException("gameId is required");
		}

		GameBean bean = getGame(gameId);

		if (bean == null) {
			throw new EntityNotFoundException( "Game " + gameId + " not found");
		}

		GameDescriptorBean game = new GameDescriptorBean(bean);

		URL image = getGameImage(gameId + "/logo.png");
		game.setGameLogoUrl(image);
		game.setGameLobbyUrl(getGameImage(gameId + "/lobby.png"));
		return game;
	}

	@Override
	public int getGamesSize(SearchContext<GameBean> searchContext)throws ServiceException {
		checkUpdate();
		List<GameBean> copyList = CollectionUtils.copyList(games);

		if(searchContext != null) {
			SearchCondition<GameBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}
		return games.size();
	}
	
	@Override
	public GameConfigBean getGameActiveConfig(Integer gameId) throws ServiceException {
		
		if (gameId == null || gameId <= 0) {
			throw new InvalidParameterException("Invalid gameId");
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get connection to get games");
		}

		String sql = "SELECT config_id FROM game_current_configs WHERE game_id = ?";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);
			
			stmt.setInt(1, gameId);
			
			rs = stmt.executeQuery();
			
			Long configId = null;
			
			if (rs.next()) {
				configId = DbUtil.getLong(rs, "config_id");
			}
			
			/* Make sure we return the default configuration */
			if (configId == null) {
				configId = 0L;
			}
			
			return gameConfigs.getGameConfig(gameId, configId);
			
		} catch (SQLException e) {
			throw new DatabaseException("SQL Error to get game active config", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

	}

	@Override
	public void insertGame(GameBean gameBean) throws ServiceException {
		if(gameBean == null || gameBean.getGameId() == null || gameBean.getName() == null || gameBean.getGameTemplate() == null) {
			throw new InvalidParameterException("Error with parameters");
		}
		
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		new SqlBuilder<GameBean>(GameBean.class).insertBean(gameBean);
		
		GameBean clone = new GameBean(gameBean);
		
		synchronized(this) {
			games.add(clone);
			gamesTable.put(clone.getGameId(), clone);
		}
	}
	
	private synchronized void invalidateCache() {
		this.lastUpdate = 0;
	}
	
	@Override
	public void updateGame(GameBean gameBean) throws ServiceException {
		if(gameBean == null || gameBean.getGameId() == null || gameBean.getName() == null || gameBean.getGameTemplate() == null) {
			throw new InvalidParameterException("Error with parameters");
		}
		
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true)) {
			throw new AccessDeniedException("Access Denied");
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}

		PreparedStatement stmt = null;
		int i = 1;

		try {
			stmt = conn.prepareStatement(
					"UPDATE game_types"+
					" SET game_type_variant = ?, game_category_id = ?," +
					"game_template= ?" +
			" WHERE game_type_id = ?");

			stmt.setString(i++,gameBean.getName());
			stmt.setInt(i++,gameBean.getCategoryId());
			stmt.setString(i++,gameBean.getGameTemplate());
			stmt.setInt(i++,gameBean.getGameId());
		
			if (stmt.executeUpdate() < 1) {
				throw new ServiceException("Game not found");
			}
			
		} catch (SQLException e) {
			throw new DatabaseException("Error while updating game", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		invalidateCache();
	}

	@Override
	public void setGameActiveConfig(Integer gameId, Long configId)
			throws ServiceException {
		if (gameId == null || configId == null) {
			throw new ServiceException("Invalid config");
		}
		
		if (configId.equals(0l)) {
			throw new ServiceException("Default Configuration cannot be activated");
		}

		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		GameConfigBean currentConfig = getGameActiveConfig(gameId);
		Long currentConfigId = null;
		
		if (currentConfig != null && currentConfig.getConfigId() > 0l) {
			currentConfigId = currentConfig.getConfigId();
		}
		
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		try {
			
			if (currentConfigId != null) {
				PreparedStatement stmt = null;
				try {
					stmt = conn.prepareStatement("UPDATE game_current_configs" +
							" SET config_id = ?" +
							" WHERE game_id = ? AND config_id = ?");

					stmt.setLong(1, configId);
					stmt.setInt(2, gameId);
					stmt.setLong(3, currentConfigId);

					if (stmt.executeUpdate() != 1) {
						throw new ServiceException("Concurrent modification exception");
					}
				} finally {
					DbUtil.close(stmt);
				}
			} else {
				PreparedStatement stmt = null;
				try {
					stmt = conn.prepareStatement(
							"INSERT INTO game_current_configs(game_id, config_id)" +
							" VALUES(?,?) ");

					stmt.setInt(1, gameId);
					stmt.setLong(2, configId);

					stmt.executeUpdate();
				} finally {
					DbUtil.close(stmt);
				}
			}
			
			writeChangeHistory(conn, principal, gameId, currentConfigId, configId);

		} catch (SQLException e) {
			throw new DatabaseException("Unable to update current configuration", e);
		} finally {
			DbUtil.close(conn);
		}
		
	}
	
	private void writeChangeHistory(Connection conn, ExtendedPrincipalBean principal, Integer gameId, Long oldConfigId, Long newConfigId) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(
					"INSERT INTO game_config_changes(game_id, modifier, old_config_id, new_config_id)" +
			" VALUES(?,?,?,?)");

			stmt.setLong(1, gameId);
			DbUtil.setLong(stmt, 2, principal.getUserId());
			if(oldConfigId != null) {
				stmt.setString(3, oldConfigId.toString());
			} else {
				stmt.setString(3, null);
			}
			
			if(newConfigId != null) {
				stmt.setString(4, newConfigId.toString());
			} else {
				stmt.setString(4, null);
			}

			stmt.executeUpdate();

		} finally {
			DbUtil.close(stmt);
		}
	}
}
