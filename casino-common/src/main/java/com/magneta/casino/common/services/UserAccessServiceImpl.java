package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserAccessServiceImpl implements UserAccessService {

	private final PrincipalService principalService;
	private final Map<String, Boolean> accessMap;

	public UserAccessServiceImpl(PrincipalService principalService) {
		this.principalService = principalService;
		this.accessMap = new ConcurrentHashMap<String, Boolean>();
	}

	private static String calculateAccessKey(Long principalUserId, Long userId) {
		return principalUserId + "-" + userId;
	}
	
	private static String calculateAccessKey(ExtendedPrincipalBean principal, Long userId) {
		return calculateAccessKey(principal.getUserId(), userId);
	}
	
	private static Boolean calculateCorporateAccess(ExtendedPrincipalBean principal, Long userId) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT user_id, corp_has_access(?, user_id) AS has_access FROM users WHERE user_id=?";

			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, principal.getUserId());
			stmt.setLong(2, userId);

			rs = stmt.executeQuery();
			
			if (rs.next()) {

				Long uId = DbUtil.getLong(rs, "user_id");

				if (uId == null) {
					return null;
				}
				return rs.getBoolean("has_access");
			}			

		} catch (SQLException e) {
			throw new RuntimeException("Unable to retrieve corporate access", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		return null;
	}
	
	private static UserTypeEnum getUserType(Long userId) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT user_type FROM users WHERE user_id=?";

			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, userId);

			rs = stmt.executeQuery();
			
			if (rs.next()) {
				return UserTypeEnum.valueOf(rs.getInt("user_type"));
			}			

		} catch (SQLException e) {
			throw new RuntimeException("Unable to retrieve user type", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		return null;
	}
	
	@Override
	public boolean canAccess(Long userId) {

		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || principal.getType() == null)
			return false;

		if (principal.isSystem() || principal.getUserId().equals(userId)) {
			return true;
		}

		switch (principal.getType()) {
		case SYSTEM:
			return true;
		case STAFF_USER:
		{
			if (principal.hasPrivilege(PrivilegeEnum.CORPORATE, false)) {
				return true;
			}
			String accessKey = calculateAccessKey(principal, userId);
			Boolean val = accessMap.get(accessKey);
			if (val == null) {
				UserTypeEnum userType = getUserType(userId);

				if (userType == null) {
					return false;
				}
				
				switch(userType) {
					case CORPORATE_ACCOUNT:
					case CORPORATE_PLAYER:
						val = false;
						break;
					default:
						val = true;
				}

				accessMap.put(accessKey, val);
			}
			return val;
		}
		case CORPORATE_ACCOUNT:
		{
			String accessKey = calculateAccessKey(principal, userId);
			Boolean val = accessMap.get(accessKey);
			if (val == null) {
				val = calculateCorporateAccess(principal, userId);

				if (val == null) {
					return false;
				}

				accessMap.put(accessKey, val);
			}
			return val;
		}
		default:
			return false;
		}
	}
}
