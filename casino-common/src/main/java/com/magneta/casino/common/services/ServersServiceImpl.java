package com.magneta.casino.common.services;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchCondition;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.CollectionUtils;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class ServersServiceImpl implements ServersService {

	private static final long TIMEOUT = 30 * 1000;

	private long lastUpdate;
	private List<ServerBean> servers;
	
	private final PrincipalService principalService;

	public ServersServiceImpl(PrincipalService principalService) {
		this.principalService = principalService;
		lastUpdate = 0;
	}

	private synchronized void checkUpdate() throws ServiceException {
		long now = System.currentTimeMillis();

		if (now - lastUpdate < TIMEOUT)
			return;
		
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		List<ServerBean> newServers;
		
		try {
			newServers = new SqlBuilder<ServerBean>(ServerBean.class).findBeans(conn, null, null, null);
		} finally {
			DbUtil.close(conn);
		}

		this.lastUpdate = System.currentTimeMillis();
		this.servers = newServers;
	}


	@Override
	public ServiceResultSet<ServerBean> getServers(int limit, int offset)throws ServiceException {
		return this.getServers(null,null,limit,offset);
	}

	@Override
	public ServiceResultSet<ServerBean> getServers(SearchContext<ServerBean> searchContext,SortContext<ServerBean> sortContext, int limit, int offset)throws ServiceException {
		checkUpdate();
		List<ServerBean> copyList = CollectionUtils.copyList(servers);
		
		if(offset > copyList.size())  {
			throw new InvalidParameterException("offset cannot be bigger than winners size:" + Long.toString(copyList.size()));
		}
		
		if(searchContext != null) {
			SearchCondition<ServerBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}
		
		if(sortContext != null) {
			sortContext.sortList(ServerBean.class, copyList);
		}		
		
		List<ServerBean> beans = new ArrayList<ServerBean>();
		
		ListIterator<ServerBean> iter = copyList.listIterator(offset);
		int myLimit = limit;
		
		if(myLimit != 0) {
			myLimit++;
		}
		while (iter.hasNext() && (myLimit == 0 || beans.size() < myLimit)) {
			beans.add(iter.next());
		}
		
		ServiceResultSet<ServerBean> res = new ServiceResultSet<ServerBean>();
		res.setResult(beans);
		
		if (offset > 0) {
			res.setHasPrev(true);
		}
		
		if (myLimit > 0 && beans.size() > limit) {
			res.setHasNext(true);
			beans.remove(beans.size() - 1);
		}
		return res;
	}

	@Override
	public int getServersSize(SearchContext<ServerBean> searchContext)throws ServiceException {
		checkUpdate();
		List<ServerBean> copyList = CollectionUtils.copyList(servers);
		
		if(searchContext != null) {
			SearchCondition<ServerBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}
		return servers.size();
	}

	@Override
	public ServerBean getServer(int serverId) throws ServiceException {
		checkUpdate();
		List<ServerBean> copyList = CollectionUtils.copyList(servers);
		
		for (ServerBean server: copyList) {
			if (server.getServerId().equals(serverId)) {
				return server;
			}
		}
		
		return null;
	}
	
	@Override
	public void addServer(ServerBean server) throws ServiceException {
		if (!this.principalService.getPrincipal().hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException();
		}

		new SqlBuilder<ServerBean>(ServerBean.class).insertBean(server);
		this.lastUpdate = 0;
	}

	@Override
	public void deleteServer(ServerBean server) throws ServiceException {
		if (!this.principalService.getPrincipal().hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException("Access denied");
		}

		new SqlBuilder<ServerBean>(ServerBean.class).deleteBean(server);
		this.lastUpdate = 0;
	}
}
