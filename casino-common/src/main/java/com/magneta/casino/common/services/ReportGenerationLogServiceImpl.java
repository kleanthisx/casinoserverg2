package com.magneta.casino.common.services;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.services.ReportGenerationLogService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ReportGenerationLogBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class ReportGenerationLogServiceImpl implements ReportGenerationLogService {

	@Override
	public void logReportGeneration(ReportGenerationLogBean reportLog)
			throws ServiceException {
		
		new SqlBuilder<ReportGenerationLogBean>(ReportGenerationLogBean.class).insertBean(reportLog);
	}

	@Override
	public ServiceResultSet<ReportGenerationLogBean> getReportLogs(
			SearchContext<ReportGenerationLogBean> searchContext, int limit,
			int offset, SortContext<ReportGenerationLogBean> sortContext)
			throws ServiceException {

		return new SqlBuilder<ReportGenerationLogBean>(ReportGenerationLogBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}
	
	@Override
	public int getReportLogsSize(
			SearchContext<ReportGenerationLogBean> searchContext)
			throws ServiceException {

		return new SqlBuilder<ReportGenerationLogBean>(ReportGenerationLogBean.class).countBeans(searchContext, null);
	}

}
