package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.common.utils.SqlSearchCondition;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.WebLoginBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class WebLoginsServiceImpl implements WebLoginsService{

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;

	public WebLoginsServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
		
		if(principalService == null) {
			throw new RuntimeException("User principal is null");
		}
	} 

	@Override
	public ServiceResultSet<WebLoginBean> getLogins(Long userId,SearchContext<WebLoginBean> searchContext, int limit,int offset,SortContext<WebLoginBean> sortContext) throws ServiceException{
		
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<WebLoginBean> cond = new SqlSearchCondition<WebLoginBean>("user_id=?", userId);
		
		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).executeQuery( 
				searchContext, 
				cond,
				limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<WebLoginBean> getLogins(Long userId, int limit, int offset) throws ServiceException {
		return this.getLogins(userId, null, limit, offset, null);
	}

	@Override
	public Long recordLogin(WebLoginBean bean) throws ServiceException {
		Long webLoginId = null;
		if(bean == null || bean.getUserId() == null || bean.getApplicationName() == null || bean.getApplicationName().isEmpty()) {
			throw new InvalidParameterException("Error with parameters");
		}
		Connection dbConn = ConnectionFactory.getConnection();

		if (dbConn == null){
			throw new DatabaseException("Could not get a connection to database to recordusers logins");
		}
		PreparedStatement loginRecordStmt = null;
		ResultSet rs = null;
		try {
			loginRecordStmt = dbConn.prepareStatement("INSERT INTO web_logins(user_id,application,client_ip) VALUES(?,?,?) RETURNING web_login_id");
			loginRecordStmt.setLong(1, bean.getUserId());
			loginRecordStmt.setString(2, bean.getApplicationName());
			loginRecordStmt.setString(3, bean.getClientIp());
			rs = loginRecordStmt.executeQuery();
			if(rs.next()) {
				webLoginId = DbUtil.getLong(rs, "web_login_id");
				bean.setWebLoginId(webLoginId);
			}
		} catch (SQLException e) {
			throw new DatabaseException("Error while recording login.", e);
		} finally{
			DbUtil.close(rs);
			DbUtil.close(loginRecordStmt);
			DbUtil.close(dbConn);
		}
		return webLoginId;
	}

	@Override
	public WebLoginBean getLogin(Long webLoginId, Long userId) throws ServiceException {
		
		if(webLoginId == null || webLoginId < 0 || userId == null || userId < 0) {
			throw new InvalidParameterException("parameter problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<WebLoginBean> cond =
			new SqlSearchCondition<WebLoginBean>("web_login_id=? AND user_id=?", webLoginId, userId);
		
		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).findBean(null, cond);
	}

	@Override
	public int getLoginsSize(Long userId,SearchContext<WebLoginBean> searchContext) throws ServiceException {
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<WebLoginBean> cond =
			new SqlSearchCondition<WebLoginBean>("user_id=?", userId);
		
		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).countBeans(searchContext, cond);
	}
	
	@Override
	public ServiceResultSet<WebLoginBean> getLogins(SearchContext<WebLoginBean> searchContext, int limit,int offset,SortContext<WebLoginBean> sortContext) throws ServiceException{
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		
		if (userPrincipal.getType() != UserTypeEnum.STAFF_USER && userPrincipal.getType() != UserTypeEnum.SYSTEM) {
			throw new AccessDeniedException();
		}
		
		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).executeQuery( 
				searchContext, 
				null,
				limit, offset, sortContext);
	}
	
	@Override
	public int getLoginsSize(SearchContext<WebLoginBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		
		if (userPrincipal.getType() != UserTypeEnum.STAFF_USER && userPrincipal.getType() != UserTypeEnum.SYSTEM) {
			throw new AccessDeniedException();
		}
		
		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).countBeans(searchContext, null);
	}

	@Override
	public List<Long> getDistinctLogins(
			SearchContext<WebLoginBean> searchContext, int limit, int offset,
			SortContext<WebLoginBean> sortContext) throws ServiceException {
		

		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).findUniqueValues("userId", Long.class, searchContext, null, sortContext);
	}

	@Override
	public int getDistinctLoginsSize(SearchContext<WebLoginBean> searchContext) throws ServiceException {
		
		return new SqlBuilder<WebLoginBean>(WebLoginBean.class).countUniqueValues("userId", searchContext, null);
	}
}
