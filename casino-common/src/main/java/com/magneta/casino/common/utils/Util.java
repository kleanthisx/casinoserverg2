package com.magneta.casino.common.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class Util {
    
	private static final Logger log = LoggerFactory.getLogger(Util.class);
	
    public static void logReportGeneration(String app, String report, long userID, Date minDate, Date maxDate, String ip, String requestUrl){
        Connection conn = ConnectionFactory.getConnection();
        
        if (conn == null) {
        	log.error("Out of database connections while saving '"+report+"' report generation log.");
        	return;
        }
        
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(
                    "INSERT INTO report_generation_logs (user_id, application_name, report_name, from_date, to_date, user_ip, request_url)" +
                    " VALUES (?,?,?,?,?,?,?)");
            
            stmt.setLong(1, userID);  
            stmt.setString(2, app);
            stmt.setString(3, report);
            DbUtil.setTimestamp(stmt, 4, (minDate != null? new Timestamp(minDate.getTime()): null));
            DbUtil.setTimestamp(stmt, 5, (maxDate != null? new Timestamp(maxDate.getTime()): null));
            stmt.setString(6, ip);
            stmt.setString(7, requestUrl);
            
            stmt.execute();
            
        } catch (SQLException e) {
            log.error("Error while saving '"+report+"' report generation log.", e);
        } finally {
            DbUtil.close(stmt);
            DbUtil.close(conn);
        }
    }
}
