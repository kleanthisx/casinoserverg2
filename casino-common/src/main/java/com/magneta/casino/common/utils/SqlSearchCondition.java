package com.magneta.casino.common.utils;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.magneta.casino.services.search.SearchCondition;

public class SqlSearchCondition<T> implements SearchCondition<T> {

	private final String sql;
	private final Object[] values;
	
	public SqlSearchCondition(String sql, Object... values) {
		this.sql = sql;
		this.values = values;
	}
	
	public SqlSearchCondition(String sql) {
		this.sql = sql;
		this.values = null;
	}
	
	@Override
	public boolean isMet(T pojo) {
		throw new RuntimeException("SqlSearchCondition cannot be used to compare beans");
	}
	
	@Override
	public List<T> findAll(Iterable<T> pojos) {
		throw new RuntimeException("SqlSearchCondition cannot be used to search Collections");
	}

	@Override
	public int countAll(Iterable<T> pojos) {
		throw new RuntimeException("SqlSearchCondition cannot be used to search Collections");
	}
	
	@Override
	public void appendSQLSearchString(Appendable sb) {
		try {
			sb.append(sql);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public int setSQLStatementValues(PreparedStatement stmt, int startIndex)
			throws SQLException {
		
		if (values == null)
			return 0;
		
		int currIndex = startIndex;
		
		for (Object val: values) {
			if (val instanceof String) {
				stmt.setString(currIndex, (String)val);
			} else if (val instanceof Date) {
	    		Date date = (Date)val;
	    		Timestamp timestamp = new Timestamp(date.getTime());
	            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
	            stmt.setTimestamp(currIndex, timestamp, cal);
	    	} else {		 
	    		stmt.setObject(currIndex, val);
	    	}
			
			currIndex++;
		}
		
		return currIndex - startIndex;
	}
	
	@Override
	public String toString() {
		return sql;
	}
}
