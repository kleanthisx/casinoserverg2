package com.magneta.casino.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.annotations.SQLTable;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.PropertySort;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.BeanProperty;
import com.magneta.casino.services.utils.BeanTypeInspector;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

/**
 * Convenience class to create and execute queries for bean classes
 * annotated with SqlTable and SqlColumn
 * @author anarxia
 *
 * @param <T>
 */
public class SqlBuilder<T> {
	
	private static final Logger log = LoggerFactory.getLogger(SqlBuilder.class);

	private final BeanTypeInspector<T> beanSpector;

	public SqlBuilder(Class<T> beanType) {
		this.beanSpector = new BeanTypeInspector<T>(beanType);
	}

	/**
	 * Finds a bean. Uses reports connection.
	 * @param searchContext
	 * @param sqlCondition
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public T findBean(SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();
		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		try {
			return findBean(conn, searchContext, sqlCondition);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	public T findBean(SearchContext<T> searchContext) throws ServiceException {
		return findBean(searchContext, null);
	}


	/**
	 * Appends the columns to select to the given sql statement
	 * @param sql
	 */
	private void appendColumns(StringBuilder sql, BeanLoader<T> loader) {
		List<BeanProperty<T,?>> properties = loader.getProperties();
		boolean first = true;

		for(BeanProperty<T,?> prop: properties) {
			SQLColumn col = prop.getAnnotation(SQLColumn.class);

			if (col == null)
				continue;

			if (!first) {
				sql.append(", ");
			} else {
				first = false;
				sql.append(' ');
			}

			sql.append(loader.getSelectColumnAlias(prop));
		}
	}

	/**
	 * Appends the epilogue for an SQL statement.
	 * The epilogue is the part after WHERE.
	 * @param sb
	 * @param offset Offset. 9
	 * @param limit
	 * @param sortContext
	 * @param allowGroupBy
	 */
	private void appendSqlEpilogue(StringBuilder sb, int offset, int limit, SortContext<T> sortContext, boolean allowGroupBy) {
		
		List<PropertySort<T>> constraints = null;
		
		SQLTable table = beanSpector.getBeanType().getAnnotation(SQLTable.class);
		if (allowGroupBy && table != null && !table.groupBy().isEmpty()) {
			sb.append(" GROUP BY ");
			sb.append(table.groupBy());
		}
		
		if (sortContext != null) {
			constraints = sortContext.getConditions(beanSpector.getBeanType());
		}
		
		if(constraints != null && !constraints.isEmpty()) {
			boolean isFirst = true;

			for (PropertySort<T> s: constraints) {
				if (s.getOrder().equals(PropertySort.SortOrder.NONE)) {
					continue;
				}

				String columnName = s.getSQLColumnName();
				if (columnName == null)
					continue;
				
				if (isFirst) {
					sb.append(" ORDER BY ");
					isFirst = false;
				} else {
					sb.append(", ");
				}

				sb.append(columnName);

				if (s.getOrder() == PropertySort.SortOrder.ASC) {
					sb.append(" ASC NULLS FIRST");
				} else {
					sb.append(" DESC NULLS LAST");
				}
			}
		}

		if (offset > 0) {
			sb.append(" OFFSET ");
			sb.append(offset);
			sb.append(" ROWS");
		}
		
		if (limit > 0) {
			sb.append(" FETCH FIRST ");
			sb.append(limit);
			sb.append(" ROWS ONLY");
		}
	}

	/**
	 * Executes a query to return a bean
	 * @param conn Database connection
	 * @param searchContext Required search context
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public T findBean(Connection conn, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}

		if (searchContext == null) {
			throw new ServiceException("Refusing to execute query with empty search context");
		}

		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);

		StringBuilder sql = new StringBuilder("SELECT");

		appendColumns(sql, loader);
		sql.append(" FROM ");
		sql.append(sqlTable.value());

		appendConditions(sql, searchContext, sqlCondition);
		appendSqlEpilogue(sql, 0, 0, null, true);

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());

			int paramIndex = 1;

			paramIndex += searchContext.getCondition().setSQLStatementValues(stmt, paramIndex);

			if (sqlCondition != null) {
				paramIndex += sqlCondition.setSQLStatementValues(stmt, paramIndex);
			}

			log.debug("findBean query: {}", stmt.toString());
			rs = stmt.executeQuery();

			try {
				if (rs.next()){
					T bean = beanSpector.createBean();
					loader.fill(bean, rs);
					return bean;
				}
			} catch (IllegalArgumentException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (NoSuchMethodException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (InstantiationException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (IllegalAccessException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (InvocationTargetException e) {
				throw new ServiceException("Unable to create bean", e);
			}

		} catch (SQLException e) {
			throw new DatabaseException("Database error while retrieving bean", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return null;
	}

	public List<T> findBeans(SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, SortContext<T> sortContext) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		try {
			return findBeans(conn, searchContext, sqlCondition, sortContext);
		} finally {
			DbUtil.close(conn);
		}
	}

	public List<T> findBeans(Connection conn, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, SortContext<T> sortContext) throws ServiceException {
		return findBeans(conn, searchContext, sqlCondition, 0, 0, sortContext);
	}

	public ServiceResultSet<T> executeQuery(SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, int limit, int offset, SortContext<T> sortContext) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		try {
			return executeQuery(conn, searchContext, sqlCondition, limit, offset, sortContext);
		} catch (SQLException e) {
			throw new DatabaseException("Database error while executing query", e);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	/** Executes an SQL query to return a ServiceResultSet 
	 * 
	 * @param conn
	 * @param searchContext
	 * @param limit
	 * @param offset
	 * @param sortContext
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public List<T> findBeans(Connection conn, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, int limit, int offset, SortContext<T> sortContext) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}

		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);
		List<T> beans = new ArrayList<T>();

		StringBuilder sql = new StringBuilder("SELECT");

		appendColumns(sql, loader);
		sql.append(" FROM ");
		sql.append(sqlTable.value());
		
		appendConditions(sql, searchContext, sqlCondition);
		appendSqlEpilogue(sql, offset, limit, sortContext, true);

		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = conn.prepareStatement(sql.toString());
			
			int paramIndex = 1;

			if(searchContext != null) {
				paramIndex += searchContext.getCondition().setSQLStatementValues(stmt, paramIndex);
			}

			if (sqlCondition != null) {
				paramIndex += sqlCondition.setSQLStatementValues(stmt, paramIndex);
			}

			log.debug("executeQuery SQL: {}", stmt.toString());
			rs = stmt.executeQuery();

			try {
				while (rs.next()){
					beans.add(loader.fill(beanSpector.createBean(), rs));
				}
			} catch (IllegalArgumentException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (NoSuchMethodException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (InstantiationException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (IllegalAccessException e) {
				throw new ServiceException("Unable to create bean", e);
			} catch (InvocationTargetException e) {
				throw new ServiceException("Unable to create bean", e);
			}

		} catch (SQLException e) {
			throw new DatabaseException("Database error while executing SQL", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return beans;
	}

	/** Executes an SQL query to return a ServiceResultSet 
	 * 
	 * @param conn
	 * @param searchContext
	 * @param limit
	 * @param offset
	 * @param sortContext
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public ServiceResultSet<T> executeQuery(Connection conn, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, int limit, int offset, SortContext<T> sortContext) throws ServiceException, SQLException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}

		int myLimit = limit;

		if (limit > 0){
			myLimit++;
		}

		List<T> beans = findBeans(conn, searchContext, sqlCondition, myLimit, offset, sortContext);

		ServiceResultSet<T> res = new ServiceResultSet<T>();
		res.setResult(beans);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (limit > 0 && beans.size() > limit) {
			res.setHasNext(true);
			beans.remove(limit);
		}

		return res;
	}

	private void appendConditions(StringBuilder sql, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException {
		if (searchContext == null && sqlCondition == null) {
			return;
		}

		sql.append(" WHERE ");

		if (searchContext != null) {
			if (sqlCondition != null) {
				sql.append('(');
			}

			searchContext.getCondition().appendSQLSearchString(sql);

			if (sqlCondition != null) {
				sql.append(')');
			}
		}

		if (sqlCondition != null) {
			if (searchContext != null) {
				sql.append(" AND (");
			}

			sqlCondition.appendSQLSearchString(sql);

			if (searchContext != null) {
				sql.append(")");
			}
		}
	}

	private void appendSqlConditionString(StringBuilder sb, BeanProperty<T,?> property) {
		String columnType = null;

		SQLColumn col = property.getAnnotation(SQLColumn.class);
		columnType = col.sqlType();
		if (columnType != null && columnType.trim().isEmpty()) {
			columnType = null;
		}

		sb.append(col.value());
		sb.append('=');

		if(columnType == null) {
			sb.append(" ?");
		} else {
			sb.append(" CAST (? AS ");
			sb.append(columnType);
			sb.append(")");
		}
	}

	public int countBeans(SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		try {
			return countBeans(conn, searchContext, sqlCondition);
		} catch (SQLException e) {
			throw new DatabaseException("Database error while counting beans", e);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	public int countBeans(SearchContext<T> searchContext) throws ServiceException {
		return countBeans(searchContext, null);
	}

	private void setStatementValue(PreparedStatement stmt, int index, Object val) throws SQLException {
		if (val instanceof String) {
			stmt.setString(index, (String)val);
		} else if (val instanceof Date) {
			Date date = (Date)val;
			Timestamp timestamp = new Timestamp(date.getTime());
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
			stmt.setTimestamp(index, timestamp, cal);
		} else {		 
			stmt.setObject(index, val);
		}
	}
	
	private int countGroupByBeans(Connection conn, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException, SQLException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}
		
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM ");
		
		sql.append("(SELECT DISTINCT(");
		sql.append(sqlTable.groupBy());
		sql.append(") FROM ");
		sql.append(sqlTable.value());
		appendConditions(sql, searchContext, sqlCondition);
		appendSqlEpilogue(sql, 0, 0, null, false);
		sql.append(") AS innerq");
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());

			int paramIndex = 1;

			if (searchContext != null) {
				paramIndex += searchContext.getCondition().setSQLStatementValues(stmt, paramIndex);
			}

			if (sqlCondition != null) {
				paramIndex += sqlCondition.setSQLStatementValues(stmt, paramIndex);
			}

			log.debug("countBeans SQL: {}", stmt.toString());
			rs = stmt.executeQuery();

			if (rs.next()){
				return rs.getInt(1);
			}

		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return 0;
	}

	/**
	 * Executes a query to count the number of beans.
	 * @param conn Database connection
	 * @param searchContext Required search context
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public int countBeans(Connection conn, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException, SQLException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}

		String groupBy = sqlTable.groupBy();
		
		if (groupBy != null && !groupBy.trim().isEmpty()) {
			return countGroupByBeans(conn, searchContext, sqlCondition);
		}
		
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM ");
		sql.append(sqlTable.value());

		appendConditions(sql, searchContext, sqlCondition);
		appendSqlEpilogue(sql, 0, 0, null, true);

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());

			int paramIndex = 1;

			if (searchContext != null) {
				paramIndex += searchContext.getCondition().setSQLStatementValues(stmt, paramIndex);
			}

			if (sqlCondition != null) {
				paramIndex += sqlCondition.setSQLStatementValues(stmt, paramIndex);
			}

			log.debug("countBeans SQL: {}", stmt.toString());
			rs = stmt.executeQuery();

			if (rs.next()){
				return rs.getInt(1);
			}

		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return 0;
	}

	/**
	 * Deletes the given bean from the database.
	 * The DELETE statement uses the SQLColumns marked as as isId to select the
	 * rows to delete.
	 * 
	 * @param bean
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public boolean deleteBean(T bean) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}
		
		if (sqlTable.expression()) {
			throw new ServiceException("Cannot create DELETE for table expressions");
		}

		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);

		List<BeanProperty<T,?>> props = loader.getProperties();

		StringBuilder sql = new StringBuilder("DELETE FROM ");
		sql.append(sqlTable.value());

		sql.append(" WHERE ");

		for (BeanProperty<T,?> p: props) {
			SQLColumn col = p.getAnnotation(SQLColumn.class);

			if (!col.isId())
				continue;

			appendSqlConditionString(sql, p);
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(sql.toString());

			int paramIndex = 1;

			for (BeanProperty<T,?> p: props) {
				SQLColumn col = p.getAnnotation(SQLColumn.class);

				if (!col.isId())
					continue;

				setStatementValue(stmt, paramIndex++, p.getValue(bean));
			}

			log.debug("deleteBean SQL: {}", stmt.toString());
			return stmt.executeUpdate() > 0;

		} catch (SQLException e) {
			throw new DatabaseException("Error deleting bean", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}
	
	public int deleteBeans(SearchContext<T> searchContext) throws ServiceException {
		if (searchContext == null) {
			throw new InvalidParameterException("Invalid searchContext");
		}
		
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}
		
		if (sqlTable.expression()) {
			throw new ServiceException("Cannot create DELETE for table expressions");
		}
		
		StringBuilder sql = new StringBuilder("DELETE FROM ");
		sql.append(sqlTable.value());

		appendConditions(sql, searchContext, null);
		
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(sql.toString());
			searchContext.getCondition().setSQLStatementValues(stmt, 1);
			
			return stmt.executeUpdate();
			
		} catch (SQLException e) {
			throw new DatabaseException("Unable to delete beans", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}

	/**
	 * Inserts the given bean in the database.
	 * 
	 * @param bean
	 * @return
	 * @throws ServiceException
	 */
	public void insertBean(T bean) throws ServiceException {
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		
		try {
			insertBean(conn, bean);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	public void insertBean(Connection conn, T bean) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}
		
		if (sqlTable.expression()) {
			throw new ServiceException("Cannot create INSERT for table expressions");
		}

		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);

		List<BeanProperty<T,?>> props = loader.getProperties();

		StringBuilder sql = new StringBuilder("INSERT INTO ");
		sql.append(sqlTable.value());

		int columnCount = 0;

		for (BeanProperty<T,?> p: props) {
			SQLColumn col = p.getAnnotation(SQLColumn.class);

			/* Don't insert values for expressions and NULLs (for defaults to work) */ 
			if (col.expression() || p.getValue(bean) == null) {
				continue;
			}

			if (columnCount == 0) {
				sql.append("(");
			} else{
				sql.append(", ");
			}

			columnCount++;

			sql.append(col.value());
		}

		if (columnCount > 0) {
			sql.append(")");
		}

		columnCount = 0;
		for (BeanProperty<T,?> p: props) {
			SQLColumn col = p.getAnnotation(SQLColumn.class);

			if (col.expression() || p.getValue(bean) == null) {
				continue;
			}

			if (columnCount == 0) {
				sql.append(" VALUES(");
			} else {
				sql.append(", ");
			}

			String columnType = col.sqlType();
			if (columnType != null && columnType.trim().isEmpty()) {
				columnType = null;
			}
			
			if(columnType == null) {
				sql.append(" ?");
			} else {
				sql.append(" CAST (? AS ");
				sql.append(columnType);
				sql.append(")");
			}

			columnCount++;
		}

		if (columnCount > 0) {
			sql.append(")");
		}

		columnCount = 0;
		for (BeanProperty<T,?> p: props) {
			SQLColumn col = p.getAnnotation(SQLColumn.class);

			if (!col.refreshOnInsert() && !col.expression()) {
				continue;
			}

			if (columnCount == 0) {
				sql.append(" RETURNING ");
			} else {
				sql.append(", ");
			}
			sql.append(loader.getSelectColumnAlias(p));
			columnCount++;
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());
			int paramIndex = 1;

			for (BeanProperty<T,?> p: props) {
				SQLColumn col = p.getAnnotation(SQLColumn.class);

				if (col.expression() || p.getValue(bean) == null)
					continue;

				setStatementValue(stmt, paramIndex++, p.getValue(bean));
			}

			log.debug("insertBean SQL: {}", stmt.toString());
			
			if (columnCount > 0) {
				rs = stmt.executeQuery();

				if (rs.next()) {
					for (BeanProperty<T,?> p: props) {
						SQLColumn col = p.getAnnotation(SQLColumn.class);

						if (!col.refreshOnInsert() && !col.expression())
							continue;

						loader.fillProperty(bean, p, rs);
					}
				} else {
					throw new DatabaseException("No rows created");
				}

			} else {
				int cols = stmt.executeUpdate();

				if (cols < 1) {
					throw new DatabaseException("No rows created");
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException("Unable to insert bean", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}
	}
	
	/**
	 * Updates the given bean. All properties are update (except identities). The bean MUST have identity columns.
	 * 
	 * @param bean The bean to update.
	 * @throws ServiceException
	 */
	public void updateBean(T bean) throws ServiceException {
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}
		
		try {
			updateBean(conn, bean);
		} finally {
			DbUtil.close(conn);
		}
	}
	
	public void updateBean(Connection conn, T bean) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}
		
		if (sqlTable.expression()) {
			throw new ServiceException("Cannot create UPDATE for table expressions");
		}
		
		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);

		List<BeanProperty<T,?>> props = loader.getProperties();
		
		StringBuilder sql = new StringBuilder("UPDATE ");
		sql.append(sqlTable.value());
		sql.append(" SET");
		
		boolean isFirst = true;
		
		for (BeanProperty<T,?> p: props) {
			if (p.isReadOnly()) {
				continue;
			}		
			
			SQLColumn col = p.getAnnotation(SQLColumn.class);
			
			if (col == null || col.isId() || col.expression() || col.skipUpdate()) {
				continue;
			}
			
			if (isFirst) {
				isFirst = false;
				sql.append(" ");
			} else {
				sql.append(", ");
			}
			
			sql.append(col.value());
			sql.append(" =");
			
			String columnType = col.sqlType();
			if (columnType != null && columnType.trim().isEmpty()) {
				columnType = null;
			}
			
			if(columnType == null) {
				sql.append(" ?");
			} else {
				sql.append(" CAST (? AS ");
				sql.append(columnType);
				sql.append(")");
			}
		}
	
		if (isFirst == true) {
			throw new ServiceException("No updateable columns in bean");
		}
		
		sql.append(" WHERE");
		
		isFirst = true;

		for (BeanProperty<T,?> p: props) {
			SQLColumn col = p.getAnnotation(SQLColumn.class);
			
			if (col == null || !col.isId()) {
				continue;
			}
			
			if (isFirst) {
				isFirst = false;
			} else {
				sql.append(" AND");
			}
			
			sql.append(" ");
			appendSqlConditionString(sql, p);
		}
		
		if (isFirst == true) {
			throw new ServiceException("Bean does not have id column(s)");
		}
		
		int returnColumnCount = 0;
		
		for (BeanProperty<T,?> p: props) {
			SQLColumn col = p.getAnnotation(SQLColumn.class);

			if (!col.refreshOnUpdate() && !col.expression()) {
				continue;
			}

			if (returnColumnCount == 0) {
				sql.append(" RETURNING ");
			} else {
				sql.append(", ");
			}
			sql.append(loader.getSelectColumnAlias(p));
			returnColumnCount++;
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());
			int paramIndex = 1;

			/* Set properties to update */
			for (BeanProperty<T,?> p: props) {
				if (p.isReadOnly()) {
					continue;
				}		
				
				SQLColumn col = p.getAnnotation(SQLColumn.class);
				
				if (col == null || col.isId() || col.expression() || col.skipUpdate()) {
					continue;
				}

				setStatementValue(stmt, paramIndex++, p.getValue(bean));
			}

			log.debug("updateBean SQL: {}", stmt.toString());
			
			/* Set where properties */
			for (BeanProperty<T,?> p: props) {
				SQLColumn col = p.getAnnotation(SQLColumn.class);
				
				if (col == null || !col.isId()) {
					continue;
				}

				setStatementValue(stmt, paramIndex++, p.getValue(bean));
			}
			
			if (returnColumnCount > 0) {
				rs = stmt.executeQuery();

				if (rs.next()) {
					for (BeanProperty<T,?> p: props) {
						SQLColumn col = p.getAnnotation(SQLColumn.class);

						if (!col.refreshOnUpdate() && !col.expression())
							continue;

						loader.fillProperty(bean, p, rs);
					}
				} else {
					throw new DatabaseException("No rows updated");
				}

			} else {
				int cols = stmt.executeUpdate();

				if (cols < 1) {
					throw new DatabaseException("No rows updated");
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException("Unable to update bean",e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}
	}
	
	public <E> List<E> findUniqueValues(String propertyName, Class<E> propertyType, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, SortContext<T> sortContext) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		try {
			return findUniqueValues(conn, propertyName, propertyType, searchContext, sqlCondition, sortContext);
		} finally {
			DbUtil.close(conn);
		}
	}

	@SuppressWarnings("unchecked")
	public <E> List<E> findUniqueValues(Connection conn, String propertyName, Class<E> propertyType, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition, SortContext<T> sortContext) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}

		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);
		List<E> beans = new ArrayList<E>();

		StringBuilder sql = new StringBuilder("SELECT DISTINCT ");

		BeanProperty<T,?> prop = beanSpector.getProperty(propertyName);
		if (prop == null) {
			throw new InvalidParameterException("Invalid property " + propertyName);
		}
		
		sql.append(loader.getSelectColumnAlias(prop));

		sql.append(" FROM ");
		sql.append(sqlTable.value());

		appendConditions(sql, searchContext, sqlCondition);
		appendSqlEpilogue(sql, 0, 0, sortContext, true);

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());

			int paramIndex = 1;

			if(searchContext != null) {
				paramIndex += searchContext.getCondition().setSQLStatementValues(stmt, paramIndex);
			}

			if (sqlCondition != null) {
				paramIndex += sqlCondition.setSQLStatementValues(stmt, paramIndex);
			}

			
			log.debug("findBeans query: {}", stmt.toString());
			rs = stmt.executeQuery();
			
			while (rs.next()){
				beans.add((E)loader.getValue(prop, rs));
			}

		} catch (SQLException e) {
			throw new DatabaseException("Database error while retrieving beans", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return beans;
	}
	
	public int countUniqueValues(String propertyName, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		try {
			return countUniqueValues(conn, propertyName, searchContext, sqlCondition);
		} finally {
			DbUtil.close(conn);
		}
	}

	public int countUniqueValues(Connection conn, String propertyName, SearchContext<T> searchContext, SqlSearchCondition<T> sqlCondition) throws ServiceException {
		SQLTable sqlTable = beanSpector.getBeanType().getAnnotation(SQLTable.class);

		if (sqlTable == null) {
			throw new ServiceException("Bean class is not annotated with SQLTable");
		}

		int count = 0;
		
		BeanLoader<T> loader = new BeanLoader<T>(beanSpector);

		StringBuilder sql = new StringBuilder("SELECT COUNT(DISTINCT ");

		BeanProperty<T,?> prop = beanSpector.getProperty(propertyName);
		if (prop == null) {
			throw new InvalidParameterException("Invalid property " + propertyName);
		}
		
		sql.append(loader.getSelectColumnAlias(prop));
		sql.append(')');

		sql.append(" FROM ");
		sql.append(sqlTable.value());

		appendConditions(sql, searchContext, sqlCondition);
		appendSqlEpilogue(sql, 0, 0, null, true);

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql.toString());

			int paramIndex = 1;

			if(searchContext != null) {
				paramIndex += searchContext.getCondition().setSQLStatementValues(stmt, paramIndex);
			}

			if (sqlCondition != null) {
				paramIndex += sqlCondition.setSQLStatementValues(stmt, paramIndex);
			}


			log.debug("findBeans query: {}", stmt.toString());
			rs = stmt.executeQuery();
			
			while (rs.next()){
				count += rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new DatabaseException("Database error while retrieving beans", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return count;
	}
}
