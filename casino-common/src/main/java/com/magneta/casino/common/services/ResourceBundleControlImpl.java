package com.magneta.casino.common.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;

public class ResourceBundleControlImpl extends Control {
	
	private static final Logger log = LoggerFactory.getLogger(ResourceBundleControlImpl.class);
	
	private static final long TTL = 60*1000; 
	
	private final String resourcePath;
	
	public ResourceBundleControlImpl() {
		resourcePath = Config.get("resource.path",null);
	}
	
	@Override
	public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
	throws IllegalAccessException, InstantiationException, IOException {
		
		if (!format.equals("java.properties")) {
			return null;
		}
		
		log.info("Requesting {}", baseName);
		
		String bundleName = toBundleName(baseName, locale);

		ResourceBundle bundle = null;

		InputStreamReader reader = null;
		InputStream is = null;
		try {

			if (resourcePath != null) {
				File file = new File(resourcePath, toResourceName(bundleName, "properties"));

				if (file.isFile()) { // Also checks for existence
					is = new FileInputStream(file);
				}
			}
			
			/* Fall-back to resources in casino-common */
			if (is == null) {
				String fullPath = "com/magneta/casino/resources/" + toResourceName(bundleName, "properties");
				
				log.info("Loading {}", fullPath);
				is = loader.getResourceAsStream(fullPath);
			}
			
			if (is != null) {
				reader = new InputStreamReader(is, Charset.forName("UTF-8"));
				bundle = new PropertyResourceBundle(reader);
			}
			
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {}
			}
		}
		return bundle;
	}
	
	@Override
	public long getTimeToLive(String baseName,
            Locale locale) {
		return TTL;
	}
}
