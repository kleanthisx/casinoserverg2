package com.magneta.casino.common.services;

import java.net.URL;

import com.magneta.casino.services.GameImagesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameSymbolBean;
import com.magneta.casino.services.exceptions.InvalidParameterException;

public class GameImagesServiceImpl implements GameImagesService {
	
	private static final String IMAGE_ROOT = "com/magneta/games/images/";
	
	@Override
	public GameSymbolBean getGameImage(Integer gameId,String imageKey) throws ServiceException {
		if(gameId == null || gameId < 0 || imageKey == null || imageKey.isEmpty()) {
			throw new InvalidParameterException("Parameters error");
		}
		URL gameImageUrl = this.getClass().getClassLoader().getResource(IMAGE_ROOT + "game" +  gameId + "/symbols/" + imageKey + ".png");
		
		/* Not in game folder. Try cards */
		if(gameImageUrl == null && imageKey.startsWith("card")) {
			gameImageUrl = this.getClass().getClassLoader().getResource(IMAGE_ROOT + "cards/" + imageKey.substring("card".length()) + ".png");
		}
		
		if (gameImageUrl == null) {
			return null;
		}
		
		GameSymbolBean bean = new GameSymbolBean();
		bean.setSymbolName(imageKey);
		bean.setSymbolUrl(gameImageUrl);
		return bean;
	}
}
