package com.magneta.casino.common.utils.cache;

public class CachedBean<T> {

	private T bean;
	private long lastUpdate;
	
	public CachedBean(T bean) {
		this.bean = bean;
		setLastUpdate(System.currentTimeMillis());
	}
	
	public CachedBean(T bean, long updateTime) {
		this.bean = bean;
		this.setLastUpdate(updateTime);
	}

	public T getBean() {
		return bean;
	}

	public void setLastUpdate(long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public long getLastUpdate() {
		return lastUpdate;
	}
}
