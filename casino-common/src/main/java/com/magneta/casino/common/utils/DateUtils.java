/**
 * 
 */
package com.magneta.casino.common.utils;

import java.util.Date;
import java.util.TimeZone;

/**
 * @author anarxia
 *
 */
public class DateUtils {

    /**
     * Normalises a date retrieved from tapestry components.
     * You should call this method for every date or time retrieved from
     * a DatePicker or TimePicker.
     * 
     * Normalisation is needed because tapestry assumes that all dates
     * entered by the user are in the server timezone, but the user really uses his
     * timezone.
     */
    public static Date getNormalizedDate(TimeZone tz, Date date) {
        
        if (date == null) {
            return null;
        }
        
        int defaultOffset = TimeZone.getDefault().getOffset(date.getTime());
        
        int offset = tz.getOffset(date.getTime());
        
        return new Date(date.getTime() + defaultOffset - offset);
    }
    
    /**
     * Denormalizes a date for display on tapestry pages
     * @param tz
     * @param date
     * @return
     */
    public static Date getDenormalizedDate(TimeZone tz, Date date) {
        
        if (date == null) {
            return null;
        }
        
        int defaultOffset = TimeZone.getDefault().getOffset(date.getTime());
        
        int offset = tz.getOffset(date.getTime());
        
        return new Date(date.getTime() - defaultOffset + offset);
    }
}
