package com.magneta.casino.common.services;

import java.sql.SQLException;
import java.util.List;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporateTransfersService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.AffiliateUsersBean;
import com.magneta.casino.services.beans.CorporateTransferTransactionBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InsufficientFundsException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;

public class CorporateTransfersServiceImpl implements CorporateTransfersService {

	private final UserAccessService userAccessService;
	private final PrincipalService principalService;
	private final CorporatesService corporateService;

	public CorporateTransfersServiceImpl(UserAccessService userAccessService,
			PrincipalService principalService,
			CorporatesService corporateService) {
		this.userAccessService = userAccessService;
		this.principalService = principalService;
		this.corporateService = corporateService;
	}

	@Override
	public CorporateTransferTransactionBean getTransfer(Long transferId)
			throws ServiceException {

		SearchContext<CorporateTransferTransactionBean> searchContext = FiqlContextBuilder.create(CorporateTransferTransactionBean.class, "transferId==?", transferId);

		CorporateTransferTransactionBean bean = new SqlBuilder<CorporateTransferTransactionBean>(CorporateTransferTransactionBean.class).findBean(searchContext, null);

		if (bean == null || !userAccessService.canAccess(bean.getUserId())
				|| !userAccessService.canAccess(bean.getTransferUser())) {
			throw new EntityNotFoundException();
		}

		return bean;
	}

	@Override
	public void createTransfer(CorporateTransferTransactionBean transfer) throws ServiceException {
		if (transfer == null) {
			throw new InvalidParameterException();
		}
		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal.getType() != UserTypeEnum.CORPORATE_ACCOUNT) {
			throw new AccessDeniedException();
		}

		List<AffiliateUsersBean> users = corporateService.getSubAccounts(principal.getUserId(),
				FiqlContextBuilder.create(AffiliateUsersBean.class, "userId==?", transfer.getUserId()), null);		

		if (users.isEmpty()) {
			throw new AccessDeniedException();
		}

		transfer.setTransferUser(principal.getUserId());

		try {
			new SqlBuilder<CorporateTransferTransactionBean>(CorporateTransferTransactionBean.class).insertBean(transfer);
		} catch (DatabaseException e) {
			if (e.getCause() != null && e.getCause() instanceof SQLException) {
				if (e.getMessage().toLowerCase().contains("check_user_balance_positive")) {
					throw new InsufficientFundsException();
				}
			}
		}
	}

}
