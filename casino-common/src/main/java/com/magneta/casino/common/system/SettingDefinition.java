package com.magneta.casino.common.system;

import com.magneta.casino.services.enums.SettingTypeEnum;

public class SettingDefinition {

	private final String key;
	
	private final String description;
	private final String section;
	private final String subSection;
	
	private final SettingTypeEnum type;
	private final boolean hidden;
	
	private final Object defaultValue;
	
	public SettingDefinition(String key, SettingTypeEnum type, Object defaultValue) {
		this(key, type, defaultValue, null, null, null, false);
	}

	public SettingDefinition(String key, SettingTypeEnum type, Object defaultValue, String description, String section, String subSection) {
		this(key, type, defaultValue, description, section, subSection, false);
	}
	
	public SettingDefinition(String key, SettingTypeEnum type, Object defaultValue, String description, String section, String subSection, boolean hidden) {
		this.key = key;
		this.description = description;
		this.section = section;
		this.subSection = subSection;
		this.type = type;
		this.hidden = hidden;
		this.defaultValue = defaultValue;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public boolean isHidden() {
		return hidden;
	}

	public String getSubSection() {
		return subSection;
	}

	public String getSection() {
		return section;
	}

	public String getDescription() {
		return description;
	}

	public String getKey() {
		return key;
	}

	public SettingTypeEnum getType() {
		return type;
	}
}
