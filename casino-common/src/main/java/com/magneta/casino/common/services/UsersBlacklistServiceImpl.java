package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsersBlacklistService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBlacklistBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.SortUtil;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UsersBlacklistServiceImpl implements UsersBlacklistService {

	private final PrincipalService principalService;

	public UsersBlacklistServiceImpl(PrincipalService principalService){
		this.principalService = principalService;
	}

	@Override
	public ServiceResultSet<UserBlacklistBean> getUserBlackLists(SearchContext<UserBlacklistBean> searchContext, int limit,int offset,SortContext<UserBlacklistBean> sortContext) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not get connection to get blacklists");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;


		List<UserBlacklistBean> blacklists = new ArrayList<UserBlacklistBean>();
		try {
			StringBuilder sql = new StringBuilder("SELECT * FROM users_blacklist");
			if(searchContext != null) {
				sql.append(" WHERE ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}

			int myLimit = limit;

			if (limit > 0){
				myLimit++;
			}

			if(sortContext != null) {
				SortUtil.appendSqlEpilogue(sql, sortContext.getConditions(UserBlacklistBean.class), offset, myLimit);
			}else{
				SortUtil.appendSqlEpilogue(sql, null, offset, myLimit);
			}
			stmt = conn.prepareStatement(sql.toString());
			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, 1);
			}
			rs = stmt.executeQuery();
			while (rs.next()){
				UserBlacklistBean bean = new UserBlacklistBean();
				bean.setItemId(DbUtil.getLong(rs,"item_id"));
				bean.setCountryCode(rs.getString("country"));
				bean.setEmail(rs.getString("email"));
				bean.setFirstName(rs.getString("first_name"));
				bean.setLastName(rs.getString("last_name"));
				bean.setUsername(rs.getString("username"));

				blacklists.add(bean);
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL query Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

		ServiceResultSet<UserBlacklistBean> res = new ServiceResultSet<UserBlacklistBean>();
		res.setResult(blacklists);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (limit > 0 && blacklists.size() > limit) {
			res.setHasNext(true);
			blacklists.remove(limit);
		}
		return res;
	}

	@Override
	public UserBlacklistBean getUserBlacklist(Long blackListId) throws ServiceException {
		if(blackListId == null) {
			throw new InvalidParameterException("Parameter blacklistid is required");
		}
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not get connection to get blacklist for id:" + blackListId);
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;



		try {
			StringBuilder sql = new StringBuilder("SELECT * FROM users_blacklist WHERE item_id = ?");
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, blackListId); 
			rs = stmt.executeQuery();
			if (rs.next()){
				UserBlacklistBean bean  = new UserBlacklistBean();
				bean.setItemId(DbUtil.getLong(rs,"item_id"));
				bean.setCountryCode(rs.getString("country"));
				bean.setEmail(rs.getString("email"));
				bean.setFirstName(rs.getString("first_name"));
				bean.setLastName(rs.getString("last_name"));
				bean.setUsername(rs.getString("username"));
				return bean;
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL query Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return null;
	}

	@Override
	public ServiceResultSet<UserBlacklistBean> getUserBlackLists(int limit, int offset) throws ServiceException {
		return this.getUserBlackLists(null,limit,offset,null);
	}

	@Override
	public UserBlacklistBean isUserBlackList(String username, String firstname,String lastname, String email, String countryCode) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not get connection to search if blacklist" );
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT * FROM users_blacklist " +
					" WHERE (username IS NULL OR LOWER(username) = LOWER(?))" +
					" AND (first_name IS NULL OR LOWER(first_name) = LOWER(?))"+
					" AND (last_name IS NULL OR LOWER(last_name) = LOWER(?))" +
					" AND (email IS NULL OR LOWER(email) = LOWER(?))" +
			" AND (country IS NULL OR country = ?)");
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1, username);
			stmt.setString(2, firstname);
			stmt.setString(3, lastname);
			stmt.setString(4, email);
			stmt.setString(5, countryCode);
			rs = stmt.executeQuery();
			if (rs.next()){
				UserBlacklistBean bean = new UserBlacklistBean();
				bean.setItemId(DbUtil.getLong(rs,"item_id"));
				bean.setCountryCode(rs.getString("country"));
				bean.setEmail(rs.getString("email"));
				bean.setFirstName(rs.getString("first_name"));
				bean.setLastName(rs.getString("last_name"));
				bean.setUsername(rs.getString("username"));
				return bean;
			}

		} catch (SQLException e) {
			throw new DatabaseException("Sql query error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return null;
	}

	@Override
	public int getUserBlackListsSize(SearchContext<UserBlacklistBean> searchContext) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();

		if (conn == null) {
			throw new DatabaseException("Could not get connection to get blacklists");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT count(*) FROM users_blacklist");
			if(searchContext != null) {
				sql.append(" WHERE ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}
			stmt = conn.prepareStatement(sql.toString());
			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, 1);
			}
			rs = stmt.executeQuery();
			if (rs.next()){
				return rs.getInt("count");
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL query Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return 0;
	}

	@Override
	public boolean deleteBlacklistUser(UserBlacklistBean userBlacklistBean) throws ServiceException {
		if(userBlacklistBean == null){
			throw new InvalidParameterException();
		}

		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException();
		}
		
		return new SqlBuilder<UserBlacklistBean>(UserBlacklistBean.class).deleteBean(userBlacklistBean);
	}

	@Override
	public void insertBlacklistUser(UserBlacklistBean userBlacklistBean) throws ServiceException {
		if(userBlacklistBean == null){
			throw new InvalidParameterException();
		}

		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException();
		}
		new SqlBuilder<UserBlacklistBean>(UserBlacklistBean.class).insertBean(userBlacklistBean);
	}

	@Override
	public void updateBlacklistUser(UserBlacklistBean userBlacklistBean)throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if(userBlacklistBean == null){
			throw new InvalidParameterException();
		}
		
		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException();
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Database Error");
		}

		PreparedStatement stmt = null;

		try {

			StringBuilder sql = new StringBuilder("UPDATE users_blacklist" +
				" SET username = ?, first_name = ?, last_name = ?, email = ?, country = ?" +
				" WHERE item_id = ?");

			stmt = conn.prepareStatement(sql.toString());

			stmt.setString(1, userBlacklistBean.getUsername());
			stmt.setString(2, userBlacklistBean.getFirstName());
			stmt.setString(3, userBlacklistBean.getLastName());
			stmt.setString(4, userBlacklistBean.getEmail());
			stmt.setString(5, userBlacklistBean.getCountryCode());
			stmt.setLong(6, userBlacklistBean.getItemId());

			int updates = stmt.executeUpdate();
			if(updates <= 0) {
				throw new EntityNotFoundException("User blacklist not found");
			}
		} catch (SQLException e) {
			throw new DatabaseException("SQL query error", e);
		} finally{
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}
}
