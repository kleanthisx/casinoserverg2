package com.magneta.casino.common.services;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.GeoIpService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.LocationBean;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import com.maxmind.geoip.regionName;
import com.maxmind.geoip.timeZone;

public class GeoIpServiceImpl implements GeoIpService {

	private static final Logger log = LoggerFactory.getLogger(GeoIpServiceImpl.class);

	private final CountryService countryService;

	/* GeoIP city database */
	private final LookupService locationLookup;

	public GeoIpServiceImpl(CountryService countryService) {
		this.countryService = countryService;

		String locationDb = Config.get("geoip.db.city");

		if (locationDb != null) {
			LookupService service = null;
			try {
				service = new LookupService(locationDb);
			} catch (IOException e) {
				log.warn("Could not load location GeoIP database");
			}

			locationLookup = service;

		} else {
			locationLookup = null;
		}
	}

	private Location getLocation(String ip) {
		InetAddress addr;
		try {
			addr = InetAddress.getByName(ip);
		}
		catch (UnknownHostException e) {
			return null;
		}
		return getLocation(addr);
	}

	/**
	 * Returns the Location for the IP.
	 * It includes country, city etc.
	 * @param ipAddress
	 * @return
	 */
	private Location getLocation(InetAddress ipAddress) {
		Location loc;

		if (ipAddress == null || locationLookup == null) {
			return null;
		}

		if (ipAddress instanceof Inet6Address) {
			loc = locationLookup.getLocationV6(ipAddress);
		} else if (ipAddress instanceof Inet4Address) {
			loc = locationLookup.getLocation(ipAddress);
		} else {
			return null;
		}

		if (loc == null) {
			return null;
		}

		if (loc.countryCode == null || loc.countryCode.startsWith("-") ) {
			return null;
		}

		return loc;
	}

	private String getTimezone(Location loc) {
		if (loc == null || loc.countryCode == null) {
			return null;
		}

		String tz = timeZone.timeZoneByCountryAndRegion(loc.countryCode, loc.region);

		if (tz == null || tz.isEmpty()) {
			return null;
		}

		return tz;
	}

	private String getRegion(Location loc) {
		if (loc == null || loc.countryCode == null || loc.region == null) {
			return null;
		}

		String region = regionName.regionNameByCode(loc.countryCode, loc.region);


		if (region == null || region.isEmpty()) {
			return null;
		}

		return region;
	}

	@Override
	public LocationBean getLocationFromIp(String ip) throws ServiceException {
		if(ip == null || ip.isEmpty()) {
			throw new InvalidParameterException("IP parameters problem");
		}

		Location loc = getLocation(ip);

		if (loc == null) {
			return null;
		}

		CountryBean c = countryService.getCountry(loc.countryCode);
		if (c == null) {
			return null;
		}

		LocationBean bean = new LocationBean(c);
		bean.setCity(loc.city);

		bean.setRegion(getRegion(loc));
		bean.setTimeZone(getTimezone(loc));

		return bean;
	}

}
