package com.magneta.casino.common.utils.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanCache<T,E> {

	private Map<T,CachedBean<E>> beanCache;
	private final long ttl;
	
	public BeanCache(long ttl) {
		this.ttl = ttl;
		this.beanCache = new ConcurrentHashMap<T,CachedBean<E>>();
	}
	
	public E get(T key) { 
		CachedBean<E> bean = getEntry(key);
		
		if (bean == null) {
			return null;
		}
		
		return bean.getBean();
	}
	
	public CachedBean<E> getEntry(T key) { 
		CachedBean<E> bean = beanCache.get(key);
		
		if (bean == null) {
			return null;
		}
		
		if (System.currentTimeMillis() - bean.getLastUpdate() > ttl) {
			beanCache.remove(key);
			return null;
		}
		
		return bean;
	}
	
	public void put(T key, E value) {
		beanCache.put(key, new CachedBean<E>(value));
	}
	
	public void remove(T key) {
		beanCache.remove(key);
	}
}
