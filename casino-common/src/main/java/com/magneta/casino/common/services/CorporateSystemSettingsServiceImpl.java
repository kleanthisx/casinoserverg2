package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporateSystemSettingsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class CorporateSystemSettingsServiceImpl implements CorporateSystemSettingsService {

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	
	public CorporateSystemSettingsServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
	}
	
	@Override
	public String getSetting(Long corporateId, String key)
			throws ServiceException {
		
		if (!userAccessService.canAccess(corporateId)) {
			throw new EntityNotFoundException("Corporate not found");
		}

		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, false)) {
			throw new AccessDeniedException("Access denied");
		}
		
		Connection conn = ConnectionFactory.getReportsConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get database connection");
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT setting_value FROM corporate_system_settings WHERE corporate_id=? AND setting_key=?";
			
			stmt = conn.prepareStatement(sql);
			
			stmt.setLong(1, corporateId);
			stmt.setString(2, key);
			
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				return rs.getString(1);
			} 
			
		} catch (SQLException e) {
			throw new DatabaseException("Error while retrieving setting", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		return null;
	}
	
	private final void insertSetting(Connection conn, Long corporateId, String key, String value) throws ServiceException {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO corporate_system_settings(corporate_id, setting_key, setting_value) VALUES(?,?,?)";

			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, corporateId);
			stmt.setString(2, key);
			stmt.setString(3, value);

			stmt.executeUpdate();

		} catch (SQLException e) {
			throw new DatabaseException("Error while removing setting", e);
		} finally {
			DbUtil.close(stmt);
		}
	}

	@Override
	public void setSetting(Long corporateId, String key, String value)
			throws ServiceException {
		if (!userAccessService.canAccess(corporateId)) {
			throw new EntityNotFoundException("Corporate not found");
		}

		ExtendedPrincipalBean principal = principalService.getPrincipal();

		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		if (value == null) {
			removeSetting(corporateId, key);
			return;
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get database connection");
		}

		boolean needsInsert = false;
		try {
			PreparedStatement stmt = null;
			try {
				String sql = "UPDATE corporate_system_settings SET setting_value=? WHERE corporate_id=? AND setting_key=?";

				stmt = conn.prepareStatement(sql);

				stmt.setString(1, value);
				stmt.setLong(2, corporateId);
				stmt.setString(3, key);

				if (stmt.executeUpdate() < 1) {
					needsInsert=true;
				}

			} catch (SQLException e) {
				throw new DatabaseException("Error while removing setting", e);
			} finally {
				DbUtil.close(stmt);
			}
			
			if (needsInsert) {
				insertSetting(conn, corporateId, key, value);
			}
			
		} finally {
			DbUtil.close(conn);
		}

	}

	@Override
	public void removeSetting(Long corporateId, String key)
			throws ServiceException {
		if (!userAccessService.canAccess(corporateId)) {
			throw new EntityNotFoundException("Corporate not found");
		}
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get database connection");
		}
		
		PreparedStatement stmt = null;
		try {
			String sql = "DELETE FROM corporate_system_settings WHERE corporate_id=? AND setting_key=?";
			
			stmt = conn.prepareStatement(sql);
			
			stmt.setLong(1, corporateId);
			stmt.setString(2, key);
			
			stmt.executeUpdate();

		} catch (SQLException e) {
			throw new DatabaseException("Error while removing setting", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
	}

}
