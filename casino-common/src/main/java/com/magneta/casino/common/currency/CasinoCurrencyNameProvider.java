package com.magneta.casino.common.currency;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.spi.CurrencyNameProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CasinoCurrencyNameProvider extends CurrencyNameProvider {

	private static final Logger log = LoggerFactory.getLogger(CasinoCurrencyNameProvider.class);

	private final Properties currencySymbols;

	public CasinoCurrencyNameProvider() {
		
		currencySymbols = new Properties();

		InputStream in = this.getClass().getResourceAsStream("symbols.properties");
		if (in == null) {
			log.warn("Currency symbols file missing. Using Java runtime symbols.");
			return;
		}
		
		try {
			currencySymbols.load(new InputStreamReader(in, Charset.forName("UTF-8")));
		} catch (IOException e) {
			log.error("Unable to read currency symbol from properties file.", e);
			return;
		} finally {
			try {
				in.close();
			} catch (IOException e) {
			}
		}
	}

	@Override
	public String getSymbol(String currencyCode, Locale locale) {
		return currencySymbols.getProperty(currencyCode);
	}

	@Override
	public Locale[] getAvailableLocales() {
		return Locale.getAvailableLocales();
	}
	
	public static void register() {
		ServiceLoader.load(CurrencyNameProvider.class);
	}
}
