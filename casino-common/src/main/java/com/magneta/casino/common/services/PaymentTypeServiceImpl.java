package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class PaymentTypeServiceImpl implements PaymentTypeService {
	@Override
	public ServiceResultSet<PaymentTypeBean> getPaymentTypes(SearchContext<PaymentTypeBean> searchContext,int limit,int offset,SortContext<PaymentTypeBean> sortContext) throws ServiceException {
		Connection conn = ConnectionFactory.getConnection();
		try {
			return new SqlBuilder<PaymentTypeBean>(PaymentTypeBean.class).executeQuery(conn, searchContext, null, limit, offset, sortContext);
		} catch (SQLException e) {
			throw new DatabaseException("Database error while retrieving payment types", e);
		} finally {
			DbUtil.close(conn);
		}
	}

	@Override
	public PaymentTypeBean getPaymentType(String type) throws ServiceException {
		if(type == null ||  type.isEmpty()) {
			return null;
		}
		SearchContext<PaymentTypeBean> searchContext = new FiqlContextBuilder().build(PaymentTypeBean.class, "paymentType==?", type);
		return new SqlBuilder<PaymentTypeBean>(PaymentTypeBean.class).findBean(searchContext, null);
	}

	@Override
	public PaymentTypeBean createPaymentType(PaymentTypeBean paymentTypeBean) throws ServiceException {
		if(paymentTypeBean == null){
			throw new InvalidParameterException();
		}
		new SqlBuilder<PaymentTypeBean>(PaymentTypeBean.class).insertBean(paymentTypeBean);
		
		return paymentTypeBean;
	}

	@Override
	public void updatePaymentType(PaymentTypeBean paymentTypeBean) throws ServiceException {

		if(paymentTypeBean ==  null) {
			throw new InvalidParameterException();
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get a connection to database to update payment type");
		}

		PreparedStatement stmt = null;
		try {
			StringBuilder sql = new StringBuilder("UPDATE payment_type SET enabled = ? WHERE payment_type = ?");

			stmt = conn.prepareStatement(sql.toString());
			stmt.setBoolean(1, paymentTypeBean.isEnabled());
			stmt.setString(2, paymentTypeBean.getPaymentType());
			
			int updates = stmt.executeUpdate();
			if(updates <= 0) {
				throw new EntityNotFoundException("Payment type not found");
			}
		} catch (SQLException e) {
			throw new DatabaseException("SQL query error", e);
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}			
	}

	@Override
	public int getPaymentTypesSize(SearchContext<PaymentTypeBean> searchContext) throws ServiceException {
		return new SqlBuilder<PaymentTypeBean>(PaymentTypeBean.class).countBeans(searchContext, null);
	}
}
