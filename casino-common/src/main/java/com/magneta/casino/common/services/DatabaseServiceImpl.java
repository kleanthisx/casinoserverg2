package com.magneta.casino.common.services;

import java.sql.Connection;

import com.magneta.casino.internal.services.DatabaseService;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.db.ConnectionFactory;

public class DatabaseServiceImpl implements DatabaseService {

	@Override
	public Connection getConnection(boolean readOnly) throws DatabaseException {
		Connection conn = null;
		
		if (readOnly) {
			conn = ConnectionFactory.getReportsConnection();
		} else {
			conn = ConnectionFactory.getConnection();
		}
		
		if (conn == null) {
			throw new DatabaseException("Cannot get connection to database");
		}
		return conn;
	}
}
