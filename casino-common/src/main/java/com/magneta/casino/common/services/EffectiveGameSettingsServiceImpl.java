package com.magneta.casino.common.services;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.services.CorporateGameSettingsService;
import com.magneta.casino.services.EffectiveGameSettingsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CorporateGameSettingBean;
import com.magneta.casino.services.beans.EffectiveGameSettingsBean;
import com.magneta.casino.services.beans.GameSettingBean;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;

public class EffectiveGameSettingsServiceImpl implements EffectiveGameSettingsService {

	private final CorporateGameSettingsService corporateGameSettingsService;

	public EffectiveGameSettingsServiceImpl(CorporateGameSettingsService corporateGameSettingsService) {
		this.corporateGameSettingsService = corporateGameSettingsService;
	}
	
	@Override
	public EffectiveGameSettingsBean getEffectiveGameSettings(Long userId, Long corporateId,
			Integer gameId) throws ServiceException {
		
		Double minBet = null;
		Double maxBet = null;
		
		if (corporateId != null) {
			CorporateGameSettingBean corporateSettings = corporateGameSettingsService.getEffectiveCorporateGameSettings(corporateId, gameId);

			if (corporateSettings != null)  {
				minBet = corporateSettings.getMinBet();
				maxBet = corporateSettings.getMaxBet();
			}
		}
		
		if (minBet == null || maxBet == null) {
			SearchContext<GameSettingBean> categorySearchContext = FiqlContextBuilder.create(GameSettingBean.class, "gameId==?", gameId);

			GameSettingBean gameSettings = new SqlBuilder<GameSettingBean>(GameSettingBean.class).findBean(categorySearchContext, null);
			
			if (gameSettings != null)  {
				if (minBet == null) {
					minBet = gameSettings.getMinBet();
				}
				
				if (maxBet == null) {
					maxBet = gameSettings.getMaxBet();
				}
			}
		}
		
		if (minBet != null || maxBet != null) {
			EffectiveGameSettingsBean bean = new EffectiveGameSettingsBean();
			bean.setMinBet(minBet);
			bean.setMaxBet(maxBet);
			return bean;
		}
		return null;
	}

}
