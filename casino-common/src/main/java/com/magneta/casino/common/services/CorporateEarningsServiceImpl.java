package com.magneta.casino.common.services;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.CorporateEarningsTransactionBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;

public class CorporateEarningsServiceImpl implements CorporateEarningsService {

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	
	public CorporateEarningsServiceImpl(PrincipalService principalService,
			 UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
	}
	
	@Override
	public CorporateEarningsBean getEarnings(Long corporateId) throws ServiceException {
		if(corporateId == null) {
			throw new InvalidParameterException("CorporateId is required");
		}
		
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException("Access denied");
		}
		
		if (!userAccessService.canAccess(corporateId)) {
			throw new AccessDeniedException("Access denied");
		}
		
		SearchContext<CorporateEarningsBean> searchContext =
				FiqlContextBuilder.create(CorporateEarningsBean.class, "corporateId==?", corporateId);
		
		return new SqlBuilder<CorporateEarningsBean>(CorporateEarningsBean.class).findBean(searchContext);
	}
	
	@Override
	public void transferEarnings(CorporateEarningsTransactionBean transaction) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException("Access denied");
		}
		
		if (!principal.hasPrivilege(PrivilegeEnum.FUNDS, true)) {
			if (!userAccessService.canAccess(transaction.getCorporateId())) {
				throw new AccessDeniedException("Access denied");
			}
			
			if (transaction.getCorporateId().equals(principal.getUserId())) {
				throw new AccessDeniedException("Access denied");
			}
		}
		
		if (!principal.isSystem()) {
			transaction.setInitiatorUserId(principal.getUserId());
		}

		new SqlBuilder<CorporateEarningsTransactionBean>(CorporateEarningsTransactionBean.class).insertBean(transaction);
		
	}

}
