package com.magneta.casino.common.utils;

import java.lang.reflect.Method;
import java.util.Date;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.services.annotations.SQLColumn;
import com.magneta.casino.services.utils.BeanProperty;
import com.magneta.casino.services.utils.BeanTypeInspector;
import com.magneta.casino.services.utils.BeanUtil;
import com.magneta.db.DbUtil;

/**
 *  
 * Loads a bean from a resultset 
 * 
 */
public class BeanLoader<T> {

	private final List<BeanProperty<T,?>> properties;

	public static <E> BeanLoader<E> create(Class<E> type) {
		return new BeanLoader<E>(type);
	}
	
	public BeanLoader(BeanTypeInspector<T> beanSpector) {
		properties = new ArrayList<BeanProperty<T,?>>();
		Method[] methods = beanSpector.getBeanType().getMethods();

		for (Method m: methods) {
			SQLColumn col = m.getAnnotation(SQLColumn.class);

			if (col == null)
				continue;

			String propName = BeanUtil.getPropertyName(m);
			BeanProperty<T,?> prop = beanSpector.getProperty(propName);
			if (prop != null) {
				properties.add(prop);
			}
		}
	}
	
	public BeanLoader(Class<T> beanType) {
		this(new BeanTypeInspector<T>(beanType));
	}

	/**
	 * Returns a list of bean properties
	 * @return
	 */
	public List<BeanProperty<T,?>> getProperties() {
		return this.properties;
	}
	
	/**
	 * Returns the name of the column in a ResultSet for the given property.
	 * @param prop
	 * @return
	 */
	public String getColumnAlias(BeanProperty<T,?> prop) {
		SQLColumn col = prop.getAnnotation(SQLColumn.class);
		
		if (col.expression()) {
			return "ALIAS_" + prop.getName();
		}

		return col.value();
	}
	
	/**
	 * Returns the column in a SELECT statement for the given property.
	 * @param prop
	 * @return
	 */
	public String getSelectColumnAlias(BeanProperty<T,?> prop) {
		SQLColumn col = prop.getAnnotation(SQLColumn.class);
		
		if (col.expression()) {
			return "(" + col.value() + ") AS ALIAS_" + prop.getName();
		} 
		
		return col.value();
	}
	
	public void fillProperty(T bean, BeanProperty<T,?> prop, ResultSet rs) {
		try {
			prop.setValue(bean, getValue(prop, rs));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public Object getValue(BeanProperty<T,?> prop, ResultSet rs) throws SQLException {
		String colName = getColumnAlias(prop);

		if (prop.getType().equals(String.class)) {
			return rs.getString(colName);
		} else if (prop.getType().equals(Long.TYPE)) {
			return rs.getLong(colName);
		} else if (prop.getType().equals(Integer.TYPE)) {
			return rs.getInt(colName);
		} else if (prop.getType().equals(Short.TYPE)) {
			return rs.getShort(colName);
		} else if (prop.getType().equals(Boolean.TYPE)) {
			return rs.getBoolean(colName);
		} else if (prop.getType().equals(Double.TYPE)) {
			return rs.getDouble(colName);
		} else if (prop.getType().equals(Long.class)) {
			return DbUtil.getLong(rs, colName);
		} else if (prop.getType().equals(Integer.class)) {
			return DbUtil.getInteger(rs, colName);
		} else if (prop.getType().equals(Boolean.class)) {
			return DbUtil.getBoolean(rs, colName);
		} else if (prop.getType().equals(Double.class)) {
			return DbUtil.getDouble(rs, colName);
		} else if (prop.getType().equals(Date.class)) {	
			Timestamp ts = DbUtil.getTimestamp(rs, colName);

			if (ts == null) {
				return null;
			}

			return new Date(ts.getTime());
			
		} else if (prop.getType().equals(byte[].class)) {
			return rs.getBytes(colName);
		} else if (prop.getType().isArray()) {
			Array arr = rs.getArray(colName);

			if (arr == null || rs.wasNull()) {
				return null;
			} 
			
			return arr.getArray();
		} else {
			return rs.getObject(colName);
		}
	}
	
	/** 
	 * Fills the given bean from the ResultSet current record.
	 * @param bean
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	public T fill(T bean, ResultSet rs) throws SQLException {
		for (BeanProperty<T,?> prop: properties) {
			fillProperty(bean, prop, rs);
		}
		return bean;
	}
}
