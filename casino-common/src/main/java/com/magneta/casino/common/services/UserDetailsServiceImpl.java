package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.BeanLoader;
import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateNicknameException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.FinancialInconsistencyException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.SortUtil;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserDetailsServiceImpl implements UserDetailsService {

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	
	public UserDetailsServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		
		this.principalService = principalService;
		this.userAccessService = userAccessService;
		
		if(principalService == null) {
			throw new RuntimeException("User principal is null");
		}
	}
	@Override
	public UserDetailsBean getUser(Long userId) throws ServiceException{
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		if (userId == null || userId <= 0) {
			throw new InvalidParameterException("ERROR with userid parameter");
		}
		
		if (!userAccessService.canAccess(userId)) {
			return null;
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get a connection to database to get user");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT * FROM users WHERE user_id=?");
			
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, userId);
			rs = stmt.executeQuery();

			if (rs.next()) {
				UserDetailsBean user = new UserDetailsBean();

				user.setId(DbUtil.getLong(rs,"user_id"));
				user.setIsActive(DbUtil.getBoolean(rs,"is_active"));
				user.setIsClosed(DbUtil.getBoolean(rs,"closed"));

				user.setCountryCode(rs.getString("country"));
				user.setEmail(rs.getString("email"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setMiddleName(rs.getString("middle_name"));
				user.setNickName(rs.getString("nickname"));
				user.setPhone(rs.getString("phone"));
				user.setPostalAddress(rs.getString("postal_address"));
				user.setPostalCode(rs.getString("postal_code"));
				user.setRegion(rs.getString("region"));
				user.setRegisteredDate(DbUtil.getTimestamp(rs, "register_date"));
				user.setTown(rs.getString("town"));
				user.setUserName(rs.getString("username"));
				user.setUserType(DbUtil.getInteger(rs,"user_type"));
				user.setTimezone(rs.getString("time_zone"));
				user.setModifierUser(DbUtil.getLong(rs, "modifier_user"));
				return user;
			}

		} catch (Exception e) {
			throw new DatabaseException("SQL query error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

		return null;
	}

	private String getSearchByRestrictionsSqlQuery(SearchContext<UserDetailsBean> searchContext,SortContext<UserDetailsBean> sortContext,int limit,int offset,ExtendedPrincipalBean userPrincipal) throws ServiceException {

		StringBuilder sql = new StringBuilder("SELECT * FROM users");
		
		switch (userPrincipal.getType()) {
			case CORPORATE_ACCOUNT:
				sql.append(" INNER JOIN corp_sub_accounts(?) AS subs(sub_id) ON users.user_id = subs.sub_id WHERE 1=1");
				break;
			case SYSTEM:
			case STAFF_USER:
				sql.append(" WHERE 1=1");
				break;
			default:
				sql.append(" WHERE user_id=?");
		}

		if(searchContext != null) {
			sql.append(" AND ");
			searchContext.getCondition().appendSQLSearchString(sql);
		}
		if(sortContext != null) {
			SortUtil.appendSqlEpilogue(sql, sortContext.getConditions(UserDetailsBean.class), offset, limit);
		}else{
			SortUtil.appendSqlEpilogue(sql, null,offset,limit);
		}
		return sql.toString();
	}

	@Override
	public ServiceResultSet<UserDetailsBean> getUsers(SearchContext<UserDetailsBean> searchContext, int limit,int offset,SortContext<UserDetailsBean> sortContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null){
			throw new DatabaseException("Could not get a connection to database to get user");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;

		List<UserDetailsBean> userDetailsBeanList = new ArrayList<UserDetailsBean>();
		try {
			int myLimit = limit;
			
			if (limit > 0){
				myLimit++;
			}
			stmt = conn.prepareStatement(this.getSearchByRestrictionsSqlQuery(searchContext,sortContext,myLimit,offset,userPrincipal));
			int num = 1;
			
			switch (userPrincipal.getType()) {
			case CORPORATE_ACCOUNT:
				stmt.setLong(num++, userPrincipal.getUserId());
				break;
			case SYSTEM:
			case STAFF_USER:
				break;
			default:
				stmt.setLong(num++, userPrincipal.getUserId());
			}
			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}

			rs = stmt.executeQuery();
			
			BeanLoader<UserDetailsBean> loader = new BeanLoader<UserDetailsBean>(UserDetailsBean.class);
			while(rs.next()) {
				UserDetailsBean user = new UserDetailsBean();
				loader.fill(user, rs);
				userDetailsBeanList.add(user);
			}
		}catch(SQLException ex) {
			throw new DatabaseException("Sql Query Exception", ex);
		}finally{
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		ServiceResultSet<UserDetailsBean> res = new ServiceResultSet<UserDetailsBean>();
		res.setResult(userDetailsBeanList);
		
		if (offset > 0) {
			res.setHasPrev(true);
		}
		
		if (limit > 0 && userDetailsBeanList.size() > limit) {
			res.setHasNext(true);
			userDetailsBeanList.remove(limit);
		}
		return res;
	}

	@Override
	public ServiceResultSet<UserDetailsBean> getUsers(int limit, int offset) throws ServiceException {
		return this.getUsers(null, limit, offset, null);
	}

	@Override
	public void updateUser(UserDetailsBean user) throws ServiceException {
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		if (user == null) {
			throw new InvalidParameterException("user is required");
		}

		if (!userAccessService.canAccess(user.getId())) {
			throw new EntityNotFoundException("user not found");
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Database Error");
		}

		PreparedStatement stmt = null;

		try {
			StringBuilder sql = new StringBuilder("UPDATE users SET is_active = ?,closed = ?,country = ?," +
			" email = ?,first_name = ?,last_name = ?,middle_name = ?,nickname = ?," +
			" phone = ?,postal_address=?,postal_code=?,region=?,town=?,time_zone=?,modifier_user=?" +
			" WHERE user_id = ?");

			stmt = conn.prepareStatement(sql.toString());
			if(user.getIsActive() == null) {
				stmt.setBoolean(1, false);
			}else{
				stmt.setBoolean(1, user.getIsActive());
			}
			if(user.getIsClosed() == null) {
				stmt.setBoolean(2, false);
			}else{
				stmt.setBoolean(2, user.getIsClosed());
			}
			stmt.setString(3, user.getCountryCode());
			stmt.setString(4, user.getEmail());
			stmt.setString(5, user.getFirstName());
			stmt.setString(6, user.getLastName());
			stmt.setString(7, user.getMiddleName());
			stmt.setString(8, user.getNickName());
			stmt.setString(9, user.getPhone());
			stmt.setString(10, user.getPostalAddress());
			stmt.setString(11, user.getPostalCode());
			stmt.setString(12, user.getRegion());
			stmt.setString(13, user.getTown());
			stmt.setString(14, user.getTimezone());
			DbUtil.setLong(stmt, 15, userPrincipal.getUserId());
			stmt.setLong(16, user.getId());

			int size = stmt.executeUpdate();		
			if(size != 1) {
				throw new DatabaseException("Error updating user with id:" + Long.toString(user.getId()));
			}
		} catch (SQLException e) {
			if (e.getMessage().contains("_username")) {
				throw new DuplicateUsernameException("Username already exists", e);
			} else if (e.getMessage().contains("_nickname")) {
				throw new DuplicateNicknameException();
			} else if (e.getMessage().contains("_email")) {
				throw new DuplicateEmailException();
			} else {
				throw new DatabaseException("Database Error", e);
			}

		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}
	
	@Override
	public UserBalanceBean getUserBalance(Long userId) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		if(userId == null || userId <= 0) {
			throw new InvalidParameterException("Userid is required");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new EntityNotFoundException();
		}
		
		return new SqlBuilder<UserBalanceBean>(UserBalanceBean.class).findBean(
				FiqlContextBuilder.create(UserBalanceBean.class, "userId==?", userId));
	}
	
	@Override
	public List<UserBalanceBean> getUserBalances(List<Long> userIds) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		if(userIds == null) {
			throw new InvalidParameterException("Userid is required");
		}
		
		for (Long userId: userIds) {
			if (!userAccessService.canAccess(userId)) {
				throw new EntityNotFoundException();
			}
		}
		
		return new SqlBuilder<UserBalanceBean>(UserBalanceBean.class).findBeans(
				FiqlContextBuilder.create(UserBalanceBean.class, "userId=in=?", userIds), null, null);
	}

	@Override
	public int getUsersSize(SearchContext<UserDetailsBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null){
			throw new DatabaseException("Could not get a connection to database to get user");
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			
			StringBuilder sql = new StringBuilder("SELECT count(*) FROM users");
					
			switch (userPrincipal.getType()) {
			case CORPORATE_ACCOUNT:
				sql.append(" INNER JOIN corp_sub_accounts(?) AS subs(sub_id) ON users.user_id = subs.sub_id WHERE 1=1");
				break;
			case SYSTEM:
			case STAFF_USER:
				sql.append(" WHERE 1=1");
				break;
			default:
				sql.append(" WHERE user_id=?");
			}
			
			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}
		
			stmt = conn.prepareStatement(sql.toString());
			
			int num = 1;
			
			switch (userPrincipal.getType()) {
			case CORPORATE_ACCOUNT:
				stmt.setLong(num++, userPrincipal.getUserId());
				break;
			case SYSTEM:
			case STAFF_USER:
				break;
			default:
				stmt.setLong(num++, userPrincipal.getUserId());
			}

			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				return rs.getInt("count");
			}
		}catch(SQLException ex) {
			throw new DatabaseException("Sql Query Exception", ex);
		}finally{
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return 0;
	}

	@Override
	public boolean deleteUser(UserDetailsBean userDetailsBean) throws ServiceException {
		if(userDetailsBean == null){
			throw new InvalidParameterException("Error with parameters");
		}
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		
		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException();
		}
		
		return new SqlBuilder<UserDetailsBean>(UserDetailsBean.class).deleteBean(userDetailsBean);
	}
	
	@Override
	public void closeAccount(Long userId) throws ServiceException {
		
		UserDetailsBean user = getUser(userId);
		
		if (user == null) {
			throw new AccessDeniedException();
		}
		
		if (user.getIsClosed() == true) {
			return;
		}
		
		UserBalanceBean balance = getUserBalance(userId);
		
		if (balance.getBalance() > 0.009) {
			throw new FinancialInconsistencyException("User has balance");
		}
		
		if (user.getUserTypeEnum() == UserTypeEnum.CORPORATE_ACCOUNT) {
			try {
				if (hasFinancialObligations(userId)) {
					throw new FinancialInconsistencyException("User has balance");
				}
			} catch (SQLException e) {
				throw new DatabaseException("Unable to check for financial obligations", e);
			}
		}
		
		try {
			close(userId);
		} catch (SQLException e) {
			throw new DatabaseException("Cannot close user account", e);
		}
	}
	
	private boolean hasFinancialObligations(long corporateId) throws SQLException {
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new SQLException("Out of database connections");
		}
		
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT SUM(balance) As balance, SUM(CASE WHEN earnings > 0 THEN earnings ELSE 0 END) AS earnings, SUM(CASE WHEN earnings < 0 THEN earnings * -1 ELSE 0 END) AS neg_earnings" + 
					" FROM user_balance LEFT OUTER JOIN affiliate_earnings ON user_balance.user_id=affiliate_earnings.affiliate_id" +
					" WHERE user_balance.user_id IN (SELECT corp_sub_accounts(?) UNION ALL SELECT ?)";
			
			statement = conn.prepareStatement(sql);
			statement.setLong(1, corporateId);
			statement.setLong(2, corporateId);

			rs = statement.executeQuery();
			
			if (rs.next()) {
				return rs.getDouble("balance") > 0.009 || rs.getDouble("earnings") > 0.009 || rs.getDouble("neg_earnings") > 0.009;
			}			

		} finally {
			DbUtil.close(rs);
			DbUtil.close(statement);
			DbUtil.close(conn);
		}
		
		return false;
	}
	
	private final boolean close(long userId) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new SQLException("Out of database connections");
		}
		
		PreparedStatement statement = null;
		try{

			String sql = 
				"UPDATE users " +
				" SET closed = TRUE, modifier_user = ?" +
				" WHERE user_id = ?";

			statement = conn.prepareStatement(sql);
			DbUtil.setLong(statement, 1, principalService.getPrincipal().getUserId());
			statement.setLong(2, userId);

			return statement.executeUpdate() == 1;				

		} finally {
			DbUtil.close(statement);
			DbUtil.close(conn);
		}
	}
}
