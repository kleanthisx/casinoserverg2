package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.common.utils.SqlSearchCondition;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserLoginsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserLoginBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserLoginsServiceImpl implements UserLoginsService {

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	
	public UserLoginsServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
		if(principalService == null) {
			throw new RuntimeException("User principal is null");
		}
	} 

	@Override
	public ServiceResultSet<UserLoginBean> getLogins(Long userId,SearchContext<UserLoginBean> searchContext, int limit,int offset,SortContext<UserLoginBean> sortContext) throws ServiceException{
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}

		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<UserLoginBean> sqlCondition =
			new SqlSearchCondition<UserLoginBean>("user_id=?", userId);
		
		return new SqlBuilder<UserLoginBean>(UserLoginBean.class).executeQuery(searchContext, sqlCondition, limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<UserLoginBean> getLogins(Long userId, int limit, int offset) throws ServiceException {
		return this.getLogins(userId, null, limit, offset, null);
	}

	@Override
	public UserLoginBean getLogin(Long userLoginId, Long userId) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		if(userLoginId == null || userLoginId < 0 || userId == null || userId < 0) {
			throw new InvalidParameterException("parameter problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new EntityNotFoundException();
		}
				
		return new SqlBuilder<UserLoginBean>(UserLoginBean.class).findBean(
				FiqlContextBuilder.create(UserLoginBean.class, "userLoginId==?;userId==?", userLoginId, userId)
				, null);
	}

	@Override
	public int getLoginsSize(Long userId,SearchContext<UserLoginBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new EntityNotFoundException();
		}
		
		Connection conn = ConnectionFactory.getReportsConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		if (conn == null){
			throw new DatabaseException("Could not get a connection to database to get users logins");
		}
		try {
			StringBuilder sql = new StringBuilder("SELECT count(*) FROM user_logins WHERE user_id = ?");
			
			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}
			
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, userId);
			int num = 2;

			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				return rs.getInt("count");
			}
		} catch(SQLException ex) {
			throw new DatabaseException("SQL Query exception", ex);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return 0;
	}

	@Override
	public ServiceResultSet<UserLoginBean> getLogins(SearchContext<UserLoginBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		
		if (userPrincipal.getType() != UserTypeEnum.STAFF_USER && userPrincipal.getType() != UserTypeEnum.SYSTEM) {
			throw new AccessDeniedException();
		}
		return new SqlBuilder<UserLoginBean>(UserLoginBean.class).executeQuery(searchContext, null, 0, 0, null);
	}
}
