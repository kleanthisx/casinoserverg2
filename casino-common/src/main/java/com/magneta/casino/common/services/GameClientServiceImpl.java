package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.beans.GameClientGameBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.SortUtil;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GameClientServiceImpl implements GameClientService {
	
	private final PrincipalService principalService;
	private final GamesService gamesService;
	
	public GameClientServiceImpl(PrincipalService principalService,GamesService gamesService) {
		this.principalService = principalService;
		this.gamesService = gamesService;
	}

	@Override
	public ServiceResultSet<GameClientBean> getGameClients(SearchContext<GameClientBean> searchContext, int limit, int offset,SortContext<GameClientBean> sortContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		SqlBuilder<GameClientBean> sqlBuilder = new SqlBuilder<GameClientBean>(GameClientBean.class);
		return sqlBuilder.executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<GameClientBean> getGameClients(int limit, int offset)throws ServiceException {
		return this.getGameClients(null, limit, offset, null);
	}
	
	@Override
	public GameClientBean getGameClient(Integer clientId)throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		SqlBuilder<GameClientBean> sqlBuilder = new SqlBuilder<GameClientBean>(GameClientBean.class);
		
		SearchContext<GameClientBean> searchContext = FiqlContextBuilder.create(GameClientBean.class, "clientId==?", clientId);
		
		return sqlBuilder.findBean(searchContext, null);
	}
	
	@Override
	public ServiceResultSet<GameClientGameBean> getGameClientGames(Integer clientId,SearchContext<GameClientGameBean> searchContext, int limit, int offset,SortContext<GameClientGameBean> sortContext)throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;


		List<GameClientGameBean> gameClients = new ArrayList<GameClientGameBean>();
		try {
			StringBuilder sql = new StringBuilder("SELECT * FROM game_client_games INNER JOIN game_clients ON " +
					"game_client_games.client_id = game_clients.client_id INNER JOIN game_types ON game_client_games.game_id = game_types.game_type_id  " +
					"WHERE game_clients.client_id = ?");

			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}
			
			int myLimit = limit;
			
			if (limit > 0)
				myLimit++;
			
			if(sortContext != null) {
				SortUtil.appendSqlEpilogue(sql, sortContext.getConditions(GameClientGameBean.class), offset, myLimit);
			}else{
				SortUtil.appendSqlEpilogue(sql, null, offset, myLimit);
			}
			stmt = conn.prepareStatement(sql.toString());
			int num = 1;
			stmt.setInt(1, clientId);
			num++;
			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			rs = stmt.executeQuery();
			while (rs.next()){
				GameClientGameBean bean = new GameClientGameBean(gamesService.getGame(DbUtil.getInteger(rs, "game_id")));
				bean.setEnabled(DbUtil.getBoolean(rs, "enabled"));
				gameClients.add(bean);
			}
		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		ServiceResultSet<GameClientGameBean> res = new ServiceResultSet<GameClientGameBean>();
		res.setResult(gameClients);
		
		if (offset > 0) {
			res.setHasPrev(true);
		}
		
		if (limit > 0 && gameClients.size() > limit) {
			res.setHasNext(true);
			gameClients.remove(limit);
		}
		return res;
	}


	@Override
	public int getGameClientsSize(SearchContext<GameClientBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		SqlBuilder<GameClientBean> sqlBuilder = new SqlBuilder<GameClientBean>(GameClientBean.class);
		return sqlBuilder.countBeans(searchContext, null);
	}


	@Override
	public int getGameClientGamesSize(Integer clientId,SearchContext<GameClientGameBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new DatabaseException("Could not connect to database");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT count(*) FROM game_client_games INNER JOIN game_clients ON " +
					"game_client_games.client_id = game_clients.client_id INNER JOIN game_types ON game_client_games.game_id = game_types.game_type_id  " +
					"WHERE game_clients.client_id = ?");
			if(searchContext != null) {
				sql.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sql);
			}
		
			stmt = conn.prepareStatement(sql.toString());
			int num = 1;
			stmt.setInt(1, clientId);
			num++;
			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, num);
			}
			rs = stmt.executeQuery();
			if (rs.next()){
				return rs.getInt("count");
			}
		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return 0;
	}


	@Override
	public void deleteGameClient(Integer clientId) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException("Not authenticated");
		}
		
		if (!principal.hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        
        if (dbConn == null){
        	throw new DatabaseException("Out of database connections");
		}
        
        try {            
            String sql = 
                "DELETE FROM game_clients" +
                " WHERE client_id = ?";
            
                statement = dbConn.prepareStatement(sql);
                statement.setInt(1, clientId);
                
                if (statement.executeUpdate() < 1) {
                	throw new EntityNotFoundException("Unable to find game client to delete");
                }
                
        } catch (SQLException e) {
        	throw new DatabaseException("Unable to delete game client", e);
        } finally{
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
	}
	
	@Override
	public void addGameClient(GameClientBean client) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException("Not authenticated");
		}
		
		if (!principal.hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		new SqlBuilder<GameClientBean>(GameClientBean.class).insertBean(client);
	}
	
	@Override
	public void updateGameClient(GameClientBean client) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException("Not authenticated");
		}
		
		if (!principal.hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			throw new AccessDeniedException("Access denied");
		}
		
		new SqlBuilder<GameClientBean>(GameClientBean.class).updateBean(client);
	}
}
