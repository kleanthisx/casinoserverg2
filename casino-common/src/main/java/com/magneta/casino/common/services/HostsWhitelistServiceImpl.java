package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.common.utils.SqlSearchCondition;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.HostWhitelistBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class HostsWhitelistServiceImpl implements HostsWhitelistService{

	private PrincipalService principalService;
	
	public HostsWhitelistServiceImpl(PrincipalService principalService){
		this.principalService = principalService;
	}
	
	@Override
	public ServiceResultSet<HostWhitelistBean> getHosts(SearchContext<HostWhitelistBean> searchContext, int limit,int offset, SortContext<HostWhitelistBean> sortContext) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, false)) {
			throw new AccessDeniedException("Access Denied");
		}
		return new SqlBuilder<HostWhitelistBean>(HostWhitelistBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public int getHostsSize(SearchContext<HostWhitelistBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, false)) {
			throw new AccessDeniedException("Access Denied");
		}
		return new SqlBuilder<HostWhitelistBean>(HostWhitelistBean.class).countBeans(searchContext, null);
	}

	@Override
	public HostWhitelistBean getHost(Long itemId) throws ServiceException {
		if(itemId == null){
			throw new InvalidParameterException();
		}
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, false)) {
			throw new AccessDeniedException("Access Denied");
		}
		SearchContext<HostWhitelistBean> searchContext = FiqlContextBuilder.create(HostWhitelistBean.class, "id==?", itemId);
		return new SqlBuilder<HostWhitelistBean>(HostWhitelistBean.class).findBean(searchContext, null);
	}

	@Override
	public void updateHost(HostWhitelistBean hostWhitelist) throws ServiceException {
		if(hostWhitelist == null){
			throw new InvalidParameterException();
		}
		
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException("Access Denied");
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Could not get database connection");
		}

		PreparedStatement stmt = null;
		
		try{
			StringBuilder sql = new StringBuilder( "UPDATE hosts_whitelist SET item_ip = CAST (? AS INET), item_desc = ? WHERE item_id = ?");
			
			stmt = conn.prepareStatement(sql.toString());
			
			stmt.setString(1, hostWhitelist.getIp());
			stmt.setString(2, hostWhitelist.getDescription());
			stmt.setLong(3, hostWhitelist.getId());
			
			int updates = stmt.executeUpdate();
			if(updates <= 0) {
				throw new EntityNotFoundException();
			}
			
		} catch (SQLException e) {
			throw new DatabaseException("SQL query error", e);
		}finally{
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}

	@Override
	public void insertHost(HostWhitelistBean hostWhitelist) throws ServiceException {
		if(hostWhitelist == null){
			throw new InvalidParameterException();
		}
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		new SqlBuilder<HostWhitelistBean>(HostWhitelistBean.class).insertBean(hostWhitelist);
	}

	@Override
	public boolean deleteHost(HostWhitelistBean hostWhitelist) throws ServiceException {
		if(hostWhitelist == null){
			throw new InvalidParameterException();
		}
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		return new SqlBuilder<HostWhitelistBean>(HostWhitelistBean.class).deleteBean(hostWhitelist);
	}

	@Override
	public boolean isHost(String ip) throws ServiceException {
		if(ip == null || ip.isEmpty()){
			throw new InvalidParameterException();
		}
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();

		if(principal == null || !principal.hasPrivilege(PrivilegeEnum.ROLE, true)) {
			throw new AccessDeniedException("Access Denied");
		}
		SqlSearchCondition<HostWhitelistBean> sqlCondition = new SqlSearchCondition<HostWhitelistBean>("item_ip >>= CAST(? AS INET)", ip); 
		return new SqlBuilder<HostWhitelistBean>(HostWhitelistBean.class).countBeans(null, sqlCondition) > 0;	
	}	
}
