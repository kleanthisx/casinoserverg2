package com.magneta.casino.common.services;

import java.sql.Connection;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserWithdrawalsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserDepositBean;
import com.magneta.casino.services.beans.UserWithdrawalBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserWithdrawalsServiceImpl implements UserWithdrawalsService{
	
	private final PrincipalService principalService;
	private final UserAccessService userAccessService;

	public UserWithdrawalsServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
	}

	@Override
	public ServiceResultSet<UserWithdrawalBean> getWithdrawals(Long userId,SearchContext<UserWithdrawalBean> searchContext, int limit,int offset, SortContext<UserWithdrawalBean> sortContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		if (userPrincipal.getType() == UserTypeEnum.STAFF_USER) {
			if (!userPrincipal.hasPrivilege(PrivilegeEnum.FUNDS, false)) {
				throw new AccessDeniedException();
			}
		}

		if (searchContext != null) {
			searchContext = FiqlContextBuilder.create(UserWithdrawalBean.class, "userId==?;?", userId, searchContext);
		} else {
			searchContext = FiqlContextBuilder.create(UserWithdrawalBean.class, "userId==?", userId);
		}

		return new SqlBuilder<UserWithdrawalBean>(UserWithdrawalBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<UserWithdrawalBean> getWithdrawals(Long userId,int limit, int offset) throws ServiceException {
		return this.getWithdrawals(userId, null, limit, offset, null);
	}

	@Override
	public int getWithdrawalsSize(Long userId,SearchContext<UserWithdrawalBean> searchContext)throws ServiceException {
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter error");
		}
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		if (userPrincipal.getType() == UserTypeEnum.STAFF_USER) {
			if (!userPrincipal.hasPrivilege(PrivilegeEnum.FUNDS, false)) {
				throw new AccessDeniedException();
			}
		}

		if (searchContext != null) {
			searchContext = FiqlContextBuilder.create(UserWithdrawalBean.class, "userId==?;?", userId, searchContext);
		} else {
			searchContext = FiqlContextBuilder.create(UserWithdrawalBean.class, "userId==?", userId);
		}

		return new SqlBuilder<UserWithdrawalBean>(UserWithdrawalBean.class).countBeans(searchContext, null);
	}

	@Override
	public UserWithdrawalBean getWithdrawal(Long userId, Long withdrawalId) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		if (userPrincipal.getType() == UserTypeEnum.STAFF_USER) {
			if (!userPrincipal.hasPrivilege(PrivilegeEnum.FUNDS, false)) {
				throw new AccessDeniedException();
			}
		}

		SearchContext<UserWithdrawalBean> searchContext = FiqlContextBuilder.create(UserWithdrawalBean.class, "userId==?;transactionId==?", userId, withdrawalId);

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}

		try {
			return new SqlBuilder<UserWithdrawalBean>(UserWithdrawalBean.class).findBean(conn, searchContext, null);
		} finally {
			DbUtil.close(conn);
		}
	}

	@Override
	public UserWithdrawalBean getWithdrawal(Long withdrawalId) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		if (userPrincipal.getType() == UserTypeEnum.STAFF_USER) {
			if (!userPrincipal.hasPrivilege(PrivilegeEnum.FUNDS, false)) {
				throw new AccessDeniedException();
			}
		}

		SearchContext<UserWithdrawalBean> searchContext = FiqlContextBuilder.create(UserWithdrawalBean.class, "transactionId==?", withdrawalId);

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}

		try {
			UserWithdrawalBean bean = new SqlBuilder<UserWithdrawalBean>(UserWithdrawalBean.class).findBean(conn, searchContext, null);

			if (!userAccessService.canAccess(bean.getUserId())) {
				throw new AccessDeniedException();
			}

			return bean;
		} finally {
			DbUtil.close(conn);
		}
	}
	
	@Override
	public String getUserAccount(UserDepositBean userDepositBean) {
		return userDepositBean.getUserAccount();
	}

	@Override
	public void createManualWithdrawal(UserWithdrawalBean withdrawal)
			throws ServiceException {
		
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException();
		}
		
		if (!principal.hasPrivilege(PrivilegeEnum.FUNDS, true)) {
			throw new AccessDeniedException();
		}
		
		if (!userAccessService.canAccess(withdrawal.getUserId())) {
			throw new AccessDeniedException();
		}

		if (withdrawal.getType() == null) {
			throw new ServiceException("Withdrawals MUST have a type");
		}
		
		if (principal.getUserId() != null) {
			withdrawal.setApprovedBy(principal.getUserId());
		}
		
		withdrawal.setDateCompleted(null);
		
		SqlBuilder<UserWithdrawalBean> builder = new SqlBuilder<UserWithdrawalBean>(UserWithdrawalBean.class);	
		builder.insertBean(withdrawal);
	}
}
