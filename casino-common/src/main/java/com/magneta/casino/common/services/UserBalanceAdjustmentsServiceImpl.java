package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.common.utils.SqlSearchCondition;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserBalanceAdjustmentsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBalanceAdjustmentBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserBalanceAdjustmentsServiceImpl implements UserBalanceAdjustmentsService {

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	
	public UserBalanceAdjustmentsServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
	}
	
	@Override
	public ServiceResultSet<UserBalanceAdjustmentBean> getAdjustments(
			Long userId,
			SearchContext<UserBalanceAdjustmentBean> searchContext, int limit,
			int offset, SortContext<UserBalanceAdjustmentBean> sortContext)
			throws ServiceException {
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		if (userId == null || userId <= 0) {
			throw new InvalidParameterException("ERROR with userid parameter");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<UserBalanceAdjustmentBean> sqlCondition =
			new SqlSearchCondition<UserBalanceAdjustmentBean>("user_id=?", userId);
		
		
		return new SqlBuilder<UserBalanceAdjustmentBean>(UserBalanceAdjustmentBean.class).executeQuery( 
				searchContext, 
				sqlCondition, limit, offset, sortContext);
	}

	@Override
	public int getAdjustmentsSize(Long userId,
			SearchContext<UserBalanceAdjustmentBean> searchContext)
			throws ServiceException {
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		if (userId == null || userId <= 0) {
			throw new InvalidParameterException("ERROR with userid parameter");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<UserBalanceAdjustmentBean> sqlCondition =
			new SqlSearchCondition<UserBalanceAdjustmentBean>("user_id=?", userId);
		
		
		return new SqlBuilder<UserBalanceAdjustmentBean>(UserBalanceAdjustmentBean.class).countBeans(
				searchContext, 
				sqlCondition);
	}

	@Override
	public UserBalanceAdjustmentBean getAdjustment(Long userId,
			Long adjustmentId) throws ServiceException {
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		
		if (userId == null || userId <= 0) {
			throw new InvalidParameterException("ERROR with userid parameter");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		SqlSearchCondition<UserBalanceAdjustmentBean> sqlCondition = new SqlSearchCondition<UserBalanceAdjustmentBean>("user_id=?", userId);
		
		return new SqlBuilder<UserBalanceAdjustmentBean>(UserBalanceAdjustmentBean.class).findBean(
				new FiqlContextBuilder().build(UserBalanceAdjustmentBean.class, "adjustmentId==?;userId==?", adjustmentId, userId),
				sqlCondition);
	}

	@Override
	public void createAdjustment(UserBalanceAdjustmentBean adjustment)
			throws ServiceException {
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		switch (userPrincipal.getType()) {
			case STAFF_USER:
				if (!userPrincipal.hasPrivilege(PrivilegeEnum.FUNDS, true)) {
					throw new AccessDeniedException();	
				}
				break;
			case SYSTEM:
				break;
			default:
				throw new AccessDeniedException();	
		}
		
		if (!userAccessService.canAccess(adjustment.getUserId())) {
			throw new AccessDeniedException();
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Cannot connect to database");
		}
		
		String sql = "INSERT INTO user_balance_adjustments(user_id, adjustment_user, adjustment_description, adjustment_amount) VALUES(?,?,?,?)" + 
		             " RETURNING adjustment_id, adjustment_date";

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, adjustment.getUserId());
			stmt.setLong(2, userPrincipal.getUserId());
			stmt.setString(3, adjustment.getAdjustmentDescription());
			stmt.setDouble(4, adjustment.getAdjustmentAmount());
			
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				adjustment.setAdjustmentId(DbUtil.getLong(rs, "adjustment_id"));
				adjustment.setAdjustmentDate(DbUtil.getDate(DbUtil.getTimestamp(rs, "adjustment_date")));
				adjustment.setAdjustmentUser(userPrincipal.getUserId());
			}
			
		} catch (SQLException e) {
			throw new DatabaseException("SQL Query Error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
	}
	
	@Override
	public UserBalanceAdjustmentBean clearBalance(Long userId) throws ServiceException {
		UserBalanceAdjustmentBean adjustment = new UserBalanceAdjustmentBean();
		adjustment.setUserId(userId);
		
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		switch (userPrincipal.getType()) {
			case STAFF_USER:
				if (!userPrincipal.hasPrivilege(PrivilegeEnum.FUNDS, true)) {
					throw new AccessDeniedException();
				}
				break;
			case SYSTEM:
				break;
			default:
				throw new AccessDeniedException();
		}
		
		if (!userAccessService.canAccess(adjustment.getUserId())) {
			throw new AccessDeniedException();
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Cannot connect to database");
		}
		
		String sql = "INSERT INTO user_balance_adjustments(user_id, adjustment_user, adjustment_description, adjustment_amount)" +
					  " (SELECT ?, ?, ?, (balance * (-1.0)) FROM user_balance WHERE user_id = ? AND balance > 0.0)" + 
		             " RETURNING adjustment_id, adjustment_date, adjustment_amount";
		
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, adjustment.getUserId());
			stmt.setLong(2, userPrincipal.getUserId());
			stmt.setString(3, adjustment.getAdjustmentDescription());

			stmt.setDouble(5, adjustment.getUserId());
			
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				adjustment.setAdjustmentId(DbUtil.getLong(rs, "adjustment_id"));
				adjustment.setAdjustmentDate(DbUtil.getDate(DbUtil.getTimestamp(rs, "adjustment_date")));
				adjustment.setAdjustmentUser(userPrincipal.getUserId());
				adjustment.setAdjustmentAmount(DbUtil.getDouble(rs, "adjustment_amount"));
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			throw new DatabaseException("SQL Query Error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		return adjustment;
	}

}
