package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameActionValue;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserRoundsService;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserRoundActionBean;
import com.magneta.casino.services.beans.UserRoundBean;
import com.magneta.casino.services.beans.UserRoundFilteredActionBean;
import com.magneta.casino.services.beans.UserRoundFilteredActionValueBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.SortUtil;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.games.configuration.GameConfiguration;

public class UserRoundsServiceImpl implements UserRoundsService {

	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	private final GameConfigsService gameConfigService;
	private final GameTableService gameTableService;
	private final GameActionFilterService gameActionFilterService;

	public UserRoundsServiceImpl(PrincipalService principalService,
			UserAccessService userAccessService,
			GameConfigsService gameConfigService,
			GameTableService gameTableService,
			GameActionFilterService gameActionFilterService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
		this.gameConfigService = gameConfigService;
		this.gameTableService = gameTableService;
		this.gameActionFilterService = gameActionFilterService;
	}

	@Override
	public List<UserRoundActionBean> getRoundActions(Long userId, UserRoundBean roundBean) throws ServiceException {
		if(userId == null || userId < 0 || roundBean == null) {
			throw new InvalidParameterException("Invalid/Missing Parameter(s)");
		}
		
		ExtendedPrincipalBean userPrincipal = principalService.getPrincipal();

		if (userPrincipal == null || !userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		GameTableBean table = gameTableService.getTable(roundBean.getTableId());

		if (table == null || !userId.equals(table.getOwner())) {
			return null;
		}

		String sql = "SELECT * FROM game_round_actions" +
				" LEFT OUTER JOIN user_transactions ON game_round_actions.table_id = user_transactions.action_table_id "+
				" AND game_round_actions.round_id = user_transactions.action_round_id "+
				" AND game_round_actions.action_id = user_transactions.action_action_id "+
				" AND game_round_actions.sub_action_id = user_transactions.action_sub_action_id "+
				" WHERE game_round_actions.table_id=? AND game_round_actions.round_id=?";

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Error getting connection to database");
		}


		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, roundBean.getTableId());
			stmt.setInt(2, roundBean.getRoundId());

			rs = stmt.executeQuery();

			List<UserRoundActionBean> actions = new ArrayList<UserRoundActionBean>();

			while(rs.next()) {
				UserRoundActionBean bean = new UserRoundActionBean();
				bean.setUserId(DbUtil.getLong(rs, "user_id"));
				bean.setActionId(DbUtil.getInteger(rs, "action_id"));
				bean.setActionDescription(rs.getString("action_desc"));
				bean.setActionValue(rs.getString("action_value"));
				bean.setAmount(DbUtil.getDouble(rs, "amount"));
				bean.setSeatId(DbUtil.getInteger(rs, "seat_id"));
				bean.setActionTime(DbUtil.getTimestamp(rs, "action_time"));
				bean.setTransactionTime(DbUtil.getTimestamp(rs, "transaction_time"));
				bean.setActionArea(rs.getString("action_area"));
				bean.setSubActionId(DbUtil.getInteger(rs, "sub_action_id"));
				bean.setNewBalance(DbUtil.getDouble(rs, "new_balance"));
				actions.add(bean);
			}

			return actions;

		} catch (SQLException e) {
			throw new DatabaseException("SQL query exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

	}

	/**
	 * Converts an image string from the old style to the new
	 * @param oldImg
	 * @return
	 */
	private UserRoundFilteredActionValueBean getImage(String oldImg) {
		UserRoundFilteredActionValueBean bean = new UserRoundFilteredActionValueBean();
		if (oldImg == null || oldImg.trim().isEmpty() || oldImg.startsWith("cards/front") || oldImg.startsWith("cards/back")) {
			return null;
		}

		bean.setType(UserRoundFilteredActionValueBean.ValueType.SYMBOL);


		if (oldImg.startsWith("cards/")) {
			bean.setType(UserRoundFilteredActionValueBean.ValueType.CARD);
		}

		int pathSep = oldImg.lastIndexOf('/');

		if (pathSep > -1) {
			oldImg = oldImg.substring(pathSep + 1);
		}

		int ext = oldImg.lastIndexOf('.');
		if (ext > -1) {
			oldImg = oldImg.substring(0, ext);
		}

		bean.setValue(oldImg);
		return bean;
	}

	@Override
	public List<UserRoundFilteredActionBean> getFilteredRoundActions(Long userId,UserRoundBean roundBean)throws ServiceException {
		List<UserRoundActionBean> actions = this.getRoundActions(userId,roundBean);


		ActionFilter filter;
		if(roundBean.getGameConfigId() != null) {
			try {
				principalService.pushSystem();

				GameConfiguration config = gameConfigService.getFilteredConfiguration(roundBean.getGameId(), roundBean.getGameConfigId());
				filter = gameActionFilterService.getFilter(roundBean.getGameId(), config);
			} finally {
				principalService.popSystem();
			}
		}else{
			filter = gameActionFilterService.getFilter(roundBean.getGameId());
		}

		ExtendedPrincipalBean principal = principalService.getPrincipal();

		filter.setShowGuards(false);

		if (principal.getType() == UserTypeEnum.STAFF_USER
				|| principal.getType() == UserTypeEnum.SYSTEM) {
			filter.setShowGuards(true);
		}

		/* Convert to old type actions */
		List<GameActionBean> oldTypeActions = new ArrayList<GameActionBean>(actions.size());
		for (UserRoundActionBean action: actions) {
			GameActionBean oldAction = new GameActionBean();
			oldAction.setAction(action.getActionDescription());
			oldAction.setAmount(action.getAmount());
			oldAction.setArea(action.getActionArea());
			oldAction.setBalance(action.getNewBalance());
			oldAction.setSeat(action.getSeatId());
			oldAction.setValue(action.getActionValue());
			oldAction.setActionTime(action.getActionTime());

			if(action.getSubActionId() != null) {
				oldAction.setGenerated(0 == action.getSubActionId());
			}

			oldTypeActions.add(oldAction);
		}

		oldTypeActions = filter.filterActions(oldTypeActions);

		/* Convert to new type actions */
		List<UserRoundFilteredActionBean> newActions = new ArrayList<UserRoundFilteredActionBean>(oldTypeActions.size());

		for (GameActionBean oldAction: oldTypeActions) {
			UserRoundFilteredActionBean newAction = new UserRoundFilteredActionBean();
			String actionDescription = oldAction.getAction();
			if(actionDescription != null && !actionDescription.isEmpty()) {
				actionDescription = actionDescription.toLowerCase().replace(" ","-");
				newAction.setActionDescription(actionDescription);
			}
			newAction.setAmount(oldAction.getAmount());
			newAction.setArea(oldAction.getArea());
			newAction.setBalance(oldAction.getBalance());
			newAction.setActionTime(oldAction.getActionTime());
			
			if(oldAction.getSeat() > 0) {
				newAction.setSeat(oldAction.getSeat());
			}

			if (oldAction.getValues() == null || oldAction.getValues().isEmpty()) {
				List<UserRoundFilteredActionValueBean> vals =  new ArrayList<UserRoundFilteredActionValueBean>(1);
				UserRoundFilteredActionValueBean bean = new UserRoundFilteredActionValueBean();

				bean.setType(UserRoundFilteredActionValueBean.ValueType.LITERAL);
				if (oldAction.getValue() != null && !oldAction.getValue().trim().isEmpty()) {
					bean.setValue(oldAction.getValue());
				}

				vals.add(bean);
				newAction.setActionValue(vals);
			} else {
				List<UserRoundFilteredActionValueBean> vals =  new ArrayList<UserRoundFilteredActionValueBean>(oldAction.getValues().size());

				for (GameActionValue oldVal : oldAction.getValues()) {
					UserRoundFilteredActionValueBean bean;

					if (oldVal.getImage() != null && !oldVal.getValue().equals("front")) {
						bean = getImage(oldVal.getImage());
					} else {
						bean = new UserRoundFilteredActionValueBean();
						bean.setType(UserRoundFilteredActionValueBean.ValueType.LITERAL);
						if (oldVal.getValue() != null && !oldVal.getValue().trim().isEmpty())
							bean.setValue(oldVal.getValue());
					}

					if(bean == null) {
						continue;
					}
					vals.add(bean);
				}

				newAction.setActionValue(vals);
			}		
			newActions.add(newAction);
		}
		return newActions;
	}

	@Override
	public UserRoundBean getRound(Long userId, Long tableId, Integer roundId) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException("User principal is null");
		}
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter errro");
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		GameTableBean table = gameTableService.getTable(tableId);

		if (table == null) {
			return null;
		}

		if (!userId.equals(table.getOwner())) {
			throw new AccessDeniedException("Access denied");
		}

		String sql = "SELECT date_opened, date_closed, game_config_id FROM game_rounds WHERE table_id=? AND round_id=?";

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, tableId);
			stmt.setInt(2, roundId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				UserRoundBean bean = new UserRoundBean();
				bean.setRoundId(roundId);
				bean.setTableId(table.getTableId());
				bean.setGameId(table.getGameId());
				bean.setDateOpened(DbUtil.getTimestamp(rs, "date_opened"));
				bean.setDateClosed(DbUtil.getTimestamp(rs, "date_closed"));
				bean.setGameConfigId(DbUtil.getLong(rs, "game_config_id"));
				
				return bean;
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL query exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}	

		return null;
	}

	@Override
	public ServiceResultSet<UserRoundBean> getRounds(Long userId,SearchContext<UserRoundBean> searchContext, int limit,int offset, SortContext<UserRoundBean> sortContext)throws ServiceException {
		
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter errro");
		}
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		List<UserRoundBean> rounds = null;
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		if (conn == null){
			throw new DatabaseException("Error getting connection to database");
		}
		try {
			String sql = "SELECT tables.table_id, rounds.round_id," +  
			" rounds.date_opened,rounds.date_closed, tables.game_type_id, rounds.game_config_id" +
			" FROM game_tables AS tables "+
			" INNER JOIN game_rounds AS rounds ON tables.table_id = rounds.table_id" +
			" WHERE tables.table_owner = ?";

			StringBuilder sqlQuery = new StringBuilder(sql);

			if(searchContext != null) {
				sqlQuery.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sqlQuery);
			}

			int myLimit = limit;

			if (limit > 0){
				myLimit++;
			}

			if(sortContext != null) {
				SortUtil.appendSqlEpilogue(sqlQuery, sortContext.getConditions(UserRoundBean.class), offset, myLimit);
			} else{
				SortUtil.appendSqlEpilogue(sqlQuery, null, offset, myLimit);
			}
			stmt = conn.prepareStatement(sqlQuery.toString());

			rounds = new ArrayList<UserRoundBean>();

			stmt.setLong(1, userId);

			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, 2);
			}

			rs = stmt.executeQuery();
			while(rs.next()) {
				UserRoundBean bean = new UserRoundBean();
				bean.setRoundId(DbUtil.getInteger(rs, "round_id"));
				bean.setTableId(DbUtil.getLong(rs, "table_id"));
				bean.setDateOpened(DbUtil.getTimestamp(rs, "date_opened"));
				bean.setDateClosed(DbUtil.getTimestamp(rs, "date_closed"));
				bean.setGameId(DbUtil.getInteger(rs, "game_type_id"));
				bean.setGameConfigId(DbUtil.getLong(rs, "game_config_id"));
				rounds.add(bean);
			}
		}catch(SQLException ex) {
			throw new DatabaseException("SQL query exception", ex);
		}finally{
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		ServiceResultSet<UserRoundBean> res = new ServiceResultSet<UserRoundBean>();
		res.setResult(rounds);

		if (offset > 0) {
			res.setHasPrev(true);
		}

		if (limit > 0 && rounds.size() > limit) {
			res.setHasNext(true);
			rounds.remove(limit);
		}
		return res;
	}

	@Override
	public int getRoundsSize(Long userId,SearchContext<UserRoundBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter errro");
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		if (conn == null){
			throw new DatabaseException("Error getting connection to database");
		}
		try {
			StringBuilder sqlQuery = new StringBuilder()
			.append("SELECT COUNT(*) FROM game_tables AS tables")
			.append(" INNER JOIN game_rounds AS rounds ON tables.table_id = rounds.table_id")
			.append(" WHERE table_owner=?");

			if(searchContext != null) {
				sqlQuery.append(" AND ");
				searchContext.getCondition().appendSQLSearchString(sqlQuery);
			}

			stmt = conn.prepareStatement(sqlQuery.toString());

			stmt.setLong(1, userId);

			if(searchContext != null) {
				searchContext.getCondition().setSQLStatementValues(stmt, 2);
			}

			rs = stmt.executeQuery();

			int count = 0;

			while(rs.next()) {
				count += rs.getInt("count");
			}

			return count;
		} catch(SQLException ex) {
			throw new DatabaseException("SQL query exception", ex);
		}finally{
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	}

}
