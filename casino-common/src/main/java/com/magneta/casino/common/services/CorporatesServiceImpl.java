package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.BeanLoader;
import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.common.utils.SqlSearchCondition;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.DatabaseService;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.AffiliateUsersBean;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.DbUtil;

public class CorporatesServiceImpl implements CorporatesService {

	private final UserAccessService userAccessService;
	private final PrincipalService principalService;
	private final DatabaseService databaseService;
	
	public CorporatesServiceImpl(
			DatabaseService databaseService,
			UserAccessService userAccessService,
			PrincipalService principalService) {
		this.databaseService = databaseService;
		this.userAccessService = userAccessService;
		this.principalService = principalService;
	}
	
	@Override
	public CorporateBean getCorporate(Long corporateId) throws ServiceException {
		if(corporateId == null) {
			throw new InvalidParameterException("corporateId id is null");
		}
		
		if (!userAccessService.canAccess(corporateId)) {
			return null;
		}
		SearchContext<CorporateBean> searchContext = FiqlContextBuilder.create(CorporateBean.class, "affiliateId==?", corporateId);

		return new SqlBuilder<CorporateBean>(CorporateBean.class).findBean(searchContext);
	}

	@Override
	public ServiceResultSet<CorporateBean> getCorporates(SearchContext<CorporateBean> searchContext,SortContext<CorporateBean> sortContext, int limit, int offset)throws ServiceException {
			return new SqlBuilder<CorporateBean>(CorporateBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<CorporateBean> getCorporates(int limit, int offset)throws ServiceException {
		return this.getCorporates(null, null, limit, offset);
	}

	@Override
	public CorporateBean getSuperCorporate(Long userId) throws ServiceException {
		if(userId == null) {
			throw new InvalidParameterException("userId id is null");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException("Access denied");
		}
		
		BeanLoader<CorporateBean> loader = new BeanLoader<CorporateBean>(CorporateBean.class); 
		
		Connection conn = databaseService.getConnection(true);

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT affiliates.*"+
					" FROM affiliate_users"+
					" INNER JOIN affiliates ON affiliates.affiliate_id = affiliate_users.affiliate_id"+
			" WHERE affiliate_users.user_id = ?");
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, userId); 
			rs = stmt.executeQuery();
			if (rs.next()) {
				return loader.fill(new CorporateBean(), rs);
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return null;
	}

	@Override
	public int getCorporatesSize(SearchContext<CorporateBean> searchContext) throws ServiceException {
		return new SqlBuilder<CorporateBean>(CorporateBean.class).countBeans(searchContext);
	}

	@Override
	public List<Long> getCorporateHierarchy(Long corporateId) throws ServiceException {
		if (corporateId == null || corporateId <= 0) {
			throw new InvalidParameterException("ERROR with corporateid parameter");
		}

		if (!userAccessService.canAccess(corporateId)) {
			return null;
		}
		
		Connection conn = databaseService.getConnection(true);
		
		List<Long> corporates = new ArrayList<Long>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			StringBuilder sql = new StringBuilder("SELECT corp_super_accounts(?) AS corporateId");
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1,corporateId);
			rs = stmt.executeQuery();
			
			while (rs.next()){
				corporates.add(rs.getLong("corporateId"));
			}
			
		} catch (SQLException e) {
			throw new DatabaseException("Sql Exception", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
	
		return corporates;
	}

	@Override
	public int getSubAccountsSize(Long corporateId, SearchContext<AffiliateUsersBean> searchContext) throws ServiceException {	
		if (corporateId == null || corporateId <= 0) {
			throw new InvalidParameterException("ERROR with corporateid parameter");
		}
		
		if (!userAccessService.canAccess(corporateId)) {
			return 0;
		}
		
		if (searchContext != null) {
			searchContext = FiqlContextBuilder.create(AffiliateUsersBean.class, "(?);(affiliateId==?)", searchContext, corporateId);
		} else {
			searchContext = FiqlContextBuilder.create(AffiliateUsersBean.class, "affiliateId==?", corporateId);
		}
		
		return new SqlBuilder<AffiliateUsersBean>(AffiliateUsersBean.class).countBeans(searchContext, null);
	}
	
	@Override
	public List<AffiliateUsersBean> getSubAccounts(Long corporateId, SearchContext<AffiliateUsersBean> searchContext, SortContext<AffiliateUsersBean> sortContext) throws ServiceException {	
		if (corporateId == null || corporateId <= 0) {
			throw new InvalidParameterException("Invalid corporateId");
		}
		
		if (!userAccessService.canAccess(corporateId)) {
			throw new EntityNotFoundException("Corporate not found");
		}
		
		if (searchContext != null) {
			searchContext = FiqlContextBuilder.create(AffiliateUsersBean.class, "(?);(affiliateId==?)", searchContext, corporateId);
		} else {
			searchContext = FiqlContextBuilder.create(AffiliateUsersBean.class, "affiliateId==?", corporateId);
		}

		return new SqlBuilder<AffiliateUsersBean>(AffiliateUsersBean.class).findBeans(searchContext, null, sortContext);
	}
	
	@Override
	public ServiceResultSet<CorporateBean> getSuperCorporates(SearchContext<CorporateBean> searchContext,SortContext<CorporateBean> sortContext, int limit, int offset) throws ServiceException {
		
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.CORPORATE, false)) {
			throw new AccessDeniedException("Access denied");
		}

		SqlSearchCondition<CorporateBean> sqlCondition = new SqlSearchCondition<CorporateBean>("NOT EXISTS (SELECT user_id FROM affiliate_users WHERE affiliate_users.user_id = affiliates.affiliate_id)");
		
		return new SqlBuilder<CorporateBean>(CorporateBean.class).executeQuery(searchContext, sqlCondition, limit, offset, sortContext);
	}
	
	@Override
	public int getSuperCorporatesSize(SearchContext<CorporateBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		
		if (principal == null || !principal.hasPrivilege(PrivilegeEnum.CORPORATE, false)) {
			throw new AccessDeniedException("Access denied");
		}

		SqlSearchCondition<CorporateBean> sqlCondition = new SqlSearchCondition<CorporateBean>("NOT EXISTS (SELECT user_id FROM affiliate_users WHERE affiliate_users.user_id = affiliates.affiliate_id)");
		
		return new SqlBuilder<CorporateBean>(CorporateBean.class).countBeans(searchContext, sqlCondition);
	}
}
