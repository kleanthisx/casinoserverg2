package com.magneta.casino.common.services;

import java.util.Date;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.common.utils.SqlSearchCondition;
import com.magneta.casino.services.GamesHistoryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.GameSummaryBean;
import com.magneta.casino.services.beans.GameSummaryHistoryBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class GamesHistoryServiceImpl implements GamesHistoryService {

	private final UserAccessService userAccessService;
	
	public GamesHistoryServiceImpl(UserAccessService userAccessService) {
		this.userAccessService = userAccessService;
	}
	
	@Override
	public ServiceResultSet<GameSummaryBean> getGameSummary(Long userId, Date minDate, Date maxDate, SearchContext<GameSummaryBean> searchContext,SortContext<GameSummaryBean> sortContext, int limit,int offset) throws ServiceException {
		if(userId == null || userId <= 0) {
			throw new InvalidParameterException("userid parameters problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		SqlSearchCondition<GameSummaryBean> sqlContext;
		
		if (minDate == null && maxDate == null) {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=?", userId);
		} else if (maxDate == null) {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=? AND game_period_amounts.period_date >= ?", userId, minDate);
		} else if (minDate == null) {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=? AND game_period_amounts.period_date <= ?", userId, maxDate);
		} else {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=? AND game_period_amounts.period_date >= ? AND game_period_amounts.period_date <= ?", userId, minDate, maxDate);
		}
		
		return new SqlBuilder<GameSummaryBean>(GameSummaryBean.class).executeQuery(searchContext, sqlContext, limit, offset, sortContext);
	}
	
	@Override
	public int getGameSummarySize(Long userId, Date minDate, Date maxDate, SearchContext<GameSummaryBean> searchContext,SortContext<GameSummaryBean> sortContext, int limit,int offset) throws ServiceException {
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("userid parameters problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		SqlSearchCondition<GameSummaryBean> sqlContext;
		
		if (minDate == null && maxDate == null) {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=?", userId);
		} else if (maxDate == null) {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=? AND game_period_amounts.period_date >= ?", userId, minDate);
		} else if (minDate == null) {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=? AND game_period_amounts.period_date <= ?", userId, maxDate);
		} else {
			sqlContext = new SqlSearchCondition<GameSummaryBean>("game_period_amounts.user_id=? AND game_period_amounts.period_date >= ? AND game_period_amounts.period_date <= ?", userId, minDate, maxDate);
		}
		
		return new SqlBuilder<GameSummaryBean>(GameSummaryBean.class).countBeans(searchContext, sqlContext);
	}
	
	@Override
	public ServiceResultSet<GameSummaryHistoryBean> getSummaryGameHistory(Long userId,SearchContext<GameSummaryHistoryBean> searchContext,SortContext<GameSummaryHistoryBean> sortContext, int limit,int offset) throws ServiceException {
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("userid parameters problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		if (searchContext == null) {
			searchContext = FiqlContextBuilder.create(GameSummaryHistoryBean.class, "userId==?", userId);
		} else {
			searchContext = FiqlContextBuilder.create(GameSummaryHistoryBean.class, "userId==?;?", userId, searchContext);
		}
		
		return new SqlBuilder<GameSummaryHistoryBean>(GameSummaryHistoryBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<GameSummaryHistoryBean> getSummaryGameHistory(Long userId,int limit, int offset) throws ServiceException {
		return this.getSummaryGameHistory(userId, null, null, limit, offset);
	}
	
	
	@Override
	public int getSummaryGameHistorySize(Long userId,SearchContext<GameSummaryHistoryBean> searchContext) throws ServiceException {
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("userid parameters problem");
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException("Access Denied");
		}
		
		if (searchContext == null) {
			searchContext = FiqlContextBuilder.create(GameSummaryHistoryBean.class, "userId==?", userId);
		} else {
			searchContext = FiqlContextBuilder.create(GameSummaryHistoryBean.class, "userId==?;?", userId, searchContext);
		}
		
		return new SqlBuilder<GameSummaryHistoryBean>(GameSummaryHistoryBean.class).countBeans(searchContext, null);
	}
}
