package com.magneta.casino.common.services;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.beans.RolePrivilegeBean;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidAuthenticationCredentialsException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.security.Password;

public class UserAuthenticationServiceImpl implements UserAuthenticationService {

	private final PrincipalService principalService;

	public UserAuthenticationServiceImpl(PrincipalService principalService) {
		this.principalService = principalService;
	}

	private Object[] getTypesAsArray(UserTypeEnum[] types) {
		Object[] arr = new Object[types.length];

		for (int i=0; i < types.length; i++) {
			arr[i] = types[i].getId();
		}

		return arr;
	}

	private void loadPrivileges(Connection conn, UserAuthenticationBean bean) throws ServiceException {

		Map<String, RolePrivilegeBean> privileges = new HashMap<String, RolePrivilegeBean>();

		String sql= "SELECT privilege_key, allow_write FROM role_privileges" +
		" INNER JOIN roles ON role_privileges.role_id=roles.role_id" +
		" INNER JOIN user_roles ON user_roles.role_id=roles.role_id AND user_roles.user_id=?" +
		" ORDER BY allow_write, privilege_key";

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, bean.getUserId());
			rs = stmt.executeQuery();

			while (rs.next()) {
				RolePrivilegeBean privilege = new RolePrivilegeBean();
				privilege.setPrivilege(rs.getString("privilege_key"));
				privilege.setAllowWrite(rs.getBoolean("allow_write"));

				privileges.put(privilege.getPrivilege(), privilege);
			}

		} catch (SQLException e) {
			throw new DatabaseException("Database error while loading privileges", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		List<RolePrivilegeBean> privs = new ArrayList<RolePrivilegeBean>();

		Iterator<Entry<String, RolePrivilegeBean>> iter = privileges.entrySet().iterator();

		while (iter.hasNext()) {
			privs.add(iter.next().getValue());
		}

		bean.setPrivileges(privs);
	}

	private boolean corporateApproved(Connection conn, Long userId) throws ServiceException {
		String sql = "SELECT approved FROM affiliates WHERE affiliate_id=?";

		boolean approved = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, userId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				approved = rs.getBoolean("approved");
			}

		} catch (SQLException e) {
			throw new DatabaseException("Database error while checking for corporate approval",e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return approved;
	}


	private UserAuthenticationBean authenticateUser(Connection conn, String username, String password, UserTypeEnum[] types) throws ServiceException {

		UserAuthenticationBean authenticatedUser = null;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT user_id, username, user_type, password_hash, password_salt" +
			" FROM users WHERE LOWER(username)=? AND is_active=true AND closed=false";

			if (types != null) {
				sql += " AND user_type =ANY(?)";
			}

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, username.toLowerCase());

			if (types != null) {
				Array arr = conn.createArrayOf("integer", getTypesAsArray(types));
				stmt.setArray(2, arr);
			}

			rs = stmt.executeQuery();

			if (rs.next()) {
				String dbHash = rs.getString("password_hash");
				String salt = rs.getString("password_salt");

				String hash = Password.getPasswordHash(password, salt);

				if (!hash.equals(dbHash)) {
					throw new InvalidAuthenticationCredentialsException();
				}

				UserTypeEnum userType = UserTypeEnum.valueOf(rs.getInt("user_type"));
				authenticatedUser = new UserAuthenticationBean();

				authenticatedUser.setUserType(userType);
				authenticatedUser.setUserId(rs.getLong("user_id"));
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL Error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return authenticatedUser;
	}
	
	@Override
	public void authenticateUser(String username, String password, UserTypeEnum[] types, PrivilegeEnum requiredPrivilege) throws ServiceException {
		UserAuthenticationBean authenticatedUser = null;

		if (username == null || password == null) {
			throw new InvalidParameterException("Username and password are required");
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		try {
			authenticatedUser = authenticateUser(conn, username, password, types);

			if (authenticatedUser != null) {
				UserTypeEnum userType = authenticatedUser.getUserType();

				/* Check corporates for approval */
				if (userType.equals(UserTypeEnum.CORPORATE_ACCOUNT)) {
					if (!corporateApproved(conn, authenticatedUser.getUserId())) {
						throw new AccessDeniedException("Access denied");
					}
				}

				loadPrivileges(conn, authenticatedUser);

				if (requiredPrivilege != null) {
					boolean foundPrivilege = false;

					for (RolePrivilegeBean priv: authenticatedUser.getPrivileges()) {
						if (priv.getPrivilege().equals(requiredPrivilege.getId())) {
							foundPrivilege = true;
							break;
						}
					}

					if (!foundPrivilege) {
						throw new AccessDeniedException();
					}
				}
			}
		} finally {
			DbUtil.close(conn);
		}

		if (authenticatedUser == null) {
			throw new InvalidAuthenticationCredentialsException();
		}

		try {
			principalService.onUserAuthenticated(authenticatedUser);
		} catch (ServiceException e) {
			logoutUser();
			throw e;
		}
	}

	private UserAuthenticationBean authenticateUser(Connection conn, Long userId) throws ServiceException {

		UserAuthenticationBean authenticatedUser = null;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT user_type" +
			" FROM users WHERE user_id=? AND is_active=true AND closed=false";

			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, userId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				UserTypeEnum userType = UserTypeEnum.valueOf(rs.getInt("user_type"));
				authenticatedUser = new UserAuthenticationBean();

				authenticatedUser.setUserType(userType);
				authenticatedUser.setUserId(userId);
			}

		} catch (SQLException e) {
			throw new DatabaseException("SQL Error", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return authenticatedUser;
	}
	
	@Override
	public UserAuthenticationBean createUserAuthenticationBean(Long userId) throws ServiceException {
		UserAuthenticationBean authenticatedUser = null;

		if (userId == null) {
			throw new InvalidParameterException("UserId is required");
		}

		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		try {
			authenticatedUser = authenticateUser(conn, userId);

			if (authenticatedUser == null) {
				throw new AccessDeniedException();
			} 

			UserTypeEnum userType = authenticatedUser.getUserType();

			/* Check corporates for approval */
			if (userType.equals(UserTypeEnum.CORPORATE_ACCOUNT)) {
				if (!corporateApproved(conn, authenticatedUser.getUserId())) {
					throw new AccessDeniedException();
				}
			}

			if (authenticatedUser.getUserType() != UserTypeEnum.DEMO_USER) {
				loadPrivileges(conn, authenticatedUser);
			}
		} finally {
			DbUtil.close(conn);
		}
		
		return authenticatedUser;
	}
	
	@Override
	public void logoutUser() {
		principalService.onUserLogout();
	}

}
