package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UsernameServiceImpl implements UsernameService {

	private final UserAccessService userAccessService;
	private final Map<Long, String> usernameMap;

	public UsernameServiceImpl(UserAccessService userAccessService) {
		this.userAccessService = userAccessService;
		this.usernameMap = new ConcurrentHashMap<Long,String>();
	}

	private String retrieveUsername(Long userId) throws ServiceException {
		Connection conn = ConnectionFactory.getReportsConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections while retrieving username");
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement("SELECT username FROM users WHERE user_id=?");

			stmt.setLong(1, userId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				return rs.getString("username");
			}

		} catch (SQLException e) {
			throw new DatabaseException("Unable to retrieve username", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

		return null;
	}

	@Override
	public String getUsername(Long userId) throws ServiceException {
		if (userId == null) {
			return null;
		}

		if (!userAccessService.canAccess(userId)) {
			return null;
		}

		String username = usernameMap.get(userId);

		if (username != null) {
			return username;
		}

		username = retrieveUsername(userId);

		if (username != null) {
			usernameMap.put(userId, username);
		}

		return username;
	}


}
