package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserDepositsService;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserDepositBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserDepositsServiceImpl implements UserDepositsService {
	
	private final PrincipalService principalService;
	private final UserAccessService userAccessService;
	
	public UserDepositsServiceImpl(PrincipalService principalService, UserAccessService userAccessService,
			 UsernameService usernameService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
	} 
	
	@Override
	public ServiceResultSet<UserDepositBean> getDeposits(Long userId,SearchContext<UserDepositBean> searchContext, int limit,int offset,SortContext<UserDepositBean> sortContext) throws ServiceException{
		
		if (userId == null) {
			throw new InvalidParameterException("Missing userId");
		}
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		if (searchContext != null) {
			searchContext = FiqlContextBuilder.create(UserDepositBean.class, "(?);(userId==?)", searchContext, userId);
		} else {
			searchContext = FiqlContextBuilder.create(UserDepositBean.class, "userId==?", userId);
		}
		
		return new SqlBuilder<UserDepositBean>(UserDepositBean.class).executeQuery(searchContext, null, limit, offset, sortContext);
	}

	@Override
	public ServiceResultSet<UserDepositBean> getDeposits(Long userId, int limit,
			int offset) throws ServiceException {
		return this.getDeposits(userId, null, limit, offset, null);
	}

	@Override
	public int getDepositsSize(Long userId,SearchContext<UserDepositBean> searchContext) throws ServiceException {
		if (userId == null) {
			throw new InvalidParameterException("Missing userId");
		}
		
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		if (searchContext != null) {
			searchContext = FiqlContextBuilder.create(UserDepositBean.class, "(?);(userId==?)", searchContext, userId);
		} else {
			searchContext = FiqlContextBuilder.create(UserDepositBean.class, "userId==?", userId);
		}
		
		return new SqlBuilder<UserDepositBean>(UserDepositBean.class).countBeans(searchContext, null);
	}

	@Override
	public UserDepositBean getDeposit(Long userId, Long depositId)
			throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		if(userId == null || userId < 0) {
			throw new AccessDeniedException();
		}
		
		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		SearchContext<UserDepositBean> searchContext = FiqlContextBuilder.create(UserDepositBean.class, "userId==?;transactionId==?", userId, depositId);
		
		try {
			return new SqlBuilder<UserDepositBean>(UserDepositBean.class).findBean(conn, searchContext, null);
		} finally {
			DbUtil.close(conn);
		}
		
	}
	
	@Override
	public UserDepositBean getDeposit(Long depositId)
			throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new DatabaseException("Out of database connections");
		}
		
		SearchContext<UserDepositBean> searchContext = FiqlContextBuilder.create(UserDepositBean.class, "transactionId==?", depositId);
		
		try {
			UserDepositBean bean = new SqlBuilder<UserDepositBean>(UserDepositBean.class).findBean(conn, searchContext, null);
			
			if (!userAccessService.canAccess(bean.getUserId())) {
				throw new AccessDeniedException();
			}
			
			return bean;
		} finally {
			DbUtil.close(conn);
		}
	}

	@Override
	public UserDepositBean getLastCompletedDeposit(Long userId,SearchContext<UserDepositBean> searchContext) throws ServiceException {
		ExtendedPrincipalBean userPrincipal = this.principalService.getPrincipal();
		if(userPrincipal == null) {
			throw new AccessDeniedException();
		}
		if(userId == null || userId < 0) {
			throw new InvalidParameterException("Userid parameter errro");
		}

		if (!userAccessService.canAccess(userId)) {
			throw new AccessDeniedException();
		}

		SortContext<UserDepositBean> sortContext = new SortContext<UserDepositBean>("!transactionId");
		
		if (searchContext == null) {
			searchContext = FiqlContextBuilder.create(UserDepositBean.class, "userId==?;dateCompleted!=?", userId, null);
		} else {
			searchContext = FiqlContextBuilder.create(UserDepositBean.class, "userId==?;dateCompleted!=?;?", userId, null, searchContext);
		}
		
		Connection conn = ConnectionFactory.getConnection();
		try {
			ServiceResultSet<UserDepositBean> res;
			try {
				res = new SqlBuilder<UserDepositBean>(UserDepositBean.class).executeQuery(conn, searchContext, null, 1, 0, sortContext);
			} catch (SQLException e) {
				throw new DatabaseException("Unable to get last deposit", e);
			}

			List<UserDepositBean> beans = res.getResult();

			if (beans == null || beans.isEmpty()) {
				return null;
			}

			return beans.get(0);
		} finally {
			DbUtil.close(conn);
		}
	}

	@Override
	public void createManualDeposit(UserDepositBean deposit)
			throws ServiceException {
		ExtendedPrincipalBean principal = this.principalService.getPrincipal();
		if(principal == null) {
			throw new AccessDeniedException();
		}
		
		if (!principal.hasPrivilege(PrivilegeEnum.FUNDS, true)) {
			throw new AccessDeniedException();
		}
		
		if (!userAccessService.canAccess(deposit.getUserId())) {
			throw new AccessDeniedException();
		}
		
		if (deposit.getType() == null) {
			throw new ServiceException("Manual deposits MUST have a type");
		}

		if (principal.getUserId() != null) {
			deposit.setApprovedBy(principal.getUserId());
		}
		
		SqlBuilder<UserDepositBean> builder = new SqlBuilder<UserDepositBean>(UserDepositBean.class);

		builder.insertBean(deposit);		
	}
}
