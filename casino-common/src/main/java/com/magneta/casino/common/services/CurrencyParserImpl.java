package com.magneta.casino.common.services;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;
import java.util.spi.CurrencyNameProvider;

import com.magneta.casino.common.currency.CasinoCurrencyNameProvider;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;

public class CurrencyParserImpl implements CurrencyParser {
	
	private final CurrencyService currencyService;
	private final CurrencyNameProvider currencyNameProvider;
	
	public CurrencyParserImpl(CurrencyService currencyService) {
		this.currencyService = currencyService;
		this.currencyNameProvider = new CasinoCurrencyNameProvider();
	}
	
	private DecimalFormat getDefaultDecimalFormat() {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		
		StringBuilder formatBuilder = new StringBuilder("0.");
		
		for (int i=0; i < currencyService.getActiveCurrency().getDefaultFractionDigits(); i++) {
			formatBuilder.append('0');
		}
		
		DecimalFormat f = new DecimalFormat(formatBuilder.toString());
		f.setGroupingUsed(false);
		f.setDecimalFormatSymbols(symbols);
		f.setRoundingMode(RoundingMode.DOWN);
		
		return f;
	}
	
	@Override
	public Double parseDouble(String doubleString) {
		return parseDouble(Locale.US, doubleString);
	}

	@Override
	public Double parseDouble(Locale locale, String doubleString) {
		if (doubleString == null || doubleString.isEmpty()) {
			return null;
		}

		Currency curr = currencyService.getActiveCurrency();
        NumberFormat nf = NumberFormat.getNumberInstance(locale);
        
        nf.setRoundingMode(RoundingMode.DOWN);
        nf.setMaximumFractionDigits(curr.getDefaultFractionDigits());
        nf.setMinimumFractionDigits(0);
		
        double rawVal;
        try {
			 rawVal = nf.parse(doubleString).doubleValue();
		} catch (ParseException e) {
			return null;
		}
        
        return normalizeDouble(rawVal);
	}
	
	@Override
    public double normalizeDouble(double d) {
    	java.util.Currency curr = currencyService.getActiveCurrency();
    	int decimals = curr.getDefaultFractionDigits();
    	
    	double thres = Math.pow(10.0d, -(decimals+1));
    	
    	/**
    	 * Ensure that we never return approximations
    	 * for zero.
    	 */
    	if (Math.abs(d) < thres) {
    		return 0.0;
    	}
    	
    	double factor = Math.pow(10.0d, decimals);
    	
        return Math.floor((d + thres) * factor) / factor;
    }
	
	@Override
    public String formatDouble(Double amount, Locale locale) {
    	if (amount == null) {
    		return "";
    	}
    	
    	Currency curr = currencyService.getActiveCurrency();

    	NumberFormat nf = NumberFormat.getNumberInstance(locale);

        nf.setRoundingMode(RoundingMode.DOWN);
        nf.setMaximumFractionDigits(curr.getDefaultFractionDigits());
        nf.setMinimumFractionDigits(curr.getDefaultFractionDigits());

        return nf.format(amount);
    }
    
	@Override
	public String formatDouble(Double amount) {
		if (amount == null) {
			return "";
		}
		
		DecimalFormat f = getDefaultDecimalFormat();
		
		return f.format(amount);
    }
	
	@Override
	public String getCurrencySymbol(Locale locale) {
		Currency curr = currencyService.getActiveCurrency();
		
		String symbol = currencyNameProvider.getSymbol(curr.getCurrencyCode(), locale);
		
		if (symbol == null) {
			symbol = curr.getSymbol();
		}
		
		return symbol;
	}
	

	@Override
	public String formatCurrency(Double amount, Locale locale) {
		if (amount == null) {
			return "";
		}
		Currency curr = currencyService.getActiveCurrency();
		
		String symbol = currencyNameProvider.getSymbol(curr.getCurrencyCode(), locale);
		
		if (symbol == null) {
			symbol = curr.getSymbol();
		}
		
		return symbol + " " + formatDouble(amount, locale);
	}

	@Override
	public String formatCurrency(Double amount) {
		return formatCurrency(amount, Locale.US);
	}
}
