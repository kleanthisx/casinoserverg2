package com.magneta.casino.common.user.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserBalance {

    public static double getBalance(long userId) {
        Connection conn = ConnectionFactory.getConnection();
        try {
            return getAvailableBalance(conn, userId);
        } finally {
            DbUtil.close(conn);
        }
    }
    
    /**
     * Gets the account balance of the given user.
     * @param userID The ID of the user.
     * @return A boolean indicating whether the balance was retrieved successfully. 
     */
    private static double getAvailableBalance(Connection dbConn, long userID){
        double balance = -1;
       
        PreparedStatement statement = null;
        ResultSet results = null;
        
        if (dbConn == null){
            return -1;
        }
        
        try {
           
            String sql = 
                    "SELECT balance"+
                    " FROM user_balance"+
                    " WHERE user_id = ?";
            
            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, userID);
            
            results = statement.executeQuery();
            
            if (results.next())
                balance = results.getDouble("balance");
          
        } catch (SQLException e) {
        	throw new RuntimeException("Error while getting user balance.", e);
        } finally {
            DbUtil.close(results);
            DbUtil.close(statement);
        }
        
        return balance;
    }
}
