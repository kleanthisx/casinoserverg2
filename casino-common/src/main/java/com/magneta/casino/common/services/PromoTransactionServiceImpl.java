package com.magneta.casino.common.services;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.PromoTransactionService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.PromoTransactionBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.EntityNotFoundException;

public class PromoTransactionServiceImpl implements PromoTransactionService {

	private final UserAccessService userAccessService;
	private final PrincipalService principalService;

	public PromoTransactionServiceImpl(PrincipalService principalService, UserAccessService userAccessService) {
		this.principalService = principalService;
		this.userAccessService = userAccessService;
	}

	@Override
	public void createPromoTransaction(PromoTransactionBean transaction) throws ServiceException {
		if (!userAccessService.canAccess(transaction.getUserId())) {
			throw new EntityNotFoundException();
		}

		ExtendedPrincipalBean principal = principalService.getPrincipal();

		switch(principal.getType()) {
			case SYSTEM:
			case CORPORATE_ACCOUNT:
				break;
			case STAFF_USER:
				if (!principal.hasPrivilege(PrivilegeEnum.USER_BONUS, true)) {
					throw new AccessDeniedException("Access denied");
				}
				break;
			default:
				throw new AccessDeniedException("Access denied");
		}

		new SqlBuilder<PromoTransactionBean>(PromoTransactionBean.class).insertBean(transaction);
	}

}
