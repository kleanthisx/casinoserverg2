package com.magneta.casino.common.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.system.SettingDefinition;
import com.magneta.casino.common.system.SettingDefinitions;
import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.exceptions.InvalidParameterException;
import com.magneta.casino.services.search.SearchCondition;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;
import com.magneta.casino.services.utils.CollectionUtils;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class SettingsServiceImpl implements SettingsService {

	private static final Logger log = LoggerFactory.getLogger(SettingsServiceImpl.class);
			
	private static final long TIMEOUT = 5 * 60 * 1000;

	private long lastUpdate;
	private List<SettingBean> settings;
	private Map<String, SettingBean> settingsTable;
	
	private final PrincipalService principalService;

	public SettingsServiceImpl(PrincipalService principalService) {
		this.principalService = principalService;
		lastUpdate = 0;
	}
	
	private String translateValue(SettingBean setting, Object value) {
		if (value == null) {
			return null;
		}

		return value.toString();
	}

	private synchronized void checkUpdate() throws ServiceException {
		long now = System.currentTimeMillis();

		if (now - lastUpdate < TIMEOUT)
			return;
		
		List<SettingBean> settings = new SqlBuilder<SettingBean>(SettingBean.class).findBeans(null, null, null);
		
		Map<String, SettingBean> settingsTable = new HashMap<String, SettingBean>();
		
		List<SettingDefinition> definitions = SettingDefinitions.getDefinitions();
		
		for (SettingDefinition def: definitions) {
			SettingBean c = new SettingBean();
			
			c.setKey(def.getKey());
			c.setType(def.getType().getIntVal());
			c.setDescription(def.getDescription());
			c.setHide(def.isHidden());
			c.setSettingSection(def.getSection());
			c.setSettingSubsection(def.getSubSection());
			c.setValue(translateValue(c, def.getDefaultValue()));

			settingsTable.put(c.getKey(), c);
		}
		
		if (settings != null && !settings.isEmpty()) {
			for (SettingBean c: settings) {
				SettingDefinition def = SettingDefinitions.get(c.getKey());
				
				if (def == null) {
					log.warn("Undefined setting {}", c.getKey());
				} else {
					c.setType(def.getType().getIntVal());
					c.setDescription(def.getDescription());
					c.setHide(def.isHidden());
					c.setSettingSection(def.getSection());
					c.setSettingSubsection(def.getSubSection());
				}
				
				settingsTable.put(c.getKey(), c);
			}
		}
		
		this.lastUpdate = System.currentTimeMillis();
		this.settingsTable = settingsTable;
		
		List<SettingBean> list = new ArrayList<SettingBean>();
		
		for (SettingBean b: settingsTable.values()) {
			list.add(b);
		}
		this.settings = list;

	}

	@Override
	public SettingBean getSetting(String settingKey) throws ServiceException {
		if(settingKey == null || settingKey.isEmpty()) {
			throw new InvalidParameterException("settingKey is required");
		}
		checkUpdate();
		Map<String, SettingBean> settings = this.settingsTable;

		if (settings == null) {
			throw new ServiceException("settingsTable is null");
		}

		return settings.get(settingKey);
	}

	@Override
	public ServiceResultSet<SettingBean> getSettings(int limit, int offset)throws ServiceException {
		return this.getSettings(null,null,limit, offset);
	}

	@Override
	public ServiceResultSet<SettingBean> getSettings(SearchContext<SettingBean> searchContext,SortContext<SettingBean> sortContext, int limit, int offset)throws ServiceException {
		checkUpdate();
		List<SettingBean> copyList = CollectionUtils.copyList(settings);

		if(offset > copyList.size())  {
			throw new InvalidParameterException("offset cannot be bigger than settings size:" + Long.toString(copyList.size()));
		}

		if(searchContext != null) {
			SearchCondition<SettingBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}

		if(sortContext != null) {
			sortContext.sortList(SettingBean.class, copyList);
		}		

		List<SettingBean> beans = new ArrayList<SettingBean>();

		ListIterator<SettingBean> iter = copyList.listIterator(offset);

		int myLimit = limit;
		
		if(myLimit != 0) {
			myLimit++;
		}
		
		while (iter.hasNext() && (myLimit == 0 || beans.size() < myLimit)) {
			beans.add(iter.next());
		}

		ServiceResultSet<SettingBean> res = new ServiceResultSet<SettingBean>();
		res.setResult(beans);
		
		if (offset > 0) {
			res.setHasPrev(true);
		}
		
		if (myLimit > 0 && beans.size() > limit) {
			res.setHasNext(true);
			beans.remove(beans.size() - 1);
		}
		
		return res;
	}

	@Override
	public int getSettingsSize(SearchContext<SettingBean> searchContext)throws ServiceException {
		checkUpdate();
		List<SettingBean> copyList = CollectionUtils.copyList(settings);

		if(searchContext != null) {
			SearchCondition<SettingBean> searchCondition = searchContext.getCondition();
			copyList = searchCondition.findAll(copyList);
		}
		return copyList.size();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getSettingValue(String settingKey, Class<T> type)
			throws ServiceException {
		checkUpdate();
		SettingBean setting = this.getSetting(settingKey);
		if (setting == null) {
			SettingDefinition def = SettingDefinitions.get(settingKey);
			try {
			return (T)def.getDefaultValue();
			} catch (Throwable e) {
				throw new ServiceException("Incompatible setting type", e);
			}
		}
		return getSettingValue(setting, type);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getSettingValue(SettingBean setting, Class<T> type)
			throws ServiceException {
		if (setting.getValue() == null) {
			return null;
		}
		
		if (type == String.class) {
			return (T)setting.getValue();
		}
		
		switch(setting.getTypeEnum()) {
			case INTEGER:
				if (setting.getValue().trim().isEmpty()) {
					return null;
				}
				try {					
					return (T)(Integer)Integer.parseInt(setting.getValue());
				} catch (Throwable e) {
					throw new ServiceException("Unable to parse setting value", e);
				}
			case DOUBLE:
			case PERCENTAGE:
				if (setting.getValue().trim().isEmpty()) {
					return null;
				}
				
				try {
					return (T)(Double)Double.parseDouble(setting.getValue());
				} catch (Throwable e) {
					throw new ServiceException("Unable to parse setting value", e);
				}
			case BOOLEAN:
				if (setting.getValue().trim().isEmpty()) {
					return null;
				}
				try {
					return (T)(Boolean)Boolean.parseBoolean(setting.getValue());
				} catch (Throwable e) {
					throw new ServiceException("Unable to parse setting value", e);
				}
			case LONG:
				if (setting.getValue().trim().isEmpty()) {
					return null;
				}
				try {
					return (T)(Long)Long.parseLong(setting.getValue());
				} catch (Throwable e) {
					throw new ServiceException("Unable to parse setting value", e);
				}
			case STRING:
			case COUNTRY:
			case MULTILINE_STRING:
				try {
					return (T)setting.getValue();
				} catch (Throwable e) {
					throw new ServiceException("Unable to parse setting value", e);
				}
			default:
				throw new ServiceException("Unknown setting type");
		}
	}
	
	public void updateSettings(List<SettingBean> settings) throws ServiceException {
		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null) {
			throw new ServiceException("Out of database connections");
		}

		PreparedStatement statement = null;
		
		try {
			String sql = 
					"SELECT change_system_setting(?,?,?)";

			dbConn.setAutoCommit(false);
			statement = dbConn.prepareStatement(sql);
			
			for (SettingBean s: settings) {
				SettingBean orig = this.getSetting(s.getKey());
				if (orig.getValue() == null && s.getValue() == null) {
					continue;
				}
				
				if (orig.getValue() != null && orig.getValue().equals(s.getValue())) {
					continue;
				}
				
				statement.setString(1, s.getKey());
				statement.setString(2, s.getValue());
				statement.setLong(3, principalService.getPrincipal().getUserId());
				
				ResultSet rs = null;
				try {
					rs = statement.executeQuery();
					rs.next();
				} finally {
					DbUtil.close(rs);
				}
			}
			dbConn.commit();
			
		} catch (SQLException e) {
			log.error("Unable to save settings", e);
			try {
				dbConn.rollback();
			} catch (SQLException e1) {
			}
			
		} finally {
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
	}

	@Override
	public void invalidate() {
		this.lastUpdate = 0;
	}
}
