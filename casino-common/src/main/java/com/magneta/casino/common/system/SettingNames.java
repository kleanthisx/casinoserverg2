package com.magneta.casino.common.system;

public class SettingNames {

	public static final String SYSTEM_NAME = "system.name";

	public static final String DEFAULT_PROMO_RATE = "system.default_promo_credits_percentage";
	public static final String GAME_CLIENTS = "game.clients";
}
