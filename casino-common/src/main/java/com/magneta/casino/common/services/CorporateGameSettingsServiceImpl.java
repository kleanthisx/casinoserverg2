package com.magneta.casino.common.services;

import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.common.utils.SqlBuilder;
import com.magneta.casino.services.CorporateGameSettingsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CorporateGameSettingBean;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class CorporateGameSettingsServiceImpl  implements CorporateGameSettingsService {

	private final CorporatesService corporateService;
	
	public CorporateGameSettingsServiceImpl(CorporatesService corporateService) {
		this.corporateService = corporateService;
	}
	
	@Override
	public CorporateGameSettingBean getCorporateGameSettings(Long corporateId, Integer gameId) throws ServiceException {
		
		SqlBuilder<CorporateGameSettingBean> sqlBuilder = new SqlBuilder<CorporateGameSettingBean>(CorporateGameSettingBean.class);
		
		SearchContext<CorporateGameSettingBean> searchContext = FiqlContextBuilder.create(CorporateGameSettingBean.class, "corporateId==?;gameId==?", corporateId, gameId);
		
		List<CorporateGameSettingBean> result = sqlBuilder.executeQuery(searchContext, null, 0, 0, null).getResult();
		
		if (result.isEmpty()) {
			return null;
		}
		
		return result.get(0);
	}
	
	@Override
	public CorporateGameSettingBean getEffectiveCorporateGameSettings(Long corporateId, Integer gameId) throws ServiceException {
		List<Long> upperCorporates = corporateService.getCorporateHierarchy(corporateId);
		
		List<Long> corporates = new ArrayList<Long>();
		corporates.add(corporateId);
		corporates.addAll(upperCorporates);
		
		SqlBuilder<CorporateGameSettingBean> sqlBuilder = new SqlBuilder<CorporateGameSettingBean>(CorporateGameSettingBean.class);
		
		SearchContext<CorporateGameSettingBean> searchContext = FiqlContextBuilder.create(CorporateGameSettingBean.class, "corporateId=in=?;gameId==?", corporates, gameId);
		SortContext<CorporateGameSettingBean> sortContext = new SortContext<CorporateGameSettingBean>("corporateId");
		
		List<CorporateGameSettingBean> result = sqlBuilder.executeQuery(searchContext, null, 0, 0, sortContext).getResult();
		
		if (result.isEmpty()) {
			return null;
		}
		
		return result.get(0);
	}
}
