package com.magneta.casino.reports;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.magneta.casino.common.services.CurrencyParserImpl;
import com.magneta.casino.common.services.UsernameServiceImpl;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.enums.GameCategoryEnum;

/**
 * The only class used by reports. 
 * 
 * You shouldn't use this class outside of reports.
 * 
 * @author anarxia
 */
public class ReportUtil {
	
	private static CurrencyParser currencyParser;
	private static UsernameService usernameService;

	private static final class DummyCurrencyServiceImpl implements CurrencyService {

		@Override
		public Currency getActiveCurrency() {
			return Currency.getInstance("EUR");
		}
	}
	
	private static final class DummyUserAccessServiceImpl implements UserAccessService {

		@Override
		public boolean canAccess(Long userId) {
			return true;
		}		
	}
	
	/* Only used for report testing */
	static {
		CurrencyParser currencyParser = new CurrencyParserImpl(new DummyCurrencyServiceImpl());
		UserAccessService userAccessService = new DummyUserAccessServiceImpl();
		UsernameService usernameService = new UsernameServiceImpl(userAccessService);
		
		init(currencyParser, usernameService);
	}
	
	/**
	 * Initialization method. Applications must call this on startup.
	 * 
	 * @param currencyService
	 * @param usernameService
	 * @param gameCategoriesService
	 */
	public static void init(CurrencyParser currencyParser,
			UsernameService usernameService) {
		ReportUtil.currencyParser = currencyParser;
		ReportUtil.usernameService = usernameService;
	}
	
	@Deprecated
    public static String getUsername(Long userId) {
    	try {
			return usernameService.getUsername(userId);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
    }
    
    @Deprecated
    public static String getCategory(Integer categoryId) {
    	
    	GameCategoryEnum val = GameCategoryEnum.valueOf(categoryId);
    	
    	if (val == null) {
    		return null;
    	}
    	
    	return val.getName(Locale.US);
    }

	/**
     * Formats the given double amount into a string which truncates all but the two last decimals.
     * @param amount The amount to format.
     * @param locale The locale of the client.
     * @return The formatted amount.
     */
    @Deprecated
    public static String getFormattedAmount(Double amount, Locale locale) {
    	if (amount == null) {
    		return null;
    	}
    	
    	return currencyParser.formatDouble(amount, locale);
    }
    
    @Deprecated
    public static String getFormattedAmount(double amount) {
    	return currencyParser.formatDouble(amount, Locale.getDefault());
    }

    /**
     * Formats the given date into a string for the given time zone and locale. 
     * @param date The date to format.
     * @param destZone The time zone to format the date in.
     * @param locale The locale for the formatted date.
     * @return A string with the formatted date.
     */
    @Deprecated
    public static String getFormattedDate(Date date, TimeZone destZone, Locale locale) {     
        if (date == null) {
            return "";
        }
        if (destZone == null) {
            destZone = TimeZone.getDefault();
        }
        
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        format.setTimeZone(destZone);
        
        DateFormat dateFormat;
        
        if (locale == null) {
            dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        } else {
            dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        }

        dateFormat.setTimeZone(destZone);
        
        return dateFormat.format(date) + " " + 
            format.format(date.getTime());
    }
    
    /**
     * Gets a localized formatted string representing the selected date part.
     * @param date The date to format.
     * @param part The date part to get.
     * @param timeZone The time zone for the date.
     * @param locale The locale for the string localization.
     * @return The formatted string.
     */
    @Deprecated
    public static String getDatePart(Date date, String part, TimeZone timeZone, Locale locale){
        Calendar calendar = Calendar.getInstance(timeZone, locale);
        calendar.setTime(date);
        
        if (("year").equals(part)){
            return new SimpleDateFormat("yyyy", locale).format(calendar.getTime());
        } else if (("month").equals(part)){
            return new SimpleDateFormat("MMMM yyyy", locale).format(calendar.getTime());
        } else if (("day").equals(part)){
            return new SimpleDateFormat("EEEE d/MM/yyyy", locale).format(calendar.getTime());
        } else {
            return null;
        }
    }
    
    /**
     * Method used from jasperreports.
     * 
     * @param timestamp
     * @param part
     * @param timeZone
     * @param locale
     * @return
     */
    @Deprecated
    public static String getDatePart(Timestamp timestamp, String part, TimeZone timeZone, Locale locale) {
    	return getDatePart((Date)timestamp, part, timeZone, locale);
    }
}
