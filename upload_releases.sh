#!/bin/sh
# Upload releases to a remote server using scp
# You must run ./build_all.sh before running this

set -e

show_help() {
  echo "Uploads all releases using scp"
  echo "Usage: $0 <destination>"
}

if [ -z "$1" ]; then
   show_help $0
   exit 1
fi

if [ "--help" = "$1" ]; then
   show_help $0
   exit 0
fi

dest=$1

scp backoffice/target/backoffice.war \
    partner-webapp/target/partner-webapp.war \
    $dest
