package com.magneta.administration.commons.services.reporting;

import org.apache.tapestry.engine.ServiceEncoder;
import org.apache.tapestry.engine.ServiceEncoding;
import org.apache.tapestry.services.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportGenerationServiceEncoder implements ServiceEncoder {

	public static final String REPORT_NAME_PARAMETER = "report";
	public static final String FORMAT_PARAMETER = "format";
	
	private static final Logger log = LoggerFactory.getLogger(ReportGenerationServiceEncoder.class);
	
	private String reportPath =  "report";
	
	@Override
    public void decode(ServiceEncoding encoding) {
		String path = encoding.getServletPath();
		
		/* Detect if its our own url */
		if (!path.equalsIgnoreCase("/"+ reportPath)) {
			return;
		}

		String pathInfo = encoding.getPathInfo();
		
		int dotx = pathInfo.lastIndexOf('.');
        if (dotx < 0) return;

        String format = pathInfo.substring(dotx + 1);
        String report = pathInfo.substring(1, dotx);
		
		encoding.setParameterValue(ServiceConstants.SERVICE, ReportGenerationService.SERVICE_NAME);
		encoding.setParameterValue(REPORT_NAME_PARAMETER, report);
		encoding.setParameterValue(FORMAT_PARAMETER, format);
		log.debug("ReportServiceEncoder: \"" + encoding.getParameterValue(REPORT_NAME_PARAMETER) + "\", Format: \"" + encoding.getParameterValue(FORMAT_PARAMETER) + "\"");
	}

	@Override
    public void encode(ServiceEncoding encoding) {
		String service = encoding.getParameterValue(ServiceConstants.SERVICE);

		if (!service.equals(ReportGenerationService.SERVICE_NAME)) {
			return;
		}
		
		String report = encoding.getParameterValue(REPORT_NAME_PARAMETER);
		String format = encoding.getParameterValue(FORMAT_PARAMETER);
		
		if (format == null) {
			format = "html";
		}
		
		if (report == null) {
			return;
		}
		
		StringBuilder buf = new StringBuilder("/");
		buf.append(reportPath);
		buf.append('/');
		buf.append(report);
		buf.append('.');
		buf.append(format.toLowerCase());
		
		encoding.setServletPath(buf.toString());
		encoding.setParameterValue(ServiceConstants.SERVICE, null);
		encoding.setParameterValue(REPORT_NAME_PARAMETER, null);
		encoding.setParameterValue(FORMAT_PARAMETER, null);
	}

	public void setReportPath(String path) {
		this.reportPath = path;
	}
	
	public String getReportPath() {
		return reportPath;
	}
	
}
