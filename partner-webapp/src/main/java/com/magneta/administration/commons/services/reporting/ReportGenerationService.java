package com.magneta.administration.commons.services.reporting;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.services.LinkFactory;
import org.apache.tapestry.services.ServiceConstants;
import org.apache.tapestry.util.ContentType;
import org.apache.tapestry.web.WebContext;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;
import org.apache.tapestry.web.WebSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class ReportGenerationService implements IEngineService {
	
	private static final Logger log = LoggerFactory.getLogger(ReportGenerationService.class);
	
	public static final String SERVICE_NAME = "reporting";   
	
	private WebContext context;
  	private LinkFactory linkFactory;
    
  	private PrincipalService principalService;
  	
  	public void setPrincipalService(PrincipalService principalService) {
  		this.principalService = principalService;
  	}
  	
  	public PrincipalService getPrincipalService() {
  		return this.principalService;
  	}
  	
  	public abstract void validateServiceAccess(IRequestCycle cycle) throws IOException;

	public void setLinkFactory(LinkFactory factory) {
		this.linkFactory = factory;
	}
  	
	public void setContext(WebContext context) {
		this.context = context;
	}
	
	@Override
    public ILink getLink(boolean post, Object parameter) {

		Object [] params = (Object[])parameter; 

		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put(ServiceConstants.SERVICE, getName());

		for (Object param: params) {
			ServiceParameter p = (ServiceParameter)param;
			String stringVal = ServiceUtils.encodeToString(p.getParameterValue());
			if (stringVal != null && stringVal.length() > 0) {
				parameters.put(p.getParameterName(), stringVal);
			}
		}

		return linkFactory.constructLink(this, post, parameters, true);
	}

	@Override
    public String getName() {
		return SERVICE_NAME;
	}

	@Override
    public void service(IRequestCycle cycle) throws IOException {
		
		WebRequest request = cycle.getInfrastructure().getRequest();
		
		WebSession session = request.getSession(false);
		if (session == null) {
			cycle.getInfrastructure().getResponse().sendError(503, "Session Expired");
			return;
		}
		
		ExtendedPrincipalBean principal = this.getPrincipalService().getPrincipal();

		if (principal == null) {
			cycle.getInfrastructure().getResponse().sendError(HttpServletResponse.SC_FORBIDDEN, "You need to log in to access the reports");
			return;
		}
		
		validateServiceAccess(cycle);		
		generateReport(cycle, cycle.getInfrastructure().getRequest(), cycle.getInfrastructure().getResponse());
	}
	
	private static JasperReport getReport(String reportsRealPath, String report) throws JRException {
	    
	    File designFile = new File(reportsRealPath+"/"+report+".jrxml");
	    if (!designFile.exists()) {
	        designFile = new File(reportsRealPath+"/"+report+"_report.jrxml");
	    }
	    	    
		File reportFile = new File(reportsRealPath+"/"+report+".jasper");
		File lockFile = new File(reportsRealPath+"/"+report+".jasper.lock");
		
		/* If the lock file exists someone else is generating the report.
		 * Wait for it to go away (the report will be compiled). 
		 */
		if (lockFile.exists()) {
			while(lockFile.exists()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					Thread.interrupted();
				}
			}
		
		/* Compile report if:
		 * - Compiled report file is missing.
		 * - Design file is newer than the compiled report
		 */
		} else if (!reportFile.exists() || (designFile.exists() && designFile.lastModified() > reportFile.lastModified())) {	

			if (!designFile.exists()) {
				return null;
			}

			/*
			 * Use a lock file while compiling the report to avoid many threads
			 * compiling the same report at the same time.
			 * The lock file is deleted after the report is compiled
			 */
			boolean compileReport;

			try {
				compileReport = lockFile.createNewFile();
			} catch (IOException e1) {
				throw new JRException("Could not create lock file");
			}

			if (compileReport) {		

				try {
					JasperCompileManager.compileReportToFile(designFile.getPath(), reportFile.getPath());
				} finally {
					lockFile.delete();
				}
			} else {

				/* Another thread is compiling the report
				 * Wait for the lock file to go away.
				 */
				while(lockFile.exists()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}

			}


		}

		return (JasperReport)JRLoader.loadObject(reportFile); 
	}
	
	@SuppressWarnings("unchecked")
	private void generateReport(IRequestCycle cycle, WebRequest request, WebResponse response){
		Map<String,Object> requestParams = new HashMap<String,Object>();
		
		List<String> paramNames = request.getParameterNames();
		
		for (String param: paramNames) {
			requestParams.put(param, request.getParameterValue(param));
		}
		
		String report = (String)requestParams.get("report");
		if (report == null) {
			report = cycle.getParameter("report");
		}
		
		String format = (String)requestParams.get("format");
		if (format == null) {
			format = cycle.getParameter("format");
		}
		
		if (report == null || format == null){
			try {
				if (report == null && format == null) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad request; Report and format not specified.");
				} else if (report == null) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad request: Report is not specified.");
				} else {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad request: Format not specified.");
				}
				
				
			} catch (IOException e) {
				log.error("IO error occured while sending reponse error",e);
			}
			log.warn("Required parameter was not provided.");
			return;
		}
		
		
		format = format.toLowerCase();

		ContentType contentType = ReportUtil.getContentType(format);
		
		if (contentType == null ){
			try {
				response.sendError(400, "Invalid report file format.");
			} catch (IOException e) {
				log.error("IO error occured while sending reponse error",e);
			}
			log.warn("Invalid report format requested.");
			return;
		}
		
		String reportsRealPath = context.getRealPath("/reports/");
		
		JasperReport jasperReport = null;
		
		try{
			jasperReport = getReport(reportsRealPath, report);
		} catch (Exception e){
			try {
				response.sendError(400, "Error while generating report.");
			} catch (IOException e1) {
				log.error("IO error occured while sending reponse error",e);
			}
			log.error("Error loading report.",e);
			return;
		}

		if (jasperReport == null) {
			try {
				response.sendError(400, "Report does not exist");
			} catch (IOException e) {
				log.error("IO error occured while sending reponse error",e);
			}
			return;
		}
		
		Locale locale = request.getLocale();
		
		Map<String,Object> paramVals = getReportParameterValues(jasperReport, format, locale, requestParams);
		
		PrincipalService principalService = getPrincipalService();		
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		
		Long userId = principal.getUserId();
		
		if (userId != null) {
			paramVals.put("request_user", userId);
		} else {
			paramVals.remove("request_user");
		}
		
		if (principal.hasPrivilege(PrivilegeEnum.CORPORATE, false)) {
			paramVals.put("show_corporate", true);
		} else {
			paramVals.put("show_corporate", false);
		}
		
		//setExtraParameters(appStateManager, paramVals);
		
		JRFileVirtualizer virtualizer = new JRFileVirtualizer(2, System.getProperty("java.io.tmpdir"));
		paramVals.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		paramVals.put(JRFileVirtualizer.PROPERTY_TEMP_FILES_SET_DELETE_ON_EXIT, false);
		
		Connection connection = ConnectionFactory.getReportsConnection();
		JasperPrint jasperPrint = null;
		
		if (connection == null){
			try {
				response.sendError(400, "Error while generating report.");
			} catch (IOException ioExc) {
				log.error("IO error occured while sending response error",ioExc);
			}
			log.error("Could not connect to database.");
			return;
		}
		
		/*Log.info("Report parameters:");
		for (Entry<String, Object> e: paramVals.entrySet()) {
			Log.info(e.getKey() + "=" + e.getValue());
		}*/
		
		try{
			jasperPrint = JasperFillManager.fillReport(jasperReport, paramVals, connection);
			
			log.debug("JasperPrint time zone {}", jasperPrint.getTimeZoneId());
		} catch (JRException e){
			try {
				response.sendError(400, "Error while generating report.");
			} catch (IOException ioExc) {
				log.error("IO error occured while sending response error",ioExc);
			}
			log.error("Could not fill report.",e);
			return;
		} finally {
		    DbUtil.close(connection);
		}
		
		OutputStream outputStream = null;
		
		try{
			outputStream = response.getOutputStream(contentType);
			
			response.setHeader("Content-Disposition","inline; filename=\""+report+"_report."+format+"\"");
			response.setHeader("Content-Type",contentType.getMimeType()+";charset=utf-8");
			response.setHeader("Cache-Control", "no-cache");
			
			try {
			    JRExporter exporter = ReportUtil.createExporter(format);
			    if (exporter != null) {
			        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
			        exporter.exportReport();
			    } else {
			        response.sendError(400, "Invalid report file format.");
			    }
			} catch (JRException e) {
			    log.error("Error while generating report.", e);
			    response.sendError(400, "Error while generating report.");
			} finally {
			   
			}
			
		} catch (IOException e){
			log.error("IO error occured while generating report", e);
		} finally {
			if (outputStream != null){
				try {
					outputStream.flush();
				} catch (IOException e) {
					log.debug("Error flushing output stream", e);
				}
				try {
					outputStream.close();
				} catch (IOException e) {
					log.debug("Error closing output stream", e);
				}
			}
			
			virtualizer.cleanup();
		}
	}

	@SuppressWarnings("rawtypes")
    private Map<String,Object> getReportParameterValues(JasperReport jasperReport, String format, Locale locale, Map<String,Object> requestParams){
		JRParameter[] reportParams = jasperReport.getParameters();
		Map<String,Class> reportParamTypes = new HashMap<String,Class>();
		Map<String,Object> paramVals = ReportUtil.createReportParameters(format, locale);
		
		for (JRParameter param: reportParams){
			log.debug("Report Parameter '"+param.getName()+"' of type "+param.getValueClassName());
			reportParamTypes.put(param.getName(), param.getValueClass());
		}
		
		Iterator<String> it = reportParamTypes.keySet().iterator();
		String nextParam;
		while (it.hasNext()){
			nextParam = it.next();
			
			String val = (String)requestParams.get(nextParam);
			
			if (val != null){
				paramVals.put(nextParam, ServiceUtils.decodeString(val, reportParamTypes.get(nextParam)));
			}	
		}
		
		return paramVals;
	}
}
