/**
 * 
 */
package com.magneta.administration.commons.services.reporting;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;

import org.apache.tapestry.util.ContentType;

/**
 * @author anarxia
 *
 */
public class ReportUtil {

    public static final ContentType getContentType(String format) {
        ContentType contentType = null;

        if (format == null) {
            return null;
        }

        format = format.toLowerCase();

        if (format.equals("pdf")){
            contentType = new ContentType("application/pdf");
        } else if (format.equals("html")){
            contentType = new ContentType("text/html");
        } else if (format.equals("xls")){
            contentType = new ContentType("application/vnd.ms-excel");
        } else if (format.equals("rtf")){
            contentType = new ContentType("application/rtf");
        } else if (format.equals("odt")) {
            contentType = new ContentType("application/vnd.oasis.opendocument.text");
        } else if (format.equals("docx")) {
            contentType = new ContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document"); 
        }
        return contentType;
    }
    
    private static final String HTML_HEADER = "<html>\n" +
    	"<head>\n" +
    	"  <title>Report</title>\n" +
    	"  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
    	"  <style type=\"text/css\">\n" +
    	"    a {text-decoration: none}\n" +
    	"  </style>\n" +
    	"</head>\n" +
    	"<body text=\"#000000\" link=\"#000000\" alink=\"#000000\" vlink=\"#000000\">\n" +
    	"<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
    	"<tr><td width=\"50%\">&nbsp;</td><td align=\"center\">\n" +
    	"\n";
	
    public static final JRExporter createExporter(String format) {
        JRExporter exporter = null;
        
        if (format.equals("pdf")){
            exporter = new JRPdfExporter();
            exporter.setParameter(JRPdfExporterParameter.METADATA_TITLE, "Report");
        } else if (format.equals("html")){
            exporter = new JRHtmlExporter();
            exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
            exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, HTML_HEADER);
        } else if (format.equals("xls")){
            exporter = new JRXlsExporter();
            exporter.setParameter(JRXlsAbstractExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
            exporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
            exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        } else if (format.equals("rtf")){
            exporter = new JRRtfExporter();
        } else if (format.equals("odt")) {
            exporter = new JROdtExporter();
        } else if (format.equals("docx")) {
            exporter = new JRDocxExporter();
        }
        
        if (exporter != null) {
            exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
        }
        
        return exporter;
    }
    
    public static Map<String, Object> createReportParameters(String format, Locale locale) {
    	Map<String,Object> params = new HashMap<String,Object>();
    	
    	if ("xls".equals(format)) {
    		params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
    	} else if ("html".equals(format)) {
    		params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
    	}
    	
    	if (locale != null) {
    		params.put(JRParameter.REPORT_LOCALE, locale);
    	}	
    	
    	return params;
    }
}
