package com.magneta.administration.commons.services.reporting;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

public class ServiceUtils {

	/**
	 * Gets a string representation of the given object specific to the report generation service. 
	 * @param o The object to encode.
	 * @return The string representing the object.
	 */
	public static String encodeToString(Object o){
		if (o == null) {
			return null;
		} 
		
		if (o instanceof Integer || o instanceof Double || o instanceof Boolean
			|| o instanceof Long) {
			return o.toString();
		} else if (o instanceof String){
			return (String)o;
		} else if (o instanceof Date){
			return String.valueOf(((Date)o).getTime());
		} else if (o instanceof TimeZone){
			return ((TimeZone)o).getID();
		} else if (o instanceof Locale){
			Locale locale = (Locale)o;
			StringBuilder buf = new StringBuilder();
			
			buf.append(locale.getLanguage());
			
			if (locale.getCountry().length() > 0) {
				buf.append('-');
				buf.append(locale.getCountry());
			}
			
			if (locale.getVariant().length() > 0) {
				buf.append('-');
				buf.append(locale.getVariant());
			}
			
			return buf.toString();
			
		} else {
			throw new RuntimeException("Data type "+o.getClass().getName()+" not supported.");
		}
	}
	
	/**
	 * Gets the object value from the encoded string represenation given.
	 * @param value The string representing the object.
	 * @param targetClass The class the object should be an instance of.
	 * @return The object value of the encoded string represenation.
	 */
	public static Object decodeString(String value, Class<?> targetClass) {
		if (targetClass == String.class){
			return value;
		} else if (targetClass == Integer.class){
			return Integer.parseInt(value);
		} else if (targetClass == Long.class){
			return Long.parseLong(value);
		} else if (targetClass == Double.class){
			return Double.parseDouble(value);
		} else if (targetClass == Boolean.class){
			return Boolean.parseBoolean(value);
		} else if (targetClass == Timestamp.class){
			return new Timestamp(Long.parseLong(value));
		} else if (targetClass == TimeZone.class){
			return TimeZone.getTimeZone(value);
		} else if (targetClass == Locale.class){
			StringTokenizer strTok = new StringTokenizer(value,"-");
			
			if (strTok.countTokens() == 1){
				String language = strTok.nextToken();
				return new Locale(language);
			} else if (strTok.countTokens() == 2){
				String language = strTok.nextToken();
				String country = strTok.nextToken();
				return new Locale(language, country);
			} else if (strTok.countTokens() > 2){
				String language = strTok.nextToken();
				String country = strTok.nextToken();
				String variant = strTok.nextToken();
				return new Locale(language, country, variant);
			} else {
				return null;
			}
		} else {
			throw new RuntimeException("Decoding for data type "+targetClass.getName()+" not supported.");
		}
	}
}
