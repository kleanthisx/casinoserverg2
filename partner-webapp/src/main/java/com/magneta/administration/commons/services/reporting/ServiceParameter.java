package com.magneta.administration.commons.services.reporting;

import java.io.Serializable;

public class ServiceParameter implements Serializable{

	private static final long serialVersionUID = 1457395101411963136L;
	
	private final String paramName;
	private final Object paramValue;

	public ServiceParameter(String paramName,Object paramValue){
		this.paramName = paramName;
		this.paramValue = paramValue;
	}

	public String getParameterName() {
		return paramName;
	}

	public Object getParameterValue() {
		return paramValue;
	}
	
}
