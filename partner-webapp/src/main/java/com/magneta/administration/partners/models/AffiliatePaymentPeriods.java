/**
 * 
 */
package com.magneta.administration.partners.models;

import org.apache.tapestry.form.IPropertySelectionModel;

/**
 * @author User
 *
 */
public class AffiliatePaymentPeriods implements IPropertySelectionModel {

    private String[] labels = {"Every Week", "Every Month"};
    private String[] values = {"7 days", "1 mon"};
    
    @Override
	public String getLabel(int arg0) {
        return labels[arg0];
    }

    @Override
	public Object getOption(int arg0) {
        return labels[arg0];
    }

    @Override
	public int getOptionCount(){
        return values.length;
    }

    @Override
	public String getValue(int arg0) {
        return values[arg0];
    }

    @Override
	public boolean isDisabled(int arg0) {
        return false;
    }

    @Override
	public Object translateValue(String arg0) {
        return arg0;
    }
}