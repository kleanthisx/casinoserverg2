package com.magneta.administration.partners.reports;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.administration.commons.services.reporting.ReportUtil;
import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.administration.commons.services.reporting.ServiceUtils;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class ReportGenerationQueue implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ReportGenerationQueue.class);
	
	private static ReportGenerationQueue _instance;

	public static void init() throws ServiceException {
		_instance = new ReportGenerationQueue();
	}
	
	public static ReportGenerationQueue getInstance(){
		return _instance;
	}
	
	private ServletContext context;
	private ArrayBlockingQueue<ReportGenerationRequest> queue;
	private Map<Long, List<ReportGenerationRequest>> repository;
	private boolean stopped;
	
	private final SettingsService settingsService;
	private final PrincipalService principalService;
	
	private ReportGenerationQueue() throws ServiceException{
		this.settingsService = ServiceLocator.getService(SettingsService.class);
		this.principalService = ServiceLocator.getService(PrincipalService.class);
		
		queue = new ArrayBlockingQueue<ReportGenerationRequest>(settingsService.getSettingValue("system.reports.queue_size", Integer.class));
		repository = new HashMap<Long, List<ReportGenerationRequest>>();
		stopped = false;
	}
	
	public boolean clearResources() {
		File genReportsFolder = new File(context.getRealPath("/reports/gen/"));
		if (genReportsFolder.exists()){
			return genReportsFolder.delete();
		}

		return true;
	}
	
	/**Enqueues the specified report request into the queue and returns the position
	 * where the request was placed, or -1 if it could not fit into the queue.*/
	public int enqueueReport(ReportGenerationRequest reportRequest){
		try {
			if (queue.add(reportRequest)){
				return queue.size();
			} 
			return -1;
		} catch (IllegalStateException e){
			return -1;
		}
	}
	
	public void setContext(ServletContext context) {
		this.context = context;
	}

	@Override
	public void run() {
		log.info("Starting reports generation thread.");
		
		while(!stopped){
			ReportGenerationRequest reportRequest = queue.poll();
			if (reportRequest != null){
				try {
					executeReport(reportRequest);
				} catch (Exception e){
					log.warn("Exception while executing report", e);
				}
			} else {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) { Thread.interrupted(); }
			}
		}
	}
	
	private void executeReport(ReportGenerationRequest reportRequest) throws ServiceException, JRException {
		log.debug("Starting generation of report: "+reportRequest.getReport());
		String uID = generateReport(reportRequest);
		if (uID != null){
			reportRequest.setUID(uID);
			addToRepository(reportRequest);
		}
		log.debug("Finished generation of report: "+reportRequest.getReport()+"; File UID is: "+uID);
	}
	
	private void addToRepository(ReportGenerationRequest reportRequest) throws ServiceException{
		Integer maxRepoSize = this.settingsService.getSettingValue("system.reports.max_repo_size_per_user", Integer.class);
		
		List<ReportGenerationRequest> list = repository.get(reportRequest.getPrincipal().getUserId());

		if (list != null){
			//in case we exceed the maximum size of the current user's repository then remove the 
			//report from the list and delete the respective file.
			synchronized(list){
				if (list.size() + 1 > maxRepoSize) {
					ReportGenerationRequest tmp = list.remove(0);
					File oldFile = new File(context.getRealPath("/reports/gen/")+"/"+tmp.getUID()+"."+tmp.getFormat());
					oldFile.delete();
				}
				list.add(reportRequest);
			}
		} else {
			List<ReportGenerationRequest> newList = new ArrayList<ReportGenerationRequest>();
			newList.add(reportRequest);
			repository.put(reportRequest.getPrincipal().getUserId(), newList);
		}
	}

	private String generateReport(ReportGenerationRequest reportRequest) throws JRException {		
		String reportsRealPath = context.getRealPath("/reports/");
		String report = reportRequest.getReport();
		String format = reportRequest.getFormat().toLowerCase();
		
		JasperReport jasperReport = getReport(reportsRealPath, report);

		if (jasperReport == null) {
			log.error("Error while generating report: report was not found.");
			return null;
		}
		
		Map<String,Object> paramVals = getReportParameterValues(jasperReport, format, reportRequest.getParams());
		
		JRFileVirtualizer virtualizer = new JRFileVirtualizer(2, System.getProperty("java.io.tmpdir"));
		try {
			paramVals.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			paramVals.put(JRFileVirtualizer.PROPERTY_TEMP_FILES_SET_DELETE_ON_EXIT, false);

			Connection connection = ConnectionFactory.getReportsConnection();

			if (connection == null){
				throw new JRException("Could not connect to database.");
			}

			JasperPrint jasperPrint = null;

			try {
				principalService.pushSystem();
				jasperPrint = JasperFillManager.fillReport(jasperReport, paramVals, connection);
			} finally {
				DbUtil.close(connection);
				principalService.popSystem();
			}


			//create the temporary directory if it doesn't exist
			File dir = new File(reportsRealPath+"/gen/");
			if (!dir.exists()){
				dir.mkdir();
			}

			String uID = report+reportRequest.getTime().getTime();
			String file = reportsRealPath+"/gen/"+uID+"."+format;

			JRExporter exporter = ReportUtil.createExporter(format);
			if (exporter != null) {
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file);
				exporter.exportReport();
			} else {
				throw new JRException("Invalid report file format.");
			}

			return uID;
		} finally {
			virtualizer.cleanup();
		}
	}
	
	private static JasperReport getReport(String reportsRealPath, String report) throws JRException {
		File designFile = new File(reportsRealPath+"/"+report+".jrxml");
		if (!designFile.exists()) {
			designFile = new File(reportsRealPath+"/"+report+"_report.jrxml");
		}

		File reportFile = new File(reportsRealPath+"/"+report+".jasper");
		File lockFile = new File(reportsRealPath+"/"+report+".jasper.lock");
		
		/* If the lock file exists someone else is generating the report.
		 * Wait for it to go away (the report will be compiled). 
		 */
		if (lockFile.exists()) {
			while(lockFile.exists()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					Thread.interrupted();
				}
			}
		
		/* Compile report if:
		 * - Compiled report file is missing.
		 * - Design file is newer than the compiled report
		 */
		} else if (!reportFile.exists() || (designFile.exists() && designFile.lastModified() > reportFile.lastModified())) {	

			if (!designFile.exists()) {
				return null;
			}

			/*
			 * Use a lock file while compiling the report to avoid many threads
			 * compiling the same report at the same time.
			 * The lock file is deleted after the report is compiled
			 */
			boolean compileReport;

			try {
				compileReport = lockFile.createNewFile();
			} catch (IOException e1) {
				throw new JRException("Could not create lock file");
			}

			if (compileReport) {		

				try {
					JasperCompileManager.compileReportToFile(designFile.getPath(), reportFile.getPath());
				} finally {
					lockFile.delete();
				}
			} else {

				/* Another thread is compiling the report
				 * Wait for the lock file to go away.
				 */
				while(lockFile.exists()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}

			}
		}

		return (JasperReport)JRLoader.loadObject(reportFile); 
	}
	
	@SuppressWarnings("rawtypes")
	private Map<String,Object> getReportParameterValues(JasperReport jasperReport, String format, List<ServiceParameter> params) {
		Map<String,Object> requestParams = ReportUtil.createReportParameters(format, Locale.getDefault());
		
		for (ServiceParameter p: params) {
			String stringVal = ServiceUtils.encodeToString(p.getParameterValue());
			if (stringVal != null && stringVal.length() > 0) {
				requestParams.put(p.getParameterName(), stringVal);
			}
		}
		
		JRParameter[] reportParams = jasperReport.getParameters();
		Map<String,Class> reportParamTypes = new HashMap<String,Class>();
		Map<String,Object> paramVals = new HashMap<String,Object>();
		
		for (JRParameter param: reportParams){
			log.debug("Report Parameter '"+param.getName()+"' of type "+param.getValueClassName());
			reportParamTypes.put(param.getName(), param.getValueClass());
		}
		
		Iterator<String> it = reportParamTypes.keySet().iterator();
		String nextParam;
		while (it.hasNext()){
			nextParam = it.next();
			
			Object val = requestParams.get(nextParam);
			
			if (val != null){
				if (val instanceof String) {
					paramVals.put(nextParam, ServiceUtils.decodeString((String)val, reportParamTypes.get(nextParam)));
				} else {
					paramVals.put(nextParam, val);
				}
			}
		}
		
		return paramVals;
	}
	
	public void stop(){
		this.stopped = true;
	}

	public List<ReportGenerationRequest> getRepository(Long userID) {
		List<ReportGenerationRequest> list = repository.get(userID);
		if (list != null){
			synchronized(list){
				return Collections.unmodifiableList(list);
			}
		}
		return null;
	}
	
	public void deleteFromRepository(Long userID, String reportUid){
		List<ReportGenerationRequest> list = repository.get(userID);
		if (list == null)
			return;
		
		synchronized(list) {
			for (ReportGenerationRequest req: list) {
				if (req.getUID().equals(reportUid)) {
					list.remove(req);
					break;
				}
			}
		}
	}
	
	public void deleteFromRepository(Long userID, ReportGenerationRequest request){
		List<ReportGenerationRequest> list = repository.get(userID);
		if (list != null){
			synchronized(list){
				list.remove(request);
			}
		}
	}
}
