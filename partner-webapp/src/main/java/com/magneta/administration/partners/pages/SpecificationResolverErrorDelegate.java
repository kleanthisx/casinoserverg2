/**
 * 
 */
package com.magneta.administration.partners.pages;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry.INamespace;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.PageRedirectException;
import org.apache.tapestry.resolver.ISpecificationResolverDelegate;
import org.apache.tapestry.spec.IComponentSpecification;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;

/**  
* Custom SpecificationResolverDelegate that throws a page redirect exception  
* when an application page cannot be found. This improves upon the normal  
* Tapestry action of raising an application runtime exception.  
*/  
public class SpecificationResolverErrorDelegate implements ISpecificationResolverDelegate {  
   
    /* The tapestry page for 404 errors */
    public static final String ERROR_PAGE = "NotFound";  
 
    /*  
     * Throws a Page Redirect exception to report that a requested page  
     * specification could not be found. The target error page name is  
     * the value configured in the application specification file or the  
     * default given by DEFAULT_SPECIFICATION_ERROR_PAGE.  
     */  
    @Override
	public IComponentSpecification findPageSpecification(IRequestCycle cycle,  
            INamespace namespace, String simplePageName) {  
  
        /* Handle special case where the error page itself cannot be found */  
        if (simplePageName.equals(ERROR_PAGE))  
            return null;  
 
        WebRequest request = cycle.getInfrastructure().getRequest();
        WebResponse response = cycle.getInfrastructure().getResponse();
        
        /* Store information about the error in the request similar to Tomcat */  
        request.setAttribute("javax.servlet.error.status_code", new Integer(HttpServletResponse.SC_NOT_FOUND));  
        request.setAttribute("javax.servlet.error.message", "Unable to find the requested page");  
        request.setAttribute("javax.servlet.error.request_uri", request.getRequestURI());  
        request.setAttribute("javax.servlet.error.servlet_name", request.getServerName());  
        request.setAttribute("javax.servlet.error.exception_type", null);  
 
        /* Set the http response code to 404 */  
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);  
        
        throw new PageRedirectException(simplePageName, null, null, ERROR_PAGE);  
    }  
 
    @Override
	public IComponentSpecification findComponentSpecification(  
            IRequestCycle cycle, INamespace namespace, String type) {  
        
       return null;
    }  
}  