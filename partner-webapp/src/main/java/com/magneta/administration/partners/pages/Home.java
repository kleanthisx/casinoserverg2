package com.magneta.administration.partners.pages;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;

import com.magneta.administration.partners.pages.SecurePage;

public abstract class Home extends SecurePage {
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	@Component(id="username",
			bindings={"value=User.getUsername()"})
	public abstract Insert getUserName();
}
