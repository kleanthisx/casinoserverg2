package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.casino.services.enums.UserAffiliationEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class CorporateRelationshipsSelectionModel implements IPropertySelectionModel {

	public static final int USER_RELATIONSHIP = 1;
	public static final int SUB_CORPORATE_RELATIONSHIP = 2;
	public static final int SUPER_CORPORATE_RELATIONSHIP = 3;

	private List<Long> ids;
	private List<String> usernames;

	public CorporateRelationshipsSelectionModel(Long affiliateId, int relationship, boolean allOption, boolean activeOnly){
		this(affiliateId, relationship, allOption, activeOnly, false, false, false);
	}

	public CorporateRelationshipsSelectionModel(Long affiliateId, int relationship, boolean allOption, boolean activeOnly, boolean hideClosed) {
		this(affiliateId, relationship, allOption, activeOnly, hideClosed, false, false);
	}
	
	public CorporateRelationshipsSelectionModel(Long affiliateId, int relationship, boolean allOption, boolean activeOnly, boolean hideClosed, boolean noneOption) {	
		this(affiliateId, relationship, allOption, activeOnly, hideClosed, noneOption, false);
	}

	public CorporateRelationshipsSelectionModel(Long affiliateId, int relationship, boolean allOption, boolean activeOnly, boolean hideClosed, boolean noneOption, boolean showMe){
		Connection dbConn = ConnectionFactory.getReportsConnection();
		ids = new ArrayList<Long>();
		usernames = new ArrayList<String>(); 

		if(affiliateId == null){
			affiliateId = 0L;
		}

		if (allOption){
			ids.add(new Long(-1));
			if(!showMe) { 
				usernames.add("All");
			} else {
				usernames.add("Me");
			}
		}

		if(noneOption) {
			ids.add(new Long(-1));
			usernames.add("--Please select--");
		}

		if (dbConn != null){
			try {
				PreparedStatement statement = null;
				ResultSet res = null;

				try {
					if (relationship == USER_RELATIONSHIP){
						statement = dbConn.prepareStatement(
								"SELECT affiliate_users.affiliation, users.user_id, users.username, users.first_name, users.last_name, FALSE As super_corp"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.user_id = users.user_id"+(activeOnly? " AND is_active = TRUE" : "") +
								" WHERE affiliation = 1"+
						" AND affiliate_id = ?");
						statement.setLong(1, affiliateId);
					} else if (relationship == SUB_CORPORATE_RELATIONSHIP){
						statement = dbConn.prepareStatement(
								"SELECT affiliate_users.affiliation, users.user_id, users.username, users.first_name, users.last_name, FALSE As super_corp"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.user_id = users.user_id" + (activeOnly? " AND is_active = TRUE" : "") + (hideClosed? " AND users.closed = FALSE" : "") +
								" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id AND affiliates.approved = TRUE" +
								" WHERE affiliation = 3"+
						" AND affiliate_users.affiliate_id = ?");
						statement.setLong(1, affiliateId);
					} else if (relationship == SUPER_CORPORATE_RELATIONSHIP){
						statement = dbConn.prepareStatement(
								"SELECT affiliate_users.affiliation, users.user_id, users.username, TRUE AS super_corp"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.affiliate_id = users.user_id" +(activeOnly? " AND is_active = TRUE" : "") +
								" WHERE affiliate_users.user_id = ?" +
						" AND affiliation = 3");
						statement.setLong(1, affiliateId);
					} else {
						statement = dbConn.prepareStatement(
								" SELECT affiliate_users.affiliation, users.user_id, users.username, TRUE AS super_corp"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.affiliate_id = users.user_id" +(activeOnly? " AND is_active = TRUE" : "") +
								" WHERE affiliate_users.user_id = ?" +
								" UNION ALL" +
								" SELECT affiliate_users.affiliation, users.user_id, users.username, false AS super_corp"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.user_id = users.user_id" +(activeOnly? " AND is_active = TRUE" : "") +
								" LEFT OUTER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id"+
								" WHERE affiliate_users.affiliate_id = ? AND (affiliates.affiliate_id IS NULL OR affiliates.approved = TRUE)" +
						" ORDER BY affiliation ASC, super_corp ASC");

						statement.setLong(1, affiliateId);
						statement.setLong(2, affiliateId);
					}
					res = statement.executeQuery();

					while (res.next()){
						ids.add(res.getLong("user_id"));
						if (res.getInt("affiliation") == UserAffiliationEnum.USER_AFFILIATION.getId()) {
							usernames.add(res.getString("username")+(relationship >= 1 && relationship <= 3 ? "" : "[User]"));
						} else if (res.getInt("affiliation") == UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId()) {
							if (res.getBoolean("super_corp")){
								usernames.add(res.getString("username")+(relationship >= 1 && relationship <= 3 ? "" : "[Super]"));
							} else {
								usernames.add(res.getString("username")+(relationship >= 1 && relationship <= 3 ? "" : "[Sub]"));
							}
						}
					}
				} finally {
					DbUtil.close(res);
					DbUtil.close(statement);
				}
			} catch (SQLException e) {
				throw new RuntimeException("Error while getting corporate users list.", e);
			} finally {
				DbUtil.close(dbConn);
			}
		}
	}

	@Override
	public String getLabel(int arg0) {
		return usernames.get(arg0);
	}

	@Override
	public Object getOption(int arg0) {
		return ids.get(arg0);
	}

	@Override
	public int getOptionCount() {
		return ids.size();
	}

	@Override
	public String getValue(int arg0) {
		return String.valueOf(ids.get(arg0));
	}

	@Override
	public boolean isDisabled(int arg0) {
		return false;
	}

	@Override
	public Object translateValue(String arg0) {
		return Long.parseLong(arg0);
	}
}