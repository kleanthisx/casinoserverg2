/**
 * 
 */
package com.magneta.administration.partners;

import static com.magneta.casino.services.enums.UserTypeEnum.CORPORATE_ACCOUNT;

import java.util.HashMap;
import java.util.Map;

import com.magneta.administration.partners.security.User;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;

/**
 * @author User
 *
 */
public class PageAccessControl implements PageMenuItemAccessValidator {
	public static final int SUPER_CORPORATE_USER = -3;
	
	private static final Map<String, int[]> page_constraints;

	static{
		page_constraints = new HashMap<String, int[]>();

		//Access for all
		page_constraints.put("Home", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("ChangePassword", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("Profile", new int[]{CORPORATE_ACCOUNT.getId()});

		//Corporate accounts only access
		page_constraints.put("CorporateInfo", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("TransfersStatement", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("TransfersStatementRequest", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("UserTransfersStatement", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("UserTransfersStatementRequest", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("EarningsStatement", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("EarningsStatementRequest", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("MenuListing", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("Settings", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("SubAccounts", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("UserInformation", new int[]{CORPORATE_ACCOUNT.getId()});  
		page_constraints.put("reports/SubCorporateTurnoverReportOptions", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("reports/SuperCorporateTurnoverReportOptions", new int[]{CORPORATE_ACCOUNT.getId()});             
		page_constraints.put("reports/AccountStatementOptions", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("reports/SubCorporateGamingRevenue", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("reports/CorporateGamingRevenue", new int[]{CORPORATE_ACCOUNT.getId()});
		
		page_constraints.put("ConfirmUserStateChange", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("EditPartner", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("ConfirmUserAccountClose", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("ConfirmFundsTransfer", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("ConfirmFundsClear", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("FundsTransferResult", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("TransferFunds", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("EditUser", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("ResetUserPassword", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("CreateUser", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("CreateSubPartner", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("SubCorporateRequiresApproval", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("PayCorporate", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("reports/SubCorporateTransactionsReportOptions", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("reports/CorporateTransactionsOptions", new int[]{CORPORATE_ACCOUNT.getId()});
		page_constraints.put("reports/ProfitDocumentReportOptions", new int[]{CORPORATE_ACCOUNT.getId()});  
		page_constraints.put("reports/CorporateBalanceStatement",new int[]{CORPORATE_ACCOUNT.getId()});

		//Super-Corporate accounts only access
		page_constraints.put("SubPartnersApproval", new int[]{SUPER_CORPORATE_USER});
		page_constraints.put("ConfirmSubcorporateApproval", new int[]{SUPER_CORPORATE_USER});
	}

	public static boolean checkAccess(String page, User partner) {
		if (partner == null || !partner.isLoggedIn()){
			return false;
		}

		if (page == null) {
			return false;
		}

		/* Remove leading / */
		if (page.startsWith("/")) {
			page = page.substring(1);
		}

		int[] constraints = page_constraints.get(page);

		if (constraints == null){
			return false;
		}
		boolean allowAccess = false;
		for (int constraint: constraints) {

			switch(constraint) {
			case SUPER_CORPORATE_USER:
				if (partner.getType() == UserTypeEnum.CORPORATE_ACCOUNT && (partner.getSuperCorp() == null || partner.getSuperCorp() <= 0)) {
					allowAccess = true;
				}
				break;
			default:
				/* Check user type */
				if (partner.getUserType() == constraint) {
					allowAccess = true;
				}
			}
		}

		return allowAccess;
	}

	private final PrincipalService principalService;

	public PageAccessControl(PrincipalService principalService) {
		this.principalService = principalService;
	}

	@Override
	public boolean checkAccess(String page) {
		return checkAccess(page, (User)principalService.getPrincipal());
	}

	@Override
	public boolean checkSpecial(String special) {
		if (special == null)
			return true;
		
		User user = (User)principalService.getPrincipal();

		if (special.equals("logout")) {
			return user != null && user.isLoggedIn();
		}

		return true;
	}

	@Override
	public boolean checkWriteAccess(String pageName) {
		return true;
	}
}
