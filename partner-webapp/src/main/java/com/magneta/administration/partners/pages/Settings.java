package com.magneta.administration.partners.pages;

import java.util.List;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;

import com.magneta.administration.partners.services.PageMenuService;
import com.magneta.tapestry4.components.pagemenu.PageMenu;
import com.magneta.tapestry4.components.pagemenu.PageMenuItem;

public abstract class Settings extends SecurePage {

	@InjectObject("service:casino.partner.PageMenu")
	public abstract PageMenuService getPageMenuService();
	
	@Component(id ="menuListing", type="magneta:PageMenu",
			bindings={"items=MenuPageItems","cssClass=literal:vr_menu"})
	public abstract PageMenu getMenuListing();
	
	public List<PageMenuItem> getMenuPageItems() {
		 List<PageMenuItem> items = getPageMenuService().getVerticalMenuItems(this.getPage().getLocale());
		
		for (PageMenuItem item: items) {
			if (getPageName().equals(item.getTargetPage())){
				return item.getSubItems();
			}
		}
		return null;
	}
}
