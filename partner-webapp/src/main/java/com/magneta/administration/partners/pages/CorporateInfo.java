package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.UserBalanceBean;


public abstract class CorporateInfo extends SecurePage{
	
	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporateService();

	@InjectObject("service:casino.common.CorporateEarnings")
	public abstract CorporateEarningsService getCorporateEarningsService();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@Bean
	public abstract ValidationDelegate getDelegate();

	public abstract void setAffiliateRate(double affiliateRate);
	public abstract void setDeposit(double deposit);
	public abstract void setBalance(double balance);
	public abstract void setAllowFree(boolean allowFree);
	public abstract void setFreeRate(double freeRate);

	@Bean
	public abstract EvenOdd getEvenOdd();

	@Component(id="freeCredits",
			bindings={"value=(allowFree ? formatAmount(getFreeRate()*100)+' %' : getMsg('Disabled'))"})
			public abstract Insert getFreeCredits();

	@Component(id="subaffRate",
			bindings={"value=formatAmount(getAffiliateRate()*100)"})
			public abstract Insert getSubAffRate();

	@Component(id="earnings",
			bindings={"value=formatCurrency(balance)"})
			public abstract Insert getEarnings();

	@Component(id="balance",
			bindings={"value=formatCurrency(deposit)"})
			public abstract Insert getBalanceField();
	
	@Override
	public void prepareForRender(IRequestCycle cycle) {
		super.prepareForRender(cycle);
		
		CorporateBean corporate;
		
		try {
			corporate = this.getCorporateService().getCorporate(getUser().getId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		UserBalanceBean balance;
		try {
			balance = this.getUserDetailsService().getUserBalance(getUser().getId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		CorporateEarningsBean earnings;
		try {
			earnings = this.getCorporateEarningsService().getEarnings(getUser().getId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		setAffiliateRate(corporate.getAffiliateRate());
		setBalance(earnings.getEarnings());
		
		setDeposit(balance.getBalance());
		
		setAllowFree(getUser().isAllowFreeCredits());
		try {
			setFreeRate(getUser().getFreeCreditsRate());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
}



