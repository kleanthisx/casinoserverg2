package com.magneta.administration.partners.reports;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.services.LinkFactory;
import org.apache.tapestry.services.ServiceConstants;
import org.apache.tapestry.util.ContentType;
import org.apache.tapestry.web.WebContext;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;
import org.apache.tapestry.web.WebSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.administration.commons.services.reporting.ServiceUtils;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.enums.UserTypeEnum;

public class GeneratedReportDownloadService implements IEngineService {

	private static final Logger log = LoggerFactory.getLogger(GeneratedReportDownloadService.class);
	
	public static final String SERVICE_NAME = "generated_report_download";   

	private WebContext context;
	private LinkFactory linkFactory;
	private PrincipalService principalService;

	public void setContext(WebContext context) {
		this.context = context;
	}
	
	public void setLinkFactory(LinkFactory factory) {
		this.linkFactory = factory;
	}

	public void setPrincipalService(PrincipalService principalService) {
		this.principalService = principalService;
	}

	@Override
	public ILink getLink(boolean post, Object parameter) {

		Object [] params = (Object[])parameter; 

		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put(ServiceConstants.SERVICE, getName());

		for (Object param: params) {
			ServiceParameter p = (ServiceParameter)param;
			String stringVal = ServiceUtils.encodeToString(p.getParameterValue());
			if (stringVal != null && stringVal.length() > 0) {
				parameters.put(p.getParameterName(), stringVal);
			}
		}

		return linkFactory.constructLink(this, post, parameters, true);
	}

	@Override
	public String getName() {
		return SERVICE_NAME;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void service(IRequestCycle cycle) throws IOException {

		WebRequest request = cycle.getInfrastructure().getRequest();
		WebResponse response = cycle.getInfrastructure().getResponse();
		WebSession session = request.getSession(false);
		
		if (session == null) {
			cycle.getInfrastructure().getResponse().sendError(503, "Session Expired");
			return;
		}

		ExtendedPrincipalBean principal = principalService.getPrincipal();
		
		if (principal == null) {
			cycle.getInfrastructure().getResponse().sendError(HttpServletResponse.SC_FORBIDDEN, "You need to log in to access the reports.");
			return;
		}

		if (principal.getType() != UserTypeEnum.CORPORATE_ACCOUNT) {
			cycle.getInfrastructure().getResponse().sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to view reports.");
			return;
		}

		Map<String,Object> requestParams = new HashMap<String,Object>();
		List<String> paramNames = request.getParameterNames();
		for (String param: paramNames) {
			requestParams.put(param, request.getParameterValue(param));
			log.debug("Param: {} has value: {}", param, request.getParameterValue(param));
		}
		
		String uID = (String)requestParams.get("report_uid");
		if (uID == null){
			uID = cycle.getParameter("report_uid");
		}
		
		String format = (String)requestParams.get("format");
		if (format == null){
			format = cycle.getParameter("format");
		}
		
		if (uID == null || format == null) {
			cycle.getInfrastructure().getResponse().sendError(HttpServletResponse.SC_BAD_REQUEST, "Error while getting report: missing parameters.");
			return;
		}
		
		format = format.toLowerCase();
		
		ContentType contentType; 
		
		if (format.equals("pdf")){
			contentType = new ContentType("application/pdf");
		} else if (format.equals("html")){
			contentType = new ContentType("text/html");
		} else if (format.equals("xls")){
			contentType = new ContentType("application/vnd.ms-excel");
		} else if (format.equals("rtf")){
			contentType = new ContentType("application/rtf");
		} else if (format.equals("odt")) {
			contentType= new ContentType("application/vnd.oasis.opendocument.text"); 
		} else {
			cycle.getInfrastructure().getResponse().sendError(HttpServletResponse.SC_BAD_REQUEST, "Error while getting report: invalid format.");
			return;
		}

		response.setHeader("Content-Disposition","inline; filename="+uID+"."+format);
		response.setHeader("Content-Type",contentType.getMimeType()+";charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");

		InputStream in = null;
		OutputStream out = response.getOutputStream(contentType);

		try {
			in = new FileInputStream(context.getRealPath("/reports/gen/")+"/"+uID+"."+format);
			byte[] buffer = new byte[1024];

			while (true){
				int amountRead = in.read(buffer);
				if (amountRead == -1) {
					break;
				}
				out.write(buffer, 0, amountRead);
			}
		} finally {
			if (out != null){
				out.flush();
				out.close();
			}
			if (in != null){
				in.close();
			}
		}

	}
}
