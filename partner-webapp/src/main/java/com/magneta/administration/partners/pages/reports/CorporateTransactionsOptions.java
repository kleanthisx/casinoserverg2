package com.magneta.administration.partners.pages.reports;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.casino.services.ServiceException;

public abstract class CorporateTransactionsOptions extends ReportOptionsPage {

	@Override
	public abstract IFieldTracking getCurrentFieldTracking();

	public static final String REPORT_NAME = "corporate_transactions_statement";
	private static final ReportBean rep = new ReportBean("corporate_transactions_statement", "Corporate Transactions Statement" , "Corporate Account Movement Statement", "reports/SubCorporateTransactionsReportOptions ", " ", false);

	@Component(id = "summariseUsersCB",
			bindings={"value=summariseUsers", "displayName=message:SummariseUsers"})
			public abstract Checkbox getShowSubsCB();

	@Component(id = "summariseUsersCBLabel",
			bindings={"field=component:summariseUsersCB"})
			public abstract FieldLabel getShowSubsCBLabel();

	@Override
	public ILink onSubmit(IRequestCycle cycle) throws ServiceException{
		Date maxDate = normalizeDate(getMaxDate());
		Date minDate = normalizeDate(getMinDate());

		int countDays=(int)((maxDate.getTime()- minDate.getTime())/ (1000*60*60*24));

		if (getDelegate().getHasErrors()){
			return null;
		}

		if(countDays>31){
			getDelegate().record(getMsg("month-error"), null);
			return null;
		}	

		if(getMinDate().after(getMaxDate())){
			getDelegate().record(getMsg("date-error"),null);
			return null;
		}

		return super.onSubmit(cycle);
	} 


	public Date getInitialMinDate() {
		Calendar fromCal = Calendar.getInstance();
		fromCal.setTime(getMaxDate());
		fromCal.roll(Calendar.MONTH, -1);
		return fromCal.getTime();	
	}

	@InitialValue("initialMinDate")
	public abstract Date getMinDate();
	public abstract void setMinDate(Date date);
	public abstract void setMaxDate(Date date);
	@InitialValue("new java.util.Date()")
	public abstract Date getMaxDate();
	@InitialValue("true")
	public abstract boolean getSummariseUsers();
	public abstract Long getUserId();

	@Override
	public void fillReportParameters(List<ServiceParameter> reportParams) {
		super.fillReportParameters(reportParams);

		TimeZone tz = getUser().getTimeZone();

		Date minDate = removeTimeMin(normalizeDate(getMinDate()));
		Date maxDate = removeTimeMax(normalizeDate(getMaxDate()));

		//NORMALIZE DATES
		if (minDate != null){       	
			/* Convert min date to UTC day start.
			 * This is important since the database daily records are using UTC */
			long utcMinDate = minDate.getTime() + tz.getOffset(minDate.getTime());

			reportParams.add(new ServiceParameter("min_date", new Timestamp(utcMinDate)));
		}

		if (maxDate != null){       	
			/* Convert max date to UTC day start.
			 * This is important since the database daily records are using UTC */
			long utcMaxDate = maxDate.getTime() + tz.getOffset(maxDate.getTime());

			reportParams.add(new ServiceParameter("max_date", new Timestamp(utcMaxDate)));
		}

		reportParams.add(new ServiceParameter("summarise_users",getSummariseUsers()));
		reportParams.add(new ServiceParameter("subcorp_id",getUserId() != null? getUserId() : getUser().getId()));
		reportParams.add(new ServiceParameter("report_title",getMsg("report-title")));

		if(getUserId() == null) {
			reportParams.add(new ServiceParameter("username", getUser().getUsername()));
		} else {
			reportParams.add(new ServiceParameter("username", getUsername(getUserId())));
		}
	}

	@Override
	public final ReportBean getReport() {
		return rep;
	}

	@Override
	public String getReportName(){
		return REPORT_NAME;
	}

	@Override
	public void onCancel(IRequestCycle cycle){
		redirectHome();
	}

	@Override
	public String toString(){
		return this.getPageName();
	}

}
