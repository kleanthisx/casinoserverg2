package com.magneta.administration.partners.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.administration.partners.beans.EarningTransactionBean;
import com.magneta.administration.partners.models.AffiliateEarningsTableModel;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class EarningsStatement extends SecurePage {

	@Persist("client:page")
	public abstract void setMinDate(Date minDate);
	public abstract Date getMinDate();
	
	@Persist("client:page")
	public abstract void setMaxDate(Date maxDate);	
	public abstract Date getMaxDate();
	
	public abstract void setTotal(double total);
	
	@Persist("client:page")
	public abstract Long getSelectedUser();
	public abstract void setSelectedUser(Long selectedUser);

	@InjectObject("service:tapestry.globals.HttpServletRequest")
	public abstract HttpServletRequest getRequest();
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract EarningTransactionBean getCurrTransaction();
	
	@Bean
    public abstract ValidationDelegate getDelegate();
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	public boolean isUserIdNull = false;
	
	@Component(id="earnings", type="contrib:Table",
			bindings={"source=ognl:getEarningsTableModel(selectedUser, minDate, maxDate)",
			"columns=literal:!Date, !Username, !Initiator, !Details, !Amount","pageSize=30","rowsClass=ognl:beans.evenOdd.next","initialSortOrder=false",
			"row=currTransaction","persist=literal:client:page"})
	public abstract Table getEarnings();
	
	public AffiliateEarningsTableModel getEarningsTableModel(Long selectedUser, Date minDate, Date maxDate) {
		logReportGeneration("Earnings Statement (Page)", minDate, maxDate, getRequest().getRemoteAddr(), null);

		if (selectedUser == -2) {
			return new AffiliateEarningsTableModel(getUser().getId(), true, minDate, maxDate);
		} else if (selectedUser == -3) {
			selectedUser = getUser().getId();
		}
	
		return new AffiliateEarningsTableModel(selectedUser, false, minDate, maxDate);
	}
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Component(id="date",
			bindings={"value=formatDate(currTransaction.Date)"})
	public abstract Insert getDate();
	
	@Component(
			bindings={"value=(currTransaction.amount == 0.00 ? '-' : formatAmount(currTransaction.amount))"})
	public abstract Insert getAmount();
	
	@Component(id="formattedFromDate",
			bindings={"value=(minDate != null ? getMsg('From') + ' '+formatDateDay(minDate) : '')"})
	public abstract Insert getFormattedFromDate();
	
	@Component(id="formattedToDate",
			bindings={"value=(maxDate != null ? getMsg('Until') +' '+ formatDateDay(maxDate) : '')"})
	public abstract Insert getFormattedToDate();
	
	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		ValidationDelegate delegate = getDelegate();
		 Messages messages = getMessages();
		 isUserIdNull = false; 
		 
		 
		if(getSelectedUser() == null){
			 isUserIdNull = true;
			 delegate.record(messages.getMessage("invalid-request"), null);
			 return;
		}
		
	}
	
	public double getTotalAmount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        double total = 0.0;     
        
        if (dbConn == null){
            return 0;
        }
        
        try {            
            String sql = 
            	" SELECT SUM(amount)"+
            	" FROM affiliate_earnings_transactions"+
            	" WHERE (affiliate_id = ?"+
            	(getSelectedUser() == -2 ?" OR affiliate_id IN (SELECT affiliate_users.user_id FROM affiliate_users WHERE affiliate_users.affiliate_id=?)":"")+
				" )" +
            	(getMaxDate() != null? " AND ((transaction_date AT TIME ZONE 'UTC') <= ?::timestamp)" : "")+
            	(getMinDate() != null? " AND ((transaction_date AT TIME ZONE 'UTC') >= ?::timestamp)" : "");
            
            statement = dbConn.prepareStatement(sql);
            int i = 1;
            
            if(getSelectedUser() == -2 || getSelectedUser() == -3){
            	statement.setLong(i++, getUser().getId());
            }else{
            	statement.setLong(i++, getSelectedUser());
            }
            
            if (getSelectedUser() == -2){
            	statement.setLong(i++, getUser().getId());
            }
            if (getMaxDate() != null){
            	statement.setTimestamp(i++, new Timestamp(getMaxDate().getTime()));
            }
            if (getMinDate() != null){
            	statement.setTimestamp(i++, new Timestamp(getMinDate().getTime()));
            }
            res = statement.executeQuery();
            
            while (res.next()){
                total += res.getDouble(1);
            }
            setTotal(Math.abs(total));
          
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving earnings statement totals.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return total;
	}
	
	public String formatDateDay(Date d){
    	String str = formatDate(d);
    	return str.substring(0, str.lastIndexOf(" "));    	
    }
	
	public String getAmountColumnHeader(String column) {
		String symbol = getCurrencyParser().getCurrencySymbol(getLocale());
		
		return getMsg(column) + "  (" + symbol + ")"; 
	}
}