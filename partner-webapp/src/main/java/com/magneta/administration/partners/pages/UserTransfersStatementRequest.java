/**
 * 
 */
package com.magneta.administration.partners.pages;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.dojo.form.DropdownTimePicker;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

public abstract class UserTransfersStatementRequest extends SecurePage {
    
	public abstract IFieldTracking getCurrentFieldTracking();
	
    public abstract Long getSelectedUser();
    
    @InitialValue("new java.util.Date()")
    public abstract Date getMinDate();
    @InitialValue("new java.util.Date()")
    public abstract Date getMaxDate();
    public abstract Date getMinTime();
    public abstract Date getMaxTime();
	
	@Bean
	public abstract ValidationDelegate getDelegate();
	
    @InjectPage("UserTransfersStatement")
    public abstract UserTransfersStatement getStatementPage();
    
    @Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrorsDelegator();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
    
    @Component(id="requestForm",
    		bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
			"clientValidationEnabled=true"})
    public abstract Form getRequestForm();
    
    @Component(id="userLabel",
    		bindings={"field=component:user"})
    public abstract FieldLabel getUserLabel();
    
    @Component(id="user",
    		bindings={"value=selectedUser","displayName=message:User","model=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsSelectionModel(user.Id,@com.magneta.administration.partners.models.CorporateRelationshipsSelectionModel@USER_RELATIONSHIP,true,false)"})
    public abstract PropertySelection getUserField();
    
    @Component(id="fromDate",
    		bindings={"value=minDate","displayName=message:From","translator=translator:date,pattern=dd/MM/yyyy",
    		"validators=validators:required"})
    public abstract DatePicker getFromDate();
    
    @Component(id="fromDateLabel",
    		bindings={"field=component:fromDate"})
    public abstract FieldLabel getFromDateLabel();
   
    @Component( id="fromTime",
    		bindings={"value=minTime","disabled=false","displayName=message:Time","translator=translator:date,pattern=HH:mm"})
    public abstract DropdownTimePicker getFromTime();
    
    @Component(id="toDate",
    		bindings={"value=maxDate","displayName=message:To","translator=translator:date,pattern=dd/MM/yyyy",
    		"validators=validators:required"})
    public abstract DatePicker getToDate();
    
    @Component(id="toDateLabel",
    		bindings={"field=component:toDate"})
    public abstract FieldLabel getToDateLabel();
    
    @Component(id="toTime",
    		bindings={"value=maxTime","disabled=false","displayName=message:Time","translator=translator:date,pattern=HH:mm"})
    public abstract DropdownTimePicker getToTime();
    
    @Component(id="okSubmit")
    public abstract Submit getOkSubmit();
    
    @Component(id="cancelSubmit")
    public abstract Submit getCancelSubmit();
    
    public void onSubmit(IRequestCycle cycle){
        Date minDate = normalizeDate(getMinDate());
        Date maxDate = normalizeDate(getMaxDate());
        
        int countDays=(int)((maxDate.getTime()- minDate.getTime())/ (1000*60*60*24));
		
		if (getDelegate().getHasErrors()){
			return;
		}
		
		if(countDays>31){
			getDelegate().record(getMsg("month-error"), null);
			return;
		}	
        
        if(getMinDate().after(getMaxDate())){
        	getDelegate().record(getMsg("date-error"),null);
        	return;
        }
        
        //NORMALIZE DATES
        {
            int offset = TimeZone.getDefault().getOffset(minDate.getTime());
            
            Calendar calendar = Calendar.getInstance(getUser().getTimeZone(), getLocale());
            
            int hour = 0;
            int minute = 0;
            
            Date minTime = normalizeDate(getMinTime());
            
            if (minTime != null){
                calendar.setTimeInMillis(minTime.getTime());
            
                hour = calendar.get(Calendar.HOUR_OF_DAY);
                minute = calendar.get(Calendar.MINUTE);
            }
            
            calendar.setTimeInMillis(minDate.getTime() + offset);
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            minDate = new Date(calendar.getTimeInMillis());
        }
    
        
        {
            int offset = TimeZone.getDefault().getOffset(maxDate.getTime());
            
            Calendar calendar = Calendar.getInstance(getUser().getTimeZone(), getLocale());
            
            
            
            Date maxTime = normalizeDate(getMaxTime());
            
            if (maxTime != null){
                calendar.setTimeInMillis(maxTime.getTime());
                
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                
                calendar.setTimeInMillis(maxDate.getTime()+offset);
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
            } else {
                calendar.setTimeInMillis(maxDate.getTime()+offset);
                calendar.set(Calendar.HOUR_OF_DAY, 24);
                calendar.set(Calendar.MINUTE, 0);
                calendar.add(Calendar.MILLISECOND, -1);
            }
            
            maxDate = new Date(calendar.getTimeInMillis());
        }
        
        UserTransfersStatement stmt = getStatementPage();
        stmt.setUserId(getSelectedUser());
        stmt.setMinDate(minDate);
        stmt.setMaxDate(maxDate);
        cycle.activate(stmt);
    }  
    
    public void onCancel(IRequestCycle cycle){
        redirectHome();
    }
}
