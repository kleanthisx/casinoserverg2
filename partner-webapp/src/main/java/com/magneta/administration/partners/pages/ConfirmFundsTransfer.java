package com.magneta.administration.partners.pages;

import java.util.Date;

import org.apache.hivemind.util.PropertyUtils;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.partners.beans.TransferBean;
import com.magneta.casino.services.CorporateTransfersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.CorporateTransferTransactionBean;
import com.magneta.casino.services.exceptions.InsufficientFundsException;
import com.magneta.tapestry4.callback.RedirectExternalCallback;

public abstract class ConfirmFundsTransfer extends SecurePage{

	private static final Logger log = LoggerFactory.getLogger(ConfirmFundsTransfer.class);

	@InjectObject("service:casino.common.Settings")
	public abstract SettingsService getSettingsService();

	@InjectObject("service:casino.common.CorporateTransfers")
	public abstract CorporateTransfersService getTransfersService();
	
	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();

	@Bean
	public abstract ValidationDelegate getDelegate();

	public abstract IFieldTracking getCurrentFieldTracking();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();

	@Component(id="confirmForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","clientValidationEnabled=true"})
	public abstract Form getConfirmForm();

	@Component(id="userId",
			bindings={"value=userId"})
	public abstract Hidden getUserIdField();

	@Component(id="amount",
			bindings={"value=amount"})
	public abstract Hidden getAmountField();

	@Component(id="description",
			bindings={"value=description"})
	public abstract Hidden getDescriptionField();

	@Component(id="amountVal",
			bindings={"value=formatCurrency(amount)"})
	public abstract Insert getAmountVal();

	@Component(id="usernameVal",
			bindings={"value=username"})
	public abstract Insert getUsernameVal();

	@Component(id="descriptionVal",
			bindings={"value=description"})
	public abstract Insert getDescriptionVal();

	@Component(id="password",
			bindings={"value=password","hidden=true","displayName=message:Password","validators=validators:required"})
	public abstract TextField getPasswordField();

	@Component(id="passwordLabel",
			bindings={"field=component:password"})
	public abstract FieldLabel getPasswordLabel();

	@Component(id="confirmSubmit")
	public abstract Submit getConfirmSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		ValidationDelegate delegate = getDelegate();	

		if(getUserId() == null){
			delegate.record(getMessages().getMessage("invalid-request"), null);
			return;
		}

		String username = getUsername(getUserId());
		if(username !=null) {
			PropertyUtils.write(this, "username", username);
		}
	}

	public abstract String getUsername();
	public abstract Long getUserId();
	public abstract void setUserId(Long userId);
	public abstract double getAmount();
	public abstract void setAmount(double amount);
	public abstract String getPassword();
	public abstract String getDescription();
	public abstract void setDescription(String description);

	public void onSubmit(IRequestCycle cycle) throws ServiceException {
		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors()){
			return;
		} else if (!getUserRegistrationService().verifyPassword(getUser().getUserId(), getPassword())) {
			delegate.record(getPasswordField(), getMsg("invalid-password"));
			return;
		}

		if(getUserId() == null){
			delegate.record(getMessages().getMessage("invalid-request"), null);
			return;
		}

		CorporateTransferTransactionBean transaction = new CorporateTransferTransactionBean();

		transaction.setUserId(getUserId());
		transaction.setAmount(getAmount());
		transaction.setDescription(getDescription());
		
		try {
			getTransfersService().createTransfer(transaction);
		} catch (InsufficientFundsException e) {
			delegate.record(getAmountField(), getMsg("insufficient-funds"));
			return;
		} catch (ServiceException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Error while transfering amount to user/corporate",e);
			return;
		}
		
		TransferBean bean = new TransferBean();
		bean.setType("Transfer");
		bean.setUsername(getUsername(getUserId()));
		bean.setAmount(getAmount());
		bean.setTransferDate(new Date());
		getUser().addTransfer(bean);
		
		new RedirectExternalCallback("FundsTransferResult",
				new Object[] {transaction.getTransferId()}).performCallback(cycle);
	}

	public void onCancel(IRequestCycle cycle){
		cycle.forgetPage(this.getPageName());
		redirectToPage("TransferFunds");
	}
}