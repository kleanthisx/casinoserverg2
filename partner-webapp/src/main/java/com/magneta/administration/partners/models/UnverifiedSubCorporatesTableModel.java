package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.partners.beans.CorporateUserBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UnverifiedSubCorporatesTableModel implements IBasicTableModel {

	private Long affiliateId;

	public UnverifiedSubCorporatesTableModel(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	@Override
	public Iterator<CorporateUserBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		
		List<Long> firstLevelSubIds = new ArrayList<Long> ();
		List<String> firstLevelSubUsernames = new ArrayList<String> ();

		Iterator<CorporateUserBean> it = null;

		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;


		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}

		try{
			try
			{   
				String sql = 
					" SELECT affiliate_users.user_id AS sub_id, users.username"+ 
					" FROM affiliate_users"+ 
					" INNER JOIN affiliates ON affiliates.affiliate_id = affiliate_users.user_id" +
					" INNER JOIN users ON users.user_id = affiliates.affiliate_id"+ 
					" WHERE affiliate_users.affiliate_id = ?";


				statement = dbConn.prepareStatement(sql);
				statement.setLong(1, affiliateId);

				res = statement.executeQuery();
				
				while (res.next()){
					firstLevelSubIds.add(res.getLong("sub_id"));
					firstLevelSubUsernames.add(res.getString("username"));
				}
			} catch (SQLException e) {
				throw new RuntimeException("Error while retrieving first level subcorporates.",e);
			} finally{
				DbUtil.close(res);
				DbUtil.close(statement);
			}

			statement = null;
			res = null;
			
			List<CorporateUserBean> rows = new ArrayList<CorporateUserBean>();

			while(!firstLevelSubIds.isEmpty()) {
				statement = null;
				res = null;
				try
				{   
					String sql = 
						" SELECT affiliate_users.user_id AS sub_id, users.username, requires_super_approval, users.register_date," +
						" require_approval, approved"+ 
						" FROM affiliate_users"+ 
						" INNER JOIN affiliates ON affiliates.affiliate_id = affiliate_users.user_id" +
						" INNER JOIN users ON users.user_id = affiliates.affiliate_id" +
						" WHERE affiliate_users.affiliate_id = ?";


					statement = dbConn.prepareStatement(sql);
					statement.setLong(1, firstLevelSubIds.get(0));

					res = statement.executeQuery();

					CorporateUserBean currRow;
					while (res.next()){						
						if(!res.getBoolean("approved")) {
							currRow = new CorporateUserBean();
							currRow.setUsername(res.getString("username"));
							currRow.setSuperUsername(firstLevelSubUsernames.get(0));
							currRow.setUserId(res.getLong("sub_id"));
							currRow.setRegisterDate(DbUtil.getTimestamp(res,"register_date"));
							currRow.setSuperApprovalRequired(res.getBoolean("requires_super_approval"));
							rows.add(currRow);
						}
						firstLevelSubIds.add(res.getLong("sub_id"));
						firstLevelSubUsernames.add(res.getString("username"));
					}

				} catch (SQLException e) {
					throw new RuntimeException("Error while retrieving sub corporates that need approval.",e);
				} finally {
					DbUtil.close(res);
					DbUtil.close(statement);
				}
				firstLevelSubIds.remove(0);
				firstLevelSubUsernames.remove(0);
			}
			it = rows.iterator();

		} finally{
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		int count = 0;

		List<Long> firstLevelSubIds = new ArrayList<Long> ();

		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;

		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
		try{
			try
			{   
				String sql = 
					" SELECT affiliate_users.user_id AS sub_id, users.username"+ 
					" FROM affiliate_users"+ 
					" INNER JOIN affiliates ON affiliates.affiliate_id = affiliate_users.user_id" +
					" INNER JOIN users ON users.user_id = affiliates.affiliate_id"+ 
					" WHERE affiliate_users.affiliate_id = ?";

				statement = dbConn.prepareStatement(sql);

				statement.setLong(1, affiliateId);

				res = statement.executeQuery();

				while (res.next()){
					firstLevelSubIds.add(res.getLong("sub_id"));
				}
			} catch (SQLException e) {
				throw new RuntimeException("Error while retrieving first level subcorporates.",e);
			} finally{
				DbUtil.close(res);
				DbUtil.close(statement);
			}

			while(firstLevelSubIds.size() > 0) {
				statement = null;
				res = null;
				try
				{   
					String sql = 
						" SELECT affiliate_users.user_id AS sub_id, requires_super_approval, approved"+ 
						" FROM affiliate_users"+ 
						" INNER JOIN affiliates ON affiliates.affiliate_id = affiliate_users.user_id" +
						" INNER JOIN users ON users.user_id = affiliates.affiliate_id"+ 
						" WHERE affiliate_users.affiliate_id = ?";


					statement = dbConn.prepareStatement(sql);
					statement.setLong(1, firstLevelSubIds.get(0));

					res = statement.executeQuery();

					while(res.next()) {
						if(!res.getBoolean("approved")) {
							count = count + 1;
						}
						firstLevelSubIds.add(res.getLong("sub_id"));
					}

				} catch (SQLException e) {
					throw new RuntimeException("Error while retrieving row count.",e);
				} finally {
					DbUtil.close(res);
					DbUtil.close(statement);
				}
				firstLevelSubIds.remove(0);
			}

		} finally{
			DbUtil.close(dbConn);
		}

		return count;
	}
}

