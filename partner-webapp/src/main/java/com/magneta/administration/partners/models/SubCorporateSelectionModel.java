package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class SubCorporateSelectionModel implements IPropertySelectionModel {

	private List<Long> ids;
	private List<String> usernames;

	public SubCorporateSelectionModel(Long affiliateId, boolean showAllOption, boolean showNoneOption, boolean showMe, boolean activeOnly, boolean hideClosed) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		ids = new ArrayList<Long>();
		usernames = new ArrayList<String>(); 

		if(showNoneOption) {
			ids.add(new Long(-1));
			usernames.add("--Please select--");
		}
		
		if (showAllOption) {
			ids.add(new Long(-2));
			usernames.add("All");
		}
		
		if (showMe) {
			ids.add(new Long(-3));
			usernames.add("Me");
		}

		if (dbConn != null){
			try {
				PreparedStatement statement = null;
				ResultSet res = null;

				try {
						statement = dbConn.prepareStatement(
								"SELECT affiliate_users.affiliation, users.user_id, users.username, users.first_name, users.last_name, FALSE As super_corp"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.user_id = users.user_id" + (activeOnly? " AND is_active = TRUE" : "") + (hideClosed? " AND users.closed = FALSE" : "") +
								" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id AND affiliates.approved = TRUE" +
								" WHERE affiliation = 3"+
						" AND affiliate_users.affiliate_id = ?");
					
						statement.setLong(1, affiliateId);
					
					res = statement.executeQuery();

					while (res.next()){
						ids.add(res.getLong("user_id"));
						usernames.add(res.getString("username"));
					}
				} finally {
					DbUtil.close(res);
					DbUtil.close(statement);
				}
			} catch (SQLException e) {
				throw new RuntimeException("Error while getting corporate users list.",e);
			} finally {
				DbUtil.close(dbConn);
			}
		}
	}

	@Override
	public String getLabel(int arg0) {
		return usernames.get(arg0);
	}

	@Override
	public Object getOption(int arg0) {
		return ids.get(arg0);
	}

	@Override
	public int getOptionCount() {
		return ids.size();
	}

	@Override
	public String getValue(int arg0) {
		return String.valueOf(ids.get(arg0));
	}

	@Override
	public boolean isDisabled(int arg0) {
		return false;
	}

	@Override
	public Object translateValue(String arg0) {
		return Long.parseLong(arg0);
	}
}