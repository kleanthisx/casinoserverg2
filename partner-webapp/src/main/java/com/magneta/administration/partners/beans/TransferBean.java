package com.magneta.administration.partners.beans;

import java.util.Date;

public class TransferBean {

	private String username;
	private double amount;
	private Date transferDate;
	private double promoAmount;
	private String type;
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAmount() {
		return amount;
	}
	
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setPromoAmount(double promoAmount) {
		this.promoAmount = promoAmount;
	}

	public double getPromoAmount() {
		return promoAmount;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
}
