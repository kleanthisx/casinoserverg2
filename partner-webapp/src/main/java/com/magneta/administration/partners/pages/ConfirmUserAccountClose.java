package com.magneta.administration.partners.pages;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.exceptions.AccessDeniedException;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.FinancialInconsistencyException;

public abstract class ConfirmUserAccountClose extends SecurePage {
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@Bean
    public abstract ValidationDelegate getDelegate();
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	public boolean isUserIdNull = false;
	
	public abstract void setResult(String result);
	
	public abstract Long getUserID();
	public abstract void setUserID(Long userID);
	
	public abstract String getUsername();
	public abstract void setUsername(String username);
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Component(id="result",
			bindings={"value=result"})
	public abstract Insert getResultField();
	
	@Component(id="confirmMsg",
			bindings={"value=getMsg('Confirm')+' '+username+'?'"})
	public abstract Insert getConfirmMsg();
	
	@Component(id="confirmForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel"})
	public abstract Form getConfirmForm();
	
	@Component(id="userID",
			bindings={"value=userID"})
	public abstract Hidden getUserId();
	
	@Component(id="confirmSubmit")
	public abstract Submit getConfirmSubmit();
	
	@Component(id="cancelRemoval")
	public abstract Button getCancelRemoval();
    
	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		ValidationDelegate delegate = getDelegate();
		 Messages messages = getMessages();
		 isUserIdNull = false; 
		
		 if(getUserID() == null){
			 isUserIdNull = true;
			 delegate.record(messages.getMessage("invalid-request"), null);
			 return;
		 }
		 
		setUsername(getUsername(getUserID()));
	}
	
    public void onSubmit(IRequestCycle cycle) {
    	
    	try {
    		this.getUserDetailsService().closeAccount(getUserID());
    		redirectToPage("SubAccounts");
    	} catch (FinancialInconsistencyException e) {
    		setResult(getMsg("settle-account-error"));
    	} catch (DatabaseException e) {
    		setResult(getMsg("db-error"));
    	} catch (AccessDeniedException e) {
    		setResult("Illegal operation.");
    	} catch (ServiceException e) {
    		setResult("Error");
    	}
    }
    
    public void onCancel(IRequestCycle cycle) {
        redirectToPage("SubAccounts");
    }
}
