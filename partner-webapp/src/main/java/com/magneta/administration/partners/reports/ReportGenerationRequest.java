package com.magneta.administration.partners.reports;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;

public class ReportGenerationRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private ExtendedPrincipalBean principal;
	private String report;
	private String format;
	private List<ServiceParameter> params;
	private String uID;
	private Date time;
	private String reportDesc;
	private Locale locale;

	public ReportGenerationRequest(ExtendedPrincipalBean principal, String report,
			String format, List<ServiceParameter> params, String reportDesc) {
		super();
		this.principal = principal;
		this.report = report;
		this.format = format;
		this.params = params;
		this.time = new Date(); 
		this.reportDesc = reportDesc;
	}

	public Date getTime() {
		return time;
	}

	public String getUID() {
		return uID;
	}
	
	public ExtendedPrincipalBean getPrincipal() {
		return principal;
	}

	public void setPrincipal(ExtendedPrincipalBean principal) {
		this.principal = principal;
	}

	public List<ServiceParameter> getParams() {
		return params;
	}

	public void setParams(List<ServiceParameter> params) {
		this.params = params;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
	public void setUID(String uID) {
		this.uID = uID;
	}

	public String getReportDesc() {
		return reportDesc;
	}

	public void setReportDesc(String reportDesc) {
		this.reportDesc = reportDesc;
	}
	
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	@Override
	public boolean equals(Object o){
		if (o instanceof ReportGenerationRequest){
			ReportGenerationRequest req = (ReportGenerationRequest)o;
			if (this.uID != null && this.uID.equals(req.uID)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		if (this.uID == null)
			return 0;
		
		return this.uID.hashCode();
	}
}