package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.link.ExternalLink;

import com.magneta.casino.services.CorporateTransfersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CorporateTransferTransactionBean;
import com.magneta.casino.services.exceptions.EntityNotFoundException;

public abstract class FundsTransferResult extends SecurePage {

	@InjectObject("service:casino.common.CorporateTransfers")
	public abstract CorporateTransfersService getCorporateTransfersService();
	
	@Component(id="amountVal",
			bindings={"value=formatCurrency(transfer.amount)"})
	public abstract Insert getAmountVal();
	
	@Component(id="usernameVal",
			bindings={"value=getUsername(transfer.userId)"})
	public abstract Insert getUsernameVal();

	@Component(id="transfersLink",
			bindings={"page=literal:TransferFunds"})
	public abstract ExternalLink getTransfersLink();

	public abstract Long getTransferId();
	public abstract void setTransferId(Long transferId);
	
	public abstract CorporateTransferTransactionBean getTransfer();
	public abstract void setTransfer(CorporateTransferTransactionBean transfer);
	
    @Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
    	if (params != null && params.length > 0){
    		this.setTransferId((Long)params[0]);
    	}
    	
    	if (this.getTransferId() == null) {
    		redirectToPage("AccessDenied");
    	}
	}
	
	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		
		try {
			setTransfer(getCorporateTransfersService().getTransfer(getTransferId()));
		} catch (EntityNotFoundException e) {
			redirectToPage("AccessDenied");
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
    }
}