package com.magneta.administration.partners.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.StringPropertySelectionModel;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.partners.models.AffiliatePaymentPeriods;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.callback.RedirectExternalCallback;
import com.magneta.tapestry4.components.ExFieldLabel;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

public abstract class EditPartner extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditPartner.class);

	public static final AffiliatePaymentPeriods AFFILIATE_PAYMENT_PERIODS = new AffiliatePaymentPeriods();
	public static final StringPropertySelectionModel AFFILIATE_PAYMENT_TYPES = new StringPropertySelectionModel(new String[]{"Cheque", "Credit Card"});

	@InjectObject("service:casino.common.UserAccess")
	public abstract UserAccessService getUserAccessService();

	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporateService();

	@Bean
	public abstract ValidationDelegate getDelegate();

	@Persist("client:page")
	public abstract Long getUserID();
	public abstract void setUserID(Long userID);

	public abstract String getUsername();
	public abstract void setUsername(String username);

	public abstract double getAffiliateRate();
	public abstract void setAffiliateRate(double affiliateRate);

	public abstract String getPaymentPeriod();
	public abstract void setPaymentPeriod(String paymentPeriod);

	@InitialValue("user.allowFreeCredits")
	public abstract boolean getAllowFreeCredits();
	public abstract void setAllowFreeCredits(boolean allowFreeCredits);

	public abstract boolean getAllowFullFreeCredits();
	public abstract void setAllowFullFreeCredits(boolean allowFreeCredits);

	public abstract double getAllowedMaxRate();

	public abstract IFieldTracking getCurrentFieldTracking();


	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();


	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();


	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();

	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumZeroTranslator();

	@Bean
	public abstract Min getMinAmount();

	@Bean
	public abstract Max getMaxAmount();

	@Component(id="registerForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
	"clientValidationEnabled=true"})
	public abstract Form getRegisterForm();

	@Component(id="username",
			bindings={"value=username"})
	public abstract Insert getUsernameField();

	@Component(id="registerSubmit")
	public abstract Submit getRegisterSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Component(id="affiliateRateLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:affiliateRate"})
	public abstract ExFieldLabel getAffiliateRateLabel();

	@Component(id="affiliateRate",
			bindings={"value=affiliateRate","displayName=message:AffiliateRate","translator=bean:numZeroTranslator",
	"validators=validators:required,$minAmount,$maxAmount"})
	public abstract TextField getAffiliateRateField();

	@Component(id="paymentPeriodLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:paymentPeriod"})
	public abstract ExFieldLabel getPaymentPeriodLabel();

	@Component(id="paymentPeriod",
			bindings={"value=paymentPeriod","displayName=message:PaymentPeriod","model=ognl:@com.magneta.administration.partners.pages.EditPartner@AFFILIATE_PAYMENT_PERIODS"})
	public abstract PropertySelection getPaymentPeriodField();

	@Component(id="allowFreeCreditsCBLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:allowFreeCreditsCB"})
	public abstract ExFieldLabel getAllowFreeCreditsCBLabel();

	@Component(id="allowFreeCreditsCB",
			bindings={"value=allowFreeCredits","displayName=message:AllowFreeCredits"})
	public abstract Checkbox getAllowFreeCreditsCB();

	@Component(id="allowFullFreeCreditsCBLabel",
			bindings={"field=component:allowFullFreeCreditsCB"})
	public abstract FieldLabel getAllowFullFreeCreditsCBLabel(); 

	@Component(id="allowFullFreeCreditsCB",
			bindings={"value=allowFullFreeCredits","displayName=message:AllowFullFreeCredits"})
	public abstract Checkbox getAllowFullFreeCreditsCB();


	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		if (params != null && params.length == 1) {
			setUserID((Long)params[0]);
		}
		
		if (this.getUserID() == null) {
			redirectToPage("AccessDenied");
		}

		if (!this.getUserAccessService().canAccess(getUserID())) {
			redirectToPage("AccessDenied");
		}
		
		if (getUser().getUserId().equals(getUserID())) {
			redirectToPage("AccessDenied");
		}
	}

	@Override
	public void pageBeginRender(PageEvent event) {

		try {
			setUsername(getUsernameService().getUsername(getUserID()));
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}

		CorporateBean corporateBean;
		try {
			corporateBean = this.getCorporateService().getCorporate(getUserID());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}

		setAffiliateRate(corporateBean.getAffiliateRate() * 100.0);
		setPaymentPeriod(corporateBean.getPaymentPeriod());
		setAllowFreeCredits(corporateBean.getAllowFreeCredits());
		setAllowFullFreeCredits(corporateBean.getAllowFullFreeCredits());

		List<Long> supers;
		try {
			supers = this.getCorporateService().getCorporateHierarchy(getUserID());
		} catch (ServiceException e1) {
			throw new RuntimeException(e1);
		}

		Double maxRate;
		Double minRate;

		if (supers.isEmpty()) {
			maxRate = corporateBean.getAffiliateRate();
		} else {
			CorporateBean superCorporateBean;
			try {
				superCorporateBean = this.getCorporateService().getCorporate(supers.get(0));
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}

			maxRate = superCorporateBean.getAffiliateRate() * 100.0;
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			this.getDelegate().record("Database error", null);
			return;
		}


		PreparedStatement stmt5 = null;
		ResultSet res = null;
		try {
			stmt5 = conn.prepareStatement(
					"SELECT (MAX(sub_affiliates.affiliate_rate))"+
							" FROM affiliates"+
							" LEFT OUTER JOIN affiliate_users ON affiliate_users.affiliate_id = affiliates.affiliate_id"+
							" AND affiliation = 3"+
							" LEFT OUTER JOIN affiliates AS sub_affiliates ON sub_affiliates.affiliate_id = affiliate_users.user_id"+
					" WHERE affiliates.affiliate_id = ?");
			stmt5.setLong(1, getUserID());

			res = stmt5.executeQuery();

			if (res.next()) {
				minRate = res.getDouble(1) * 100.0;
			} else {
				minRate = 0.0d;
			}

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving user information", e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(stmt5);
			DbUtil.close(conn);
		}

		getMinAmount().setMin(minRate);
		getMaxAmount().setMax(maxRate);
	}

	public void onSubmit(IRequestCycle cycle) {
		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			throw new RuntimeException("Out of database connections");
		}

		PreparedStatement stmt2 = null;
		try {
			stmt2 = conn.prepareStatement(
					"UPDATE affiliates SET"+
							" affiliate_rate = ?, payment_period = CAST (? AS INTERVAL)," +
							" allow_free_credits = ?," +
							" modifier_user = ?, allow_full_free_credits = ?" +
					" WHERE affiliate_id = ?");

			stmt2.setDouble(1, getAffiliateRate() / 100.0);
			stmt2.setString(2, getPaymentPeriod());
			stmt2.setBoolean(3, getAllowFreeCredits());
			stmt2.setLong(4, getUser().getId());
			stmt2.setBoolean(5, getAllowFullFreeCredits());
			stmt2.setLong(6, getUserID());

			stmt2.executeUpdate();
		} catch (SQLException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Error updating partner.",e);
			return;
		} finally {
			DbUtil.close(stmt2);
			DbUtil.close(conn);
		}

		cycle.forgetPage(this.getPageName());
		new RedirectExternalCallback("UserInformation", new Object[] { getUserID()}).performCallback(cycle);
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.forgetPage(this.getPageName());
		new RedirectExternalCallback("UserInformation", new Object[] { getUserID()}).performCallback(cycle);
	}
}
