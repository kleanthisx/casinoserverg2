package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.UserRegistrationBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.AccountLevelViolation;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.casino.services.utils.PhoneUtils;
import com.magneta.tapestry4.components.ExFieldLabel;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

public abstract class CreateSubPartner extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(CreateSubPartner.class);
	
	@Bean
	public abstract ValidationDelegate getDelegate();
	
	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();
	
	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporatesService();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();

	public abstract String getUsername();
	public abstract String getPassword();
	public abstract String getPasswordConfirm();
	public abstract String getEmail();
	public abstract String getFirstName();
	public abstract String getLastName();
	public abstract String getMiddleName();
	public abstract String getCountry();
	public abstract void setCountry(String country);
	
	public abstract String getPhone();

	public abstract String getRegion();
	public abstract void setRegion(String region);

	public abstract String getTown();
	public abstract void setTown(String town);
	public abstract String getPostCode();
	public abstract String getAddress();
	
	public abstract String getTimeZone();
	public abstract void setTimeZone(String timezone);
	
	public abstract Double getAffiliateRate();
	public abstract String getPaymentPeriod();

	public abstract boolean getAllowFreeCredits();
	public abstract void setAllowFreeCredits(boolean allow);

	public abstract boolean getAllowFullFreeCredits();

	public abstract boolean isApprovalRequired();
	public abstract void setApprovalRequired(boolean required);

	public abstract Integer getSubLevels();
	public abstract void setSubLevels(Integer levels);
	
	public abstract boolean isSuperApprovalRequired();
	public abstract void setSuperApprovalRequired(boolean req);
	
	@InitialValue("true")
	public abstract Boolean getPartner();
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getError();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumTranslator();
	
	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();
	
	public abstract void setAllowedMaxRate(double allowedMaxRate);
	public abstract double getAllowedMaxRate();
	
	@Bean
	public abstract Max getMaxAmount();

	@Component(id="registerForm",
			bindings={"success=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
			"clientValidationEnabled=true"})
	public abstract Form getRegisterForm();
	
	@Component(id="username",
			bindings={"value=username","displayName=message:Username","validators=validators:required,minLength=6"})
	public abstract TextField getUsernameField();
	
	@Component(id="usernameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:username"})
	public abstract ExFieldLabel getUsernameLabel();
	
	@Component(id="password",
			bindings={"value=password","hidden=true","displayName=message:Password","validators=validators:required,minLength=6"})
	public abstract TextField getPasswordField();
	
	@Component(id="passwordConfirm",
			bindings={"value=passwordConfirm","hidden=true","displayName=message:ConfirmPassword",
			"validators=validators:required,match=password[%confirm-password-not-equal]"})
	public abstract TextField getPasswordConfirmField();
	
	@Component(id="passwordLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:password"})
	public abstract ExFieldLabel getPasswordLabel();
	
	@Component(id="passwordConfirmLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:passwordConfirm"})
	public abstract ExFieldLabel getPasswordConfirmLabel();
	
	@Component(id="email",
			bindings={"value=email","displayName=literal:E-mail","validators=validators:email"})
	public abstract TextField getEmailField();
	
	@Component(id="emailLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:email"})
	public abstract ExFieldLabel getEmailLabel();
	
	@Component(id="firstName",
			bindings={"value=firstName","displayName=message:first-name","validators=validators:required"})
	public abstract TextField getFirstNameField();
	
	@Component(id="firstNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:firstName"})
	public abstract ExFieldLabel getFirstNameLabel();
	
	@Component(id="middleName",
			bindings={"value=middleName","displayName=literal:Middle Name"})
	public abstract TextField getMiddleNameField();
	
	@Component(id="middleNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:middleName"})
	public abstract ExFieldLabel getMiddleNameLabel();

	@Component(id="lastName",
			bindings={"value=lastName","displayName=message:last-name","validators=validators:required"})
	public abstract TextField getLastNameField();
	
	@Component(id="lastNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:lastName"})
	public abstract ExFieldLabel getLastNameLabel();
	
	@Component(id="town",
			bindings={"value=town","displayName=message:Town"})
	public abstract TextField getTownField();
	
	@Component(id="townLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:town"})
	public abstract ExFieldLabel getTownLabel();
	
	@Component(id="postCode",
			bindings={"value=postCode","displayName=message:postal-code"})
	public abstract TextField getPostCodefield();
	
	@Component(id="postCodeLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:postCode"})
	public abstract ExFieldLabel getPostCodeLabel();
	
	@Component(id="region",
			bindings={"value=region","displayName=message:Region"})
	public abstract TextField getRegionField();
	
	@Component(id="regionLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:region"})
	public abstract ExFieldLabel getRegionLabel();
	
	@Component(id="address",
			bindings={"value=address","displayName=message:Address"})
	public abstract TextArea getAddressField();
	
	@Component(id="addressLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:address"})
	public abstract ExFieldLabel getAddressLabel();
	
	@Component(id="country",
			bindings={"value=country","displayName=message:Country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel(true)",
			"validators=validators:required,minLength=2[%no-country-selected]"})
	public abstract PropertySelection getCountryField();
	
	@Component(id="countryLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:country"})
	public abstract ExFieldLabel getCountryLabel();
	
	@Component(id="zone",
			bindings={"value=timeZone","displayName=message:TimeZone",
			"model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel()","validators=validators:minLength=2[%timezone-required]"})
	public abstract PropertySelection getZone();
	
	@Component(id="zoneLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:zone"})
	public abstract ExFieldLabel getZoneLabel();
	
	@Component(id="phone",
			bindings={"value=phone","displayName=message:Phone"})
	public abstract TextField getPhoneField();
	
	@Component(id="phoneLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:phone"})
	public abstract ExFieldLabel getPhoneLabel();
	
	@Component(id="registerSubmit",
			bindings={"disabled=!allowNewCorp()"})
	public abstract Submit getRegisterSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Component(id="allowFreeCreditsCBLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:allowFreeCreditsCB"})
	public abstract ExFieldLabel getAllowFreeCreditsCBLabel();
	
	@Component(id="allowFreeCreditsCB",
			bindings={"value=allowFreeCredits","displayName=message:AllowFreeCredits"})
	public abstract Checkbox getAllowFreeCreditsCB();
	
	@Component(id="allowFullFreeCreditsCBLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:allowFullFreeCreditsCB"})
	public abstract ExFieldLabel getAllowFullFreeCreditsCBLabel();
	
	@Component(id="allowFullFreeCreditsCB",
			bindings={"value=allowFullFreeCredits","displayName=message:AllowFullFreeCredits"})
	public abstract Checkbox getAllowFullFreeCreditCB();
	
	@Component(id="affiliateRateLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:affiliateRate"})
	public abstract ExFieldLabel getAffiliateRateLabel();
	
	@Component(id="affiliateRate",
			bindings={"value=affiliateRate","displayName=message:AffiliateRate","translator=bean:numTranslator","validators=validators:required,$minAmount,$maxAmount"})
	public abstract TextField getAffiliateRateField();
	
	@Component(id="paymentPeriodLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:paymentPeriod"})
	public abstract ExFieldLabel getPaymentPeriodLabel();
	
	@Component(id="paymentPeriod",
			bindings={"value=paymentPeriod","displayName=message:PaymentPeriod","model=ognl:@com.magneta.administration.partners.pages.EditPartner@AFFILIATE_PAYMENT_PERIODS"})
	public abstract PropertySelection getPaymentPeriodField();
	
	@Override
	public void pageBeginRender(PageEvent event) {
		
		CorporateBean corporate;
		
		try {
			corporate = getCorporatesService().getCorporate(getUser().getId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		setAllowedMaxRate(corporate.getAffiliateRate() * 100.0d);
		setApprovalRequired(corporate.getRequiresApproval());
		setSuperApprovalRequired(corporate.getRequiredSuperApproval());
		setSubLevels(corporate.getSubLevels());
		
		if(this.getSubLevels() != null && this.getSubLevels() == 0) {
			getDelegate().record(null, getMsg("registration-not-allowed"));
		}

		getMaxAmount().setMax(this.getAllowedMaxRate());

		if (!event.getRequestCycle().isRewinding()) {
			setAllowFreeCredits(corporate.getAllowFreeCredits());
			
			UserDetailsBean usr;
			try {
				usr = this.getUserDetailsService().getUser(getUser().getId());
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}

			setTown(usr.getTown());
			setRegion(usr.getRegion());
			setCountry(usr.getCountryCode());
			setTimeZone(usr.getTimezone());
		}
	}
	
	public void onSubmit(IRequestCycle cycle) {

		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors())
			return;
		else if (!getPassword().equals(getPasswordConfirm())) {
			delegate.record(getPasswordConfirmField(), getMsg("confirm-password-not-equal"));
			return;
		}

		UserRegistrationBean usr = new UserRegistrationBean();
		usr.setCorporateId(getUser().getId());
		
		usr.setUserName(getUsername().trim());
		usr.setPassword(getPassword());
		usr.setEmail(getEmail());
		usr.setFirstName(getFirstName());
		usr.setLastName(getLastName());
		usr.setMiddleName(getMiddleName());
		usr.setCountryCode(getCountry());

		if (PhoneUtils.isValidPhone(getPhone())) {
        	usr.setPhone(PhoneUtils.normalizePhone(getPhone()));  
        } else {
        	delegate.record(getPhoneField(), getMsg("phone-error"));
        	return;
        }
		
		usr.setRegion(getRegion());
		usr.setTown(getTown());
		usr.setPostalCode(getPostCode());
		usr.setUserType(UserTypeEnum.CORPORATE_ACCOUNT.getId());
		usr.setTimezone(getTimeZone());
		
		CorporateBean corporateInfo = new CorporateBean();
		corporateInfo.setAffiliateRate(getAffiliateRate() / 100.0d);
		corporateInfo.setPaymentPeriod(getPaymentPeriod());
		corporateInfo.setAllowFreeCredits(getAllowFreeCredits());
		corporateInfo.setAllowFullFreeCredits(getAllowFullFreeCredits());
		corporateInfo.setFreeCreditsRate(null);
		corporateInfo.setRequiresApproval(isApprovalRequired());
		corporateInfo.setSubLevels(getSubLevels());
		corporateInfo.setRequiredSuperApproval(isSuperApprovalRequired());
		
		try {
			getUserRegistrationService().registerCorporate(usr, corporateInfo);
		} catch (DuplicateUsernameException e) {
			delegate.record(getUsernameField(), getMsg("username-exists"));
			return;
		} catch (DuplicateEmailException e) {
			delegate.record(getEmailField(), getMsg("email-exists"));
			return;
		} catch (AccountLevelViolation e) {
			delegate.record(null, getMsg("registration-not-allowed"));
			return;
		} catch (ServiceException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Error creating corporate account", e);
			return;
		}
		
		if(corporateInfo.getApproved()) {
			redirectHome();
		} else {
			redirectToPage("SubCorporateRequiresApproval");
		}
	}

	public void onCancel(IRequestCycle cycle) {
		redirectHome();
	}
	
	public boolean allowNewCorp() {
		return this.getSubLevels() == null || this.getSubLevels() > 0;
	}
}
