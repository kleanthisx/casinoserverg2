package com.magneta.administration.partners.pages;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.partners.beans.TransferBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class ConfirmFundsClear extends SecurePage {

	private static final Logger log = LoggerFactory.getLogger(ConfirmFundsClear.class);
	
	@InjectObject("service:casino.common.UserAccess")
	public abstract UserAccessService getUserAccessService();
	
	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();

	@Bean
	public abstract ValidationDelegate getDelegate();

	@Persist("client:page")
	public abstract Long getUserId();
	public abstract void setUserId(Long userId);

	public abstract String getPassword();

	public abstract String getUsername();
	public abstract void setUsername(String username);

	public abstract IFieldTracking getCurrentFieldTracking();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();

	@Component(id="confirmForm",
			bindings={"success=listener:onSubmit","cancel=listener:onCancel","clientValidationEnabled=true"})
	public abstract Form getConfirmForm();

	@Component(id="password",
			bindings={"value=password","hidden=true","displayName=message:Password","validators=validators:required"})
	public abstract TextField getPasswordField();

	@Component(id="passwordLabel",
			bindings={"field=component:password"})
	public abstract FieldLabel getPasswordLabel();

	@Component(id="confirmSubmit")
	public abstract Submit getConfirmSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {            
		if ((params != null)  && (params.length > 0)){
			setUserId((Long)params[0]);
		}
	}

	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);	
		ValidationDelegate delegate = getDelegate();	

		if(getUserId() == null){
			delegate.record(getMessages().getMessage("invalid-request"), null);
			return;
		}

		setUsername(getUsername(getUserId()));
	}


	public void onSubmit(IRequestCycle cycle) throws ServiceException {
		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors()){
			return;
		} else if (!getUserRegistrationService().verifyPassword(getUser().getUserId(), getPassword())){
			delegate.record(getPasswordField(), getMsg("invalid-password"));
			return;
		}

		if (!getUserAccessService().canAccess(getUserId())) {
			getDelegate().record(null, "Illegal operation.");
			return;
		}

		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null) {
			delegate.record(null, getMsg("db-error"));
			return;
		}

		PreparedStatement statement = null;
		ResultSet res = null;
		try {
			String sql = 
					"SELECT clear_corp_user_balance(?,?)";

			statement = dbConn.prepareStatement(sql);
			statement.setLong(1, getUser().getId());
			statement.setLong(2, getUserId());

			res = statement.executeQuery();

			if (res.next()) {
				Array arr = res.getArray(1);

				ResultSet amountsRs = null;
				try {
					amountsRs = arr.getResultSet();

					TransferBean bean = null;

					if (amountsRs.next()) {
						bean = new TransferBean();
						bean.setType("Clear");
						bean.setUsername(getUsername(getUserId()));
						bean.setAmount(amountsRs.getDouble(2));
						bean.setTransferDate(new Date());
						
						if (amountsRs.next()) {
							bean.setPromoAmount(amountsRs.getDouble(2));
						}	
						getUser().addTransfer(bean);
					}

				} finally {
					DbUtil.close(amountsRs);
				}

			}
		} catch (SQLException e) {
			if (e.getMessage().toLowerCase().contains("check_user_balance_positive")){
				delegate.record(null, getMsg("insufficient-funds"));
			} else {
				log.error("Error while clearing user/corporate balance", e);
				delegate.record(null, getMsg("db-error"));
			}
			return;
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
		
		getRequestCycle().forgetPage(this.getPageName());
		redirectToPage("TransferFunds");
	}

	public void onCancel(){
		getRequestCycle().forgetPage(this.getPageName());
		redirectToPage("TransferFunds");
	}

	public String getConfirmationMessage() {
		return new MessageFormat(getMsg("Confirmation")).format(new Object[]{getUsername()});
	}
}