package com.magneta.administration.partners.pages;

import java.util.Date;

import org.apache.tapestry.IExternalPage;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.engine.ExternalServiceParameter;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.event.PageValidateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.partners.SystemProperties;
import com.magneta.casino.common.utils.DateUtils;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.common.utils.Util;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;

/**
 * Parent class for all pages requiring the user to login before
 * they can be accessed
 * 
 * @author Nicos
 *
 */
public abstract class SecurePage extends PublicPage implements PageValidateListener, IExternalPage {
	
	private static final Logger log = LoggerFactory.getLogger(SecurePage.class);

	@InjectObject("service:casino.common.Username")
	public abstract UsernameService getUsernameService();
	
	@InjectObject("service:t4.common.PageMenuItemAccessValidator")
	public abstract PageMenuItemAccessValidator getAccessValidator();

	public String formatDate(Date d) {
        return FormatUtils.getFormattedDate(d, getUser().getTimeZone(), getLocale());
    }
	
	public Date normalizeDate(Date d) {
        return DateUtils.getNormalizedDate(getUser().getTimeZone(), d);
    }
	
	public Date denormalizeDate(Date d) {
        return DateUtils.getDenormalizedDate(getUser().getTimeZone(), d);
    }
	
	public String formatAmount(Double amount) {
        return getCurrencyParser().formatDouble(amount, getLocale());
    }
	
	public String formatAmount(double amount) {
        return getCurrencyParser().formatDouble(amount, getLocale());
    }
	
	public String formatAmountHideZero(double amount) {
		double normalAmount = getCurrencyParser().normalizeDouble(amount);
		if (normalAmount == 0.0) {
			return "";
		}
		
        return getCurrencyParser().formatDouble(amount, getLocale());
    }
	
	
	public String formatCurrency(Double amount) {
        if (amount == null ) {
            return "";
        }

        return getCurrencyParser().formatCurrency(amount);
    }
	
    @Override
	public String getMsg(String msg) {
        return this.getMessages().getMessage(msg);
    }
    
    public String getLanguage() {
    	try {
            return this.getLocale().getLanguage();
        } catch (Exception ex) {
            return "en";
        }
    }
    
	@Override
	public void pageValidate(PageEvent event) {		          
		if (!getUser().isLoggedIn()) {
			IEngineService service =
					event.getRequestCycle().getInfrastructure().getServiceMap().getService(Tapestry.EXTERNAL_SERVICE);

			ILink redirectLink = service.getLink(false, new ExternalServiceParameter(this.getPageName(), event.getRequestCycle().getListenerParameters())); 
			String redirectUrl = redirectLink.getURL();
			
			ILink loginLink = service.getLink(false, new ExternalServiceParameter("Login", new Object[] {redirectUrl})); 
			throw new RedirectException(loginLink.getURL());
		}
		
		if (!getAccessValidator().checkAccess(getPageName())) {
            redirectToPage("AccessDenied");
        }
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
	}
	
	public void redirectHome(){
		IEngineService s = getRequestCycle().getInfrastructure().getServiceMap().getService(Tapestry.PAGE_SERVICE);
        
        ILink link = s.getLink(false, "Home");
        
        if (link != null) {
            String redirect = link.getURL();
            throw new RedirectException(redirect);
        }
	}
	
	protected void logReportGeneration(String report, Date minDate, Date maxDate, String IP, String requestUrl) {
		try {
			if (SystemProperties.logReportsGeneration()) {
				Util.logReportGeneration(SystemProperties.APP_NAME, report, getUser().getId(), minDate, maxDate, IP, requestUrl);
			}
		} catch (ServiceException e) {
			log.warn("Unable to log report generation", e);
		}
    }
	
	public String getUsername(Long userId) {
		try {
			return getUsernameService().getUsername(userId);
		} catch (ServiceException e) {
			log.error("Unable to retrieve username", e);
		}
		
		return null;
	}
	
	@Override
	public boolean getDiplayMenu() {
		return true;
	}
}
