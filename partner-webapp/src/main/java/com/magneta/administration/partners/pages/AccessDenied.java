package com.magneta.administration.partners.pages;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.link.PageLink;

public abstract class AccessDenied extends PublicPage{
	
	@Component(id="homeLink",
			bindings={"page=literal:Home"})
	public abstract PageLink getHomeLink();
	
	
}
