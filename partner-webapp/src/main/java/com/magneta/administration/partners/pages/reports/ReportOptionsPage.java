package com.magneta.administration.partners.pages.reports;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.administration.commons.services.reporting.ReportGenerationServiceEncoder;
import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.administration.partners.SystemProperties;
import com.magneta.administration.partners.pages.SecurePage;
import com.magneta.administration.partners.reports.ReportGenerationQueue;
import com.magneta.administration.partners.reports.ReportGenerationRequest;
import com.magneta.casino.services.ServiceException;

public abstract class ReportOptionsPage extends SecurePage {

	@InjectObject("service:casino.partner.Reporting")
	public abstract IEngineService getReportingService();

	@InjectObject("service:tapestry.globals.HttpServletRequest")
	public abstract HttpServletRequest getRequest();

	@Bean
	public abstract ValidationDelegate getDelegate();

	@Component(id = "formats",
			bindings = {"value=format", "model=ognl:new com.magneta.administration.commons.models.ReportFormatsSelectionModel()", 
			"displayName=message:ReportFormat"})
	public abstract PropertySelection getFormats();

	@Component(id = "formatsLabel", 
			bindings = {"field=component:formats"})
	public abstract FieldLabel getFormatsLabel();

	@Component(id = "zones",
			bindings = {"value=timeZone","model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel(User.TimeZone)", 
			"displayName=message:ReportTimeZone"})
	public abstract PropertySelection getZones();

	@Component(id = "zoneLabel",
			bindings = {"field=component:zones"})
	public abstract FieldLabel getZoneLabel();
	
	@Component(id = "fromDate",
			bindings={"value=minDate","displayName=message:From", "translator=translator:date,pattern=dd/MM/yyyy","validators=validators:required"})
			public abstract DatePicker getFromDate();

	@Component(id = "toDate",
			bindings={"value=maxDate","displayName=message:To", "translator=translator:date,pattern=dd/MM/yyyy","validators=validators:required"})
			public abstract DatePicker getToDate();

	@Component(id = "fromDateLabel",
			bindings={"field=component:fromDate"})
	public abstract FieldLabel getFromDateLabel();

	@Component(id = "toDateLabel",
			bindings={"field=component:toDate"})
	public abstract FieldLabel getToDateLabel();
	
	@Component(id="reportDataForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
			"clientValidationEnabled=true"})
	public abstract Form getReportDataForm();

	@Component(id = "okSubmit")
	public abstract Submit getOkSubmit(); 

	@Component(id = "cancelButton")
	public abstract Button getCancelButton();

	public abstract String getFormat();
	public abstract String getTimeZone();

	public abstract ReportBean getReport();

	public abstract void setReportDesc(String reportDesc);
	public abstract String getReportDesc();

	public abstract void setResult(String result);
	public abstract void setError(String error);

	public abstract IFieldTracking getCurrentFieldTracking();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrorsDelegator();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	public ILink onSubmit(IRequestCycle cycle) throws ServiceException{
		if (getDelegate().getHasErrors()){
			return null;
		}

		boolean logReport = SystemProperties.logReportsGeneration();
		
		List<ServiceParameter> reportParams = new ArrayList<ServiceParameter>();
		fillReportParameters(reportParams);

		ILink link = getReportingService().getLink(true, reportParams.toArray());

		if (logReport){
			Timestamp minDate = null;
			Timestamp maxDate = null;

			for (ServiceParameter p: reportParams){
				if (p.getParameterName().equals("min_date")){
					minDate = (Timestamp)p.getParameterValue();
				} else if (p.getParameterName().equals("max_date")){
					maxDate = (Timestamp)p.getParameterValue();
				}

				if (minDate != null && maxDate != null){
					break;
				}
			}
	
			logReportGeneration(getReport().getReportName(), minDate, maxDate, getRequest().getRemoteAddr(), link.getURL());
		}

		if (isQueuedReport()) {
			ReportGenerationRequest generation = new ReportGenerationRequest(this.getUser(), getReportName(), this.getFormat(), reportParams, getReport().getReportName());
			int queuePos = ReportGenerationQueue.getInstance().enqueueReport(generation);
			
			if (queuePos > 0){
				setResult("Your report was successfully entered in queue position "+queuePos+". You can find the finished reports in the 'Other Statements' page.");
			} else {
				setError("Report generation queue full. Please wait a few minutes and try again.");
			}
			return null;
		}

		return link;
	}

	/**
	 * Fills the parameter values of the report with the values supplied by the form of the page.
	 * Should be overriden by subclasses when they have report-specific parameters to add.
	 * @param reportParams The list to fill with the parameters and values. Should never be null.
	 */
	public void fillReportParameters(List<ServiceParameter> reportParams) {
		reportParams.add(new ServiceParameter(ReportGenerationServiceEncoder.REPORT_NAME_PARAMETER, getReportName()));
		reportParams.add(new ServiceParameter(ReportGenerationServiceEncoder.FORMAT_PARAMETER, getFormat()));
		reportParams.add(new ServiceParameter(JRParameter.REPORT_TIME_ZONE, TimeZone.getTimeZone(getTimeZone())));	
	}

	/**
	 * Gets the name of the report the page configures. Should always be overriden by subclasses.
	 * @return The name of the report the page configures.
	 */
	public String getReportName(){
		if (getReport() == null) {
			return null;
		}
		
		return getReport().getReportName();
	}

	public void onCancel(IRequestCycle cycle){
		IEngineService s = cycle.getInfrastructure().getServiceMap().getService(Tapestry.PAGE_SERVICE);

		ILink link = s.getLink(false, "Home");

		if (link != null) {
			String redirect = link.getURL();
			throw new RedirectException(redirect);
		}
	}

	/* Removes the time part of the min date */
	public Date removeTimeMin(Date d) {
		if (d == null) {
			return null;
		}
		
		Calendar calendar = Calendar.getInstance(getUser().getTimeZone());
    	calendar.setTimeInMillis(d.getTime());        	
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
    	calendar.set(Calendar.MINUTE, 0);
    	calendar.set(Calendar.SECOND, 0);
    	calendar.set(Calendar.MILLISECOND, 0);
    	
    	return calendar.getTime();
	}
	
	/* Removes the time part of the max date */
	public Date removeTimeMax(Date d) {
		if (d == null) {
			return null;
		}
		
		Calendar calendar = Calendar.getInstance(getUser().getTimeZone());
    	calendar.setTimeInMillis(d.getTime());        	
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
    	calendar.set(Calendar.MINUTE, 0);
    	calendar.set(Calendar.SECOND, 0);
    	calendar.set(Calendar.MILLISECOND, 0);
    	calendar.add(Calendar.DAY_OF_MONTH, 1);
    	
    	return calendar.getTime();
	}
	
	public boolean isQueuedReport() throws ServiceException{	
    	return getReport() != null && getReport().isQueueReport() && SystemProperties.useReportQueue();
    }
}