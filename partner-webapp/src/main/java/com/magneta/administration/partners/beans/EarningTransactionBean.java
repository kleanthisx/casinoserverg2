package com.magneta.administration.partners.beans;

import java.io.Serializable;
import java.util.Date;

public class EarningTransactionBean implements Serializable{
    
    private static final long serialVersionUID = 8224156905566449055L;
    private Date date;
    private double amount;
    private String username;
    private String initiator;
    private String details;
    
    public Date getDate() {
        return date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public double getAmount() {
        return amount;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }
    
	public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
	public void setInitiator(String initiatorId) {
		this.initiator = initiatorId;
	}

	public String getInitiator() {
		return initiator;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getDetails() {
		return details;
	}
}
