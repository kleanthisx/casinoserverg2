package com.magneta.administration.partners.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.administration.partners.beans.TransferBean;
import com.magneta.casino.common.system.SettingNames;
import com.magneta.casino.common.user.utils.UserBalance;
import com.magneta.casino.internal.beans.ExtendedPrincipalBeanImpl;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.AffiliateUsersBean;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.UserAffiliationEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.util.CircularBuffer;

public class User extends ExtendedPrincipalBeanImpl implements Serializable, Cloneable {
	
	private static final long serialVersionUID = -7897018879966479542L;
	
	private UserDetailsBean details;
	private CorporateBean corporate;	
	private Long superCorp;

	private boolean hasUsers;
	private boolean hasSubCorps;
	private CircularBuffer<TransferBean> lastTransfers;	
	
	public User() {
        this.setUserId(0L);
		this.superCorp = 0L;
		lastTransfers = new CircularBuffer<TransferBean>(TransferBean.class, 10);
	}

	public User(UserAuthenticationBean authenticationBean) {
		super(authenticationBean);		
		this.superCorp = 0L;
		lastTransfers = new CircularBuffer<TransferBean>(TransferBean.class, 10);
	}

	public User(User user) {
        super(user);
        
        this.details = user.details;
        this.corporate = user.corporate;
        this.superCorp = user.superCorp;

        this.hasUsers = user.hasUsers;
        this.hasSubCorps = user.hasSubCorps;        
        this.lastTransfers = new CircularBuffer<TransferBean>(TransferBean.class, 10);
    }
	
	@Override
	public Object clone() {
		return new User(this);
	}
	
	public void addTransfer(TransferBean transfer) {
		synchronized (lastTransfers) {
			lastTransfers.add(transfer);
		}
	}

	public TransferBean[] getLastTransfers() {
		synchronized (lastTransfers) {
			return lastTransfers.toArray(new TransferBean[0]);
		}
	}
	
	public boolean isLoggedIn() { 
    	return this.getUserId() != null && this.getUserId() > 0;
    }
    
    public int getUserType() {
        return getType().getId();
    }

    public void setUserType(int userType) {
        setType(UserTypeEnum.valueOf(userType));
    }

    public String getUsername() {
        return details.getUserName();
    }

    public String getEmail() {
        return details.getEmail();
    }

    public String getFirstName() {
        return details.getFirstName();
    }

    public String getLastName() {
        return details.getLastName();
    }

    public String getMiddleName() {
        return details.getMiddleName();
    }

    public String getRegion() {
        return details.getRegion();
    }

    public String getCountry() {
        return details.getCountryCode();
    }

    public String getPhone() {
        return details.getPhone();
    }

    public String getTown() {
        return details.getTown();
    }

    public String getPostCode() {
        return details.getPostalCode();
    }

    public Long getId() {
        return getUserId();
    }

    public double getBalance() {
        return UserBalance.getBalance(getUserId());
    }
    
    public TimeZone getTimeZone() {
        String tz =  details.getTimezone();
        
        if (tz == null) {
        	return TimeZone.getDefault();
        }

        return TimeZone.getTimeZone(tz);
    }
 
    public String getAddress() {
        return details.getPostalAddress();
    }
	
	private boolean hasUsers(UserAffiliationEnum userType, CorporatesService corporateService, UserDetailsService userDetailsService) throws ServiceException {
		List<AffiliateUsersBean> players = corporateService.getSubAccounts(this.getUserId(), 
				FiqlContextBuilder.create(AffiliateUsersBean.class, "affiliation==?", userType.getId()), 
				null);

		List<Long> playerIds = new ArrayList<Long>(players.size());
		for (AffiliateUsersBean player: players) {
			playerIds.add(player.getUserId());
		}
		
		ServiceResultSet<UserDetailsBean> userDetails = userDetailsService.getUsers(
				FiqlContextBuilder.create(UserDetailsBean.class, "id=in=?", playerIds),
				0, 0, null);
		
		boolean hasUsers = false;
		for (UserDetailsBean player: userDetails.getResult()) {
			if (player.getIsActive() && ! player.getIsClosed()) {
				hasUsers = true;
				break;
			}
		}
		
		return hasUsers;
	}
	
	private long LAST_USER_UPDATE;

	public void updateHasUsers(CorporatesService corporateService, UserDetailsService userDetailsService) throws ServiceException {
		/* Allow update every 5 minutes */
		if (System.currentTimeMillis() - LAST_USER_UPDATE < (5 * 60 * 1000)) {
			return;
		}

		setHasUsers(hasUsers(UserAffiliationEnum.USER_AFFILIATION, corporateService, userDetailsService));
		setHasSubCorps(hasUsers(UserAffiliationEnum.SUB_CORPORATE_AFFILIATION, corporateService, userDetailsService));
	
		LAST_USER_UPDATE = System.currentTimeMillis();
	}

	public void onAuthenticated(UserDetailsBean userDetails, CorporateBean corporate, CorporateBean superCorp, CorporatesService corporateService, UserDetailsService userDetailsService, SettingsService settingsService) throws ServiceException {
		 
		 this.details = userDetails;
		 this.corporate = corporate;
		 
		 if (superCorp != null) {
			 this.superCorp = superCorp.getAffiliateId();
		 } else {
			 this.superCorp = null;
		 }

		 updateHasUsers(corporateService, userDetailsService);
	}

	public boolean isAllowFreeCredits() {
		return corporate.getAllowFreeCredits();
	}

	public boolean isAllowFullFreeCredits() {
		return corporate.getAllowFullFreeCredits();
	}

	public double getFreeCreditsRate() throws ServiceException {
		Double freeRate = corporate.getFreeCreditsRate();

		 if (freeRate == null || freeRate < 0.001) {
			 freeRate = ServiceLocator.getService(SettingsService.class).getSettingValue(SettingNames.DEFAULT_PROMO_RATE, Double.TYPE);
		 }
		 
		 return freeRate;
	}

	public Long getSuperCorp() {
		return superCorp;
	}

	public boolean allowFreeCredits() {
		return corporate.getAllowFreeCredits();		
	}

	public void setHasUsers(boolean hasUsers) {
		this.hasUsers = hasUsers;
	}

	public boolean isHasUsers() {
		return hasUsers;
	}

	public void setHasSubCorps(boolean hasSubCorps) {
		this.hasSubCorps = hasSubCorps;
	}

	public boolean isHasSubCorps() {
		return hasSubCorps;
	}
	
	public void updateDetails(UserDetailsBean details) {
		this.details = details;
	}
}
