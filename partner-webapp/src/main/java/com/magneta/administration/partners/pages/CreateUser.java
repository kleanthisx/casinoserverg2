package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.UserRegistrationBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateNicknameException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.tapestry4.components.ExFieldLabel;

public abstract class CreateUser extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(CreateUser.class);
	
	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@Bean
	public abstract ValidationDelegate getDelegate();

	public abstract String getUsername();
	public abstract String getPassword();
	public abstract String getPasswordConfirm();
	public abstract String getNickname();
	public abstract String getEmail();
	public abstract String getFirstName();
	public abstract String getLastName();
	public abstract String getMiddleName();
	public abstract String getAddress();
	
	public abstract String getCountry();
	public abstract void setCountry(String country);
	
	public abstract String getPhone();
	
	public abstract String getRegion();
	public abstract void setRegion(String region);
	
	public abstract String getTown();
	public abstract void setTown(String town);
	
	public abstract String getPostCode();
	
	public abstract String getTimeZone();
	public abstract void setTimeZone(String timezone);
	
	public abstract void setNickname(String nick);

	public abstract IFieldTracking getCurrentFieldTracking();
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Component(id="registerForm",
			bindings={"success=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
			"clientValidationEnabled=true"})
	public abstract Form getRegisterForm();
	
	@Component(id="username",
			bindings={"value=username","displayName=message:Username","validators=validators:required,minLength=6"})
	public abstract TextField getUsernameField();
	
	@Component(id="usernameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:username"})
	public abstract ExFieldLabel getUsernameLabel();
	
	@Component(id="password",
			bindings={"value=password","hidden=true","displayName=message:Password","validators=validators:required,minLength=6"})
	public abstract TextField getPasswordField();
	
	@Component(id="passwordConfirm",
			bindings={"value=passwordConfirm","hidden=true","displayName=message:ConfirmPassword","validators=validators:required,match=password[%confirm-password-not-equal]"})
	public abstract TextField getPasswordConfirmField();
	
	@Component(id="passwordLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:password"})
	public abstract ExFieldLabel getPasswordLabel();
	
	@Component(id="passwordConfirmLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:passwordConfirm"})
	public abstract ExFieldLabel getPasswordConfirmLabel();

	@Component(id="nickname",
			bindings={"value=nickname","displayName=message:Nickname","validators=validators:minLength=6"})
	public abstract TextField getNicknameField();
	
	@Component(id="nicknameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:nickname"})
	public abstract ExFieldLabel getNicknameLabel();
	
	@Component(id="email",
			bindings={"value=email","displayName=literal:E-mail","validators=validators:email"})
	public abstract TextField getEmailField();
	
	@Component(id="emailLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:email"})
	public abstract ExFieldLabel getEmailLabel();
	
	@Component(id="firstName",
			bindings={"value=firstName","displayName=message:first-name","validators=validators:required"})
	public abstract TextField getFirstNameField();
	
	@Component(id="firstNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:firstName"})
	public abstract ExFieldLabel getFirstNameLabel();

	@Component(id="middleName",
			bindings={"value=middleName","displayName=literal:Middle Name"})
	public abstract TextField getMiddleNameField();
	
	@Component(id="middleNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:middleName"})
	public abstract ExFieldLabel getMiddleNameLabel();
	
	@Component(id="lastName", 
			bindings={"value=lastName","displayName=message:last-name","validators=validators:required"})
	public abstract TextField getLastNameField();
	
	@Component(id="lastNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:lastName"})
	public abstract ExFieldLabel getLastNameLabel();
	
	@Component(id="town",
			bindings={"value=town","displayName=message:Town"})
	public abstract TextField getTownField();
	
	@Component(id="townLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:town"})
	public abstract ExFieldLabel getTownLabel();
	
	@Component(id="postCode",
			bindings={"value=postCode","displayName=message:postal-code"})
	public abstract TextField getPostCodeField();
	
	@Component(id="postCodeLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:postCode"})
	public abstract ExFieldLabel getPostCodeLabel();
	
	@Component(id="region",
			bindings={"value=region","displayName=message:Region"})
	public abstract TextField getRegionField();
	
	@Component(id="regionLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:region"})
	public abstract ExFieldLabel getRegionLabel();
	
	@Component(id="address",
			bindings={"value=address","displayName=message:Address"})
	public abstract TextArea getAddressField();
	
	@Component(id="addressLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:address"})
	public abstract ExFieldLabel getAddressLabel();
	
	@Component(id="country",
			bindings={"value=country","displayName=message:Country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel(true)",
			"validators=validators:required,minLength=2[%no-country-selected]"})
	public abstract PropertySelection getCountryField();
	
	@Component(id="countryLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:country"})
	public abstract ExFieldLabel getCountryLabel();
	
	@Component(id="zone",
			bindings={"value=timeZone","displayName=message:TimeZone","model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel()",
			"validators=validators:minLength=2"})
	public abstract PropertySelection getZone();
	
	@Component(id="zoneLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:zone"})
	public abstract ExFieldLabel getZoneLabel();
	
	@Component(id="phone",
			bindings={"value=phone","displayName=message:Phone"})
	public abstract TextField getPhoneField();
	
	@Component(id="phoneLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:phone"})
	public abstract ExFieldLabel getPhoneLabel();
	
	@Component(id="registerSubmit")
	public abstract Submit getRegisterSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void pageBeginRender(PageEvent event) {

		if (!event.getRequestCycle().isRewinding()) {
			UserDetailsBean currentUser;
			try {
				currentUser = getUserDetailsService().getUser(getUser().getId());
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}


			setTown(currentUser.getTown());
			setRegion(currentUser.getRegion());
			setCountry(currentUser.getCountryCode());
			setTimeZone(currentUser.getTimezone());
		}
	}

	public void onSubmit(IRequestCycle cycle) {

		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors())
			return;
		else if (!getPassword().equals(getPasswordConfirm())){
			delegate.record(getPasswordConfirmField(), getMessages().getMessage("confirm-password-not-equal"));
			return;
		}

		String nickname = getNickname();
		
		if (nickname != null) {
			nickname = nickname.trim();
			
			if (nickname.isEmpty()) {
				nickname = null;
			}
		}
		
		UserRegistrationBean user = new UserRegistrationBean();
		user.setCountryCode(getCountry());
		user.setEmail(getEmail());
		user.setFirstName(getFirstName());
		user.setLastName(getLastName());
		user.setMiddleName(getMiddleName());
		user.setNickName(nickname);
		user.setPassword(getPassword());
		user.setPhone(getPhone());
		user.setPostalAddress(getAddress());
		user.setPostalCode(getPostCode());
		user.setRegion(getRegion());
		user.setTimezone(getTimeZone());
		user.setTown(getTown());
		user.setUserName(getUsername());
		
		user.setCorporateId(getUser().getId());
		user.setUserTypeEnum(UserTypeEnum.CORPORATE_PLAYER);
		
		try {
			this.getUserRegistrationService().registerUser(user);
		} catch (DuplicateUsernameException e) {
			delegate.record(getUsernameField(), getMsg("username-exists"));
			return;
		} catch (DuplicateEmailException e) {
			delegate.record(getEmailField(), getMsg("email-exists"));
			return;
		} catch (DuplicateNicknameException e) {
			delegate.record(getNicknameField(), getMsg("nickname-exists"));
			return;
		} catch (DatabaseException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Error while creating user", e);
			return;
		} catch (ServiceException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Error while creating user", e);
			return;
		}
		
		getUser().setHasUsers(true);
		redirectHome();
	}

	public void onCancel(IRequestCycle cycle) {
		redirectHome();
	}
}
