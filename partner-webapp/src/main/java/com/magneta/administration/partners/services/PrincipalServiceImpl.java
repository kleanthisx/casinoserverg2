package com.magneta.administration.partners.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry.engine.state.ApplicationStateManager;

import com.magneta.Config;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.administration.partners.SystemProperties;
import com.magneta.administration.partners.security.User;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.beans.SystemPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.WebLoginBean;

public class PrincipalServiceImpl implements PrincipalService {

	private static final String PRINCIPAL_KEY = "casino-user";

	private ApplicationStateManager appStateManager;
	private HttpServletRequest request;

	private final ThreadLocal<Integer> systemCount;
	private static final ExtendedPrincipalBean systemBean = new SystemPrincipalBean();


	public PrincipalServiceImpl() {
		this.systemCount = new ThreadLocal<Integer>();
	}

	public void setAppStateManager(ApplicationStateManager appStateManager) {
		this.appStateManager = appStateManager;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public ExtendedPrincipalBean getPrincipal() {
		Integer count = this.systemCount.get();
		if (count != null && count > 0) {
			return systemBean;
		}
		return (ExtendedPrincipalBean)this.appStateManager.get(PRINCIPAL_KEY);
	}

	private boolean isTrustedProxy(String ip) {
		String proxyList = Config.get("server.proxies");

		if (proxyList == null) {
			return false;
		}

		String[] proxies =  proxyList.split(" ");

		for (String proxy : proxies) {
			if (proxy.trim().equals(ip)) {
				return true;
			}
		}

		return false;
	}

	private String getRemoteIp() {
		String forwardedFor = request.getHeader("X-Forwarded-For");

		if (forwardedFor == null || !isTrustedProxy(request.getRemoteAddr())) {
			return request.getRemoteAddr();
		}

		String[] forwarders = forwardedFor.split(",");

		if (forwarders.length == 1) {
			return request.getRemoteAddr();
		}

		for (int i=1; i < forwarders.length; i++) {
			if (!isTrustedProxy(forwarders[i].trim())) {
				return request.getRemoteAddr();
			}
		}

		return forwarders[0].trim();
	}

	@Override
	public void onUserAuthenticated(UserAuthenticationBean authenticatedUser) throws ServiceException {

		CorporatesService corporateService = ServiceLocator.getService(CorporatesService.class);
		UserDetailsService userDetailsService = ServiceLocator.getService(UserDetailsService.class);
		WebLoginsService webLoginService = ServiceLocator.getService(WebLoginsService.class);

		pushSystem();
		try {

			CorporateBean corporate = corporateService.getCorporate(authenticatedUser.getUserId());
			CorporateBean superCorporate = corporateService.getSuperCorporate(authenticatedUser.getUserId());
			UserDetailsBean userDetails = userDetailsService.getUser(authenticatedUser.getUserId());

			if (!corporate.getApproved()) {
				throw new ServiceException("Account not approved");
			}

			User user = new User(authenticatedUser);
			user.onAuthenticated(userDetails, corporate, superCorporate,
					corporateService,
					userDetailsService,
					ServiceLocator.getService(SettingsService.class));

			WebLoginBean webLogin = new WebLoginBean();
			webLogin.setApplicationName(SystemProperties.APP_NAME);
			webLogin.setClientIp(getRemoteIp());
			webLogin.setUserId(authenticatedUser.getUserId());

			webLoginService.recordLogin(webLogin);
			this.appStateManager.store(PRINCIPAL_KEY, user);
		} finally {
			popSystem();
		}
	}

	@Override
	public void onUserLogout() {
		this.appStateManager.store(PRINCIPAL_KEY, null);
	}

	@Override
	public void pushSystem() {
		Integer count = this.systemCount.get();
		if (count == null) {
			count = 0;
		}

		count++;
		systemCount.set(count);
	}

	@Override
	public void popSystem() {
		Integer count = this.systemCount.get();
		if (count == null || count.equals(0)) {
			throw new RuntimeException("Unbalanced push/popSystem");
		}

		count--;

		if (count.equals(0)) {
			systemCount.remove();
		} else {
			systemCount.set(count);
		}
	}

	@Override
	public void resetSystem() {
		systemCount.remove();
	}

}
