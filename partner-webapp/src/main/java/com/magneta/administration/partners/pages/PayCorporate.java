package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.CorporateEarningsTransactionBean;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

public abstract class PayCorporate extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(PayCorporate.class);

	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();

	@InjectObject("service:casino.common.CorporateEarnings")
	public abstract CorporateEarningsService getCorporateEarningsService();

	@Bean
	public abstract Min getMinAmount();

	@Bean
	public abstract Max getMaxAmount();

	@Bean
	public abstract ValidationDelegate getDelegate();

	@Persist("client:page")
	public abstract Long getAffiliateId();
	public abstract void setAffiliateId(Long affiliateId);

	public abstract double getAmount();
	public abstract void setAmount(double amount);

	public abstract String getDetails();
	public abstract void setEarnings(double earnings);
	public abstract double getEarnings();
	public abstract void setBalance(double balance);

	public abstract void setUsername(String username);
	public abstract String getUsername();

	public abstract IFieldTracking getCurrentFieldTracking();

	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();

	@Component(id="payForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
	"clientValidationEnabled=true"})
	public abstract Form getPayForm();

	@Component(id="earnings",
			bindings={"value=formatCurrency(earnings)"})
	public abstract Insert getEarningsField();

	@Component(id="balance",
			bindings={"value=formatCurrency(balance)"})
	public abstract Insert getBalanceField();

	@Component(id="username",
			bindings={"value=username"})
	public abstract Insert getUsernameField();

	@Component(id="amountLabel",
			bindings={"field=component:amount"})
	public abstract FieldLabel getAmountLbl();

	@Component(id="amount",
			bindings={"value=amount","displayName=amountLabel","translator=bean:numTranslator","validators=validators:$minAmount,$maxAmount"})
	public abstract TextField getAmountField();

	@Component(id="detailsLabel",
			bindings={"field=component:details"})
	public abstract FieldLabel getDetailsLabel();

	@Component(id="details",
			bindings={"value=details","displayName=message:Details"})
	public abstract TextArea getDetailsField();

	@Component(id="okSubmit",
			bindings={"disabled=earnings < 0.009"})
	public abstract Submit getOkSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)) {    	
			setAffiliateId((Long)params[0]);
		} else {
			throw new RuntimeException("Invalid affiliateId");
		}
	}

	@Override
	public void pageBeginRender(PageEvent evt) {

		try {
			setUsername(getUsernameService().getUsername(getAffiliateId()));
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}

		UserBalanceBean balanceBean;
		try {
			balanceBean = getUserDetailsService().getUserBalance(getAffiliateId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		setBalance(balanceBean.getBalance());

		CorporateEarningsBean earningsBean;
		try {
			earningsBean = getCorporateEarningsService().getEarnings(getAffiliateId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}

		setEarnings(earningsBean.getEarnings());

		getMaxAmount().setMax(getEarnings() > 0.0? getEarnings() : 0.0);
		getMinAmount().setMin(getEarnings() < 0.0? getEarnings() : 0.0);

		setAmount(getEarnings());
	}

	public void onSubmit(IRequestCycle cycle) {
		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		if(getAmount() > -0.001 && getAmount() < 0.001){
			getDelegate().record(null, getMsg("zero-error"));
			return;
		}

		CorporateEarningsTransactionBean transaction = new CorporateEarningsTransactionBean();

		transaction.setCorporateId(getAffiliateId());
		transaction.setAmount(getAmount());
		transaction.setDetails(getDetails());


		try {
			this.getCorporateEarningsService().transferEarnings(transaction);
		} catch (ServiceException e) {
			log.error("Error while transfering earnings",e);
			getDelegate().record(null, getMsg("db-error"));
			return;
		}

		cycle.forgetPage(this.getPage().getPageName());
		redirectToPage("SubAccounts");
	}

	public String getAmountLabel() {
		String symbol = getCurrencyParser().getCurrencySymbol(getLocale());

		return getMsg("Amount") + " " + symbol;
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.forgetPage(this.getPage().getPageName());
		redirectToPage("SubAccounts");
	}
}
