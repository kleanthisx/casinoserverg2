package com.magneta.administration.partners.beans;

import java.io.Serializable;
import java.util.Date;

public class AffiliateBean implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private Date registerDate;
	
	private double affiliateRate;
	private double affiliateBalance;
	private boolean approved;
	private double deposit;
	private String paymentPeriod;
	private Date lastPayment;
	private boolean paymentDue;
	private Long superAffiliate;
	private boolean active;
	
	@Override
	public Object clone() {
		AffiliateBean clone = new AffiliateBean();
		
		clone.id = this.id;
		clone.username = this.username;
		clone.firstName = this.firstName;
		clone.lastName = this.lastName;
		clone.email = this.email;
		clone.registerDate = this.registerDate;
		clone.affiliateRate = this.affiliateRate;
		clone.affiliateBalance = this.affiliateBalance;
		clone.approved = this.approved;
		clone.deposit = this.deposit;
		clone.paymentPeriod = this.paymentPeriod;
		clone.paymentDue = this.paymentDue;
		clone.superAffiliate = this.superAffiliate;
		clone.active = this.active;
		
		return clone;
	}

	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Long getSuperAffiliate() {
		return superAffiliate;
	}
	public void setSuperAffiliate(Long superAffiliate) {
		this.superAffiliate = superAffiliate;
	}
	public Date getLastPayment() {
		return lastPayment;
	}
	public void setLastPayment(Date lastPayment) {
		this.lastPayment = lastPayment;
	}

	public double getAffiliateRate() {
		return affiliateRate;
	}
	public void setAffiliateRate(double affiliateRate) {
		this.affiliateRate = affiliateRate;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public double getDeposit() {
		return deposit;
	}
	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}

	public String getPaymentPeriod() {
		return paymentPeriod;
	}
	public void setPaymentPeriod(String paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

	public double getAffiliateBalance() {
		return affiliateBalance;
	}
	public void setAffiliateBalance(double affiliateBalance) {
		this.affiliateBalance = affiliateBalance;
	}
	public boolean isPaymentDue() {
		return paymentDue;
	}
	public void setPaymentDue(boolean paymentDue) {
		this.paymentDue = paymentDue;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
