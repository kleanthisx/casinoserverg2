package com.magneta.administration.partners.pages;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry.IExternalPage;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.InvalidAuthenticationCredentialsException;

public abstract class Login extends PublicPage implements IExternalPage {
 
	private static final Logger log = LoggerFactory.getLogger(Login.class);
	
	@InjectObject("service:tapestry.globals.HttpServletRequest")
    public abstract HttpServletRequest getRequest();
	
	@Bean
    public abstract ValidationDelegate getDelegate();
	
    public abstract IFieldTracking getCurrentFieldTracking();
    
    @Component(id="errors", type="For",
    		bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
    public abstract ForBean getErrors();
    
    @Component(id="error",
    		bindings={"delegate=currentFieldTracking.errorRenderer"})
    public abstract Delegator getError();
    
    @Component(id="isInError", type="If",
    		bindings={"condition=currentFieldTracking.inError","renderTag=true"})
    public abstract IfBean getIsInError();
	
	@Component(bindings={"value=username","displayName=message:Username", "validators=validators:required"})
	public abstract TextField getUsernameField();
	
	@Component(bindings={"field=component:usernameField"})
	public abstract FieldLabel getUsernameLabel();
	
	@Component(bindings={"value=password","hidden=true","displayName=message:Password", "validators=validators:required"})
	public abstract TextField getPasswordField();
	
	@Component(bindings={"field=component:passwordField"})
	public abstract FieldLabel getpasswordLabel();
	
	@Component(id="loginForm",
			bindings={"listener=listener:onOk", "delegate=beans.delegate", "clientValidationEnabled=true"})
	public abstract Form getLoginForm();
	
	public abstract String getResult();
	public abstract String getUsername();
	public abstract String getPassword();
	
	@Override
	public void prepareForRender(IRequestCycle cycle) {
		getUserAuthenticationService().logoutUser();
	}
	
	@Persist("client:page")
	public abstract String getRedirectUrl();
	public abstract void setRedirectUrl(String url);

    @Override
	public void activateExternalPage(Object[] parameters, IRequestCycle cycle) {
    	if (parameters != null && parameters.length > 0) {
    		setRedirectUrl((String)parameters[0]);
    	}
    }
	
	public void onOk(IRequestCycle cycle) {
		ValidationDelegate delegate = getDelegate();
	     
        if (delegate.getHasErrors()){
            return;
        }
		
		String username = getUsername();
		String password = getPassword();

		UserAuthenticationService authenticationService = getUserAuthenticationService();
		
		try {
			authenticationService.authenticateUser(username, password, 
					new UserTypeEnum[] { UserTypeEnum.CORPORATE_ACCOUNT },
					null);

			if (getRedirectUrl() == null) {
				redirectHome();
             } else {
            	 cycle.forgetPage(this.getPageName());
            	 throw new RedirectException(getRedirectUrl());
             }
		} catch (InvalidAuthenticationCredentialsException e) {
			getDelegate().record(null, getMessages().getMessage("LoginFailed"));
		} catch (ServiceException e) {
			log.error("Error while authenticating user", e);
			getDelegate().record(null, getMessages().getMessage("db-error"));			
		}
	}
	
	public void redirectHome(){
		IEngineService s = getRequestCycle().getInfrastructure().getServiceMap().getService(Tapestry.PAGE_SERVICE);
        
        ILink link = s.getLink(false, "Home");
        
        if (link != null) {
            String redirect = link.getURL();
            throw new RedirectException(redirect);
        }
	}
}
