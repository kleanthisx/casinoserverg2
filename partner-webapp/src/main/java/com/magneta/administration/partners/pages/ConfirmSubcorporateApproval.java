package com.magneta.administration.partners.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class ConfirmSubcorporateApproval extends SecurePage {

	private static final Logger log = LoggerFactory.getLogger(ConfirmSubcorporateApproval.class);
	
	@Persist("client:page")
	public abstract void setUserId(Long id);
	public abstract Long getUserId();
	public abstract void setUsername(String username);
	public abstract String getUsername();
	public abstract boolean isBackofficeApprovalRequired();
	public abstract void setBackofficeApprovalRequired(boolean required);

	@Bean
    public abstract ValidationDelegate getDelegate();
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	public boolean isUserIdNull=false;
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Component(id="confirmForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel"})
	public abstract Form getConfirmForm();
	
	@Component(id="confirmSubmit")
	public abstract Submit getConfirmSubmit();
	
	@Component(id="cancelRemoval")
	public abstract Button getCancelRemoval();
	
	@Override
	public void prepareForRender(IRequestCycle cycle) {
		super.prepareForRender(cycle);
		ValidationDelegate delegate = getDelegate();
		 Messages messages = getMessages();
		 isUserIdNull = false; 
		
		if (getUserId() == null){
			isUserIdNull = true;
			 delegate.record(messages.getMessage("invalid-request"), null);
			 return;
			
		}
		
		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null){
			return;
		}
		PreparedStatement stmt = null;
    	ResultSet res = null;
    	try {
    		stmt = dbConn.prepareStatement(
    				"SELECT users.username, affiliates.require_approval, affiliate_users.affiliate_id" +
    				" FROM affiliates" +
    				" INNER JOIN users ON users.user_id = affiliates.affiliate_id" +
    				" INNER JOIN affiliate_users ON affiliate_users.user_id = affiliates.affiliate_id" +
    				" WHERE affiliates.affiliate_id = ?");
    		
    		stmt.setLong(1, getUserId());    		
    		res = stmt.executeQuery();
    		
    		Long superId = null;
    		if (res.next()){
    			setBackofficeApprovalRequired(res.getBoolean("require_approval"));
    			setUsername(res.getString("username"));
    			superId = res.getLong("affiliate_id");
    		}
    		
    		if(!isBackofficeApprovalRequired() && superId != null) {
    			setBackofficeApprovalRequired(checkIfApprovalIsRequired(dbConn, superId));
    		}
    	} catch (SQLException e) {
    		throw new RuntimeException("Error getting details of the unverified subcorporate.",e);
    	} finally{
    		DbUtil.close(stmt);
    		DbUtil.close(res);
    		DbUtil.close(dbConn);
    	}
    		
	}
	
	private boolean checkIfApprovalIsRequired(Connection conn, Long superId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			stmt = conn.prepareStatement(
					"SELECT affiliate_users.affiliate_id, affiliates.require_approval"+
					" FROM affiliate_users"+
					" INNER JOIN affiliates ON affiliate_users.affiliate_id = affiliates.affiliate_id"+
			" WHERE user_id = ?");

			stmt.setLong(1, superId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				if(!rs.getBoolean("require_approval")) {
					Long affId = rs.getLong("affiliate_id");
					DbUtil.close(rs);
					DbUtil.close(stmt);
					return checkIfApprovalIsRequired(conn, affId);
				}
				
				return true;
			} 
		}catch (SQLException e) {
			throw new RuntimeException("Error while retrieving affiliate's approval flag", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
		}

		return false;
	}
	
	public void onSubmit(IRequestCycle cycle){
		
		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null){
			return;
		}
		PreparedStatement stmt = null;
    	try {
    		stmt = dbConn.prepareStatement(
    				"UPDATE affiliates" +
    				" SET requires_super_approval = FALSE" +
    				(!isBackofficeApprovalRequired()? ", approved = TRUE": "") +
    				" WHERE affiliate_id = ?");
    		
    		stmt.setLong(1, getUserId());    		
    		stmt.executeUpdate();
    		
    	} catch (SQLException e) {
    		
    		this.getConfirmForm().getDelegate().record(null, "Error marking approval status");
    		log.error("Error while approving unverified subcorporate.",e);
    	} finally{
    		DbUtil.close(stmt);
    		DbUtil.close(dbConn);
    	}
    	
		redirectToPage("SubPartnersApproval");
	}

	public void onCancel(IRequestCycle cycle){
		redirectToPage("SubPartnersApproval");
	}


}
