package com.magneta.administration.partners.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.ajax.XTile;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.link.ExternalLink;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.partners.beans.CorporateUserBean;
import com.magneta.administration.partners.beans.TransferBean;
import com.magneta.administration.partners.models.LastTranfersModel;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.PromoTransactionService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.PromoTransactionBean;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

public abstract class TransferFunds extends SecurePage {
	
	private static final Logger log = LoggerFactory.getLogger(TransferFunds.class);
	
    public abstract Double getBalance();
    public abstract void setBalance(Double balance);
    
	@Bean
    public abstract ValidationDelegate getDelegate();
    
	@InjectPage("ConfirmFundsTransfer")
	public abstract ConfirmFundsTransfer getConfirmTransferPage();
	
	@InjectObject("service:casino.common.PromoTransaction")
	public abstract PromoTransactionService getPromoTransactionService();
	
	@InjectObject("service:casino.common.Settings")
	public abstract SettingsService getSettingsService();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporateService();
	
	private LastTranfersModel lastTransferModel;

	public IBasicTableModel getLastTransferModel() {
		return lastTransferModel;
	}

	public abstract Integer getCurrIndex();
	public abstract Integer getCurrIndex2(); 
	
	public abstract CorporateUserBean getCurrUser();
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	public abstract TransferBean getCurrTransfer();
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();
	
	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();
	
	@Component(id="lastTransfers", type="contrib:Table",
			bindings={"source=ognl:lastTransferModel","columns=literal:!Time, !Username, !Type, !Free, !Real",
			"pageSize=10","row=currTransfer"})
	public abstract Table getLastTransfers();
	
	@Component(id="users", type="contrib:Table",
			bindings={"source=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsTableModel(getUser().getId(), @com.magneta.administration.partners.models.CorporateRelationshipsTableModel@USER_RELATIONSHIP)",
			"columns=literal: !UserId, !Username, !FirstName, !LastName, !Balance, !FreeCredits, !clearBalance, !free","pageSize=30","row=currUser","index=currIndex"})
	public abstract Table getUsers();
	
	@Component(id="userTransferForm",
			bindings={"listener=listener:onSubmit","delegate=beans.delegate","clientValidationEnabled=true"})
	public abstract Form getUserTransfersForm();
	
	@Component(id="userSelect",
			bindings={"value=userId","displayName=message:User","model=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsSelectionModel(user.Id,1,false,true)"})
	public abstract PropertySelection getUserSelect();
	
	@Component(id="userSelectLabel",
			bindings={"field=component:userSelect"})
	public abstract FieldLabel getUserSelectLabel();
	
	@Component(id="userAmountLabel",
			bindings={"field=component:userAmount"})
	public abstract FieldLabel getUserAmountLabel();
	
	@Component(id="userAmount",
			bindings={"value=amount","displayName=amountLabel","translator=bean:numTranslator","validators=validators:required"})
	public abstract TextField getUserAmount();
	
	@Component(id="userTransferSubmit")
	public abstract Submit getUserTransferSubmit();
	
	@Component(id="subCorpTransferForm",
			bindings={"listener=listener:onSubmit","delegate=beans.delegate","clientValidationEnabled=true"})
	public abstract Form getSubCorpTransferForm();
	
	@Component(id="subCorpSelect",
			bindings={"value=userId","displayName=message:User",
			"model=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsSelectionModel(user.Id,2,false,true)"})
	public abstract PropertySelection getSubCorpSelect();
	
	@Component(id="subCorpSelectLabel",
			bindings={"field=component:subCorpSelect"})
	public abstract FieldLabel getSubCorpSelectLabel();
	
	@Component(id="subCorpAmountLabel",
			bindings={"field=component:subCorpAmount"})
	public abstract FieldLabel getSubCorpAmountLabel();
	
	@Component(id="subCorpAmount",
			bindings={"value=amount","displayName=amountLabel","translator=bean:numTranslator","validators=validators:required"})
	public abstract TextField getSubCorpAmount();
	
	@Component(id="subCorpDesc",
			bindings={"value=description","displayName=message:Description"})
	public abstract TextField getSubCorpDesc();
	
	@Component(id="subCorpDescLabel",
			bindings={"field=component:subCorpDesc"})
	public abstract FieldLabel getSubCorpDescLabel();
	
	@Component(id="subCorpTransferSubmit")
	public abstract Submit getSubCorpTransferSubmit();
	
	@Component(id ="userBalance",
			bindings={"value=formatAmount(balance)",
			"id='bal_'+user.UserId", "renderTag=true"})
	public abstract Insert getUserBalance();
	
	@Component(id="subCorps", type="contrib:Table",
			bindings={"source=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsTableModel(getUser().getId(), @com.magneta.administration.partners.models.CorporateRelationshipsTableModel@SUB_CORPORATE_RELATIONSHIP)",
			"columns=literal: !UserId, !Username, !FirstName, !LastName, !Balance","pageSize=20","row=currUser","index=currIndex2"})
	public abstract Table getSubCorps();
	
	@Component(id="balance",
			bindings={"value=formatAmount(currUser.Balance)","renderTag=true",
			"id='bal_'+currUser.UserId"})
	public abstract Insert getBalanceField();
	
	@Component(id="freeCredits",
			bindings={"value=formatAmount(currUser.freeCredits)","renderTag=true",
			"id='free_'+currUser.UserId"})
	public abstract Insert getFreeCredits();
	
	@Component(id="userId",
			bindings={"value=currUser.UserId","id='userId_'+userIdIndex",
			"renderTag=true","style='display:none;'"})
	public abstract Insert getUserIdField();
		
	public int getUserIdIndex() {
		int count = 0;
		
		if (this.getCurrIndex() != null) {
			count += this.getCurrIndex();
		}
		
		if (this.getCurrIndex2() != null) {
			if (this.getCurrIndex() != null) {
				count++;
			}
			
			count += this.getCurrIndex2();
		}
		
		return count;
	}

	@Component(id="balanceRefreshTile", type="contrib:XTile",
			bindings={"listener=listener:handleBalanceRefresh","sendName=literal:requestBalances","receiveName=literal:getRefreshedBalances",
			"disableCaching=true"})
	public abstract XTile getBalanceRefreshTile();
	
	@Component(id="clearUserBalanceLink",
			bindings={"page=literal:ConfirmFundsClear","parameters=currUser.UserId"})
	public abstract ExternalLink getClearUserBalanceLink();
	
	@Component(id="freeCreditsLink",
			bindings={"listener=listener:onGiveFree","parameters=currUser.UserId","name=currUser.Username"})
	public abstract DirectLink getFreeCreditsLink();
	
	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);

		lastTransferModel = new LastTranfersModel(getUser());
		setHasSuperCorps(getUser().getSuperCorp() != null && getUser().getSuperCorp() > 0);
		setHasUsers(getUser().isHasUsers());
		setHasSubCorps(getUser().isHasSubCorps());
		
		try {
			UserBalanceBean balanceBean = this.getUserDetailsService().getUserBalance(getUser().getId());
			
			if (balanceBean != null) {
				setBalance(balanceBean.getBalance());
			}
			
			getUser().updateHasUsers(getCorporateService(), getUserDetailsService());
			
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public abstract void setHasUsers(boolean hasUsers);
	public abstract void setHasSubCorps(boolean hasSubCorps);
	public abstract void setHasSuperCorps(boolean hasSuperCorps);
	
	public abstract Long getUserId();
	public abstract double getAmount();
	public abstract String getDescription();

    public void onSubmit(IRequestCycle cycle) {
        if (getDelegate().getHasErrors()){
        	return;
        }
        
        ConfirmFundsTransfer confirm = getConfirmTransferPage();
        confirm.setUserId(getUserId());
        confirm.setAmount(getAmount());
        confirm.setDescription(getDescription());
        cycle.activate(confirm);
    }
    
    public void onGiveFree(IRequestCycle cycle, Long userId) throws ServiceException {
    	if (!getUser().isAllowFreeCredits()){
    		return;
    	}
    	
    	Double minBalance = getSettingsService().getSettingValue("system.promo.free_max_balance", Double.class);
        Integer freeCreditSecsAllowed = getSettingsService().getSettingValue("system.promo.free_secs_time_allowed", Integer.class);
    	
        Connection dbConn = ConnectionFactory.getConnection();

        if (dbConn == null) {
            return;
        }
        
        double promoAmount = 0.0;
        
        try
        {            
            /* Last transfer */
            Long transactionId = null;
            double transferAmount = 0.0;
            double oldBalance = 0.0;
            
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try {
                stmt = dbConn.prepareStatement(
                		"SELECT user_transactions.user_transaction_id," +
                		" user_transactions.amount AS transfer_amount," +
                		" user_transactions.new_balance - user_transactions.amount AS old_balance" +
                		" FROM latest_non_gaming_transactions" +
                		" INNER JOIN user_transactions" +
                		"  ON latest_non_gaming_transactions.transaction_id = user_transactions.user_transaction_id" +
                		"  AND user_transactions.deposit_transfer_id IS NOT NULL" +
                		"  AND user_transactions.amount > 0.0" +
                		" WHERE latest_non_gaming_transactions.user_id = ?" +
                		" AND EXISTS (SELECT game_trans.user_id" +
                		"       FROM latest_gaming_transactions" + 
                		"	    INNER JOIN user_transactions AS game_trans" +
                		"	      ON latest_gaming_transactions.transaction_id=game_trans.user_transaction_id" +
                		"         AND game_trans.new_balance <= ?" +
                		"         AND age(now_utc(), game_trans.transaction_time) < CAST(? AS INTERVAL)" +
                		"       WHERE latest_gaming_transactions.user_id = ?" +
                		"       AND latest_gaming_transactions.transaction_id > latest_non_gaming_transactions.transaction_id)" +
                		" AND NOT EXISTS (SELECT promo_transaction_id" +
                		"                 FROM promo_transactions WHERE user_id = ?" +
                		"                 AND promo_transactions.transaction_time >= user_transactions.transaction_time" +
                		"                 AND promo_transactions.amount > 0.0)");
                
                stmt.setLong(1, userId);
                stmt.setBigDecimal(2, new BigDecimal(minBalance));
                stmt.setString(3, freeCreditSecsAllowed+" seconds");
                stmt.setLong(4, userId);
                stmt.setLong(5, userId);
                
                rs = stmt.executeQuery();
                
                if (rs.next()){
                	transactionId = rs.getLong(1);
                	transferAmount = rs.getDouble(2);
                	oldBalance = rs.getDouble(3);
                }
                
            } finally {
                DbUtil.close(rs);
                DbUtil.close(stmt);
            }
            
            if (transactionId != null){
                double amount = 0.0;
                /*
                 * For full free credits the amount is the
                 * amount of the last transfer
                 */
                if (getUser().isAllowFullFreeCredits()) {
                	amount = transferAmount - oldBalance;
                } else {

                	PreparedStatement slotsPayoutStmt = null;
                	ResultSet slotsPayoutRs = null;
                	try {
                		slotsPayoutStmt = dbConn.prepareStatement(
                				"SELECT SUM(user_transactions.amount * -1.0)"+
                				" FROM game_tables"+
                				" INNER JOIN game_types ON game_types.game_type_id = game_tables.game_type_id"+
                				" AND game_types.game_category_id = 2"+
                				" INNER JOIN user_transactions ON user_transactions.action_table_id = game_tables.table_id"+
                				" AND user_transactions.user_id = game_tables.table_owner"+
                				" AND user_transactions.user_transaction_id > ?"+
                		" WHERE game_tables.table_owner = ?");

                		slotsPayoutStmt.setLong(1, transactionId);
                		slotsPayoutStmt.setLong(2, userId);

                		slotsPayoutRs = slotsPayoutStmt.executeQuery();

                		if (slotsPayoutRs.next()){
                			amount = slotsPayoutRs.getDouble(1);
                		}

                		amount -= oldBalance;

                	} finally {
                		DbUtil.close(slotsPayoutRs);
                		DbUtil.close(slotsPayoutStmt);
                	}
                }
                
                promoAmount = amount * getUser().getFreeCreditsRate();
            }
        } catch (SQLException e){
            log.error("Error while giving free credits to corporate_user.",e);
        } finally{
            DbUtil.close(dbConn);
        }
        
        if (promoAmount > 0.009) {
        	PromoTransactionBean transactionBean = new PromoTransactionBean();
        	transactionBean.setUserId(userId);
        	transactionBean.setAmount(promoAmount);
        	
        	this.getPromoTransactionService().createPromoTransaction(transactionBean);
        	
        	TransferBean bean = new TransferBean();
            bean.setType("Promo");
            bean.setPromoAmount(transactionBean.getAmount());
            bean.setTransferDate(transactionBean.getTransactionTime());
            bean.setUsername(getUsername(transactionBean.getUserId()));
            
            getUser().addTransfer(bean);
        }
        
        //Return the same page to get the correct url.
        redirectToPage("TransferFunds");
    }
    
    public void handleBalanceRefresh(IRequestCycle cycle, String userIdList) {
    	if (getUser() == null || !getUser().isLoggedIn()) {
    		cycle.setListenerParameters(null);
            return;
    	}

        List<Long> userIds;
        
        if (userIdList != null && !userIdList.trim().isEmpty()) {
            String[] uIds = userIdList.trim().split(",");
            
            userIds = new ArrayList<Long>(1 + uIds.length);
            
            userIds.add(getUser().getUserId());

            try {
            	for (String uId : uIds) {
            		userIds.add(Long.parseLong(uId));
            	}
            } catch (NumberFormatException e) {
            	cycle.setListenerParameters(null);
                return;
            }
        } else {
        	userIds = new ArrayList<Long>(1);
        	userIds.add(getUser().getUserId());
        }
        
        List<UserBalanceBean> balances;
		try {
			balances = this.getUserDetailsService().getUserBalances(userIds);
		} catch (ServiceException e) {
			log.error("Error retrieving user balances", e);
			cycle.setListenerParameters(null);
			return;
		}
        
		String[] output = new String[balances.size()];
		int i = 0;
		
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setGroupingUsed(false);
		nf.setParseIntegerOnly(true);
		
		for (UserBalanceBean balance : balances) {
			output[i] = nf.format(balance.getUserId()) +
					"_" +
					getCurrencyParser().formatDouble(balance.getBalance()) +
    				"_" +
    				getCurrencyParser().formatDouble(balance.getPlayCredits());

			i++;
		}
        
        cycle.setListenerParameters(output);
    }

    public String getAmountLabel() {
    	String symbol = getCurrencyParser().getCurrencySymbol(getLocale());
    	return getMsg("Amount") + " " + symbol;	
    }

    public String getBalanceColumnHeader() {
    	String symbol = getCurrencyParser().getCurrencySymbol(getLocale());
		return getMsg("Balance") + "  (" + symbol + ")"; 
	}
}
