package com.magneta.administration.partners.pages;

import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.link.DirectLink;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.administration.partners.pages.reports.ReportOptionsPage;
import com.magneta.administration.partners.reports.ReportGenerationQueue;
import com.magneta.administration.partners.reports.ReportGenerationRequest;
import com.magneta.administration.partners.services.PageMenuService;
import com.magneta.tapestry4.components.pagemenu.PageMenu;
import com.magneta.tapestry4.components.pagemenu.PageMenuItem;

public abstract class MenuListing extends SecurePage {
	
	public abstract ReportGenerationRequest getReport();
	
	@InjectObject("service:casino.partner.GeneratedReportDownload")
	public abstract IEngineService getGeneratedReportDownloadService();
	
	@InjectObject("service:casino.partner.PageMenu")
	public abstract PageMenuService getPageMenuService();
	
	@Component(id="reportDate",
			bindings={"value=formatDate(report.time)"})
		public abstract Insert getReportDate();
	
	@Component(id="reportName",
			bindings={"value=report.reportDesc"})
		public abstract Insert getReportName();
	
	@Component(id="reportsFor", type="For",
			bindings={"source=userReports","value=report","element=literal:tr"})
		public abstract ForBean getReportsFor();
	
	@Component(id ="menuListing", type="magneta:PageMenu",
			bindings={"items=MenuPageItems","cssClass=literal:vr_menu"})
	public abstract PageMenu getMenuListing();

	@Component(id="viewReportLink",
			bindings={"listener=listener:download","parameters={report.UID, report.format}"})
		public abstract DirectLink getViewReportLink();
	
	@Component(id="delReportLink",
			bindings={"listener=listener:delete","parameters=report.UID"})
		public abstract DirectLink getDelReportLink();
	
	public List<PageMenuItem> getVRMenuItems(){
    	return this.getPageMenuService().getVerticalMenuItems(this.getPage().getLocale());
	}
	
	public List<PageMenuItem> getMenuPageItems(){
		for (PageMenuItem item: getVRMenuItems()){
			if (getPageName().equals(item.getTargetPage())){
				return item.getSubItems();
			}
		}
		return null;
	}
	
	public List<ReportGenerationRequest> getUserReports(){
		return ReportGenerationQueue.getInstance().getRepository(getUser().getId());
	}
	
	public void getReport(IRequestCycle cycle, ReportBean report){
		ReportOptionsPage page = (ReportOptionsPage) cycle.getPage(report.getOptionsPage());
		page.setReportDesc(report.getReportName());
		cycle.activate(page);
	}
	
	public void download(IRequestCycle cycle){
		String uID = (String)cycle.getListenerParameters()[0];
		String format = (String)cycle.getListenerParameters()[1];
		
		ServiceParameter[] svcParams = new ServiceParameter[2];
		svcParams[0] = new ServiceParameter("report_uid",uID);
		svcParams[1] = new ServiceParameter("format",format);
			
		throw new RedirectException(getGeneratedReportDownloadService().getLink(true, svcParams).getURL());
	}
	
	public void delete(IRequestCycle cycle,String reportUid){
		ReportGenerationQueue.getInstance().deleteFromRepository(getUser().getId(), reportUid);
	}
	
}
