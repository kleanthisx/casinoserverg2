package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateNicknameException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.casino.services.utils.PhoneUtils;
import com.magneta.tapestry4.callback.RedirectExternalCallback;
import com.magneta.tapestry4.components.ExFieldLabel;

public abstract class EditUser extends SecurePage implements PageBeginRenderListener {
	
	private static final Logger log = LoggerFactory.getLogger(EditUser.class);
	
    @Bean
    public abstract ValidationDelegate getDelegate();

    @InjectPage("change_password")
    public abstract ChangePassword getChangePassword();

    @InjectObject("engine-service:external")
    public abstract IEngineService getExternalService();
    
	@InjectObject("service:casino.common.UserAccess")
	public abstract UserAccessService getUserAccessService();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
    
    @Persist("client:page")
    public abstract Long getUserId();
    public abstract void setUserId(Long userId);
    
    public abstract String getUsername();
    public abstract void setUsername(String username);
    
    
    public abstract String getEmail();
    public abstract String getFirstName();
    public abstract String getLastName();
    public abstract String getMiddleName();
    public abstract String getNickname();
    public abstract String getAddress();
    public abstract String getCountry();
    public abstract String getPhone();
    public abstract String getRegion();
    public abstract String getTown();
    public abstract String getPostCode();
    public abstract String getTimeZone();
   
    public abstract void setEmail(String email);
    public abstract void setFirstName(String firstName);
    public abstract void setLastName(String lastName);
    public abstract void setMiddleName(String middleName);
    public abstract void setNickname(String nickname);
    public abstract void setAddress(String address);
    public abstract void setCountry(String country);
    public abstract void setPhone(String phone);
    public abstract void setRegion(String region);
    public abstract void setTown(String town);
    public abstract void setPostCode(String postCode);
    public abstract void setTimeZone(String timeZone);
     
    public abstract IFieldTracking getCurrentFieldTracking();
    
    @Component(id="errors", type="For",
    		bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
    public abstract ForBean getErrors();
    
    @Component(id="error",
    		bindings={"delegate=currentFieldTracking.errorRenderer"})
    public abstract Delegator getError();
    
    @Component(id="isInError", type="If",
    		bindings={"condition=currentFieldTracking.inError","renderTag=true"})
    public abstract IfBean getIsInError();
    
    @Component(id="registerForm",
    		bindings={"success=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
    		"clientValidationEnabled=true"})
    public abstract Form getRegisterForm();
  
    @Component(id="changeLink",
			bindings={"listener=listener:onPasswordChange"})
	public abstract DirectLink getChangeLink();
    
    @Component(id="username",
    		bindings={"value=username"})
    public abstract Insert getUsernameField();
    
    @Component(id="email",
    		bindings={"value=email","displayName=literal:E-mail","validators=validators:email"})
    public abstract TextField getEmailField();
    
    @Component(id="emailLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:email"})
    public abstract ExFieldLabel getEmailLabel();
    
    @Component(id="firstName",
    		bindings={"value=firstName","displayName=message:first-name","validators=validators:required"})
    public abstract TextField getFirstNameField();
    
    @Component(id="firstNameLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:firstName"})
    public abstract ExFieldLabel getFirstNameLabel();
    
    @Component(id="middleName",
    		bindings={"value=middleName","displayName=literal:Middle Name"})
    public abstract TextField getMiddleNameField();
    
    @Component(id="middleNameLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:middleName"})
    public abstract ExFieldLabel getMiddleNameLabel();
    
    @Component(id="lastName",
    		bindings={"value=lastName","displayName=message:last-name","validators=validators:required"})
    public abstract TextField getLastNameField();
    
    @Component(id="lastNameLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:lastName"})
    public abstract ExFieldLabel getLastNameLabel();
    
    @Component(id="nickname",
    		bindings={"value=nickname","displayName=message:Nickname"})
    public abstract TextField getNicknameField();
    
    @Component(id="nicknameLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:nickname"})
    public abstract ExFieldLabel getNicknameLabel();
    
    @Component(id="town",
    		bindings={"value=town","displayName=message:Town"})
    public abstract TextField getTownField();
    
    @Component(id="townLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:town"})
    public abstract ExFieldLabel getTownLabel();
    
    
    @Component(id="postCode",
    		bindings={"value=postCode","displayName=message:postal-code"})
    public abstract TextField getPostCodeField();
    
    @Component(id="postCodeLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:postCode"})
    public abstract ExFieldLabel getPostCodeLabel();
    
    @Component(id="region",
    		bindings={"value=region","displayName=message:Region"})
    public abstract TextField getRegionField();
    
    @Component(id="regionLabel", type="magneta:ExFieldLabel",
    	bindings={"field=component:region"})
    public abstract ExFieldLabel getRegionLabel();
    
    @Component(id="address",
    		bindings={"value=address","displayName=message:Address"})
    public abstract TextArea getAddressField();
    
    @Component(id="addressLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:address"})
    public abstract ExFieldLabel getAddressLabel();
    
    @Component(id="country",
    		bindings={"value=country","displayName=message:Country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel(true)",
    		"validators=validators:required,minLength=2[%no-country-selected]"})
    public abstract PropertySelection getCountryField();
    
    @Component(id="countryLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:country"})
    public abstract ExFieldLabel getCountryLabel();
    
    @Component(id="zone",
    		bindings={"value=timeZone","displayName=message:TimeZone","model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel()",
    		"validators=validators:minLength=2"})
    public abstract PropertySelection getZone();
    
    @Component(id="zoneLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:zone"})
    public abstract ExFieldLabel getZoneLabel();
    
    @Component(id="phone",
    		bindings={"value=phone","displayName=message:Phone"})
    public abstract TextField getPhoneField();
    
    @Component(id="phoneLabel", type="magneta:ExFieldLabel",
    		bindings={"field=component:phone"})
    public abstract ExFieldLabel getPhoneLabel();
    
    @Component(id="updateSubmit")
    public abstract Submit getUpdateSubmit();
    
    @Component(id="cancelButton")
    public abstract Button getCancelButton(); 
    
    @Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
    	if (params != null && params.length > 0){
    		this.setUserId((Long)params[0]);
    	}
    	
		if (this.getUserId() == null) {
			redirectToPage("AccessDenied");
		}

		if (!this.getUserAccessService().canAccess(getUserId())) {
			redirectToPage("AccessDenied");
		}
		
		if (getUser().getUserId().equals(getUserId())) {
			redirectToPage("AccessDenied");
		}
	}
    
    @Override
    public void pageBeginRender(PageEvent event) {

    	if (!this.getUserAccessService().canAccess(getUserId())) {
    		this.getDelegate().record(null, "Access Denied");
    		return;
    	}


    	if (!event.getRequestCycle().isRewinding()) {

    		UserDetailsBean userDetails;
    		try {
    			userDetails = this.getUserDetailsService().getUser(getUserId());
    		} catch (ServiceException e) {
    			throw new RuntimeException(e);
    		}

    		setUsername(userDetails.getUserName());
    		setEmail(userDetails.getEmail());
    		setFirstName(userDetails.getFirstName());
    		setMiddleName(userDetails.getMiddleName());
    		setLastName(userDetails.getLastName());
    		setNickname(userDetails.getNickName());
    		setCountry(userDetails.getCountryCode());
    		setPhone(userDetails.getPhone());
    		setRegion(userDetails.getRegion());
    		setTown(userDetails.getTown());
    		setPostCode(userDetails.getPostalCode());
    		setAddress(userDetails.getPostalAddress());
    		setTimeZone(userDetails.getTimezone());
    	}
    }
    
    public void onSubmit(IRequestCycle cycle) {

    	ValidationDelegate delegate = getDelegate();

    	if (delegate.getHasErrors())
    		return;

    	UserDetailsBean userDetails;
		try {
			userDetails = getUserDetailsService().getUser(getUserId());
		} catch (ServiceException e1) {
			delegate.record(null, getMsg("db-error"));
			return;
		}
    	
    	userDetails.setEmail(getEmail());
    	userDetails.setFirstName(getFirstName());
    	userDetails.setLastName(getLastName());
    	userDetails.setMiddleName(getMiddleName());
    	userDetails.setNickName(getNickname());
    	userDetails.setPostalAddress(getAddress());
    	userDetails.setCountryCode(getCountry());
    	
    	if (PhoneUtils.isValidPhone(getPhone())) {
        	userDetails.setPhone(PhoneUtils.normalizePhone(getPhone()));  
        } else {
        	getDelegate().record(getPhoneField(), getMsg("phone-error"));
        	return;
        }    

    	userDetails.setRegion(getRegion());
    	userDetails.setTown(getTown());
    	userDetails.setPostalCode(getPostCode());
    	userDetails.setTimezone(getTimeZone());

    	try {
			this.getUserDetailsService().updateUser(userDetails);
    	} catch (DuplicateEmailException e) {
    		delegate.record(getEmailField(), getMsg("email-exists"));
    		return;
    	} catch (DuplicateNicknameException e) {
    		delegate.record(getNicknameField(), getMsg("nickname-exists"));
    		return;
    	} catch (DuplicateUsernameException e) {
    		log.error("Database error while updating user", e);
    		delegate.record(null, getMsg("username-exists"));
    		return;
    	} catch (DatabaseException e) {
    		log.error("Database error while updating user", e);
    		delegate.record(null, getMsg("db-error"));
    		return;
		} catch (ServiceException e) {
			log.error("Database error while updating user", e);
    		delegate.record(null, getMsg("db-error"));
    		return;
		} catch (ConditionalUpdateException e) {
			delegate.record(null, getMsg("db-error"));
			return;
		}

    	cycle.forgetPage(getPageName());
    	new RedirectExternalCallback("UserInformation", new Object[] { getUserId()}).performCallback(cycle);
    }

    public void onCancel(IRequestCycle cycle) {
    	cycle.forgetPage(getPageName());
    	new RedirectExternalCallback("UserInformation", new Object[] { getUserId()}).performCallback(cycle);
    }
    
    @InjectPage("ResetUserPassword")
    public abstract ResetUserPassword getResetUserPassword();
    
	public void onPasswordChange(IRequestCycle cycle){
		ResetUserPassword changePwPage = getResetUserPassword();
        changePwPage.setUserId(getUserId());
        cycle.activate(changePwPage);
    }
}
