/**
 * 
 */
package com.magneta.administration.partners.pages;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry.PageNotFoundException;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.event.PageDetachListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.event.PageValidateListener;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author anarxia
 *
 */
public abstract class ExceptionPage extends PublicPage implements PageValidateListener, PageDetachListener {

	private static final Logger log = LoggerFactory.getLogger(ExceptionPage.class);
	
    @InjectObject("service:tapestry.globals.HttpServletResponse")
    public abstract HttpServletResponse getResponse();
    
    @InitialValue("false")
    public abstract boolean isPageNotFound();
    public abstract void setPageNotFound(boolean pageNotFound);

    public abstract void setNotFoundURI(String s);
    
    @Override
    public String getPageTitle() {
        if (isPageNotFound()) {
            return getMsg("page-not-found");
        }

        return getMsg("page-title");
    }
    
    public void setException(Throwable t)
    {
        if(t instanceof PageNotFoundException ||
                t.getCause() instanceof PageNotFoundException)
        {
            setPageNotFound(true);
        } else if (t instanceof NoClassDefFoundError ||
                t.getCause() instanceof NoClassDefFoundError) {
            setPageNotFound(true);
        }

        if (!isPageNotFound()) {
            log.error("an exception occured", t);
        }
    }

    @Override
	public void pageValidate(PageEvent event)  {
        if (isPageNotFound()) {
            WebRequest request = event.getRequestCycle().getInfrastructure().getRequest();
            WebResponse response = event.getRequestCycle().getInfrastructure().getResponse();

            response.setStatus(404);
            
            setNotFoundURI(request.getRequestURI());
        }
    }
}
