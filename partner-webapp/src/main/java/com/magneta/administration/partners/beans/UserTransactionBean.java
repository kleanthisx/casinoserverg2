package com.magneta.administration.partners.beans;

import java.io.Serializable;
import java.util.Date;

public class UserTransactionBean implements Serializable {
    
    private static final long serialVersionUID = 8224156905566449055L;
    private Date date;
    private double debitAmount;
    private double creditAmount;
    private String description;
    private String username;
    private Long transactionId;
    private String type;
    private String user;
    
    public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public double getCreditAmount() {
        return creditAmount;
    }
    
    public void setCreditAmount(double creditAmount) {
        this.creditAmount = creditAmount;
    }
    
    public double getDebitAmount() {
        return debitAmount;
    }
    
    public void setDebitAmount(double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public Date getDate() {
        return date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
}
