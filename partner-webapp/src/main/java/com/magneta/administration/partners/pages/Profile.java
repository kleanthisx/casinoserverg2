package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.link.ExternalLink;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.utils.PhoneUtils;
import com.magneta.tapestry4.components.ExFieldLabel;

public abstract class Profile extends SecurePage implements PageBeginRenderListener {
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
			public abstract ForBean getErrors();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
			public abstract Delegator getError(); 

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
			public abstract IfBean getIsInError();

	@Component(id="registerForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel",
			"delegate=beans.delegate","clientValidationEnabled=true"})
			public abstract Form getRegisterForm();

	@Component(id="username",
			bindings={"value=username"})
			public abstract Insert getUsernameField();

	@Component(id="passwordMsg",
			bindings={"value=message:Password"})
			public abstract Insert getPasswordMsg();

	@Component(id="changeLink",
			bindings={"page=literal:ChangePassword"})
			public abstract ExternalLink getChangeLink();

	@Component(id="email",
			bindings={"value=email","displayName=literal:E-mail","validators=validators:email"})
			public abstract TextField getEmailField();

	@Component(id="emailLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:email"})
			public abstract ExFieldLabel getEmailLabel();

	@Component(id="firstName",
			bindings={"value=firstName","displayName=message:first-name","validators=validators:required"})
			public abstract TextField getFirstNameField();

	@Component(id="firstNameLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:firstName"})
			public abstract ExFieldLabel getFirstNameLabel();

	@Component(id="middleName",
			bindings={"value=middleName","displayName=literal:Middle Name"})
			public abstract TextField getMiddleNameField();

	@Component(id="middleNameLabel",
			bindings={"field=component:middleName"})
			public abstract FieldLabel getMiddleNameLabel();

	@Component(id="lastName",
			bindings={"value=lastName","displayName=message:last-name","validators=validators:required"})
			public abstract TextField getLastNameField();

	@Component(id="lastNameLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:lastName"})
			public abstract ExFieldLabel getLastNameLabel();

	@Component(id="town",
			bindings={"value=town","displayName=message:Town"})
			public abstract TextField getTownField();

	@Component(id="townLabel",
			bindings={"field=component:town"})
			public abstract FieldLabel getTownLabel();

	@Component(id="address",
			bindings={"value=address","displayName=message:Address"})
			public abstract TextArea getAddressField();

	@Component(id="addressLabel",
			bindings={"field=component:address"})
			public abstract FieldLabel getAddressLabel();

	@Component(id="postCode",
			bindings={"value=postCode","displayName=message:postal-code"})
			public abstract TextField getPostCodeField();

	@Component(id="postCodeLabel",
			bindings={"field=component:postCode"})
			public abstract FieldLabel getPostCodeLabel();

	@Component(id="region",
			bindings={"value=region","displayName=message:Region"})
			public abstract TextField getRegionField();

	@Component(id="regionLabel",
			bindings={"field=component:region"})
			public abstract FieldLabel getRegionLabel();

	@Component(id="country",
			bindings={"value=country","displayName=message:Country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel(true)",
	"validators=validators:required,minLength=2[%no-country-selected]"})
	public abstract PropertySelection getCountryField();

	@Component(id="countryLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:country"})
			public abstract ExFieldLabel getCountryLabel();

	@Component(id="zone",
			bindings={"value=timeZone","displayName=message:TimeZone","model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel()",
	"validators=validators:minLength=2"})
	public abstract PropertySelection getZone();

	@Component(id="zoneLabel", type="magneta:ExFieldLabel",
			bindings={"field=component:zone"})
			public abstract ExFieldLabel getExFieldLabel();

	@Component(id="phone",
			bindings={"value=phone","displayName=message:Phone"})
			public abstract TextField getPhoneField();

	@Component(id="phoneLabel",
			bindings={"field=component:phone"})
			public abstract FieldLabel getPhoneLabel();

	@Component(id="registerSubmit")
	public abstract Submit getRegisterSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Bean
	public abstract ValidationDelegate getDelegate();

	@InjectPage("ChangePassword")
	public abstract ChangePassword getChangePassword();

	public abstract String getUsername();
	public abstract void setUsername(String username);
	
	public abstract String getEmail();
	public abstract void setEmail(String email);
	
	public abstract String getFirstName();
	public abstract void setFirstName(String firstName);
	
	public abstract String getLastName();
	public abstract void setLastName(String lastName);
	
	public abstract String getMiddleName();
	public abstract void setMiddleName(String middleName);
	
	public abstract String getAddress();
	public abstract void setAddress(String address);
	
	public abstract String getCountry();
	public abstract void setCountry(String country);
	
	public abstract String getPhone();
	public abstract void setPhone(String phone);
	
	public abstract String getRegion();
	public abstract void setRegion(String region);
	
	public abstract String getTown();
	public abstract void setTown(String town);
	
	public abstract String getPostCode();
	public abstract void setPostCode(String postCode);
	
	public abstract String getTimeZone();
	public abstract void setTimeZone(String timeZone);

	@Override
	public void pageBeginRender(PageEvent evt) {
		
		UserDetailsBean userDetails;
		try {
			userDetails = this.getUserDetailsService().getUser(getUser().getId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}		

		setUsername(userDetails.getUserName());
		setEmail(userDetails.getEmail());
		setFirstName(userDetails.getFirstName());
		setMiddleName(userDetails.getMiddleName());
		setLastName(userDetails.getLastName());
		setTown(userDetails.getTown());
		setCountry(userDetails.getCountryCode());
		setAddress(userDetails.getPostalAddress());

		if (userDetails.getTimezone() != null) {
			setTimeZone(userDetails.getTimezone());
		}
		setPostCode(userDetails.getPostalCode());
		setRegion(userDetails.getRegion());
		setPhone(userDetails.getPhone());
	}

	public void onSubmit(IRequestCycle cycle) {

		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors())
			return;
		
		UserDetailsBean userDetails;
		try {
			userDetails = this.getUserDetailsService().getUser(getUser().getId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		userDetails.setEmail(getEmail());
		userDetails.setFirstName(getFirstName());
		userDetails.setLastName(getLastName());
		userDetails.setMiddleName(getMiddleName());
		userDetails.setPostalAddress(getAddress());
		userDetails.setCountryCode(getCountry());
		userDetails.setRegion(getRegion());
		userDetails.setTown(getTown());
		userDetails.setPostalCode(getPostCode());
		userDetails.setTimezone(getTimeZone());

		if (PhoneUtils.isValidPhone(getPhone())) {
			userDetails.setPhone(PhoneUtils.normalizePhone(getPhone()));  
		} else {
			getDelegate().record(getPhoneField(), getMsg("phone-error"));
			return;
		}    

		try {
			this.getUserDetailsService().updateUser(userDetails);
		} catch (DatabaseException e) {
			delegate.record(null, getMsg("db-error"));
			return;
		} catch (ConditionalUpdateException e) {
			delegate.record(null, getMsg("db-error"));
			return;
		} catch (ServiceException e) {
			delegate.record(null, getMsg("db-error"));
			return;
		}
		
		getUser().updateDetails(userDetails);
		redirectHome();
	}

	public int getUserType(){
		return getUser().getUserType();
	}
	
	public void onCancel(IRequestCycle cycle) {
		redirectHome();
	}
}
