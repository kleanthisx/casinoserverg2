package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;
import com.magneta.administration.partners.beans.EarningTransactionBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class AffiliateEarningsTableModel implements IBasicTableModel {

	private Long corporateId;
	private boolean includeSubs;
	private Date minDate;
	private Date maxDate;
	
	public AffiliateEarningsTableModel(Long corporateId, boolean includeSubs, Date minDate, Date maxDate){
		this.corporateId = corporateId;
	    this.includeSubs = includeSubs;
	    this.minDate = minDate;
		this.maxDate = maxDate;
	}
	
    @Override
	public Iterator<EarningTransactionBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<EarningTransactionBean> it = null;
        
        if (dbConn == null) {
        	throw new RuntimeException("Out of database connections");
        }
        
        try
        {   
            String sql = 
            	"SELECT transaction_date AS date, users.username AS username, amount, details, initiator.username AS initiator" +
				" FROM affiliate_earnings_transactions"+
				" INNER JOIN users ON users.user_id=affiliate_id" +
				" LEFT OUTER JOIN users AS initiator ON initiator.user_id=initiator_user_id" +
				" WHERE (affiliate_id=?" +
				(includeSubs?" OR affiliate_id IN (SELECT affiliate_users.user_id FROM affiliate_users WHERE affiliate_users.affiliate_id=?)":"")+
				" )" +
				(minDate != null? " AND ((transaction_date AT TIME ZONE 'UTC') >= ?::timestamp)" : "")+
				(maxDate != null? " AND ((transaction_date AT TIME ZONE 'UTC') <= ?::timestamp)" : "")+
				" ORDER BY date" +
				" LIMIT ? OFFSET ?";

            statement = dbConn.prepareStatement(sql);
            
            int i = 1;
            
            statement.setLong(i++, corporateId);
            
            if (includeSubs) {
            	statement.setLong(i++, corporateId);
            }
            
            if (minDate != null){
            	statement.setTimestamp(i++, new Timestamp(minDate.getTime()));
            }
            
            if (maxDate != null){
            	statement.setTimestamp(i++, new Timestamp(maxDate.getTime()));
            }

            statement.setInt(i++, limit);
            statement.setInt(i, offset);

            res = statement.executeQuery();
            
            List<EarningTransactionBean> rows = new ArrayList<EarningTransactionBean>();
            EarningTransactionBean currRow;
            while (res.next()){
            	currRow = new EarningTransactionBean();
                currRow.setUsername(res.getString("username"));
                currRow.setAmount(res.getDouble("amount"));
                currRow.setDate(DbUtil.getTimestamp(res,"date"));
                currRow.setInitiator(res.getString("initiator"));
                currRow.setDetails(res.getString("details"));
                rows.add(currRow);
            }
            it = rows.iterator();
        } catch (SQLException e) {
        	throw new RuntimeException("Error while retrieving affiliate earnings.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return it;
    }

    @Override
	public int getRowCount() {
    	int count = 0;
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn == null){
            throw new RuntimeException("Out of database connections");
        }
        
        try {            
            String sql = 
            	"SELECT COUNT(*)" +
				" FROM affiliate_earnings_transactions"+
				" WHERE (affiliate_id=?" +
				(includeSubs?" OR affiliate_id IN (SELECT affiliate_users.user_id FROM affiliate_users WHERE affiliate_users.affiliate_id=?)":"")+
				" )" +
				(minDate != null? " AND ((transaction_date AT TIME ZONE 'UTC') >= ?::timestamp)" : "")+
				(maxDate != null? " AND ((transaction_date AT TIME ZONE 'UTC') <= ?::timestamp)" : "");

            statement = dbConn.prepareStatement(sql);
            int i = 1;
            statement.setLong(i++, corporateId);
            
            if (includeSubs) {
            	statement.setLong(i++, corporateId);
            }
            
            if (minDate != null){
            	statement.setTimestamp(i++, new Timestamp(minDate.getTime()));
            }
            
            if (maxDate != null){
            	statement.setTimestamp(i++, new Timestamp(maxDate.getTime()));
            }
            
            res = statement.executeQuery();

            while (res.next()){
                count += res.getInt(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving earnings count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return count;
    }
}
