package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class AllLevelSubCorpsSelectionModel implements IPropertySelectionModel {

	public static final int SUB_CORPORATE_RELATIONSHIP = 2;

	private List<Long> ids;
	private List<String> usernames;


	public AllLevelSubCorpsSelectionModel(Long affiliateId){
		Connection dbConn = ConnectionFactory.getReportsConnection();
		ids = new ArrayList<Long>();
		usernames = new ArrayList<String>(); 

		ids.add(new Long(-1));
		usernames.add("All");


		if (dbConn != null){

			PreparedStatement statement = null;
			ResultSet res = null;

			ArrayList<Long> list = new ArrayList<Long>();
			list.add(affiliateId);
			try {
				while(list.size() != 0) {
					try {
						statement = null;
						res = null;

						statement = dbConn.prepareStatement(
								"SELECT users.user_id, users.username"+
								" FROM affiliate_users"+
								" INNER JOIN users ON affiliate_users.user_id = users.user_id" +
								" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id AND affiliates.approved = TRUE" +
								" WHERE affiliation = 3"+
						" AND affiliate_users.affiliate_id = ?");

						statement.setLong(1, list.get(0));

						res = statement.executeQuery();

						while (res.next()){
							list.add(res.getLong("user_id"));
							ids.add(res.getLong("user_id"));
							usernames.add(res.getString("username"));					
						}
						list.remove(0);
					} finally {
						DbUtil.close(res);
						DbUtil.close(statement);
					}
				}

			} catch (SQLException e) {
				throw new RuntimeException("Error while getting approved sub-account list.",e);
			} finally {
				DbUtil.close(dbConn);
			}
		}
	}

	@Override
	public String getLabel(int arg0) {
		return usernames.get(arg0);
	}

	@Override
	public Object getOption(int arg0) {
		return ids.get(arg0);
	}

	@Override
	public int getOptionCount() {
		return ids.size();
	}

	@Override
	public String getValue(int arg0) {
		return String.valueOf(ids.get(arg0));
	}

	@Override
	public boolean isDisabled(int arg0) {
		return false;
	}

	@Override
	public Object translateValue(String arg0) {
		return Long.parseLong(arg0);
	}

}