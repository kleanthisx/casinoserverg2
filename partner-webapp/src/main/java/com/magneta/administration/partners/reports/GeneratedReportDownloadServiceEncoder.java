package com.magneta.administration.partners.reports;

import org.apache.tapestry.engine.ServiceEncoder;
import org.apache.tapestry.engine.ServiceEncoding;
import org.apache.tapestry.services.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.reporting.ReportGenerationServiceEncoder;

public class GeneratedReportDownloadServiceEncoder implements ServiceEncoder {

	private static final Logger log = LoggerFactory.getLogger(GeneratedReportDownloadServiceEncoder.class);
	
	private String reportPath = "genreport";
	
	@Override
	public void decode(ServiceEncoding encoding) {
		String path = encoding.getServletPath();
		
		/* Detect if its our own url */
		if (!path.equalsIgnoreCase("/"+ reportPath)){
			return;
		}

		String pathInfo = encoding.getPathInfo();
		
		int dotx = pathInfo.lastIndexOf('.');
        if (dotx < 0){ 
        	return;
        }

        String format = pathInfo.substring(dotx + 1);
        String report = pathInfo.substring(1, dotx);
		
		encoding.setParameterValue(ServiceConstants.SERVICE, GeneratedReportDownloadService.SERVICE_NAME);
		encoding.setParameterValue("report_uid", report);
		encoding.setParameterValue(ReportGenerationServiceEncoder.FORMAT_PARAMETER, format);
		log.debug("ReportDownloadServiceEncoder: \"" + encoding.getParameterValue("report_uid") + "\", Format: \"" + encoding.getParameterValue(ReportGenerationServiceEncoder.FORMAT_PARAMETER) + "\"");
	}

	@Override
	public void encode(ServiceEncoding encoding) {
		String service = encoding.getParameterValue(ServiceConstants.SERVICE);

		if (!service.equals(GeneratedReportDownloadService.SERVICE_NAME)) {
			return;
		}
		
		String report = encoding.getParameterValue("report_uid");
		String format = encoding.getParameterValue(ReportGenerationServiceEncoder.FORMAT_PARAMETER);
		
		if (format == null) {
			format = "html";
		}
		
		if (report == null) {
			return;
		}
		
		StringBuilder buf = new StringBuilder("/");
		buf.append(reportPath);
		buf.append('/');
		buf.append(report);
		buf.append('.');
		buf.append(format.toLowerCase());
		
		encoding.setServletPath(buf.toString());
		encoding.setParameterValue(ServiceConstants.SERVICE, null);
		encoding.setParameterValue("report_uid", null);
		encoding.setParameterValue(ReportGenerationServiceEncoder.FORMAT_PARAMETER, null);
	}

	public void setReportPath(String path) {
		this.reportPath = path;
	}
	
	public String getReportPath() {
		return reportPath;
	}
}
