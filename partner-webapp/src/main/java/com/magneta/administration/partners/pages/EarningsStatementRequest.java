/**
 * 
 */
package com.magneta.administration.partners.pages;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

public abstract class EarningsStatementRequest extends SecurePage {

	public abstract IFieldTracking getCurrentFieldTracking();

	@InitialValue("initialMinDate")
	public abstract Date getMinDate();
	@InitialValue("new java.util.Date()")
	public abstract Date getMaxDate();

	@Bean
	public abstract ValidationDelegate getDelegate();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
			public abstract ForBean getErrorsDelegator();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
			public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
			public abstract IfBean getIsInError();


	@Component(id="requestForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate",
	"clientValidationEnabled=true"})
	public abstract Form getRequestForm();

	@Component(id="userLabel",
			bindings={"field=component:user"})
			public abstract FieldLabel getUserLabel();

	@Component(id="user",
			bindings={"value=selectedUser","displayName=message:User","model=ognl:new com.magneta.administration.partners.models.SubCorporateSelectionModel(user.Id,true,false,true,false,false)"})
			public abstract PropertySelection getUserField();

	@Component(id="fromDate",
			bindings={"value=minDate","displayName=message:From","translator=translator:date,pattern=dd/MM/yyyy",
	"validators=validators:required"})
	public abstract DatePicker getFromDate();

	@Component(id="fromDateLabel",
			bindings={"field=component:fromDate"})
			public abstract FieldLabel getFromDateLabel();

	@Component(id="toDate",
			bindings={"value=maxDate","displayName=message:To","translator=translator:date,pattern=dd/MM/yyyy",
	"validators=validators:required"})
	public abstract DatePicker getToDate();

	@Component(id="toDateLabel",
			bindings={"field=component:toDate"})
			public abstract FieldLabel getToDateLabel();

	@Component(id="okSubmit")
	public abstract Submit getOkSubmit();

	@Component(id="cancelSubmit")
	public abstract Submit getCancelSubmit();

	@InjectPage("EarningsStatement")
	public abstract EarningsStatement getStatementPage();

	public abstract Long getSelectedUser();

	public void onSubmit(IRequestCycle cycle){
		Date minDate = normalizeDate(getMinDate());
		Date maxDate = normalizeDate(getMaxDate());

		int countDays=(int)((maxDate.getTime()- minDate.getTime())/ (1000*60*60*24));

		if (getDelegate().getHasErrors()){
			return;
		}

		if(countDays>31){
			getDelegate().record(getMsg("month-error"), null);
			return;
		}	

		if(getMinDate().after(getMaxDate())){
			getDelegate().record(getMsg("date-error"),null);
			return;
		}

		//NORMALIZE DATES
		{
			int offset = TimeZone.getDefault().getOffset(minDate.getTime());

			Calendar calendar = Calendar.getInstance(getUser().getTimeZone(), getLocale());
			calendar.setTimeInMillis(minDate.getTime() + offset);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			minDate = new Date(calendar.getTimeInMillis());
		}

		{
			int offset = TimeZone.getDefault().getOffset(maxDate.getTime());

			Calendar calendar = Calendar.getInstance(getUser().getTimeZone(), getLocale());
			calendar.setTimeInMillis(maxDate.getTime()+offset);
			calendar.set(Calendar.HOUR_OF_DAY, 24);
			calendar.set(Calendar.MINUTE, 0);
			calendar.add(Calendar.MILLISECOND, -1);
			maxDate = new Date(calendar.getTimeInMillis());
		}

		EarningsStatement stmt = getStatementPage();
		stmt.setMinDate(minDate);
		stmt.setMaxDate(maxDate);
		stmt.setSelectedUser(getSelectedUser());
		cycle.activate(stmt);
	}

	public Date getInitialMinDate() {
		Calendar fromCal = Calendar.getInstance();
		fromCal.setTime(getMaxDate());
		fromCal.roll(Calendar.MONTH, -1);
		return fromCal.getTime();	
	}
	
	public void onCancel(IRequestCycle cycle){
		redirectHome();
	}
}
