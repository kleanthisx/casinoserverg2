package com.magneta.administration.partners.beans;

import java.io.Serializable;
import java.util.Date;

public class CorporateUserBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long userId;
	private int relation;
	private String username;
	private String firstName;
	private String lastName;
	private double transferAmount;
	private double balance;
	private double freeCredits;
	private boolean active;
	private String superUsername;
	private Date registerDate;
	private boolean requiresSuperApproval;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public double getFreeCredits() {
		return freeCredits;
	}
	public void setFreeCredits(double freeCredits) {
		this.freeCredits = freeCredits;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public int getRelation() {
		return relation;
	}
	public void setRelation(int relation) {
		this.relation = relation;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuperUsername() {
		return superUsername;
	}
	public void setSuperUsername(String superUsername) {
		this.superUsername = superUsername;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public boolean isSuperApprovalRequired() {
		return requiresSuperApproval;
	}
	public void setSuperApprovalRequired(boolean approval) {
		this.requiresSuperApproval = approval;
	}
}
