package com.magneta.administration.partners.pages.reports;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

public abstract class CorporateOptions extends CorporateTransactionsOptions{ 
	
	@Component(id="subCorpSelect",
			bindings={"value=userId","displayName=message:SubCorporate","model=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsSelectionModel(user.Id,2,false,false,true,true)",
			"validators=validators:min=0[%no-corporate-selection]"})
	public abstract PropertySelection getSubCorpSelect();
	
	@Component(id="subCorpSelectLabel",
			bindings={"field=component:subCorpSelect"})
	public abstract FieldLabel getSubCorpSelectLabel();

}
