package com.magneta.administration.partners.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.partners.beans.UserTransactionBean;
import com.magneta.administration.partners.models.CorporateTransfersTableModel;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class TransfersStatement extends SecurePage {

	@InitialValue("-1")
	@Persist("client:page")
	public abstract void setUserId(Long userId);
	public abstract Long getUserId();

	@Persist("client:page")
	public abstract void setMinDate(Date minDate);
	public abstract Date getMinDate();
	@Persist("client:page")
	public abstract void setMaxDate(Date maxDate);	
	public abstract Date getMaxDate();
	
	public abstract void setTotalIn(double totalIn);
	public abstract void setTotalOut(double totalOut);

	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract UserTransactionBean getCurrTransaction(); 
	
	@Component(id="transfers", type="contrib:Table",
			bindings={"source=ognl:corporateTransfersTableModel",
			"columns=literal:!Date, !Username, !Description, !CreditAmount, !DebitAmount","pageSize=30","rowsClass=ognl:beans.evenOdd.next",
			"initialSortOrder=false","row=currTransaction","persist=literal:client:page"})
	public abstract Table getTransfers();
	
	public CorporateTransfersTableModel getCorporateTransfersTableModel() { 
		return new CorporateTransfersTableModel(getUser().getId(), getUserId(), getMinDate(), getMaxDate());
	}
	
	@Component(id="date",
			bindings={"value=formatDate(currTransaction.Date)"})
	public abstract Insert getDate();
	
	@Component(id="creditAmount",
			bindings={"value=(currTransaction.CreditAmount == 0.0 ? '-' : formatAmount(currTransaction.CreditAmount))"})
	public abstract Insert getCreditAmount();
	
	@Component(id="debitAmount",
			bindings={"value=(currTransaction.DebitAmount == 0.0 ? '-' : formatAmount(currTransaction.DebitAmount))"})
	public abstract Insert getDebitAmount();
	
	@Component(id="formattedFromDate",
			bindings={"value=(minDate != null ? getMsg('From') + ' '+formatDate(minDate) : '')"})
	public abstract Insert getFormattedFromDate();
	
	@Component(id="formattedToDate",
			bindings={"value=(maxDate != null ? getMsg('Until') +' '+ formatDate(maxDate) : '')"})
	public abstract Insert getFormattedToDate();
	
	@Component(id="totalIn",
			bindings={"value=formatAmount(totalIn)"})
	public abstract Insert getTotalInField();
	
	@Component(id="totalOut",
			bindings={"value=formatAmount(totalOut)"})
	public abstract Insert getTotalOutField();
	
	@Component(id="total",
			bindings={"value=formatAmount(getTotalIn()-getTotalOut())"})
	public abstract Insert getTotal();

    @InjectObject("service:tapestry.globals.HttpServletRequest")
    public abstract HttpServletRequest getRequest();
	
	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		
        logReportGeneration("Transfer Statement (Page)", getMinDate(), getMaxDate(), getRequest().getRemoteAddr(), null);
		
		Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn == null){
            return;
        }
        
        try {            
            String sql = 
            	"SELECT SUM (CASE WHEN amount < 0.0 THEN amount * -1.0 ELSE 0 END), SUM (CASE WHEN amount > 0.0 THEN amount ELSE 0 END)"+
            	" FROM corporate_deposit_transfers"+
            	" INNER JOIN users ON users.user_id = corporate_deposit_transfers.user_id"+
            	" WHERE corporate_deposit_transfers.transfer_user = ?"+
            	(getMaxDate() != null? " AND ((transfer_date AT TIME ZONE 'UTC') <= ?::timestamp)" : "")+
            	(getMinDate() != null? " AND ((transfer_date AT TIME ZONE 'UTC') >= ?::timestamp)" : "")+
            	(getUserId() > 0 ? " AND corporate_deposit_transfers.user_id = ?" : "")+
            	" UNION ALL"+
            	" SELECT SUM (CASE WHEN amount > 0.0 THEN amount ELSE 0 END), SUM (CASE WHEN amount < 0.0 THEN amount ELSE 0 END)"+
            	" FROM corporate_deposit_transfers"+
            	" INNER JOIN users ON users.user_id = corporate_deposit_transfers.transfer_user"+
            	" WHERE corporate_deposit_transfers.user_id = ?"+
	            (getMaxDate() != null? " AND ((transfer_date AT TIME ZONE 'UTC') <= ?::timestamp)" : "")+
	        	(getMinDate() != null? " AND ((transfer_date AT TIME ZONE 'UTC') >= ?::timestamp)" : "")+
	        	(getUserId() > 0 ? " AND corporate_deposit_transfers.transfer_user = ?" : "");

            statement = dbConn.prepareStatement(sql);
            int i = 1;
            statement.setLong(i++, getUser().getId());
            if (getMaxDate() != null){
            	statement.setTimestamp(i++, new Timestamp(getMaxDate().getTime()));
            }
            if (getMinDate() != null){
            	statement.setTimestamp(i++, new Timestamp(getMinDate().getTime()));
            }
            if (getUserId() > 0){
            	statement.setLong(i++, getUserId());
            }
            statement.setLong(i++, getUser().getId());
            if (getMaxDate() != null){
            	statement.setTimestamp(i++, new Timestamp(getMaxDate().getTime()));
            }
            if (getMinDate() != null){
            	statement.setTimestamp(i++, new Timestamp(getMinDate().getTime()));
            }
            if (getUserId() > 0){
            	statement.setLong(i++, getUserId());
            }
            res = statement.executeQuery();

            double totalIn = 0.0;
            double totalOut = 0.0;
            while (res.next()){
                totalIn += res.getDouble(1);
                totalOut += res.getDouble(2);
            }
            setTotalIn(Math.abs(totalIn));
            setTotalOut(Math.abs(totalOut));
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving transfers statement totals.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
	}
}