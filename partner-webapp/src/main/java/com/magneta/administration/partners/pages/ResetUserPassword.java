package com.magneta.administration.partners.pages;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.tapestry4.callback.RedirectExternalCallback;

public abstract class ResetUserPassword extends SecurePage {

	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();

	@Bean
	public abstract ValidationDelegate getDelegate();

	public abstract String getOldPassword();
	public abstract String getNewPassword();
	public abstract String getNewPasswordConfirm();

	public boolean isUserIdNull = false;

	@Persist("client:page")
	public abstract void setUserId(Long userId);
	public abstract Long getUserId();

	public abstract IFieldTracking getCurrentFieldTracking();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();

	@Component(id="passwordChangeForm",
			bindings={"delegate=beans.delegate","clientValidationEnabled=true","listener=listener:onSubmit",
	"cancel=listener:onCancel"})
	public abstract Form getPasswordChangeForm();

	@Component(id="username",
			bindings={"value=getUsername(getUserId())"})
	public abstract Insert getUsernameField();

	@Component(id="newPasswordLabel",
			bindings={"field=component:newPassword"})
	public abstract FieldLabel getNewPasswordLabel();

	@Component(id="newPassword",
			bindings={"value=newPassword","hidden=true","disabled=false","displayName=message:NewPassword",
	"validators=validators:required,minLength=6"})
	public abstract TextField getNewPasswordField();

	@Component(id="newPasswordConfirmLabel",
			bindings={"field=component:newPasswordConfirm"})
	public abstract FieldLabel getNewPasswordConfirmLabel();

	@Component(id="newPasswordConfirm",
			bindings={"value=newPasswordConfirm","hidden=true","disabled=false","displayName=message:NewPasswordConfirm",
	"validators=validators:required"})
	public abstract TextField getNewPasswordConfirmField();

	@Component(id="okSubmit")
	public abstract Submit getOkSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		if (params != null && params.length > 0){
			this.setUserId((Long)params[0]);
		}

		if (this.getUserId() == null) {
			throw new RuntimeException("Bad request");
		}
	}

	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		ValidationDelegate delegate = getDelegate();
		Messages messages = getMessages();
		isUserIdNull = false; 

		if (getUserId() == null){
			isUserIdNull = true;
			delegate.record(messages.getMessage("invalid-request"), null);
			return;

		}
	}

	public void onSubmit(IRequestCycle cycle){

		ValidationDelegate delegate = getDelegate();

		if (delegate.getHasErrors()) {
			return;
		}  //password checks 
		else if (getNewPassword().toLowerCase().equals(getUser().getUsername().toLowerCase())){
			delegate.record(getNewPasswordField(), getMessages().getMessage("username-equal-password"));
			return;
		}

		Messages messages = getMessages();

		if (!getNewPassword().equals(getNewPasswordConfirm())) {
			delegate.record(getNewPasswordConfirmField(), messages.getMessage("PasswordConfirmUnequal"));
			return;
		}

		try {
			this.getUserRegistrationService().updatePassword(getUserId(), getNewPassword());
		} catch (DatabaseException e) {
			delegate.record(null, messages.getMessage("db-error"));
			return;
		} catch (ServiceException e) {
			delegate.record(null, messages.getMessage("unknown-error"));
			return;
		}
		
		cycle.forgetPage(this.getPageName());
		new RedirectExternalCallback("UserInformation", new Object[] {getUserId()}).performCallback(cycle);		
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.forgetPage(this.getPageName());
		new RedirectExternalCallback("UserInformation", new Object[] {getUserId()}).performCallback(cycle);		

	}
}
