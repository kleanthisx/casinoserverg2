package com.magneta.administration.partners;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;

public class SystemProperties {

	public static final String APP_NAME = "Partner Web-Application";

    public static boolean logReportsGeneration() throws ServiceException{
    	SettingsService settingsService = ServiceLocator.getService(SettingsService.class);
    	
    	return settingsService.getSettingValue("system.log_reports_generation", Boolean.class);
    }
    
    public static boolean realMoneyMode() throws ServiceException {
    	SettingsService settingsService = ServiceLocator.getService(SettingsService.class);
    	
    	return !settingsService.getSettingValue("system.fun.enable", Boolean.class);
    }
    
    public static String getSingleCountry() throws ServiceException { 
    	SettingsService settingsService = ServiceLocator.getService(SettingsService.class);
    	
    	return settingsService.getSettingValue("system.single_country", String.class);
    }
    
    public static boolean useReportQueue() throws ServiceException {
    	SettingsService settingsService = ServiceLocator.getService(SettingsService.class);
    	
    	return settingsService.getSettingValue("system.reports.use_report_queue", Boolean.class);
    }
    
}
