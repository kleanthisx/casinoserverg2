package com.magneta.administration.partners.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.DirectLink;

import com.magneta.administration.partners.beans.CorporateUserBean;
import com.magneta.administration.partners.models.UnverifiedSubCorporatesTableModel;

public abstract class SubPartnersApproval extends SecurePage {
	
	public abstract CorporateUserBean getCurrCorp();
	
	@Component(id="subCorps", type="contrib:Table",
			bindings={"source=ognl:unverifiedSubCorporatesTableModel",
			"columns=literal:!Username,!SuperUsername,!RegisterDate,!Approve","row=currCorp"})
	public abstract Table getSubCorps();
	
	public UnverifiedSubCorporatesTableModel getUnverifiedSubCorporatesTableModel() {
		return new UnverifiedSubCorporatesTableModel(getUser().getId());
	}
	
	@Component(id="registerDate",
			bindings={"value=formatDate(currCorp.registerDate)"})
	public abstract Insert getRegisterDate();
	
	@Component(id="verifyLink",
			bindings={"listener=listener:onVerifySubCorp","parameters={currCorp.userId}"})
	public abstract DirectLink getVerifyLink();
	
	@InjectPage("ConfirmSubcorporateApproval")
    public abstract ConfirmSubcorporateApproval getConfirmSubcorporateApproval();
	
	public void onVerifySubCorp(IRequestCycle cycle,Long userId) {
    	ConfirmSubcorporateApproval approveSubCorp = getConfirmSubcorporateApproval();
    	approveSubCorp.setUserId(userId);
    	cycle.activate(approveSubCorp);
    }
	
}
