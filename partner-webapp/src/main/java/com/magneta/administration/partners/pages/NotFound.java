/**
 * 
 */
package com.magneta.administration.partners.pages;

import java.text.MessageFormat;

import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.web.WebRequest;

/**
 * @author Nicos
 *
 */
public abstract class NotFound extends PublicPage implements PageBeginRenderListener {
	
    private String requestURL = "";
    
    public String getNotFoundMessage() {
    	return new MessageFormat(getMsg("not-found-msg")).format(new Object[]{requestURL});
    }
    
    @Override
	public void pageBeginRender(PageEvent event) {
        
        WebRequest request = event.getRequestCycle().getInfrastructure().getRequest();
        
        Object url = request.getAttribute("javax.servlet.error.request_uri");
        
        if (url != null) {
            this.requestURL = (String)url;
        } else {
            this.requestURL = "";
        }
        
    }
}
