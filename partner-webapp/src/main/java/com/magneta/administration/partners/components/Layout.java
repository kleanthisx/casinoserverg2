/**
 * 
 */
package com.magneta.administration.partners.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry.BaseComponent;
import org.apache.tapestry.IAsset;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.annotations.Asset;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;

import com.magneta.Config;
import com.magneta.administration.partners.pages.PublicPage;
import com.magneta.administration.partners.services.PageMenuService;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.tapestry4.components.pagemenu.PageMenu;
import com.magneta.tapestry4.components.pagemenu.PageMenuItem;

/**
 * @author anarxia
 *
 */
public abstract class Layout extends BaseComponent {

	@InjectObject("service:casino.common.UserAuthentication")
	public abstract UserAuthenticationService getUserAuthenticationService();
	
	@InjectObject("service:casino.partner.PageMenu")
	public abstract PageMenuService getPageMenuService();
    
    @Asset("context:/css/style.css")
    public abstract IAsset getStylesheet();
    
    @InjectObject("service:tapestry.globals.HttpServletRequest")
    public abstract HttpServletRequest getRequest();
    
    @Component(type="magneta:PageMenu", bindings={"items=VRMenuItems", "cssClass=literal:vr_menu", "behaviorCssClass=literal:mbv"})
	public abstract PageMenu getVerticalMenu();
	
	@Component(type="magneta:PageMenu", bindings={"items=HRMenuItems", "cssClass=literal:hr_menu"})
	public abstract PageMenu getHorizontalMenu();
	
	
	public boolean getDisplayMenu() {
		if (getPage() instanceof PublicPage) {
            return ((PublicPage)getPage()).getDiplayMenu();
        }

        return false;
	}
    
    public String getFullPageTitle() {
        if (getPage() instanceof PublicPage) {
            return "Partner: " + ((PublicPage)getPage()).getPageTitle();
        } 
        
        return "Partner";
    }
    
    public void redirectToPage(String page) {
        IEngineService s = this.getPage().getRequestCycle().getInfrastructure().getServiceMap().getService(Tapestry.PAGE_SERVICE);
    
        ILink link = s.getLink(false, page);
    
        if (link != null) {
            String redirect = link.getURL();
            throw new RedirectException(redirect);
        }
    }
    
    public void onLogout(IRequestCycle cycle) {
    	getUserAuthenticationService().logoutUser();
        redirectToPage("Login");
    }
    
    public List<PageMenuItem> getVRMenuItems(){
    	return this.getPageMenuService().getVerticalMenuItems(this.getPage().getLocale());
	}
	
	public List<PageMenuItem> getHRMenuItems(){
		return this.getPageMenuService().getHorizontalMenuItems(this.getPage().getLocale());
	}
    
    public String getMsg(String msg) {
        return this.getMessages().getMessage(msg);
    }
    
    public boolean isDebugEnabled() {
    	return Config.getBoolean("server.development", false);
    }
}
