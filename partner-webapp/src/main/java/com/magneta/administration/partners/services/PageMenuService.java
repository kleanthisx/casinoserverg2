package com.magneta.administration.partners.services;

import java.util.List;
import java.util.Locale;

import com.magneta.tapestry4.components.pagemenu.PageMenuItem;

public interface PageMenuService {
	List<PageMenuItem> getHorizontalMenuItems(Locale locale);
	List<PageMenuItem> getVerticalMenuItems(Locale locale);
}