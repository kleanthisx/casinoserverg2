package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.partners.beans.AffiliateBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class AffiliatesTableModel implements IBasicTableModel {
	
    private final Boolean approvedFlag;
    private final Long superCorp;
    
    /**
     * @param showApproved
     * @param superCorp The id of the super affiliate of the list of affiliates to show. A value of 0 will show only top
     *  level affiliates, while a negative value will show all affiliates.
     */    
    public AffiliatesTableModel(Boolean approvedFlag, Long superCorp) {
        this.approvedFlag = approvedFlag;
        if(superCorp == null || superCorp <= 0) {
        	this.superCorp = null;
        } else{
        	this.superCorp = superCorp;
        }
    }
    
    @Override
	public Iterator<AffiliateBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<AffiliateBean> it = null;
        
        if (dbConn == null){
			return null;
		}
        
        try
        {   
            
            String sql = 
                "SELECT users.user_id, username, users.is_active, email, first_name, last_name, register_date, affiliate_rate,"+
            	" affiliate_earnings.earnings, approved, user_balance.balance, payment_period,"+
            	" MAX(transaction_date) as last_payment, (now_utc() - (COALESCE(MAX(transaction_date),register_date) + INTERVAL '1 day') > payment_period AND earnings > 0.0) as payment_due," +
            	" affiliate_users.affiliate_id AS super_affiliate"+
            	" FROM affiliates"+
            	" INNER JOIN affiliate_earnings ON affiliates.affiliate_id=affiliate_earnings.affiliate_id" +
            	" LEFT OUTER JOIN affiliate_earnings_transactions ON affiliate_earnings_transactions.affiliate_id = affiliates.affiliate_id"+
            	" INNER JOIN users ON affiliates.affiliate_id = users.user_id" +
            	" AND users.closed = FALSE"+
            	" INNER JOIN user_balance ON users.user_id = user_balance.user_id" +
            	" LEFT OUTER JOIN affiliate_users ON affiliate_users.user_id = affiliates.affiliate_id";
            	
            	if (superCorp == null) {
            		sql += " WHERE affiliate_users.affiliate_id IS NULL";
            	} else {
            		sql += " WHERE affiliate_users.affiliate_id = ?";
            	}
        
            	if (approvedFlag != null) {
            		sql += " AND approved = ?";
            	}
            	
            	sql += 
            		" GROUP BY users.user_id, username, users.is_active, email, first_name, last_name, register_date, affiliate_rate,"+
            		" earnings, approved, balance, payment_period," +
            		" super_affiliate";
            	

                sql += " ORDER BY users.username ASC LIMIT ? OFFSET ?";
            
                statement = dbConn.prepareStatement(sql);
                int i = 1;

                if (superCorp != null) {
                	statement.setLong(i++, superCorp);
            	}
                
                if (approvedFlag != null) {
                	statement.setBoolean(i++, approvedFlag);
                }
                
                statement.setInt(i++, limit);
                statement.setInt(i, offset);

                res = statement.executeQuery();
                
                ArrayList<AffiliateBean> myArr = new ArrayList<AffiliateBean>();
                AffiliateBean currRow;
                
                while (res.next()){
                    currRow = new AffiliateBean();
                    
                    currRow.setId(res.getLong("user_id"));
                    currRow.setUsername(res.getString("username"));
                    currRow.setEmail(res.getString("email"));
                    currRow.setFirstName(res.getString("first_name"));
                    currRow.setLastName(res.getString("last_name"));
                    currRow.setRegisterDate(DbUtil.getTimestamp(res, "register_date"));

                    currRow.setAffiliateRate(res.getDouble("affiliate_rate"));
                    currRow.setAffiliateBalance(res.getDouble("earnings"));
                    currRow.setApproved(res.getBoolean("approved"));
                    currRow.setDeposit(res.getDouble("balance"));
                    currRow.setPaymentPeriod(res.getString("payment_period"));
                    currRow.setLastPayment(DbUtil.getTimestamp(res, "last_payment"));
                    currRow.setPaymentDue(res.getBoolean("payment_due"));
                    currRow.setSuperAffiliate(res.getLong("super_affiliate"));
                    currRow.setActive(res.getBoolean("is_active"));
                    
                    myArr.add(currRow);
                }
               
                it = myArr.iterator();
                
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving affiliates list.",e);
        } finally {
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
        int count = 0;
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {            
            String sql = 
                "SELECT count(*)"+
                " FROM affiliates" +
                " INNER JOIN users ON affiliates.affiliate_id = users.user_id" +
                " LEFT OUTER JOIN affiliate_users ON affiliate_users.user_id = affiliates.affiliate_id"+
                " AND users.closed = FALSE";
	        	
	        	if (superCorp == null) {
            		sql += " WHERE affiliate_users.affiliate_id IS NULL";
            	} else {
            		sql += " WHERE affiliate_users.affiliate_id = ?";
            	}
	        	
	        	if (approvedFlag != null){
	        		sql += " AND approved = ?";
	        	}
            
                statement = dbConn.prepareStatement(sql);
                int i = 1;
                if (superCorp != null) {
                	statement.setLong(i++, superCorp);
            	}
                
                if (approvedFlag != null) {
                	statement.setBoolean(i++, approvedFlag);
                }

                res = statement.executeQuery();
                
                if (res.next()){
                    count = res.getInt(1);
                }
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving affiliates count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }
    
}
