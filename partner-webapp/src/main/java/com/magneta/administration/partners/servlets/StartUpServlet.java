package com.magneta.administration.partners.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.administration.partners.reports.ReportGenerationQueue;
import com.magneta.casino.reports.ReportUtil;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;
import com.magneta.tapestry4.servlets.AppServlet;

public class StartUpServlet extends AppServlet {

	private static final Logger log = LoggerFactory.getLogger(StartUpServlet.class);
	
    private static final long serialVersionUID = 1L;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	
    	ReportUtil.init(ServiceLocator.getService(CurrencyParser.class),
				ServiceLocator.getService(UsernameService.class));
    	
    	try {
			ReportGenerationQueue.init();
		} catch (ServiceException e) {
			throw new ServletException(e);
		}
    	
        Thread t = new Thread(ReportGenerationQueue.getInstance()); 
        ReportGenerationQueue.getInstance().setContext(this.getServletContext());
        if (!ReportGenerationQueue.getInstance().clearResources()){
        	log.error("Error while cleaning up reports resources.");
        }
        t.setDaemon(true);
        t.start();
    }
}