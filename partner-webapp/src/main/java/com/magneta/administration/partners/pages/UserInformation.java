package com.magneta.administration.partners.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.link.ExternalLink;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserLoginsService;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.UserLoginBean;
import com.magneta.casino.services.beans.WebLoginBean;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class UserInformation extends SecurePage implements PageBeginRenderListener {

	@InjectObject("service:casino.common.UserLogin")
	public abstract UserLoginsService getUserLoginsService();

	@InjectObject("service:casino.common.WebLogin")
	public abstract WebLoginsService getWebLoginsService();

	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();

	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporatesService();

	@InjectObject("service:casino.common.CorporateEarnings")
	public abstract CorporateEarningsService getCorporateEarningsService();

	@InjectObject("service:casino.common.Country")
	public abstract CountryService getCountryService();
	
	@InjectObject("service:casino.common.UserAccess")
	public abstract UserAccessService getUserAccessService();

	public abstract Long getUserId();
	public abstract void setUserId(Long userId);
	
	public abstract UserDetailsBean getUserDetails();
	public abstract void setUserDetails(UserDetailsBean userDetails);
	
	public abstract double getBalance();
	public abstract void setBalance(double balance);

	public abstract double getAffiliateRate();
	public abstract void setAffiliateRate(double affiliateRate);


	public abstract void setLastLoginLocations(List<UserLoginBean> logins);

	public abstract void setWebLastLogin(List<WebLoginBean> login);

	public abstract void setEarnings(double earnings);
	public abstract double getEarnings();

	public abstract void setRounds(int rounds);
	public abstract void setBets(double bets);
	public abstract void setWins(double wins);
	public abstract void setJackpotWins(double jackpotWins);

	public UserLoginBean currLogin;
	public WebLoginBean currWebLogin;

	@Bean
	public abstract ValidationDelegate getDelegate();

	public abstract IFieldTracking getCurrentFieldTracking();

	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();

	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();

	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();

	@Component(id="username",
			bindings={"value=userDetails.userName"})
	public abstract Insert getUsernameField();

	@Component(id="nickname",
			bindings={"value=userDetails.nickname"})
	public abstract Insert getNicknameField();

	@Component(id="firstName",
			bindings={"value=userDetails.firstName"})
	public abstract Insert getFirstNameField();

	@Component(id="middleName",
			bindings={"value=userDetails.middleName"})
	public abstract Insert getMiddleNameField();

	@Component(id="lastName",
			bindings={"value=userDetails.lastName"})
	public abstract Insert getLastNameField();

	@Component(id="registerDate",
			bindings={"value=formatDate(userDetails.registeredDate)"})
	public abstract Insert getRegisterDate();

	@Component(id="email",
			bindings={"value=userDetails.email"})
	public abstract Insert getEmailField();

	@Component(id="phone",
			bindings={"value=userDetails.phone"})
	public abstract Insert getPhoneField();

	@Component(id="region",
			bindings={"value=userDetails.region"})
	public abstract Insert getRegionField();

	@Component(id="affiliateRate",
			bindings={"value=formatAmount(getAffiliateRate() * 100)"})
	public abstract Insert getAffiliateRateField();

	@Component(id="town",
			bindings={"value=userDetails.town"})
	public abstract Insert getTownField();

	@Component(id="balance",
			bindings={"value=formatCurrency(balance)"})
	public abstract Insert getBalanceField();

	@Component(id="address",
			bindings={"value=userDetails.address"})
	public abstract Insert getAddressField();

	@Component(id="postalCode",
			bindings={"value=userDetails.postalCode"})
	public abstract Insert getPostalCodeField();

	@Component(id="country",
			bindings={"value=country"})
	public abstract Insert getCountryField();

	@Component(id="status",
			bindings={"value=status"})
	public abstract Insert getStatusField();

	@Component(id="editPartnerLink",
			bindings={"page=literal:EditPartner","parameters={userId}"})
	public abstract ExternalLink getEditPartnerLink();

	@Component(id="editUserLink",
			bindings={"page=literal:EditUser","parameters={userId}"})
	public abstract ExternalLink getEditUserLink();

	@Component(id="changeLink",
			bindings={"page=literal:ResetUserPassword","parameters={userId}"})
	public abstract ExternalLink getChangeLink();

	@Component(id="loginsList", type="For",
			bindings={"source=LastLoginLocations","value=currLogin","renderTag=true"})
	public abstract ForBean getLoginsList();

	@Component(id="webLoginsList", type="For",
			bindings={"source=WebLastLogin","value=currWebLogin","renderTag=true"})
	public abstract ForBean getWebLoginsList();

	@Component(id="userActivateMsg",
			bindings={"value=(userDetails.getIsActive() ? getMsg('Deactivate') : getMsg('Activate'))"})
	public abstract Insert getUserActivateMsg();

	@Component(id="userActivateLink",
			bindings={"listener=listener:onChangeActive","parameters={userId,userDetails.getIsActive()}"})
	public abstract DirectLink getUserActivateLink();

	@Component(id="closeLink",
			bindings={"listener=listener:onClose","parameters={userId}"})
	public abstract DirectLink getPCloseLink();

	public String logins(UserLoginBean bean) {
		return formatDate(bean.getLoginDate()) + " - (" + bean.getLoginIp() + ")  <br/>" + bean.getMacAddess() ;
	}

	public String webLogins(WebLoginBean bean) {
		return formatDate(bean.getLoginDate()) + " - " + bean.getApplicationName() + " - " +bean.getClientIp() ;
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)) {    	
			setUserId((Long)params[0]);
    	}
		
		if (getUserId() == null) {
			redirectToPage("AccessDenied");
		}

		if (!getUserAccessService().canAccess(getUserId())) {
			redirectToPage("AccessDenied");
		}
	}

	@Override
	public void pageBeginRender(PageEvent event) {
		ValidationDelegate delegate = getDelegate();
		Messages messages = getMessages();

		if (getUserId() == null) {
			delegate.record(null, messages.getMessage("invalid-request"));
			return;
		}
		
		if (!getUserAccessService().canAccess(getUserId())) {
			redirectToPage("AccessDenied");
		}

		UserDetailsBean userDetails;
		try {
			userDetails = getUserDetailsService().getUser(getUserId());
		} catch (ServiceException e1) {
			delegate.record(null, messages.getMessage("invalid-request"));
			return;
		}
		
		if (userDetails == null) {
			delegate.record(null, messages.getMessage("invalid-request"));
			return;
		}
		
		setUserDetails(userDetails);

		if (getUserDetails().getUserTypeEnum() == UserTypeEnum.CORPORATE_ACCOUNT) {
			CorporateBean corporateBean;
			try {
				corporateBean = this.getCorporatesService().getCorporate(getUserId());
			} catch (ServiceException e) {
				throw new RuntimeException("Error getting corporate information", e);
			}
			CorporateEarningsBean earningsBean;
			try {
				earningsBean = this.getCorporateEarningsService().getEarnings(getUserId());
			} catch (ServiceException e) {
				throw new RuntimeException("Error getting corporate information", e);
			}

			setAffiliateRate(corporateBean.getAffiliateRate());
			setEarnings(earningsBean.getEarnings());
		}
		
		UserBalanceBean balanceBean;
		try {
			balanceBean = this.getUserDetailsService().getUserBalance(getUserId());
		} catch (ServiceException e) {
			throw new RuntimeException("Error getting user balance", e);
		}

		setBalance(balanceBean.getBalance());

		Connection dbConn = ConnectionFactory.getConnection();
		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}

		PreparedStatement statement4 = null;
		ResultSet res4 = null;

		try {
			statement4 = dbConn.prepareStatement(
					"SELECT SUM(rounds) AS rounds, SUM(bets) AS bets, SUM(wins) AS wins, SUM(jackpot_wins) AS jackpot_wins"+
							" FROM game_period_amounts" +
							" WHERE user_id = ?"+
					" AND age(period_date) <= '1 mon'");

			statement4.setLong(1, getUserId());

			res4 = statement4.executeQuery();

			if(res4.next()) {
				setRounds(res4.getInt("rounds"));
				setBets(res4.getDouble("bets"));
				setWins(res4.getDouble("wins"));
				setJackpotWins(res4.getDouble("jackpot_wins"));
			}

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving user details.",e);
		} finally {
			DbUtil.close(res4);
			DbUtil.close(statement4);
			DbUtil.close(dbConn);
		}

		try { 
			ServiceResultSet<UserLoginBean> userLogins = getUserLoginsService().getLogins(getUserId(), 5, 0); 
			if(userLogins != null) {
				setLastLoginLocations(userLogins.getResult());	
			} 			
		} catch (ServiceException e) {
			throw new RuntimeException("Error while getting user logins");
		}

		try {
			ServiceResultSet<WebLoginBean> webLogins = getWebLoginsService().getLogins(getUserId(), 5, 0);
			if(webLogins != null){
				setWebLastLogin(webLogins.getResult());
			}
		} catch (ServiceException e) {
			throw new RuntimeException("Error while getting web logins");
		}

	}

	@InjectPage("ConfirmUserStateChange")
	public abstract ConfirmUserStateChange getStateChangeConfirmPage();

	public void onChangeActive(IRequestCycle cycle,Long userID,boolean activate) {
		ConfirmUserStateChange stateChangeConfirm = getStateChangeConfirmPage();
		stateChangeConfirm.setUserID(userID);
		stateChangeConfirm.setActivate(!activate);
		cycle.activate(stateChangeConfirm);
	}

	@InjectPage("ConfirmUserAccountClose")
	public abstract ConfirmUserAccountClose getUserAccountClosePage();

	public void onClose(IRequestCycle cycle,Long userID) {
		ConfirmUserAccountClose stateChangeConfirm = getUserAccountClosePage();
		stateChangeConfirm.setUserID(userID);
		cycle.activate(stateChangeConfirm);
	}

	public String getStatus() {
		if (getUserDetails().getIsClosed()) {
			return getMessages().getMessage("account-closed");
		}
		if (getUserDetails().getIsActive()) {
			return getMessages().getMessage("Active");
		}
		return getMessages().getMessage("Inactive");
	}

	public String getCountry() throws ServiceException {
		CountryBean country = this.getCountryService().getCountry(getUserDetails().getCountryCode());
		
		return country.getName();
	}
}
