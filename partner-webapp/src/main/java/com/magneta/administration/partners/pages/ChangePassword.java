/**
 * 
 */
package com.magneta.administration.partners.pages;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.InvalidParameterException;

public abstract class ChangePassword extends SecurePage {
	
	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();
	
    @Bean
    public abstract ValidationDelegate getDelegate();
    
    public abstract String getOldPassword();
    public abstract String getNewPassword();
    public abstract String getNewPasswordConfirm();
    
    public abstract IFieldTracking getCurrentFieldTracking();
    
    @Component(id="errors", type="For",
    		bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
    public abstract ForBean getErrors();
    
    @Component(id="error",
    		bindings={"delegate=currentFieldTracking.errorRenderer"})
    public abstract Delegator getError();
    
    @Component(id="isInError", type="If",
    		bindings={"condition=currentFieldTracking.inError","renderTag=true"})
    public abstract IfBean getIsInError();
    
    @Component(id="passwordChangeForm",
    		bindings={"delegate=beans.delegate","clientValidationEnabled=true","success=listener:onSubmit",
    		"cancel=listener:onCancel"})
    public abstract Form getPasswordChangeForm();
    
    @Component(id="oldPasswordLabel",
    		bindings={"field=component:oldPassword"})
    public abstract FieldLabel getOldPasswordLabel();
    
    @Component(id="oldPassword",
    		bindings={"value=oldPassword","disabled=false","hidden=true","displayName=message:OldPassword","validators=validators:required"})
    public abstract TextField getOldPass();
      
    @Component(id="newPasswordLabel",
    		bindings={"field=component:newPassword"})
    public abstract FieldLabel getNewPasswordLabel();
    
    @Component(id="newPassword",
    		bindings={"value=newPassword","hidden=true","disabled=false","displayName=message:NewPassword",
    		"validators=validators:required,minLength=6"})
    public abstract TextField getNewPass();
    
    @Component(id="newPasswordConfirmLabel",
    		bindings={"field=component:newPasswordConfirm"})
    public abstract FieldLabel getNewPasswordConfirmLabel();
    
    @Component(id="newPasswordConfirm",
    		bindings={"value=newPasswordConfirm","hidden=true","disabled=false","displayName=message:NewPasswordConfirm",
    		"validators=validators:required,match=newPassword"})
    public abstract TextField getNewPassConfirm();
    
    @Component(id="okSubmit")
    public abstract Submit getOkSubmit();
    
    @Component(id="cancelButton")
    public abstract Button getCancelButton();
       
    public void onSubmit(IRequestCycle cycle) throws ServiceException{
        ValidationDelegate delegate = getDelegate();
     
        if (delegate.getHasErrors()){
            return;
        }
        Messages messages = getMessages();
        
        if (!getUserRegistrationService().verifyPassword(getUser().getUserId(), getOldPassword())) {
        	delegate.record(getOldPass(), messages.getMessage("InvalidPassword"));
            return;
        } else if (!getNewPassword().equals(getNewPasswordConfirm())){
            delegate.record(getNewPassConfirm(), messages.getMessage("PasswordConfirmUnequal"));
            return;
        }
        
        try {
        	this.getUserRegistrationService().updatePassword(getUser().getId(), getNewPassword());
        	redirectHome();
        } catch (DatabaseException e) {
        	delegate.record(null, messages.getMessage("db-error"));
        } catch (InvalidParameterException e) {
        	delegate.record(getNewPass(), "Password too weak");
        } catch (ServiceException e) {
        	delegate.record(null, "Unknown error");
        }
    }
    
    public void onCancel(IRequestCycle cycle) {
    	redirectHome();
    }
}
