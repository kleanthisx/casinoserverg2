package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.partners.beans.UserTransactionBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class CorporateUserTransfersTableModel implements IBasicTableModel {

	private final Long corporateId;
	private final Long userId;
	private final Date minDate;
	private final Date maxDate;
	
	public CorporateUserTransfersTableModel(Long corporateId, Date minDate, Date maxDate) {
		this(corporateId, null, minDate, maxDate);
	}
	
	public CorporateUserTransfersTableModel(Long corporateId, Long userId, Date minDate, Date maxDate){
		this.corporateId = corporateId;
		
		if (userId != null && userId <= 0) {
			this.userId = null;
		} else {
			this.userId = userId;
		}
	    this.minDate = minDate;
	    this.maxDate = maxDate;
	}
	
    @Override
	public Iterator<UserTransactionBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<UserTransactionBean> it = null;
        
        if (dbConn == null) {
        	throw new RuntimeException("Out of database connections");
        }
        
        try
        {   
            String sql = 
            	"SELECT transfer_date, users.username, amount, transfer_desc"+
            	" FROM corporate_deposit_transfers"+
            	" INNER JOIN affiliate_users ON affiliate_users.affiliate_id = corporate_deposit_transfers.transfer_user"+
            	" AND affiliate_users.user_id = corporate_deposit_transfers.user_id"+
            	" AND affiliation = 1"+
            	" INNER JOIN users ON users.user_id = corporate_deposit_transfers.user_id"+
            	" WHERE corporate_deposit_transfers.transfer_user = ?"+
            	(maxDate != null? " AND ((transfer_date AT TIME ZONE 'UTC') <= ?::timestamp AT TIME ZONE 'UTC')" : "")+
            	(minDate != null? " AND ((transfer_date AT TIME ZONE 'UTC') >= ?::timestamp AT TIME ZONE 'UTC')" : "")+
            	(userId != null ? " AND corporate_deposit_transfers.user_id = ?" : "")+
            	" ORDER BY transfer_date ASC" +
            	" LIMIT ? OFFSET ?";

            statement = dbConn.prepareStatement(sql);
            int i = 1;
            statement.setLong(i++, corporateId);
            if (maxDate != null){
            	DbUtil.setTimestamp(statement, i++, maxDate);    
            }
            if (minDate != null){
            	DbUtil.setTimestamp(statement, i++, minDate);           
            }
            if (userId != null) {
            	statement.setLong(i++, userId);
            }
            statement.setInt(i++, limit);
            statement.setInt(i, offset);

            res = statement.executeQuery();

            List<UserTransactionBean> rows = new ArrayList<UserTransactionBean>();
            UserTransactionBean currRow;
            while (res.next()){
                currRow = new UserTransactionBean();
                currRow.setUsername(res.getString("username"));
                if (res.getDouble("amount") > 0.0){
                	currRow.setDebitAmount(res.getDouble("amount"));
                } else {
                	currRow.setCreditAmount(Math.abs(res.getDouble("amount")));
            	}
                currRow.setDate(DbUtil.getTimestamp(res,"transfer_date"));
                currRow.setDescription(res.getString("transfer_desc"));
                rows.add(currRow);
            }
            it = rows.iterator();
        } catch (SQLException e) {
        	throw new RuntimeException("Error while retrieving corporate user transfers.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
    	int count = 0;
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn == null){
            return 0;
        }
        
        try {            
            String sql = 
            	"SELECT COUNT(*)"+
            	" FROM corporate_deposit_transfers"+
            	" INNER JOIN affiliate_users ON affiliate_users.affiliate_id = corporate_deposit_transfers.transfer_user"+
            	" AND affiliate_users.user_id = corporate_deposit_transfers.user_id"+
            	" AND affiliation = 1"+
            	" WHERE corporate_deposit_transfers.transfer_user = ?"+
            	(maxDate != null? " AND ((transfer_date AT TIME ZONE 'UTC') <= ?::timestamp AT TIME ZONE 'UTC')" : "")+
            	(minDate != null? " AND ((transfer_date AT TIME ZONE 'UTC') >= ?::timestamp AT TIME ZONE 'UTC')" : "")+
            	(userId != null ? " AND corporate_deposit_transfers.user_id = ?" : "");

            statement = dbConn.prepareStatement(sql);
            int i = 1;
            statement.setLong(i++, corporateId);
            if (maxDate != null){
            	DbUtil.setTimestamp(statement, i++, maxDate);
            }
            if (minDate != null){
            	DbUtil.setTimestamp(statement, i++, minDate);
            }
            if (userId != null) {
            	statement.setLong(i++, userId);
            }
            
            res = statement.executeQuery();

            while (res.next()){
                count += res.getInt(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving corporate user transfers count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }

        return count;
    }
    
}
