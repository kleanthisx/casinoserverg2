package com.magneta.administration.partners.pages.reports;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.administration.commons.services.reporting.ServiceParameter;
import com.magneta.casino.services.ServiceException;

public abstract class CorporateGamingRevenue extends ReportOptionsPage {

	public static final String REPORT_NAME = "corporate_gaming_revenue";
	private static final ReportBean rep = new ReportBean("corporate_gaming_revenue", "Corporate Gaming Revenue" , "Corporate Gaming Revenue", "reports/CorporateGamingRevenue ", " ", false);

	@InitialValue("initialMinDate")
	public abstract Date getMinDate();
	public abstract void setMinDate(Date date);
	public abstract void setMaxDate(Date date);
	@InitialValue("new java.util.Date()")
	public abstract Date getMaxDate();
	
	public abstract boolean getShowSubs();
	
	@Component(id = "showSubsCB", 
			bindings={"value=ShowSubs", "displayName=message:ShowSubs"})
	public abstract Checkbox getShowSubsCB();
	
	@Component(id = "showSubsCBLabel", 
			bindings={"field=component:showSubsCB"})
	public abstract FieldLabel getShowSubsCBLabel();

	@InitialValue("-1")
	public abstract Long getSubCorp();

	@Override
	public ILink onSubmit(IRequestCycle cycle) throws ServiceException{
		Date maxDate = normalizeDate(getMaxDate());
		Date minDate = normalizeDate(getMinDate());

		int countDays=(int)((maxDate.getTime()- minDate.getTime())/ (1000*60*60*24));

		if (getDelegate().getHasErrors()){
			return null;
		}

		if(countDays>31){
			getDelegate().record(getMsg("month-error"), null);
			return null;
		}	

		if(getMinDate().after(getMaxDate())){
			getDelegate().record(getMsg("date-error"),null);	
		}

		return super.onSubmit(cycle);
	} 

	public Date getInitialMinDate() {
		Calendar fromCal = Calendar.getInstance();
		fromCal.setTime(getMaxDate());
		fromCal.roll(Calendar.MONTH, -1);
		return fromCal.getTime();	
	}

	@Override
	public void fillReportParameters(List<ServiceParameter> reportParams){
		super.fillReportParameters(reportParams);

		TimeZone tz = getUser().getTimeZone();
		
		Date minDate = removeTimeMin(normalizeDate(getMinDate()));
		Date maxDate = removeTimeMax(normalizeDate(getMaxDate()));

		//NORMALIZE DATES
		if (minDate != null){       	
        	/* Convert min date to UTC day start.
        	 * This is important since the database daily records are using UTC */
        	long utcMinDate = minDate.getTime() + tz.getOffset(minDate.getTime());
            reportParams.add(new ServiceParameter("min_date", new Timestamp(utcMinDate)));
        }
		
        if (maxDate != null){       	
        	/* Convert max date to UTC day start.
        	 * This is important since the database daily records are using UTC */
        	long utcMaxDate = maxDate.getTime() + tz.getOffset(maxDate.getTime());
            reportParams.add(new ServiceParameter("max_date", new Timestamp(utcMaxDate)));
		} 
        
        reportParams.add(new ServiceParameter("corp_id",getUser().getUserId()));
		reportParams.add(new ServiceParameter("show_subs",getShowSubs()));
	}

	@Override
	public final ReportBean getReport() {
		return rep;
	}
	
	@Override
	public String getReportName(){
		return REPORT_NAME;
	}

	@Override
	public void onCancel(IRequestCycle cycle){
		redirectHome();
	}

	@Override
	public String toString(){
		return this.getPageName();
	}
	
}
