package com.magneta.administration.partners.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.partners.beans.CorporateUserBean;
import com.magneta.casino.services.enums.UserAffiliationEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class CorporateRelationshipsTableModel implements IBasicTableModel {

	public static final int USER_RELATIONSHIP = 1;
	public static final int SUB_CORPORATE_RELATIONSHIP = 2;
	public static final int SUPER_CORPORATE_RELATIONSHIP = 3;
	
    private final Long affiliateId;
    private final int relationship;
    private final boolean showOnlyActive;
    private final boolean showClosed;
    
    public CorporateRelationshipsTableModel(Long affiliateId, int relationship){
    	this(affiliateId, relationship, true);
    }
    
    public CorporateRelationshipsTableModel(Long affiliateId, int relationship, boolean showOnlyActive){
        this(affiliateId, relationship, showOnlyActive, true);
    }
    
    public CorporateRelationshipsTableModel(Long affiliateId, int relationship, boolean showOnlyActive, boolean showClosed){
    	if(affiliateId == null){
    		this.affiliateId = 0L;
    	}else{
    		this.affiliateId = affiliateId;
    	}
        this.relationship = relationship;
        this.showOnlyActive = showOnlyActive;
        this.showClosed = showClosed;
    }

    @Override
	public Iterator<CorporateUserBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getReportsConnection();
        Iterator<CorporateUserBean> it = null;

        if (dbConn == null){
            return null;
        }
        
        try {
        	PreparedStatement statement = null;
            ResultSet res = null;
            try {
            	if (relationship == USER_RELATIONSHIP){
            		statement = dbConn.prepareStatement(
            				"SELECT affiliate_users.affiliation, users.user_id, users.username, users.first_name, users.last_name, user_balance.balance, users.is_active, user_balance.play_credits AS play_credits"+
            				" FROM affiliate_users"+
            				" INNER JOIN users ON affiliate_users.user_id = users.user_id"+(showOnlyActive ? " AND is_active = TRUE" : "")+(showClosed ? "" : " AND closed = FALSE")+
            				" INNER JOIN user_balance ON affiliate_users.user_id = user_balance.user_id"+
            				" WHERE affiliation = ?"+
            				" AND affiliate_id = ?" +
            				" ORDER BY users.username ASC LIMIT ? OFFSET ?");
            		statement.setInt(1, UserAffiliationEnum.USER_AFFILIATION.getId());
                } else if (relationship == SUB_CORPORATE_RELATIONSHIP){
                	statement = dbConn.prepareStatement(
                			"SELECT affiliate_users.affiliation, users.user_id, users.username, users.first_name, users.last_name, user_balance.balance, users.is_active, user_balance.play_credits AS play_credits"+
            				" FROM affiliate_users"+
            				" INNER JOIN users ON affiliate_users.user_id = users.user_id "+(showOnlyActive ? " AND is_active = TRUE" : "")+(showClosed ? "" : " AND closed = FALSE")+
            				" INNER JOIN user_balance ON affiliate_users.user_id = user_balance.user_id"+
            				" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id AND affiliates.approved = TRUE" +
            				" WHERE affiliation = ?"+
            				" AND affiliate_users.affiliate_id = ?"+
            				" ORDER BY users.username ASC LIMIT ? OFFSET ?");
                	statement.setInt(1, UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId());
                } else {
                	statement = dbConn.prepareStatement(
            				" SELECT affiliate_users.affiliation, users.user_id, users.username, users.first_name, users.last_name, users.is_active"+
                			" FROM affiliate_users"+
                			" INNER JOIN users ON affiliate_users.affiliate_id = users.user_id"+(showOnlyActive ? " AND is_active = TRUE" : "")+(showClosed ? "" : " AND closed = FALSE")+
                			" WHERE affiliation = ?"+
                			" AND affiliate_users.user_id = ?"+
                			" ORDER BY users.username ASC LIMIT ? OFFSET ?");
                	statement.setInt(1, UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId());
                }
            	
        		statement.setLong(2, affiliateId);
            	
        		statement.setInt(3, limit);
                statement.setInt(4, offset);
        		
            	ArrayList<CorporateUserBean> myArr = new ArrayList<CorporateUserBean>();
                res = statement.executeQuery();

                CorporateUserBean currRow;
                while (res.next()){
                    currRow = new CorporateUserBean();
                    currRow.setActive(res.getBoolean("is_active"));
                    currRow.setUsername(res.getString("username"));
                    currRow.setFirstName(res.getString("first_name"));
                    currRow.setLastName(res.getString("last_name"));
                    currRow.setUserId(res.getLong("user_id"));
                    currRow.setRelation(this.relationship);
                    currRow.setBalance(relationship == USER_RELATIONSHIP || 
                    	relationship == SUB_CORPORATE_RELATIONSHIP ? res.getDouble("balance") : 0.0);
                    currRow.setFreeCredits(relationship == USER_RELATIONSHIP || 
                    	relationship == SUB_CORPORATE_RELATIONSHIP ? res.getDouble("play_credits") : 0.0);
                    myArr.add(currRow);
                }
                
                it = myArr.iterator();
            } finally {
                DbUtil.close(res);
                DbUtil.close(statement);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting corporate users list.", e);
        } finally {
            DbUtil.close(dbConn);
        }

        return it;
    }

    @Override
	public int getRowCount() {
        int count = 0;
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn == null){
            return 0;
        }
        
        try {            
        	if (relationship == USER_RELATIONSHIP){
        		statement = dbConn.prepareStatement(
        				"SELECT COUNT(*)"+
        				" FROM affiliate_users"+
        				" INNER JOIN users ON affiliate_users.user_id = users.user_id "+(showOnlyActive ? " AND users.is_active = TRUE" : "")+(showClosed ? "" : " AND closed = FALSE")+
        				" WHERE affiliation = ?"+
        				" AND affiliate_id = ?");
        		
        		statement.setInt(1, UserAffiliationEnum.USER_AFFILIATION.getId());
        		statement.setLong(2, affiliateId);
            } else if (relationship == SUB_CORPORATE_RELATIONSHIP){
            	statement = dbConn.prepareStatement(
        				"SELECT COUNT(*)"+
        				" FROM affiliate_users"+
        				" INNER JOIN users ON affiliate_users.user_id = users.user_id"+(showOnlyActive ? " AND users.is_active = TRUE" : "")+(showClosed ? "" : " AND closed = FALSE")+
        				" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id AND affiliates.approved = TRUE" +
        				" WHERE affiliation = ?"+
        				" AND affiliate_users.affiliate_id = ?");
        		
        		statement.setInt(1, UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId());
        		statement.setLong(2, affiliateId);
            } else {
            	statement = dbConn.prepareStatement(
        				" SELECT COUNT(*)"+
            			" FROM affiliate_users"+
            			" INNER JOIN users ON affiliate_users.affiliate_id = users.user_id"+(showOnlyActive ? " AND users.is_active = TRUE" : "")+(showClosed ? "" : " AND closed = FALSE")+
            			" WHERE affiliation = ?"+
            			" AND affiliate_users.user_id = ?");
            	
        		statement.setInt(1, UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId());
        		statement.setLong(2, affiliateId);
            }

            res = statement.executeQuery();

            if (res.next()){
                count = res.getInt(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving corporate users count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return count;
    }
}