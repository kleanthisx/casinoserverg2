package com.magneta.administration.partners.pages;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.link.PageLink;

public abstract class SubCorporateRequiresApproval extends SecurePage {
	
	@Component(id="home",
			bindings={"page=literal:Home"})
	public abstract PageLink getHome();
}
