package com.magneta.administration.partners.models;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.partners.beans.TransferBean;
import com.magneta.administration.partners.security.User;

public class LastTranfersModel implements IBasicTableModel {

	TransferBean[] lastTransfers;
	
	public LastTranfersModel(User user) {
		lastTransfers = user.getLastTransfers();
	}
	
	@Override
	public Iterator<TransferBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol,
			boolean asc) {
		
		int listSize = lastTransfers.length - offset;
		if (limit < listSize) {
			listSize = limit;
		}
		
		ArrayList<TransferBean> myList = new ArrayList<TransferBean>(listSize);
		for (int i=listSize - 1; (i-offset) < listSize && i >= offset; i--) {
			myList.add(lastTransfers[i]);
		}

		return myList.iterator();
	}

	@Override
	public int getRowCount() {
		return lastTransfers.length;
	}

	
}
