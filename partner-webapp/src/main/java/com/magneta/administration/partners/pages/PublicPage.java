package com.magneta.administration.partners.pages;

import org.apache.tapestry.IAsset;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.annotations.Asset;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.html.BasePage;

import com.magneta.Config;
import com.magneta.administration.partners.security.User;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.UserAuthenticationService;

public abstract class PublicPage extends BasePage {

	@Asset("context:css/style.css")
	public abstract IAsset getStylesheet();
	
	@InjectObject("service:casino.common.Principal")
	public abstract PrincipalService getPrincipalService();

	@InjectObject("service:casino.common.UserAuthentication")
    public abstract UserAuthenticationService getUserAuthenticationService();
	
	@InjectObject("service:casino.common.Currency")
	public abstract CurrencyService getCurrencyService();
	
	@InjectObject("service:casino.common.CurrencyParser")
	public abstract CurrencyParser getCurrencyParser();
	
    public String getMsg(String msg) {
        return this.getMessages().getMessage(msg);
    }
    
	public User getUser() {
		return (User)getPrincipalService().getPrincipal();
	}

    public void onLogout(IRequestCycle cycle) {
    	logout(cycle);
    }
    
    public void logout(IRequestCycle cycle) {
    	getUserAuthenticationService().logoutUser();
        redirectToPage("Login");
    }
    
	public void redirectToPage(String page){
		IEngineService s = getRequestCycle().getInfrastructure().getServiceMap().getService(Tapestry.PAGE_SERVICE);
        
        ILink link = s.getLink(false, page);
        
        if (link != null) {
            String redirect = link.getURL();
            throw new RedirectException(redirect);
        }
	}

    public boolean isDebugEnabled() {
    	return Config.getBoolean("server.development", false);
    }
    
    public String getSystemCurrencySymbol() {
    	return getCurrencyParser().getCurrencySymbol(getLocale());
    }
    
    public String getPageTitle() {
    	return getMessages().getMessage("page-title");
    }

	public boolean getDiplayMenu() {
		return false;
	}
}
