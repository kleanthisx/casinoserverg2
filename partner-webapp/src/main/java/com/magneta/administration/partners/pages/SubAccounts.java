package com.magneta.administration.partners.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.link.ExternalLink;

import com.magneta.administration.partners.beans.AffiliateBean;
import com.magneta.administration.partners.beans.CorporateUserBean;
import com.magneta.administration.partners.models.AffiliatesTableModel;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class SubAccounts extends SecurePage{
	
	@Bean
	public abstract EvenOdd getEvenOdd();

	@Component(id="corporates", type="contrib:Table",
			bindings={"source=ognl:affiliatesTableModel",
			"columns=columns","pageSize=30","rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:Username",
			"row=currAffiliate","initialSortOrder=true"})
			public abstract Table getCorporates();
	
	public AffiliatesTableModel getAffiliatesTableModel() {
		return new AffiliatesTableModel(true, getCurrentCorp());
	}

	@Component(id="lastPayment",
			bindings={"value=formatDate(currAffiliate.LastPayment)"})
			public abstract Insert getLastPayment();

	@Component(id="affiliateBalance",
			bindings={"value=formatAmount(currAffiliate.affiliateBalance)"})
			public abstract Insert getAffiliateBalance();

	@Component(id="balance",
			bindings={"value=formatAmount(currAffiliate.deposit)"})
			public abstract Insert getBalance();

	@Component(id="transfer2balanceMsg",
			bindings={"value=message:TransferToBalance","renderTag=currAffiliate.paymentDue"})
			public abstract Insert getTransfer2BalanceMsg();

	@Component(id="transfer2balanceLink",
			bindings={"page=literal:PayCorporate","parameters=currAffiliate.Id"})
			public abstract ExternalLink getTransfer2BalanceLink();

	@Component(id="usernameLink",
			bindings={"page=literal:UserInformation","parameters=currAffiliate.Id"})
			public abstract ExternalLink getUsernameLink();

	@Component(id="userUsernameLink",
			bindings={"page=literal:UserInformation","parameters=currUser.userId"})
			public abstract ExternalLink getuserUsernameLink();
			
	@Component(bindings={"page=literal:SubAccounts","parameters={currAffiliate.Id}"})
	public abstract ExternalLink getSubLink();

	@Component(id="users", type="contrib:Table",
			bindings={"source=ognl:new com.magneta.administration.partners.models.CorporateRelationshipsTableModel(currentCorp, @com.magneta.administration.partners.models.CorporateRelationshipsTableModel@USER_RELATIONSHIP, false, false)",
			"columns=userColumns","pageSize=20","row=currUser"})
			public abstract Table getUsers();

	@Component(id="userActivateMsg",
			bindings={"value=(currUser.active ? getMsg('Deactivate') : getMsg('Activate'))"})
			public abstract Insert getUserActivateMsg();

	@Component(id="userActivateLink",
			bindings={"listener=listener:onChangeActive","parameters={currUser.userId,currUser.Active}"})
			public abstract DirectLink getUserActivateLink();

	@Component(id="partnerActivateMsg",
			bindings={"value=(currAffiliate.active ? getMsg('Deactivate') : getMsg('Activate'))"})
			public abstract Insert getPartnerAvtivateMsg();

	@Component(id="partnerActivateLink",
			bindings={"listener=listener:onChangeActive","parameters={currAffiliate.Id, currAffiliate.Active}"})
			public abstract DirectLink getPartnerActivateLink();

	@Component(id="pCloseLink",
			bindings={"listener=listener:onClose","parameters={currAffiliate.Id}"})
			public abstract DirectLink getPCloseLink();

	@Component(id="uCloseLink", type="DirectLink",
			bindings={"listener=listener:onClose","parameters={currUser.userId}"})
			public abstract DirectLink getUCloseLink();

	public abstract AffiliateBean getCurrAffiliate();
	public abstract CorporateUserBean getCurrUser();
	
	public abstract void setSuperCorp(Long superCorp);

	public abstract void setSuperCorpUsername(String superCorpUsername);

	public abstract void setCurrentCorp(Long id);
	public abstract Long getCurrentCorp();

	public abstract void setCurrentCorpUsername(String username);
	public abstract String getCurrentCorpUsername();

	public abstract void setHasUsers(boolean hasUsers);
	public abstract void setHasSubCorps(boolean hasSubCorps);
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		if (params != null && params.length == 1) {
			setCurrentCorp((Long)params[0]);
		}
	}
	
	@Override
	public void prepareForRender(IRequestCycle cycle) {
		super.prepareForRender(cycle);
		if(getCurrentCorp() == null) {
			setCurrentCorp(getUser().getId());
		}
		
		setCurrentCorpUsername(getUsername(getCurrentCorp()));

		Connection conn = ConnectionFactory.getConnection();

		try {
			PreparedStatement stmt2 = null;
			ResultSet rs2 = null;

			try {
				stmt2 = conn.prepareStatement(
						"SELECT COUNT(user_id)"+
						" FROM affiliate_users"+
						" WHERE affiliate_users.affiliate_id = ?" +
				" AND affiliation = 1");
				stmt2.setLong(1, getCurrentCorp());

				rs2 = stmt2.executeQuery();

				if (rs2.next()){
					setHasUsers(rs2.getInt(1) > 0);
				}
			} finally {
				DbUtil.close(rs2);
				DbUtil.close(stmt2);
			}

			PreparedStatement stmt3 = null;
			ResultSet rs3 = null;

			try {
				stmt3 = conn.prepareStatement(
						"SELECT COUNT(user_id)"+
						" FROM affiliate_users"+
						" WHERE affiliate_users.affiliate_id = ?" +
				" AND affiliation = 3");
				stmt3.setLong(1, getCurrentCorp());

				rs3 = stmt3.executeQuery();

				if (rs3.next()){
					setHasSubCorps(rs3.getInt(1) > 0);
				}
			} finally {
				DbUtil.close(rs3);
				DbUtil.close(stmt3);
			}

		} catch (SQLException e) {
			throw new RuntimeException("Error retrieving username.", e);
		} finally {
			DbUtil.close(conn);
		}
	}

	@InjectPage("ConfirmUserStateChange")
	public abstract ConfirmUserStateChange getStateChangeConfirmPage();

	public void onChangeActive(IRequestCycle cycle,Long userID,boolean activate) {
		ConfirmUserStateChange stateChangeConfirm = getStateChangeConfirmPage();
		stateChangeConfirm.setUserID(userID);
		stateChangeConfirm.setActivate(!activate);
		cycle.activate(stateChangeConfirm);
	}

	@InjectPage("ConfirmUserAccountClose")
	public abstract ConfirmUserAccountClose getUserAccountClosePage();

	public void onClose(IRequestCycle cycle,Long userID) {
		ConfirmUserAccountClose stateChangeConfirm = getUserAccountClosePage();
		stateChangeConfirm.setUserID(userID);
		cycle.activate(stateChangeConfirm);
	}

	public String getColumns() {
		String cols = "!AffiliateUsername, !Deposit, !AffiliateBalance, !PaymentPeriod, !LastPayment";
		cols += ",!transferEarnings,!subaccounts, !pActivate, !pClose";
				
		return cols;		
	}
	public String getUserColumns() {
		String cols = "!Username, !FirstName, !LastName";
		cols += ",!uActivate, !uClose";
		return cols;		
	}

	public String getAmountColumnHeader(String column) {
		String symbol = getCurrencyParser().getCurrencySymbol(getLocale());
		
		return getMsg(column) + "  (" + symbol + ")"; 
	}
}
