package com.magneta.administration.partners.services;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry.IRequestCycle;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;

public class ReportGenerationService extends com.magneta.administration.commons.services.reporting.ReportGenerationService {

	@Override
	public void validateServiceAccess(IRequestCycle cycle) throws IOException {
		
		ExtendedPrincipalBean principal = this.getPrincipalService().getPrincipal();

		if (principal == null) {
			cycle.getInfrastructure().getResponse().sendError(HttpServletResponse.SC_FORBIDDEN, "You need to log in to access the reports");
		}
	}
}
