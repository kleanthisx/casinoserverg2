package com.magneta.administration.partners.pages;

import java.sql.Connection;

import org.apache.hivemind.Messages;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Delegator;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.IfBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.common.user.utils.ChangeUserStateUtil;
import com.magneta.casino.services.UserAccessService;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class ConfirmUserStateChange extends SecurePage {

	@InjectObject("service:casino.common.UserAccess")
    public abstract UserAccessService getUserAccessService();
	
	@Component(id="errors", type="For",
			bindings={"source=beans.delegate.fieldTracking","value=currentFieldTracking","renderTag=false"})
	public abstract ForBean getErrors();
	
	@Component(id="error",
			bindings={"delegate=currentFieldTracking.errorRenderer"})
	public abstract Delegator getErrorDelegator();
	
	@Component(id="isInError", type="If",
			bindings={"condition=currentFieldTracking.inError","renderTag=true"})
	public abstract IfBean getIsInError();
	
	@Component(id="result",
			bindings={"value=result"})
			public abstract Insert getRes();

	@Component(id="confirmForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel"})
			public abstract Form getConfirmForm();

	@Component(id="userID",
			bindings={"value=userID"})
			public abstract Hidden getUserId();

	@Component(id="activate",
			bindings={"value=activate"})
			public abstract Hidden getActive();

	@Component(id="confirmSubmit")
	public abstract Submit getConfirmSubmit();

	@Component(id="cancelRemoval")
	public abstract Button getCancelRemoval();

	@Bean
    public abstract ValidationDelegate getDelegate();
	
	public abstract IFieldTracking getCurrentFieldTracking();
	
	public abstract void setResult(String result);

	public abstract Long getUserID();
	public abstract void setUserID(Long userID);

	public abstract String getUsername();
	public abstract void setUsername(String username);

	public abstract boolean getActivate();
	public abstract void setActivate(boolean activate);

	public boolean isUserIdNull = false;

	@Override
	public void prepareForRender(IRequestCycle cycle){
		super.prepareForRender(cycle);
		 ValidationDelegate delegate = getDelegate();
		 Messages messages = getMessages();
		 isUserIdNull = false; 
		
		 if(getUserID() == null){
			 isUserIdNull = true;
			 delegate.record(messages.getMessage("invalid-request"), null);
			 return;
		 }
		 
		setUsername(getUsername(getUserID()));	
	}

	public String getConfirmMessage(){

		if (getActivate()){
			return getMessages().getMessage("ActivateConfirm")+" "+getUsername()+"?";
		} 
		
		return getMessages().getMessage("DeactivateConfirm")+" "+getUsername()+"?";
	}

	private boolean changeUserState(Long userID, boolean active) {
		if (!this.getUserAccessService().canAccess(userID)) {
			setResult("Illegal operation.");
			return false;
		}
		
		
		Connection dbConn = ConnectionFactory.getConnection();

		if (dbConn == null){
			setResult(getMsg("db-error"));
			return false;
		}
		
		try {
			if(!ChangeUserStateUtil.changeUserState(dbConn, getUserID(), getUser().getId(), active)) {
				setResult(getMsg("db-error"));
				return false;
			}
			return true;
		} finally {
			DbUtil.close(dbConn);
		}
	}

	public void onSubmit(IRequestCycle cycle){
		if (changeUserState(getUserID(), getActivate())){
			redirectToPage("SubAccounts");
		}
	}

	public void onCancel(IRequestCycle cycle){
		redirectToPage("SubAccounts");
	}

	@Override
	public String toString(){
		return "SubAccounts,"+this.getPageName();
	}
}
