function getBalances() {
	var userIds = new Array();
	var currUserId = null;

	for (var i = 0;; i++) {
		currUserId = document.getElementById('userId_' + i);

		if (currUserId) {
			userIds[i] = currUserId.firstChild.nodeValue;
		} else {
			break;
		}
	}

	requestBalances(userIds);
}

function getRefreshedBalances(balances) {
	if (balances)  {
		for (var i = 0; i < balances.length; i++) {
			var data = balances[i].split('_', 3);
			var userId = data[0];
			var balance = data[1];
			var freeCredits = data[2];

			var currBalance = document.getElementById('bal_' + userId);
			var currFree = document.getElementById('free_' + userId);

			if (currBalance) {
				if (currBalance.firstChild) {
					currBalance.firstChild.nodeValue = balance;
				} else {
					currBalance.appendChild(document.createTextNode(balance));
				}
			}

			if (currFree) {
				if (currFree.firstChild) {
					currFree.firstChild.nodeValue = freeCredits;
				} else {
					currFree.appendChild(document.createTextNode(freeCredits));
				}
			}
		}
	}
	setTimeout("getBalances()", 5000);
}

function initBalanceRefresh() {
	setTimeout("getBalances()", 5000);
}

/*
 * 
*/
var promoClicked = true;

function allowPromo() {
	promoClicked = false;
}

function initPromo() {
	promoClicked = true;

	setTimeout("allowPromo()", 500);

	var usrTable = document.getElementById("users");
	var usrRows = null;
	if (usrTable) {
		usrRows = usrTable.childNodes;
		for (var j = 0; j < usrRows.length; j++) {
			usrRows[j].onclick = userRowClicked;
			if (usrRows[j].captureEvents)
				usrRows[j].captureEvents(Event.CLICK);
		}
	}

	usrTable = document.getElementById("subCorps");
	if (usrTable) {
		usrRows = usrTable.childNodes;
		for (var k = 0; k < usrRows.length; k++) {
			usrRows[k].onclick = userRowClicked;
			if (usrRows[k].captureEvents)
				usrRows[k].captureEvents(Event.CLICK);
		}
	}
}

function userRowClicked(e) {
	if (e == null) {
		e = window.event;
	}
	var usrClicked = (e.target == null ? e.srcElement.parentNode
			: e.target.parentNode);

	if (usrClicked.tagName.toLowerCase() == 'tr') {
		highlightRow(usrClicked);
		var user = usrClicked.getElementsByTagName('span')[0].firstChild.nodeValue;
		var sel = document.getElementById("userSelect");
		if (sel) {
			for (var i = 0; i < sel.length; i++) {
				if (sel[i].value == user) {
					sel[i].selected = true;
				}
			}
		}
		var subSel = document.getElementById("subCorpSelect");
		if (subSel) {
			for (var l = 0; l < subSel.length; l++) {
				if (subSel[l].value == user) {
					subSel[l].selected = true;
				}
			}
		}
	}
}

function highlightRow(row) {
	var rows = row.parentNode.getElementsByTagName('tr');
	for (var m = 0; m < rows.length; m++) {
		rows[m].className = "";
	}
	row.className = 'highlight';
}

function givePromo(username) {
	if (promoClicked) {
		return false;
	}
	promoClicked = confirm('Are you sure you want to give promo credits to user '
			+ username + '?');

	return promoClicked;

}