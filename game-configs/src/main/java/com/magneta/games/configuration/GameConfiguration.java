/**
 * 
 */
package com.magneta.games.configuration;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author anarxia
 *
 */
@XmlRootElement 
public interface GameConfiguration {

    @XmlElement
    Long getConfigId();
    
    /**
     * Converts the configuration to a string representation.
     * @return
     * @throws JAXBException
     */
    String serialize() throws JAXBException;
}
