package com.magneta.games.configuration.poker.texasholdem;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.poker.PokerConstants;

/**
 * @author User
 *
 */
@XmlRootElement 
public class TexasHoldemConfiguration implements GameConfiguration {
    
	
	private Long configId;
	private List<WinCombination> winCombinations;
	private List<WinCombination> sideWinCombinations;
	
	public static enum TexasPokerSideGame{
		AA_AA(1),
		AA(2),
		AK_SUITED(3),
		AQ_AJ_SUITED(4),
		AK_UNSUITED(5),
		KK_QQ_JJ(6),
		AQ_AJ_UNSUITED(7),
		TEN_TO_TWO_PAIR(8);
		
		private final int code;
		
		TexasPokerSideGame(int i) {
			this.code = i;
		}
		
		public int getCode() {
			return code;
		}	
	}

	private static final WinCombination[] defaultWinCombinations = {
		new WinCombination(PokerConstants.NONE, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.JACKS_OR_BETTER, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.TWO_PAIRS, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.THREE_OF_A_KIND, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.STRAIGHT, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.FLUSH, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.FULL_HOUSE, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.FOUR_OF_A_KIND, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.STRAIGHT_FLUSH, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.ROYAL_FLUSH, WinType.BET_RATIO, 1.0),
	};
	
	private static final WinCombination[] defaultSideWinCombinations = {
		new WinCombination(TexasPokerSideGame.AA_AA.code, WinType.BET_RATIO, 1000.0),
		new WinCombination(TexasPokerSideGame.AA.code, WinType.BET_RATIO, 30.0),
		new WinCombination(TexasPokerSideGame.AK_SUITED.code, WinType.BET_RATIO, 25.0),
		new WinCombination(TexasPokerSideGame.AQ_AJ_SUITED.code, WinType.BET_RATIO, 20.0),
		new WinCombination(TexasPokerSideGame.AK_UNSUITED.code, WinType.BET_RATIO, 15.0),
		new WinCombination(TexasPokerSideGame.KK_QQ_JJ.code, WinType.BET_RATIO, 10.0),
		new WinCombination(TexasPokerSideGame.AQ_AJ_UNSUITED.code, WinType.BET_RATIO, 5.0),
		new WinCombination(TexasPokerSideGame.TEN_TO_TWO_PAIR.code, WinType.BET_RATIO, 3.0),
	};
    
    public TexasHoldemConfiguration() {
        super();
        this.winCombinations = new ArrayList<WinCombination>();
        this.sideWinCombinations = new ArrayList<WinCombination>();
        
        for (WinCombination c: defaultWinCombinations) {
        	this.winCombinations.add(c);
        }
        
        for (WinCombination c: defaultSideWinCombinations) {
        	this.sideWinCombinations.add(c);
        }
    }
    
    public TexasHoldemConfiguration(long configId, String configStr) throws JAXBException {
    	JAXBContext context = getJAXBContext();
    	Unmarshaller unmarshaller = context.createUnmarshaller();
    	
    	StringReader sr = new StringReader(configStr);
    	
    	TexasHoldemConfiguration config = (TexasHoldemConfiguration)unmarshaller.unmarshal(sr);
    	
    	this.sideWinCombinations = config.sideWinCombinations;
    	this.winCombinations = config.winCombinations;
    	this.configId = configId;
    }
    
    @Override
	public String serialize() throws JAXBException {
    	JAXBContext ctx = this.getJAXBContext();
    	
    	StringWriter sw = new StringWriter();
    	
    	Marshaller marshaller = ctx.createMarshaller();
    	marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    	
    	marshaller.marshal(this, sw);
    	
    	return sw.toString();
    }
	
	@XmlElement
	public List<WinCombination> getWinCombinations() {
		return winCombinations;
	}
	
	public void setWinCombinations(List<WinCombination> winCombinations) {
		this.winCombinations = winCombinations;
	}
	
	@XmlElement
	public List<WinCombination> getSideWinCombinations() {
		return sideWinCombinations;
	}
	
	public void setSideWinCombinations(List<WinCombination> sideWinCombinations) {
		this.sideWinCombinations = sideWinCombinations;
	}

    private JAXBContext getJAXBContext() throws JAXBException {
    	return JAXBContext.newInstance(this.getClass(), WinCombination.class, WinType.class);
    }
    
    public WinCombination getSideWinCombination(TexasPokerSideGame combo) {
    	for (WinCombination c: sideWinCombinations) {
    		if (c.getTriggerCombo() == combo.code) {
    			return c;
    		}
    	}
    	
    	return null;
    }
    
    public WinCombination getWinCombination(int combo) {
    	for (WinCombination c: winCombinations) {
    		if (c.getTriggerCombo() == combo) {
    			return c;
    		}
    	}
    	
    	return null;
    }
    
    public void setConfigId(Long configId) {
    	this.configId = configId;
    }

	@Override
	@XmlElement
	public Long getConfigId() {
		return configId;
	}
}
