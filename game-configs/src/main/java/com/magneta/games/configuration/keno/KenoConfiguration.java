package com.magneta.games.configuration.keno;



public class KenoConfiguration extends KenoBingoConfiguration {

	public KenoConfiguration() {
		super();
	}

	public KenoConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
    public boolean isJackpotCombination(int betNumber, int hitsNumber) {
		for (int i = 0; i < getWinCombos().length; i++) {
			Object[] combo = getWinCombo(i);
			int betnumbers = (Integer)combo[0];
			int hits = (Integer)combo[1];
			if(betnumbers==betNumber && hits == hitsNumber && (Double)combo[2]<0.00 ){
				return true;
			}
		}
        return false;
    }

	private static final Object[][] WIN_CONSTRAINTS = {
		/*          betnumbers      Hits            Multiplier*/
		/*C01*/{new Integer(1), new Integer(1), new Double(3.0)},
		
		/*C02*/{new Integer(2), new Integer(1), new Double(1.0)},
		/*C03*/{new Integer(2), new Integer(2), new Double(8.0)},
		
		/*C04*/{new Integer(3), new Integer(2), new Double(3.0)},
		/*C05*/{new Integer(3), new Integer(3), new Double(30.0)},
		
		/*C06*/{new Integer(4), new Integer(2), new Double(1.0)},
		/*C07*/{new Integer(4), new Integer(3), new Double(5.5)},
		/*C08*/{new Integer(4), new Integer(4), new Double(130.0)},
		
		/*C09*/{new Integer(5), new Integer(3), new Double(2.5)},
		/*C10*/{new Integer(5), new Integer(4), new Double(25.0)},
		/*C11*/{new Integer(5), new Integer(5), new Double(500.0)},

		/*C12*/{new Integer(6), new Integer(3), new Double(1.5)},
		/*C13*/{new Integer(6), new Integer(4), new Double(8.0)},
		/*C14*/{new Integer(6), new Integer(5), new Double(60.0)},
		/*C15*/{new Integer(6), new Integer(6), new Double(1800.0)},
		
		/*C16*/{new Integer(7), new Integer(3), new Double(1.0)},
		/*C17*/{new Integer(7), new Integer(4), new Double(3.5)},
		/*C18*/{new Integer(7), new Integer(5), new Double(30.0)},
		/*C19*/{new Integer(7), new Integer(6), new Double(120.0)},
		/*C20*/{new Integer(7), new Integer(7), new Double(6000.0)},
		
		/*C21*/{new Integer(8), new Integer(4), new Double(2.5)},
		/*C22*/{new Integer(8), new Integer(5), new Double(12.0)},
		/*C23*/{new Integer(8), new Integer(6), new Double(60.0)},
		/*C24*/{new Integer(8), new Integer(7), new Double(1500.0)},
		/*C25*/{new Integer(8), new Integer(8), new Double(8000.0)},
		
		/*C26*/{new Integer(9), new Integer(4), new Double(1.0)},
		/*C27*/{new Integer(9), new Integer(5), new Double(6.0)},
		/*C28*/{new Integer(9), new Integer(6), new Double(30.0)},
		/*C29*/{new Integer(9), new Integer(7), new Double(300.0)},
		/*C30*/{new Integer(9), new Integer(8), new Double(6000.0)},
		/*C31*/{new Integer(9), new Integer(9), new Double(10000.0)},
		
		/*C32*/{new Integer(10), new Integer(0), new Double(2.5)},
		/*C33*/{new Integer(10), new Integer(5), new Double(2.5)},
		/*C34*/{new Integer(10), new Integer(6), new Double(25.0)},
		/*C35*/{new Integer(10), new Integer(7), new Double(100.0)},
		/*C36*/{new Integer(10), new Integer(8), new Double(750.0)},
		/*C37*/{new Integer(10), new Integer(9), new Double(8000.0)},
		/*C38*/{new Integer(10), new Integer(10), new Double(10000.0)},

		/*C25*/{new Integer(8), new Integer(8), new Double(-0.0075)},
		/*C31*/{new Integer(9), new Integer(9), new Double(-0.0475)},
		/*C38*/{new Integer(10), new Integer(10), new Double(-0.2707)},

	};
	
	public double getMultiplier(int BetLength, int hitCounter) {
		for (int i = 0; i < getWinCombos().length; i++) {
			Object[] combo = getWinCombo(i);
			int betnumbers = (Integer)combo[0];
			int hits = (Integer)combo[1];
			if(betnumbers==BetLength && hits == hitCounter){
				if((Double) combo[2]>0.0)
				return (Double) combo[2];
			}
		}
		return 0.0;
	}

	public double getJackpotPercentage(int BetLength, int hitCounter) {
		for (int i = 0; i < getWinCombos().length; i++) {
			Object[] combo = getWinCombo(i);
			int betnumbers = (Integer)combo[0];
			int hits = (Integer)combo[1];
			if(betnumbers==BetLength && hits == hitCounter && (Double)combo[2]<0.00 ){
					return -(Double) combo[2];
			}
		}
		return 0.0;
	}


}
