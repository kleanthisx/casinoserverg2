package com.magneta.games.configuration.slots;

import java.util.HashMap;
import java.util.Map;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.slots.ConfigurationLoadingUtils;

/**
 * Parses configuration for slot games from a string.
 * @author anarxia
 *
 */
public class SlotConfigurationParser {

    private double[] payouts;
    private int[][][] reelSet;
    private Map<String,String> settings;

    public SlotConfigurationParser(String configString, int reelSetCount, int noReels, int minSymbol, int maxSymbol, int combosLength, GameTemplateSetting[] defaultSettings) {
    	if (configString == null) {
    		return;
    	}
    	
    	configString = configString.replaceAll("\\n", "").replaceAll("\\t","").trim();
    	String[] strs = configString.split(";");

    	if (strs.length < reelSetCount) {
    		return;
    	}
    	    	
    	reelSet = new int[reelSetCount][][];
    	for (int i=0; i < reelSetCount; i++) {
    		int[][] reels = ConfigurationLoadingUtils.parseReels(strs[i], noReels, minSymbol, maxSymbol);
    		reelSet[i] = reels;
    	}
    	   	
    	if (strs.length > reelSetCount && !strs[reelSetCount].isEmpty()){
    		payouts = ConfigurationLoadingUtils.parsePayouts(strs[reelSetCount], combosLength);
    	}
    	
    	if (strs.length > reelSetCount + 1) {
    		if (defaultSettings == null) {
    			throw new RuntimeException("Game configuration does not support settings but settings found");
    		}
    		
    		settings = new HashMap<String,String>();
    		for (int i=reelSetCount+1; i < strs.length; i++) {
    			int delimIndex = strs[i].indexOf('=');
    			
    			if (delimIndex < 1) {
    				throw new RuntimeException("Setting does not have a value");
    			}
    			
    			String key = strs[i].substring(0, delimIndex);
    			String val = strs[i].substring(delimIndex+1);
    			
    			boolean foundSetting = false;
    			
    			for (GameTemplateSetting s: defaultSettings) {
    				if (s.getKey().equals(key)) {
    					foundSetting = true;
    					GameTemplateSetting.parseValue(val, s.getType());
    					break;
    				}
    			}
    			
    			if (foundSetting) {
    				settings.put(key, val);
    			} else {
    				throw new RuntimeException("Game does not support " + key +" setting.");
    			}
    		}
    	}
    	
    }
    
    public double[] getPayouts() {
        return payouts;
    }
    
    public int[][][] getReels() {
    	return reelSet;
    }
    
    public boolean isLoadedSuccesfully() {
        return this.reelSet != null && this.reelSet[0] != null;
    }
    
    public Map<String,String> getSettings() {
    	return settings;
    }
}
