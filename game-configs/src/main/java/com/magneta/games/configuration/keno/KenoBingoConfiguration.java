/**
 * 
 */
package com.magneta.games.configuration.keno;

import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;

@XmlRootElement 
public abstract class KenoBingoConfiguration implements GameConfiguration {

    protected Long configId;
    protected Object[][] winCombos;
    protected Map<String,String> settings;
    
    public KenoBingoConfiguration() {
        this.configId = null;
        this.winCombos = getDefaultWinCombos();
        this.settings = null;
    }
    
    public KenoBingoConfiguration(long configId, String configuration) {
        this.configId = configId;

        KenoConfigurationParser sc = createConfigParser(configuration);
        if (!sc.isLoadedSuccesfully()) {
            throw new IllegalArgumentException("Invalid configuration");
        }

        double[] payouts = sc.getPayouts();
        if (payouts != null) {
            this.winCombos = newWinCombos(payouts);
        } else {
            this.winCombos = getDefaultWinCombos();
        }
        
        this.settings = sc.getSettings();
    }

    public boolean isJackpotCombination(int betNumber, int hitsNumber) {
        return false;
    }

    public double getPayout(int winCombo) {
        return (Double)winCombos[winCombo][2];
    }
    
    public void setPayout(Object[][] combos, int winCombo, double payout) {
        combos[winCombo][2] = payout;
    }
    
    @XmlElement
    public final Object[][] getWinCombos() {
        return this.winCombos;
    }
    
    public final Object[] getWinCombo(int combo) {
        return this.winCombos[combo];
    }
    
    public void setWinCombos(Object[][] winCombos) {
        this.winCombos = winCombos;
    }
    
    @XmlElement
    @Override
    public Long getConfigId() {
        return this.configId;
    }
    
    public void setConfigId(Long configId) {
        this.configId = configId;
    }
    
    @Override
	public String serialize() {
    	StringBuilder sb = new StringBuilder();

		Object[][] combos = getWinCombos();
		for(int i=0;i < combos.length;i++) {
			Object[] combo = combos[i];
			for(int z=0;z<combo.length;z++) {
				//Mean amount class;
				if(combo[z].getClass() == Double.class) {
					Double value = (Double) combo[z];
					sb.append(Integer.toString(i)).append("=").append(value).append(":");
				}
			}
		}
		
		if (this.settings != null) {
			for (Entry<String,String> e: settings.entrySet()) {
				sb.append(";");
				sb.append(e.getKey());
				sb.append('=');
				sb.append(e.getValue());
			}
		}
		
		return sb.toString();
    }
    
    protected abstract Object[][] getDefaultWinCombos();
    
    public GameTemplateSetting[] getDefaultSettings() {
    	return null;
    }
    
    private Object[][] newWinCombos(double[] payouts) {
        Object[][] defaultWinCombos = getDefaultWinCombos();
        Object[][] newCombos = new Object[defaultWinCombos.length][];
        
        for (int i=0; i < defaultWinCombos.length; i++) {
            newCombos[i] = new Object[defaultWinCombos[i].length];
            for (int j=0; j < defaultWinCombos[i].length; j++) {
                newCombos[i][j] = defaultWinCombos[i][j];
            }
        }
        
        if (payouts != null) {
            for (int i=0; i < defaultWinCombos.length && i < payouts.length; i++) {
                setPayout(newCombos, i, payouts[i]);
            } 
        }
        
        return newCombos;
    }
    
    private KenoConfigurationParser createConfigParser(String configuration) {
        Object[][] defaultWinCombos = getDefaultWinCombos();
   
        return new KenoConfigurationParser(configuration, defaultWinCombos.length, this.getDefaultSettings());
    }
    
    @SuppressWarnings("unchecked")
	public <T> T getSetting(String key, Class<T> settingType) {
    	GameTemplateSetting[] defaultSettings = this.getDefaultSettings();
    	
    	if (defaultSettings == null) {
    		throw new IllegalArgumentException("Unknown setting " + key);
    	}
    	
    	for (GameTemplateSetting s : defaultSettings) {
    		if (s.getKey().equals(key)) {
    			if (this.settings == null) {
    				return (T)s.getDefaultValue();
    			}
    			
    			String val = this.settings.get(key);
    			if (val == null) {
    				return (T)s.getDefaultValue();
    			}
    			
    			return (T)GameTemplateSetting.parseValue(val, s.getType());
    		}
    	}
    	
    	throw new IllegalArgumentException("Unknown setting " + key);
    }
    
    public Object getSetting(String key) {
    	GameTemplateSetting[] settings = this.getDefaultSettings();
    	
    	if (settings == null) {
    		throw new IllegalArgumentException("Unknown setting " + key);
    	}
    	
    	for (GameTemplateSetting s : settings) {
    		if (s.getKey().equals(key)) {
    			if (this.settings == null) {
    				return s.getDefaultValue();
    			}
    			
    			String val = this.settings.get(key);
    			if (val == null) {
    				return s.getDefaultValue();
    			}
    			
    			return GameTemplateSetting.parseValue(val, s.getType());
    		}
    	}
    	
    	throw new IllegalArgumentException("Unknown setting " + key);
    }
}
