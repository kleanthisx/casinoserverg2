/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;

/**
 * @author User
 *
 */
@XmlRootElement 
public class BankBurstConfiguration extends SlotConfiguration {
   
	public static final String REEL_WEIGHT_0 = "reel_weight_0";
	public static final String REEL_WEIGHT_1 = "reel_weight_1";
	public static final String REEL_WEIGHT_2 = "reel_weight_2";
	public static final String REEL_WEIGHT_3 = "reel_weight_3";
	
    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting(REEL_WEIGHT_0, GameTemplateSetting.INTEGER, "Reel Set 0 Weight", 2537),
        new GameTemplateSetting(REEL_WEIGHT_1, GameTemplateSetting.INTEGER, "Reel Set 1 Weight", 39),
        new GameTemplateSetting(REEL_WEIGHT_2, GameTemplateSetting.INTEGER, "Reel Set 2 Weight", 43),
        new GameTemplateSetting(REEL_WEIGHT_3, GameTemplateSetting.INTEGER, "Reel Set 3 Weight", 7260),
    };
	
    private static final int MAX_SYMBOL = 10;
    
    public static final int WILD_SYMBOL = 1;
    
    public static final int BANK_BURST_SCATTER = 8;
    public static final int CODE_SCATTER = 9;
    public static final int FREE_SPIN_SCATTER = 10;
    public static final int FREE_SPIN_REEL_SET = 4;

    public static final int[][] FREE_SPIN_WEIGHTS = {
    	{10, 2333},
    	{12, 333},
    	{8, 2000},
    	{6 ,1333},
    	{5, 1333},
    	{20, 1000},
    	{16, 333},
    	{15, 1333}
    };
    
    public static final int[][][] CODE_BONUS_WEIGHTS = {
    /* 3 */ {
    			{ 1, 1333},
    			{ 2, 1667},
    			{ 3, 1333},
    			{ 4, 1000},
    			{ 5, 1667},
    			{ 6, 1000},
    			{10, 1000},
    			{12, 667},
    			{25, 333}
    		},
    /* 4 */ {
    			{  10, 1250},
    			{  15, 1250},
    			{  20, 1500},
    			{  25, 1250},
    			{  30, 1500},
    			{  35,  250},
    			{  40, 1250},
    			{  50, 1000},
    			{  60,  250},
    			{  75,  250},
    			{ 100,  250}
    		},
    /*	5 */{
    			{ 25,  400},
    			{ 30,  400},	
    			{ 35,  600},
    			{ 40,  800},
    			{ 45,  600},
    			{ 50,  800},
    			{ 60,  800},
    			{ 75,  800},
    			{ 80, 1000},
    			{100, 1200},
    			{125,  800},
    			{150,  600},
    			{175,  200},
    			{200,  600},
    			{250,  200},
    			{500,  200}
    		}
    };
    
    public static final int[][][] BANK_BURST_BONUS_WEIGHTS = {
    /* 3 */ {
    			{ 3 , 526},
    			{ 4 , 737},
    			{ 5 , 1526},
    			{ 6 , 1105},
    			{ 8 , 1211},
    			{10 , 2316},
    			{15 , 2316},
    			{50 , 211},
    			{125, 53},
    			{-1 , 526}
    		},
    /* 4 */ {
    			{ 10, 1400},
    			{ 12, 1500},
    			{ 15, 1200},
    			{ 20, 1500},
    			{ 25, 750},
    			{ 30, 750},
    			{35 , 950},
    			{40 , 950},
    			{250, 400},
    			{-1 , 600}
    		},
    /*	5 */{
    			{ 50 , 2600},
    			{ 125, 2600},
    			{ 200, 2400},
    			{ 250, 1200},
    			{ 500, 1200},
    		}
    };
    
    
     
    private static final int[][][] PAYLINES = {
        { {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} }, /* 1  */
        { {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} }, /* 2  */
        { {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} }, /* 3  */
        { {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 4  */
        { {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 5  */
        { {0, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 6  */ 
        { {2, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 7  */
        { {1, 0}, {0, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 8  */
        { {1, 0}, {2, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 9  */
        { {0, 0}, {1, 1}, {1, 2}, {1, 3}, {2, 4} }, /* 10 */
        { {2, 0}, {1, 1}, {1, 2}, {1, 3}, {0, 4} }, /* 11 */
        { {1, 0}, {0, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 12 */
        { {1, 0}, {2, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 13 */
        { {1, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 14 */
        { {1, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 15 */
        { {0, 0}, {0, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 16 */
        { {2, 0}, {2, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 17 */
        { {1, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 18 */
        { {1, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 19 */
        { {0, 0}, {0, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 20 */
    };

    
    private static final int[][][] REELS = {
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	},
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	},
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	},
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	},
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	}
    };

    private static final Object[][] WIN_CONSTRAINTS = {
                /*Symbol         Quantity Req.   Multiplier*/
    	/*C00*/{new Integer(1), new Integer(2), new Double(10.0)},
        /*C01*/{new Integer(1), new Integer(3), new Double(250.0)},
        /*C02*/{new Integer(1), new Integer(4), new Double(1250.0)},
        /*C03*/{new Integer(1), new Integer(5), new Double(50000.0)},
        /*C04*/{new Integer(2), new Integer(2), new Double(10.0)},
        /*C05*/{new Integer(2), new Integer(3), new Double(200.0)},
        /*C06*/{new Integer(2), new Integer(4), new Double(1000.0)},
        /*C07*/{new Integer(2), new Integer(5), new Double(2500.0)},
        /*C08*/{new Integer(3), new Integer(3), new Double(100.0)},
        /*C09*/{new Integer(3), new Integer(4), new Double(200.0)},
        /*C10*/{new Integer(3), new Integer(5), new Double(1000.0)},
        /*C11*/{new Integer(4), new Integer(3), new Double(200.0)},
        /*C12*/{new Integer(4), new Integer(4), new Double(200.0)},
        /*C13*/{new Integer(4), new Integer(5), new Double(1000.0)},
        /*C14*/{new Integer(5), new Integer(3), new Double(25.0)},
        /*C15*/{new Integer(5), new Integer(4), new Double(100.0)},
        /*C16*/{new Integer(5), new Integer(5), new Double(500.0)},
        /*C17*/{new Integer(6), new Integer(3), new Double(25.0)},
        /*C18*/{new Integer(6), new Integer(4), new Double(100.0)},
        /*C19*/{new Integer(6), new Integer(5), new Double(500.0)},
        /*C20*/{new Integer(7), new Integer(3), new Double(25.0)},
        /*C21*/{new Integer(7), new Integer(4), new Double(100.0)},
        /*C22*/{new Integer(7), new Integer(5), new Double(500.0)},
        /*C23*/{new Integer(-8), new Integer(3), new Double(0.0)},
        /*C24*/{new Integer(-8), new Integer(4), new Double(0.0)},
        /*C25*/{new Integer(-8), new Integer(5), new Double(0.0)},
        /*C26*/{new Integer(-9), new Integer(3), new Double(0.0)},
        /*C27*/{new Integer(-9), new Integer(4), new Double(0.0)},
        /*C28*/{new Integer(-9), new Integer(5), new Double(0.0)},
        /*C29*/{new Integer(-10), new Integer(3), new Double(2.0)},
        /*C30*/{new Integer(-10), new Integer(4), new Double(2.0)},
        /*C31*/{new Integer(-10), new Integer(5), new Double(2.0)},
        /*C32*/{new Integer(0), new Integer(3), new Double(50.0), new Integer(1), new Integer(10)}, /* Bonus payouts */
        /*C33*/{new Integer(0), new Integer(4), new Double(100.0), new Integer(1), new Integer(10)},
        /*C34*/{new Integer(0), new Integer(5), new Double(200.0), new Integer(1), new Integer(5)},
    };
    
    public BankBurstConfiguration() {
        super();
    }
    
    public BankBurstConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return combo == 3;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
	
    @Override
	public int getNumberOfReelSets() {
    	return REELS.length;
    }
    
    @Override
	public GameTemplateSetting[] getDefaultSettings() {
    	return DEFAULT_SETTINGS;
    }
    
}
