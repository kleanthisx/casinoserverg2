package com.magneta.games.configuration.bingo;

import com.magneta.games.configuration.keno.KenoBingoConfiguration;

public class BingoConfiguration extends KenoBingoConfiguration {

	public static final int LINE_NUMBERS = 5;
	public static final int CARD_NUMBERS = 15;
	
	public BingoConfiguration() {
		super();
	}

	public BingoConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
    public boolean isJackpotCombination(int betNumber, int hitsNumber) {
		for (int i = 0; i < getWinCombos().length; i++) {
			Object[] combo = getWinCombo(i);
			int betnumbers = (Integer)combo[0];
			int hits = (Integer)combo[1];
			if(betnumbers==betNumber && hits == hitsNumber && (Double)combo[2]<0.00 ){
				return true;
			}
		}
        return false;
    }

	private static final Object[][] WIN_CONSTRAINTS = {
		/*          betnumbers      Hits            Multiplier*/
		/*C01*/{new Integer(1), new Integer(15), new Double(0.0)},
		/*C02*/{new Integer(2), new Integer(15), new Double(0.0)},
		/*C03*/{new Integer(3), new Integer(15), new Double(0.0)},
		/*C04*/{new Integer(4), new Integer(15), new Double(0.0)},
		/*C05*/{new Integer(5), new Integer(15), new Double(0.0)},
		/*C06*/{new Integer(6), new Integer(15), new Double(0.0)},
		/*C07*/{new Integer(7), new Integer(15), new Double(0.0)},
		/*C08*/{new Integer(8), new Integer(15), new Double(0.0)},
		/*C09*/{new Integer(9), new Integer(15), new Double(0.0)},
		/*C10*/{new Integer(10), new Integer(15), new Double(0.0)},
		/*C11*/{new Integer(11), new Integer(15), new Double(0.0)},
		/*C12*/{new Integer(12), new Integer(15), new Double(0.0)},
		/*C13*/{new Integer(13), new Integer(15), new Double(0.0)},
		/*C14*/{new Integer(14), new Integer(15), new Double(0.0)},

		/*Cjackpot*/{new Integer(15), new Integer(15), new Double(-1.0)},	
		/*C15*/{new Integer(15), new Integer(15), new Double(500.0)},
		/*C16*/{new Integer(16), new Integer(15), new Double(500.0)},
		/*C17*/{new Integer(17), new Integer(15), new Double(500.0)},
		/*C18*/{new Integer(18), new Integer(15), new Double(500.0)},
		/*C19*/{new Integer(19), new Integer(15), new Double(500.0)},
		/*C20*/{new Integer(20), new Integer(15), new Double(500.0)},
		/*C21*/{new Integer(21), new Integer(15), new Double(500.0)},
		/*C22*/{new Integer(22), new Integer(15), new Double(500.0)},
		/*C23*/{new Integer(23), new Integer(15), new Double(500.0)},
		/*C24*/{new Integer(24), new Integer(15), new Double(500.0)},
		/*C25*/{new Integer(25), new Integer(15), new Double(500.0)},
		/*C26*/{new Integer(26), new Integer(15), new Double(500.0)},
		/*C27*/{new Integer(27), new Integer(15), new Double(500.0)},
		/*C28*/{new Integer(28), new Integer(15), new Double(500.0)},
		/*C29*/{new Integer(29), new Integer(15), new Double(500.0)},
		/*C30*/{new Integer(30), new Integer(15), new Double(500.0)},
		/*C31*/{new Integer(31), new Integer(15), new Double(500.0)},
		/*C32*/{new Integer(32), new Integer(15), new Double(500.0)},
		/*C33*/{new Integer(33), new Integer(15), new Double(500.0)},
		/*C34*/{new Integer(34), new Integer(15), new Double(500.0)},
		
		/*C35*/{new Integer(35), new Integer(15), new Double(200.0)},
		/*C36*/{new Integer(36), new Integer(15), new Double(200.0)},
		/*C37*/{new Integer(37), new Integer(15), new Double(200.0)},
		/*C38*/{new Integer(38), new Integer(15), new Double(200.0)},
		/*C39*/{new Integer(39), new Integer(15), new Double(200.0)},
		/*C40*/{new Integer(40), new Integer(15), new Double(200.0)},
		/*C41*/{new Integer(41), new Integer(15), new Double(200.0)},
		/*C42*/{new Integer(42), new Integer(15), new Double(200.0)},
		/*C43*/{new Integer(43), new Integer(15), new Double(200.0)},
		/*C44*/{new Integer(44), new Integer(15), new Double(200.0)},
		
		/*C45*/{new Integer(45), new Integer(15), new Double(100.0)},
		/*C46*/{new Integer(46), new Integer(15), new Double(100.0)},
		/*C47*/{new Integer(47), new Integer(15), new Double(100.0)},
		/*C48*/{new Integer(48), new Integer(15), new Double(100.0)},
		/*C49*/{new Integer(49), new Integer(15), new Double(100.0)},
		/*C50*/{new Integer(50), new Integer(15), new Double(100.0)},
		/*C51*/{new Integer(51), new Integer(15), new Double(100.0)},
		/*C52*/{new Integer(52), new Integer(15), new Double(100.0)},
		/*C53*/{new Integer(53), new Integer(15), new Double(100.0)},
		/*C54*/{new Integer(54), new Integer(15), new Double(100.0)},
		/*C55*/{new Integer(55), new Integer(15), new Double(100.0)},
		
		/*C56*/{new Integer(56), new Integer(15), new Double(50.0)},
		/*C57*/{new Integer(57), new Integer(15), new Double(50.0)},
		/*C58*/{new Integer(58), new Integer(15), new Double(50.0)},
		/*C59*/{new Integer(59), new Integer(15), new Double(50.0)},
		/*C60*/{new Integer(60), new Integer(15), new Double(50.0)},
		/*C61*/{new Integer(61), new Integer(15), new Double(50.0)},
		/*C62*/{new Integer(62), new Integer(15), new Double(50.0)},
		/*C63*/{new Integer(63), new Integer(15), new Double(50.0)},
		/*C64*/{new Integer(64), new Integer(15), new Double(50.0)},
		/*C65*/{new Integer(65), new Integer(15), new Double(50.0)},
		
		/*C66*/{new Integer(66), new Integer(15), new Double(10.0)},
		/*C67*/{new Integer(67), new Integer(15), new Double(10.0)},
		/*C68*/{new Integer(68), new Integer(15), new Double(10.0)},
		/*C69*/{new Integer(69), new Integer(15), new Double(10.0)},
		/*C70*/{new Integer(70), new Integer(15), new Double(10.0)},
		/*C71*/{new Integer(71), new Integer(15), new Double(10.0)},
		/*C72*/{new Integer(72), new Integer(15), new Double(10.0)},
		/*C73*/{new Integer(73), new Integer(15), new Double(10.0)},
		/*C74*/{new Integer(74), new Integer(15), new Double(10.0)},
		/*C75*/{new Integer(75), new Integer(15), new Double(10.0)},
		
		/*C76*/{new Integer(76), new Integer(15), new Double(1.0)},
		/*C77*/{new Integer(77), new Integer(15), new Double(1.0)},
		/*C78*/{new Integer(78), new Integer(15), new Double(1.0)},
		/*C79*/{new Integer(79), new Integer(15), new Double(1.0)},
		/*C80*/{new Integer(80), new Integer(15), new Double(1.0)},
		
		/*C81*/{new Integer(81), new Integer(15), new Double(0.0)},
		/*C82*/{new Integer(82), new Integer(15), new Double(0.0)},
		/*C83*/{new Integer(83), new Integer(15), new Double(0.0)},
		/*C84*/{new Integer(84), new Integer(15), new Double(0.0)},
		/*C85*/{new Integer(85), new Integer(15), new Double(0.0)},
		/*C86*/{new Integer(86), new Integer(15), new Double(0.0)},
		/*C87*/{new Integer(87), new Integer(15), new Double(0.0)},
		/*C88*/{new Integer(88), new Integer(15), new Double(0.0)},
		/*C89*/{new Integer(89), new Integer(15), new Double(0.0)},
		/*C90*/{new Integer(90), new Integer(15), new Double(0.00)},
		
		
		// Line payouts
		/*C01*/{new Integer(1), new Integer(5), new Double(0.0)},
		/*C02*/{new Integer(2), new Integer(5), new Double(0.0)},
		/*C03*/{new Integer(3), new Integer(5), new Double(0.0)},
		/*C04*/{new Integer(4), new Integer(5), new Double(0.0)},
		
		/*C05*/{new Integer(5), new Integer(5), new Double(50.0)},
		/*C06*/{new Integer(6), new Integer(5), new Double(50.0)},
		/*C07*/{new Integer(7), new Integer(5), new Double(50.0)},
		/*C08*/{new Integer(8), new Integer(5), new Double(50.0)},
		/*C09*/{new Integer(9), new Integer(5), new Double(50.0)},
		
		/*C10*/{new Integer(10), new Integer(5), new Double(40.0)},
		/*C11*/{new Integer(11), new Integer(5), new Double(40.0)},
		/*C12*/{new Integer(12), new Integer(5), new Double(40.0)},
		/*C13*/{new Integer(13), new Integer(5), new Double(40.0)},
		/*C14*/{new Integer(14), new Integer(5), new Double(40.0)},
		

		/*C15*/{new Integer(15), new Integer(5), new Double(30.0)},
		/*C16*/{new Integer(16), new Integer(5), new Double(30.0)},
		/*C17*/{new Integer(17), new Integer(5), new Double(30.0)},
		/*C18*/{new Integer(18), new Integer(5), new Double(30.0)},
		/*C19*/{new Integer(19), new Integer(5), new Double(30.0)},
		
		/*C20*/{new Integer(20), new Integer(5), new Double(20.0)},
		/*C21*/{new Integer(21), new Integer(5), new Double(20.0)},
		/*C22*/{new Integer(22), new Integer(5), new Double(20.0)},
		/*C23*/{new Integer(23), new Integer(5), new Double(20.0)},
		/*C24*/{new Integer(24), new Integer(5), new Double(20.0)},
		
		/*C25*/{new Integer(25), new Integer(5), new Double(10.0)},
		/*C26*/{new Integer(26), new Integer(5), new Double(10.0)},
		/*C27*/{new Integer(27), new Integer(5), new Double(10.0)},
		/*C28*/{new Integer(28), new Integer(5), new Double(10.0)},
		/*C29*/{new Integer(29), new Integer(5), new Double(10.0)},
		
		/*C30*/{new Integer(30), new Integer(5), new Double(5.0)},
		/*C31*/{new Integer(31), new Integer(5), new Double(5.0)},
		/*C32*/{new Integer(32), new Integer(5), new Double(5.0)},
		/*C33*/{new Integer(33), new Integer(5), new Double(5.0)},
		/*C34*/{new Integer(34), new Integer(5), new Double(5.0)},
		
		/*C35*/{new Integer(35), new Integer(5), new Double(1.0)},
		/*C36*/{new Integer(36), new Integer(5), new Double(1.0)},
		/*C37*/{new Integer(37), new Integer(5), new Double(1.0)},
		/*C38*/{new Integer(38), new Integer(5), new Double(1.0)},
		/*C39*/{new Integer(39), new Integer(5), new Double(1.0)},
		/*C40*/{new Integer(40), new Integer(5), new Double(0.0)},
		/*C41*/{new Integer(41), new Integer(5), new Double(0.0)},
		/*C42*/{new Integer(42), new Integer(5), new Double(0.0)},
		/*C43*/{new Integer(43), new Integer(5), new Double(0.0)},
		/*C44*/{new Integer(44), new Integer(5), new Double(0.0)},
		
		/*C45*/{new Integer(45), new Integer(5), new Double(0.0)},
		/*C46*/{new Integer(46), new Integer(5), new Double(0.0)},
		/*C47*/{new Integer(47), new Integer(5), new Double(0.0)},
		/*C48*/{new Integer(48), new Integer(5), new Double(0.0)},
		/*C49*/{new Integer(49), new Integer(5), new Double(0.0)},
		/*C50*/{new Integer(50), new Integer(5), new Double(0.0)},
		/*C51*/{new Integer(51), new Integer(5), new Double(0.0)},
		/*C52*/{new Integer(52), new Integer(5), new Double(0.0)},
		/*C53*/{new Integer(53), new Integer(5), new Double(0.0)},
		/*C54*/{new Integer(54), new Integer(5), new Double(0.0)},
		/*C55*/{new Integer(55), new Integer(5), new Double(0.0)},
		/*C56*/{new Integer(56), new Integer(5), new Double(0.0)},
		/*C57*/{new Integer(57), new Integer(5), new Double(0.0)},
		/*C58*/{new Integer(58), new Integer(5), new Double(0.0)},
		/*C59*/{new Integer(59), new Integer(5), new Double(0.0)},
		/*C60*/{new Integer(60), new Integer(5), new Double(0.0)},
		/*C61*/{new Integer(61), new Integer(5), new Double(0.0)},
		/*C62*/{new Integer(62), new Integer(5), new Double(0.0)},
		/*C63*/{new Integer(63), new Integer(5), new Double(0.0)},
		/*C64*/{new Integer(64), new Integer(5), new Double(0.0)},
		/*C65*/{new Integer(65), new Integer(5), new Double(0.0)},
		
		/*C66*/{new Integer(66), new Integer(5), new Double(0.0)},
		/*C67*/{new Integer(67), new Integer(5), new Double(0.0)},
		/*C68*/{new Integer(68), new Integer(5), new Double(0.0)},
		/*C69*/{new Integer(69), new Integer(5), new Double(0.0)},
		/*C70*/{new Integer(70), new Integer(5), new Double(0.0)},
		/*C71*/{new Integer(71), new Integer(5), new Double(0.0)},
		/*C72*/{new Integer(72), new Integer(5), new Double(0.0)},
		/*C73*/{new Integer(73), new Integer(5), new Double(0.0)},
		/*C74*/{new Integer(74), new Integer(5), new Double(0.0)},
		/*C75*/{new Integer(75), new Integer(5), new Double(0.0)},
		/*C76*/{new Integer(76), new Integer(5), new Double(0.0)},
		/*C77*/{new Integer(77), new Integer(5), new Double(0.0)},
		/*C78*/{new Integer(78), new Integer(5), new Double(0.0)},
		/*C79*/{new Integer(79), new Integer(5), new Double(0.0)},
		/*C80*/{new Integer(80), new Integer(5), new Double(0.0)},
		/*C81*/{new Integer(81), new Integer(5), new Double(0.0)},
		/*C82*/{new Integer(82), new Integer(5), new Double(0.0)},
		/*C83*/{new Integer(83), new Integer(5), new Double(0.0)},
		/*C84*/{new Integer(84), new Integer(5), new Double(0.0)},
		/*C85*/{new Integer(85), new Integer(5), new Double(0.0)},
		/*C86*/{new Integer(86), new Integer(5), new Double(0.0)},
		/*C87*/{new Integer(87), new Integer(5), new Double(0.0)},
		/*C88*/{new Integer(88), new Integer(5), new Double(0.0)},
		/*C89*/{new Integer(89), new Integer(5), new Double(0.0)},
		/*C90*/{new Integer(90), new Integer(5), new Double(0.0)},
		

	};
	
	public double getMultiplier(int ballsTowin, int numers) {
		for (int i = 0; i < getWinCombos().length; i++) {
			Object[] combo = getWinCombo(i);
			int betnumbers = (Integer)combo[0];
			int numberCase = (Integer)combo[1];
			if(betnumbers==ballsTowin && numberCase == numers){
				if((Double) combo[2]>0.0)
				return (Double) combo[2];
			}
		}
		return 0.0;
	}

	public double getJackpotPercentage(int BetLength, int hitCounter) {
		for (int i = 0; i < getWinCombos().length; i++) {
			Object[] combo = getWinCombo(i);
			int betnumbers = (Integer)combo[0];
			int numberCase = (Integer)combo[1];
			if(betnumbers==BetLength && numberCase == hitCounter && (Double)combo[2]<0.00 ){
					return -(Double) combo[2];
			}
		}
		return 0.0;
	}


}
