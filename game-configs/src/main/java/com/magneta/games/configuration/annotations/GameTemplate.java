/**
 * 
 */
package com.magneta.games.configuration.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @author anarxia
 *
 */
@Retention( RetentionPolicy.RUNTIME )
@Target( ElementType.TYPE)
public @interface GameTemplate {
    String value();
}
