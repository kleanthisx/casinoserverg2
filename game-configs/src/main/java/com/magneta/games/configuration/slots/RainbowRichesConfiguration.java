/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;

/**
 * @author User
 *
 */
@XmlRootElement 
public class RainbowRichesConfiguration extends SlotConfiguration {

	public static final String REEL_WEIGHT_0 = "reel_weight_0";
	public static final String REEL_WEIGHT_1 = "reel_weight_1";
	public static final String REEL_WEIGHT_2 = "reel_weight_2";
	public static final String REEL_WEIGHT_3 = "reel_weight_3";

	private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
		new GameTemplateSetting(REEL_WEIGHT_0, GameTemplateSetting.INTEGER, "Reel Set 0 Weight", 2797),
		new GameTemplateSetting(REEL_WEIGHT_1, GameTemplateSetting.INTEGER, "Reel Set 1 Weight", 7121),
		new GameTemplateSetting(REEL_WEIGHT_2, GameTemplateSetting.INTEGER, "Reel Set 2 Weight", 25),
		new GameTemplateSetting(REEL_WEIGHT_3, GameTemplateSetting.INTEGER, "Reel Set 3 Weight", 57),
	};

	private static final int MAX_SYMBOL = 10;

	public static final int WILD_SYMBOL = 1;

	public static final int ROAD_SCATTER = 8;
	public static final int WELL_SCATTER = 9;
	public static final int POT_SCATTER = 10;

	public static final int[][] POT_WEIGHTS = {
		{500 , 833},
		{ 70 , 833},
		{120 , 833},
		{ 60 , 833},
		{250 , 833},
		{ 90 , 833},
		{140 , 833},
		{ 50 , 833},
		{200 , 833},
		{100 , 833},
		{150 , 833},
		{ 80 , 833},
	};

	public static final int[][][] ROAD_BONUS_WEIGHTS = {
		/* 3 */ {
			{ 8  , 130 },
			{ 9  , 430 },
			{ 10 , 900 },
			{ 11 , 1190},
			{ 12 , 1370},
			{ 13 , 1420},
			{ 14 , 1290},
			{ 15 , 1030},
			{ 20 , 960 },
			{ 30 , 650 },
			{ 40 , 340 },
			{ 50 , 120 },
			{ 60 , 80  },
			{ 75 , 60  },
			{ 100, 10  },
			{ 175, 10  },
			{ 200, 10  }
		},
		/* 4 */ {
			{ 27 , 380 },
			{ 30 , 900 },
			{ 33 , 1120},
			{ 36 , 1150},
			{ 39 , 1220},
			{ 42 , 1140},
			{ 45 , 940 },
			{ 48 , 740 },
			{ 60 , 530 },
			{ 75 , 430 },
			{ 90 , 320 },
			{ 150, 200 },
			{ 225, 220 },
			{ 300, 630 },
			{ 350, 30  },
			{ 400, 50  }
		},
		/*	5 */{
			{ 65 , 260 },
			{ 70 , 550 },
			{ 75 , 710 },
			{ 80 , 900 },
			{ 100, 1010},
			{ 125, 1110},
			{ 150, 1120},
			{ 200, 1110},
			{ 250, 1120},
			{ 300, 850 },
			{ 400, 590 },
			{ 500, 660 }
		}
	};

	public static final int[][][] WELL_BONUS_WEIGHTS = {
		/* 3 */ {
			{ 2 , 2069},
			{ 3 , 345 },
			{ 4 , 1034},
			{ 5 , 1724},
			{ 6 , 1379},
			{ 8 , 690 },
			{10 , 690 },
			{12 , 345 },
			{15 , 103 },
			{20 , 345 },
			{25 , 345 }
		},
		/* 4 */ {
			{ 20, 2000},
			{ 25, 2000},
			{ 30, 1500},
			{ 35, 250 },
			{ 40, 1250},
			{ 50, 1000},
			{ 60, 500 },
			{ 70, 500 },
			{ 80, 500 },
			{100, 500 }
		},
		/*	5 */{
			{ 250, 2000},
			{ 200, 2000},
			{ 300, 2400},
			{ 400, 2000},
			{ 500, 2000},
		}
	};



	private static final int[][][] PAYLINES = {
		{ {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} }, /* 1  */
		{ {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} }, /* 2  */
		{ {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} }, /* 3  */
		{ {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 4  */
		{ {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 5  */
		{ {0, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 6  */ 
		{ {2, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 7  */
		{ {1, 0}, {0, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 8  */
		{ {1, 0}, {2, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 9  */
		{ {0, 0}, {1, 1}, {1, 2}, {1, 3}, {2, 4} }, /* 10 */
		{ {2, 0}, {1, 1}, {1, 2}, {1, 3}, {0, 4} }, /* 11 */
		{ {1, 0}, {0, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 12 */
		{ {1, 0}, {2, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 13 */
		{ {1, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 14 */
		{ {1, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 15 */
		{ {0, 0}, {0, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 16 */
		{ {2, 0}, {2, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 17 */
		{ {1, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 18 */
		{ {1, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 19 */
		{ {0, 0}, {0, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 20 */
	};

	//reelstrips 
	private static final int[][][] REELS = {

		{
			/*reel 1:*/
			{
				4,5,7,1,4,7,1,6,7,1, 
				3,5,2,2,2,5,7,2,2,2,      
				6,4,3,5,4,7,6,8,7,6,      
				8,7,6,8,7,6,3,5,9,3,   
				5,9,3,5,9,3,5,6,7,5,   
				6,7,5,6,7,4,6,7,4,6,    
				7,4,6,7              
			},
			/*reel 2:*/
			{
				7,6,4,1,6,7,1,6,7,5,    
				6,7,2,2,2,6,7,2,2,2,       
				6,7,2,6,5,3,6,10,7,6,    
				10,7,6,5,3,7,4,5,8,4,       
				5,8,4,5,3,4,5,7,3,9,       
				7,5,9,7,3,4,6,5,7,6,        
				5,7,6,5  
			},
			/*reel 3:*/
			{
				3,6,7,1,6,7,1,6,7,5, 
				6,7,2,2,2,6,7,2,2,2, 
				6,7,2,6,7,5,4,10,5,4, 
				10,5,4,6,7,8,6,7,8,6, 
				3,7,6,9,7,6,9,7,5,9, 
				7,3,5,4,6,5,3,4,5,3, 
				4,5,6,7 
			},
			/*reel 4:*/
			{
				3,5,4,1,6,7,1,5,3,7,   
				6,3,2,2,2,3,7,2,2,2,   
				7,3,2,2,6,5,4,10,5,4, 
				10,5,4,7,6,3,7,5,8,4,   
				3,8,7,6,4,5,3,6,4,9,    
				6,5,9,4,7,6,5,3,6,4,
				5,7,6,4  
			},
			/*reel 5:*/
			{
				5,3,4,1,5,3,1,4,3,7, 
				6,5,2,2,2,7,4,2,2,2, 
				6,3,2,2,2,7,5,8,4,6, 
				8,7,3,6,5,7,3,4,9,5, 
				4,9,6,5,7,4,3,7,5,6, 
				4,7,5,6,3,7,4,5,7,3, 
				6,7,3,4 
			}
		},

		/*reelstrips rgstrips2*/
		{
			//reel 1:
			{
				4,5,7,1,4,5,3,4,7,5, 
				3,7,2,2,2,5,6,4,5,3, 
				6,4,5,6,4,7,6,8,7,6, 
				8,7,6,8,7,6,3,5,9,3,  
				6,9,3,7,9,3,5,6,7,5, 
				6,7,5,6,7,4,6,7,4,6,  
				7,4,6,7 
			},
			//reel 2:
			{
				7,6,4,1,6,7,1,6,7,5, 
				6,7,2,2,2,6,7,5,3,4,  
				5,7,3,6,5,3,6,10,7,6, 
				10,7,6,5,3,7,4,5,6,4,  
				5,8,4,7,8,4,6,7,3,9,  
				7,3,9,7,3,4,6,5,7,6,   
				5,7,6,5  
			},
			//reel 3:
			{
				3,6,7,1,6,7,1,6,7,5, 
				6,7,2,2,2,5,7,2,2,2, 
				6,7,2,6,7,5,4,10,5,4,  
				10,5,4,6,7,8,6,7,8,6, 
				3,7,6,4,7,6,9,7,6,9, 
				7,6,3,4,6,5,3,7,4,5,  
				6,3,4,7  
			},
			//reel 4:
			{
				3,5,4,1,6,7,4,5,3,7,  
				6,3,2,2,2,7,5,2,2,2, 
				4,3,2,2,6,5,7,10,5,4,  
				10,5,4,7,6,3,7,5,6,4,  
				3,8,7,6,8,5,3,7,4,9,   
				6,5,9,4,7,6,5,3,6,4,   
				3,7,6,4                
			},
			//reel 5:
			{
				5,3,4,7,5,3,1,4,3,7,   
				6,5,2,2,2,7,4,2,2,2,   
				6,7,2,2,2,7,5,8,4,6,   
				8,7,3,6,5,7,3,4,9,5,   
				4,9,6,5,7,4,3,6,5,3,   
				4,7,5,6,3,7,4,5,7,3,   
				6,7,3,4                
			}
		},

		//reelstrips rgstrips3
		{
			//reel 1:
			{
				4,5,1,1,1,6,7,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,7,5,8,3,5,8,3,5,8,   
				3,5,6,7,9,4,6,9,7,4,   
				9,6,7,4,6,3,7,6,5,3,   
				7,4,6,7                
			},
			//reel 2:
			{
				7,6,1,1,7,3,5,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,6,7,10,4,7,10,4,7,8, 
				6,4,8,6,7,8,6,4,3,5,   
				9,3,5,9,3,5,4,3,7,6,   
				5,7,6,5                
			},
			//reel 3:
			{
				3,6,1,1,5,6,4,2,2,2,   
				7,2,2,2,6,2,2,2,7,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,5,3,10,5,3,10,5,3,8, 
				7,4,8,6,7,9,4,7,6,3,   
				5,9,7,4,6,2,2,2,3,5,   
				7,6,4,7                
			},
			//reel 4:
			{
				3,5,1,1,4,7,5,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,6,7,10,3,5,4,7,8,5,  
				4,8,5,3,8,4,6,7,3,9,   
				5,6,9,4,6,9,5,3,6,5,   
				10,3,6,4               
			},
			//reel 5:
			{
				5,3,1,1,3,5,4,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,2,2,2,2,2,2,2,2,2,   
				2,4,6,8,5,7,8,6,7,5,   
				4,9,7,6,9,3,5,7,4,6,   
				3,7,4,5,7,3,6,4,3,7,   
				6,5,3,4                
			}
		},

		//reelstrips rgstrips4
		{
			//reel 1:
			{
				4,5,7,1,4,7,1,6,7,1,    
				3,7,2,2,2,5,7,2,2,2,    
				6,4,3,5,4,7,6,8,7,6,    
				8,7,6,8,7,6,3,5,9,3,    
				5,9,3,5,9,3,5,6,7,4,    
				6,7,5,6,7,4,6,7,4,6,    
				7,4,6,7                 
			},
			//	reel 2:
			{
				7,6,5,1,6,7,1,6,7,2,    
				2,2,6,7,2,2,2,2,3,6,    
				7,10,5,3,10,5,3,10,6,7, 
				10,6,7,5,6,7,4,5,8,4,   
				5,8,4,5,8,4,5,7,3,9,    
				7,3,9,7,3,4,6,5,10,6,   
				4,10,6,5                
			},
			//reel 3:
			{
				3,6,7,1,6,7,1,5,4,2,    
				2,2,5,7,2,2,2,2,6,5,    
				4,10,5,3,10,4,5,10,3,4, 
				10,5,4,6,7,8,5,7,8,5,   
				3,7,6,9,7,6,9,7,6,9,    
				7,6,3,4,6,5,3,7,10,5,   
				6,10,4,7                
			},
			//reel 4:
			{
				3,5,7,1,6,7,1,5,3,2,    
				2,2,3,6,2,2,2,2,5,6,    
				7,10,4,5,10,6,4,10,7,4, 
				10,6,7,4,6,3,7,5,8,4,   
				3,8,7,6,8,5,3,7,4,9,    
				6,5,9,4,7,9,5,3,10,4,   
				5,10,6,4                
			},
			//reel 5:
			{
				5,3,4,1,5,3,1,4,3,7, 
				6,5,2,2,2,7,4,2,2,2, 
				6,3,2,2,2,7,5,8,4,6, 
				8,7,3,6,5,7,3,4,9,5, 
				4,9,6,5,7,4,6,3,5,6, 
				4,7,5,6,3,7,4,5,7,3, 
				6,7,3,4              
			}
		}
	};

	private static final Object[][] WIN_CONSTRAINTS = {
		/*Symbol         Quantity Req.   Multiplier*/
		/*C00*/{new Integer(5), new Integer(3), new Double(5.0)},
		/*C01*/{new Integer(5), new Integer(4), new Double(10.0)},
		/*C02*/{new Integer(5), new Integer(5), new Double(100.0)},

		/*C03*/{new Integer(6), new Integer(3), new Double(5.0)},
		/*C04*/{new Integer(6), new Integer(4), new Double(10.0)},
		/*C05*/{new Integer(6), new Integer(5), new Double(100.0)},

		/*C06*/{new Integer(7), new Integer(3), new Double(5.0)},
		/*C07*/{new Integer(7), new Integer(4), new Double(10.0)},
		/*C08*/{new Integer(7), new Integer(5), new Double(100.0)},
		
		/*C09*/{new Integer(3), new Integer(3), new Double(10.0)},
		/*C10*/{new Integer(3), new Integer(4), new Double(50.0)},
		/*C11*/{new Integer(3), new Integer(5), new Double(150.0)},

		/*C12*/{new Integer(4), new Integer(3), new Double(10.0)},
		/*C13*/{new Integer(4), new Integer(4), new Double(50.0)},
		/*C14*/{new Integer(4), new Integer(5), new Double(150.0)},
		
		/*C15*/{new Integer(2), new Integer(2), new Double(2.0)},
		/*C16*/{new Integer(2), new Integer(3), new Double(50.0)},
		/*C17*/{new Integer(2), new Integer(4), new Double(200.0)},
		/*C18*/{new Integer(2), new Integer(5), new Double(250.0)},

		/*C19*/{new Integer(1), new Integer(2), new Double(2.0)},
		/*C20*/{new Integer(1), new Integer(3), new Double(60.0)},
		/*C21*/{new Integer(1), new Integer(4), new Double(200.0)},
		/*C22*/{new Integer(1), new Integer(5), new Double(500.0)},

		/*C23*/{new Integer(-8), new Integer(3), new Double(0.0)},
		/*C24*/{new Integer(-8), new Integer(4), new Double(0.0)},
		/*C25*/{new Integer(-8), new Integer(5), new Double(0.0)},

		/*C26*/{new Integer(-9), new Integer(3), new Double(0.0)},
		/*C27*/{new Integer(-9), new Integer(4), new Double(0.0)},
		/*C28*/{new Integer(-9), new Integer(5), new Double(0.0)},

		/*C29*/{new Integer(-10), new Integer(3), new Double(0.0)},
		/*C30*/{new Integer(-10), new Integer(4), new Double(0.0)},
		/*C31*/{new Integer(-10), new Integer(5), new Double(0.0)},

	};

	public RainbowRichesConfiguration() {
		super();
	}

	public RainbowRichesConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected int[][][] getDefaultReels() {
		return REELS;
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
	public int getSymbolCount() {
		return MAX_SYMBOL;
	}

	@Override
	public boolean isJackpotCombination(int line, int combo) {
		return combo == 22;
	}

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}

	@Override
	public int getNumberOfReelSets() {
		return REELS.length;
	}

	@Override
	public GameTemplateSetting[] getDefaultSettings() {
		return DEFAULT_SETTINGS;
	}

}
