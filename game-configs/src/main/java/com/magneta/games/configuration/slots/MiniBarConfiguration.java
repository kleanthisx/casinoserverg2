/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class MiniBarConfiguration extends SlotConfiguration {
    
    private static int MAX_SYMBOL = 11;
    
    private static final int[][][] PAYLINES = {
    		{ {1,0}, {1,1}, {1,2} }, /* 0 */
    		{ {0,0}, {0,1}, {0,2} }, /* 1 */
    		{ {2,0}, {2,1}, {2,2} }, /* 2 */ 
    		{ {0,0}, {1,1}, {2,2} }, /* 3 */
    		{ {2,0}, {1,1}, {0,2} }, /* 4 */
    		{ {1,3}, {1,2}, {1,1} }, /* 5 */
    		{ {0,3}, {0,2}, {0,1} }, /* 6 */
    		{ {2,3}, {2,2}, {2,1} }, /* 7 */
            { {2,3}, {1,2}, {0,1} }, /* 8 */
            { {0,3}, {1,2}, {2,1} }  /* 9 */
    };
    
    /*90.10*/
    private static final int[][][] REELS = {
    	{
    		{1,1,1,1,2,3,3,3,4,4,4,5,6,7,7,7,7,8,5,1,1,1,3,3,3,4,4,4,5,6,5,6,7,8,1,1,1,1,3,3,3,5,4,5,5,6,6,7,7},
    		{1,1,1,2,3,3,3,3,4,4,4,6,6,7,7,7,7,7,8,9,1,1,1,1,3,3,3,4,4,4,4,6,6,7,7,7,7,8,9,1,1,1,1,3,4,8,7,9,7},
    		{1,1,1,1,2,3,3,3,3,5,7,7,5,7,7,7,1,1,3,3,7,7,7,7,5,7,5,8,1,1,1,3,3,3,3,3,5,3,7,7,8,1,1,5,3,3,3,3,5},
    		{1,1,1,1,2,3,3,3,4,4,4,5,6,7,7,7,7,8,5,1,1,1,3,3,3,4,4,4,5,6,5,6,7,8,1,1,1,1,3,3,3,5,4,5,5,6,6,7,7},
    	}
    };
    
    /*
     * 1 = lemons
     * 2 = oranges
     * 3 = peaches
     * 4 = strawberries
     * 5 = blue peaches
     * 6 = stafili
     * 7 = watermelon
     * 8 = bells
     * 9 = orange + bar
     * 10 = lemon + bar
     * 11 = blue peach + bar
     */
    
    private static final Object[][] WIN_COMBOS = {
              /*Reel 1           Reel 2           Reel 3           Multiplier    */
        /*Lemon combos*/
        /*C1 */{new Integer(1),  new Integer(1),  new Integer(1),  new Double(4.0)},
        /*C2 */{new Integer(10),  new Integer(1),  new Integer(1),  new Double(4.0)},
        /*C3 */{new Integer(1),  new Integer(10),  new Integer(1),  new Double(4.0)},
        /*C4 */{new Integer(10),  new Integer(10),  new Integer(1),  new Double(4.0)},
        /*C5 */{new Integer(1),  new Integer(1),  new Integer(10),  new Double(4.0)},
        /*C6 */{new Integer(10),  new Integer(1),  new Integer(10),  new Double(4.0)},
        /*C7 */{new Integer(1),  new Integer(10),  new Integer(10),  new Double(4.0)},
        
        /*Orange combos*/
        /*C8 */{new Integer(2),  new Integer(2),  new Integer(2),  new Double(4.0)},
        /*C9 */{new Integer(9),  new Integer(2),  new Integer(2),  new Double(4.0)},
        /*C10*/{new Integer(2),  new Integer(9),  new Integer(2),  new Double(4.0)},
        /*C11*/{new Integer(2),  new Integer(2),  new Integer(9),  new Double(4.0)},
        /*C12*/{new Integer(9),  new Integer(9),  new Integer(2),  new Double(4.0)},
        /*C13*/{new Integer(9),  new Integer(2),  new Integer(9),  new Double(4.0)},
        /*C14*/{new Integer(2),  new Integer(9),  new Integer(9),  new Double(4.0)},
        
        /*C15*/{new Integer(3),  new Integer(3),  new Integer(3),  new Double(8.0)},
        /*C16*/{new Integer(4),  new Integer(4),  new Integer(4),  new Double(8.0)},
        
        /*Blue Peach Combos*/
        /*C17*/{new Integer(5),  new Integer(5),  new Integer(5),  new Double(8.0)},
        /*C18*/{new Integer(11),  new Integer(5),  new Integer(5),  new Double(4.0)},
        /*C19*/{new Integer(5),  new Integer(11),  new Integer(5),  new Double(4.0)},
        /*C20*/{new Integer(11),  new Integer(11),  new Integer(5),  new Double(4.0)},
        /*C21*/{new Integer(5),  new Integer(5),  new Integer(11),  new Double(4.0)},
        /*C22*/{new Integer(11),  new Integer(5),  new Integer(11),  new Double(4.0)},
        /*C23*/{new Integer(5),  new Integer(11),  new Integer(11),  new Double(4.0)},
        
        /*C24*/{new Integer(6),  new Integer(6),  new Integer(6),  new Double(16.0)},
        /*C25*/{new Integer(7),  new Integer(7),  new Integer(7),  new Double(16.0)},
        /*C26*/{new Integer(8),  new Integer(8),  new Integer(8),  new Double(32.0)},
       
        /*BAR combos*/
        /*C27*/{new Integer(9),  new Integer(9),  new Integer(9),  new Double(60.0)},
        /*C28*/{new Integer(9),  new Integer(9),  new Integer(10), new Double(60.0)},
        /*C29*/{new Integer(9),  new Integer(9),  new Integer(11), new Double(60.0)},
        /*C30*/{new Integer(9),  new Integer(10), new Integer(9),  new Double(60.0)},
        /*C31*/{new Integer(9),  new Integer(10), new Integer(10), new Double(60.0)},
        /*C32*/{new Integer(9),  new Integer(10), new Integer(11), new Double(60.0)},
        /*C33*/{new Integer(9),  new Integer(11), new Integer(9),  new Double(60.0)},
        /*C34*/{new Integer(9),  new Integer(11), new Integer(10), new Double(60.0)},
        /*C35*/{new Integer(9),  new Integer(11), new Integer(11), new Double(60.0)},
        /*C36*/{new Integer(10), new Integer(9),  new Integer(9),  new Double(60.0)},
        /*C37*/{new Integer(10), new Integer(9),  new Integer(10), new Double(60.0)},
        /*C38*/{new Integer(10), new Integer(9),  new Integer(11), new Double(60.0)},
        /*C39*/{new Integer(10), new Integer(10), new Integer(9),  new Double(60.0)},
        /*C40*/{new Integer(10), new Integer(10), new Integer(10), new Double(60.0)},
        /*C41*/{new Integer(10), new Integer(10), new Integer(11), new Double(60.0)},
        /*C42*/{new Integer(10), new Integer(11), new Integer(9),  new Double(60.0)},
        /*C43*/{new Integer(10), new Integer(11), new Integer(10), new Double(60.0)},
        /*C44*/{new Integer(10), new Integer(11), new Integer(11), new Double(60.0)},
        /*C45*/{new Integer(11), new Integer(9),  new Integer(9),  new Double(60.0)},
        /*C46*/{new Integer(11), new Integer(9),  new Integer(10), new Double(60.0)},
        /*C47*/{new Integer(11), new Integer(9),  new Integer(11), new Double(60.0)},
        /*C48*/{new Integer(11), new Integer(10), new Integer(9),  new Double(60.0)},
        /*C49*/{new Integer(11), new Integer(10), new Integer(10), new Double(60.0)},
        /*C50*/{new Integer(11), new Integer(10), new Integer(11), new Double(60.0)},
        /*C51*/{new Integer(11), new Integer(11), new Integer(9),  new Double(60.0)},
        /*C52*/{new Integer(11), new Integer(11), new Integer(10), new Double(60.0)},
        /*C53*/{new Integer(11), new Integer(11), new Integer(11), new Double(60.0)}
    };
    
    public MiniBarConfiguration() {
        super();
    }
    
    public MiniBarConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_COMBOS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public double getPayout(int winCombo) {
        return (Double)winCombos[winCombo][3];
    }
    
    @Override
    public void setPayout(Object[][] combos, int winCombo, double payout) {
        combos[winCombo][3] = payout;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
