package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class CleopatraConfiguration extends SlotConfiguration {

	private static final int MAX_SYMBOL = 13;

	public static final int FREE_SPIN_SCATTER_1 = 13;
	public static final int WILD = 12;

	private static final int[][][] PAYLINES = {
		
		  	{ {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} }, /* 1  */ // M/M/M/M/M
	        { {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} }, /* 2  */ // T/T/T/T/T
	        { {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} }, /* 3  */ // B/B/B/B/B	        
	        { {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 4  */ // T/M/B/M/T
	        { {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 5  */ // B/M/T/M/B
	        
	        { {0, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 6  */ // T/T/M/B/B
	        { {2, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 7  */ // B/B/M/T/T
	        { {1, 0}, {0, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 8  */ // M/T/M/B/M
	        { {1, 0}, {2, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 9  */ // M/B/M/T/M
	        { {0, 0}, {1, 1}, {1, 2}, {1, 3}, {2, 4} }, /* 10 */ // T/M/M/M/B
	        
	        { {2, 0}, {1, 1}, {1, 2}, {1, 3}, {0, 4} }, /* 11 */ // B/M/M/M/T
	        { {1, 0}, {0, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 12 */ // M/T/T/M/B
	        { {1, 0}, {2, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 13 */ // M/B/B/M/T
	        { {1, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 14 */ // M/M/T/M/B
	        { {1, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 15 */ // M/M/B/M/T
	        
	        { {0, 0}, {0, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 16 */ // T/T/M/B/M
	        { {2, 0}, {2, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 17 */ // B/B/M/T/M
	        { {1, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 18 */ // M/T/M/B/B
	        { {1, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 19 */ // M/B/M/T/T
	        { {0, 0}, {0, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 20 */ // T/T/T/M/B

	};

	private static final int[][][] REELS = {
		{
    		/*Reel 1*/{13,8,9,12,8,6,2,7,1,8,5,7,4,10,1,11,3,10,5,8,10,3,9,6,4,10,8,2,11,10},
    		/*Reel 2*/{5,11,8,3,7,1,9,2,10,1,11,13,6,4,10,7,2,9,8,5,9,4,8,9,3,8,6,12,8,9},
    		/*Reel 3*/{3,7,9,12,8,10,3,11,9,1,6,10,4,6,7,1,11,10,2,8,11,4,7,5,11,13,7,11,5,10},
    		/*Reel 4*/{4,11,6,2,11,6,12,7,11,5,6,2,9,1,10,4,9,3,8,5,9,10,4,6,8,13,10,7,3,9},
    		/*Reel 5*/{2,7,10,12,7,11,1,10,5,6,9,13,11,6,4,8,6,4,10,5,8,3,11,12,9,2,8,7,5,9,10,3,8,4,11,1,6,11,3,10,9}
    	}
	};

	private static final Object[][] WIN_CONSTRAINTS = {
        /*Symbol         Quantity Req.   Multiplier*/
		
		/*C00*/{new Integer(12), new Integer(2), new Double(10.0   )},
        /*C01*/{new Integer(12), new Integer(3), new Double(200.0  )},
        /*C02*/{new Integer(12), new Integer(4), new Double(2000.0 )},
        /*C03*/{new Integer(12), new Integer(5), new Double(10000.0)},
		
    	/*C04*/{new Integer(1), new Integer(2), new Double(2.0)},
        /*C05*/{new Integer(1), new Integer(3), new Double(25.0)},
        /*C06*/{new Integer(1), new Integer(4), new Double(100.0)},
        /*C07*/{new Integer(1), new Integer(5), new Double(750.0)},
        
        /*C08*/{new Integer(2), new Integer(2), new Double(2.0)},
        /*C09*/{new Integer(2), new Integer(3), new Double(25.0)},
        /*C10*/{new Integer(2), new Integer(4), new Double(100.0)},
        /*C11*/{new Integer(2), new Integer(5), new Double(750.0)},
        
        /*C12*/{new Integer(3), new Integer(3), new Double(15.0)},
        /*C13*/{new Integer(3), new Integer(4), new Double(100.0)},
        /*C14*/{new Integer(3), new Integer(5), new Double(400.0)},
        
        /*C15*/{new Integer(4), new Integer(3), new Double(10.0)},
        /*C16*/{new Integer(4), new Integer(4), new Double(75.0)},
        /*C17*/{new Integer(4), new Integer(5), new Double(250.0)},
        
        /*C18*/{new Integer(5), new Integer(3), new Double(10.0)},
        /*C19*/{new Integer(5), new Integer(4), new Double(50.0)},
        /*C20*/{new Integer(5), new Integer(5), new Double(250.0)},
        
        /*C21*/{new Integer(6), new Integer(3), new Double(10.0)},
        /*C22*/{new Integer(6), new Integer(4), new Double(50.0)},
        /*C23*/{new Integer(6), new Integer(5), new Double(125.0)},
        
        /*C24*/{new Integer(7), new Integer(3), new Double(5.0)},
        /*C25*/{new Integer(7), new Integer(4), new Double(50.0)},
        /*C26*/{new Integer(7), new Integer(5), new Double(100.0)},
        
        /*C27*/{new Integer(8), new Integer(3), new Double(5.0)},
        /*C28*/{new Integer(8), new Integer(4), new Double(25.0)},
        /*C29*/{new Integer(8), new Integer(5), new Double(100.0)},
        
        /*C30*/{new Integer(9), new Integer(3), new Double(5.0)},
        /*C31*/{new Integer(9), new Integer(4), new Double(25.0)},
        /*C32*/{new Integer(9), new Integer(5), new Double(100.0)},
        
        /*C33*/{new Integer(10), new Integer(3), new Double(5.0)},
        /*C34*/{new Integer(10), new Integer(4), new Double(25.0)},
        /*C35*/{new Integer(10), new Integer(5), new Double(100.0)},
        
        /*C36*/{new Integer(11), new Integer(2), new Double(2.0)},
        /*C37*/{new Integer(11), new Integer(3), new Double(5.0)},
        /*C38*/{new Integer(11), new Integer(4), new Double(25.0)},
        /*C39*/{new Integer(11), new Integer(5), new Double(100.0)},
        
        /*C40*/{new Integer(-13), new Integer(2), new Double(2.0  )}, /* Bonus payouts */
        /*C41*/{new Integer(-13), new Integer(3), new Double(5.0  )}, 
        /*C42*/{new Integer(-13), new Integer(4), new Double(20.0 )},
        /*C43*/{new Integer(-13), new Integer(5), new Double(100.0)},
		
	};

	public CleopatraConfiguration() {
		super();
	}

	public CleopatraConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected int[][][] getDefaultReels() {
		return REELS;
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
	public int getSymbolCount() {
		return MAX_SYMBOL;
	}

	@Override
	public boolean isJackpotCombination(int line, int combo) {
		return combo == 3;
	}

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}

	@Override
	public int getNumberOfReelSets() {
		return REELS.length;
	}

}
