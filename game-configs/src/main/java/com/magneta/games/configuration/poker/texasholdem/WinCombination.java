package com.magneta.games.configuration.poker.texasholdem;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class WinCombination implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3641863605583127295L;
	private double amount;
	private WinType type;
	private int triggerCombo;
	
	public WinCombination(int triggerCombo, WinType winType, double amount) {
		this.setTriggerCombo(triggerCombo);
		this.setType(winType);
		this.setAmount(amount);
	}
	
	public WinCombination() {
	}

	@XmlElement
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@XmlElement
	public WinType getType() {
		return type;
	}

	public void setType(WinType type) {
		this.type = type;
	}

	@XmlElement
	public int getTriggerCombo() {
		return triggerCombo;
	}

	public void setTriggerCombo(int triggerCombo) {
		this.triggerCombo = triggerCombo;
	}
}