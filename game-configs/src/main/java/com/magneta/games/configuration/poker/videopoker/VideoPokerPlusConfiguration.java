/**
 * 
 */
package com.magneta.games.configuration.poker.videopoker;

import static com.magneta.games.configuration.poker.PokerConstants.*;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.games.configuration.poker.PokerConfiguration;

@XmlRootElement 
public class VideoPokerPlusConfiguration extends PokerConfiguration {
    
    public static final double[] multipliersLocal;

    public VideoPokerPlusConfiguration() {
        super();
    }
    
    public VideoPokerPlusConfiguration(long configId, String config) {
        super(configId,config);
    }
    
    static {
        multipliersLocal = new double[10];
        multipliersLocal[NONE] = 0.0;
        multipliersLocal[JACKS_OR_BETTER] = 1.0;
        multipliersLocal[TWO_PAIRS] = 2.0;
        multipliersLocal[THREE_OF_A_KIND] = 3.0;
        multipliersLocal[STRAIGHT] = 4.0;
        multipliersLocal[FLUSH] = 5.0;
        multipliersLocal[FULL_HOUSE] = 6.0;
        multipliersLocal[FOUR_OF_A_KIND] = 25.0;
        multipliersLocal[STRAIGHT_FLUSH] = 50.0;
        multipliersLocal[ROYAL_FLUSH] = 1000.0;
    }

    @Override
    protected double[] getMultipliers() {
        return multipliersLocal;
    }
   
}
