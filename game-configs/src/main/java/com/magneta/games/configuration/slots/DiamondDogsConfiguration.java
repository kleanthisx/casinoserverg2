package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;

/**
 * @author Klanthis
 *
 */
@XmlRootElement
public class DiamondDogsConfiguration extends SlotConfiguration {

	public static final String REEL_WEIGHT_0 = "reel_weight_0";
	public static final String REEL_WEIGHT_1 = "reel_weight_1";
	public static final String REEL_WEIGHT_2 = "reel_weight_2";
	
    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting(REEL_WEIGHT_0, GameTemplateSetting.INTEGER, "Reel Set 0 Weight", 50),
        new GameTemplateSetting(REEL_WEIGHT_1, GameTemplateSetting.INTEGER, "Reel Set 1 Weight", 100),
        new GameTemplateSetting(REEL_WEIGHT_2, GameTemplateSetting.INTEGER, "Reel Set 2 Weight", 100),
    };
    
	public static final int MAX_SYMBOL = 11;

	public static final int FREE_SPIN_SYMBOL = 11;
	public static final int BONUS_SYMBOL = 10; 
	public static final int WILD_SYMBOL = 9;
	
	public static final int MAXIMUM_BOXES_TO_OPEN = 9;

	public static final int FREE_SPIN_REEL_SET = 3;
	
	public static final int[][] BONUS_WEIGHTS = {
		{10  , 20},
		{20  , 18},
		{40  , 15},
		{80  , 13},
		{160 , 10},
		{320 ,  8},
		{640 ,  4},
		{1280,  2},
		//    	{0   , 30}
	};

	private static final int[][][] PAYLINES = {
		{ { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 } }, /* 0 */
		{ { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 } }, /* 1 */
		{ { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 } }, /* 2 */
		
		{ { 0, 0 }, { 1, 1 }, { 2, 2 }, { 1, 3 }, { 0, 4 } }, /* 3 */
		{ { 2, 0 }, { 1, 1 }, { 0, 2 }, { 1, 3 }, { 2, 4 } }, /* 4 */
		
		{ { 0, 0 }, { 0, 1 }, { 1, 2 }, { 0, 3 }, { 0, 4 } }, /* 5 */
		{ { 2, 0 }, { 2, 1 }, { 1, 2 }, { 2, 3 }, { 2, 4 } }, /* 6 */
		
		{ { 1, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 1, 4 } }, /* 7 */
		{ { 1, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 4 } }, /* 8 */
		
		{ { 1, 0 }, { 0, 1 }, { 1, 2 }, { 0, 3 }, { 1, 4 } }, /* 9 */
		{ { 1, 0 }, { 2, 1 }, { 1, 2 }, { 2, 3 }, { 1, 4 } }, /* 10*/
		
		{ { 0, 0 }, { 1, 1 }, { 0, 2 }, { 1, 3 }, { 0, 4 } }, /* 11*/
		{ { 2, 0 }, { 1, 1 }, { 2, 2 }, { 1, 3 }, { 2, 4 } }, /* 12*/
		
		{ { 1, 0 }, { 1, 1 }, { 0, 2 }, { 1, 3 }, { 1, 4 } }, /* 13*/
		{ { 1, 0 }, { 1, 1 }, { 2, 2 }, { 1, 3 }, { 1, 4 } }, /* 14*/
		
		{ { 0, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 0, 4 } }, /* 15*/
		{ { 2, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 2, 4 } }, /* 16*/
		
		{ { 0, 0 }, { 1, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 } }, /* 17*/
		{ { 2, 0 }, { 1, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 } }, /* 18*/
		
		{ { 0, 0 }, { 2, 1 }, { 0, 2 }, { 2, 3 }, { 0, 4 } }, /* 19*/
		{ { 2, 0 }, { 0, 1 }, { 2, 2 }, { 0, 3 }, { 2, 4 } }, /* 20*/
		
		{ { 0, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 0, 4 } }, /* 21*/
		{ { 2, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 2, 4 } }, /* 22*/
		
		{ { 0, 0 }, { 0, 1 }, { 2, 2 }, { 0, 3 }, { 0, 4 } }, /* 23*/
		{ { 2, 0 }, { 2, 1 }, { 0, 2 }, { 2, 3 }, { 2, 4 } }  /* 24*/

	};

	private static final int[][][] REELS = {
		{
			{9, 2, 3, 1, 10, 6, 5, 10, 7, 1, 10, 4, 3, 10, 6, 5, 8, 4, 2, 6, 8, 7, 2, 8, 7, 5, 3, 2, 9, 3, 10, 4, 6, 10, 4, 11, 6, 4, 7, 2, 4, 10, 6, 11, 5, 1, 9, 10, 1, 7, 5, 2, 10, 8},
			{10, 8, 3, 4, 10, 5, 3, 11, 1, 7, 4, 6, 5, 1, 10, 8, 1, 11, 9, 6, 1, 8, 11, 10, 7, 4, 10, 1, 2, 11, 1, 6, 7, 3, 2, 10, 11, 5, 7, 11, 10, 4, 11, 3, 10, 4, 3, 10, 5, 6, 7, 3, 5},
			{5, 7, 1, 3, 6, 7, 2, 6, 4, 5, 3, 9, 7, 6, 3, 5, 2, 1, 8, 2, 3, 9, 2, 6, 7, 10, 1, 2, 8, 1, 3, 7, 11, 4, 7, 5, 4, 1, 8, 6, 3, 1, 6, 2, 4, 1, 2, 5, 10, 4, 5, 10, 4},
			{2, 5, 3, 8, 6, 3, 5, 7, 8, 3, 10, 8, 3, 9, 4, 3, 6, 7, 3, 4, 5, 2, 3, 1, 2, 6, 5, 3, 6, 1, 3, 7, 5, 10, 8, 11, 7, 1, 6, 8, 7, 3, 5, 8, 2, 4, 3, 7, 4, 5, 6, 3},
			{3, 5, 7, 8, 5, 2, 3, 5, 7, 3, 10, 9, 7, 5, 1, 2, 3, 10, 8, 2, 5, 4, 11, 1, 3, 10, 7, 1, 5, 6, 1, 7, 5, 1, 6, 8, 5, 6, 8, 2, 6, 8, 7, 5, 6, 1, 7, 6, 5, 7, 2, 5, 8},
		},
		{
			{8, 6, 3, 8, 2, 5, 4, 3, 7, 8, 2, 7, 11, 4, 2, 9, 1, 6, 11, 4, 2, 3, 4, 6, 3, 1, 6, 9, 5, 1, 7, 2, 6, 7, 2, 5, 6, 1, 7, 5, 4, 8, 10, 4, 5, 9},
			{10, 3, 11, 6, 10, 7, 11, 10, 4, 7, 5, 1, 3, 11, 10, 3, 6, 10, 7, 3, 11, 5, 2, 1, 3, 7, 10, 2, 5, 1, 11, 5, 8, 6, 4, 1, 11, 4, 10, 8, 4, 10, 5, 9, 10, 11, 8, 1, 4, 3, 6, 1, 7},
			{1, 4, 5, 7, 1, 2, 7, 8, 4, 3, 6, 4, 10, 6, 3, 8, 2, 7, 1, 5, 10, 6, 7, 9, 1, 5, 2, 7, 4, 2, 3, 4, 7, 6, 3, 2, 1, 9, 2, 3, 6, 1, 4, 6, 1, 5, 3, 2, 5, 10, 8, 5},
			{3, 10, 7, 3, 4, 6, 8, 3, 2, 5, 3, 6, 8, 2, 3, 6, 7, 3, 11, 5, 7, 6, 3, 1, 8, 3, 7, 9, 8, 1, 3, 2, 8, 5, 3, 4, 5, 7, 6, 5, 7, 3, 5, 6, 2, 1, 4, 8, 10, 3, 5, 4},
			{2, 6, 3, 7, 5, 10, 6, 5, 7, 6, 1, 5, 3, 8, 7, 2, 5, 4, 3, 8, 5, 10, 1, 3, 9, 6, 7, 5, 11, 6, 5, 1, 6, 5, 1, 7, 5, 8, 7, 1, 2, 8, 5, 1, 8, 5, 2, 7, 10, 3, 2, 8, 7}
		},
		{
			{10, 6, 4, 10, 3, 2, 9, 5, 1, 8, 6, 2, 3, 11, 10, 1, 7, 10, 5, 11, 4, 10, 6, 5, 10, 4, 7, 10, 5, 4, 2, 3, 6, 2, 8, 3, 2, 9, 1, 8, 5, 6, 2, 1, 7, 9, 4, 7, 10, 8, 6, 10, 7, 4},
			{4, 5, 11, 3, 1, 8, 11, 3, 5, 4, 3, 6, 5, 3, 11, 1, 10, 11, 5, 8, 11, 6, 4, 11, 7, 1, 4, 3, 11, 2, 1, 8, 7, 6, 1, 7, 4, 2, 5, 7, 10, 9, 6, 7, 1, 3},
			{9, 3, 4, 7, 11, 5, 1, 2, 4, 1, 8, 2, 5, 6, 4, 10, 7, 2, 6, 5, 4, 6, 1, 7, 2, 3, 5, 2, 3, 7, 1, 10, 2, 1, 7, 8, 1, 6, 4, 10, 6, 5, 9, 1, 3, 2, 8, 3, 5, 7, 3, 4, 6},
			{6, 1, 3, 6, 7, 2, 3, 5, 1, 3, 5, 2, 3, 6, 4, 3, 1, 2, 6, 5, 8, 3, 5, 8, 3, 11, 8, 7, 3, 9, 7, 4, 8, 7, 4, 5, 3, 7, 5, 4, 3, 10, 2, 5, 7, 6, 8, 3, 6, 8, 10, 3},
			{4, 5, 1, 7, 6, 5, 7, 1, 5, 6, 7, 5, 10, 3, 8, 2, 7, 8, 2, 7, 5, 2, 10, 9, 5, 8, 7, 6, 3, 1, 8, 3, 1, 5, 6, 3, 5, 1, 8, 7, 5, 8, 2, 6, 3, 5, 6, 1, 10, 2, 5, 7}
		},
		{
			{9, 2, 3, 1, 10, 6, 5, 10, 7, 1, 10, 4, 3, 10, 6, 5, 8, 4, 2, 6, 8, 7, 2, 8, 7, 5, 3, 2, 9, 3, 10, 4, 6, 10, 4, 11, 6, 4, 7, 2, 4, 10, 6, 11, 5, 1, 9, 10, 1, 7, 5, 2, 10, 8},
			{10, 8, 3, 4, 10, 5, 3, 11, 1, 7, 4, 6, 5, 1, 10, 8, 1, 11, 9, 6, 1, 8, 11, 10, 7, 4, 10, 1, 2, 11, 1, 6, 7, 3, 2, 10, 11, 5, 7, 11, 10, 4, 11, 3, 10, 4, 3, 10, 5, 6, 7, 3, 5},
			{5, 7, 1, 3, 6, 7, 2, 6, 4, 5, 3, 9, 7, 6, 3, 5, 2, 1, 8, 2, 3, 9, 2, 6, 7, 10, 1, 2, 8, 1, 3, 7, 11, 4, 7, 5, 4, 1, 8, 6, 3, 1, 6, 2, 4, 1, 2, 5, 10, 4, 5, 10, 4},
			{2, 5, 3, 8, 6, 3, 5, 7, 8, 3, 10, 8, 3, 9, 4, 3, 6, 7, 3, 4, 5, 2, 3, 1, 2, 6, 5, 3, 6, 1, 3, 7, 5, 10, 8, 11, 7, 1, 6, 8, 7, 3, 5, 8, 2, 4, 3, 7, 4, 5, 6, 3},
			{3, 5, 7, 8, 5, 2, 3, 5, 7, 3, 10, 9, 7, 5, 1, 2, 3, 10, 8, 2, 5, 4, 11, 1, 3, 10, 7, 1, 5, 6, 1, 7, 5, 1, 6, 8, 5, 6, 8, 2, 6, 8, 7, 5, 6, 1, 7, 6, 5, 7, 2, 5, 8},
		}
	};

	private static final Object[][] WIN_CONSTRAINTS = {
		/*Symbol        Quantity Req.   Multiplier*/

		//REGULAR PAYLINES (ordered by payout)
		/*C00*/{new Integer(1), new Integer(3), new Double(2.0)},
		/*C01*/{new Integer(1), new Integer(4), new Double(12.0)},
		/*C02*/{new Integer(1), new Integer(5), new Double(60.0)},

		/*C03*/{new Integer(2), new Integer(3), new Double(2.0)},
		/*C04*/{new Integer(2), new Integer(4), new Double(12.0)},
		/*C05*/{new Integer(2), new Integer(5), new Double(60.0)},

		/*C06*/{new Integer(3), new Integer(3), new Double(5.0)},
		/*C07*/{new Integer(3), new Integer(4), new Double(30.0)},
		/*C08*/{new Integer(3), new Integer(5), new Double(90.0)},

		/*C09*/{new Integer(4), new Integer(3), new Double(5.0)},
		/*C10*/{new Integer(4), new Integer(4), new Double(60.0)},
		/*C11*/{new Integer(4), new Integer(5), new Double(120.0)},

		/*C12*/{new Integer(5), new Integer(3), new Double(10.0)},
		/*C13*/{new Integer(5), new Integer(4), new Double(60.0)},
		/*C14*/{new Integer(5), new Integer(5), new Double(120.0)},

		/*C15*/{new Integer(6), new Integer(3), new Double(15.0)},
		/*C16*/{new Integer(6), new Integer(4), new Double(90.0)},
		/*C17*/{new Integer(6), new Integer(5), new Double(240.0)},

		/*C18*/{new Integer(7), new Integer(3), new Double(15.0)},
		/*C19*/{new Integer(7), new Integer(4), new Double(90.0)},
		/*C20*/{new Integer(7), new Integer(5), new Double(240.0)},

		/*C18*/{new Integer(8), new Integer(3), new Double(15.0)},
		/*C19*/{new Integer(8), new Integer(4), new Double(120.0)},
		/*C20*/{new Integer(8), new Integer(5), new Double(600.0)},

		/*C21*/{new Integer(WILD_SYMBOL), new Integer(2), new Double(2.0)},
		/*C21*/{new Integer(WILD_SYMBOL), new Integer(3), new Double(200.0)},
		/*C22*/{new Integer(WILD_SYMBOL), new Integer(4), new Double(2000.0)},
		/*C23*/{new Integer(WILD_SYMBOL), new Integer(5), new Double(10000.0)},
		
		/*C24*/{new Integer(-BONUS_SYMBOL), new Integer(3), new Double(9.0)},
		/*C25*/{new Integer(-BONUS_SYMBOL), new Integer(4), new Double(9.0)},
		/*C26*/{new Integer(-BONUS_SYMBOL), new Integer(5), new Double(9.0)},

		//SCATTER COMBINATIONS
		/*C32*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(2), new Double(2.0)},
		/*C33*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(3), new Double(4.0)},
		/*C34*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(4), new Double(25.0)},
		/*C35*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(5), new Double(100.0)},
	};
	


	public DiamondDogsConfiguration() {
		super();
	}

	public DiamondDogsConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected int[][][] getDefaultReels() {
		return REELS;
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
	public int getSymbolCount() {
		return MAX_SYMBOL;
	}

	@XmlElement
	public int getBonusSymbol() {
		return BONUS_SYMBOL;
	}

	@Override
	public boolean isJackpotCombination(int line, int combo) {
		return combo == 23;
	}

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
	
    @Override
	public int getNumberOfReelSets() {
    	return REELS.length;
    }
    	
    @Override
	public GameTemplateSetting[] getDefaultSettings() {
    	return DEFAULT_SETTINGS;
    }
}
