/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class JuicyFruityConfiguration extends SlotConfiguration {

    private static final int MAX_SYMBOL = 8;
    
    private static final int[][][] PAYLINES = {
    	{ {1,0}, {1,1}, {1,2} }, /* 0 */
    	{ {0,0}, {0,1}, {0,2} }, /* 1 */
    	{ {2,0}, {2,1}, {2,2} }, /* 2 */
    	{ {0,0}, {1,1}, {2,2} }, /* 3 */
    	{ {2,0}, {1,1}, {0,2} }  /* 4 */
    };
    
    private static final int[][][] REELS = {
    	{
    		{1,1,2,2,2,3,3,3,4,4,4,6,7,8,5,1,1,1,2,2,2,3,6,1,1,1,2,2,1,3,4,4,6,7,1,1,2,3,3,3,4,1,1,3,2,1},
    		{1,1,2,2,2,3,3,4,4,6,7,8,1,1,2,2,2,5,3,4,6,1,1,1,2,3,1,1,1,3,4,4,6,7,1,1,2,3,3,3,4,1,2,3,2,1},
    		{1,1,2,2,2,3,3,3,3,4,4,6,6,7,8,5,1,1,1,1,2,2,2,4,1,1,2,2,1,3,4,4,6,7,1,1,2,3,3,3,4,1,2,3,1,1}
    	}
    };
    
    /**
     * Symbols Legend: 1 - Cherry, 2 - Melon, 3 - Banana, 4 - Lemon, 5 - Bells, 6 - Bar, 7 - Grapes, 8 - Sevens
     */
    private static final Object[][] WIN_CONSTRAINTS = {
                /*Symbol         Quantity Req.   Multiplier*/
        /*C0 */{new Integer(1),  new Integer(3), new Double(1.0)},
        /*C1 */{new Integer(2),  new Integer(3), new Double(4.0)},
        /*C2 */{new Integer(3),  new Integer(3), new Double(5.0)},
        /*C3 */{new Integer(4),  new Integer(3), new Double(10.0)},
        /*C4 */{new Integer(-5), new Integer(3), new Double(50.0)},
        /*C5 */{new Integer(6),  new Integer(3), new Double(100.0)},
        /*C6 */{new Integer(7),  new Integer(3), new Double(200.0)},
        /*C7 */{new Integer(8),  new Integer(3), new Double(500.0)}
    };
    
    public JuicyFruityConfiguration() {
        super();
    }
    
    public JuicyFruityConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return combo == 7;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
