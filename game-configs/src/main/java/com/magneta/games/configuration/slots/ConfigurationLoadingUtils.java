/**
 * 
 */
package com.magneta.games.configuration.slots;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author User
 *
 */
public class ConfigurationLoadingUtils {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationLoadingUtils.class);

	/**
	 * Parses reels from a string
	 * @param reelsString The string to parse
	 * @param noOfReels The number of reels for the game
	 * @param minSymbol The minimum symbol
	 * @param maxSymbol The maximum symbol
	 * @return
	 */
	public static int[][] parseReels(String reelsString, int noOfReels, int minSymbol, int maxSymbol){ 
		String regex = "[[0-9]{1}[,[0-9]{1}]*\\|{1}]+";

		if (reelsString != null){
			reelsString =  reelsString.replaceAll("\\s", "");
			if (reelsString.matches(regex)){
				String[] tmp = reelsString.split("\\|{1}");
				if (tmp.length < noOfReels){
					//Log.warn("Expected "+noOfReels+" in configuration string but only "+tmp.length+" found; string ignored.");
					return null;
				}
				int[][] reels = new int[noOfReels][];
				for (int i=0;i<reels.length;i++){
					//Log.debug("Parsing reel: "+tmp[i]);
					String[] nums = tmp[i].split(",");
					reels[i] = new int[nums.length];
					for (int j=0;j<nums.length;j++){
						reels[i][j] = Integer.parseInt(nums[j]);
						if (reels[i][j] < minSymbol || reels[i][j] > maxSymbol){
							//Log.warn("A number out of range was found in reels configuration string; string ignored.");
							return null;
						}
					}
				}
				return reels;
			}
		}

		return null;
	}

	/**
	 * Parses paytable from a string
	 * @param payoutsString
	 * @param combosLength
	 * @return
	 */
	public static double[] parsePayouts(String payoutsString, int combosLength) {
		try{
			if (payoutsString != null){
				double[] vals = new double[combosLength];
				String[] payouts = payoutsString.split(":");
				for(String payout:payouts) {
					String[] set = payout.split("=");
					int combo = Integer.parseInt(set[0].trim());
					if (combo >= 0 && combo < combosLength){
						vals[combo] = Double.parseDouble(set[1].trim());
					}else{
						return null;
					}
				}
				return vals;
			}
			return null;
		}catch(Throwable e) {
			logger.error("Error parsing payouts",e);
			return null;

		}
	}


}
