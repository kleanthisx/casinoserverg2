
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class NightInVeniceConfiguration extends SlotConfiguration {

    private static final int MAX_SYMBOL = 8;
    
    private static final int[][][] PAYLINES = {
    	{ {1,0}, {1,1}, {1,2}, {1,3}, {1,4}	}, /* 0 */
    	{ {0,0}, {0,1}, {0,2}, {0,3}, {0,4} }, /* 1 */
    	{ {2,0}, {2,1}, {2,2}, {2,3}, {2,4}	}, /* 2 */
    	{ {0,0}, {1,1}, {2,2}, {1,3}, {0,4}	}, /* 3 */
    	{ {2,0}, {1,1}, {0,2}, {1,3}, {2,4}	}  /* 4 */
    };
    
    /* 91.16% */
    private static final int[][][] REELS = {
    	{
    		{ 8,7,6,5,5,5,5,4,4,4,3,3,2,2,2,2,1,1,1,8,7,6,4,4,3,3,2,2,1,1,8,7,6,4,4,3,3,2,1,8,7,6,4,4,3,3,2,2,1,1},
    		{ 8,7,6,6,5,5,4,3,3,3,2,2,2,2,1,1,1,1,8,8,8,7,6,5,4,3,2,2,2,2,1,1,1,1,1,8,8,8,8,6,5,4,4,4,3,2,2,2,1,1},
    		{ 8,7,6,5,5,5,5,4,3,2,2,1,1,1,1,1,8,7,6,5,5,5,5,4,4,3,3,3,2,2,2,1,1,7,7,7,7,7,6,5,4,4,4,3,5,4,2,7,7,7},
    		{ 8,7,6,5,5,4,4,4,3,3,3,2,2,2,2,2,1,1,1,8,8,8,6,6,6,5,4,4,3,3,3,2,2,2,2,1,1,1,6,6,6,5,4,4,3,2,1,1,1,1},
    		{ 8,7,6,5,4,4,4,4,3,3,3,2,2,2,2,2,1,1,1,1,5,5,5,5,4,4,3,3,3,2,2,2,2,1,1,1,1,4,4,4,3,3,3,3,2,2,1,1,4,1}
    	}
    };

    private static final Object[][] WIN_CONSTRAINTS = {
        /*Symbol         Quantity Req.   Multiplier*/
        /*C1 */{new Integer(1), new Integer(3), new Double(2.0)},
        /*C2 */{new Integer(1), new Integer(4), new Double(5.0)},
        /*C3 */{new Integer(1), new Integer(5), new Double(10.0)},
        /*C4 */{new Integer(2), new Integer(3), new Double(2.0)},
        /*C5 */{new Integer(2), new Integer(4), new Double(5.0)},
        /*C6 */{new Integer(2), new Integer(5), new Double(10.0)},
        /*C7 */{new Integer(3), new Integer(3), new Double(5.0)},
        /*C8 */{new Integer(3), new Integer(4), new Double(15.0)},
        /*C9 */{new Integer(3), new Integer(5), new Double(40.0)},
        /*C10*/{new Integer(4), new Integer(3), new Double(5.0)},
        /*C11*/{new Integer(4), new Integer(4), new Double(15.0)},
        /*C12*/{new Integer(4), new Integer(5), new Double(40.0)},
        /*C13*/{new Integer(5), new Integer(3), new Double(10.0)},
        /*C14*/{new Integer(5), new Integer(4), new Double(25.0)},
        /*C15*/{new Integer(5), new Integer(5), new Double(200.0)},
        /*C16*/{new Integer(6), new Integer(3), new Double(12.0)},
        /*C17*/{new Integer(6), new Integer(4), new Double(25.0)},
        /*C18*/{new Integer(6), new Integer(5), new Double(200.0)},
        /*C19*/{new Integer(7), new Integer(3), new Double(12.0)},
        /*C20*/{new Integer(7), new Integer(4), new Double(25.0)},
        /*C21*/{new Integer(7), new Integer(5), new Double(200.0)},
        /*C22*/{new Integer(8), new Integer(3), new Double(12.0)},
        /*C23*/{new Integer(8), new Integer(4), new Double(25.0)},
        /*C24*/{new Integer(8), new Integer(5), new Double(300.0)}
    };

    public NightInVeniceConfiguration() {
        super();
    }
    
    public NightInVeniceConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return line >= 0 && combo == WIN_CONSTRAINTS.length - 1;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
