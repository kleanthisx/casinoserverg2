package com.magneta.games.configuration.poker.texasholdem;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum WinType {
	BET_RATIO,
	JACKPOT_POOL_RATIO;
}