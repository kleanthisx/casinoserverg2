/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Klanthis
 *
 */
@XmlRootElement
public class BloodSuckersConfiguration extends SlotConfiguration {

	public static final int MAX_SYMBOL = 11;

	public static final int FREE_SPIN_SYMBOL = 11;
	public static final int BONUS_SYMBOL = 10; 
	public static final int WILD_SYMBOL = 9;
	
	public static final int MAXIMUM_BOXES_TO_OPEN = 9;

	/* Important the lowest payout MUST be first in the array */
	public static final int[][] BONUS_WEIGHTS = {
		{10  , 20},
		{20  , 18},
		{40  , 15},
		{80  , 13},
		{160 , 10},
		{320 ,  8},
		{640 ,  4},
		{1280,  2},
	};

	private static final int[][][] PAYLINES = {
		{ { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 } }, /* 0 */
		{ { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 } }, /* 1 */
		{ { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 } }, /* 2 */
		
		{ { 0, 0 }, { 1, 1 }, { 2, 2 }, { 1, 3 }, { 0, 4 } }, /* 3 */
		{ { 2, 0 }, { 1, 1 }, { 0, 2 }, { 1, 3 }, { 2, 4 } }, /* 4 */
		
		{ { 0, 0 }, { 0, 1 }, { 1, 2 }, { 0, 3 }, { 0, 4 } }, /* 5 */
		{ { 2, 0 }, { 2, 1 }, { 1, 2 }, { 2, 3 }, { 2, 4 } }, /* 6 */
		
		{ { 1, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 1, 4 } }, /* 7 */
		{ { 1, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 4 } }, /* 8 */
		
		{ { 1, 0 }, { 0, 1 }, { 1, 2 }, { 0, 3 }, { 1, 4 } }, /* 9 */
		{ { 1, 0 }, { 2, 1 }, { 1, 2 }, { 2, 3 }, { 1, 4 } }, /* 10*/
		
		{ { 0, 0 }, { 1, 1 }, { 0, 2 }, { 1, 3 }, { 0, 4 } }, /* 11*/
		{ { 2, 0 }, { 1, 1 }, { 2, 2 }, { 1, 3 }, { 2, 4 } }, /* 12*/
		
		{ { 1, 0 }, { 1, 1 }, { 0, 2 }, { 1, 3 }, { 1, 4 } }, /* 13*/
		{ { 1, 0 }, { 1, 1 }, { 2, 2 }, { 1, 3 }, { 1, 4 } }, /* 14*/
		
		{ { 0, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 0, 4 } }, /* 15*/
		{ { 2, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 2, 4 } }, /* 16*/
		
		{ { 0, 0 }, { 1, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 } }, /* 17*/
		{ { 2, 0 }, { 1, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 } }, /* 18*/
		
		{ { 0, 0 }, { 2, 1 }, { 0, 2 }, { 2, 3 }, { 0, 4 } }, /* 19*/
		{ { 2, 0 }, { 0, 1 }, { 2, 2 }, { 0, 3 }, { 2, 4 } }, /* 20*/
		
		{ { 0, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 0, 4 } }, /* 21*/
		{ { 2, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 2, 4 } }, /* 22*/
		
		{ { 0, 0 }, { 0, 1 }, { 2, 2 }, { 0, 3 }, { 0, 4 } }, /* 23*/
		{ { 2, 0 }, { 2, 1 }, { 0, 2 }, { 2, 3 }, { 2, 4 } }  /* 24*/

	};

	private static final int[][][] REELS = {
		{
			/*Reel 1*/{10, 2, 5, 4, 10, 2, 11, 10, 4, 8, 10, 5, 8, 2, 1, 6, 7, 9, 2, 3, 1, 2, 8, 6, 3, 7, 10, 5, 4, 11, 10, 7, 2, 6, 4, 1, 6, 4, 7, 3, 8, 1, 9, 8, 5, 2, 8},
			/*Reel 2*/{10,8,2,7,6,10,7,3,4,6,1,3,8,4,5,3,1,5,3,9,10,6,7,5,3,7,1,10,4,2,6,7,2,4,8,5,9,2,4,1,3,5,2,11,4,7,11,1,2,7},
			/*Reel 3*/{10,5,11,2,6,5,2,7,6,5,8,6,9,3,2,4,5,2,6,5,1,6,5,1,2,5,1,2,6,4,1,6,10,3,6,5,8,3,7,8,2,1,7,2,5,9,3},
			/*Reel 4*/{9,8,5,3,4,5,1,3,7,5,3,4,6,3,5,7,3,2,6,9,10,8,1,3,8,5,7,1,3,7,5,1,7,3,5,7,4,2,6,3,11,2,5,8,4,6,8,4,8,10,9,8,10,9,2,3},
			/*Reel 5*/{3,10,6,5,11,7,4,10,2,7,5,10,9,7,10,2,8,3,6,4,8,6,1,4,10,2,6,1,2,3,6,4,10,7,6,5,2,7,6,8,9,1,8,7,3,6,4,8,6,9,8,11,5,2,8}
		}
	};

	private static final Object[][] WIN_CONSTRAINTS = {
		/*Symbol        Quantity Req.   Multiplier*/

		//REGULAR PAYLINES (ordered by payout)
		/*C00*/{new Integer(1), new Integer(3), new Double(2.0)},
		/*C01*/{new Integer(1), new Integer(4), new Double(15.0)},
		/*C02*/{new Integer(1), new Integer(5), new Double(75.0)},

		/*C03*/{new Integer(2), new Integer(3), new Double(2.0)},
		/*C04*/{new Integer(2), new Integer(4), new Double(15.0)},
		/*C05*/{new Integer(2), new Integer(5), new Double(75.0)},

		/*C06*/{new Integer(3), new Integer(3), new Double(5.0)},
		/*C07*/{new Integer(3), new Integer(4), new Double(25.0)},
		/*C08*/{new Integer(3), new Integer(5), new Double(100.0)},

		/*C09*/{new Integer(4), new Integer(3), new Double(5.0)},
		/*C10*/{new Integer(4), new Integer(4), new Double(50.0)},
		/*C11*/{new Integer(4), new Integer(5), new Double(100.0)},

		/*C12*/{new Integer(5), new Integer(3), new Double(10.0)},
		/*C13*/{new Integer(5), new Integer(4), new Double(50.0)},
		/*C14*/{new Integer(5), new Integer(5), new Double(125.0)},

		/*C15*/{new Integer(6), new Integer(3), new Double(15.0)},
		/*C16*/{new Integer(6), new Integer(4), new Double(75.0)},
		/*C17*/{new Integer(6), new Integer(5), new Double(250.0)},

		/*C18*/{new Integer(7), new Integer(3), new Double(15.0)},
		/*C19*/{new Integer(7), new Integer(4), new Double(75.0)},
		/*C20*/{new Integer(7), new Integer(5), new Double(250.0)},

		/*C18*/{new Integer(8), new Integer(3), new Double(50.0)},
		/*C19*/{new Integer(8), new Integer(4), new Double(100.0)},
		/*C20*/{new Integer(8), new Integer(5), new Double(500.0)},

		/*C21*/{new Integer(WILD_SYMBOL), new Integer(2), new Double(5.0)},
		/*C21*/{new Integer(WILD_SYMBOL), new Integer(3), new Double(200.0)},
		/*C22*/{new Integer(WILD_SYMBOL), new Integer(4), new Double(2000.0)},
		/*C23*/{new Integer(WILD_SYMBOL), new Integer(5), new Double(7500.0)},
		
		/*C24*/{new Integer(-BONUS_SYMBOL), new Integer(3), new Double(9.0)},
		/*C25*/{new Integer(-BONUS_SYMBOL), new Integer(4), new Double(9.0)},
		/*C26*/{new Integer(-BONUS_SYMBOL), new Integer(5), new Double(9.0)},

		//SCATTER COMBINATIONS
		/*C32*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(2), new Double(2.0)},
		/*C33*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(3), new Double(4.0)},
		/*C34*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(4), new Double(25.0)},
		/*C35*/{new Integer(-FREE_SPIN_SYMBOL),new Integer(5), new Double(100.0)},
	};

	public BloodSuckersConfiguration() {
		super();
	}

	public BloodSuckersConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected int[][][] getDefaultReels() {
		return REELS;
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
	public int getSymbolCount() {
		return MAX_SYMBOL;
	}

	@XmlElement
	public int getBonusSymbol() {
		return BONUS_SYMBOL;
	}

	@Override
	public boolean isJackpotCombination(int line, int combo) {
		return combo == 27;
	}

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
