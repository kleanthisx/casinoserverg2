/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class Gods2Configuration extends SlotConfiguration {

    private static final int MAX_SYMBOL = 8;
    
    private static final int[][][] PAYLINES = {
    	{ {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} },
    	{ {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} },
    	{ {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} },
    	{ {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} },
    	{ {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} },

    	{ {1, 5}, {1, 4}, {1, 3}, {1, 2}, {1, 1} },
    	{ {0, 5}, {0, 4}, {0, 3}, {0, 2}, {0, 1} },
    	{ {2, 5}, {2, 4}, {2, 3}, {2, 2}, {2, 1} },
    	{ {0, 5}, {1, 4}, {2, 3}, {1, 2}, {0, 1} },
    	{ {2, 5}, {1, 4}, {0, 3}, {1, 2}, {2, 1} },
    }; 
    
    /* 90.55% */
    private static final int[][][] REELS = {
    	{
    		{1,1,2,2,3,3,4,5,6,7,1,1,1,2,3,4,4,5,6,1,1,1,2,3,4,2,1},
    		{1,1,2,2,2,3,3,4,4,5,6,7,1,1,1,2,2,3,4,5,1,1,2,2,3,4,1},
    		{1,1,2,2,2,3,3,4,4,5,5,6,7,1,1,1,2,2,2,2,3,3,4,5,1,2,1},
    		{1,1,2,2,2,3,3,4,4,5,5,6,7,1,2,2,2,3,4,5,1,1,1,2,2,2,1},
    		{1,1,2,2,2,3,3,4,4,5,5,6,7,1,1,1,2,2,2,3,3,4,4,1,1,2,1},
    		{1,1,2,2,2,3,3,4,4,4,5,6,7,1,1,1,2,3,1,1,2,3,4,5,6,1,1}
    	}
    };
    
    private static final Object[][] WIN_CONSTRAINTS = {
               /*Symbol         Quantity Req.   Multiplier*/
        /*C1 */{new Integer(1), new Integer(3), new Double(1.0)},
        /*C2 */{new Integer(1), new Integer(4), new Double(3.0)},
        /*C3 */{new Integer(1), new Integer(5), new Double(5.0)},
        
        /*C4 */{new Integer(2), new Integer(3), new Double(1.0)},
        /*C5 */{new Integer(2), new Integer(4), new Double(3.0)},
        /*C6 */{new Integer(2), new Integer(5), new Double(5.0)},
        
        /*C7 */{new Integer(3), new Integer(3), new Double(2.0)},
        /*C8 */{new Integer(3), new Integer(4), new Double(5.0)},
        /*C9 */{new Integer(3), new Integer(5), new Double(20.0)},
        
        /*C10*/{new Integer(4), new Integer(3), new Double(2.0)},
        /*C11*/{new Integer(4), new Integer(4), new Double(5.0)},
        /*C12*/{new Integer(4), new Integer(5), new Double(20.0)},
        
        /*C13*/{new Integer(5), new Integer(3), new Double(5.0)},
        /*C14*/{new Integer(5), new Integer(4), new Double(25.0)},
        /*C15*/{new Integer(5), new Integer(5), new Double(80.0)},
        
        /*C16*/{new Integer(6), new Integer(3), new Double(10.0)},
        /*C17*/{new Integer(6), new Integer(4), new Double(25.0)},
        /*C18*/{new Integer(6), new Integer(5), new Double(160.0)},
        
        /*C19*/{new Integer(7), new Integer(3), new Double(20.0)},
        /*C20*/{new Integer(7), new Integer(4), new Double(50.0)},
        /*C21*/{new Integer(7), new Integer(5), new Double(500.0)}
    };
    
    public Gods2Configuration() {
        super();
    }
    
    public Gods2Configuration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return combo == 20;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
