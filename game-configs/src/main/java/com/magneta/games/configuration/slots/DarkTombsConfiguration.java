/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class DarkTombsConfiguration extends SlotConfiguration {
   
    private static final int MAX_SYMBOL = 12;
    
    public static final int BONUS_SYMBOL = 11; 
    public static final int WILD_SYMBOL = 10;
    public static final int SCATTER_SYMBOL = 12;
    
    private static final int[][][] PAYLINES = {
        { {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} }, /*  1 */
        { {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} }, /*  2 */
        { {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} }, /*  3 */
        { {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /*  4 */
        { {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /*  5 */
        { {0, 0}, {0, 1}, {1, 2}, {0, 3}, {0, 4} }, /*  6 */
        { {2, 0}, {2, 1}, {1, 2}, {2, 3}, {2, 4} }, /*  7 */
        { {1, 0}, {0, 1}, {0, 2}, {0, 3}, {1, 4} }, /*  8 */
        { {1, 0}, {2, 1}, {2, 2}, {2, 3}, {1, 4} }, /*  9 */
        { {0, 0}, {1, 1}, {1, 2}, {1, 3}, {0, 4} }, /* 10 */
        { {2, 0}, {1, 1}, {1, 2}, {1, 3}, {2, 4} }, /* 11 */
        { {1, 0}, {1, 1}, {0, 2}, {1, 3}, {1, 4} }, /* 12 */
        { {1, 0}, {1, 1}, {2, 2}, {1, 3}, {1, 4} }, /* 13 */
        { {1, 0}, {0, 1}, {1, 2}, {0, 3}, {1, 4} }, /* 14 */
        { {1, 0}, {2, 1}, {1, 2}, {2, 3}, {1, 4} }, /* 15 */
        { {0, 0}, {1, 1}, {0, 2}, {1, 3}, {0, 4} }, /* 16 */
        { {2, 0}, {1, 1}, {2, 2}, {1, 3}, {2, 4} }, /* 17 */
        { {0, 0}, {2, 1}, {0, 2}, {2, 3}, {0, 4} }, /* 18 */
        { {2, 0}, {0, 1}, {2, 2}, {0, 3}, {2, 4} }, /* 19 */
        { {1, 0}, {0, 1}, {2, 2}, {0, 3}, {1, 4} }, /* 20 */
        { {1, 0}, {2, 1}, {0, 2}, {2, 3}, {1, 4} }, /* 21 */
        { {0, 0}, {2, 1}, {1, 2}, {0, 3}, {2, 4} }, /* 22 */
        { {2, 0}, {2, 1}, {0, 2}, {2, 3}, {2, 4} }, /* 23 */
        { {0, 0}, {0, 1}, {2, 2}, {0, 3}, {0, 4} }, /* 24 */
        { {2, 0}, {0, 1}, {1, 2}, {2, 3}, {0, 4} }, /* 25 */
    };
    
    private static final int[][][] REELS = {
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,11,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,11,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,11,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,11,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,11,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	},
    	{
    		/*Reel 1*/{1,3,4,1,3,4,1,3,4,1,9,3,1,2,3,4,1,3,5,1,3,4,1,3,10,1,3,9,1,3,4,1,3,4,1,11,3,4,1,3,9,1,3,4,1,3,5,1,3,6,1,10,3,1,2,5,7,1,8,3,2,5},
    		/*Reel 2*/{1,3,4,1,3,4,1,3,4,2,9,3,1,2,3,4,1,3,2,1,3,4,1,3,10,2,3,9,1,3,4,1,3,6,1,11,3,1,5,3,1,6,3,1,7,3,1,10,3,1,4,3,1,5,3,1,5,8,1,3,7,8},
    		/*Reel 3*/{1,3,4,1,3,4,1,3,4,1,9,3,1,5,3,4,1,3,5,1,3,4,1,3,10,1,3,6,1,3,4,2,3,4,1,11,3,4,1,3,7,1,3,8,1,3,5,2,3,10,1,4,6,1,2,5,1,7,3,1,2,8},
    		/*Reel 4*/{1,2,3,1,2,4,1,2,3,2,4,3,1,4,3,1,2,6,1,3,5,1,2,9,1,5,10,2,3,4,3,7,4,2,3,4,2,8,11,2,3,5,2,3,4,2,1,4,2,10,8,2,1,3,4,1,2,6,1,4,1,7},
    		/*Reel 5*/{1,2,3,1,2,3,1,2,4,1,2,4,1,2,8,1,11,10,1,2,3,2,3,2,5,6,2,3,5,4,2,10,8,2,3,1,2,4,1,2,3,4,2,4,7,2,9,7,2,1,3,4,2,3,5,2,4,1,3,2,6,3}
    	}
    };

    public static final Object[][] WIN_CONSTRAINTS = {
                /*Symbol         Quantity Req.   Multiplier*/
       
        /*C00*/{new Integer(1), new Integer(3), new Double(5.0)},
        /*C01*/{new Integer(1), new Integer(4), new Double(25.0)},
        /*C02*/{new Integer(1), new Integer(5), new Double(250.0)},
        /*C03*/{new Integer(2), new Integer(3), new Double(5.0)},
        /*C04*/{new Integer(2), new Integer(4), new Double(25.0)},
        /*C05*/{new Integer(2), new Integer(5), new Double(250.0)},
        /*C06*/{new Integer(3), new Integer(3), new Double(10.0)},
        /*C07*/{new Integer(3), new Integer(4), new Double(50.0)},
        /*C08*/{new Integer(3), new Integer(5), new Double(500.0)},
        /*C09*/{new Integer(4), new Integer(3), new Double(10.0)},
        /*C10*/{new Integer(4), new Integer(4), new Double(50.0)},
        /*C11*/{new Integer(4), new Integer(5), new Double(250.0)},
        /*C12*/{new Integer(5), new Integer(3), new Double(15.0)},
        /*C13*/{new Integer(5), new Integer(4), new Double(100.0)},
        /*C14*/{new Integer(5), new Integer(5), new Double(1000.0)},
        /*C15*/{new Integer(6), new Integer(3), new Double(15.0)},
        /*C16*/{new Integer(6), new Integer(4), new Double(100.0)},
        /*C17*/{new Integer(6), new Integer(5), new Double(1000.0)},
        /*C18*/{new Integer(7), new Integer(3), new Double(25.0)},
        /*C19*/{new Integer(7), new Integer(4), new Double(150.0)},
        /*C20*/{new Integer(7), new Integer(5), new Double(2000.0)},
        /*C21*/{new Integer(8), new Integer(3), new Double(25.0)},
        /*C22*/{new Integer(8), new Integer(4), new Double(150.0)},
        /*C23*/{new Integer(8), new Integer(5), new Double(2000.0)},
        /*C24*/{new Integer(9), new Integer(3), new Double(150.0)},
        /*C25*/{new Integer(9), new Integer(4), new Double(1500.0)},
        /*C26*/{new Integer(9), new Integer(5), new Double(15000.0)},
       
        /*Bonus Combination*/
        /*C27*/{new Integer(-BONUS_SYMBOL), new Integer(3), new Double(50.0)},
        /*C28*/{new Integer(-BONUS_SYMBOL), new Integer(4), new Double(150.0)},
        /*C29*/{new Integer(-BONUS_SYMBOL), new Integer(5), new Double(250.0)},
        /* Scatter Combination */
        /*C30*/{new Integer(-SCATTER_SYMBOL), new Integer(3), new Double(0.0)},
        /*C31*/{new Integer(-SCATTER_SYMBOL), new Integer(4), new Double(0.0)},
        /*C32*/{new Integer(-SCATTER_SYMBOL), new Integer(5), new Double(0.0)},
    };
    
    public DarkTombsConfiguration() {
        super();
    }
    
    public DarkTombsConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @XmlElement
    public int getBonusSymbol() {
        return BONUS_SYMBOL;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return combo == 26;
    }
    
    @Override
	public int getNumberOfReelSets() {
    	return REELS.length;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
