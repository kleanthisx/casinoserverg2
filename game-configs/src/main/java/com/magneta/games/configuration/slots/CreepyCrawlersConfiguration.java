package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;

/**
 * @author User
 *
 */
@XmlRootElement 
public class CreepyCrawlersConfiguration extends SlotConfiguration {

	public static final String REEL_WEIGHT_0 = "reel_weight_0";
	public static final String REEL_WEIGHT_1 = "reel_weight_1";
	public static final String REEL_WEIGHT_2 = "reel_weight_2";
	public static final String REEL_WEIGHT_3 = "reel_weight_3";

	private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
		new GameTemplateSetting(REEL_WEIGHT_0, GameTemplateSetting.INTEGER, "Reel Set 0 Weight", 7300),
		new GameTemplateSetting(REEL_WEIGHT_1, GameTemplateSetting.INTEGER, "Reel Set 1 Weight", 1525),
		new GameTemplateSetting(REEL_WEIGHT_2, GameTemplateSetting.INTEGER, "Reel Set 2 Weight", 775),
		new GameTemplateSetting(REEL_WEIGHT_3, GameTemplateSetting.INTEGER, "Reel Set 3 Weight", 400),
	};

	private static final int MAX_SYMBOL = 12;

	public static final int FREE_SPIN_SCATTER_1 = 10;
	public static final int FREE_SPIN_SCATTER_2 = 11;
	public static final int FREE_SPIN_SCATTER_3 = 12;
	public static final int FREE_SPIN_REEL_SET = 4;

	private static final int[][][] PAYLINES = {
		{ {1, 0}, {1, 1}, {1, 2} }, /* 1  */
		{ {0, 0}, {0, 1}, {0, 2} }, /* 2  */
		{ {2, 0}, {2, 1}, {2, 2} }, /* 3  */
		{ {0, 0}, {1, 1}, {2, 2} }, /* 4  */
		{ {2, 0}, {1, 1}, {0, 2} }, /* 5  */
		{ {0, 0}, {1, 0}, {2, 0} }, /* 6  */ 
		{ {0, 1}, {1, 1}, {2, 1} }, /* 7  */ 
		{ {0, 2}, {1, 2}, {2, 2} }, /* 8  */
	};

	private static final int[][][] REELS = {
		{
			/*Reel 1*/{4,4,9,8,9,8,2,6,6,6,7,7,7,9,9,9,5,5,5,3,8,8,8,1},
			/*Reel 2*/{5,5,9,9,9,9,8,3,3,8,8,8,2,8,8,7,7,7,6,4,6,6,6,1},
			/*Reel 3*/{7,7,7,5,5,9,9,8,9,8,8,8,3,2,6,6,6,4,4,9,9,9,7,1}
		},
		{
			/*Reel 1*/{4,4,9,8,10,8,2,6,6,6,10,7,7,9,9,9,5,5,10,3,8,8,8,1},
			/*Reel 2*/{5,10,9,9,9,9,8,3,3,8,8,10,2,8,8,7,7,7,10,4,6,6,6,1},
			/*Reel 3*/{7,7,7,5,5,9,9,10,9,8,8,8,3,2,6,6,6,10,4,9,9,9,10,1}
		},
		{
			/*Reel 1*/{4,4,9,8,11,8,2,6,6,6,7,7,11,9,9,9,5,5,5,3,8,8,11,1},
			/*Reel 2*/{5,5,9,9,9,11,8,3,3,8,8,8,2,8,11,7,7,7,11,4,6,6,6,1},
			/*Reel 3*/{7,7,7,5,5,9,9,11,9,8,8,8,11,2,6,6,6,11,4,9,9,9,7,1}
		},
		{
			/*Reel 1*/{4,4,9,8,12,8,2,6,6,6,7,7,7,9,9,12,5,5,5,12,8,8,8,1},
			/*Reel 2*/{5,5,12,9,9,9,8,3,3,8,8,12,2,8,8,7,7,7,12,4,6,6,6,1},
			/*Reel 3*/{7,7,7,5,5,9,9,12,9,8,8,8,3,2,6,6,6,12,4,9,9,12,7,1}
		},
		{
			/*Reel 1*/{4,4,9,8,9,8,2,6,6,6,7,7,7,9,9,9,5,5,5,3,8,8,8,1},
			/*Reel 2*/{5,5,9,9,9,9,8,3,3,8,8,8,2,8,8,7,7,7,6,4,6,6,6,1},
			/*Reel 3*/{7,7,7,5,5,9,9,8,9,8,8,8,3,2,6,6,6,4,4,9,9,9,7,1}
		}
	};

	private static final Object[][] WIN_CONSTRAINTS = {
				/*Symbol          Quantity Req.   Multiplier*/
		/*C00*/{new Integer(1)  , new Integer(3), new Double(100.0)},
		/*C01*/{new Integer(2)  , new Integer(3), new Double(75.0)},
		/*C02*/{new Integer(3)  , new Integer(3), new Double(50.0)},
		/*C03*/{new Integer(4)  , new Integer(3), new Double(40.0)},
		/*C04*/{new Integer(5)  , new Integer(3), new Double(20.0)},
		/*C05*/{new Integer(6)  , new Integer(3), new Double(10.0)},
		/*C06*/{new Integer(7)  , new Integer(3), new Double(10.0)},
		/*C07*/{new Integer(8)  , new Integer(3), new Double(5.0)},
		/*C08*/{new Integer(9)  , new Integer(3), new Double(5.0)},
		/*C09*/{new Integer(-10), new Integer(3), new Double(0.0)},
		/*C10*/{new Integer(-11), new Integer(3), new Double(0.0)},
		/*C11*/{new Integer(-12), new Integer(3), new Double(0.0)},
		
	};

	public CreepyCrawlersConfiguration() {
		super();
	}

	public CreepyCrawlersConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected int[][][] getDefaultReels() {
		return REELS;
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
	public int getSymbolCount() {
		return MAX_SYMBOL;
	}

	@Override
	public boolean isJackpotCombination(int line, int combo) {
		return combo == 0;
	}

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}

	@Override
	public int getNumberOfReelSets() {
		return REELS.length;
	}

	@Override
	public GameTemplateSetting[] getDefaultSettings() {
		return DEFAULT_SETTINGS;
	}

}
