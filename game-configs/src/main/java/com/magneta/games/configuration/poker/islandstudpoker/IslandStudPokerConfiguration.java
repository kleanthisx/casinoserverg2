package com.magneta.games.configuration.poker.islandstudpoker;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.poker.PokerConstants;

/**
 * @author User
 *
 */
@XmlRootElement 
public class IslandStudPokerConfiguration implements GameConfiguration {
    
	
	private Long configId;
	private List<WinCombination> winCombinations;
	private List<WinCombination> jackpotCombinations;

	private static final WinCombination[] defaultWinCombinations = {
		new WinCombination(PokerConstants.NONE, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.JACKS_OR_BETTER, WinType.BET_RATIO, 1.0),
		new WinCombination(PokerConstants.TWO_PAIRS, WinType.BET_RATIO, 2.0),
		new WinCombination(PokerConstants.THREE_OF_A_KIND, WinType.BET_RATIO, 3.0),
		new WinCombination(PokerConstants.STRAIGHT, WinType.BET_RATIO, 4.0),
		new WinCombination(PokerConstants.FLUSH, WinType.BET_RATIO, 5.0),
		new WinCombination(PokerConstants.FULL_HOUSE, WinType.BET_RATIO, 7.0),
		new WinCombination(PokerConstants.FOUR_OF_A_KIND, WinType.BET_RATIO, 20.0),
		new WinCombination(PokerConstants.STRAIGHT_FLUSH, WinType.BET_RATIO, 50.0),
		new WinCombination(PokerConstants.ROYAL_FLUSH, WinType.BET_RATIO, 100.0),
	};
	
	private static final WinCombination[] defaultJackpotWinCombinations = {
		new WinCombination(PokerConstants.FLUSH, WinType.BET_RATIO, 50.0),
		new WinCombination(PokerConstants.FULL_HOUSE, WinType.BET_RATIO, 100.0),
		new WinCombination(PokerConstants.FOUR_OF_A_KIND, WinType.BET_RATIO, 500.0),
		new WinCombination(PokerConstants.STRAIGHT_FLUSH, WinType.JACKPOT_POOL_RATIO, 0.10),
		new WinCombination(PokerConstants.ROYAL_FLUSH, WinType.JACKPOT_POOL_RATIO, 1.00),
	};
    
    public IslandStudPokerConfiguration() {
        super();
        this.winCombinations = new ArrayList<WinCombination>();
        this.jackpotCombinations = new ArrayList<WinCombination>();
        
        for (WinCombination c: defaultWinCombinations) {
        	this.winCombinations.add(c);
        }
        
        for (WinCombination c: defaultJackpotWinCombinations) {
        	this.jackpotCombinations.add(c);
        }
    }
    
    public IslandStudPokerConfiguration(long configId, String configStr) throws JAXBException {
    	JAXBContext context = getJAXBContext();
    	Unmarshaller unmarshaller = context.createUnmarshaller();
    	
    	StringReader sr = new StringReader(configStr);
    	
    	IslandStudPokerConfiguration config = (IslandStudPokerConfiguration)unmarshaller.unmarshal(sr);
    	
    	this.jackpotCombinations = config.jackpotCombinations;
    	this.winCombinations = config.winCombinations;
    	this.configId = configId;
    }
    
    @Override
	public String serialize() throws JAXBException {
    	JAXBContext ctx = this.getJAXBContext();
    	
    	StringWriter sw = new StringWriter();
    	
    	Marshaller marshaller = ctx.createMarshaller();
    	marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    	
    	marshaller.marshal(this, sw);
    	
    	return sw.toString();
    }
	
	@XmlElement
	public List<WinCombination> getWinCombinations() {
		return winCombinations;
	}
	
	public void setWinCombinations(List<WinCombination> winCombinations) {
		this.winCombinations = winCombinations;
	}
	
	@XmlElement
	public List<WinCombination> getJackpotWinCombinations() {
		return jackpotCombinations;
	}
	
	public void setJackpotWinCombinations(List<WinCombination> jackpotCombinations) {
		this.jackpotCombinations = jackpotCombinations;
	}

    private JAXBContext getJAXBContext() throws JAXBException {
    	return JAXBContext.newInstance(this.getClass(), WinCombination.class, WinType.class);
    }
    
    public WinCombination getJackpotWinCombination(int combo) {
    	for (WinCombination c: jackpotCombinations) {
    		if (c.getTriggerCombo() == combo) {
    			return c;
    		}
    	}
    	
    	return null;
    }
    
    public WinCombination getWinCombination(int combo) {
    	for (WinCombination c: winCombinations) {
    		if (c.getTriggerCombo() == combo) {
    			return c;
    		}
    	}
    	
    	return null;
    }
    
    public void setConfigId(Long configId) {
    	this.configId = configId;
    }

	@Override
	@XmlElement
	public Long getConfigId() {
		return configId;
	}
}
