/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class PlayTimeConfiguration extends SlotConfiguration {

    private static final int MAX_SYMBOL = 8;
    
    private static final int [][][] PAYLINES = {
    	{ {1,0}, {1,1}, {1,2}, {1,3}, {1,4} }, /* 0 */
    	{ {0,0}, {0,1}, {0,2}, {0,3}, {0,4}	}, /* 1 */
    	{ {2,0}, {2,1}, {2,2}, {2,3}, {2,4}	}, /* 2 */
    	{ {0,0}, {1,1}, {2,2}, {1,3}, {0,4}	}, /* 3 */
    	{ {2,0}, {1,1}, {0,2}, {1,3}, {2,4}	}  /* 4 */
    };
    
    /*89.26% - 25/07/2007*/
    private static final int[][][] REELS = {
    	{
    		{1,2,3,4,4,4,5,5,5,6,7,8,1,1,1,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,1,2,3,3,4,5,5,5,5},
    		{1,2,3,4,4,4,4,5,5,5,6,6,6,7,8,1,1,1,1,2,2,2,3,3,3,4,4,5,6,7,7,8,8,1,1,1,1,2,2,2,2,3,3,7,8,1,1,1,1,1,2,2,2,2,2},
    		{1,2,3,4,5,5,6,7,8,1,1,2,2,3,3,4,4,5,6,6,6,7,7,7,7,8,8,8,8,1,3,5,6,6,6,6,7,7,7,7,8,8,8,8,2,4,6,6,6,7,7,7,8,8,8},
    		{1,2,3,4,5,5,6,7,8,1,1,1,1,1,2,2,2,2,3,3,3,4,4,4,5,5,6,6,6,7,7,8,1,1,1,2,2,2,2,2,3,4,1,1,1,2,2,2,3,4,7,8,2,2,2},
    		{1,2,3,4,4,4,5,5,5,6,7,8,1,1,1,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,1,2,3,3,4,5,5,5,5}
    	}
    };
    
    private static final Object[][] WIN_CONSTRAINTS = {
                /*Symbol         Quantity Req.   Multiplier*/
         /*C1 */{new Integer(1), new Integer(3), new Double(12.0)},
         /*C2 */{new Integer(1), new Integer(4), new Double(25.0)},
         /*C3 */{new Integer(1), new Integer(5), new Double(200.0)},
         /*C4 */{new Integer(2), new Integer(3), new Double(12.0)},
         /*C5 */{new Integer(2), new Integer(4), new Double(25.0)},
         /*C6 */{new Integer(2), new Integer(5), new Double(200.0)},
         /*C7 */{new Integer(3), new Integer(3), new Double(12.0)},
         /*C8 */{new Integer(3), new Integer(4), new Double(25.0)},
         /*C9 */{new Integer(3), new Integer(5), new Double(200.0)},
         /*C10*/{new Integer(4), new Integer(3), new Double(10.0)},
         /*C11*/{new Integer(4), new Integer(4), new Double(25.0)},
         /*C12*/{new Integer(4), new Integer(5), new Double(200.0)},
         /*C13*/{new Integer(5), new Integer(3), new Double(5.0)},
         /*C14*/{new Integer(5), new Integer(4), new Double(15.0)},
         /*C15*/{new Integer(5), new Integer(5), new Double(40.0)},
         /*C16*/{new Integer(6), new Integer(3), new Double(5.0)},
         /*C17*/{new Integer(6), new Integer(4), new Double(15.0)},
         /*C18*/{new Integer(6), new Integer(5), new Double(40.0)},
         /*C19*/{new Integer(7), new Integer(3), new Double(2.0)},
         /*C20*/{new Integer(7), new Integer(4), new Double(5.0)},
         /*C21*/{new Integer(7), new Integer(5), new Double(10.0)},
         /*C22*/{new Integer(8), new Integer(3), new Double(2.0)},
         /*C23*/{new Integer(8), new Integer(4), new Double(5.0)},
         /*C24*/{new Integer(8), new Integer(5), new Double(10.0)},
         /*C25*/{new Integer(-1),new Integer(15),new Double(900.0)}
    };
    
    public PlayTimeConfiguration() {
        super();
    }
    
    public PlayTimeConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
