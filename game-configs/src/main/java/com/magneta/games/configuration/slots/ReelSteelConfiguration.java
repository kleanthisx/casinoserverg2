/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;

/**
 * @author User
 *
 */
@XmlRootElement 
public class ReelSteelConfiguration extends SlotConfiguration {
   
	public static final String REEL_WEIGHT_0 = "reel_weight_0";
	public static final String REEL_WEIGHT_1 = "reel_weight_1";
	
    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting(REEL_WEIGHT_0, GameTemplateSetting.INTEGER, "Reel Set 0 Weight", 100),
        new GameTemplateSetting(REEL_WEIGHT_1, GameTemplateSetting.INTEGER, "Reel Set 1 Weight", 100),
    };
	
    public static final int FREE_SPIN_REEL_SET = 2;
	
    
    private static final int MAX_SYMBOL = 12;
    
    public static final int WILD_SYMBOL = 1;
    public static final double WILD_SYMBOL_MULTIPLIER = 5;
    public static final double FREE_SPIN_MULTIPLIER = 5;

    public static final int FREE_SPIN_SCATTER = 12;
    public static final int FREE_SPIN_NUMBER = 15;
	
    private static final int[][][] PAYLINES = {

		{ {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} }, /* 1  */ // M/M/M/M/M
		{ {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} }, /* 2  */ // T/T/T/T/T
		{ {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} }, /* 3  */ // B/B/B/B/B
		
		{ {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 4  */ // T/M/B/M/T
		{ {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 5  */ // B/M/T/M/B
		
		{ {0, 0}, {0, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 7  */ // T/T/M/T/T
		{ {2, 0}, {2, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 6  */ // B/B/M/B/B
				
		{ {1, 0}, {2, 1}, {2, 2}, {2, 3}, {1, 4} }, /* 8  */ // M/B/B/B/M
		{ {1, 0}, {0, 1}, {0, 2}, {0, 3}, {1, 4} }, /* 9  */ // N/T/T/T/M
		
	};
    
    private static final int[][][] REELS = {
    	{
    		{5, 11, 6, 10, 11, 9, 10, 4, 9, 2, 11, 7, 2, 4, 8, 3, 11, 10, 8, 11, 10, 4, 7, 11, 3, 7, 8, 2, 9, 8, 4, 5, 11, 12, 8, 3, 10, 12, 7, 10, 5, 9, 7, 5, 11, 9, 2, 7, 6, 10, 11, 6, 3, 5, 11, 10},
    		{10, 2, 11, 10, 9, 11, 7, 9, 11, 3, 2, 12, 8, 2, 6, 8, 7, 10, 8, 9, 4, 2, 9, 6, 2, 4, 11, 2, 10, 9, 5, 11, 7, 3, 11, 10, 7, 6, 4, 7, 10, 11, 3, 8, 9, 11, 10, 4, 5, 10, 9, 11, 5, 2, 1, 11, 9},
    		{11, 10, 4, 11, 12, 10, 3, 9, 6, 8, 11, 3, 10, 11, 7, 6, 8, 11, 7, 2, 10, 4, 11, 5, 3, 11, 7, 10, 2, 4, 10, 11, 6, 1, 9, 3, 7, 12, 3, 7, 11, 5, 10, 2, 8, 10, 5, 4, 10, 5, 6, 8, 11, 2, 6, 9},
    		{4, 7, 11, 1, 7, 11, 3, 10, 11, 7, 2, 11, 9, 12, 11, 2, 9, 8, 10, 11, 7, 10, 8, 6, 7, 8, 9, 7, 10, 9, 5, 11, 6, 8, 11, 10, 8, 3, 9, 8, 3, 11, 8, 6, 4, 5, 10, 2, 5, 10, 2, 9, 7, 5, 10, 4, 1, 8, 7, 3},
    		{11, 2, 4, 8, 11, 10, 7, 5, 10, 6, 11, 2, 5, 6, 11, 10, 6, 7, 11, 9, 7, 11, 1, 9, 7, 8, 12, 7, 8, 5, 7, 10, 5, 11, 4, 3, 2, 4, 10, 8, 9, 10, 8, 9, 2, 4, 3, 9, 10, 3, 2, 7, 6, 8, 9, 11, 3, 8},
    	},
    	{
    		{9, 6, 11, 10, 9, 5, 10, 4, 6, 3, 11, 7, 5, 6, 10, 3, 7, 8, 11, 12, 9, 11, 4, 2, 8, 10, 2, 5, 11, 10, 7, 3, 1, 10, 11, 7, 3, 11, 10, 7, 4, 10, 2, 9, 8, 11, 4, 5, 11, 7, 8, 9, 5, 8, 11, 2},
    		{11, 7, 9, 10, 11, 8, 9, 2, 11, 5, 7, 3, 1, 8, 4, 2, 9, 11, 2, 9, 7, 2, 6, 10, 11, 4, 6, 11, 9, 3, 11, 7, 10, 4, 9, 11, 12, 9, 8, 10, 3, 11, 6, 10, 2, 8, 10, 2, 7, 5, 2, 4, 11, 10, 5, 9, 10},
    		{5, 7, 6, 10, 11, 6, 1, 4, 11, 10, 5, 11, 7, 8, 11, 3, 2, 11, 3, 6, 11, 7, 8, 2, 3, 10, 11, 12, 10, 11, 2, 10, 9, 6, 10, 9, 3, 10, 4, 12, 7, 8, 10, 5, 3, 6, 4, 7, 5, 11, 10, 9, 8, 4, 11, 2},
    		{9, 11, 8, 10, 11, 4, 2, 8, 10, 11, 8, 6, 7, 11, 8, 3, 11, 9, 3, 8, 7, 10, 9, 11, 7, 4, 2, 6, 7, 1, 8, 10, 7, 9, 10, 7, 2, 5, 3, 6, 8, 11, 7, 8, 5, 2, 4, 5, 10, 11, 5, 7, 9, 10, 11, 12, 9, 10, 3},
    		{4, 11, 3, 7, 2, 11, 5, 11, 10, 2, 11, 6, 2, 9, 8, 7, 5, 10, 9, 5, 2, 3, 10, 8, 5, 11, 8, 7, 12, 9, 1, 10, 9, 8, 4, 9, 11, 3, 4, 7, 8, 10, 7, 4, 8, 10, 11, 7, 10, 6, 7, 8, 6, 3, 2, 6, 11, 9},
    	},
    	{
    		{10, 11, 7, 8, 10, 11, 9, 5, 11, 4, 6, 3, 7, 5, 6, 11, 5, 4, 10, 1, 9, 10, 3, 11, 10, 2, 9, 10, 2, 8, 5, 7, 8, 10, 7, 5, 11, 4, 2, 11, 3, 10, 11, 3, 9, 7, 11, 12, 8, 4, 11, 8, 9, 2, 6, 7},
    		{11, 3, 8, 10, 2, 11, 9, 10, 11, 2, 4, 11, 5, 7, 11, 4, 10, 9, 11, 10, 7, 4, 9, 10, 11, 6, 2, 9, 5, 7, 9, 11, 12, 7, 2, 1, 9, 6, 8, 9, 2, 10, 1, 7, 4, 11, 8, 3, 11, 10, 2, 3, 6, 10, 8, 9, 2, 12, 5},
    		{8, 5, 10, 11, 6, 2, 11, 10, 6, 11, 2, 5, 1, 11, 9, 10, 11, 7, 9, 3, 7, 11, 1, 10, 7, 12, 8, 9, 5, 11, 10, 5, 6, 2, 8, 4, 3, 2, 4, 3, 10, 4, 3, 6, 11, 10, 6, 7, 10, 11, 4, 12, 8, 3, 10, 7, 11},
    		{8, 3, 10, 11, 7, 9, 11, 7, 4, 6, 7, 10, 3, 7, 6, 10, 7, 11, 8, 10, 9, 11, 1, 8, 12, 7, 8, 5, 3, 11, 10, 8, 9, 11, 5, 8, 10, 7, 11, 8, 10, 4, 6, 10, 2, 3, 9, 7, 4, 11, 8, 12, 2, 9, 1, 2, 5, 11, 9, 5, 2},
    		{11, 3, 10, 11, 5, 7, 11, 12, 2, 8, 9, 10, 6, 2, 11, 6, 5, 2, 8, 3, 2, 7, 1, 10, 7, 5, 4, 9, 7, 8, 9, 11, 7, 9, 11, 8, 4, 11, 8, 6, 7, 11, 2, 4, 3, 8, 10, 7, 9, 10, 8, 5, 10, 3, 6, 4, 10, 9},
    	}
    	
    };

    private static final Object[][] WIN_CONSTRAINTS = {
                /*Symbol         Quantity Req.   Multiplier*/
    	 /*C05*/{new Integer(1), new Integer(3), new Double(0.0)},
         /*C06*/{new Integer(1), new Integer(4), new Double(0.0)},
         /*C07*/{new Integer(1), new Integer(5), new Double(0.0)},
    	// GUNNEER
        /*C05*/{new Integer(2), new Integer(3), new Double(25.0)},
        /*C06*/{new Integer(2), new Integer(4), new Double(150.0)},
        /*C07*/{new Integer(2), new Integer(5), new Double(1500.0)},
        // WOMAN
        /*C05*/{new Integer(3), new Integer(3), new Double(20.0)},
        /*C06*/{new Integer(3), new Integer(4), new Double(100.0)},
        /*C07*/{new Integer(3), new Integer(5), new Double(1000.0)},
        // CIGAR
        /*C11*/{new Integer(4), new Integer(3), new Double(15.0)},
        /*C12*/{new Integer(4), new Integer(4), new Double(75.0)},
        /*C12*/{new Integer(4), new Integer(5), new Double(750.0)},
        // CAR MAN
        /*C14*/{new Integer(5), new Integer(3), new Double(10.0)},
        /*C15*/{new Integer(5), new Integer(4), new Double(50.0)},
        /*C16*/{new Integer(5), new Integer(5), new Double(400.0)},
        // THIEF
        /*C14*/{new Integer(6), new Integer(3), new Double(10.0)},
        /*C15*/{new Integer(6), new Integer(4), new Double(50.0)},
        /*C16*/{new Integer(6), new Integer(5), new Double(200.0)},
        // SAFE
        /*C20*/{new Integer(7), new Integer(3), new Double(10.0)},
        /*C21*/{new Integer(7), new Integer(4), new Double(20.0)},
        /*C22*/{new Integer(7), new Integer(5), new Double(100.0)},
        // SUITCASE
        /*C20*/{new Integer(8), new Integer(3), new Double(5.0)},
        /*C21*/{new Integer(8), new Integer(4), new Double(15.0)},
        /*C22*/{new Integer(8), new Integer(5), new Double(75.0)},
        // BAG
        /*C20*/{new Integer(9), new Integer(3), new Double(4.0)},
        /*C21*/{new Integer(9), new Integer(4), new Double(12.0)},
        /*C22*/{new Integer(9), new Integer(5), new Double(60.0)},
        // CAMERA
        /*C20*/{new Integer(10), new Integer(3), new Double(2.0)},
        /*C21*/{new Integer(10), new Integer(4), new Double(10.0)},
        /*C22*/{new Integer(10), new Integer(5), new Double(50.0)},
        // GUN
        /*C20*/{new Integer(11), new Integer(3), new Double(2.0)},
        /*C21*/{new Integer(11), new Integer(4), new Double(10.0)},
        /*C22*/{new Integer(11), new Integer(5), new Double(40.0)},
        // 9
        /*C29*/{new Integer(-12), new Integer(1), new Double(0.0),new Integer(0),new Integer(1)},
        /*C29*/{new Integer(-12), new Integer(2), new Double(2.0),new Integer(0),new Integer(2)},
        /*C30*/{new Integer(-12), new Integer(3), new Double(4.0),new Integer(15),new Integer(3)},
        /*C31*/{new Integer(-12), new Integer(4), new Double(15.0),new Integer(20),new Integer(4)},
        /*C31*/{new Integer(-12), new Integer(5), new Double(100.0),new Integer(25),new Integer(5)},
        
    };

    
    public ReelSteelConfiguration() {
        super();
    }
    
    public ReelSteelConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_CONSTRAINTS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return combo == 5;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
	
    @Override
	public int getNumberOfReelSets() {
    	return REELS.length;
    }
    
    @Override
	public GameTemplateSetting[] getDefaultSettings() {
    	return DEFAULT_SETTINGS;
    }
    
}
