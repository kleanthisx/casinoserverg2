/**
 * 
 */
package com.magneta.games.configuration.poker;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.slots.ConfigurationLoadingUtils;


/**
 * @author Zenios
 *
 */
@XmlRootElement
public abstract class PokerConfiguration implements GameConfiguration {

    protected Long configId;
    protected double[] paytable;
    
    
    protected abstract double[] getMultipliers();
    
    public PokerConfiguration() {
        this.paytable = getMultipliers();
        this.configId = null;
    }
    
    @Override
    public Long getConfigId() {
        return this.configId;
    }
    
    
    public void setConfigId(Long configId) {
        this.configId = configId;
    }
    
    public PokerConfiguration(long configId, String config) {
        this.configId = configId;
        double[] latestPayouts = null;
        if (config != null){
            String[] strs = config.split(";");
            if (strs.length >= 1) {
                latestPayouts = ConfigurationLoadingUtils.parsePayouts(strs[0], getMultipliers().length);
            }
        }
        this.paytable = latestPayouts;
    }
    
    @Override
	public String serialize() {
    	
		double[] paytable = getPaytable();
		StringBuilder sb = new StringBuilder();
		
		for(int i=0;i<paytable.length;i++) {
			Double value = paytable[i];
			sb.append(Integer.toString(i)).append("=").append(value).append(":");
		}
		sb.append(";");
		
		return sb.toString();
    }
    
    public double[] getPaytable() {
        return this.paytable;
    }
    
    public void setPaytable(double[] paytable) {
        this.paytable = paytable;
    }
    
    public double getMultiplier(int index) {
        if (index < 0 || index >= this.paytable.length)
            return 0.0;

        return this.paytable[index];
    }
}
