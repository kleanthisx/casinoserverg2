/**
 * 
 */
package com.magneta.games.configuration;

import java.lang.reflect.Constructor;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;

/**
 * @author Zenios
 *
 */
public class GameConfigurationFactory {
    
    public static final Class<? extends GameConfiguration> getConfigurationClass(String gameTemplate) {
        
        GameTemplateInfo info = GameTemplateInfoFactory.getGameTemplateInfo(gameTemplate);
        if (info == null)
            return null;
        
        return info.getConfigurationClass();
    }
    
    public static final GameConfiguration loadConfiguration(String gameTemplate) throws GameConfigLoadingException {   
        Class<? extends GameConfiguration> klass = GameConfigurationFactory.getConfigurationClass(gameTemplate);
        if (klass == null) {
            throw new GameConfigLoadingException("Could not find game config class for template " + gameTemplate);
        }

        try {
            Constructor<? extends GameConfiguration> c = klass.getConstructor();
            return c.newInstance();
        } catch (Throwable e) {
            throw new GameConfigLoadingException("Unable to load default config for game template " + gameTemplate, e);
        }
    }
    
    public static final GameConfiguration loadConfiguration(String gameTemplate, long configId, String config) throws GameConfigLoadingException {
        
        if (configId == 0) {
            return loadConfiguration(gameTemplate);
        }
        
        Class<? extends GameConfiguration> klass = GameConfigurationFactory.getConfigurationClass(gameTemplate);
        if (klass == null) {
            throw new GameConfigLoadingException("Could not find game config class for template " + gameTemplate);
        }

        try {
            Constructor<? extends GameConfiguration> c = klass.getConstructor(Long.TYPE, String.class);
            return c.newInstance(configId, config);
        } catch (Throwable e) {
            throw new GameConfigLoadingException("Unable to load game config " + configId, e);
        }
    }
}
