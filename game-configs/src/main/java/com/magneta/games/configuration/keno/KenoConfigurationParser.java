package com.magneta.games.configuration.keno;

import java.util.HashMap;
import java.util.Map;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.slots.ConfigurationLoadingUtils;

/**
 * Parses configuration for slot games from a string.
 * @author anarxia
 *
 */
public class KenoConfigurationParser {

    private double[] payouts;
    private Map<String,String> settings;

    public KenoConfigurationParser(String configString, int combosLength, GameTemplateSetting[] defaultSettings) {
    	if (configString == null) {
    		return;
    	}
    	
    	configString = configString.replaceAll("\\n", "").replaceAll("\\t","").trim();
    	String[] strs = new String[]{configString};

    	if (strs.length <1) {
    		return;
    	}
    	   	
    	
    		payouts = ConfigurationLoadingUtils.parsePayouts(strs[0], combosLength);

    	
    	if (strs.length > 1) {
    		if (defaultSettings == null) {
    			throw new RuntimeException("Game configuration does not support settings but settings found");
    		}
    		
    		settings = new HashMap<String,String>();
//    		for (int i=1; i < strs.length; i++) {
//    			int delimIndex = strs[i].indexOf('=');
//    			
//    			if (delimIndex < 1) {
//    				throw new RuntimeException("Setting does not have a value");
//    			}
//    			
//    			String key = strs[i].substring(0, delimIndex);
//    			String val = strs[i].substring(delimIndex+1);
//    			
//    			boolean foundSetting = false;
//    			
//    			for (GameTemplateSetting s: defaultSettings) {
//    				if (s.getKey().equals(key)) {
//    					foundSetting = true;
//    					GameTemplateSetting.parseValue(val, s.getType());
//    					break;
//    				}
//    			}
//    			
//    			if (foundSetting) {
//    				settings.put(key, val);
//    			} else {
//    				throw new RuntimeException("Game does not support " + key +" setting.");
//    			}
//    		}
    	}
    	
    }
    
    public double[] getPayouts() {
        return payouts;
    }
    
    public boolean isLoadedSuccesfully() {
        return true;
    }
    
    public Map<String,String> getSettings() {
    	return settings;
    }
}
