/**
 * 
 */
package com.magneta.games.configuration.slots;

import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;

/**
 * @author anarxia
 *
 */
@XmlRootElement 
public abstract class SlotConfiguration implements GameConfiguration {

    protected Long configId;
    protected int[][][] reelSet;
    protected Object[][] winCombos;
    protected int symbolCount;
    protected Map<String,String> settings;
    
    public SlotConfiguration() {
        this.configId = null;
        this.reelSet = getDefaultReels();
        this.winCombos = getDefaultWinCombos();
        this.symbolCount = getSymbolCount();
        this.settings = null;
    }
    
    public SlotConfiguration(long configId, String configuration) {
        this.configId = configId;
      
       
        SlotConfigurationParser sc = createConfigParser(configuration);
        if (!sc.isLoadedSuccesfully()) {
            throw new IllegalArgumentException("Invalid configuration");
        }

        this.reelSet = sc.getReels();
        
        double[] payouts = sc.getPayouts();
        if (payouts != null) {
            this.winCombos = newWinCombos(payouts);
        } else {
            this.winCombos = getDefaultWinCombos();
        }
        
        
        this.symbolCount = getSymbolCount();
        
        this.settings = sc.getSettings();
    }

    @XmlElement
    public abstract int getSymbolCount();
    
    public void setSymbolCount(int symbolCount) {
       this.symbolCount = symbolCount;
    }
    
    public boolean isJackpotCombination(int line, int combo) {
        return false;
    }

    public double getPayout(int winCombo) {
        return (Double)winCombos[winCombo][2];
    }
    
    public void setPayout(Object[][] combos, int winCombo, double payout) {
        combos[winCombo][2] = payout;
    }
    

    public final int[][] getReels(int index) {
        return this.reelSet[index];
    }
    
    public final int[][][] getReelSet() {
        return this.reelSet;
    }
    
    public final int getNumberOfReels(int index) {
    	if (index < 0 || index > this.reelSet.length) {
    		return 0;
    	}
    	
        return this.reelSet[index].length;
    }
    
    @XmlElement
    public final Object[][] getWinCombos() {
        return this.winCombos;
    }
    
    public final Object[] getWinCombo(int combo) {
        return this.winCombos[combo];
    }
    
    public void setWinCombos(Object[][] winCombos) {
        this.winCombos = winCombos;
    }
    
    @XmlElement
    @Override
    public Long getConfigId() {
        return this.configId;
    }
    
    public void setConfigId(Long configId) {
        this.configId = configId;
    }
    
    @Override
	public String serialize() {
    	StringBuilder sb = new StringBuilder();
    	
    	int[][][] reelsets = getReelSet();
		for(int z=0;z<reelsets.length;z++) {
			int[][] reels = getReels(z);
			for(int i=0;i<reels.length;i++) {
				int[] reel = reels[i];
				for(int k=0;k<reel.length;k++) {
					sb.append(Integer.toString(reel[k]));
					if(k != reel.length -1) {
						sb.append(",");
					}
				}
				sb.append("|\n");
			}
			sb.append(";");
		}

		Object[][] combos = getWinCombos();
		for(int i=0;i < combos.length;i++) {
			Object[] combo = combos[i];
			for(int z=0;z<combo.length;z++) {
				//Mean amount class;
				if(combo[z].getClass() == Double.class) {
					Double value = (Double) combo[z];
					sb.append(Integer.toString(i)).append("=").append(value).append(":");
				}
			}
		}
		
		if (this.settings != null) {
			for (Entry<String,String> e: settings.entrySet()) {
				sb.append(";");
				sb.append(e.getKey());
				sb.append('=');
				sb.append(e.getValue());
			}
		}
		
		return sb.toString();
    }
        
    protected abstract int[][][] getDefaultReels();
    protected abstract Object[][] getDefaultWinCombos();
    public abstract int[][][] getPaylines();
    
    public GameTemplateSetting[] getDefaultSettings() {
    	return null;
    }
    
    public final void getPayline(int[][] view, int line, int[] lineDest) {
    	for (int i=0; i < lineDest.length; i++) {
    		lineDest[i] = -1;
    	}
    	   	
    	int [][][] paylines = this.getPaylines();
    	
        if (line < 0 || line >= paylines.length) {
            return;
        }

        for (int i = 0; i < paylines[line].length; i++) {
        	if(paylines[line][i][0] == -1 || paylines[line][i][1] == -1) {
        		lineDest[i] = -1;
        	}else{
        		lineDest[i] = view[paylines[line][i][0]][paylines[line][i][1]];
        	}
        }
    }
    
    public int getNumberOfReelSets() {
    	return 1;
    }
    
    private Object[][] newWinCombos(double[] payouts) {
        Object[][] defaultWinCombos = getDefaultWinCombos();
        Object[][] newCombos = new Object[defaultWinCombos.length][];
        
        for (int i=0; i < defaultWinCombos.length; i++) {
            newCombos[i] = new Object[defaultWinCombos[i].length];
            for (int j=0; j < defaultWinCombos[i].length; j++) {
                newCombos[i][j] = defaultWinCombos[i][j];
            }
        }
        
        if (payouts != null) {
            for (int i=0; i < defaultWinCombos.length && i < payouts.length; i++) {
                setPayout(newCombos, i, payouts[i]);
            } 
        }
        
        return newCombos;
    }
    
    private SlotConfigurationParser createConfigParser(String configuration) {
        int[][][] defaultReels = getDefaultReels();
        Object[][] defaultWinCombos = getDefaultWinCombos();
   
        return new SlotConfigurationParser(configuration, getNumberOfReelSets(), defaultReels[0].length, 1, getSymbolCount(), defaultWinCombos.length, this.getDefaultSettings());
    }

    /**
     * @return
     */
    public int getMinSymbol() {
        return 0;
    }
    
    @SuppressWarnings("unchecked")
	public <T> T getSetting(String key, Class<T> settingType) {
    	GameTemplateSetting[] defaultSettings = this.getDefaultSettings();
    	
    	if (defaultSettings == null) {
    		throw new IllegalArgumentException("Unknown setting " + key);
    	}
    	
    	for (GameTemplateSetting s : defaultSettings) {
    		if (s.getKey().equals(key)) {
    			if (this.settings == null) {
    				return (T)s.getDefaultValue();
    			}
    			
    			String val = this.settings.get(key);
    			if (val == null) {
    				return (T)s.getDefaultValue();
    			}
    			
    			return (T)GameTemplateSetting.parseValue(val, s.getType());
    		}
    	}
    	
    	throw new IllegalArgumentException("Unknown setting " + key);
    }
    
    public Object getSetting(String key) {
    	GameTemplateSetting[] settings = this.getDefaultSettings();
    	
    	if (settings == null) {
    		throw new IllegalArgumentException("Unknown setting " + key);
    	}
    	
    	for (GameTemplateSetting s : settings) {
    		if (s.getKey().equals(key)) {
    			if (this.settings == null) {
    				return s.getDefaultValue();
    			}
    			
    			String val = this.settings.get(key);
    			if (val == null) {
    				return s.getDefaultValue();
    			}
    			
    			return GameTemplateSetting.parseValue(val, s.getType());
    		}
    	}
    	
    	throw new IllegalArgumentException("Unknown setting " + key);
    }
}
