package com.magneta.games.configuration.poker.islandstudpoker;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum WinType {
	BET_RATIO,
	JACKPOT_POOL_RATIO;
}