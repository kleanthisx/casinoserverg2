package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class ThunderstruckConfiguration extends SlotConfiguration {

	private static final int MAX_SYMBOL = 13;

	public static final int FREE_SPIN_SCATTER_1 = 13;
	public static final int WILD = 12;

	private static final int[][][] PAYLINES = {
		
		{ {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4} }, /* 1 0 */ // M/M/M/M/M
        { {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4} }, /* 2 1 */ // T/T/T/T/T
        { {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4} }, /* 3 2 */ // B/B/B/B/B
        
        { {0, 0}, {1, 1}, {2, 2}, {1, 3}, {0, 4} }, /* 4 3 */ // T/M/B/M/T
        { {2, 0}, {1, 1}, {0, 2}, {1, 3}, {2, 4} }, /* 5 4 */ // B/M/T/M/B
        
        { {1, 0}, {2, 1}, {2, 2}, {2, 3}, {1, 4} }, /* 6 5 */ // M/B/B/B/M
        { {1, 0}, {0, 1}, {0, 2}, {0, 3}, {1, 4} }, /* 7 6 */ // M/T/T/T/M
        
        { {2, 0}, {2, 1}, {1, 2}, {0, 3}, {0, 4} }, /* 8 7 */ // B/B/M/T/T
        { {0, 0}, {0, 1}, {1, 2}, {2, 3}, {2, 4} }, /* 9 8 */ // T/T/M/B/B
        

	};

	private static final int[][][] REELS = {
		{
    		/*Reel 1*/{12,2,11,5,1,2,7,10,9,5,4,6,13,9,8,3,2,1,3,9,10,5,6,9,8,11,9,8,4,5},
    		/*Reel 2*/{12,11,13,4,2,1,11,9,5,6,7,9,11,4,5,3,9,10,3,9,2,4,3,6,9,4,11,7,8,9},
    		/*Reel 3*/{12,2,10,8,1,4,7,10,8,6,10,13,3,1,6,11,10,8,5,6,10,7,3,8,4,1,8,10,9,8},
    		/*Reel 4*/{12,4,9,7,11,6,5,1,2,7,8,7,11,10,9,3,7,4,5,11,7,3,13,6,2,1,5,6,10,11},
    		/*Reel 5*/{12,3,2,9,8,4,1,2,5,10,6,4,9,1,10,6,8,7,5,3,8,9,3,1,2,5,7,11,6,10,8,7,2,4,10,3,6,11,9,1,5,13,2,4}
    	}
	};

	private static final Object[][] WIN_CONSTRAINTS = {
        /*Symbol         Quantity Req.   Multiplier*/
		/*C00*/{new Integer(12), new Integer(2), new Double(10.0   )},
        /*C01*/{new Integer(12), new Integer(3), new Double(200.0  )},
        /*C02*/{new Integer(12), new Integer(4), new Double(2000.0 )},
        /*C03*/{new Integer(12), new Integer(5), new Double(10000.0)},
		
    	/*C04*/{new Integer(1), new Integer(2), new Double(3.0)},
        /*C05*/{new Integer(1), new Integer(3), new Double(25.0)},
        /*C06*/{new Integer(1), new Integer(4), new Double(100.0)},
        /*C07*/{new Integer(1), new Integer(5), new Double(750.0)},
        
        /*C08*/{new Integer(2), new Integer(2), new Double(2.0)},
        /*C09*/{new Integer(2), new Integer(3), new Double(25.0)},
        /*C10*/{new Integer(2), new Integer(4), new Double(100.0)},
        /*C11*/{new Integer(2), new Integer(5), new Double(750.0)},
        
        /*C12*/{new Integer(3), new Integer(3), new Double(15.0)},
        /*C13*/{new Integer(3), new Integer(4), new Double(100.0)},
        /*C14*/{new Integer(3), new Integer(5), new Double(400.0)},
        
        /*C15*/{new Integer(4), new Integer(3), new Double(10.0)},
        /*C16*/{new Integer(4), new Integer(4), new Double(75.0)},
        /*C17*/{new Integer(4), new Integer(5), new Double(250.0)},
        
        /*C18*/{new Integer(5), new Integer(3), new Double(10.0)},
        /*C19*/{new Integer(5), new Integer(4), new Double(50.0)},
        /*C20*/{new Integer(5), new Integer(5), new Double(250.0)},
        
        /*C21*/{new Integer(6), new Integer(3), new Double(10.0)},
        /*C22*/{new Integer(6), new Integer(4), new Double(50.0)},
        /*C23*/{new Integer(6), new Integer(5), new Double(125.0)},
        
        /*C24*/{new Integer(7), new Integer(3), new Double(5.0)},
        /*C25*/{new Integer(7), new Integer(4), new Double(50.0)},
        /*C26*/{new Integer(7), new Integer(5), new Double(125.0)},
        
        /*C27*/{new Integer(8), new Integer(3), new Double(5.0)},
        /*C28*/{new Integer(8), new Integer(4), new Double(25.0)},
        /*C29*/{new Integer(8), new Integer(5), new Double(100.0)},
        
        /*C30*/{new Integer(9), new Integer(3), new Double(5.0)},
        /*C31*/{new Integer(9), new Integer(4), new Double(25.0)},
        /*C32*/{new Integer(9), new Integer(5), new Double(100.0)},
        
        /*C33*/{new Integer(10), new Integer(3), new Double(5.0)},
        /*C34*/{new Integer(10), new Integer(4), new Double(25.0)},
        /*C35*/{new Integer(10), new Integer(5), new Double(100.0)},
        
        /*C36*/{new Integer(11), new Integer(2), new Double(2.0)},
        /*C37*/{new Integer(11), new Integer(3), new Double(5.0)},
        /*C38*/{new Integer(11), new Integer(4), new Double(25.0)},
        /*C39*/{new Integer(11), new Integer(5), new Double(100.0)},
        
        /*C40*/{new Integer(-13), new Integer(2), new Double(2.0  )}, /* Bonus payouts */
        /*C41*/{new Integer(-13), new Integer(3), new Double(5.0  )}, 
        /*C42*/{new Integer(-13), new Integer(4), new Double(20.0 )},
        /*C43*/{new Integer(-13), new Integer(5), new Double(500.0)},
		
	};

	public ThunderstruckConfiguration() {
		super();
	}

	public ThunderstruckConfiguration(long configId, String config) {
		super(configId, config);
	}

	@Override
	protected int[][][] getDefaultReels() {
		return REELS;
	}

	@Override
	protected Object[][] getDefaultWinCombos() {
		return WIN_CONSTRAINTS;
	}

	@Override
	public int getSymbolCount() {
		return MAX_SYMBOL;
	}

	@Override
	public boolean isJackpotCombination(int line, int combo) {
		return combo == 3;
	}

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}

	@Override
	public int getNumberOfReelSets() {
		return REELS.length;
	}

}
