/**
 * 
 */
package com.magneta.games.configuration.slots;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author User
 *
 */
@XmlRootElement 
public class TuttiFruityConfiguration extends SlotConfiguration {

    private static final int MAX_SYMBOL = 11;
    
    private static final int[][][] PAYLINES = {
    	{ {1,0}, {1,1}, {1,2} }, /* 0 */
    	{ {0,0}, {0,1}, {0,2} }, /* 1 */
    	{ {2,0}, {2,1}, {2,2} }, /* 2 */
    	{ {0,0}, {1,1}, {2,2} }, /* 3 */
    	{ {2,0}, {1,1}, {0,2} }  /* 4 */
    };
    
    private static final int[][][] REELS = {
    	{
    		{1,1,1,2,3,3,3,4,4,4,5,6,6,7,8,1,1,1,3,3,3,4,4,4,5,6,7,7,3,1,1,3,3,4,4,5,6,7,5,1,1,3,4,5,7,1,5,3,1,5,1},
    		{1,1,1,2,3,3,3,4,4,4,6,7,7,7,8,9,1,1,1,9,3,3,4,4,6,7,7,3,1,9,1,1,9,3,3,4,4,7,7,7,1,1,3,3,3,4,4,6,7,1,1},
    		{1,1,1,2,3,5,3,3,5,7,7,7,8,11,3,1,1,1,3,3,3,5,7,3,11,1,1,3,3,3,5,7,7,1,1,3,3,5,7,7,1,5,3,3,3,7,11,3,5,7,1}
    	}
    };
    
    private static final Object[][] WIN_COMBOS = {
              /*Reel 1           Reel 2           Reel 3           Multiplier    */
        /*C1 */{new Integer(4),  new Integer(0),  new Integer(0),  new Double(1.0)},
        /*C2 */{new Integer(4),  new Integer(4),  new Integer(0),  new Double(3.0)},
        /*C3 */{new Integer(1),  new Integer(1),  new Integer(1),  new Double(10.0)},
        /*C4 */{new Integer(1),  new Integer(1),  new Integer(11), new Double(10.0)},
        /*C5 */{new Integer(1),  new Integer(9),  new Integer(11), new Double(10.0)},
        /*C6 */{new Integer(3),  new Integer(3),  new Integer(3),  new Double(15.0)},
        /*C7 */{new Integer(3),  new Integer(3),  new Integer(11), new Double(15.0)},
        /*C8 */{new Integer(3),  new Integer(9),  new Integer(11), new Double(15.0)},
        /*C9 */{new Integer(7),  new Integer(7),  new Integer(7),  new Double(20.0)},
        /*C10*/{new Integer(-8), new Integer(-8), new Integer(-8), new Double(50.0)},
        /*C11*/{new Integer(5),  new Integer(9),  new Integer(11), new Double(100.0)},
        /*C12*/{new Integer(6),  new Integer(6),  new Integer(11), new Double(150.0)},
        /*C13*/{new Integer(2),  new Integer(2),  new Integer(2),  new Double(250.0)}
    };
    
    public TuttiFruityConfiguration() {
        super();
    }
    
    public TuttiFruityConfiguration(long configId, String config) {
        super(configId, config);
    }
    
    @Override
    protected int[][][] getDefaultReels() {
        return REELS;
    }
    
    @Override
    protected Object[][] getDefaultWinCombos() {
        return WIN_COMBOS;
    }

    @Override
    public int getSymbolCount() {
        return MAX_SYMBOL;
    }
    
    @Override
    public double getPayout(int winCombo) {
        return (Double)winCombos[winCombo][3];
    }
    
    @Override
    public void setPayout(Object[][] combos, int winCombo, double payout) {
        combos[winCombo][3] = payout;
    }
    
    @Override
    public boolean isJackpotCombination(int line, int combo) {
        return combo == 12;
    }

	@Override
	public int[][][] getPaylines() {
		return PAYLINES;
	}
}
