/**
 * 
 */
package com.magneta.games.configuration.poker;


/**
 * @author User
 *
 */
public interface PokerConstants {
    
    public static final int NONE = 0;
    public static final int JACKS_OR_BETTER = 1; // (or pair)
    public static final int TWO_PAIRS = 2;
    public static final int THREE_OF_A_KIND = 3;
    public static final int STRAIGHT = 4;
    public static final int FLUSH = 5;
    public static final int FULL_HOUSE = 6;
    public static final int FOUR_OF_A_KIND = 7;
    public static final int STRAIGHT_FLUSH = 8;
    public static final int ROYAL_FLUSH = 9;

    public static final int POSSIBLE_STRAIGHT = 10;
    public static final int POSSIBLE_FLUSH = 11;
    public static final int POSSIBLE_STRAIGHT_FLUSH = 12;
    public static final int POSSIBLE_ROYAL_FLUSH = 13;
}