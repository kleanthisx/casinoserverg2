/**
 * 
 */
package com.magneta.games.configuration;


/**
 * A configuration class for games that cannot really be configured.
 * All games must have configuration classes.
 * 
 * @author anarxia
 *
 */
public class DummyConfiguration implements GameConfiguration {

    private Long configId;
    
    public DummyConfiguration() {
        this.configId = null;
    }
    
    public DummyConfiguration(long configId, String config)  {
        this.configId = configId;
    }
    
    @Override
    public final Long getConfigId() {
        return this.configId;
    }

	@Override
	public String serialize() {
		return null;
	}

}
