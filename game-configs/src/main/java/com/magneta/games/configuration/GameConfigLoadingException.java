/**
 * 
 */
package com.magneta.games.configuration;


/**
 * @author anarxia
 *
 */
public class GameConfigLoadingException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1189199546917973199L;

    public GameConfigLoadingException(String msg, Throwable e) {
        super(msg, e);
    }
    
    public GameConfigLoadingException(String msg) {
        super(msg);
    }
}
