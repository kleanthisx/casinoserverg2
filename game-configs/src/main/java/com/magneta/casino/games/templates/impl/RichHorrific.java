/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.RichHorrificConfiguration;


/**
 * @author User
 *
 */
@GameTemplate("RichHorrific")
public class RichHorrific extends SlotGameTemplate {

    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#getConfigurationClass()
     */
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return RichHorrificConfiguration.class;
    }

}
