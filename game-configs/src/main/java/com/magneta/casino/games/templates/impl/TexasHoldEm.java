/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;

/**
 * @author anarxia
 *
 */
@GameTemplate("TexasHoldEm")
public class TexasHoldEm extends AbstractGameTemplateInfo {
    
    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
    	new GameTemplateSetting("min_side_bet", GameTemplateSetting.DOUBLE, "Min. Side Bet", 0.10),
    	new GameTemplateSetting("max_side_bet", GameTemplateSetting.DOUBLE, "Max. Side Bet", 1.00),
    };
    
    @Override
    public boolean supportsConfiguration() {
        return true;
    }
    
    @Override
    public boolean supportsGameJackpot() {
        return false;
    }

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return TexasHoldemConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
