/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.MexicoConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("Mexico")
public class Mexico extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return MexicoConfiguration.class;
    }
}
