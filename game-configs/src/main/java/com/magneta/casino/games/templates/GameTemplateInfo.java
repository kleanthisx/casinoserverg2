/**
 * 
 */
package com.magneta.casino.games.templates;

import java.util.List;

import com.magneta.games.configuration.GameConfiguration;


/**
 * @author anarxia
 *
 */
public interface GameTemplateInfo {

    /**
     * Whether the game supports game jackpot
     * @return
     */
    boolean supportsGameJackpot();
    
    /**
     * Whether the game supports mystery jackpot
     * @return
     */
    boolean supportsMysteryJackpot();
    
    /**
     * Whether the game is multi-seat
     * @return
     */    
    boolean isMultiSeat();
    
    /**
     * Gets the template name for the game
     * @return
     */
    String getTemplate();
    
    /**
     * Whether the game supports configuration (in game_configs)
     * @return
     */
    boolean supportsConfiguration();
    
    /**
     * Returns the game configuration class
     * @return
     */
    Class<? extends GameConfiguration> getConfigurationClass();
    
    void getSettings(List<GameTemplateSetting> list);
    GameTemplateSetting getSetting(String key);
}
