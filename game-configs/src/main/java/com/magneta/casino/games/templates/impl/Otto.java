/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.OttoConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("Otto")
public class Otto extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return OttoConfiguration.class;
    }
}
