/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.StopTimeConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("StopTime")
public class StopTime extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return StopTimeConfiguration.class;
    }
    
    @Override
    public boolean supportsGameJackpot() {
        return false;
    }
}
