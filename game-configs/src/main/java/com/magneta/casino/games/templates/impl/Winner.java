package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.WinnerConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("Winner")
public class Winner extends SlotGameTemplate {

    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("allow_double_up", GameTemplateSetting.BOOLEAN, "Enable double up", true),
        new GameTemplateSetting("max_double_win_amount", GameTemplateSetting.DOUBLE, "Max. double up win amount", 100000.00d),
        new GameTemplateSetting("max_double_amount", GameTemplateSetting.DOUBLE, "Max. win amount to allow double up", 4000.00d),
    };
    
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return WinnerConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}

