package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.ReelSteelConfiguration;

/**
 * @author Kleanthis
 *
 */ 
@GameTemplate("ReelSteel")
public class ReelSteel extends SlotGameTemplate {

	 private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
	        new GameTemplateSetting("max_payout", GameTemplateSetting.DOUBLE, "Maximum Single Spin Pay", 0.0,true),
	    };
	 
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return ReelSteelConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }

}
