/**
 * 
 */
package com.magneta.casino.games.templates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.reflections.Configuration;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.reflections.Reflections;

import com.google.common.base.Predicate;

import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author Nicos
 *
 */
public class GameTemplateInfoFactory {

    private static final Logger logger = LoggerFactory.getLogger(GameTemplateInfoFactory.class);

    private static final Map<String,GameTemplateInfo> gameTemplateInfos = new HashMap<String,GameTemplateInfo>();
    
    static {
        
        Configuration config = new LocalConfigurationBuilder() {
            String prefix = GameTemplateInfo.class.getPackage().getName();
            final Predicate<String> filter = new FilterBuilder.Include(FilterBuilder.prefix(prefix));

            {
                filterInputsBy(filter);
                setUrls(ClasspathHelper.forPackage(prefix));

                setScanners(
                        new TypeAnnotationsScanner().filterResultsBy(filter),
                        new SubTypesScanner().filterResultsBy(filter));
            }
        }; 
            
        Reflections reflections = new Reflections(config);

        Set<Class<? extends GameTemplateInfo>> annotated = reflections.getSubTypesOf(GameTemplateInfo.class); 

        for(Class<? extends GameTemplateInfo> test:annotated) {
            GameTemplate gTemplate = test.getAnnotation(GameTemplate.class);
            if (gTemplate == null) {
                continue;
            }
            
            try {
                gameTemplateInfos.put(gTemplate.value(), test.newInstance());
            } catch (InstantiationException e) {
                logger.error("Unable to create GameTemplateInfo " + gTemplate.value(),e);
            } catch (IllegalAccessException e) {
                logger.error("Unable to create GameTemplateInfo " + gTemplate.value(),e);
            }
        }
    }
    
    /**
     * Calls this to initialize the factory early on.
     * This is to avoid stalls when the factory is used for the first time.
     */
    public static final void init() {
    	/* Do nothing */
    }
    
    public static final GameTemplateInfo getGameTemplateInfo(String gameTemplate) {
        return gameTemplateInfos.get(gameTemplate);
    }
    
    /**
     * Returns a list of gameinfo templates. This is
     * the best way to get a list of known templates since all
     * games are required to have a gameinfo class.
     * @return
     */
    public static final List<String> getGameTemplates() {
        List<String> templates = new ArrayList<String>();
        
        for (String t: gameTemplateInfos.keySet()) {
            templates.add(t);
        }
        
        Collections.sort(templates);
        
        return templates;
    }
}
