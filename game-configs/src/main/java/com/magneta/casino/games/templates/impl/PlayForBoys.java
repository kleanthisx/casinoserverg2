/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.PlayForBoysConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("PlayForBoys")
public class PlayForBoys extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return PlayForBoysConfiguration.class;
    }
}
