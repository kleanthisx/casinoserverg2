package com.magneta.casino.games.templates;


import java.net.URL;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.reflections.Configuration;
import org.reflections.adapters.JavassistAdapter;
import org.reflections.adapters.MetadataAdapter;
import org.reflections.scanners.Scanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.serializers.Serializer;
import org.reflections.util.FilterBuilder;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

/**
 * a fluent builder for {@link org.reflections.Configuration}, to be used for constructing a {@link org.reflections.Reflections} instance
 * <p>usage:
 * <pre>
 *      new Reflections(
 *          new ConfigurationBuilder()
 *              .filterInputsBy(new FilterBuilder().include("your project's common package prefix here..."))
 *              .setUrls(ClasspathHelper.getUrlsForCurrentClasspath())
 *              .setScanners(new SubTypesScanner(), new TypeAnnotationsScanner().filterResultsBy(myClassAnnotationsFilter)));
 * </pre>
 * <p>default constructor sets reasonable defaults, such as SingleThreadExecutor for scanning, accept all for {@link #inputsFilter}
 * , scanners set to {@link org.reflections.scanners.SubTypesScanner}, {@link org.reflections.scanners.TypeAnnotationsScanner},
 * using {@link org.reflections.serializers.XmlSerializer}
 */
public class LocalConfigurationBuilder implements Configuration {

    private Set<Scanner> scanners;
    private Set<URL> urls;
    private MetadataAdapter<?, ?, ?> metadataAdapter;
    private Predicate<String> inputsFilter;

    public LocalConfigurationBuilder() {
        //defaults
        scanners = Sets.<Scanner>newHashSet(new SubTypesScanner(), new TypeAnnotationsScanner());
        metadataAdapter = new JavassistAdapter();
        inputsFilter = Predicates.alwaysTrue();
    }

    @Override
    public Set<Scanner> getScanners() {
		return scanners;
	}

    /** set the scanners instances for scanning different metadata */
    public LocalConfigurationBuilder setScanners(final Scanner... scanners) {
        this.scanners = ImmutableSet.copyOf(scanners);
        return this;
    }

    @Override
    public Set<URL> getUrls() {
        return urls;
    }

    /** set the urls to be scanned
     * <p>use {@link org.reflections.util.ClasspathHelper} convenient methods to get the relevant urls
     * */
    public LocalConfigurationBuilder setUrls(final Collection<URL> urls) {
		this.urls = ImmutableSet.copyOf(urls);
        return this;
	}

    /** set the urls to be scanned
     * <p>use {@link org.reflections.util.ClasspathHelper} convenient methods to get the relevant urls
     * */
    public LocalConfigurationBuilder setUrls(final URL... urls) {
		this.urls = ImmutableSet.copyOf(urls);
        return this;
	}

    @SuppressWarnings("rawtypes")
    @Override
    public MetadataAdapter getMetadataAdapter() {
        return metadataAdapter;
    }

    /** sets the metadata adapter used to fetch metadata from classes */
    public LocalConfigurationBuilder setMetadataAdapter(final MetadataAdapter<?, ?, ?> metadataAdapter) {
        this.metadataAdapter = metadataAdapter;
        return this;
    }

    @Override
    public boolean acceptsInput(String inputFqn) {
        return inputsFilter.apply(inputFqn);
    }

    /** sets the input filter for all resources to be scanned
     * <p> supply a {@link com.google.common.base.Predicate} or use the {@link FilterBuilder}*/
    public LocalConfigurationBuilder filterInputsBy(Predicate<String> iFilter) {
        this.inputsFilter = iFilter;
        return this;
    }

    @Override
    public Serializer getSerializer() {
        return null;
    }

	@Override
	public ExecutorService getExecutorService() {
		return null;
	}

	@Override
	public ClassLoader[] getClassLoaders() {
		return new ClassLoader[] { this.getClass().getClassLoader() };
	}
}
