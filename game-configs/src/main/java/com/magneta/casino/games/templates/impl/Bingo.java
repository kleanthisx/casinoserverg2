/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.bingo.BingoConfiguration;

@GameTemplate("Bingo")
public class Bingo extends AbstractGameTemplateInfo {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return BingoConfiguration.class;
    }
    
    @Override
	public boolean supportsGameJackpot(){
    	return true;
    }
}
