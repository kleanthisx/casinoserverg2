/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.PlayBoyConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("PlayBoy")
public class PlayBoy extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return PlayBoyConfiguration.class;
    }
    
    @Override
    public boolean supportsGameJackpot() {
        return false;
    }
}
