/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.MiniBarConfiguration;

/**
 * @author User
 *
 */ 
@GameTemplate("MiniBar")
public class MiniBar extends SlotGameTemplate {

    @Override
    public boolean supportsGameJackpot() {
        return false;
    }
    
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return MiniBarConfiguration.class;
    }
}
