/**
 * 
 */
package com.magneta.casino.games.templates;

import java.io.Serializable;

/**
 * @author anarxia
 *
 */
public class GameTemplateSetting implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2990093966852419357L;

	public static final int INTEGER = 0;
    public static final int DOUBLE = 1;
    public static final int BOOLEAN = 2;
    public static final int STRING = 3;
    public static final int PERCENTAGE = 4;
    public static final int COUNTRY_VALUE = 5;
    
    private String key;
    private String description;
    private int type;
    private Object defaultValue;
    private boolean hidden;
    
    public GameTemplateSetting() {
        super();
    }
    
    public GameTemplateSetting(String key, int type, String description, Object defaultValue) { 
        this.key = key;
        this.type = type;
        this.description = description;
        this.hidden = false;
        this.defaultValue = defaultValue;
    }
    
    public GameTemplateSetting(String key, int type, String description, Object defaultValue, boolean hidden) { 
        this.key = key;
        this.type = type;
        this.description = description;
        this.hidden = hidden;
        this.defaultValue = defaultValue;
    }
    
    public GameTemplateSetting(GameTemplateSetting orig) {
    	this.key = orig.getKey();
    	this.type = orig.getType();
    	this.description = orig.getDescription();
    	this.defaultValue = orig.getDefaultValue();
    	this.hidden = orig.isHidden();
    }
    
    /**
     * @param key The key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }
    /**
     * @return Returns the key.
     */
    public String getKey() {
        return key;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param type The type to set.
     */
    public void setType(int type) {
        this.type = type;
    }
    /**
     * @return Returns the type.
     */
    public int getType() {
        return type;
    }
    
    public Object getDefaultValue() {
        return defaultValue;
    }
    
    public void setDefaultValue(Object value) {
        this.defaultValue = value;
    }

    /**
     * @param hidden The hidden to set.
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return Returns the hidden.
     */
    public boolean isHidden() {
        return hidden;
    }
    
    public static Object parseValue(String value, int type) {
		switch (type){
		case INTEGER:
			return Integer.parseInt(value);
		case DOUBLE:
			return Double.parseDouble(value);
		case BOOLEAN:
			return Boolean.parseBoolean(value);
		case STRING:
		case COUNTRY_VALUE:
			return value;
		case PERCENTAGE:
			return new Double(Double.parseDouble(value) * 100.0);
		default:
			throw new RuntimeException("Unknown setting type " + type);
	}
}
}
