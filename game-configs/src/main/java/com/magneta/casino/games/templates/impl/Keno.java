/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.keno.KenoConfiguration;

@GameTemplate("Keno")
public class Keno extends AbstractGameTemplateInfo {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return KenoConfiguration.class;
    }
    
    @Override
	public boolean supportsGameJackpot(){
    	return true;
    }
}
