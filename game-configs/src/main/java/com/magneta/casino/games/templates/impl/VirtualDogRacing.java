/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author anarxia
 *
 */
@GameTemplate("VirtualDogRacing")
public class VirtualDogRacing extends VirtualRacing {

}
