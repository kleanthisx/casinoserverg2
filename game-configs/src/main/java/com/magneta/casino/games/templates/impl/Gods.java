/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.GodsConfiguration;

/**
 * @author User
 *
 */ 
@GameTemplate("Gods")
public class Gods extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return GodsConfiguration.class;
    }
}
