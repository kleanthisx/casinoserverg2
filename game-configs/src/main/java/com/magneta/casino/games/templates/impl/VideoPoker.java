package com.magneta.casino.games.templates.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.videopoker.VideoPokerConfiguration;

/**
 * @author User
 *
 */
@XmlRootElement 
@GameTemplate("VideoPoker")
public class VideoPoker extends AbstractGameTemplateInfo {
    
    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("allow_double_up", GameTemplateSetting.BOOLEAN, "Enable double up", true),
    };
    
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return VideoPokerConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
