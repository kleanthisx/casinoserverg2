/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.DummyConfiguration;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;


/**
 * @author anarxia
 *
 */
@GameTemplate("Blackjack")
public class Blackjack extends AbstractGameTemplateInfo {

    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("decks", GameTemplateSetting.INTEGER, "Number of decks", 6),
        new GameTemplateSetting("seats", GameTemplateSetting.INTEGER, "Number of seats", 3),
    };
    
    @Override
    public boolean supportsConfiguration() {
        return false;
    }
    
    @Override
    public boolean isMultiSeat() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#getConfigurationClass()
     */
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return DummyConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
