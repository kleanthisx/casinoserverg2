/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.StopForTimeConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("StopForTime")
public class StopForTime extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return StopForTimeConfiguration.class;
    }
}
