/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.TuttiFruityConfiguration;

/**
 * @author User
 *
 */ 
@GameTemplate("TuttiFruity")
public class TuttiFruity extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return TuttiFruityConfiguration.class;
    }
}
