/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.CrazyCircusConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("CrazyCircus")
public class CrazyCircus extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return CrazyCircusConfiguration.class;
    }    
}
