/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;

/**
 * @author anarxia
 *
 */
@GameTemplate("IslandStudPoker")
public class IslandStudPoker extends AbstractGameTemplateInfo {
    
    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
    	new GameTemplateSetting("jackpot_bet", GameTemplateSetting.DOUBLE, "Jackpot Bet", 1.0)
    };
    
    @Override
    public boolean supportsConfiguration() {
        return true;
    }
    
    @Override
    public boolean supportsGameJackpot() {
        return true;
    }

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return IslandStudPokerConfiguration.class;
    }   
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
