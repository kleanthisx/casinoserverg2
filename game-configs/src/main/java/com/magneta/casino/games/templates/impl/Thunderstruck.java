/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.ThunderstruckConfiguration;

@GameTemplate("Thunderstruck")
public class Thunderstruck extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return ThunderstruckConfiguration.class;
    }

}
