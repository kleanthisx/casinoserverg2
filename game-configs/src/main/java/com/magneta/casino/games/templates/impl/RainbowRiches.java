/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.RainbowRichesConfiguration;

/**
 * @author User
 *
 */ 
@GameTemplate("RainbowRiches")
public class RainbowRiches extends SlotGameTemplate {

	private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("max_payout", GameTemplateSetting.DOUBLE, "Maximum Single Spin Pay", 0.0,true),
    };
	
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return RainbowRichesConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
