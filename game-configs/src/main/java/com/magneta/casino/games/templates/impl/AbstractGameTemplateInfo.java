/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.annotations.GameTemplate;


/**
 * @author anarxia
 *
 */
public abstract class AbstractGameTemplateInfo implements GameTemplateInfo {

    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("guard", GameTemplateSetting.BOOLEAN, "Enable game guard", false, true),
        new GameTemplateSetting("payout_ratio", GameTemplateSetting.PERCENTAGE, "Guard payout", 0.95d, true),
    };
    
    @Override
    public boolean supportsConfiguration() {
        return true;
    }
    
    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#supportsGameJackpot()
     */
    @Override
    public boolean supportsGameJackpot() {
        return false;
    }

    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#supportsMysteryJackpot()
     */
    @Override
    public boolean supportsMysteryJackpot() {
        return false;
    }
    
    @Override
    public boolean isMultiSeat() {
        return false;
    }

    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#getTemplate()
     */
    @Override
    public final String getTemplate() {
        GameTemplate template = this.getClass().getAnnotation(GameTemplate.class);
        if (template == null)
            return null;
        
        return template.value();
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
    
    @Override
    public GameTemplateSetting getSetting(String key) {
        List<GameTemplateSetting> settings = new ArrayList<GameTemplateSetting>();
        
        getSettings(settings);

        for (GameTemplateSetting s: settings) {
            if (s.getKey().equals(key)) {
                return s;
            }
        }
        
        return null;
    }
}
