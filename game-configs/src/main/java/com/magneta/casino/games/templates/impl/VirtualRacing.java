/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.DummyConfiguration;
import com.magneta.games.configuration.GameConfiguration;


/**
 * @author anarxia
 *
 */
public class VirtualRacing extends AbstractGameTemplateInfo {

    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("odds_gain_rate", GameTemplateSetting.PERCENTAGE, "Odds profit percentage", 0.15d),
    };
    
    @Override
    public boolean supportsConfiguration() {
        return false;
    }
    
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return DummyConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
