/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import java.util.List;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.games.configuration.DummyConfiguration;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author anarxia
 *
 */
@GameTemplate("FrenchRoulette")
public class FrenchRoulette extends AbstractGameTemplateInfo {

    private static final GameTemplateSetting[] DEFAULT_SETTINGS = {
        new GameTemplateSetting("total_min_bet", GameTemplateSetting.DOUBLE, "Total Min bet", 0.00d),
        new GameTemplateSetting("total_max_bet", GameTemplateSetting.DOUBLE, "Total Max bet", 400.00d),
    };
    
    @Override
    public boolean supportsConfiguration() {
        return false;
    }

    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#getConfigurationClass()
     */
    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return DummyConfiguration.class;
    }
    
    @Override
    public void getSettings(List<GameTemplateSetting> list) {
        super.getSettings(list);
        for (GameTemplateSetting s: DEFAULT_SETTINGS) {
            list.add(s);
        }
    }
}
