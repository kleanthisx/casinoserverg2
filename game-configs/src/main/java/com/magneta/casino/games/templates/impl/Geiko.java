/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.GeikoConfiguration;

/**
 * @author User
 *
 */ 
@GameTemplate("Geiko")
public class Geiko extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return GeikoConfiguration.class;
    }
}
