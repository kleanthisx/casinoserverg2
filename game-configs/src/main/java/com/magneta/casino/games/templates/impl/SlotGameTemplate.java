/**
 * 
 */
package com.magneta.casino.games.templates.impl;

/**
 * @author anarxia
 *
 */
public abstract class SlotGameTemplate extends AbstractGameTemplateInfo {
    
    @Override
    public boolean supportsGameJackpot() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.magneta.casino.games.templates.GameTemplateInfo#supportsMysteryJackpot()
     */
    @Override
    public boolean supportsMysteryJackpot() {
        return true;
    }
}
