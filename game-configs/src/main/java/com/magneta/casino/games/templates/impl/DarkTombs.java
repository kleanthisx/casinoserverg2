/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.DarkTombsConfiguration;

/**
 * @author User
 *
 */
@GameTemplate("DarkTombs")
public class DarkTombs extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return DarkTombsConfiguration.class;
    }
}
