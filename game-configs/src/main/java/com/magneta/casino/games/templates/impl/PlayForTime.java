/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.PlayForTimeConfiguration;

/**
 * @author User
 *
 */ 
@GameTemplate("PlayForTime")
public class PlayForTime extends SlotGameTemplate {

    @Override
    public Class<? extends GameConfiguration> getConfigurationClass() {
        return PlayForTimeConfiguration.class;
    }
}
