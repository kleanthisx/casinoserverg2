/**
 * 
 */
package com.magneta.casino.games.templates.impl;

import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author anarxia
 *
 */
@GameTemplate("VirtualHorseRacing")
public class VirtualHorseRacing extends VirtualRacing {
    
}
