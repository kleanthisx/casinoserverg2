@echo off
TITLE Casino Server Build

SET MVNOPTS="-Denv=distribution -Dmaven.test.skip=true -DincludeScope=runtime"

cmd /C mvn -f casino-server/pom.xml %MVNOPTS% clean package
