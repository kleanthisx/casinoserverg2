/**
 * 
 */
package com.magneta.xmlrpc.client;

import org.apache.xmlrpc.client.XmlRpcClient;

/**
 * @author Nicos
 *
 */
public class SocketXmlRpcClient extends XmlRpcClient {
   
    public SocketXmlRpcClient(String host, int port, boolean useSSL) {
        this(host, port, useSSL, null, null);
    }

    public SocketXmlRpcClient(String host, int port, boolean useSSL, String trustStore, String trustPass) {
        super();
        SocketXmlRpcClientConfig config = new SocketXmlRpcClientConfig(host, port, useSSL, trustStore, trustPass);
        
        setConfig(config);
        setTransportFactory(new SocketXmlRpcTransport(this));
    }
    
    public void destroy() {
        SocketXmlRpcTransport transport = (SocketXmlRpcTransport)getTransportFactory();
        transport.destroy();
    }
}
