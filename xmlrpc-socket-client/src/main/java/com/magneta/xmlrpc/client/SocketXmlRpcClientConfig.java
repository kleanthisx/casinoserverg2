/**
 * 
 */
package com.magneta.xmlrpc.client;

import java.util.TimeZone;

import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.common.XmlRpcStreamRequestConfig;


/**
 * XML-RPC over TCP client configuration
 * 
 * @author Nicos
 *
 * This provides the transport with the required configuration.
 * 
 * <p>The following defaults should not be changed as they will break over-the-wire compatibility:</p>
 * <ul>
 * <li>Encoding: MUST be UTF-8 as per protocol.</li>
 * <li>GZIP compression: Not supported.</li>
 * <li>Timezone: is fixed to UTC to avoid timezone issues.</li>
 * </ul>
 */
public class SocketXmlRpcClientConfig implements XmlRpcClientConfig, XmlRpcStreamConfig, XmlRpcStreamRequestConfig {

	/**
	 * Maximum response size. Arbirtarily set to 1Mbyte.
	 */
	private static final int MAX_RESPONSE = 1000000;
	
    private final String host;
    private final int port;
    
    private final boolean useSSL;
    private final String trustStore;
    private final String trustPass;
    
    public SocketXmlRpcClientConfig(String host, int port, boolean useSSL, String trustStore, String trustPass) {
        this.host = host;
        this.port = port;
        this.useSSL = useSSL;
        this.trustStore = trustStore;
        this.trustPass = trustPass;
    }
    
    @Override
	public TimeZone getTimeZone() {
        return TimeZone.getTimeZone("UTC");
    }
    
    @Override
	public String getEncoding() {
        return XmlRpcStreamConfig.UTF8_ENCODING;
    }
    
    @Override
	public boolean isEnabledForExceptions() {
        return false;
    }

    @Override
	public boolean isEnabledForExtensions() {
        return false;
    }

    @Override
	public boolean isGzipCompressing() {
        return false;
    }

    @Override
	public boolean isGzipRequesting() {
        return false;
    }

    public String getHost() {
        return this.host;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public boolean getUseSSL() {
        return this.useSSL;
    }
    
    public int getMaxResponseSize() {
    	return MAX_RESPONSE;
    }

	public String getTrustPass() {
		return trustPass;
	}

	public String getTrustStore() {
		return trustStore;
	}
}
