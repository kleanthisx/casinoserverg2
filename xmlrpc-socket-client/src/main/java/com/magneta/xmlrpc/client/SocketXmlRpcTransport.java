/**
 * 
 */
package com.magneta.xmlrpc.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientException;
import org.apache.xmlrpc.client.XmlRpcTransport;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.apache.xmlrpc.parser.XmlRpcResponseParser;
import org.apache.xmlrpc.serializer.XmlRpcWriter;
import org.apache.xmlrpc.util.SAXParsers;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;


/**
 * @author Nicos
 *
 */
public class SocketXmlRpcTransport implements XmlRpcTransport, XmlRpcTransportFactory {

    /*
     * End of response marker "</methodResponse>"
    */
    private static final byte[] END_OF_RESPONSE = {
   /* <     /     m     e     t     h     o     d     R     e     s     p     o     n     s     e     >*/ 
      0x3c, 0x2f, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x3e };
    
    /**
     * Detects when all data for a methodCall have been
     * received.
     * @author Nicos
     *
     */
    private static class ServerResponseScanner {
    
        private int lastRequestScan;
        private byte[] requestBuf;
        private int requestLen;
        
        public ServerResponseScanner(byte[] requestBuf) {
            lastRequestScan = 0;
            this.requestBuf = requestBuf;
            this.requestLen = 0;
        }
        
        public int getRequestLength() {
            return requestLen;
        }
        
        public boolean requestFinished(int requestSize) {

            int requestScanOffset = 0;
            requestLen = 0;
            
            if (requestSize >= END_OF_RESPONSE.length) {

                for (int i = lastRequestScan; i < requestSize; i++) {
                    if (END_OF_RESPONSE[requestScanOffset] == requestBuf[i]) {
                        requestScanOffset++;
                        if (requestScanOffset == END_OF_RESPONSE.length) {
                            lastRequestScan = 0;
                            requestLen = i + 1;
                            return true;
                        }

                    } else {
                        requestScanOffset = 0;
                    }
                }
            }

            return false;
        }
    
    }
    
    
    private final SocketXmlRpcClientConfig config;
    private final XmlRpcClient client;
    private final byte[] responseBuf;
    
    private Socket sock;
    private InputStream in;
    private OutputStream out;
    
    public SocketXmlRpcTransport(XmlRpcClient client) {
        this.client = client;
        this.config = (SocketXmlRpcClientConfig)client.getConfig();
        this.responseBuf = new byte[config.getMaxResponseSize()];
    }

    private void sslConnect() throws XmlRpcException {
        
        SocketFactory socketFactory;
        
        if (config.getTrustStore() != null) {
            KeyStore ksKeys = null;

            try {
                ksKeys = KeyStore.getInstance(KeyStore.getDefaultType());
            } catch (KeyStoreException e) {
                throw new XmlRpcException("Could not get default keystore type", e);
            }
            try {
                if (config.getTrustPass() != null) {
                    ksKeys.load(this.getClass().getClassLoader().getResourceAsStream(config.getTrustStore()), config.getTrustPass().toCharArray());
                } else {
                    ksKeys.load(this.getClass().getClassLoader().getResourceAsStream(config.getTrustStore()), null);
                }
            } catch (Exception e) {
                throw new XmlRpcException("Could not load client keystore", e);
            }


            /* KeyManager's decide which key material to use. */
            TrustManagerFactory tmf = null;

            try {
                tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            } catch (NoSuchAlgorithmException e) {
                throw new XmlRpcException("Could not get trust manager factory", e);
            }

            try {
                tmf.init(ksKeys);
            } catch (KeyStoreException e) {
                throw new XmlRpcException("Error while initializing keystore", e);
            }

            SSLContext sslContext;
            try {
                sslContext = SSLContext.getInstance("SSLv3");
            } catch (NoSuchAlgorithmException e) {
                throw new XmlRpcException("Could not get SSL context", e);
            }
            
            try {
                sslContext.init(null, tmf.getTrustManagers(), null);
            } catch (KeyManagementException e) {
                throw new XmlRpcException("Could not initialize SSL context", e);
            }


            socketFactory = sslContext.getSocketFactory();
        } else {
            socketFactory = SSLSocketFactory.getDefault();
        }
        
        try {
            sock = socketFactory.createSocket(config.getHost(), config.getPort());
        } catch (Exception e) {
            throw new XmlRpcException("Could not create client socket", e);
        }
    }
    
    @Override
	public synchronized Object sendRequest(XmlRpcRequest request) throws XmlRpcException {
       
        if (sock == null) {
            try {
                if (config.getUseSSL()) {
                    sslConnect();
                } else {
                    sock = new Socket(config.getHost(), config.getPort());
                }
            } catch (UnknownHostException e) {
               throw new XmlRpcException("Unknown host: " + config.getHost(), e);
            } catch (IOException e) {
                throw new XmlRpcException("Error while connecting to " + config.getHost() + ":" + config.getPort(), e);
            }
        }
        
        if (in == null) {
            try {
                if (config.isGzipRequesting()) {
                    in = new GZIPInputStream(sock.getInputStream());
                } else {
                    in = sock.getInputStream();
                }
            } catch (IOException e) {
                throw new XmlRpcException("Error while getting input stream", e);
            }
        }
        
        if (out == null) {
            try {
                if (config.isGzipCompressing()) {
                    out = new GZIPOutputStream(sock.getOutputStream());
                } else {
                    out = sock.getOutputStream();
                }
            } catch (IOException e) {
                throw new XmlRpcException("Error while getting output stream", e);
            }
        }
        
        /* write request */
        ContentHandler h = client.getXmlWriterFactory().getXmlWriter(config, out);
        XmlRpcWriter xw = new XmlRpcWriter(config, h, client.getTypeFactory());
        try {
            xw.write(request);
        } catch (SAXException e) {
            throw new XmlRpcException("Error while writing request", e);
        }
        
        /* read response */
        int responseOffset = 0;
        int readSize;
        ServerResponseScanner scanner = new ServerResponseScanner(responseBuf);
        
        while (!sock.isClosed()) {
            try {  
                readSize = in.read(responseBuf, responseOffset, responseBuf.length - responseOffset);
                if (readSize >= 0) {
                    responseOffset += readSize;
                    if (scanner.requestFinished(responseOffset)) {
                        responseOffset = 0;
                        break;
                    }
                } else {
                    throw new XmlRpcException("Received incomplete server response");
                }

            } catch (IndexOutOfBoundsException e) {
                throw new XmlRpcException("Received too long server response", e);
            } catch (SocketException e) {
                throw new XmlRpcException("Errror while reading server response", e);
            } catch (IOException e) {
                throw new XmlRpcException("Errror while reading server response", e);
            }
        }
        
        
        InputSource isource = new InputSource(new ByteArrayInputStream(responseBuf, 0, scanner.getRequestLength()));
        XMLReader xr = SAXParsers.newXMLReader();
        XmlRpcResponseParser xp;
        try {
            xp = new XmlRpcResponseParser(config, client.getTypeFactory());
            xr.setContentHandler(xp);
            xr.parse(isource);
        } catch (SAXException e) {
            throw new XmlRpcClientException("Failed to parse servers response: " + e.getMessage(), e);
        } catch (IOException e) {
            throw new XmlRpcClientException("Failed to read servers response: " + e.getMessage(), e);
        }
        
        if (!xp.isSuccess()) {
        	throw new XmlRpcException(xp.getErrorCode(), xp.getErrorMessage());
        }
        return xp.getResult();
    }

    @Override
	public XmlRpcTransport getTransport() {
        return this;
    }

    public void destroy() {
        try {
            sock.close();
        } catch (Exception e) {
            
        }
    }
}
