/**
 * 
 */
package com.magneta.xmlrpc.server;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.common.XmlRpcInvocationException;
import org.apache.xmlrpc.server.XmlRpcNoSuchHandlerException;
import org.apache.xmlrpc.server.XmlRpcStreamServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.xmlrpc.server.serializer.SocketXmlRpcTypeFactory;

/**
 *  XML-RPC stream server.
 *  
 * @author Nicos
 *
 */
public class XmlRpcSocketStreamServer extends XmlRpcStreamServer {

	private static final Logger log =  LoggerFactory.getLogger(XmlRpcSocketStreamServer.class); 
    
    private final SocketXmlRpcRequestListener requestListener;
    
    /**
     * Creates a new XML-RPC stream server.
     * 
     * Before the server you have to call setHandlerMapping.
     * 
     * @param requestListener
     */
    public XmlRpcSocketStreamServer(SocketXmlRpcRequestListener requestListener) {
    	this.requestListener = requestListener;    	
    	setTypeFactory(new SocketXmlRpcTypeFactory(this));
    }

    public void sendError(SocketXmlRpcRequestConfig requestConfig, OutputStream out, XmlRpcException e) {
        try {
            super.writeError(requestConfig, out, e);
        } catch (Throwable ex) { }
    }
    
    public void sendResponse(SocketXmlRpcRequestConfig requestConfig, OutputStream out, Object res) throws XmlRpcException {
    	super.writeResponse(requestConfig, out, res);
    }
    
    public XmlRpcRequest decodeRequest(SocketXmlRpcRequestConfig requestConfig, InputStream in) throws XmlRpcException {
    	return getRequest(requestConfig, in);
    }

    @Override
    public Object execute(XmlRpcRequest req) throws XmlRpcException {
    	if (requestListener != null) {
    		requestListener.beforeExecute(req);
    	}

    	Object res;

    	try {
    		res = super.execute(req);
    	} catch (XmlRpcException e) {
    		if (requestListener != null && req != null) {
            	requestListener.afterExecute(req, null, e);
            }
    		throw e;
    	}
    	
    	if (requestListener != null) {
			requestListener.afterExecute(req, res, null);
		}

    	return res;
    }

    public void execute(SocketXmlRpcRequestConfig requestConfig, InputStream in, OutputStream out) {
        
    	XmlRpcRequest req;
    	
        try {
            req = getRequest(requestConfig, in);
        } catch (XmlRpcException e) {
        	log.debug("Could not create request processor ", e);
        	try {
				super.writeError(requestConfig, out, e);
			} catch (XmlRpcException e1) { }
        	return;
        }
        
        Object res = null;
        try {
            if (requestListener != null) {
            	requestListener.beforeExecute(req);
            }
            
            res = super.execute(req);

            super.writeResponse(requestConfig, out, res);

            if (requestListener != null) {
            	requestListener.afterExecute(req, res, null);
            }
        } catch (Throwable e) {
        	if (e instanceof XmlRpcNoSuchHandlerException) {
        		try {
        			super.writeError(requestConfig, out, new XmlRpcException(501, "Method " + req.getMethodName() + " not implemented"));
        		} catch (XmlRpcException e1) { }
        	} else if (e instanceof XmlRpcException && !(e instanceof XmlRpcInvocationException)) {
        		try {
        			super.writeError(requestConfig, out, e);
        		} catch (XmlRpcException e1) { }
        	} else {
        		try {
					super.writeError(requestConfig, out, new XmlRpcException(500, "Internal Server Error"));
				} catch (XmlRpcException e1) { }
        	}
        	
        	if (requestListener != null && req != null) {
            	requestListener.afterExecute(req, res, e);
            }
         
        }
    }
}
