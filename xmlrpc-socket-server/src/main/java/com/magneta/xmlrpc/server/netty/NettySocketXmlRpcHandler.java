package com.magneta.xmlrpc.server.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AttributeKey;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.xmlrpc.server.SocketXmlRpcClientController;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;

public class NettySocketXmlRpcHandler extends ChannelInboundHandlerAdapter {

	private static final Logger log = LoggerFactory.getLogger(NettySocketXmlRpcHandler.class);

	private static final String REQUEST_CONFIG_KEY = "xmlrpc-socket:requestConfig";

	private static final AttributeKey<SocketXmlRpcRequestConfig> REQUEST_CONFIG =
			AttributeKey.valueOf(REQUEST_CONFIG_KEY);

	private final SocketXmlRpcClientController clientController;

	public NettySocketXmlRpcHandler(SocketXmlRpcClientController clientController) {
		super();
		this.clientController = clientController;
	}

	public static SocketXmlRpcRequestConfig getRequestConfig(ChannelHandlerContext ctx) {
		return ctx.channel().attr(REQUEST_CONFIG).get();
	}

	public static void setRequestConfig(ChannelHandlerContext ctx, SocketXmlRpcRequestConfig requestConfig) {
		ctx.channel().attr(REQUEST_CONFIG).set(requestConfig);
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		log.debug("userEventTriggered. Event: {}", evt);

		if (evt instanceof IdleStateEvent) {
			IdleStateEvent e = (IdleStateEvent) evt;

			log.debug("Channel idle. Will disconnect. Event: {}", e);
			ctx.channel().writeAndFlush(Unpooled.buffer(0)).addListener(ChannelFutureListener.CLOSE);
		}
		
		ctx.fireUserEventTriggered(evt);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {

		InetSocketAddress remoteAddress = (InetSocketAddress)ctx.channel().remoteAddress();

		log.debug("Connection from: " + remoteAddress.getAddress().getHostAddress() + ":" + remoteAddress.getPort());

		SocketXmlRpcRequestConfig requestConfig = clientController.createRequestConfig(
				(InetSocketAddress)ctx.channel().localAddress(),
				remoteAddress);

		setRequestConfig(ctx, requestConfig);

		if (clientController.getClientListener() != null) {
			clientController.getClientListener().onClientConnected(requestConfig);
		}

		super.channelActive(ctx);
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();

		InetSocketAddress address = (InetSocketAddress)channel.remoteAddress();

		if (address != null) {
			log.debug("Stopping client handler for {}:{}",
					address.getAddress().getHostAddress(), address.getPort());
		}

		SocketXmlRpcRequestConfig requestConfig = getRequestConfig(ctx);

		if (clientController.getClientListener() != null && requestConfig != null) {
			clientController.getClientListener().onClientDisconnected(requestConfig);
		}
		
		super.channelInactive(ctx);		
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable e)
			throws Exception {

		if (e instanceof IOException) {
			log.debug("I/O Exception: {}", e.getMessage());
		} else {
			log.error("Exception:", e);
		}

		ctx.channel().writeAndFlush(Unpooled.buffer(0)).addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * Invoked when a message object (e.g: {@link ChannelBuffer}) was received
	 * from a remote peer.
	 */
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object e) throws Exception {
		ByteBuf buffer = (ByteBuf)e;
		ByteBuf outputBuffer = Unpooled.buffer();

		SocketXmlRpcRequestConfig requestConfig = getRequestConfig(ctx);
		
		clientController.getXmlRpcServer().execute(requestConfig, new ByteBufInputStream(buffer), new ByteBufOutputStream(outputBuffer));

		ChannelFuture future = ctx.channel().writeAndFlush(outputBuffer);

		if (requestConfig.shouldDisconnect()) {
			future.addListener(ChannelFutureListener.CLOSE);
		}

		if (requestConfig.serverShouldShutdown()) {
			clientController.getClientListener().onShutdownRequested(requestConfig);
		}

		ctx.fireChannelRead(e);
	}
	
	@Override
	public boolean isSharable() {
		return true;
	}
}
