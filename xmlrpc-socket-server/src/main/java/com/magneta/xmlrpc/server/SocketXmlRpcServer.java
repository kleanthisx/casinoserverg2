package com.magneta.xmlrpc.server;

import javax.net.ssl.SSLContext;

/**
 * XML-RPC server.
 * 
 * @author anarxia
 *
 */
public interface SocketXmlRpcServer extends Runnable {
	
	/**
	 * Sets the request config factory for the server
	 * @param requestConfigFactory
	 * 
	 * Note: This should be called before the server is started.
	 */
	public void setSocketXmlRpcRequestConfigFactory(SocketXmlRpcRequestConfigFactory requestConfigFactory);
	
	/**
	 * Sets the idle timeout for the server.
	 * @param timeout Timeout in seconds.
	 * 
	 * Note: This should be called before the server is started.
	 */
	public void setIdleTimeout(int timeout);
	
	/**
	 * Sets the max request size for the server
	 * @param size Size in bytets
	 * 
	 * Note: This should be called before the server is started.
	 */
	public void setMaxRequestSize(int size);
	
	/**
	 * Sets SSL context for the server
	 * @param sslContext The SSL context to use
	 * 
	 * Note: This should be called before the server is started.
	 */
	public void setSslContext(SSLContext sslContext);
}
