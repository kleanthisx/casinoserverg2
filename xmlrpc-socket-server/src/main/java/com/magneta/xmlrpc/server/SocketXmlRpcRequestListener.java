package com.magneta.xmlrpc.server;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;

public interface SocketXmlRpcRequestListener {

	public void beforeExecute(XmlRpcRequest request) throws XmlRpcException;
	public void afterExecute(XmlRpcRequest request, Object result, Throwable ex);
}
