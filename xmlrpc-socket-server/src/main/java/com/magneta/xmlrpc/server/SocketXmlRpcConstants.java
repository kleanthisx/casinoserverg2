package com.magneta.xmlrpc.server;

import java.nio.charset.Charset;

public class SocketXmlRpcConstants {

	public static final byte[] END_OF_REQUEST = "</methodCall>".getBytes(Charset.forName("UTF-8"));

	/**
	 * Default max request size: 100Kbytes
	 */
    public static final int MAX_REQUEST = 102400;

    /**
     * Default server timeout: 30s
     */
    public static final int SERVER_TIMEOUT = 30;
}
