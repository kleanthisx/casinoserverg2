package com.magneta.xmlrpc.server.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.xmlrpc.server.SocketXmlRpcClientController;
import com.magneta.xmlrpc.server.SocketXmlRpcClientListener;
import com.magneta.xmlrpc.server.SocketXmlRpcConstants;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfigFactory;
import com.magneta.xmlrpc.server.SocketXmlRpcServer;
import com.magneta.xmlrpc.server.XmlRpcSocketStreamServer;
import com.magneta.xmlrpc.server.impl.SocketXmlRpcRequestConfigFactoryImpl;

public class NettySocketXmlRpcServer implements SocketXmlRpcClientController, SocketXmlRpcServer {

	 private static final Logger log =  LoggerFactory.getLogger(NettySocketXmlRpcServer.class); 

	private ServerBootstrap bootstrap;
	private Channel channel;

	private final SocketXmlRpcClientListener listener;
	private final XmlRpcSocketStreamServer xmlrpcServer;
	private final SocketAddress localAddress;

	private SocketXmlRpcRequestConfigFactory requestConfigFactory;
	private SSLContext sslContext;
	private int idleTimeout;
	private int maxRequestSize;
	private EventExecutorGroup handlerGroup;

	public NettySocketXmlRpcServer(XmlRpcSocketStreamServer xmlrpcServer, SocketXmlRpcClientListener listener, SocketAddress localAddress) {
		this.xmlrpcServer = xmlrpcServer;
		this.listener = listener;
		this.localAddress = localAddress;

		this.idleTimeout = SocketXmlRpcConstants.SERVER_TIMEOUT;
		this.maxRequestSize = SocketXmlRpcConstants.MAX_REQUEST;
		this.requestConfigFactory = new SocketXmlRpcRequestConfigFactoryImpl();
	}

	@Override
	public void setSocketXmlRpcRequestConfigFactory(SocketXmlRpcRequestConfigFactory requestConfigFactory) {
		this.requestConfigFactory = requestConfigFactory;
	}

	@Override
	public boolean isStopped() {
		if (channel == null) {
			return true;
		}
		
		return channel.closeFuture().isDone();
	}

	@Override
	public SocketXmlRpcClientListener getClientListener() {
		return this.listener;
	}

	@Override
	public SocketXmlRpcRequestConfig createRequestConfig(
			InetSocketAddress localAddress, InetSocketAddress remoteAddress) {
		return requestConfigFactory.createRequestConfig(localAddress, remoteAddress);
	}

	@Override
	public XmlRpcSocketStreamServer getXmlRpcServer() {
		return this.xmlrpcServer;
	}

	public void start() {		
		EventLoopGroup bossGroup = new NioEventLoopGroup();
	    EventLoopGroup workerGroup = new NioEventLoopGroup();
	    handlerGroup = new DefaultEventExecutorGroup(Runtime.getRuntime().availableProcessors() * 2);

		bootstrap = new ServerBootstrap();
		bootstrap.group(bossGroup, workerGroup);
		bootstrap.channel(NioServerSocketChannel.class);	

		final NettySocketXmlRpcHandler handler = new NettySocketXmlRpcHandler(this);

		bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
            	ChannelPipeline pipeline = ch.pipeline();
            	
            	pipeline.addLast("idle", new IdleStateHandler(getIdleTimeout(), 0, 0));

            	if (sslContext != null) {
            		SSLEngine sslEngine = sslContext.createSSLEngine();
        			sslEngine.setUseClientMode(false);
            		
            		pipeline.addLast("ssl", new SslHandler(sslEngine));
            	}

            	pipeline.addLast("frame", new DelimiterBasedFrameDecoder(getMaxRequestSize(), false, true, Unpooled.copiedBuffer(SocketXmlRpcConstants.END_OF_REQUEST)));            	
            	pipeline.addLast(handlerGroup, "handler", handler);
            }
        });

		try {
			channel = bootstrap.bind(localAddress).sync().channel();
		} catch (InterruptedException e) {
			Thread.interrupted();
		}
		
		log.info("TCP server: started. Address {}", localAddress);
	}
	
	@Override
	public void run() {
		start();
		
		ChannelFuture closeFuture = channel.closeFuture();
		
		try {
			closeFuture.sync();
		} catch (InterruptedException e) {
			Thread.interrupted();
		}
		
		destroy();
		log.info("TCP server: stopped");
	}

	public void stop() {
		if (channel == null) {
			return;
		}
		
		channel.close();
		
		if (bootstrap != null) {
			bootstrap.childGroup().shutdownGracefully();
			bootstrap.group().shutdownGracefully();
		}
		
		if (handlerGroup != null) {
			handlerGroup.shutdownGracefully();
		}
	}

	public void destroy() {
		if (channel != null) {
			try {
				channel.closeFuture().sync();
			} catch (InterruptedException e) {
				Thread.interrupted();
			}
			channel = null;
		}

		if (bootstrap != null) {			
			try {
				bootstrap.childGroup().terminationFuture().sync();
			} catch (InterruptedException e) {
				Thread.interrupted();
			}
			try {
				bootstrap.group().terminationFuture().sync();
			} catch (InterruptedException e) {
				Thread.interrupted();
			}

			bootstrap = null;
		}
		
		if (handlerGroup != null) {
			try {
				handlerGroup.terminationFuture().sync();
			} catch (InterruptedException e) {
				Thread.interrupted();
			}
			
			handlerGroup = null;
		}
	}
	
	public ChannelFuture getCloseFuture() {
		return channel.closeFuture();
	}

	@Override
	public void setIdleTimeout(int timeout) {
		this.idleTimeout = timeout;
	}

	@Override
	public int getIdleTimeout() {
		return this.idleTimeout;
	}

	@Override
	public int getMaxRequestSize() {
		return this.maxRequestSize;
	}

	@Override
	public void setMaxRequestSize(int size) {
		this.maxRequestSize = size;
	}

	@Override
	public void setSslContext(SSLContext sslContext) {
		this.sslContext = sslContext;
	}
}
