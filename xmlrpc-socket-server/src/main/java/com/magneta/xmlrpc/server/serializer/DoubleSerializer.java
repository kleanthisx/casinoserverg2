package com.magneta.xmlrpc.server.serializer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.xmlrpc.serializer.TypeSerializerImpl;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Custom serializer for doubles. This implementation is 100% spec compliant 
 * unlike the default included with xmlrpc-common.
 * The default serializer violates the XML-RPC spec as follows:
 * <ol>
 * <li>It's locale dependent.</li>
 * <li>Sometimes uses exponent notation</li>
 * </ol>
 * 
 * @author anarxia
 *
 */
public class DoubleSerializer extends TypeSerializerImpl {

	public static final String DOUBLE_TAG = "double";
	
	@Override
	public void write(ContentHandler pHandler, Object pObject)
			throws SAXException {
		
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(Locale.US);
		
		NumberFormat nf = new DecimalFormat("#.#############################", symbols);
		
		write(pHandler, DOUBLE_TAG, nf.format(pObject));
	}
}
