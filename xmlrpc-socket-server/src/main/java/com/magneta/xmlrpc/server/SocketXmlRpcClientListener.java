/**
 * 
 */
package com.magneta.xmlrpc.server;


/**
 * @author Nicos
 *
 */
public interface SocketXmlRpcClientListener {

    void onClientConnected(SocketXmlRpcRequestConfig requestConfig);
    void onClientDisconnected(SocketXmlRpcRequestConfig requestConfig);
	void onShutdownRequested(SocketXmlRpcRequestConfig requestConfig);
}
