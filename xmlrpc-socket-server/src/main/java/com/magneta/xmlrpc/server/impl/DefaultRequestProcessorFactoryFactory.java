package com.magneta.xmlrpc.server.impl;

import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory.RequestSpecificProcessorFactoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;

/**
 * Default RequestProcessorFactoryFactory 
 * Handlers are POJOs with a default constructor
 * or one which takes a single SocketXmlRpcRequestConfig parameter.
*/ 
public class DefaultRequestProcessorFactoryFactory extends RequestSpecificProcessorFactoryFactory {

	private static final Logger log =  LoggerFactory.getLogger(DefaultRequestProcessorFactoryFactory.class); 

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Object getRequestProcessor(java.lang.Class pClass, XmlRpcRequest request) {		
		/* Try public constructor with SocketXmlRpcRequestConfig argument */
		try {
			return pClass.getConstructor(SocketXmlRpcRequestConfig.class).newInstance(request.getConfig());
		} catch (NoSuchMethodException e) {
			/* Do nothing */
		} catch (SecurityException e) {
			/* Do nothing */
		} catch (Throwable e) {
			log.error("Unable to create request processor", e);
			return null;
		}
		
		/* Try default constructor */
		try {
			return pClass.getConstructor().newInstance();
		} catch (Throwable e) {
			log.error("Unable to create request processor", e);
			return null;
		}

	}
}
