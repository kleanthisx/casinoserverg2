package com.magneta.xmlrpc.server;

import org.apache.xmlrpc.common.TypeConverter;
import org.apache.xmlrpc.common.TypeConverterFactoryImpl;

import com.magneta.xmlrpc.server.serializer.LongSerializer;

/**
 * Socket XML-RPC type converter factory.
 * 
 * There is nothing specific about socket XML-RPC but it offers
 * the following:
 * 
 * 1. Conversion of base64 to longs.
 * 
 * @author anarxia
 *
 */
public class SocketXmlRpcTypeConverterFactory extends TypeConverterFactoryImpl {

	private final TypeConverter longTypeConverter;
	
	public SocketXmlRpcTypeConverterFactory() {
		super();
		this.longTypeConverter = new LongTypeConverter();
	}
	
	private static class LongTypeConverter implements TypeConverter {

		@Override
        public boolean isConvertable(Object pObject) {
        	if (pObject instanceof byte[]) {
        		byte[] base64 = (byte[])pObject;
        		
        		return base64.length == 8;
        	}
        	
            return pObject == null || pObject instanceof Long;
        }
        
        @Override
        public Object convert(Object pObject) {
        	if (pObject instanceof byte[]) {
        		return LongSerializer.getLong((byte[])pObject);
        	}
            return pObject;
        }

        @Override
        public Object backConvert(Object pObject) {
            return pObject;
        }
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public TypeConverter getTypeConverter(@SuppressWarnings("rawtypes") Class pClass) {
		if (pClass.isAssignableFrom(Long.TYPE) && !pClass.isAssignableFrom(Integer.TYPE)) {
            return longTypeConverter;
        }
		
		return super.getTypeConverter(pClass);
	}
}
