package com.magneta.xmlrpc.server.impl;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.common.TypeConverterFactory;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;
import org.apache.xmlrpc.server.XmlRpcHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;

/** 
 * XmlRpcHandlerMapping factory.
 * 
 */
public class HandlerMappingFactory {
	
    public static XmlRpcHandlerMapping createXmlRpcHandlerMapping(String handlersFile, XmlRpcServer server) throws XmlRpcException {
        return createXmlRpcHandlerMapping(handlersFile, server.getTypeConverterFactory(), new DefaultRequestProcessorFactoryFactory());
    }
	
    public static XmlRpcHandlerMapping createXmlRpcHandlerMapping(String handlersFile, XmlRpcServer server, RequestProcessorFactoryFactory requestProcessorFactoryFactory) throws XmlRpcException {
       return createXmlRpcHandlerMapping(handlersFile, server.getTypeConverterFactory(), requestProcessorFactoryFactory);
    }

    public static XmlRpcHandlerMapping createXmlRpcHandlerMapping(String handlersFile, TypeConverterFactory typeConverterFactory, RequestProcessorFactoryFactory requestProcessorFactoryFactory) throws XmlRpcException {
        URL url = Thread.currentThread().getContextClassLoader().getResource(handlersFile);
        if (url == null) {
            throw new XmlRpcException("Failed to locate resource " + handlersFile);
        }
        try {
            PropertyHandlerMapping mapping = new PropertyHandlerMapping();
            mapping.setTypeConverterFactory(typeConverterFactory);
            mapping.setRequestProcessorFactoryFactory(requestProcessorFactoryFactory);
            mapping.load(Thread.currentThread().getContextClassLoader(), url);
            return mapping;
        } catch (IOException e) {
            throw new XmlRpcException("Failed to load resource " + url + ": " + e.getMessage(), e);
        }
    }
    
    public static XmlRpcHandlerMapping createXmlRpcHandlerMapping(Map<String, Class<?>> handlers, XmlRpcServer server) throws XmlRpcException {
    	return createXmlRpcHandlerMapping(handlers, server.getTypeConverterFactory(), null);
    }

    public static XmlRpcHandlerMapping createXmlRpcHandlerMapping(Map<String, Class<?>> handlers, XmlRpcServer server, RequestProcessorFactoryFactory requestProcessorFactoryFactory) throws XmlRpcException {
    	return createXmlRpcHandlerMapping(handlers, server.getTypeConverterFactory(), requestProcessorFactoryFactory);
    }

    public static XmlRpcHandlerMapping createXmlRpcHandlerMapping(Map<String, Class<?>> handlers, TypeConverterFactory typeConverterFactory, RequestProcessorFactoryFactory requestProcessorFactoryFactory) throws XmlRpcException {

    	if (requestProcessorFactoryFactory == null) {
    		requestProcessorFactoryFactory = new DefaultRequestProcessorFactoryFactory();
    	}

    	PropertyHandlerMapping mapping = new PropertyHandlerMapping();
    	mapping.setTypeConverterFactory(typeConverterFactory);
    	mapping.setRequestProcessorFactoryFactory(requestProcessorFactoryFactory);

    	for (Map.Entry<String, Class<?>> h: handlers.entrySet()) {
    		mapping.addHandler(h.getKey(), h.getValue());
    	}
    	return mapping;
    }
}
