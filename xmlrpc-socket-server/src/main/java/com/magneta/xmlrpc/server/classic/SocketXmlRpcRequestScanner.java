package com.magneta.xmlrpc.server.classic;

import com.magneta.xmlrpc.server.SocketXmlRpcConstants;

/**
 * Detects when all data for a methodCall have been
 * received.
 * @author Nicos
 *
 */
public class SocketXmlRpcRequestScanner {
	
    private int lastRequestScan;
    private byte[] requestBuf;
    private int requestLen;
    
    public SocketXmlRpcRequestScanner(byte[] requestBuf) {
        lastRequestScan = 0;
        this.requestBuf = requestBuf;
        this.requestLen = 0;
    }
    
    public int getRequestLength() {
        return requestLen;
    }
    
    public boolean requestFinished(int requestSize) {

        int requestScanOffset = 0;
        requestLen = 0;
        
        if (requestSize >= SocketXmlRpcConstants.END_OF_REQUEST.length) {

            for (int i = lastRequestScan; i < requestSize; i++) {
                if (SocketXmlRpcConstants.END_OF_REQUEST[requestScanOffset] == requestBuf[i]) {
                    requestScanOffset++;
                    if (requestScanOffset == SocketXmlRpcConstants.END_OF_REQUEST.length) {
                        lastRequestScan = 0;
                        requestLen = i + 1;
                        return true;
                    }

                } else {
                    requestScanOffset = 0;
                }
            }
        }

        return false;
    }

}