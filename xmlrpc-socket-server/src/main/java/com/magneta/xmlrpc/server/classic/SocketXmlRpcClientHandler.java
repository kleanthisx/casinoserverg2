package com.magneta.xmlrpc.server.classic;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.xmlrpc.server.SocketXmlRpcClientController;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;

/**
 * Client handler class.
 * There is one instance per connected user.
 * 
 * @author anarxia
 *
 */
public class SocketXmlRpcClientHandler implements Runnable {
    
    private static final Logger log =  LoggerFactory.getLogger(SocketXmlRpcClientHandler.class); 
    
    private final Socket socket;
    private final SocketXmlRpcClientController clientController;
    
    public SocketXmlRpcClientHandler(Socket socket, SocketXmlRpcClientController clientController) {
        this.socket = socket;
        this.clientController = clientController;
    }
    
    @Override
	public void run() {
        
        log.debug("Started client handler for {}:{}", socket.getInetAddress().getHostAddress(), socket.getPort());
        boolean finished = false;

        InputStream in = null;
        OutputStream out = null;
        try {
        	int timeout = clientController.getIdleTimeout();
        	if (timeout > 0) {
        		socket.setSoTimeout(clientController.getIdleTimeout() * 1000);
        	}

            in = socket.getInputStream();
            out = socket.getOutputStream();         

        } catch (IOException e) {
            log.warn("Error while getting streams for client socket", e);
            finished = true;
        }
        
        SocketXmlRpcRequestConfig requestConfig = null;
        
        byte[] requestBuf = new byte[clientController.getMaxRequestSize()];
        int requestOffset = 0;
        int readSize;
        SocketXmlRpcRequestScanner scanner = new SocketXmlRpcRequestScanner(requestBuf);
        boolean stopServer = false;

        while(!clientController.isStopped() && !finished  && !socket.isClosed()) {

        	if (in == null) {
        		finished = true;
        		break;
        	}
        	
            try {  
                readSize = in.read(requestBuf, requestOffset, requestBuf.length - requestOffset);
                if (readSize < 0) {
                	break;
                }
                requestOffset += readSize;

                if (requestConfig == null) {

                	requestConfig = clientController.createRequestConfig((InetSocketAddress)socket.getLocalSocketAddress(), (InetSocketAddress)socket.getRemoteSocketAddress());

                	if (clientController.getClientListener() != null) {
                		clientController.getClientListener().onClientConnected(requestConfig);
                	}
                }
                
                if (requestConfig.serverShouldShutdown()) {
                	stopServer = true;
                	break;
                }

                if (requestConfig.shouldDisconnect()) {
                	break;
                }

                if (scanner.requestFinished(requestOffset)) {
                	requestOffset = 0;

                	clientController.getXmlRpcServer().execute(requestConfig, new ByteArrayInputStream(requestBuf, 0, scanner.getRequestLength()), out);
                	
                	if (requestConfig.serverShouldShutdown()) {
                    	stopServer = true;
                    	break;
                    }

                	if (requestConfig.shouldDisconnect()) {
                		break;
                	}                            
                }

            } catch (IndexOutOfBoundsException e) {
                log.warn("Overlong client request from " + socket.getInetAddress(), e);
                clientController.getXmlRpcServer().sendError(requestConfig, out, new XmlRpcException(413, "Request Entity Too Large"));
                break;
            } catch (SocketTimeoutException e) {
            	log.info("{}. Client address: {}:{}", new Object[] {e.getMessage(), socket.getInetAddress(), socket.getPort()});
            	break;
            } catch (SocketException e) {
                log.info("Error while reading socket: {}", e.getMessage());
                break;
            } catch (IOException e) {
                log.debug("Exception", e);
                requestOffset = 0;
                break;
            }

        }

        try {
            if (in != null)
                in.close();

            if (out != null)
                out.close();
        } catch (IOException e) {
            log.debug("Exception", e);
        }
        
        log.debug("Stopping client handler for {}:{}", socket.getInetAddress().getHostAddress(), socket.getPort());
        try {
            socket.close();
        } catch (IOException e) {
            log.debug("Error while closing socket", e);
        }
        
        if (requestConfig != null && clientController.getClientListener() != null) {
        	clientController.getClientListener().onClientDisconnected(requestConfig);
        	
        	if (stopServer) {
        		log.info("Server shutdown requested");
            	clientController.getClientListener().onShutdownRequested(requestConfig);
            }
        }
    }
}