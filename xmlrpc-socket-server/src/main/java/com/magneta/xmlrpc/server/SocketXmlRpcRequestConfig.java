/**
 * 
 */
package com.magneta.xmlrpc.server;

import org.apache.xmlrpc.common.XmlRpcStreamRequestConfig;

/**
 * @author Nicos
 *
 */
public interface SocketXmlRpcRequestConfig extends XmlRpcStreamRequestConfig {

    /**
     * Notifies the server that the client should be
     * disconnected after the request is processed.
     */
    public void setDisconnect();
    
    public boolean shouldDisconnect();
    
    public void setServerShutdown();
    
    public boolean serverShouldShutdown();
}
