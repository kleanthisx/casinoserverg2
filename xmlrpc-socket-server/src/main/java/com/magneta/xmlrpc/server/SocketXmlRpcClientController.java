package com.magneta.xmlrpc.server;

import java.net.InetSocketAddress;

public interface SocketXmlRpcClientController {

	boolean isStopped();
	SocketXmlRpcClientListener getClientListener();
	SocketXmlRpcRequestConfig createRequestConfig(InetSocketAddress localAddress, InetSocketAddress remoteAddress);
	XmlRpcSocketStreamServer getXmlRpcServer();
	
	/**
	 * Idle timeout in seconds
	 * @return
	 */
	int getIdleTimeout();
	int getMaxRequestSize();
}
