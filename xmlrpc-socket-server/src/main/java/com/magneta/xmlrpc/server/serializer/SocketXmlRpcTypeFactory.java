package com.magneta.xmlrpc.server.serializer;

import org.apache.xmlrpc.common.TypeFactoryImpl;
import org.apache.xmlrpc.common.XmlRpcController;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.serializer.TypeSerializer;
import org.xml.sax.SAXException;

/**
 * Socket XML-RPC type factory.
 * 
 * There is nothing specific about socket XML-RPC but it offers
 * the following:
 * 
 * 1. Spec-compliant double serialization.
 * 2. Serialization of longs as base64 if extensions are not enabled.
 * 
 * @author anarxia
 *
 */
public class SocketXmlRpcTypeFactory extends TypeFactoryImpl {

	private static final TypeSerializer DOUBLE_SERIALIZER = new DoubleSerializer();
	private static final TypeSerializer LONG_SERIALIZER = new LongSerializer();
	
	
	public SocketXmlRpcTypeFactory(XmlRpcController pController) {
		super(pController);
	}

	@Override
	public TypeSerializer getSerializer(XmlRpcStreamConfig pConfig, Object pObject) throws SAXException {
		if (pObject == null) {
			return super.getSerializer(pConfig, pObject);
		}
		
		if (pObject instanceof Double) {
			return DOUBLE_SERIALIZER;
		}
		
		if (!pConfig.isEnabledForExtensions() && pObject instanceof Long) {
			return LONG_SERIALIZER;
		}
		
		return super.getSerializer(pConfig, pObject);
	}
}
