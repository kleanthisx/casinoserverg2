package com.magneta.xmlrpc.server;

import java.net.InetSocketAddress;

public interface SocketXmlRpcRequestConfigFactory {

	public SocketXmlRpcRequestConfig createRequestConfig(InetSocketAddress localAddress, InetSocketAddress remoteAddress);
}
