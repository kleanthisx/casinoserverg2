package com.magneta.xmlrpc.server.serializer;

import org.apache.xmlrpc.serializer.ByteArraySerializer;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Custom serializer for longs. It serializes longs as base64 big-endian.
 * 
 * @author anarxia
 *
 */
public class LongSerializer extends ByteArraySerializer {

    /**
     * Gets an array of 8 bytes which represents a long number (64-bit integer).
     * @param longNumber The long number to convert.
     * @return An array of 8 bytes which represents a long number.
     */
    private static final byte[] getBytes(long longNumber) {
        byte[] num = new byte[8];

        num[0] = (byte)((longNumber >>> 56) & 0x000000FF);
        num[1] = (byte)((longNumber >>> 48) & 0x000000FF);
        num[2] = (byte)((longNumber >>> 40) & 0x000000FF);
        num[3] = (byte)((longNumber >>> 32) & 0x000000FF);
        num[4] = (byte)((longNumber >>> 24) & 0x000000FF);
        num[5] = (byte)((longNumber >>> 16) & 0x000000FF);
        num[6] = (byte)((longNumber >>> 8) & 0x000000FF);
        num[7] = (byte)((longNumber) & 0x000000FF);
        
        return num;
        
    }
    
    /**
     * Gets a long number (64-bit integer) from an array of 8 bytes.
     * @param bytesNumber An array of 8 bytes representing a long.
     * @return The long number created.
     */
    public static final long getLong(byte[] bytesNumber) {
       long num;
       
       num = 	(  ((long)bytesNumber[0]) << 56) | 
       			( (((long)bytesNumber[1]) << 48) & 0x00FF000000000000L) |
       			( (((long)bytesNumber[2]) << 40) & 0x0000FF0000000000L) |
       			( (((long)bytesNumber[3]) << 32) & 0x000000FF00000000L) | 
       			( (((long)bytesNumber[4]) << 24) & 0x00000000FF000000L) |
       			( (((long)bytesNumber[5]) << 16) & 0x0000000000FF0000L) |
       			( (((long)bytesNumber[6]) <<  8) & 0x000000000000FF00L) |
       			( bytesNumber[7] & 0x00000000000000FFL); 
        
        return num;
        
    }
	
    @Override
	public void write(final ContentHandler pHandler, Object pObject) throws SAXException {
		if (pObject instanceof Long) {
			super.write(pHandler, getBytes((Long)pObject));
		} else {
			super.write(pHandler, pObject);
		}
	}
}
