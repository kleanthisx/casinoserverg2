package com.magneta.xmlrpc.server.ssl;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

public class SSLContextFactory {

	private static final String DEFAULT_SSL_PROTOCOL = "SSLv3";
	private static final String DEFAULT_KEYMANAGER_ALGO = "SunX509";

	public static SSLContext create(ClassLoader classLoader, String keyFile, String keyPass) throws IOException, GeneralSecurityException {
		return create(classLoader.getResourceAsStream(keyFile), keyPass);
	}
	
	public static SSLContext create(InputStream keyFileStream, String keyPass) throws IOException, GeneralSecurityException {
		KeyStore ksKeys = null;

		ksKeys = KeyStore.getInstance(KeyStore.getDefaultType());

		if (keyPass != null) {
			ksKeys.load(keyFileStream, keyPass.toCharArray());
		} else {
			ksKeys.load(keyFileStream, null);
		}

		/* KeyManager's decide which key material to use. */
		KeyManagerFactory kmf =
			KeyManagerFactory.getInstance(DEFAULT_KEYMANAGER_ALGO);

		if (keyPass != null) {
			kmf.init(ksKeys, keyPass.toCharArray());
		} else {
			kmf.init(ksKeys, null);
		}

		SSLContext sslContext = SSLContext.getInstance(DEFAULT_SSL_PROTOCOL);
		sslContext.init(kmf.getKeyManagers(), null, null);

		return sslContext;
	}
}
