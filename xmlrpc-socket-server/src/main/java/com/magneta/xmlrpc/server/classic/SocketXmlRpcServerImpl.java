/**
 * 
 */
package com.magneta.xmlrpc.server.classic;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLServerSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.xmlrpc.server.SocketXmlRpcClientController;
import com.magneta.xmlrpc.server.SocketXmlRpcClientListener;
import com.magneta.xmlrpc.server.SocketXmlRpcConstants;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfigFactory;
import com.magneta.xmlrpc.server.SocketXmlRpcServer;
import com.magneta.xmlrpc.server.XmlRpcSocketStreamServer;
import com.magneta.xmlrpc.server.impl.SocketXmlRpcRequestConfigFactoryImpl;

/**
 * Streaming XML-RPC server over a socket.
 * 
 * It uses plain I/O (not new I/O) and uses a thread per connected
 * client.
 * 
 * @author Nicos
 */
public class SocketXmlRpcServerImpl implements Runnable, SocketXmlRpcClientController, SocketXmlRpcServer {

    private static final Logger log =  LoggerFactory.getLogger(SocketXmlRpcServerImpl.class); 
    
    private final XmlRpcSocketStreamServer xmlrpcServer;
    private final SocketXmlRpcClientListener clientListener;
    private final SocketAddress localAddress;
    
    private volatile boolean stopped;

    private SocketXmlRpcRequestConfigFactory requestConfigFactory;
    private int idleTimeout;
    private int maxRequestSize;  
    private SSLContext sslContext;
    
    private ServerSocket serverSocket;
    
    /**
     * Constructor for the server.
     * @param listener The listener for client events.
     * @param port The port to listen to.
     */
    public SocketXmlRpcServerImpl(XmlRpcSocketStreamServer xmlrpcServer, SocketXmlRpcClientListener listener, SocketAddress localAddress) {
    	this.xmlrpcServer = xmlrpcServer;
    	this.clientListener = listener;
    	this.requestConfigFactory = new SocketXmlRpcRequestConfigFactoryImpl();
    	this.stopped = false;
    	
    	this.idleTimeout = SocketXmlRpcConstants.SERVER_TIMEOUT;
    	this.maxRequestSize = SocketXmlRpcConstants.MAX_REQUEST;
    	this.localAddress = localAddress;
    }
    
    @Override
	public void setSocketXmlRpcRequestConfigFactory(SocketXmlRpcRequestConfigFactory requestConfigFactory) {
    	this.requestConfigFactory = requestConfigFactory;
    }
    
    @Override
	public void run() {
    	try {
    		if (this.sslContext != null) {
    			SSLServerSocketFactory sslserversocketfactory = sslContext.getServerSocketFactory();
    			serverSocket = sslserversocketfactory.createServerSocket();
    		} else {
    			serverSocket = new ServerSocket();
    		}
    	} catch (IOException e) {
    		log.error("Unable to create socket for the server. Aborting");
    		return;
    	}

    	try {
			serverSocket.bind(localAddress);
		} catch (IOException e1) {
			log.error("Unable to bind server to address. Aborting");
			serverSocket = null;
			return;
		}
    	
        log.info("Socket server started at address {}", localAddress);
        ThreadGroup workerThreads = 
            new ClientThreadGroup(Thread.currentThread().getThreadGroup(), this.getClass().getSimpleName());
        
        while (!stopped) {
            try {
                Socket clientSocket = serverSocket.accept();
                log.debug("Socket server connection from: " + clientSocket.getInetAddress().getHostAddress() + ":" + clientSocket.getPort());
                Thread t = new Thread(workerThreads, new SocketXmlRpcClientHandler(clientSocket, this));
                t.start();
            } catch (SSLException e) {
            	if (!stopped) {
            		log.error("SSL error while accepting client", e);
            	}
                break;
            } catch (Throwable e) {
            	if (!stopped) {
            		log.info("Error while accepting client", e);
            	}
            }
        }
        
        try {
            serverSocket.close();
        } catch (IOException e) {
        }       
       
        workerThreads.interrupt();
        
        serverSocket = null;
        log.info("Socket server stopped");
    }

    public void stop() {
        stopped = true;
        
        if (serverSocket != null) {
        	if (!serverSocket.isClosed() && serverSocket.isBound()) {
        		try {
					serverSocket.close();
				} catch (IOException e) {
					log.error("I/O Error while closing socket", e);
				}
        	}
        }
        
        
    }
    
    @Override
	public boolean isStopped() {
    	return stopped;
    }
    
    @Override
	public SocketXmlRpcClientListener getClientListener() {
    	return this.clientListener;
    }
    
    @Override
	public SocketXmlRpcRequestConfig createRequestConfig(InetSocketAddress localAddress, InetSocketAddress remoteAddress) {
    	return requestConfigFactory.createRequestConfig(localAddress, remoteAddress);
    }
    
    @Override
	public XmlRpcSocketStreamServer getXmlRpcServer() {
    	return this.xmlrpcServer;
    }
    
    private static class ClientThreadGroup extends ThreadGroup {

        public ClientThreadGroup(ThreadGroup parent, String name) {
            super(parent, name);
        }
     
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            log.error("Uncaught exception in client handler", e);
        }
    }

	@Override
	public void setIdleTimeout(int timeout) {
		this.idleTimeout = timeout;
	}

	@Override
	public int getIdleTimeout() {
		return this.idleTimeout;
	}

	@Override
	public int getMaxRequestSize() {
		return this.maxRequestSize;
	}

	@Override
	public void setMaxRequestSize(int size) {
		this.maxRequestSize = size;
	}

	@Override
	public void setSslContext(SSLContext sslContext) {
		this.sslContext = sslContext;
	}
}
