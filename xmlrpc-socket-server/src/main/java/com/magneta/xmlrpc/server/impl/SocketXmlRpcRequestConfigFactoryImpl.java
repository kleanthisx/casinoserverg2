package com.magneta.xmlrpc.server.impl;

import java.net.InetSocketAddress;

import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;
import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfigFactory;

public class SocketXmlRpcRequestConfigFactoryImpl implements SocketXmlRpcRequestConfigFactory {

	@Override
	public SocketXmlRpcRequestConfig createRequestConfig(
			InetSocketAddress localAddress, InetSocketAddress remoteAddress) {
		return new SocketXmlRpcRequestConfigImpl();
	}
}
