package com.magneta.xmlrpc.server.impl;

import java.util.TimeZone;

import org.apache.xmlrpc.common.XmlRpcStreamConfig;

import com.magneta.xmlrpc.server.SocketXmlRpcRequestConfig;

public class SocketXmlRpcRequestConfigImpl implements SocketXmlRpcRequestConfig {

	private boolean disconnect;
	private boolean serverShutdown;
    
    public SocketXmlRpcRequestConfigImpl() {
        disconnect = false;
        serverShutdown = false;
    }
    
    @Override
	public boolean isGzipCompressing() {
        return false;
    }
    
    @Override
	public boolean isGzipRequesting() {
        return false;
    }

    @Override
	public String getEncoding() {
        return XmlRpcStreamConfig.UTF8_ENCODING;
    }

    @Override
	public TimeZone getTimeZone() {
        return TimeZone.getTimeZone("UTC");
    }

    @Override
	public boolean isEnabledForExtensions() {
        return false;
    }

    @Override
	public boolean isEnabledForExceptions() {
        return false;
    }
    
    /**
     * Notifies the server that the client should be
     * disconnected after the request is processed.
     */
    @Override
	public void setDisconnect() {
    	this.disconnect = true;
    }
    
    @Override
	public boolean shouldDisconnect() {
    	return this.disconnect;
    }

	@Override
	public void setServerShutdown() {
		this.serverShutdown = true;
	}

	@Override
	public boolean serverShouldShutdown() {
		return this.serverShutdown;
	}
}
