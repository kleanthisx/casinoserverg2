package com.magneta.casino.common.games.filters.slots;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("TuttiFruity")
public class TuttiFruityActionFilter extends SlotGameActionFilter {
    
    @Override
    public List<GameActionBean> filterActions(List<GameActionBean> actions) {
        actions = super.filterActions(actions);
        
        for (int i=0;i<actions.size();i++){
            GameActionBean action = actions.get(i);
            String actionDesc = action.getAction();

            if (("BET").equals(actionDesc)){
                int maxLine = Integer.parseInt(action.getArea());
                
                String val = "";
                for (int j=0;j<=maxLine;j++){
                    if (j < maxLine){
                        val += (j+1)+",";
                    } else {
                        val += (j+1);
                    }
                }
                action.setValue(val);
            } else if (("g:REEL_VIEW").equals(actionDesc)){
                filterReelView(actions, i, action);
            } else if (("START").equals(actionDesc)){
                actions.remove(i);
                i--;
            } else if (("WIN").equals(actionDesc)){
                int area = -1;
                String value = "";
                
                if (action.getArea() != null){
                    area = Integer.parseInt(action.getArea());
                }
                
                if (action.getValue() != null && action.getValue().toLowerCase().contains("jackpot")){
                    if (area != -1){
                        value = String.valueOf(area+1)+" - ";
                    }
                    
                    value += "JACKPOT";
                } else {
                    value = String.valueOf(area+1);
                }
                action.setValue(value);
            }
            
            String proper = getProperDescription(action);
            action.setAction(proper);
        } 
        
        return actions;
    }

    private String getProperDescription(GameActionBean gameAction) {
        String desc = gameAction.getAction();
        String action = gameAction.getAction();
        
        if (("BET").equals(action)) {
            desc = "Placed bet";
        } else if (("FINISH").equals(action)) {
            desc = "Game finished";
        } else if (("WIN").equals(action)) {
            if (gameAction.getArea() != null){
                desc = "Winning line";
            } else {
                desc = "Bells win";
            }
        } else if (("GUARD").equals(action))
            desc = "Guard intervension";

        return desc;
    }
    
    @Override
    protected GameComboBean getGameComboBean(Object[] winCombo, int comboIndex, ComboSymbolBean[] SYMBOLS) {
    	int comboSymbols = 0;
    	int comboSymbol;
    	for (int j=0; j < 3; j++) {
    		comboSymbol = (Integer)winCombo[j];
    		if (comboSymbol == 0)
    			continue;
    		
    		comboSymbols++;
    	}
    	
    	int[] symbol = new int[comboSymbols];
    	ComboSymbolBean[] symbols = new ComboSymbolBean[symbol.length];
    	
    	for (int j=0; j < comboSymbols; j++) {
    		symbol[j] = (Integer)winCombo[j];

    		if (symbol[j] < 0){
                symbol[j] *= -1;
            }
    		
    		symbols[j] = SYMBOLS[symbol[j]-1];
    	}

        double payout = config.getPayout(comboIndex);
        return new GameComboBean(symbols, payout);
    }
}
