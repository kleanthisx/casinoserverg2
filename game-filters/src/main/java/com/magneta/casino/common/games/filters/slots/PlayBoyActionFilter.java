/**
 * 
 */
package com.magneta.casino.common.games.filters.slots;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("PlayBoy")
public class PlayBoyActionFilter extends SlotGameActionFilter {

    @Override
    public List<GameActionBean> filterActions(List<GameActionBean> actions) {
        actions = super.filterActions(actions);
        
        for (int i=0;i<actions.size();i++){
            GameActionBean action = actions.get(i);
            String actionDesc = action.getAction();

            if (("g:REEL_VIEW").equals(actionDesc)){
                filterReelView(actions, i, action);
            } else if (("START").equals(actionDesc)){
                actions.remove(i);
                i--;
            } else if (("WIN").equals(actionDesc)){
                int area = -1;
                String value = "";
                
                if (action.getArea() != null){
                    area = Integer.parseInt(action.getArea());
                }
                
                if (action.getValue() != null && action.getValue().toLowerCase().contains("jackpot")){
                    if (area != -1){
                        value = String.valueOf(area+1)+" - ";
                    }
                    
                    value += "JACKPOT";
                } else {
                    value = String.valueOf(area+1);
                }
                action.setValue(value);
            }
            
            String proper = getProperDescription(action);
            action.setAction(proper);
        } 
        
        return actions;
    }

    private String getProperDescription(GameActionBean gameAction) {
        String desc = gameAction.getAction();
        String action = gameAction.getAction();
        
        if (("BET").equals(action)) {
            desc = "Placed bet";
        } else if (("FINISH").equals(action)) {
            desc = "Game finished";
        } else if (("WIN").equals(action)) {
            if (gameAction.getArea() != null){
                desc = "Winning line";
            } else {
                desc = "Player won";
            }
        } else if (("GUARD").equals(action))
            desc = "Guard intervension";

        return desc;
    }
}
