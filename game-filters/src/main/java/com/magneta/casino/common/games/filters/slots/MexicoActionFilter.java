/**
 * 
 */
package com.magneta.casino.common.games.filters.slots;

import java.util.List;
import java.util.StringTokenizer;

import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("Mexico")
public class MexicoActionFilter extends SlotGameActionFilter {

	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
		actions = super.filterActions(actions);

		for (int i=0;i<actions.size();i++){
			GameActionBean action = actions.get(i);
			String actionDesc = action.getAction();

			if (("g:REEL_VIEW").equals(actionDesc)){
                filterReelView(actions, i, action);
			} else if (("WIN").equals(actionDesc) 
					|| ("g:TREASURE_BONUS").equals(actionDesc)){
				int area = -1;
				String value = "";

				if (action.getArea() != null){
					area = Integer.parseInt(action.getArea());
				}

				if (action.getValue() != null && action.getValue().toLowerCase().contains("jackpot")){
					if (area != -1){
						value = String.valueOf(area+1)+" - ";
					}

					value += "JACKPOT";
				} else {
					value = String.valueOf(area+1);
				}
				action.setValue(value);
			} else if (("g:FREE_SPIN").equals(actionDesc)){
				StringTokenizer tok = new StringTokenizer(action.getArea()," ");    

				if (tok.hasMoreTokens()){
					int maxLine = Integer.parseInt(tok.nextToken());
					action.setValue("1 - "+(maxLine+1));
				}
				if (tok.hasMoreTokens()){
					action.setAmount(Double.parseDouble(tok.nextToken()));
				}
			} else if (("g:BOX_SELECT").equals(actionDesc)){
				if (actions.get(i+1).getAction().equals("WIN")){
					action.setBalance(actions.get(i+1).getBalance());
					action.setAmount(actions.get(i+1).getAmount());
					actions.remove(i+1);
				}
			} else if (("START").equals(actionDesc)
					|| ("g:START_FREE_SPIN").equals(actionDesc)){
				actions.remove(i);
				i--;
			}

			String proper = getProperDescription(action);
			action.setAction(proper);
		}
		return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();

		if (("BET").equals(action)) {
			desc = "Placed bet";
		} else if (("FINISH").equals(action)) {
			desc = "Game finished";
		} else if (("WIN").equals(action)) {
			if (gameAction.getArea() != null){
				desc = "Winning line";
			} else {
				desc = "Scatter win";
			}
		} else if (("GUARD").equals(action)){
			desc = "Guard intervension";
		} else if (("g:TREASURE_BONUS").equals(action)){
			desc = "Treasure Bonus";
		} else if (("g:FREE_SPIN").equals(action)){
			desc = "Free spin";
		} else if (("g:BOX_SELECT").equals(action)){
			desc = "Selected treasure box";
		} else if (("g:FREE_SPINS_WIN").equals(action)){
			desc = "Won free spins";
		} else if ("g:SCATTER_SELECT".equals(action)) {
			desc = "Scatter Select";
		}

		return desc;
	}

	@Override
	public String getSymbolDescription(int symbol) {
		return super.getSymbolDescription(symbol);
	}
}
