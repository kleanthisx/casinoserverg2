/**
 * 
 */
package com.magneta.casino.common.games.filters;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Configuration;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;

import com.google.common.base.Predicate;
import com.magneta.casino.games.templates.LocalConfigurationBuilder;
import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author Zenios
 *
 */
public class ActionFilterFactoryImpl implements ActionFilterFactory {

    private static final Map<String,Class<? extends ActionFilter>> gameFilterClasses = new HashMap<String,Class<? extends ActionFilter>>();
    
    public ActionFilterFactoryImpl() {   
        Configuration config = new LocalConfigurationBuilder() {
            String prefix = ActionFilterFactoryImpl.class.getPackage().getName();
            final Predicate<String> filter = new FilterBuilder.Include(FilterBuilder.prefix(prefix));

            {
                filterInputsBy(filter);
                setUrls(ClasspathHelper.forPackage(prefix));

                setScanners(
                        new TypeAnnotationsScanner().filterResultsBy(filter),
                        new SubTypesScanner().filterResultsBy(filter));
            }
        }; 
            
        Reflections reflections = new Reflections(config);
    
        Set<Class<? extends ActionFilter>> annotated = reflections.getSubTypesOf(ActionFilter.class); 

        for(Class<? extends ActionFilter> test:annotated) {
            GameTemplate gTemplate = test.getAnnotation(GameTemplate.class);
            if (gTemplate == null) {
                continue;
            }

            gameFilterClasses.put(gTemplate.value(), test);
        }
    }
    
    @Override
    public Class<? extends ActionFilter> getActionFilterClass(String gameTemplate) {
        return gameFilterClasses.get(gameTemplate);
    }
}
