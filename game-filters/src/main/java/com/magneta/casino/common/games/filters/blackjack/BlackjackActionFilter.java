package com.magneta.casino.common.games.filters.blackjack;

import java.util.List;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("Blackjack")
public class BlackjackActionFilter extends AbstractActionFilter {

    @Override
	public void setConfiguration(GameConfiguration config) { }
    
	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
         actions = super.filterActions(actions);
        
         for (int i=0;i<actions.size();i++){
             String actionDesc = actions.get(i).getAction();
             GameActionBean action = actions.get(i);
             
             if (("JOIN").equals(actionDesc) || ("g:HIDDEN").equals(actionDesc) || ("LEAVE").equals(actionDesc)){
                 actions.remove(i);
                 i--;
             } else if (("g:HIT").equals(actionDesc) || ("g:FLIP").equals(actionDesc)){
                 int card = Integer.parseInt(action.getValue());

                 action.addImageValue(getCardImage(card), describeCard(card)+" ");
             }
             
             String proper = getProperDescription(action);
             
  			 action.setAction(proper);
         }

         return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();
        
		if (("START").equals(action))
            desc = "Game started";
        else if (("BET").equals(action))
            desc = "Player placed bet";
        else if (("g:HIT").equals(action)){
            if (gameAction.getGenerated()){
                desc = "Received card";
            }
            else{
                if (gameAction.getSeat() == 0){
                    desc = "Dealer hit";
                } else {
                    desc = "Player hit";
                }
            }
        }
        else if (("g:STAND").equals(action)){
            if (gameAction.getSeat() == 0){
                desc = "Dealer stand";
            } else {
                desc = "Player stand";
            }
        }
        else if (("g:DOUBLE").equals(action))
            desc = "Player doubled bet";
        else if (("g:SPLIT").equals(action))
            desc = "Player split hands";
        else if (("g:FLIP").equals(action))
            desc = "Dealer flipped hidden card";
        else if (("g:INSURE").equals(action))
            desc = "Player took insurance";
        else if (("g:EVEN").equals(action))
            desc = "Player took even money";
        else if (("FINISH").equals(action))
            desc = "Game finished";
        else if (("WIN").equals(action))
            desc = "Player Won";
        else if (("GUARD").equals(action))
            desc = "Guard intervension";
        if ((gameAction.getArea() != null) && (desc != null)) {
            desc += " (Hand "+gameAction.getArea()+")";
        }
        
        return desc;
	}    
}
