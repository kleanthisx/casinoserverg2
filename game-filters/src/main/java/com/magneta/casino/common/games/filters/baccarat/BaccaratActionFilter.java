package com.magneta.casino.common.games.filters.baccarat;

import java.util.List;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("Baccarat")
public class BaccaratActionFilter extends AbstractActionFilter {
    
    public BaccaratActionFilter() {
        super();
    }
    
    public BaccaratActionFilter(GameConfiguration config) {
        super();
    }
    
    public BaccaratActionFilter(long configId, String config) {
        super();
    }
    
    @Override
	public void setConfiguration(GameConfiguration config) { }
	
	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
         actions = super.filterActions(actions);
        
         for (int i=0;i<actions.size();i++){
             String actionDesc = actions.get(i).getAction();
             GameActionBean action = actions.get(i);
             
             if (("BET").equals(actionDesc) || ("WIN").equals(actionDesc)){
                 action.setValue(parseArea(action.getArea()));
             } else if ("g:HAND".equals(actionDesc)) {
            	 String[] cards = action.getValue().split(" ");
            	 
            	 for (String cardVal: cards) {
            		 int card = Integer.parseInt(cardVal);
            		 action.addImageValue(getCardImage(card), describeCard(card)+" ");
            	 }
             } else if ("g:DEAL".equals(actionDesc)) {
            	 actions.remove(i);
                 i--;
                 continue;
             }
             
             String proper = getProperDescription(action);
             
  			 action.setAction(proper);
         }

         return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();
        
		if (("BET").equals(action)) {
            desc = "Bet";
		} else if (("g:HAND").equals(action)) {
        	if (gameAction.getArea().equals("0")){
        		desc = "Banker Cards";
        	} else {
        		desc = "Player Cards";
        	}
        }
        else if (("FINISH").equals(action))
            desc = "Game finished";
        else if (("WIN").equals(action))
            desc = "Player Won";
        else if (("GUARD").equals(action))
            desc = "Guard intervension";
        
        return desc;
	}
    
    public String parseArea(String area) {
    	String[] parts = area.split(" ");
		
		String actor = parts[0];
		String betType = parts[1];
		
		StringBuilder sb = new StringBuilder();
		
		if (actor.equals("1")) {
			sb.append("Player");
		} else if (actor.equals("2")) {
			sb.append("Banker");
		} else {
			sb.append("Both");
		}
		
		if (betType.equals("1")) {
			sb.append(" to win");
		} else if (betType.equals("2")) {
			sb.append("Tie");
		} else if (betType.equals("3")) {
			sb.append(" to have an Ace");
		} else if (betType.equals("4")) {
			sb.append(" to have a face card");
		} else if (betType.equals("5")) {
			sb.append(" to have an ace and face card");
		}
		
		return sb.toString();
    }
}
