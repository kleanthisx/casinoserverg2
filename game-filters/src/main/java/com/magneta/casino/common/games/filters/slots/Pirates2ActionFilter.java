/**
 * 
 */
package com.magneta.casino.common.games.filters.slots;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.utils.Card;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("Pirates2")
public class Pirates2ActionFilter extends SlotGameActionFilter {

    @Override
    public List<GameActionBean> filterActions(List<GameActionBean> actions) {
        actions = super.filterActions(actions);
        GameComboBean[] paytable = getPaytable();
        double bet = 0.0;
        double totalWins = 0.0;
        boolean afterDouble = false;
        int lastRace = 0;
        
        for (int i=0;i<actions.size();i++){
            GameActionBean action = actions.get(i);
            String actionDesc = action.getAction();

            if (("BET").equals(actionDesc)){
                bet = action.getAmount();
            } else if (("g:REEL_VIEW").equals(actionDesc)){
                filterReelView(actions, i, action);
            } else if (("START").equals(actionDesc)){
                actions.remove(i);
                i--;
            } else if (("WIN").equals(actionDesc)){
                if (afterDouble){
                    action.setValue("");
                } else {
                    String value = "";
                    int area = -1;
                    
                    if (action.getArea() != null){
                        area = Integer.parseInt(action.getArea())+1;
                    }
                    
                    if (action.getValue().toLowerCase().contains("jackpot")){
                        value = "Jackpot";
                        if (area > 0){
                            value = area + " - Jackpot";
                        }
                    } else {
                        value = String.valueOf(area);
                    }
                    action.setValue(value);
                }
            } else if (("g:WIN_LINE").equals(actionDesc)){
                afterDouble = true;
                double currWin = paytable[Integer.parseInt(action.getValue())].getPayout() * bet;
                totalWins += currWin;
                action.setAmount(currWin);
                String value = "";
                
                if (action.getArea() != null){
                    value =  String.valueOf((Integer.parseInt(action.getArea())+1));
                }
                action.setValue(value);
            } else if (("g:RED").equals(actionDesc) || ("g:BLACK").equals(actionDesc)){
                int color = (("g:RED").equals(actionDesc)? Card.RED_CARD : Card.BLACK_CARD);    

                int j = i+1;
                if (actions.get(j).getAction().equals("GUARD")){
                    j++;
                }

                if (actions.get(j).getAction().equals("g:DOUBLE_DRAW")){
                    GameActionBean next = actions.get(j);
                    int card = Integer.parseInt(next.getValue());

                    next.addImageValue(CARD_FRONT_IMAGE, "");
                    next.addImageValue(getCardImage(card), describeCard(card)+" ");
                    next.addImageValue(CARD_BACK_IMAGE, "");

                    if (Card.getCardColour(card) == color){
                        totalWins *= 2.0;
                        next.setAmount(totalWins);
                    }

                    next.setAction(action.getAction());
                    next.setValue("");
                    actions.remove(i);
                    i--;
                    action = next;
                }
            } else if (("g:RACE").equals(actionDesc)){
                int race = lastRace = Integer.parseInt(action.getArea());   

                int j = i+1;
                if (actions.get(j).getAction().equals("GUARD")){
                    j++;
                }

                if (actions.get(j).getAction().equals("g:DOUBLE_DRAW")){
                    GameActionBean next = actions.get(j);
                    int card = Integer.parseInt(next.getValue());

                    next.addImageValue(CARD_FRONT_IMAGE, "");
                    next.addImageValue(getCardImage(card), describeCard(card)+" ");
                    next.addImageValue(CARD_BACK_IMAGE, "");

                    if (Card.getCardRace(card) == race){
                        totalWins *= 4.0;
                        next.setAmount(totalWins);
                    }

                    next.setAction(action.getAction());
                    next.setValue("");
                    actions.remove(i);
                    i--;
                    action = next;
                }
            } else if (("g:TAKE_SCORE").equals(actionDesc)){
                action.setAmount(totalWins);
            }

            String proper = getProperDescription(action, afterDouble, lastRace);
            action.setAction(proper);
        } 
        
        return actions;
    }

    private String getProperDescription(GameActionBean gameAction, boolean afterDouble, int race) {
        String desc = gameAction.getAction();
        String action = gameAction.getAction();
        
        if (("BET").equals(action)) {
            desc = "Placed bet";
        } else if (("FINISH").equals(action)) {
            desc = "Game finished";
        } else if (("g:WIN_LINE").equals(action)) {
            desc = "Winning line";
        } else if (("WIN").equals(action)) {
            if (afterDouble){
                desc = "Player Won";
            } else {
                desc = "Winning line";
            }
        } else if (("GUARD").equals(action)){
            desc = "Guard intervension";
        } else if (("g:TAKE_SCORE").equals(action)){
            desc = "Took score";
        } else if (("g:RED").equals(action)){
            desc = "Predicted red card";
        } else if (("g:BLACK").equals(action)){
            desc = "Predicted black card";
        } else if (("g:DOUBLE_DRAW").equals(action)){
            desc = "Drew card";
        } else if (("g:RACE").equals(action)){
            String raceStr = "";
            switch(race){
                case Card.CLUBS_CARD: 
                    raceStr = "clubs";
                    break;
                case Card.DIAMONDS_CARD: 
                    raceStr = "diamonds";
                    break;
                case Card.HEARTS_CARD: 
                    raceStr = "hearts";
                    break;
                case Card.SPADES_CARD: 
                    raceStr = "spades";
                    break;
            }
            desc = "Predicted "+raceStr+" card";
        } 

        return desc;
    }
}
