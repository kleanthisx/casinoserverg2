package com.magneta.casino.common.games.filters.virtualracing;

import java.util.List;
import java.util.StringTokenizer;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;

@GameTemplate("VirtualDogRacing")
public class DogRacingActionFilter extends AbstractActionFilter {

    private static final int WIN_BET_TYPE = 0;
    private static final int QUINELLA_BET_TYPE = 1;

    public DogRacingActionFilter() {
        super();
    }
    
    public DogRacingActionFilter(GameConfiguration config) {
        super();
    }
    
    public DogRacingActionFilter(long configId, String config) {
        super();
    }
    
    @Override
	public void setConfiguration(GameConfiguration config) { }
    
    @Override
    public List<GameActionBean> filterActions(List<GameActionBean> actions) {
        actions = super.filterActions(actions);
        
        for (int i=0;i<actions.size();i++){
            String actionDesc = actions.get(i).getAction();
            GameActionBean action = actions.get(i);
            
            if (("g:RUNNER_JOIN").equals(actionDesc) || ("FINISH").equals(actionDesc) 
                    || ("START").equals(actionDesc)){
                actions.remove(i);
                i--;
            } else if (("BET").equals(actionDesc)){
                StringTokenizer tok = new StringTokenizer(action.getArea().substring(action.getArea().indexOf(" ")+1),"-");
                String hStr = ""; 
                
                boolean first = true;
                while(tok.hasMoreTokens()){
                    if (first){
                        hStr += (Integer.parseInt(tok.nextToken())+1);
                        first = false;
                    } else {
                        hStr += "-" + (Integer.parseInt(tok.nextToken())+1);
                    }
                }
                action.setValue(hStr);
            } else if (("WIN").equals(actionDesc)){
                StringTokenizer tok = new StringTokenizer(action.getArea().substring(action.getArea().indexOf(" ")+1),"-");
                String hStr = ""; 
                
                boolean first = true;
                while(tok.hasMoreTokens()){
                    if (first){
                        hStr += (Integer.parseInt(tok.nextToken())+1);
                        first = false;
                    } else {
                        hStr += "-" + (Integer.parseInt(tok.nextToken())+1);
                    }
                }
                action.setValue(hStr+" @ "+action.getValue());
            } else if (("g:RUNNER_RANK").equals(actionDesc)){
                action.setValue(String.valueOf((Integer.parseInt(action.getValue())+1)));
            }
            
            String proper = getProperDescription(action);
            action.setAction(proper);
        }
        
        return actions;
    }

    private String getProperDescription(GameActionBean gameAction) {
        String desc = gameAction.getAction();
        String action = gameAction.getAction();

        if (("BET").equals(action)) {
            String area = gameAction.getArea();
            int betType = Integer.parseInt(area.substring(0, area.indexOf(" ")));
            switch (betType){
                case WIN_BET_TYPE:
                    desc = "Placed WIN bet";
                    break;
                case QUINELLA_BET_TYPE:
                    desc = "Placed QUINELLA bet";
                    break;
                default:
                    desc = "Placed bet";
            }
        } else if (("g:START_RACE").equals(action)){
            desc = "Race started";
        }else if (("g:RUNNER_RANK").equals(action)){
            desc = "Race result - Position "+gameAction.getArea();
        } else if (("WIN").equals(action)) {
            String area = gameAction.getArea();
            int betType = Integer.parseInt(area.substring(0, area.indexOf(" ")));
            switch (betType){
                case WIN_BET_TYPE:
                    desc = "Player won WIN bet";
                    break;
                case QUINELLA_BET_TYPE:
                    desc = "Player won QUINELLA bet";
                    break;
                default:
                    desc = "Player won";
            }
        } else if (("GUARD").equals(action))
            desc = "Guard intervension";

        return desc;
    }
}
