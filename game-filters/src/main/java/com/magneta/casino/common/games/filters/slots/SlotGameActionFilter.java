package com.magneta.casino.common.games.filters.slots;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.GameConfigLoadingException;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.GameConfigurationFactory;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.slots.SlotConfiguration;

public abstract class SlotGameActionFilter extends AbstractActionFilter {

	private static final Logger log = LoggerFactory.getLogger(SlotGameActionFilter.class);
	
    protected SlotConfiguration config;
    
    public ComboSymbolBean[] getSymbols() {
    	ComboSymbolBean[] symbols = new ComboSymbolBean[this.config.getSymbolCount()];
    	
    	for (int i=0; i < symbols.length; i++) {
    		symbols[i] =  new ComboSymbolBean("" + (i+1), Integer.toString(i+1));
    	}
    	
    	return symbols;
    }
    
    /**
     * Returns the default configuration for the game
     * @return
     * @throws GameConfigLoadingException 
     */
    private GameConfiguration getDefaultConfig() {
    	GameTemplate gameTemplate = this.getClass().getAnnotation(GameTemplate.class);
    	
    	if (gameTemplate != null) {
    		try {
				return GameConfigurationFactory.loadConfiguration(gameTemplate.value());
			} catch (GameConfigLoadingException e) {
				log.error("Error while loading default configuration", e);
				return null;
			}
    	}
    	
    	return null;
    }

    @Override
	public void setConfiguration(GameConfiguration config) {
    	this.config = (SlotConfiguration)config;
    	
    	if (this.config == null) {
    		this.config = (SlotConfiguration)getDefaultConfig();
    	}
    }
    
    @Override
    public List<GameActionBean> filterActions(List<GameActionBean> actions){
        super.filterActions(actions);
        return actions;
    }

    protected void filterReelView(List<GameActionBean> actions, int actionIndex, GameActionBean action) {
    	String stops = action.getValue();

        String[] values = stops.split(" ");
        int[] reelStopSymbol = new int[values.length];

        for (int i=0; i < values.length; i++) {
        	reelStopSymbol[i] = Integer.valueOf(values[i]);
        }

        int[][] roundReels = config.getReels(0);
        GameActionBean[] reelStops = new GameActionBean[roundReels.length];
        int[][] view = new int[roundReels.length][3];

        for (int i=0; i < view.length; i++) {
        	view[i][0] = reelStopSymbol[(i*3)];
        	view[i][1] = reelStopSymbol[(i*3)+1];
        	view[i][2] = reelStopSymbol[(i*3)+2];
        }

        for (int i=0; i < roundReels.length; i++) {
            reelStops[i] = new GameActionBean();
            reelStops[i].setActionTime(action.getActionTime());
        }

        for (int k = 0;k<3;k++){
            for (int l = 0;l<view.length;l++){
                String currToken = String.valueOf(view[l][k]);
                String alt = currToken;
                if (l != (view.length-1)){
                    alt += " ";
                }

                reelStops[k].addImageValue(getSymbols()[view[l][k]-1].getImage(), alt);
            }
        }

        actions.add(actionIndex+1, reelStops[2]);
        actions.add(actionIndex+1, reelStops[1]);
        actions.add(actionIndex+1, reelStops[0]);
        actions.remove(actionIndex);
    }

    
    public String getSymbolDescription(int symbol) {
        if (symbol < 0) {
            return "(Scatter)";
        }
        
        return "";
    }
    
    protected GameComboBean getGameComboBean(Object[] winCombo, int comboIndex, ComboSymbolBean[] SYMBOLS) {
    	int symbol = (Integer)winCombo[0];
    	int quantity = (Integer)winCombo[1];
    	double payout = config.getPayout(comboIndex);

    	int line = -1;

    	if (winCombo.length > 3 && winCombo[3] instanceof Integer) {
    		line = (Integer)winCombo[3];
    	}

    	String symbolDesc = getSymbolDescription(symbol);
    	if (line >=0) {
    		symbolDesc += "(On line "+(line+1)+")";
    	}

    	if (symbol < 0){
    		symbol *= -1;
    	}

    	ComboSymbolBean[] symbols = new ComboSymbolBean[quantity];
    	if (symbol == 0) {
    		for (int j=0; j < quantity; j++) {
    			symbols[j] = new ComboSymbolBean("S", "S");
    		}
    	} else {
    		for (int j=0; j < quantity; j++) {
    			symbols[j] = SYMBOLS[symbol-1];
    		}
    	}

    	return new GameComboBean(symbols, symbolDesc, payout);
    }
    
    public final GameComboBean[] getPaytable() {
        Object[][] winCombos = config.getWinCombos();
        if (winCombos == null)
        	return null;
        
        ComboSymbolBean[] SYMBOLS = getSymbols();
        
        GameComboBean[] combos = new GameComboBean[winCombos.length];
        for (int i=0;i<combos.length;i++){
        	combos[i] = getGameComboBean(winCombos[i], i, SYMBOLS);
        }
        return combos;
    }
}
