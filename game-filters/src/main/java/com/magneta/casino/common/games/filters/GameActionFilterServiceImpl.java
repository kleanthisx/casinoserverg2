package com.magneta.casino.common.games.filters;

import java.lang.reflect.Constructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.casino.common.games.filters.ActionFilterFactory;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.configuration.GameConfiguration;

public class GameActionFilterServiceImpl implements GameActionFilterService {

	private static final Logger log = LoggerFactory.getLogger(GameActionFilterServiceImpl.class);
	
	private final GamesService gameService;
	private final PrincipalService principalService;
	private final ActionFilterFactory actionFilterFactory;
	
	public GameActionFilterServiceImpl(GamesService gameService,
			PrincipalService principalService,
			ActionFilterFactory actionFilterFactory) {
		this.gameService = gameService;
		this.principalService = principalService;
		this.actionFilterFactory = actionFilterFactory;
	}
	
    private ActionFilter getInstance(Class<? extends ActionFilter> filterClass) {
        if (filterClass == null) {
            return null;
        }

        try {
            Constructor<? extends ActionFilter> constructor = filterClass.getConstructor();
            return constructor.newInstance();
        } catch (Exception e) {
            log.warn("Error initializing " + filterClass, e);
        }
        return null;
    }

	@Override
	public ActionFilter getFilter(Integer gameId) throws ServiceException {
		
		GameBean game;
		
		principalService.pushSystem();
		try {
			game = gameService.getGame(gameId);
		} finally {
			principalService.popSystem();
		}
		
		Class<? extends ActionFilter> filterKlass = actionFilterFactory.getActionFilterClass(game.getGameTemplate());
		
        ActionFilter filter = getInstance(filterKlass);
        if (filter == null){
            filter = new DefaultActionFilter();
        }
        
        filter.setGameId(gameId);
        filter.setConfiguration(null);
        
        return filter;
    }

    @Override
	public ActionFilter getFilter(Integer gameId, GameConfiguration config) throws ServiceException {
		
		GameBean game;
		
		principalService.pushSystem();
		try {
			game = gameService.getGame(gameId);
		} finally {
			principalService.popSystem();
		}
		
		Class<? extends ActionFilter> filterKlass = actionFilterFactory.getActionFilterClass(game.getGameTemplate());
		
        ActionFilter filter = getInstance(filterKlass);
        if (filter == null){
            filter = new DefaultActionFilter();
        }
        
        filter.setGameId(gameId);
        filter.setConfiguration(config);
        
        return filter;
    }
}
