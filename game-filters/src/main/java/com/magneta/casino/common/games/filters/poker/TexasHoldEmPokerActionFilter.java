package com.magneta.casino.common.games.filters.poker;

import java.util.List;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;

@GameTemplate("TexasHoldEm")
public class TexasHoldEmPokerActionFilter extends AbstractActionFilter {

	private TexasHoldemConfiguration config;

	@Override
	public void setConfiguration(GameConfiguration config) { 
		this.config = (TexasHoldemConfiguration)config;

		if (this.config == null) {
			this.config = new TexasHoldemConfiguration();
		}
	}

	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
		actions = super.filterActions(actions);

		for (int i=0;i<actions.size();i++){
			String actionDesc = actions.get(i).getAction();
			GameActionBean action = actions.get(i);

			if ("g:HIT".equals(actionDesc)) {
				String[] cards = action.getValue().split(" ");

				for (String cardVal: cards) {
					if(cardVal.length()>0){
						int card = Integer.parseInt(cardVal);
						action.addImageValue(getCardImage(card), describeCard(card)+" ");
					}
				}
			}

			String proper = getProperDescription(action);

			action.setAction(proper);
		}

		return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();

		if (("BET").equals(action)) {
			desc = "Bet";
		} else if (("g:HIT").equals(action)) {
			if (gameAction.getSeat() == 0) {
				desc = "Dealer Cards";
			} else if (gameAction.getSeat() == 1) {
				desc = "Player Cards";
			} else if (gameAction.getSeat() == 2) {
				desc = "Common Cards";
			}
		}
		else if (("FINISH").equals(action)){
			desc = "Game finished";
		}else if (("WIN").equals(action)){
			desc = "Player Won";
		}else if (("GUARD").equals(action)){
			desc = "Guard intervension";
		}else if ("g:RAISE".equals(action)) {
			desc = "Player Raises";
		} else if ("g:FOLD".equals(action)) {
			desc = "Player Folds";
		} else if ("g:TURN".equals(action)) {
			desc = "Player bets on turn";
		} else if ("g:RIVER".equals(action)) {
			desc = "Player bets on river";
		}
		return desc;
	}
}
