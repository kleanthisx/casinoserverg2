package com.magneta.casino.common.games.filters.keno;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.keno.KenoConfiguration;

@GameTemplate("Keno")
public class KenoActionFilter extends AbstractActionFilter {
	
	private static final Logger log = LoggerFactory.getLogger(KenoActionFilter.class);
    
	private KenoConfiguration config;
	
    public KenoActionFilter() {
        super();
    }
    
    public KenoActionFilter(GameConfiguration config) {
    	super();
    	this.config = (KenoConfiguration) config;
    }
    
    @Override
	public void setConfiguration(GameConfiguration config) {
    	this.config = (KenoConfiguration) config;
    }
	
	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
         actions = super.filterActions(actions);
        
         for (int i=0;i<actions.size();i++){
             String actionDesc = actions.get(i).getAction();
             GameActionBean action = actions.get(i);
             
             if (("BET").equals(actionDesc) || ("WIN").equals(actionDesc)){
                 action.setValue(action.getArea());
             } else if ("g:DRAW".equals(actionDesc)) {
            	 actions.remove(i);
                 i--;
                 continue;
             }
             String proper = getProperDescription(action);
             
  			 action.setAction(proper);
         }

         return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();
        
		if (("BET").equals(action)) {
            desc = "Bet";
		} else if (("g:HAND").equals(action)) {
        	if (gameAction.getArea().equals("0")){
        		desc = "Banker Cards";
        	} else {
        		desc = "Player Cards";
        	}
        }
        else if (("FINISH").equals(action))
            desc = "Game finished";
        else if (("WIN").equals(action))
            desc = "Player Won";
        else if (("GUARD").equals(action))
            desc = "Guard intervension";
        
        return desc;
	}
	
	public GameComboBean[] getPaytable() {
		Object[][] paytable = this.config.getWinCombos();
		log.debug(paytable.length+"");
		GameComboBean[] combos = new GameComboBean[paytable.length];
		for (int i=0;i<paytable.length;i++){
			Object[] combo = paytable[i];
			combos[i] = new GameComboBean(null,"maches  " +combo[1] + " out of "+combo[0], (Double)combo[2],true);
		}
		return combos;
	}
	
}
