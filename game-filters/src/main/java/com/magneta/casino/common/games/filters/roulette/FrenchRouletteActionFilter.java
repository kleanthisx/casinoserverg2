/**
 * 
 */
package com.magneta.casino.common.games.filters.roulette;

import java.util.List;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;

/**
 * @author User
 *
 */
@GameTemplate("FrenchRoulette")
public class FrenchRouletteActionFilter extends AbstractActionFilter {

        private static final int NUMBER_BET = 0;
        private static final int COLUMN_BET = 1;
        private static final int DOZEN_BET = 2;
        private static final int COLOR_BET = 3;
        private static final int ODD_EVEN_BET = 4;
        private static final int HIGH_LOW_BET = 5;

        public FrenchRouletteActionFilter() {
            super();
        }
        
        public FrenchRouletteActionFilter(GameConfiguration config) {
            super();
        }
        
        @Override
		public void setConfiguration(GameConfiguration config) {
        }

        @Override
        public List<GameActionBean> filterActions(List<GameActionBean> actions) {
            actions = super.filterActions(actions);
            
            for (int i=0;i<actions.size();i++){
                String actionDesc = actions.get(i).getAction();
                GameActionBean action = actions.get(i);
               
                if (("BET").equals(actionDesc) || ("WIN").equals(actionDesc)){
                    action.setValue(parseArea(action.getArea()));
                }
                
                String proper = getProperDescription(action);
                action.setAction(proper);
            }
            
            return actions;
        }
        
        private String getProperDescription(GameActionBean gameAction) {
            String desc = gameAction.getAction();
            String action = gameAction.getAction();

            if (("BET").equals(action)) {
                desc = "Placed bet";
            } else if (("g:SPIN").equals(action)){
                desc = "Spin Roulette";
            } else if (("WIN").equals(action)) {
                desc = "Player won"; 
            } else if (("GUARD").equals(action)){
                desc = "Guard intervension";
            } else if (("FINISH").equals(action)) {
                desc = "Game Finished"; 
            }

            return desc;
        }
        
        public String parseArea(String area){
            String[] betParts = area.split("_");
            int betType = Integer.parseInt(betParts[0]);
            int numCount = betParts[1].split(",").length;
            
            switch (betType){
                case NUMBER_BET:{
                    String val = "";
                    switch(numCount){
                        case 1:
                            val += "Straight up ";
                            break;
                        case 2:
                            val += "Split ";
                            break;
                        case 3:
                            val += "Street ";
                            break;
                        case 4:
                            val += "Corner ";
                            break;
                        case 6:
                            val += "Line ";
                            break;
                    }
                    return (val+betParts[1]);
                }
                case COLUMN_BET:{
                    int col = Integer.parseInt(betParts[1].replace(",", ""));
                    return "Column "+(col+1);
                }
                case DOZEN_BET:{
                    int col = Integer.parseInt(betParts[1].replace(",", ""));
                    
                    switch (col){
                        case 0:
                            return "1st Dozen";
                        case 1:
                            return "2nd Dozen";
                        case 2:    
                            return "3rd Dozen";
                    }
                    break;
                }
                case COLOR_BET:{
                    int col = Integer.parseInt(betParts[1].replace(",", ""));
                    
                    return (col == 0)?"Red":"Black";
                }
                case ODD_EVEN_BET:{
                    int col = Integer.parseInt(betParts[1].replace(",", ""));
                    
                    return (col == 0)?"Odd":"Even";
                }
                case HIGH_LOW_BET:{
                    int col = Integer.parseInt(betParts[1].replace(",", ""));
                    
                    return (col == 0)?"1 to 18":"19 to 36";
                }
            }
            
            return area;
        }
}
