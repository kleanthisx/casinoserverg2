/**
 * 
 */
package com.magneta.casino.common.games.filters.slots;

import java.util.List;
import java.util.StringTokenizer;

import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.games.configuration.annotations.GameTemplate;
import static com.magneta.games.configuration.slots.ThunderstruckConfiguration.*;

@GameTemplate("Thunderstruck")
public class ThunderstruckActionFilter extends SlotGameActionFilter {

	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
		actions = super.filterActions(actions);

		for (int i=0;i<actions.size();i++){
			GameActionBean action = actions.get(i);
			String actionDesc = action.getAction();

			if (("BET").equals(actionDesc)){ 
				int maxLine = Integer.parseInt(action.getArea());
				action.setValue("1 - "+(maxLine+1));
			} else if (("g:REEL_VIEW").equals(actionDesc)){
                filterReelView(actions, i, action);
			} else if (("WIN").equals(actionDesc)){
				int area = -1;
				String value = "";

				if (action.getArea() != null){
					area = Integer.parseInt(action.getArea());
				}

				if (action.getValue() != null && action.getValue().toLowerCase().contains("jackpot")){
					if (area != -1){
						value = String.valueOf(area+1)+" - ";
					}

					value += "JACKPOT";
				} else {
					value = String.valueOf(area+1);
				}
				action.setValue(value);
			} else if (("g:FREE_SPIN").equals(actionDesc)){
				StringTokenizer tok = new StringTokenizer(action.getArea()," ");    

				if (tok.hasMoreTokens()){
					int maxLine = Integer.parseInt(tok.nextToken());
					action.setValue("1 - "+(maxLine+1));
				}
				if (tok.hasMoreTokens()){
					action.setAmount(Double.parseDouble(tok.nextToken()));
				}
			} else if (("START").equals(actionDesc)
					|| ("g:START_FREE_SPIN").equals(actionDesc)){
				actions.remove(i);
				i--;
			}

			String proper = getProperDescription(action);
			action.setAction(proper);
		}
		return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();

		if (("BET").equals(action)) {
			desc = "Placed bet";
		} else if (("FINISH").equals(action)) {
			desc = "Game finished";
		} else if (("WIN").equals(action)) {
			if (gameAction.getArea() != null){
				desc = "Winning line";
			} else {
				desc = "Scatter win";
			}
		} else if (("GUARD").equals(action)){
			desc = "Guard intervension";
		} else if (("g:FREE_SPIN").equals(action)){
			desc = "Free spin";
		} else if (("g:FREE_SPINS_WIN").equals(action)){
			desc = "Won free spins";
		}

		return desc;
	}

	@Override
	public String getSymbolDescription(int symbol) {
		if (symbol == FREE_SPIN_SCATTER_1) {
			return "(Free spin symbol 1)";
		}
		
		return super.getSymbolDescription(symbol);
	}
}
