/**
 * 
 */
package com.magneta.casino.common.games.filters.slots;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.annotations.GameTemplate;
import static com.magneta.games.configuration.slots.KingOfAztecConfiguration.*;

@GameTemplate("KingOfAztec")
public class KingOfAztecActionFilter extends SlotGameActionFilter {

	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
		actions = super.filterActions(actions);

		for (int i=0;i<actions.size();i++){
			GameActionBean action = actions.get(i);
			String actionDesc = action.getAction();

			if (("BET").equals(actionDesc)){ 
				int maxLine = Integer.parseInt(action.getArea());
				action.setValue("1 - "+(maxLine+1));
			} else if (("g:REEL_VIEW").equals(actionDesc)) {
                filterReelView(actions, i, action);
			} else if (("WIN").equals(actionDesc) 
					|| ("g:TREASURE_BONUS").equals(actionDesc)){
				int area = -1;
				String value = "";

				if (action.getArea() != null){
					area = Integer.parseInt(action.getArea());
				}

				if (action.getValue() != null && action.getValue().toLowerCase().contains("jackpot")){
					if (area != -1){
						value = String.valueOf(area+1)+" - ";
					}

					value += "JACKPOT";
				} else {
					value = String.valueOf(area+1);
				}
				action.setValue(value);
			}else if (("START").equals(actionDesc)){
				actions.remove(i);
				i--;
			}

			String proper = getProperDescription(action);
			action.setAction(proper);
		}
		return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();

		if (("BET").equals(action)) {
			desc = "Placed bet";
		} else if (("FINISH").equals(action)) {
			desc = "Game finished";
		} else if (("WIN").equals(action)) {
			if (gameAction.getArea() != null){
				desc = "Winning line";
			} else {
				desc = "Scatter win";
			}
		} else if (("GUARD").equals(action)){
			desc = "Guard intervension";
		}

		return desc;
	}

	@Override
	protected GameComboBean getGameComboBean(Object[] winCombo, int comboIndex, ComboSymbolBean[] SYMBOLS) {
		if (winCombo.length <= 3) {
			return super.getGameComboBean(winCombo, comboIndex, SYMBOLS);
		}

		int symbol = (Integer)winCombo[0];
		int quantity = (Integer)winCombo[1];
		double payout = config.getPayout(comboIndex);

		ComboSymbolBean[] symbols = new ComboSymbolBean[quantity];
		for (int j=0;j<quantity;j++){
			symbols[j] = new ComboSymbolBean("S", "S");
		}

		String symbolDesc = getSymbolDescription(symbol);
		
		return new GameComboBean(symbols, symbolDesc, payout);
	}
	
	@Override
	public String getSymbolDescription(int symbol) {
		if (symbol == WILD_SYMBOL) {
			return "(Wild)";
		}

		return super.getSymbolDescription(symbol);
	}
}
