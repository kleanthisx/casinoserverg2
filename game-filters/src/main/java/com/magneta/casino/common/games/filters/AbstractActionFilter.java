package com.magneta.casino.common.games.filters;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.utils.Card;

public abstract class AbstractActionFilter implements ActionFilter {
    
	private static final String CARD_IMAGES_REL_PATH = "cards/";
	public static final String CARD_FRONT_IMAGE = CARD_IMAGES_REL_PATH+"front.jpg";
    public static final String CARD_BACK_IMAGE = CARD_IMAGES_REL_PATH+"back.jpg";
	
    protected boolean showGuards = false;
    protected int gameId;
    
    @Override
	public void setGameId(int gameId) {
    	this.gameId = gameId;
    }
    
    @Override
	public int getGameId() {
    	return this.gameId;
    }

    @Override
	public void setShowGuards(boolean showGuards) {
        this.showGuards = showGuards;
    }
    
    /*Any subclasses overriding this method should call this (base class) method in the
      first line of the overriding method and do further filtering on the returned list.*/
	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions){
        for (int i=0;i<actions.size();i++){
            String actionDesc = actions.get(i).getAction();
            GameActionBean action = actions.get(i);
            
            if (("GUARD").equals(actionDesc)){
                if (!showGuards){
                    actions.remove(i);
                    i--;
                } else {
                    if (("0").equals(action.getValue())){
                        action.setValue("Unsuccessful");
                    } else {
                        action.setValue("Success");
                    }
                }
            }
        }
        return actions;
    }

	protected String describeCard(int card){
		String races[] = {" of Clubs", " of Diamonds", " of Hearts", " of Spades", " "};
		String cards[] = new String[14];

		cards[0] = "A";
		for (int i=2;i<=10; i++){
			cards[i-1] = String.valueOf(i);
		}

		cards[10] = "J";
		cards[11] = "Q";
		cards[12] = "K";
		cards[13] = "Joker";

		return cards[Card.getCardNumber(card)-1] + races[Card.getCardRace(card)];
	}
    
	public String getCardImage(int card) {
		return "card"+ Card.getCardNumber(card) + "_" + Card.getCardRace(card);
	}
}
