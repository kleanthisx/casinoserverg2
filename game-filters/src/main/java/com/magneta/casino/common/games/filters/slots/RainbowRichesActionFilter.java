/**
 * 
 */
package com.magneta.casino.common.games.filters.slots;

import java.util.List;

import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.annotations.GameTemplate;
import static com.magneta.games.configuration.slots.RainbowRichesConfiguration.*;

@GameTemplate("RainbowRiches")
public class RainbowRichesActionFilter extends SlotGameActionFilter {

	private static final String WELL_SELECT_ACTION = "g:WELL_SELECT";
	private static final String ROAD_SELECT_ACTION = "g:ROAD_SELECT";
	private static final String POT_SELECT_ACTION  = "g:POT_SELECT";


	private static final String ROAD_SCATTER_PROMPT = "g:ROAD_PROMPT";
	private static final String POT_SCATTER_PROMPT  = "g:POT_PROMPT";
	private static final String WELL_SCATTER_PROMPT = "g:WELL_PROMPT";

	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions) {
		actions = super.filterActions(actions);

		for (int i=0;i<actions.size();i++){
			GameActionBean action = actions.get(i);
			String actionDesc = action.getAction();

			if (("BET").equals(actionDesc)){ 
				int maxLine = Integer.parseInt(action.getArea());
				action.setValue("1 - "+(maxLine+1));
			} else if (("g:REEL_VIEW").equals(actionDesc)){
				filterReelView(actions, i, action);
			} else if (("WIN").equals(actionDesc)) {
				int area = -1;
				String value = "";

				if (action.getArea() != null){
					area = Integer.parseInt(action.getArea());
				}

				if (action.getValue() != null && action.getValue().toLowerCase().contains("jackpot")){
					if (area != -1){
						value = String.valueOf(area+1)+" - ";
					}

					value += "JACKPOT";
				} else {
					value = String.valueOf(area+1);
				}
				action.setValue(value);
			} else if ((ROAD_SELECT_ACTION).equals(actionDesc)){
				if (actions.get(i+1).getAction().equals("WIN")){
					action.setBalance(actions.get(i+1).getBalance());
					action.setAmount(actions.get(i+1).getAmount());
					actions.remove(i+1);
				}			
			} else if ((POT_SELECT_ACTION).equals(actionDesc)){
				if (actions.get(i+1).getAction().equals("WIN")){
					action.setBalance(actions.get(i+1).getBalance());
					action.setAmount(actions.get(i+1).getAmount());
					actions.remove(i+1);
				}
			} else if ((WELL_SELECT_ACTION).equals(actionDesc)){
				if (actions.get(i+1).getAction().equals("WIN")){
					action.setBalance(actions.get(i+1).getBalance());
					action.setAmount(actions.get(i+1).getAmount());
					actions.remove(i+1);
				}
			}

			String proper = getProperDescription(action);
			action.setAction(proper);
		}
		return actions;
	}

	private String getProperDescription(GameActionBean gameAction) {
		String desc = gameAction.getAction();
		String action = gameAction.getAction();

		if (("BET").equals(action)) {
			desc = "Placed bet";
		} else if (("FINISH").equals(action)) {
			desc = "Game finished";
		} else if (("WIN").equals(action)) {
			if (gameAction.getArea() != null){
				desc = "Winning line";
			} else {
				desc = "Scatter win";
			}
		} else if (("GUARD").equals(action)){
			desc = "Guard intervension";
		} else if ((ROAD_SCATTER_PROMPT).equals(action)){
			desc = "Road Bonus";
		} else if ((WELL_SCATTER_PROMPT).equals(action)){
			desc = "Well Bonus";
		} else if ((POT_SCATTER_PROMPT).equals(action)){
			desc = "Pot Bonus";
		} else if ((ROAD_SELECT_ACTION).equals(action)){
			desc = "Road Spin";
		} else if ((WELL_SELECT_ACTION).equals(action)){
			desc = "Well Select";
		} else if ((POT_SELECT_ACTION).equals(action)){
			desc = "Pot Selection";
		}

		return desc;
	}

	@Override
	protected GameComboBean getGameComboBean(Object[] winCombo, int comboIndex, ComboSymbolBean[] SYMBOLS) {
		if (winCombo.length <= 3) {
			return super.getGameComboBean(winCombo, comboIndex, SYMBOLS);
		}

		// this goes for special cases only.
		int symbol = (Integer)winCombo[0];
		int quantity = (Integer)winCombo[1];
		double payout = config.getPayout(comboIndex);

		int numBoxes = (Integer)winCombo[4];

		ComboSymbolBean[] symbols = new ComboSymbolBean[quantity];
		for (int j=0;j<quantity;j++){
			symbols[j] = new ComboSymbolBean("S", "S");
		}

		String symbolDesc = getSymbolDescription(symbol);
		if (numBoxes > 0) {
			symbolDesc += "(Bonus boxes: "+numBoxes+")";
		}

		return new GameComboBean(symbols, symbolDesc, payout);
	}

	@Override
	public String getSymbolDescription(int symbol) {
		if (symbol == WILD_SYMBOL) {
			return "(Wild)";
		}

		return super.getSymbolDescription(symbol);
	}
}
