package com.magneta.casino.common.games.filters.poker;

import java.util.List;

import com.magneta.casino.common.games.filters.AbstractActionFilter;
import com.magneta.casino.common.games.filters.beans.GameActionBean;
import com.magneta.casino.common.games.filters.beans.GameActionValue;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.utils.Card;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.annotations.GameTemplate;
import com.magneta.games.configuration.poker.videopoker.VideoPokerPlusConfiguration;

@GameTemplate("VideoPokerPlus")
public class VideoPokerPlusActionFilter extends AbstractActionFilter {

	private static final String[] combo_descriptions = {
		"None",
		"Jacks Or Better",
		"Two Pairs",
		"Three Of A Kind",
		"Straight",
		"Flush",
		"Full House",
		"Four Of A Kind",
		"Straight Flush",
		"Royal Flush"
	};
	
	private VideoPokerPlusConfiguration config;

	public VideoPokerPlusActionFilter() {
		super();
		this.config = new VideoPokerPlusConfiguration();
	}

	@Override
	public void setConfiguration(GameConfiguration config) {
    	this.config = (VideoPokerPlusConfiguration)config;
    	
    	if (this.config == null) {
    		this.config = new VideoPokerPlusConfiguration();
    	}
    }

	@Override
	public List<GameActionBean> filterActions(List<GameActionBean> actions){
		actions = super.filterActions(actions);

		int cards[] = new int[5];
		int doubleUpCards[] = new int[5];
		int doubleCard = -1;
		double multiplier = 0.0;
		double betAmount = 0.0;
		int winCombo = -1;

		for (int i=0;i<actions.size();i++){
			GameActionBean action = actions.get(i);
			String actionDesc = action.getAction();

			if (("BET").equals(actionDesc)){
				betAmount = action.getAmount();
			} else if (("g:DRAW").equals(actionDesc)){
				int card = Integer.parseInt(action.getValue());
				int area = Integer.parseInt(action.getArea());

				cards[area] = card;

				action.addImageValue(getCardImage(card), describeCard(card)+" ");

				int j = i+1;
				while (actions.get(j).getAction().equals("g:DRAW")){
					card = Integer.parseInt(actions.get(j).getValue());
					area = Integer.parseInt(actions.get(j).getArea());
					cards[area] = card;

					action.addImageValue(getCardImage(card), describeCard(card)+" ");

					actions.remove(j);
				}
			} else if (("g:DOUBLE_DRAW").equals(actionDesc)){
				int card = Integer.parseInt(action.getValue());
				int area = Integer.parseInt(action.getArea());

				doubleUpCards[area] = card;

				action.addImageValue(getCardImage(card), describeCard(card)+" ");
			} else if (("g:HOLD").equals(actionDesc)){
				int area = Integer.parseInt(action.getArea());
				int card = cards[area];

				action.addImageValue(getCardImage(card), describeCard(card)+" ");

				int j = i+1;

				while (actions.get(j).getAction().equals("g:HOLD")){
					area = Integer.parseInt(actions.get(j).getArea());
					card = cards[area];

					action.addImageValue(getCardImage(card), describeCard(card)+" ");

					actions.remove(j);
				}
			} else if (("g:WIN_CARD").equals(actionDesc)){
				if ((doubleCard < 0) && (action.getValue() != null)){
					winCombo = Integer.parseInt(action.getValue());
				}
				actions.remove(i);
				i--;
			} else if (("g:CHANGE_CARD").equals(actionDesc)){
				if (actions.get(i+1).getAction().equals("g:DOUBLE_DRAW")){
					int card = Integer.parseInt(actions.get(i+1).getValue());

					int area = Integer.parseInt(actions.get(i+1).getArea());
					doubleUpCards[area] = card;
					actions.get(i+1).addImageValue(getCardImage(card), describeCard(card)+" ");
					actions.get(i+1).setAction(action.getAction());
					action = actions.get(i+1);
				}
				actions.remove(i);
			} else if (("g:DOUBLE_UP").equals(actionDesc)){
				if (winCombo != -1){
					multiplier = getMultiplier(winCombo);
					action.setValue(getComboDescription(winCombo));
					winCombo = -1;
				}
				action.setAmount(multiplier * betAmount);
			} else if (("g:DOUBLE_HALF").equals(actionDesc)){
				if (winCombo != -1){
					multiplier = getMultiplier(winCombo);
					action.setValue(getComboDescription(winCombo));
					winCombo = -1;
				}
				multiplier /= 2.0;

				if (actions.get(i+1).getAction().equals("WIN")){
					action.setAmount(actions.get(i+1).getAmount());
					action.setBalance(actions.get(i+1).getBalance());
				}
			} else if (("g:HIGHER").equals(actionDesc)){
				int j = i+1;
				if (actions.get(j).getAction().equals("GUARD")){
					j++;
				}

				if (actions.get(j).getAction().equals("g:DOUBLE_DRAW")){
					GameActionBean next = actions.get(j);
					int card = Integer.parseInt(next.getValue());
					int num = Card.getCardNumber(card);
					next.addImageValue(getCardImage(card), describeCard(card)+" ");

					int dblNum = Card.getCardNumber(doubleUpCards[Integer.parseInt(next.getArea())-1]);

					if (num == Card.ACE){
						num = 14;
					}

					if (dblNum == Card.ACE){
						num = 14;
					}

					if (num == dblNum){
						next.setAmount(multiplier * betAmount);
					} else if (num > dblNum) {
						multiplier *= 2.0;
						next.setAmount(multiplier * betAmount);
					} else {
						multiplier = 0.0;
					}

					doubleUpCards[Integer.parseInt(next.getArea())] = card;

					next.setAction(action.getAction());
					actions.remove(i);
					i--;
					action = next;
				}
			} else if (("g:LOWER").equals(actionDesc)){
				int j = i+1;
				if (actions.get(j).getAction().equals("GUARD")){
					j++;
				}

				if (actions.get(j).getAction().equals("g:DOUBLE_DRAW")){
					GameActionBean next = actions.get(j);
					int card = Integer.parseInt(next.getValue());
					int num = Card.getCardNumber(card);
					next.addImageValue(getCardImage(card), describeCard(card)+" ");

					int dblNum = Card.getCardNumber(doubleUpCards[Integer.parseInt(next.getArea())-1]);

					if (num == Card.ACE){
						num = 14;
					}

					if (dblNum == Card.ACE){
						num = 14;
					}

					if (num == dblNum){
						next.setAmount(multiplier * betAmount);
					} else if (num < dblNum) {
						multiplier *= 2.0;
						next.setAmount(multiplier * betAmount);
					} else {
						multiplier = 0.0;
					}

					doubleUpCards[Integer.parseInt(next.getArea())] = card;

					next.setAction(action.getAction());
					actions.remove(i);
					i--;
					action = next;
				}
			} else if (("g:RED").equals(actionDesc) || ("g:BLACK").equals(actionDesc)){
				int j = i+1;
				if (actions.get(j).getAction().equals("GUARD")){
					j++;
				}

				if (actions.get(j).getAction().equals("g:DOUBLE_DRAW")){
					GameActionBean next = actions.get(j);
					int card = Integer.parseInt(next.getValue());
					int num = Card.getCardNumber(card);
					next.addImageValue(getCardImage(card), describeCard(card)+" ");

					int dblNum = Card.getCardNumber(doubleUpCards[Integer.parseInt(next.getArea())-1]);

					if (num == Card.ACE){
						num = 14;
					}

					if (dblNum == Card.ACE){
						num = 14;
					}

					if (num == dblNum){
						next.setAmount(multiplier * betAmount);
					} else if (num > dblNum) {
						multiplier *= 2.0;
						next.setAmount(multiplier * betAmount);
					} else {
						multiplier = 0.0;
					}

					doubleUpCards[Integer.parseInt(next.getArea())] = card;

					next.setAction(action.getAction());
					next.setValue("");
					actions.remove(i);
					i--;
					action = next;
				}
			} else if (("g:TAKE_SCORE").equals(actionDesc)){
				if ((doubleCard <= 0) && (winCombo != -1)){
					action.setValue(getComboDescription(winCombo));
				}

				action.setAmount(actions.get(i+2).getAmount());
				action.setBalance(actions.get(i+2).getBalance());
			} else if (("FINISH").equals(actionDesc)){
				if (i < (actions.size()-1)){

					if (actions.get(i+1).getAction().equals("WIN")){
						action.setValue(getComboDescription(winCombo));
						action.setAmount(actions.get(i+1).getAmount());
						action.setBalance(actions.get(i+1).getBalance());
						actions.remove(i+1);
					} else if (actions.get(i+1).getAction().equals("g:WIN_CARD")){
						int combo = Integer.parseInt(actions.get(i+1).getValue());

						GameActionBean last = actions.get(actions.size()-1);
						action.setAmount(last.getAmount());
						action.setBalance(last.getBalance());
						action.setValue(getComboDescription(combo));
					}
				}
			} else if (("START").equals(actionDesc)
					|| ("g:HOLD_SUGGEST").equals(actionDesc) 
					|| ("g:HOLD_REASON").equals(actionDesc)
					|| ("g:POSSIBLE").equals(actionDesc)
					|| ("g:END_HOLD").equals(actionDesc)
					|| ("WIN").equals(actionDesc)){
				actions.remove(i);
				i--;
			}

			String proper = getProperDescription(action);

			action.setAction(proper);
		}

		return actions;
	}

	private double getMultiplier(int combo){
	    return this.config.getMultiplier(combo);
	}

	private String getComboDescription(int combo){
		if (combo < 0 || combo >= combo_descriptions.length) {
			return "";
		}

		return combo_descriptions[combo];
	}
	
	private String getProperDescription(GameActionBean gameAction){
		String desc = gameAction.getAction();
		String action = gameAction.getAction();

		if (("BET").equals(action)) {
			desc = "Placed bet";
		} else if (("g:DRAW").equals(action)){
			desc = "Received cards";
		} else if (("g:DOUBLE_DRAW").equals(action)){
			List<GameActionValue> values = gameAction.getValues();

			if (values != null && values.size() >= 4){
				desc = "Double cards";
			} else {
				desc = "Double card";
			}
		}else if (("g:HOLD").equals(action)){
			desc = "Kept cards";
		} else if (("g:DOUBLE_UP").equals(action)){
			desc = "Doubled up";
		} else if (("g:DOUBLE_HALF").equals(action)){
			desc = "Doubled half";
		} else if (("g:HIGHER").equals(action)){
			desc = "Predicted Higher Card";
		} else if (("g:LOWER").equals(action)){
			desc = "Predicted Lower Card";
		} else if (("g:RED").equals(action)){
			desc = "Predicted Red Card";
		} else if (("g:BLACK").equals(action)){
			desc = "Predicted Black Card";
		} else if (("g:TAKE_SCORE").equals(action)){
			desc = "Took score";
		} else if (("g:CHANGE_CARD").equals(action)){
			desc = "Changed card";
		} else if (("FINISH").equals(action)) {
			desc = "Game finished";
		} else if (("WIN").equals(action)){
			desc = "Player Won";
		} else if (("GUARD").equals(action))
			desc = "Guard intervension";

		return desc;
	}

	public GameComboBean[] getPaytable() {
		double[] paytable = config.getPaytable();

		GameComboBean[] combos = new GameComboBean[paytable.length];
		for (int i=0;i<combos.length;i++){
			combos[i] = new GameComboBean(null,combo_descriptions[i], paytable[i],(i != 0));
		}
		return combos;
	}
}