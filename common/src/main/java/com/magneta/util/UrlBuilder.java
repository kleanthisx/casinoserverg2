package com.magneta.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class UrlBuilder {
	
	private boolean isFirstParam;
	private StringBuilder builder;
	
	/**
	 * Creates a url builder from the given url.
	 * @param baseUrl The url to build. 
	 * You may omit the query but you don't have to.
	 */
	public UrlBuilder(String baseUrl) {
		builder = new StringBuilder(baseUrl);
		
		isFirstParam = !baseUrl.contains("?");
	}
	
	@Override
	public String toString() {
		return builder.toString();
	}
	
	/**
	 * Appends a URL parameter.
	 * If the value is null the parameter will not be appended.
	 * 
	 * @param name The name of the parameter
	 * @param value The value of the parameter
	 * @return
	 *
	 */
	public UrlBuilder appendParameter(String name, String value) {
		if (value == null) {
			return this;
		}
		
		char paramChar = '&';
		
		if (isFirstParam) {
			isFirstParam = false;
			paramChar = '?';
		}
		
		try {
			builder
				.append(paramChar)
				.append(URLEncoder.encode(name, "UTF-8"))
				.append('=')
				.append(URLEncoder.encode(value, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Unable to append url parameter", e);
		}
		
		return this;
	}
}
