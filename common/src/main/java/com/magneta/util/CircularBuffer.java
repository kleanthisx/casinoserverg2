/**
 * 
 */
package com.magneta.util;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;


/**
 * @author anarxia
 *
 * Circular buffer of fixed size. 
 * Elements cannot be removed from the buffer and each element added over
 * the capacity removes the oldest element.
 * 
 * Elements may be replaced.
 */
public class CircularBuffer<E extends Object> implements List<E>, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7509724786724610809L;
	private E[] buffer;
    private int head;
    private int tail;
    private int numElements;
    
    @SuppressWarnings("unchecked")
    public CircularBuffer(Class<E> klass, int size) {
        buffer = (E[])Array.newInstance(klass, size);
        numElements = 0;
        head = 0;
        tail = 0;
    }
    
    @Override
    public boolean add(E toAdd) {
        buffer[tail] = toAdd;

        tail++;

        if (tail == buffer.length) {
            tail = 0;
        } 
        
        if (numElements < buffer.length) {
            numElements++;
        } else {
            head++;
            
            if (head >= buffer.length) {
                head = 0;
            }
        }
        
        return true;
    }
    
    private int getArrayIndex(int logicalIndex) {
    	 return (head + logicalIndex) % buffer.length;
    }

    @Override
    public E get(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= numElements) {
            throw new IndexOutOfBoundsException();
        }
        
        int arrayIndex = getArrayIndex(index);
        
        return buffer[arrayIndex];
    }
    
    @Override
    public int size() {
        return numElements;
    }
    
    public int capacity() {
        return buffer.length;
    }

    @Override
    public Iterator<E> iterator() {
        return new BufferIterator();
    }

    /**
     * Unsupported operation
     */
    @Override
    public void add(int index, E element) {
        throw new UnsupportedOperationException(); 
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
    	
    	if (c == null || c.isEmpty()) {
    		return false;
    	}
    	
    	for (E e: c) {
    		add(e);
    	}
    	
    	return true;
    }

    /** Unsupported operation
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        numElements = 0;
        head = 0;
        tail = 0;
        
        for (int i=0; i < buffer.length; i++) {
            buffer[i] = null;
        }
    }

    @Override
    public boolean contains(Object o) {
        for (int i=0; i < numElements; i++) {
            
            int arrayIndex = (head + i) % buffer.length;
            
            if (o == buffer[arrayIndex]) {
                return true;
            }
            
            if (o == null) {
            	continue;
            }
            
            if (o.equals(buffer[arrayIndex])) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> col) {
        for (Object o: col) {
            if (!contains(o))
                return false;
        }
        return true;
    }

    @Override
    public int indexOf(Object o) {
        for (int i=0; i < numElements; i++) {
            
            int arrayIndex = (head + i) % buffer.length;
            
            if (o==null ? buffer[arrayIndex]==null : o.equals(buffer[arrayIndex])) {
                return arrayIndex;
            }
        }
        
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return numElements == 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        int lastIndex = -1;
        for (int i=0; i < numElements; i++) {
            
            int arrayIndex = (head + i) % buffer.length;
            
            if (o==null ? buffer[arrayIndex]==null : o.equals(buffer[arrayIndex])) {
                lastIndex = arrayIndex;
            }
        }
        
        return lastIndex;
    }

    @Override
    public ListIterator<E> listIterator() {
        return new BufferIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return new BufferIterator(index);
    }

    /**
     * Unsupported operation
     */
    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    /**
     * Unsupported operation
     */
    @Override
    public E remove(int index) {
        throw new UnsupportedOperationException();
    }

    /**
     * Unsupported operation
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    /**
     * Unsupported operation
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E set(int index, E element) {
    	if (index < 0 || index >= numElements) { 
    		throw new IndexOutOfBoundsException();
    	}

    	int arrayIndex = getArrayIndex(index);
    	buffer[arrayIndex] = element;
    	
    	return element;
    }

    /**
     * Unsupported operation
     */
    @Override
    public List<E> subList(int startIndex, int endIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[numElements];
        int arrIndex=0;
        
        for(int i=head; i < buffer.length; i++) {
            arr[arrIndex] = buffer[i];
            arrIndex++;
        }
        
        for (int i=0; i < head; i++) {
            arr[arrIndex] = buffer[i];
            arrIndex++;
        }
        
        return arr;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] arrModel) {
        int arrIndex=0;
        
        if (arrModel == null) {
            throw new NullPointerException();
        }
        
        if (arrModel.length >= numElements) {
            for(int i=head; i < buffer.length && arrIndex < numElements; i++) {
                arrModel[arrIndex] = (T)buffer[i];
                arrIndex++;
            }
            
            for (int i=0; i < head && arrIndex < numElements; i++) {
                arrModel[arrIndex] = (T)buffer[i];
                arrIndex++;
            }
            
            /*
             * null terminate the array as per documentation
             */
            if (arrIndex < arrModel.length -1) {
                arrModel[arrIndex] = null;
            }
            
            return arrModel;
        }
        
        T[] newArr = (T[])Array.newInstance(arrModel.getClass().getComponentType(), numElements);
        for(int i=head; i < buffer.length && arrIndex < numElements; i++) {
            newArr[arrIndex] = (T)buffer[i];
            arrIndex++;
        }
        
        for (int i=0; i < head && arrIndex < numElements; i++) {
            newArr[arrIndex] = (T)buffer[i];
            arrIndex++;
        }
        
        /*
         * null terminate the array as per documentation
         */
        if (arrIndex < arrModel.length -1) {
            newArr[arrIndex] = null;
        }
        
        return newArr;
    }
    
    private class BufferIterator implements ListIterator<E> {
        private int pos;

        public BufferIterator() {
            pos = 0;
        }

        public BufferIterator(int pos) {
            this.pos = pos;
        }

        @Override
        public boolean hasNext() {
            return pos < numElements;
        }

        @Override
        public E next() {
            if (pos >= numElements)
                throw new NoSuchElementException();

            E e = get(pos);
            pos++;

            return e;
        }

        /**
         * Unsupported operation
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        /** Unsupported oprtation
         * @see java.util.ListIterator#add(java.lang.Object)
         */
        @Override
        public void add(E e) {
            throw new UnsupportedOperationException();

        }

        @Override
        public boolean hasPrevious() {
            return (pos - 1) >= 0;
        }

        @Override
        public int nextIndex() {
            return pos+1 > numElements?numElements: pos+1;
        }

        @Override
        public E previous() {
            if ((pos-1) >= 0 && (pos-1) < numElements) {
                return get(pos-1);
            }  
            throw new NoSuchElementException();
        }

        @Override
        public int previousIndex() {
            return pos - 1 > -1?pos-1:-1;
        }

        @Override
        public void set(E e) {
        	if (pos <= 0 || pos > numElements) { 
        		throw new IllegalStateException();
        	}

        	int arrayIndex = getArrayIndex(pos -1);
        	buffer[arrayIndex] = e;
        }
    }
}
