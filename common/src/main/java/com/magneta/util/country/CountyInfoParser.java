package com.magneta.util.country;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parses geonames.org countryInfo.txt file.
 * 
 * The latest version of the file can be found at:
 * http://download.geonames.org/export/dump/countryInfo.txt
 * @author anarxia
 *
 */
public class CountyInfoParser {
	
	public static List<Map<String,String>> parse(InputStream is) throws IOException { 
		BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

		List<Map<String,String>> countryList = new ArrayList<Map<String,String>>(256);
		
		String[] columns = null;
		
		String line;
		while ((line = reader.readLine()) != null) {
			/* Header line. Parse column names */
			if (line.startsWith("#ISO")) {
				columns = line.substring(1).split("\t");
				continue;
			}
			
			/* Skip comments */
			if (line.startsWith("#")) {
				continue;
			}
			
			if (columns == null) {
				throw new RuntimeException("Found data but header line not found");
			}
			
			String[] values = line.split("\t");
			
			Map<String,String> country = new HashMap<String,String>();
			
			for (int i=0; i < values.length; i++) {
				country.put(columns[i], values[i]);
			}
			
			countryList.add(country);
		}
		
		return countryList;
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		List<Map<String,String>> countryList = CountyInfoParser.parse(new FileInputStream("/home/anarxia/src/countryInfo.txt"));
		
		for (Map<String,String> country: countryList) {
			System.out.println(
					"INSERT INTO countries (country_code, country_name, accept)" + 
					"VALUES ('" + country.get("ISO") + "', '"+country.get("Country") + "', true);");
			
			//System.out.println("countryMap.put(\"" + country.get("ISO") + "\",\"" + country.get("ISO3") + "\");");
		}
		
	}
}
