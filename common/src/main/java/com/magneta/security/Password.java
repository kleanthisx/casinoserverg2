package com.magneta.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Class Password provides the essential utility methods to encode and decode passwords.
 */
public class Password {

	private static final char[] SALT_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
	private static final int SALT_LENGTH = 8;
	
	private static final String HASH_ALGO = "SHA";
	private static final String HASH_ENCODING = "UTF-8";
	
	private Password() {
		super();
	}
	
    /**
     * Returns a random salt sequence to use for password decoding.
     * @return A random salt sequence to use for password decoding.
     */
	public static String getSalt() {
		Random rnd = new Random();
		
		StringBuilder salt = new StringBuilder();
		
		 for (int i = 0; i < SALT_LENGTH; i++)
	     {
			 int index = (int) (rnd.nextFloat() * SALT_CHARS.length);
	         salt.append(SALT_CHARS[index]);
	     }
		
		return salt.toString();
	}
	
	/**
     * Gets the encoded password. 
     * @param password The password to encode.
     * @param salt The salt to use.
     * @return The encoded password.
	 */
	public static String getPasswordHash(String password, String salt) {
		String passSalt = salt + password;
		
		MessageDigest md = null;
		
		try {
			md = MessageDigest.getInstance(HASH_ALGO);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Error creating Digest algorithm", e);
		}
		
		try {
			md.update(passSalt.getBytes(HASH_ENCODING));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Error creating hash encoding", e);
		}
		
		byte[] bytes = md.digest();
		return toHexString(bytes);
	}
	
	
	private static final char[] hexChar = {'0','1','2','3','4','5','6','7','8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	
    /**
     * Converts the give byte array into a hexadecimal String. 
     * @param bytes The byte array  to convert
     * @return The hexadecimal String created.
     */
	public static String toHexString(byte[] bytes) {
		
		StringBuilder buff = new StringBuilder();
		
		for (int i = 0; i < bytes.length; i++)
		{
			buff.append(hexChar[(bytes[i] >> 4) & 0x0F]);
			buff.append(hexChar[(bytes[i] & 0x0F)]);
		}
		
		return buff.toString();
	}
	
}
