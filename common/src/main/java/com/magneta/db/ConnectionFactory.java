package com.magneta.db;

import java.sql.Connection;

public final class ConnectionFactory {
	
	private static final ConnectionFactoryImpl connectionFactory;
	
	static {
		connectionFactory = new ConnectionFactoryImpl("db");
	}

    /**
     * Gets a new connection
     * @return null if the connection failed.
     */
	public static Connection getConnection() {
		return connectionFactory.getConnection(false);
	}

	/**
	 * Gets a new reports DB connection
	 * @return null if the connection failed.
	 */
	public static Connection getReportsConnection() {
	    return connectionFactory.getConnection(true);
	}
}