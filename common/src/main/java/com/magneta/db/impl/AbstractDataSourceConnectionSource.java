package com.magneta.db.impl;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionSource;

public abstract class AbstractDataSourceConnectionSource implements ConnectionSource {
	
	private static final Logger log = LoggerFactory.getLogger(AbstractDataSourceConnectionSource.class);

	protected abstract DataSource createDataSource(String prefix);
	
	private final DataSource ds;

	public AbstractDataSourceConnectionSource(String prefix) {
		ds = createDataSource(prefix);
	}

	/**
	 * Gets a new connection
	 * @return null if the connection failed.
	 */
	@Override
	public Connection getConnection(boolean readOnly) {
		try {
			Connection conn = ds.getConnection();
			if (conn == null) {
				return null;
			}
			try {
				conn.setReadOnly(readOnly);
			} catch (SQLException e) {
				log.error("Error setting read-only mode on db connection.", e);
				conn.close();
				return null;
			}
			return conn;
		} catch (Exception e) {
			log.error("Error getting DB connection: " + e.getMessage());
			return null;
		}
	}
}
