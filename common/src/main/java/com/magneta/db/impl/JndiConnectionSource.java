package com.magneta.db.impl;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.magneta.Config;

public class JndiConnectionSource extends AbstractDataSourceConnectionSource {

	public JndiConnectionSource(String prefix) {
		super(prefix);
	}

	@Override
	protected DataSource createDataSource(String prefix) {
		String jndi = Config.get(prefix + ".jndi");
		InitialContext context;

		try {
			context = new InitialContext();
			return (DataSource) context.lookup(jndi);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	

}
