package com.magneta.db.impl;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import com.magneta.Config;

public class DbCpConnectionSource extends AbstractDataSourceConnectionSource {

	@Override
	protected DataSource createDataSource(String prefix) {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUsername(Config.get(prefix + ".username"));
		dataSource.setPassword(Config.get(prefix + ".password"));
		dataSource.setUrl(Config.get(prefix + ".uri"));

		/* Set Limits */
		dataSource.setMaxActive(Config.getInt(prefix + ".max_active", -1));
		dataSource.setMinIdle(Config.getInt(prefix + ".min_idle",2));
		dataSource.setMaxIdle(Config.getInt(prefix + ".max_idle",10));
		
		return dataSource;
	}

	public DbCpConnectionSource(String prefix) {
		super(prefix);
	}
}
