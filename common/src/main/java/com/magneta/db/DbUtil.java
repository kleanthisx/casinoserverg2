package com.magneta.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DbUtil contains a set of methods useful for database connectivity. 
 */
public class DbUtil {
	
	private static final Logger log = LoggerFactory.getLogger(DbUtil.class);

    private DbUtil() {
        super();
    }

    /**
     * Closes the given connection.
     * It logs any possible warnings and catches all
     * exceptions.
     * 
     * @param conn The connection to close.
     */
    public static final void close(Connection conn) {
        if (conn == null) {
            return;
        }
        try {
            if (log.isDebugEnabled()) {
                SQLWarning warning = conn.getWarnings();
                while (warning != null) {
                    log.debug("Connection warning:", warning);
                    warning = warning.getNextWarning();
                }
            }
            conn.close();
        } catch (Exception ex) {
            log.debug("Error while closing connection.", ex);
        }
    }

    /**
     * Closes the given resultSet.
     * It logs any possible warnings and catches all
     * exceptions.
     * 
     * @param conn The resultSet to close.
     */
    public static final void close(ResultSet rs) {
        if (rs == null) {
            return;
        }
        try {
            if (log.isDebugEnabled()) {
                SQLWarning warning = rs.getWarnings();
                while (warning != null) {
                    log.debug("ResultSet warning:", warning);
                    warning = warning.getNextWarning();
                }
            }
            rs.close();
        } catch (Exception ex) {
            log.debug("Error while closing resultSet.", ex);
        }
    }
    
    /**
     * Closes the given statement.
     * It logs any possible warnings and catches all
     * exceptions.
     * 
     * @param conn The statement to close.
     */
    public static final void close(Statement statement) {
        if (statement == null) {
            return;
        }
        try {
            if (log.isDebugEnabled()) {
                SQLWarning warning = statement.getWarnings();
                while (warning != null) {
                    log.debug("Statement warning:", warning);
                    warning = warning.getNextWarning();
                }
            }
            statement.close();
        } catch (Exception ex) {
            log.debug("Error while closing statement.", ex);
        }
    }
    
    /**
     * Coverts the given javax.sql.Date to java.util.Date
     * @param sqlDate The java.sql.Date to convert.
     */
    public static final java.util.Date getDate(java.sql.Date sqlDate) {
        if (sqlDate == null)
            return null;
        
        return new java.util.Date(sqlDate.getTime());
    }
    

    public static java.util.Date getDate(java.sql.Timestamp timeStamp) {
    	if (timeStamp == null) {
    		return null;
    	}
    	
        long milliseconds = timeStamp.getTime() + (timeStamp.getNanos() / 1000000);
        return new java.util.Date(milliseconds);
    }
    public static final Timestamp getTimestamp(ResultSet rs, String column) throws SQLException {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
        return rs.getTimestamp(column, cal);
    }
    
    public static final Timestamp getTimestamp(ResultSet rs, int column) throws SQLException {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
        return rs.getTimestamp(column, cal);
    }
    
    public static final void setTimestamp(PreparedStatement stmt, int column, Timestamp timestamp) throws SQLException {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
        stmt.setTimestamp(column, timestamp, cal);
    }
    
    public static final void setTimestamp(PreparedStatement stmt, int column, java.util.Date date) throws SQLException {
        if (date == null) {
            stmt.setObject(column, null);
        } else {
            Timestamp timestamp = new Timestamp(date.getTime());
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Universal"));
            stmt.setTimestamp(column, timestamp, cal);
        }
    }
    
    
    public static final Double getDouble(ResultSet rs, String column) throws SQLException {
        Double value = rs.getDouble(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Double getDouble(ResultSet rs, int column) throws SQLException {
        Double value = rs.getDouble(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Long getLong(ResultSet rs, String column) throws SQLException {
        Long value = rs.getLong(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Long getLong(ResultSet rs, int column) throws SQLException {
        Long value = rs.getLong(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Integer getInteger(ResultSet rs, String column) throws SQLException {
        Integer value = rs.getInt(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Integer getInteger(ResultSet rs, int column) throws SQLException {
        Integer value = rs.getInt(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Boolean getBoolean(ResultSet rs, String column) throws SQLException {
        Boolean value = rs.getBoolean(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final Boolean getBoolean(ResultSet rs, int column) throws SQLException {
        Boolean value = rs.getBoolean(column);
        if(rs.wasNull()) {
            value = null;
        }
        return value;
    }
    
    public static final void setBoolean(PreparedStatement stmt, int column, Boolean value) throws SQLException {
    	if (value == null) {
    		stmt.setObject(column, null);
    	} else {
    		stmt.setBoolean(column, value);
    	}
    }
    
    public static final void setInteger(PreparedStatement stmt, int column, Integer value) throws SQLException {
    	if (value == null) {
    		stmt.setObject(column, null);
    	} else {
    		stmt.setInt(column, value);
    	}
    }
    
    public static final void setLong(PreparedStatement stmt, int column, Long value) throws SQLException {
    	if (value == null) {
    		stmt.setObject(column, null);
    	} else {
    		stmt.setLong(column, value);
    	}
    }
    
    public static final void setDouble(PreparedStatement stmt, int column, Double value) throws SQLException {
    	if (value == null) {
    		stmt.setObject(column, null);
    	} else {
    		stmt.setDouble(column, value);
    	}
    }
}
