package com.magneta.db;

import java.sql.Connection;

public interface ConnectionSource {
	
	/**
	 * Gets a new connection
	 * @return null if the connection failed.
	 */
	Connection getConnection(boolean readOnly);
}
