package com.magneta.db;

import java.sql.Connection;

import com.magneta.Config;
import com.magneta.db.impl.DbCpConnectionSource;
import com.magneta.db.impl.JndiConnectionSource;

public class ConnectionFactoryImpl {
	
	private static final String DEFAULT_POOL = "dbcp";
	private final ConnectionSource ds;
	
	private ConnectionSource createDataSource(String prefix) {
		String jndi = Config.get(prefix + ".jndi");
		if (jndi != null) {
			return new JndiConnectionSource(prefix);
		}
		
		String implementation = Config.get(prefix + ".pool", DEFAULT_POOL);
		
		if ("dbcp".equalsIgnoreCase(implementation)) {
			return new DbCpConnectionSource(prefix);
		}  
		
		throw new RuntimeException("Unknown connection pool implementation " + implementation);	
	}

	public ConnectionFactoryImpl(String prefix) {		
		ds = createDataSource(prefix);
	}

	/**
	 * Gets a new connection
	 * @return null if the connection failed.
	 */
	public Connection getConnection(boolean readOnly) {
		return ds.getConnection(readOnly);
	}
}
