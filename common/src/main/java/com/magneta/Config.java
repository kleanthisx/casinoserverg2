package com.magneta;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Read-only configuration class. All methods are static since
 * we only have one configuration file.
 *
 * @author Nicos Panayides
 */
public class Config {
	
	private static final Logger log = LoggerFactory.getLogger(Config.class);
	
	private static final String propertyFile = "server.properties";
	private static Properties res;

	static {
        try {
            res = new Properties();
            load();
           
        } catch (MissingResourceException e) {
            throw new java.lang.RuntimeException("Property file \"" + propertyFile + "\" not found in the classpath.");
        } catch (IOException e) {
            throw new java.lang.RuntimeException("Unable to load properties from file \"" + propertyFile + "\"", e);
        }
	}

	private Config() {
		super();
	}
	
	private static void load() throws IOException {
	    InputStream is = Config.class.getClassLoader().getResourceAsStream(propertyFile);
        if (is == null) {
            throw new java.lang.RuntimeException("Property file \"" + propertyFile + "\" not found in the classpath.");
        }
        
        InputStreamReader r;
        try {
            r = new InputStreamReader(is, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            r = new InputStreamReader(is);
        } 
        
        res.clear();
        res.load(r);
        
        r.close();
        is.close();
	}
	
	public static void reload() {
	    synchronized(res) {
	        try {
                load();
            } catch (IOException e) {
                log.error("Error while reloading properties", e);
            }
	    }
	}
	
	public static Set<String> getKeys() {
	     return res.stringPropertyNames();
	}
	
	/**
	* Returns the value of the given key as a string.
	*/
	public static String get(String key) {
		return get(key,null);
	}

	public static String get(String key, String def) {
	    return res.getProperty(key, def);
	}
	
	/**
	* Returns the value of the given key as a long.
	*/	
	public static long getLong(String key, long def) {
	    long val = def;

	    String stringValue = res.getProperty(key);
	    if (stringValue == null) {
	        val = def;
	    } else {
	        val = Long.parseLong(stringValue);
	    }

	    return val;
	}

	/**
	* Returns the value of the given key as an int.
	*/	
	public static int getInt(String key, int def) {
	    int val = def;

	    String stringValue = res.getProperty(key);
	    if (stringValue == null) {
	        val = def;
	    } else {
	        val = Integer.parseInt(stringValue);
	    }

	    return val;
	}
    
	public static boolean getBoolean(String key, boolean def){
	    boolean val = def;

	    String stringValue = res.getProperty(key);
	    if (stringValue == null) {
	        val = def;
	    } else {
	        val = Boolean.parseBoolean(stringValue);
	    }

	    return val;
	}

	public static double getDouble(String key, double def) {
	    double val = def;


	    String stringValue = res.getProperty(key);
	    if (stringValue == null) {
	        val = def;
	    } else {
	        val = Double.parseDouble(stringValue);
	    }

	    return val;
	}
}
