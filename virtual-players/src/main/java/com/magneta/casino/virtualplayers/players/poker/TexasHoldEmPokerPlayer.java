package com.magneta.casino.virtualplayers.players.poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;


public class TexasHoldEmPokerPlayer extends AbstractVirtualPlayer {
	
	private static enum TexasGameState {	
		INITIAL,
		DEAL,
		FLOP,
		TURN,
		RIVER,
		FINISHED;
	}

	private static final int PLAYER_SEAT = 1;
	private static final int DEALER_SEAT = 0;
	private static final int COMMON_SEAT = 2;

	private double bet;
	private double sideBetAmount;
	private RoundState state;

	private static class RoundState {
		private TexasGameState gameState=TexasGameState.INITIAL;
		private Double playerBet;
		private Double sideBet;
		private List<Integer> playerCards = new ArrayList<Integer>();
		private List<Integer> dealerCards = new ArrayList<Integer>();
		private List<Integer> commonCards = new ArrayList<Integer>();
	}

	public TexasHoldEmPokerPlayer(ClientInterface client, int gameId) {
		super(client, gameId);
		this.bet = 1.0;
		this.sideBetAmount = 0.0;
//		this.strategy = new Strategy();
		this.state = new RoundState();

	}

	@Setting(description="Side Bet")
	public void setSideBet(double sideBet) {
		this.sideBetAmount = sideBet;
	}

	@Override
	protected final void roundFinished(Map<String, Object> status) {
		this.state = new RoundState();
	}

	@Override
	protected final void onActionReceived(GameAction action) throws XmlRpcException {
		String actionDesc = action.getDescription();           
		if (actionDesc.equals("BET")) {
			state.playerBet = action.getAmount();
			state.gameState = TexasGameState.DEAL;
		} else if (actionDesc.equals("g:SIDE_BET")) {
			state.sideBet = action.getAmount();
		}
		else if (actionDesc.equals("g:RAISE")) {	
			state.gameState = TexasGameState.FLOP;
		} else if (actionDesc.equals("g:TURN")) {
			state.gameState = TexasGameState.TURN;
		}else if (actionDesc.equals("g:RIVER")) {
			state.gameState = TexasGameState.FINISHED;
		}else if (actionDesc.equals("g:FOLD")) {
			state.gameState = TexasGameState.FINISHED;
		}
		else if (actionDesc.equals("g:HIT")) {
			switch (action.getSeatId()) {
			case PLAYER_SEAT:
				addcardsToHand(state.playerCards,action);
				break;
			case DEALER_SEAT:
				addcardsToHand(state.dealerCards,action);
				break;
			case COMMON_SEAT:
				addcardsToHand(state.commonCards,action);
				break;
			default:
				throw new XmlRpcException("Received unknown seat " + action.getSeatId() + " from server");
			}	
		}

	}

	private void addcardsToHand(List<Integer> cardHand,GameAction action) {
		
		String[] sCards = action.getValue().split(" ");
		
		for (int i=0; i < sCards.length; i++) {
			cardHand.add(Integer.valueOf(sCards[i]));
		}
		
		Collections.sort(cardHand,new MyIntComparable());

	}			

	private boolean shouldRaise() {
		int cardColour1 = Card.getCardColour(state.playerCards.get(0));
		int cardColour2 = Card.getCardColour(state.playerCards.get(1));
		boolean suited = cardColour1==cardColour2;
		int min = Math.min(Card.getCardValue(state.playerCards.get(0)), Card.getCardValue(state.playerCards.get(1)));
		int max = Math.max(Card.getCardValue(state.playerCards.get(0)), Card.getCardValue(state.playerCards.get(1)));

		if(!suited && min >1  && max < 7){
			return false;
		}

		return true;
	}

	private boolean shouldturn() {
		String ph = Card.toString(state.playerCards);
		String ch = Card.toString(state.commonCards);
		
		String[] sa= TexasReqUtil.requestProbabs(ph, ch);
		double played= Double.parseDouble(sa[0]);
		double won= Double.parseDouble(sa[1]);
		double lost= Double.parseDouble(sa[2]);
//		double tied = Double.parseDouble(sa[3]);
		
//		double win = won/played;
//		double loose = lost/played;
//		double tie = tied/played;
		
		double ev = (won-lost) / played;
		
		if (ev > 0)
			return true;
		
		return false;
	}

	private boolean shouldRiver() {
		String ph = Card.toString(state.playerCards);
		String ch = Card.toString(state.commonCards);
		
		String[] sa = TexasReqUtil.requestProbabs(ph, ch);
		double played= Double.parseDouble(sa[0]);
		double won= Double.parseDouble(sa[1]);
		double lost= Double.parseDouble(sa[2]);
//		double tied = Double.parseDouble(sa[3]);
		
//		double win = won/played;
//		double loose = lost/played;
//		double tie = tied/played;
		
		double ev = (won-lost) / played;
		
		if (ev > 0)
			return true;
		
		return false;
		
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()) {
			switch (this.state.gameState) {
			case INITIAL:
				if (sideBetAmount > 0.001 && state.sideBet == null) {
					sideBet();
				} else if (state.playerBet == null) {
					bet();
				}
				break;
			case DEAL:
				if (shouldRaise()) {
					raise();
				} else {
					fold();
				}
				break;
			case FLOP:
				turn();
				break;
			case TURN:
				river();
				break;

			}
		}
	}

	private void river() throws XmlRpcException {
		if(shouldRiver()){
			doAction(1, "g:RIVER", "1", 0.0);
		}else{
			doAction(1, "g:RIVER", "0", 0.0);
		}
	}

	private void turn() throws XmlRpcException {
		if (shouldturn()){
			doAction(1, "g:TURN", "1", 0.0);
		}else{
			doAction(1, "g:TURN", "0", 0.0);
		}
	}

	private void bet() throws XmlRpcException {
		if (getBalance() >= bet) {
			doAction(1, "BET", null, bet);
		} else {
			onInfo("Out of money. Stopping");
			stop();
		}
	}

	private void fold() throws XmlRpcException{
		doAction(1, "g:FOLD", null, 0.0);
	}

	protected void raise() throws XmlRpcException {
		if (getBalance() >= state.playerBet * 2.0) {
			doAction(1, "g:RAISE", null, 0.0);
		} else {
			onInfo("Out of money. Stopping");			
			stop();
		}
	}

	protected void sideBet() throws XmlRpcException {
		if (getBalance() >= sideBetAmount) {
			doAction(1, "g:SIDE_BET", null, sideBetAmount);
		} else {
			onInfo("Out of money. Stopping");
			stop();
		}
	}

	public static class Card {

		/**
		 * The constant value used for the clubs card type.
		 */
		public static final int CLUBS_CARD = 0;
		/**
		 * The constant value used for the diamonds card type.
		 */
		public static final int DIAMONDS_CARD = 1;
		/**
		 * The constant value used for the hearts card type.
		 */
		public static final int HEARTS_CARD = 2;
		/**
		 * The constant value used for the spades card type.
		 */
		public static final int SPADES_CARD = 3;
		/**
		 * The constant value used for cards with not type (i.e. Joker).
		 */
		public static final int NO_TYPE_CARD = 4;

		/**
		 * The constant value used for the ace card.
		 */
		public static final int ACE = 1;
		/**
		 * The constant value used for the jack card.
		 */
		public static final int JACK = 11;
		/**
		 * The constant value used for the queen card.
		 */
		public static final int QUEEN = 12;
		/**
		 * The constant value used for the king card.
		 */
		public static final int KING = 13;
		/**
		 * The constant value used for the joker card.
		 */
		public static final int JOKER = 14;

		public static final int MAX_CARD = JOKER;

		/**
		 * The constant value used for a red card.
		 */
		public static final int RED_CARD = 100;

		/**
		 * The constant value used for a black card.
		 */
		public static final int BLACK_CARD = 200;

		/**
		 * Returns the race of the card.
		 * @param card The card whose race is requested.
		 */
		public static int getCardRace(int card) {
			  return (card >> 4) & 0x0F;
		}

		/**
		 * Returns the card number.
		 * @param card The card whose number is requested.
		 */
		public static int getCardNumber(int card) {
			return (card) & 0x0F;
		}

		public static int getCardColour(int card){
			int race = getCardRace(card);

			if ((race == HEARTS_CARD) || (race == DIAMONDS_CARD)){
				return RED_CARD;
			} else if ((race == CLUBS_CARD) || (race == SPADES_CARD)){
				return BLACK_CARD;
			} else {
				return -1;
			}
		}

		public static int getCard(int value, int race){
			return (race << 4) | value;
		}

		/**
		 * Returns the card value.
		 * @param card The card whose valu is requested.
		 * Ace is above King
		 */
		public static int getCardValue(int card) {
			return getCardNumber(card)== 1 ? getCardNumber(card)+13 : getCardNumber(card); 
		}

		private static String toString(int card){
			String result = "";

			switch (getCardNumber(card)) {
			case 10:
				result="t";
				break;
			case 11:
				result="j";
				break;
			case 12:
				result="q";
				break;
			case 13:
				result="k";
				break;
			case 1:
				result="a";
				break;
			default:
				result+=Integer.toString(getCardNumber(card));
			}

			switch (getCardRace(card)) {
			case 0:
				result+="c";
				break;
			case 1:
				result+="d";
				break;
			case 2:
				result+="h";
				break;
			case 3:
				result+="s";
				break;
			}
			return result;
		}
		
		public static String toString(List<Integer> cards){
			String res = "";
			boolean isFirst = true;
			for(int i : cards){
				if(isFirst){
					isFirst = false;
				}else{
					res+=" ";
				}
				res+=Card.toString(i);
			}
			return res;
		}
	}
	
	public class MyIntComparable implements Comparator<Integer>{
		 
	    @Override
	    public int compare(Integer o1, Integer o2) {
	        return (o1>o2 ? -1 : (o1==o2 ? 0 : 1));
	    }
	}
}