package com.magneta.casino.virtualplayers.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.magneta.casino.virtualplayers.players.Profile;
import com.magneta.casino.virtualplayers.players.ProfileLoader;
import com.magneta.casino.virtualplayers.players.ProfileSetting;
import com.magneta.casino.virtualplayers.players.Server;
import com.magneta.casino.virtualplayers.players.ServerLoader;

public class VirtualPlayerFrame extends JFrame implements ListSelectionListener, ActionListener {

	private static final long serialVersionUID = 1L;

	private JList<Profile> profilesList;
	private JLabel descLbl;
	private JComboBox<Server> serversCombo;
	private JPanel mainPanel;
	private JButton runBtn;
	private JTabbedPane tabs;
	private JTextField usernameTxt;
	private JPasswordField passTxt;
	private List<JComponent> extraComponents;
	private final ProfileLoader profileLoader;

	public VirtualPlayerFrame() {
		super("Virtual Players");

		Server[] servers = new ServerLoader().loadServers();
		profileLoader = new ProfileLoader();

		Profile[] profiles = profileLoader.loadProfiles();

		Container container = this.getContentPane();

		tabs = new JTabbedPane();
		JPanel tabPanel = new JPanel();
		tabPanel.setLayout(new BorderLayout());
		if (profiles != null){
			profilesList = new JList<Profile>(profiles);
		} else {
			profilesList = new JList<Profile>();
		}
		if (servers != null){
			serversCombo = new JComboBox<Server>(servers);
		} else {
			serversCombo = new JComboBox<Server>();
		}
		descLbl = new JLabel("");
		JScrollPane scroller = new JScrollPane(profilesList);
		profilesList.addListSelectionListener(this);

		mainPanel = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		mainPanel.setLayout(gridbag);

		JLabel servLbl = new JLabel("Server");
		JLabel usernameLbl = new JLabel("Username");
		JLabel passLbl = new JLabel("Password");

		usernameTxt = new JTextField(10);
		passTxt = new JPasswordField(10);

		c.fill = GridBagConstraints.NONE;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.insets = new Insets(2,2,2,2);
		gridbag.setConstraints(descLbl, c);
		mainPanel.add(descLbl);

		c.gridwidth = GridBagConstraints.RELATIVE;
		gridbag.setConstraints(servLbl, c);
		mainPanel.add(servLbl);

		c.gridwidth = GridBagConstraints.REMAINDER;
		gridbag.setConstraints(serversCombo, c);
		mainPanel.add(serversCombo);

		c.gridwidth = GridBagConstraints.RELATIVE;
		gridbag.setConstraints(usernameLbl, c);
		mainPanel.add(usernameLbl);

		c.gridwidth = GridBagConstraints.REMAINDER;
		gridbag.setConstraints(usernameTxt, c);
		mainPanel.add(usernameTxt);

		c.gridwidth = GridBagConstraints.RELATIVE;
		gridbag.setConstraints(passLbl, c);
		mainPanel.add(passLbl);

		c.gridwidth = GridBagConstraints.REMAINDER;
		gridbag.setConstraints(passTxt, c);
		mainPanel.add(passTxt);

		extraComponents = new ArrayList<JComponent>();

		runBtn = new JButton("Run");
		runBtn.setEnabled(false);
		runBtn.addActionListener(this);

		tabPanel.add(scroller, BorderLayout.WEST);
		tabPanel.add(mainPanel, BorderLayout.CENTER);
		tabs.add("Main",tabPanel);

		tabs.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_ENTER){
					actionPerformed(new ActionEvent(runBtn, 0, ""));
				}
			}
		});

		container.add(tabs);
		profilesList.setSelectedIndex(0);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == this.profilesList){
			Profile p = profilesList.getSelectedValue();
			if (p != null){
				this.descLbl.setText(p.getDesc());
				displayProfileSettings(p);
			}
		}
	}

	private JComponent createEditComponent(ProfileSetting set) {
		JComponent c;

		if (Boolean.TYPE.equals(set.getType())){
			JCheckBox check = new JCheckBox();
			check.setSelected((Boolean)set.getDef());
			c = check;
		} else if (set.getType().isEnum()) {
			Object[] values = set.getType().getEnumConstants();

			JComboBox<Object> comboBox = new JComboBox<Object>();

			for (Object value: values) {
				comboBox.addItem(value);
			}

			c = comboBox;
		} else {
			String val = null;
			if (set.getDef() != null){
				val = String.valueOf(set.getDef());
			}
			JTextField field = new JTextField(val,20);
			c = field;
		}

		c.setName(set.getName());
		return c;
	}

	public void displayProfileSettings(Profile profile) {
		Map<String, ProfileSetting> settings = profile.getSettings();
		for (JComponent c: extraComponents) {
			mainPanel.remove(c);
		}
		extraComponents.clear();

		GridBagLayout gridbag = (GridBagLayout)mainPanel.getLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(2,2,2,2);

		Iterator<String> it = settings.keySet().iterator();

		while(it.hasNext()){
			ProfileSetting set = settings.get(it.next());
			if (!set.isPrompt()) {
				continue;
			}

			String lbl = (set.getDesc() != null? set.getDesc() : set.getName());
			JLabel label = new JLabel(lbl);
			extraComponents.add(label);
			c.gridwidth = GridBagConstraints.RELATIVE;
			gridbag.setConstraints(label, c);
			mainPanel.add(label);

			JComponent editComponent = createEditComponent(set);

			c.gridwidth = GridBagConstraints.REMAINDER;
			gridbag.setConstraints(editComponent, c);
			mainPanel.add(editComponent);
			extraComponents.add(editComponent);

		}
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridbag.setConstraints(runBtn, c);
		mainPanel.add(runBtn);
		extraComponents.add(runBtn);
		runBtn.setEnabled(true);
		mainPanel.updateUI();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		StringBuilder errors = new StringBuilder("The following errors have been found:\n");
		boolean error = false;

		if (usernameTxt.getText().length() == 0){
			errors.append(" - Username is blank.\n");
			error = true;
		}

		if (passTxt.getPassword().length == 0){
			errors.append(" - Password is blank.\n");
			error = true;
		}

		Profile p = profilesList.getSelectedValue();
		Server s = (Server)serversCombo.getSelectedItem();
		
		Map<String, Object> settings = new HashMap<String, Object>();

		for (JComponent c: extraComponents) {
			if (c instanceof JTextField) {
				JTextField tf = (JTextField)c;
				ProfileSetting ps = p.getSettings().get(tf.getName());
				if (tf.getText().length() == 0){
					errors.append(" - "+ps.getDesc()+" is blank.\n");
					error = true;
				}

				if (Integer.TYPE.equals(ps.getType()) || Long.TYPE.equals(ps.getType())){
					if (!tf.getText().matches("^\\d+$")){
						errors.append(" - Value entered in "+ps.getDesc()+" is not numeric.\n");
						error = true;
					} else {
						settings.put(ps.getName(), parseValue(tf.getText(),ps.getType()));
					}
				} else if (Double.TYPE.equals(ps.getType())){
					if (!tf.getText().matches("^\\d+(\\.\\d{1,2})?$")){
						errors.append(" - Value entered in "+ps.getDesc()+" is not numeric.\n");
						error = true;
					} else {
						settings.put(ps.getName(), parseValue(tf.getText(),ps.getType()));
					}
				} else {
					settings.put(ps.getName(), tf.getText());
				}
			} else if (c instanceof JCheckBox) {
				JCheckBox cb = (JCheckBox)c;
				settings.put(cb.getName(),cb.isSelected());
			} else if (c instanceof JComboBox) {
				@SuppressWarnings("unchecked")
				JComboBox<Object> comboBox = (JComboBox<Object>)c;
				settings.put(comboBox.getName(), comboBox.getSelectedItem());
			}
		}

		if (error) {
			errors.append("\nPlease correct these errors and try again.\n");
			JOptionPane.showMessageDialog(null, errors.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		PlayerRunPanel playerPanel = new PlayerRunPanel(tabs, p.getName(), p, s, usernameTxt.getText(),String.valueOf(passTxt.getPassword()), settings);
		tabs.add(p.getName(), playerPanel);
		tabs.setSelectedComponent(playerPanel);
		playerPanel.startPlayer();
	}

	public Object parseValue(String val, Class<?> type) {
		if (String.class.equals(type)) {
			return val;
		} else if (Integer.TYPE.equals(type)){
			return Integer.parseInt(val);
		} else if (Long.TYPE.equals(type)){
			return Long.parseLong(val);
		} else if (Double.TYPE.equals(type)){
			return Double.parseDouble(val);
		} else if (Boolean.TYPE.equals(type)){
			return Boolean.parseBoolean(val);
		}
		return null;
	}
}

