/**
 * 
 */
package com.magneta.casino.virtualplayers.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.PlayerEventListener;
import com.magneta.casino.virtualplayers.players.PlayerLoader;
import com.magneta.casino.virtualplayers.players.Profile;
import com.magneta.casino.virtualplayers.players.Server;
import com.magneta.casino.virtualplayers.players.VirtualPlayer;

public class PlayerRunPanel extends JPanel implements ActionListener, PlayerEventListener {

	private static final Logger log = LoggerFactory.getLogger(PlayerRunPanel.class);
	
	private static final long serialVersionUID = 1L;

	private String title;
	private VirtualPlayer player;
	private JButton stopButton;
	private JButton exitButton;
	private JTabbedPane parentTabs;
	private JTextArea area;
	private String username;
	private String password;
	private long timeStarted;
	private long timeRoundStarted;
	private Thread playerThread;
	
	private JLabel roundIdLabel;

	public PlayerRunPanel(JTabbedPane tabs, String title, Profile p, Server s, String username, String password, Map<String, Object> settings) {
		super();
		this.title = title;
		this.username = username;
		this.password = password;

		try {
			this.player = new PlayerLoader().load(p, s, settings);
		} catch (Exception e) {
			log.error("Error while loading player", e);
			throw new RuntimeException(e);
		}
		this.parentTabs = tabs;

		
		JPanel topPanel = new JPanel();
		
		JLabel usernameLabel = new JLabel("User: "+username);
		roundIdLabel = new JLabel("Round: N/A");
		
		topPanel.add(usernameLabel);
		topPanel.add(roundIdLabel);
		
		this.stopButton = new JButton("Stop");
		this.stopButton.addActionListener(this);
		this.exitButton = new JButton("Close");
		this.exitButton.addActionListener(this);
		this.exitButton.setEnabled(false);
		this.setLayout(new BorderLayout());
		this.area = new JTextArea(20,20);
		this.area.setEditable(false);
		JScrollPane scroller = new JScrollPane(area);
		
		this.add(topPanel, BorderLayout.NORTH);
		this.add(scroller, BorderLayout.CENTER);
		JPanel controls = new JPanel();
		controls.add(stopButton);
		controls.add(exitButton);
		this.add(controls, BorderLayout.SOUTH);
		this.player.addListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == stopButton){
			player.stop();

			try {
				if (playerThread != null) {
					playerThread.join();
				}
			} catch (InterruptedException e1) {
			}
			

			try {
				player.logout();
			} catch (Throwable ex) {
			}
			
			onInfo("Run was canceled.");
			
			stopButton.setEnabled(false);
			exitButton.setEnabled(true);
		} else if (e.getSource() == exitButton){
			this.parentTabs.remove(this);
		}
	}

	public void startPlayer() {
		long loginStart = System.currentTimeMillis();
		int loginStatus;
		try {
			loginStatus = player.login(username, password);
		} catch (XmlRpcException e) {
			onError("Login failed: " + e.getMessage());
			return;
		}

		if (loginStatus != 0) {
			String error = "Player login failed!";
			switch(loginStatus) {
			case ClientInterface.INVALID_USERNAME_PASSWORD:
				error = "Invalid username or password";
				break;    
			case ClientInterface.UNKNOWN_ERROR:
				error = "Unknown error";
				break;
			case ClientInterface.SYSTEM_SUSPENDED:
				error = "System suspended";
				break;
			case ClientInterface.DATABASE_ERROR:
				error = "Database error";
				break;
			case ClientInterface.ALREADY_LOGGED_IN:
				error = "Already logged in";
				break;
			case ClientInterface.USERS_LIMIT_EXCEEDED:
				error = "Server is full";
				break;
			}
			onError("Login failed: " + error);
			return;
		}

		timeRoundStarted = timeStarted = System.currentTimeMillis();
		onInfo("Player login. Time: "+(timeStarted-loginStart)+" ms.");

		Runnable playerRunnable ;
		try {
			playerRunnable = player.start();
		} catch (XmlRpcException e) {
			playerRunnable = null;
			onError("Unable to start player: " + e.getMessage());
		}

		if (playerRunnable == null) {
			stopButton.setEnabled(false);
			exitButton.setEnabled(true);
			return;
		}

		playerThread = new Thread(playerRunnable);
		playerThread.start();
		playerThread.setUncaughtExceptionHandler(new PlayerExceptionHandler(player, this));
	}

	private static class PlayerExceptionHandler implements Thread.UncaughtExceptionHandler {

		private final PlayerEventListener eventListener;
		private final VirtualPlayer player;

		public PlayerExceptionHandler(VirtualPlayer player, PlayerEventListener eventListener) {
			this.player = player;
			this.eventListener = eventListener;
		}

		@Override
		public void uncaughtException(Thread t, Throwable e) {
			eventListener.onError(e.getMessage());
			player.logout();

		}

	}

	@Override
	public void finishedPlay() {
		long currentTime = System.currentTimeMillis();
		
		parentTabs.setTitleAt(parentTabs.indexOfComponent(this), title+" (Finished)");
		
		stopButton.setEnabled(false);
		exitButton.setEnabled(true);
		onInfo("Finished!");
		onInfo("Total time: "+(currentTime-timeStarted)+" ms.");
		onInfo("Average round "+ (long)((currentTime-timeStarted) / (double)player.getRoundsPlayed()) + " ms.");
		
		try {
			player.logout();
		} catch (Throwable e) {}
	}

	@Override
	public void finishedRound(int playedRounds, int totalRoundsToPlay) {
		onInfo("Finished round in "+(System.currentTimeMillis()-timeRoundStarted)+" ms.");
		timeRoundStarted = System.currentTimeMillis();

		if (totalRoundsToPlay > 0) {
			parentTabs.setTitleAt(parentTabs.indexOfComponent(this), title+" ("+playedRounds+"/"+totalRoundsToPlay+")");
		} else {
			parentTabs.setTitleAt(parentTabs.indexOfComponent(this), title+" ("+playedRounds + ")");
		}
	}

	private int lines = 0;

	private void addText(String text) {

		if (lines >= 100) {
			this.area.replaceRange("",0,this.area.getText().indexOf("\n")+1);
		} else {
			lines++;
		}
		this.area.append(text);
		this.area.setCaretPosition(this.area.getText().length());
	}

	@Override
	public void onError(String error) {
		addText(error+"\n");
		finishedPlay();
	}

	@Override
	public void onInfo(String info) {
		addText(info+"\n");
	}

	@Override
	public void roundStarted(long tableId, int roundId) {
		roundIdLabel.setText("Round: " + tableId + "-" + roundId);
	}
}