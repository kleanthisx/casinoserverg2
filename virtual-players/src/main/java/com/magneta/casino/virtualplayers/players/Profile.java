/**
 * 
 */
package com.magneta.casino.virtualplayers.players;

import java.util.HashMap;
import java.util.Map;


public class Profile {
    private String name;
    private String desc;
    private int gameId;
    private String className;
    private Map<String, ProfileSetting> settings;
    
    public Profile(){
        settings = new HashMap<String, ProfileSetting>();
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDesc() {
        return desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }        

    public int getGameId() {
        return gameId;
    }
    
    public void setGameID(int gameId) {
        this.gameId = gameId;
    }
    
    public String getClassName() {
        return className;
    }
    
    public void setClassName(String className) {
        this.className = className;
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
    public Map<String, ProfileSetting> getSettings() {
        return settings;
    }
    
    public void setSettings(Map<String, ProfileSetting> settings) {
        this.settings = settings;
    }
}