/**
 * 
 */
package com.magneta.casino.virtualplayers.players.roulette;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.Log;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualRoulettePlayer extends AbstractVirtualPlayer {

	private String bets;
	private int[] results;

	/**
	 * @param client
	 */
	public VirtualRoulettePlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.results = new int[37];
	}

	@Setting(description="Bets",value="0_4 1.00;0_5 1.00;0_6 1.00;0_7 1.00;0_8 1.00;0_9 1.00;")
	public void setBets(String bets){
		this.bets = bets;
	}

	@Override
	protected void onActionReceived(GameAction action) {
		String actionDesc = action.getDescription();

		if (actionDesc.equals("g:SPIN")) {
			results[Integer.parseInt(action.getValue())]++;
		}
	}

	private void bet(String betStr) throws XmlRpcException{
		doAction(1, "BET", betStr, getBet());
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()){
			DecimalFormat f = new DecimalFormat("0.00");
			DecimalFormatSymbols symbols = new DecimalFormatSymbols();
			symbols.setDecimalSeparator('.');
			symbols.setGroupingSeparator(',');
			f.setDecimalFormatSymbols(symbols);

			if (getBalance() >= getBet()){
				if (this.bets == null || bets.length() == 0){
					String allBets = "";
					for (int i=0;i<37;i++){
						if (i ==0){
							allBets += "0_"+i+" "+f.format(getBet());
						} else {
							allBets += ";0_"+i+" "+f.format(getBet());
						}
					}
					bet(allBets);
				} else {
					bet(bets);
				}
				//spin();
			} else {
				Log.info("Out of money!");
				break;
			}
		}

		String info = "\nStatistics:\n----------\n";
		for (int i=0;i<results.length;i++){
			info += i+" - "+results[i]+"\n";
		}

		onInfo(info);
	}
}
