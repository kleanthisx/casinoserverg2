package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualBankBurstPlayer extends AbstractVirtualPlayer {
    
    private enum RoundMode {
        DEFAULLT,
        BONUS,
        FREE_SPIN,
        GLASS_CUTTER_SELECT,
        CODE_SELECT,
        FREE_SPIN_SELECT;
    }
    
    private static final String GLASS_CUTTER_SELECT_ACTION = "g:CUTTER_SELECT";
    private static final String BOX_SELECT_ACTION = "g:BOX_SELECT";
    private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
    
    private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";
    private static final String GLASS_CUTTER_PROMPT = "g:CUTTER_PROMPT";
    
    private int lines;

    private RoundMode mode;
    private boolean[] openedBoxes;
    
    public VirtualBankBurstPlayer(ClientInterface client, int gameID) {
        super(client, gameID);
        this.lines = 0;
        this.mode = RoundMode.DEFAULLT;
    }  
    
    private void clearBoxes() {
        for (int i=0; i < openedBoxes.length; i++) {
            openedBoxes[i] = false;
        }
    } 
    
    @Setting(description="Number of lines", value="25")
    public void setLines(int lines){
        this.lines = lines;
    }
    
    @Setting(description="Number of boxes", value="7", prompt=false)
    public void setAvailableBoxes(int boxes) {
    	this.openedBoxes = new boolean[boxes];
    }
    
    private void bet() throws XmlRpcException {
    	if (lines > 0) {
    		doAction(1, "BET", String.valueOf(lines-1), getBet());
    	} else {
    		doAction(1, "BET", "", getBet());
    	}
    }
    
    @Override
	protected void roundFinished(Map<String, Object> status) {
    	this.mode = RoundMode.DEFAULLT;
        clearBoxes();
    }

	@Override
	protected void onActionReceived(GameAction action) {
		 if ("g:TREASURE_BONUS".equals(action.getDescription())){
             this.mode = RoundMode.BONUS;
             clearBoxes();
         } else if ("g:BOX_SELECT".equals(action.getDescription())){
             int boxNo = Integer.parseInt(action.getArea());
             int moveRow = 0;
             
             if (action.getValue() != null) {
                 moveRow = Integer.parseInt(action.getValue());
             }

             openedBoxes[boxNo] = true;

             if (moveRow > 0) {
                 clearBoxes();
             }

         } else if (FREE_SPIN_ACTION.equals(action.getDescription())) {
             this.mode = RoundMode.FREE_SPIN;
         } else if (GLASS_CUTTER_PROMPT.equals(action.getDescription())) {
             this.mode = RoundMode.GLASS_CUTTER_SELECT;
         } else if ("g:CODE_PROMPT".equals(action.getDescription())) {
         	this.mode = RoundMode.CODE_SELECT;
         } else if ("g:FREE_SPIN_PROMPT".equals(action.getDescription())) {
         	this.mode = RoundMode.FREE_SPIN_SELECT;
         }
		
	}
    
    @Override
    protected void parseStatus(Map<String, Object> status) {
    }
    
    @Override
    public void play() throws XmlRpcException {

    	while (!shouldStopPlaying()) {
    		switch (this.mode) {
    		case BONUS:
    		{
    			int boxCount = 0;

    			for (boxCount = 0; boxCount < openedBoxes.length; boxCount++) {
    				if (!openedBoxes[boxCount]) {
    					break;
    				}
    			}

    			doAction(1, BOX_SELECT_ACTION, String.valueOf(boxCount), 0.0);
    		}
    		break;
    		case FREE_SPIN:
    			doAction(1, START_FREE_SPIN_ACTION, "", 0.0);
    			break;
    		case GLASS_CUTTER_SELECT:
    			doAction(1, GLASS_CUTTER_SELECT_ACTION, "1", 0.0);
    			break;
    		case CODE_SELECT:
    			doAction(1, "g:CODE_SELECT", "1", 0.0);
    			break;
    		case FREE_SPIN_SELECT:
    			doAction(1, "g:FREE_SPIN_SELECT", "1", 0.0);
    			break;
    		default:
    			if (getBalance() >= getBet()){
    				bet();
    			} else {
    				this.stop();
    				onInfo("Out of money!");
    			}
    		}
    	}
    }
}
