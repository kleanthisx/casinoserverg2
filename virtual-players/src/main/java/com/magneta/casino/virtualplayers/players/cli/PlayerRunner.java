package com.magneta.casino.virtualplayers.players.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.PlayerLoader;
import com.magneta.casino.virtualplayers.players.Profile;
import com.magneta.casino.virtualplayers.players.ProfileLoader;
import com.magneta.casino.virtualplayers.players.ProfileSetting;
import com.magneta.casino.virtualplayers.players.Server;
import com.magneta.casino.virtualplayers.players.ServerLoader;
import com.magneta.casino.virtualplayers.players.VirtualPlayer;

public class PlayerRunner {

    private static String readLine(BufferedReader in, String prompt) throws IOException {
        System.out.print(prompt);
        String val = in.readLine();

        return val.trim();
    }

    private static String[] parseOption(String[] args, int index) throws IllegalArgumentException {
        if (!args[index].startsWith("-")) {
            throw new IllegalArgumentException("Expected option, found " + args[index]);
        }

        if (args.length <= index+1) {
            throw new IllegalArgumentException("Option without value");
        }

        String[] option = new String[2];

        option[0] = args[index].substring(1);
        option[1] = args[index+1].trim();

        return option;
    }

    private static boolean parseBoolean(String val) {
        Boolean value = null;

        val = val.toLowerCase();
        try {
            value = Boolean.parseBoolean(val);
        } catch (Throwable e) {}

        if (value == null) {
            if ("yes".equals(val)) {
                value = true;
            } else if ("no".equals(val)) {
                value = false;
            }
        }

        if (value == null) {
            throw new IllegalArgumentException("Invalid boolean value " + val);
        }

        return value;
    }

    private static int parseInt(String val) {
        Integer value = null;

        try {
            value = Integer.parseInt(val);
        } catch (Throwable e) {}

        if (value == null) {
            throw new IllegalArgumentException("Invalid integer value " + val);
        }

        return value;
    }

    public static void main(String[] args) throws IOException {

        Server[] servers = new ServerLoader().loadServers();
        Server server;
        if (servers != null && servers.length > 0) {
            server = servers[0];
        } else {
            server = new Server();
        }

        String paramUsername = null;
        String paramPass = null;
        Integer profile = null;
        List<String[]> options = new ArrayList<String[]>();

        for (int i=0; i < args.length; i+= 2) {
            String[] option = parseOption(args, i);

            if (option[0].equals("server")) {
                server.setUrl(option[1]);
            } else if (option[0].equals("ssl")) {
                server.setSsl(parseBoolean(option[1]));
            } else if (option[0].equals("port")) {
                server.setPort(parseInt(option[1]));
            } else if (option[0].equals("username")) {
                paramUsername = option[1];
            } else if (option[0].equals("password")) {
                paramPass = option[1];
            } else if (option[0].equals("profile")) {
                profile = parseInt(option[1]);
            } else if (option[0].equals("o")) {
                String[] opt = option[1].split("=");

                if (opt.length != 2) {
                    System.err.println("Invalid option spec.");
                    return;
                }

                options.add(opt);
            }
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        ProfileLoader profileLoader = new ProfileLoader();

        Profile[] profiles = profileLoader.loadProfiles();

        if (profile == null){
            System.out.println(
                    "****** CASINO VIRTUAL PLAYERS ******\n");

            int i = 1;
            for (Profile p: profiles) {
                System.out.format("%2d %s (%s)\n", i, p.getName(), p.getDesc());
                i++;
            }

            System.out.println("0 Exit");
        }

        while(profile == null ){
            profile = readInt(in, ">", 0, profiles.length);
        }

        if (profile == 0){
            System.exit(0);
            return;
        }

        while(server.getUrl() == null) {
            String url = readLine(in ,"Enter gaming server URL: ");
            if (url.trim().isEmpty()) {
                System.out.println("Invalid value.");
            } else {
                server.setUrl(url);
            }
        }

        while (server.getPort() == 0) {
            Integer port = readInt(in, "Enter gaming server port (7765): ",null,null);
            if (port == null) {
                port = 7765;
            }

            server.setPort(port);
        }

        if (server.isSsl() == null){
           server.setSsl(readBoolean(in, "Use SSL?(y/n): ")); 
        }

        Profile gameProfile = profiles[profile-1];
        VirtualPlayer player = null;
        try {
            player = configurePlayer(in, server, gameProfile, options);
        } catch (Throwable e) {
            System.err.println("Unable to create class " + gameProfile.getClassName());
            e.printStackTrace();
        }

        if (player == null){
            System.err.print("Error initializing player.");
            System.exit(1);
            return;
        }

        boolean loggedIn = false;

        while (!loggedIn){
            String username = paramUsername;

            while(username == null) {
                username = readLine(in, "Username: ");
                if (username != null) {
                    username = username.trim();

                    if (username.isEmpty()) {
                        username = null;
                    }
                }
            }

            String password = paramPass;
            while(password == null) {
                password = readLine(in, "Password: ");
                if (password != null) {
                    password = password.trim();

                    if (password.isEmpty()) {
                        password = null;
                    }
                }
            }

            try {
                int loginStatus = player.login(username, password);

                if (loginStatus == 0) {
                    loggedIn = true;
                } else {
                    String error = "Login failed";

                    switch(loginStatus) {
                        case ClientInterface.INVALID_USERNAME_PASSWORD:
                            error = "Invalid username or password";
                            break;    
                        case ClientInterface.UNKNOWN_ERROR:
                            error  ="Unknown error";
                            break;
                        case ClientInterface.SYSTEM_SUSPENDED:
                            error = "System suspended";
                            break;
                        case ClientInterface.DATABASE_ERROR:
                            error = "Database error";
                            break;
                        case ClientInterface.ALREADY_LOGGED_IN:
                            error = "Already logged in";
                            break;
                        case ClientInterface.USERS_LIMIT_EXCEEDED:
                            error = "Server is full";
                            break;
                    }
                    System.out.println(error);
                }

            } catch (XmlRpcException e) {
                System.err.println("Error during a request.");
                System.exit(1);
            }
        }

        Thread playerThread;
		try {
			playerThread = new Thread(player.start());
			playerThread.start();
			System.out.println("VIRTUAL PLAYER NOW RUNNING.\nPRESS Q or q followed by ENTER to exit at any time.");
		} catch (XmlRpcException e2) {
			playerThread = null;
			System.err.println("Unable to start player: " + e2.getMessage());
		}

        while(playerThread != null && playerThread.getState() != Thread.State.TERMINATED) {
            String exit;
            try {
                exit = readLine(in, "");
                if (exit.toLowerCase().contains("q")){
                    player.stop();
                    break;
                }
            } catch (IOException e1) {

            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }

        try {
        	if (playerThread != null) {
        		playerThread.join();
        	}
		} catch (InterruptedException e) {
		}
        player.logout();
    }
    
    private static Object getEnumValue(Class<?> enumKlass, String val) {
    	Method m;
		try {
			m = enumKlass.getMethod("valueOf", String.class);
			return m.invoke(null, val);
		} catch (Throwable e) {
			return null;
		}
    }

    private static Object parseValue(String val, Class<?> type){
        try {
            if (String.class.equals(type)){
                return val;
            } else if (Integer.TYPE.equals(type)){
                return Integer.parseInt(val);
            } else if (Long.TYPE.equals(type)){
                return Long.parseLong(val);
            } else if (Double.TYPE.equals(type)){
                return Double.parseDouble(val);
            } else if (Boolean.TYPE.equals(type)){
                return Boolean.parseBoolean(val);
            } else if (type.isEnum()) {
            	return getEnumValue(type, val);
            }
        } catch(NumberFormatException e) {}
        return null;
    }

    private static VirtualPlayer configurePlayer(BufferedReader in, Server server, Profile profile, List<String[]> options) throws Exception {
        Map<String, ProfileSetting> profileSettings = profile.getSettings();
        Map<String, Object> settings = new HashMap<String, Object>();

        Iterable<String> it = profileSettings.keySet();

        for (String s: it) {
            ProfileSetting set = profileSettings.get(s);
            /*if (!set.isPrompt()) {
                continue;
            }*/

            if (options != null) {

                boolean foundOption = false;
                for (String[] cmdOption: options) {
                     if (cmdOption[0].equals(set.getName())) {
                         Object o = parseValue(cmdOption[1], set.getType());
                         if (o != null) {
                             settings.put(set.getName(), o);
                             foundOption = true;
                             break;
                         }
                     }
                }

                if (foundOption)
                   continue;
            }

            StringBuilder prompt = new StringBuilder();
            prompt.append(set.getDesc());
            prompt.append(' ');

            boolean settingOk = false;
            Object def = set.getDef();
            if (def != null) {
                prompt.append('(');
                prompt.append(def);
                prompt.append(')');
                settingOk = true;
            }

            prompt.append(": ");

            do {
                String val = readLine(in, prompt.toString());
                if (val != null)
                    val.trim();

                if (val == null || val.isEmpty()) {
                    if (set.getDef() != null) {
                        settings.put(set.getName(), set.getDef());
                        settingOk = true;
                    } else {
                        continue;
                    }
                }

                Object o = parseValue(val, set.getType());

                if (o != null) {
                    settings.put(set.getName(), o);
                    settingOk = true;
                }

            } while(!settingOk);

        }

        return new PlayerLoader().load(profile, server, settings);
    }

    public static Integer readInt(BufferedReader in, String prompt, Integer min, Integer max) throws IOException{
        Integer val = null;
        while(true){
            String str = readLine(in, prompt);
            if (str == null || str.trim().isEmpty()) {
                return null;
            }

            try {
                val = Integer.parseInt(str);
            } catch(NumberFormatException e) {
                System.err.println("Expected integer found " + str);
            }

            if (val == null) {
            	continue;
            }
            
            if ((min == null || val >= min) && (max == null || val <= max)){
                break;
            }
        }
        return val;
    }

    public static Boolean readBoolean(BufferedReader in, String prompt) throws IOException{
        String str;
        Boolean value = null;

        while(value == null){
            str = readLine(in, prompt);

            try {
                value = parseBoolean(str);
                break;
            } catch (Throwable e) {
                System.out.println("Invalid value.");
            }
        }

        return value;
    }
}
