package com.magneta.casino.virtualplayers.players.poker;

import java.util.Map;
import java.util.Random;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.Card;
import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.Log;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

public class VirtualVideoPokerPlayer extends AbstractVirtualPlayer {

	private static final String[] COMBO_DESCRIPTIONS = {
		"Jacks Or Better",
		"Two Pairs",
		"Three Of A Kind",
		"Straight",
		"Flush",
		"Full House",
		"Four Of A Kind",
		"Straight Flush",
		"Royal Flush"
	};

	private int[] cards;
	private boolean placedBet;
	private boolean endedHold;
	private boolean suggests[];
	protected int doubleUpCard;
	protected boolean tookHalf;
	protected boolean doubledUp;
	protected boolean halfStake;
	protected int doubleUpTimes;
	protected Random random;
	private int[] comboInstances;
	private int[] cardInstances;
	private boolean newHand = false;

	public VirtualVideoPokerPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.cards = new int[5];
		this.placedBet = false;
		this.endedHold = false;
		this.suggests = new boolean[5];
		this.doubleUpCard = -1;
		this.tookHalf = false;
		this.doubledUp = false;
		this.random = new Random();
		this.doubleUpTimes = 4;
		this.halfStake = false;
		this.comboInstances = new int[9];
		this.cardInstances = new int[53];
	}

	@Setting(description="Take half")
	public void setTakeHalf(boolean takeHalf) {
		this.halfStake = takeHalf;
	}

	@Setting(description="Times to double up")
	public void setDoubleUpTimes(int doubleUpTimes) {
		this.doubleUpTimes = doubleUpTimes;
	}

	@Override
	protected final void roundFinished(Map<String, Object> status) {
		this.cards = new int[5];
		this.suggests = new boolean[5];
		this.endedHold = false;
		this.placedBet = false;
		this.doubleUpCard = -1;
		this.tookHalf = false;
		this.doubledUp = false;
	}

	@Override
	protected final void onActionReceived(GameAction action) {
		String actionDesc = action.getDescription();           
		if (actionDesc.equals("BET")) {
			placedBet = true;
		} else if (actionDesc.equals("g:DRAW")){
			int area = Integer.parseInt(action.getArea());
			int card = Integer.parseInt(action.getValue());
			cards[area] = card;
			if (Card.getCardNumber(card) == Card.JOKER){
				cardInstances[52]++;
			} else {
				cardInstances[Card.getCardRace(card) * 13 + (Card.getCardNumber(card)-1)]++;
			}
		} else if (actionDesc.equals("g:DOUBLE_DRAW")){
			int area = Integer.parseInt(action.getArea());
			if (area == 0){
				int card = Integer.parseInt(action.getValue());
				doubleUpCard = card;
			}
		} else if (actionDesc.equals("g:HOLD_SUGGEST")){
			int area = Integer.parseInt(action.getArea());
			suggests[area] = true;

			Log.debug("Suggest hold for card at "+area);
		}  else if (actionDesc.equals("g:STAKE_HALF")){
			tookHalf = true;
			doubledUp = true;
		} else if (actionDesc.equals("g:DOUBLE_UP")){
			doubledUp = true;
		} else if (actionDesc.equals("g:END_HOLD")){
			endedHold = true;
			newHand = true;
		} else if (actionDesc.equals("g:WIN_CARD")){
			if (newHand){
				int combo = Integer.parseInt(action.getValue());
				comboInstances[combo-1]++;
				newHand = false;
			}
		} 
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()) {
			if (!placedBet){
				if (getBalance() >= getBet()) {
					bet();
				} else {
					onInfo("Out of money");
					break;
				}
			}

				int round = this.roundId;

				if (!endedHold) {

					boolean noSuggests = true;

					for (int i=0;i<5;i++){
						if (suggests[i]){
							hold(i);
							noSuggests = false;
						}
					}

					if (noSuggests){
						int cardCounts[] = new int[5];
						int j = 0;
						int huntCardValue = -1;

						for (int i=0;i<cards.length;i++){
							if (VideoPokerDeck.getCardValue(cards[i]) != VideoPokerDeck.JOKER){
								huntCardValue = VideoPokerDeck.getCardValue(cards[i]);
								break;
							}
						}

						for (int i=0;i<cardCounts.length;i++){                                
							if (VideoPokerDeck.getCardValue(cards[i]) == huntCardValue){
								cardCounts[j]++;
							} else if (VideoPokerDeck.getCardValue(cards[i]) == VideoPokerDeck.JOKER){
								j=i;
							} else{
								huntCardValue = VideoPokerDeck.getCardValue(cards[i]);
								j = i;
								cardCounts[j]++;
							}
						}

						int maxCount = cardCounts[0];
						int maxCountIndex = 0;
						int maxCardValue = VideoPokerDeck.getCardValue(cards[0]);
						int maxCardValueIndex = 0;

						for (int i=0;i<cardCounts.length;i++){
							if (cardCounts[i] > maxCount){
								maxCount = cardCounts[i];
								maxCountIndex = i;
							}

							if (VideoPokerDeck.getCardValue(cards[i]) >  maxCardValue){
								maxCardValue = VideoPokerDeck.getCardValue(cards[i]);
								maxCardValueIndex = i;
							}
						}

						if (maxCount >= 2){
							int maxPairCardValue = VideoPokerDeck.getCardValue(cards[maxCountIndex]);
							for (int i = 0;i<cards.length;i++){
								if (VideoPokerDeck.getCardValue(cards[i]) == maxPairCardValue){
									hold(i);
								}
							}
						} else if (maxCardValue > 10){
							hold(maxCardValueIndex);
						}
					}

					endHold();
				}

				if (round == roundId){
					doWinningCombinationProcedure();
				}
		}

		String info = "\nStatistics:\n----------\n";
		for (int i=0;i<comboInstances.length;i++){
			info += COMBO_DESCRIPTIONS[i]+" - "+comboInstances[i]+"\n";
		}
		info += "\n\n";
		for (int i=0;i<cardInstances.length;i++){
			int race = (int)Math.floor(i / 13);
			int value = (i - (race * 13))+1;
			if (i == (cardInstances.length-1)){
				value = Card.JOKER;
			}
			info += describeCard(Card.getCard(value, race))+" - "+cardInstances[i]+"\n";
			if (value == Card.KING){
				info += "\n";
			}
		}

		onInfo(info);
	}

	protected void doWinningCombinationProcedure() throws XmlRpcException{
		int round = this.roundId;

		if (doubleUpTimes > 0){
			round = this.roundId;
			int counter = this.doubleUpTimes;

			while ((round == this.roundId) && (counter > 0)){
				if (halfStake){
					stakeHalf();
				} else {
					doubleUp();
				}

				higher(1+random.nextInt(4));
				counter--;
			}
		}

		if (round == this.roundId){
			takeScore();
		}
	}

	private void bet() throws XmlRpcException{
		doAction(1, "BET", "", getBet());
	}

	private void endHold() throws XmlRpcException{
		doAction(1, "g:END_HOLD", "", 0.0);
	}

	protected void takeScore() throws XmlRpcException{
		doAction(1, "g:TAKE_SCORE", "", 0.0);
	}

	protected void lower() throws XmlRpcException{
		doAction(1, "g:LOWER", "", 0.0);
	}

	protected void higher() throws XmlRpcException{
		doAction(1, "g:HIGHER", "", 0.0);
	}

	protected void red() throws XmlRpcException{
		doAction(1, "g:RED", "", 0.0);
	}

	protected void black() throws XmlRpcException{
		doAction(1, "g:BLACK", "", 0.0);
	}

	protected void doubleUp() throws XmlRpcException{
		doAction(1, "g:DOUBLE_UP", "", 0.0);
	}

	private void hold(int area) throws XmlRpcException{
		doAction(1, "g:HOLD", String.valueOf(area), 0.0);
	}

	protected void stakeHalf() throws XmlRpcException{
		doAction(1, "g:DOUBLE_HALF", "", 0.0);
	}

	protected void higher(int area) throws XmlRpcException{
		doAction(1, "g:HIGHER", String.valueOf(area), 0.0);
	}
}
