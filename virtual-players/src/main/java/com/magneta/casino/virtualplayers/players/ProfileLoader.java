/**
 * 
 */
package com.magneta.casino.virtualplayers.players;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.magneta.casino.virtualplayers.Log;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author anarxia
 *
 */
public class ProfileLoader {
	
	private static final Logger log = LoggerFactory.getLogger(ProfileLoader.class);
    
    private static final String PROFILES_FILE = "profiles.xml";
    
    private String setterToProperty(String methodName) {
        if (methodName.startsWith("set")) {
            return Character.toLowerCase(methodName.charAt(3)) + methodName.substring(4);
        }

        return null;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private Object parseValue(Class<?> type, String value) {
        if (String.class.equals(type)) {
            return value;
        } else if (Integer.TYPE.equals(type)) {
            return Integer.parseInt(value);
        } else if (Long.TYPE.equals(type)) {
            return Long.parseLong(value);
        } else if (Boolean.TYPE.equals(type)) {
            return Boolean.parseBoolean(value);
        } else if (Double.TYPE.equals(type)) {
            return Double.parseDouble(value);
        } else if (type.isEnum()) {
        	return Enum.valueOf((Class<? extends Enum>)type, value);
        }
        
        throw new RuntimeException("Unable to handle type " + type);
    }
    
    private Map<String, ProfileSetting> loadSettings(Profile profile) throws SecurityException, ClassNotFoundException {
        
        Map<String, ProfileSetting> settings = new HashMap<String, ProfileSetting>();
        
        @SuppressWarnings("unchecked")
        Class<? extends VirtualPlayer> klass = (Class<? extends VirtualPlayer>)Class.forName(profile.getClassName());
        
        Method[] methods = klass.getMethods();
        
        for (Method m: methods) {
            Setting setting = m.getAnnotation(Setting.class);
            if (setting != null) {
                
                String name = setterToProperty(m.getName());

                if (name == null) {
                    continue;
                }
                
                ProfileSetting set = new ProfileSetting();
                set.setName(name);
                
                if (setting.description() == null || setting.description().isEmpty()) {
                    set.setDesc(name);
                } else {
                    set.setDesc(setting.description());
                }
                
                set.setType(m.getParameterTypes()[0]);
                
                /* Initialize primitives */
                if (set.getType().isPrimitive()) {
                    if (Boolean.TYPE.equals(set.getType())) {
                        set.setDef(false);
                    } else if (Integer.TYPE.equals(set.getType())) {
                        set.setDef(0);
                    } else if (Long.TYPE.equals(set.getType())) {
                        set.setDef(0L);
                    }  else if (Double.TYPE.equals(set.getType())) {
                        set.setDef(0.0d);
                    } 
                }
                
                if (setting.value() != null && !setting.value().isEmpty()) {
                    set.setDef(parseValue(set.getType(), setting.value()));
                }
                
                set.setPrompt(setting.prompt());
                
                settings.put(name, set);
            }
        }
        
        return settings;
    }
    
    public Profile[] loadProfiles(){
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(PROFILES_FILE);

        try {
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            
            NodeList profs = doc.getElementsByTagName("profile");
            
            Profile[] arr = new Profile[profs.getLength()]; 
            for (int i = 0; i < profs.getLength(); i++) {
                Element prof = (Element) profs.item(i);
                NodeList props = prof.getChildNodes();
                
                arr[i] = new Profile();
                
                for (int j = 0; j < props.getLength(); j++) {
                    Node oNode = props.item(j);

                    if (oNode.getNodeName().equals("name")){
                        arr[i].setName(oNode.getTextContent().trim());
                    } else if (oNode.getNodeName().equals("desc")){
                        arr[i].setDesc(oNode.getTextContent().trim());
                    } else if (oNode.getNodeName().equals("class")){
                        arr[i].setClassName(oNode.getTextContent().trim());
                        arr[i].setSettings(loadSettings(arr[i]));
                    } else if (oNode.getNodeName().equals("gameid")){
                        arr[i].setGameID(Integer.parseInt(oNode.getTextContent().trim()));
                    } else if (oNode.getNodeName().equals("params")) {
                        NodeList params = oNode.getChildNodes();
                        for (int k = 0; k < params.getLength(); k++) {
                            Node par = params.item(k);
                            
                            if (par.getNodeType() != Node.ELEMENT_NODE)
                                continue;
                            
                            String name = par.getNodeName();
                            String value = par.getTextContent();
                            
                            if (value != null) {
                            	value = value.trim();
                            }
                            
                            if (name != null && value != null) {
                                ProfileSetting sett = arr[i].getSettings().get(name);
                                
                                if (sett == null) {
                                    log.warn("Unrecognised parameter {}", name);
                                    continue;
                                }
                                sett.setDef(parseValue(sett.getType(), value));
                                
                                NamedNodeMap attrs = par.getAttributes();
                                
                                Node prompt = attrs.getNamedItem("prompt");
                                if (prompt != null) {
                                    sett.setPrompt(Boolean.parseBoolean(prompt.getTextContent()));
                                }
                                
                            }
                        }
                    }
                }
            }
            return arr;
        }
        catch (Exception e) { 
           Log.error("Error reading profiles file ",e);
        }
        return null;
    }
}
