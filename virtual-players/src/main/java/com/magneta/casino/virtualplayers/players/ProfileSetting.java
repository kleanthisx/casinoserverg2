/**
 * 
 */
package com.magneta.casino.virtualplayers.players;

public class ProfileSetting {
    private String name;
    private String desc;
    private Object def;
    private Class<?> type;
    private boolean prompt;
    
    public Class<?> getType() {
        return type;
    }
    
    public void setType(Class<?> type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDesc() {
        return desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public Object getDef() {
        return def;
    }
    
    public void setDef(Object def) {
        this.def = def;
    }

    public void setPrompt(boolean prompt) {
        this.prompt = prompt;
    }

    public boolean isPrompt() {
        return prompt;
    }
}