/**
 * 
 */
package com.magneta.casino.virtualplayers.players.slots;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualSlotsPlayer extends AbstractVirtualPlayer {
    
    private int lines;
    
    public VirtualSlotsPlayer(ClientInterface client, int gameId) {
        super(client, gameId);
        this.lines = -1;
    }   
    
    @Setting(description="Lines to play", prompt=false)
    public void setLines(int lines){
        this.lines = lines;
    }
    
    private void bet() throws XmlRpcException{
        String strLines = null;
        if (lines < 0){
            strLines = "";
        } else {
            strLines = String.valueOf(lines-1);
        }
        
        doAction(1, "BET", strLines, getBet());
    }
    
    @Override
    public void play() throws XmlRpcException {

    	while (!shouldStopPlaying()) {
    		if (getBalance() >= getBet()){
    			bet();
    		} else {
    			onInfo("Out of money!");
    			break;
    		}
    	}
    }
}
