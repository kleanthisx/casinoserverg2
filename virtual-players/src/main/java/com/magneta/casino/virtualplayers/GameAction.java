/**
 * 
 */
package com.magneta.casino.virtualplayers;

import java.util.Map;


/**
 * @author anarxia
 *
 */
public class GameAction {

    private final String description;
    private final Double amount;
    private final String area;
    private final String value;
    private final Integer roundId;
    private final Integer seatId;
    private final boolean ownAction;
    
    /**
     * Creates an action from the XML-RPC struct from the server.
     * @param action
     */
    public GameAction(Map<String, Object> action) {  
        this.roundId = (Integer)action.get("round");
        this.description = (String)action.get("action_desc");
        this.area = (String)action.get("action_area");
        this.value = (String)action.get("action_value");
        this.amount = (Double)action.get("amount");
        this.seatId = (Integer)action.get("seat_id");
        
        Integer own = (Integer)action.get("own_action");
        
        if (own != null && own > 0) {
            ownAction = true;
        } else {
            ownAction = false;
        }
    }
    
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return Returns the amount.
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @return Returns the area.
     */
    public String getArea() {
        return area;
    }

    /**
     * @return Returns the value.
     */
    public String getValue() {
        return value;
    }

    /**
     * @return Returns the roundId.
     */
    public Integer getRoundId() {
        return roundId;
    }

    /**
     * @return Returns the seatId.
     */
    public Integer getSeatId() {
        return seatId;
    }

    /**
     * @return Returns the ownAction.
     */
    public boolean isOwnAction() {
        return ownAction;
    }    
}
