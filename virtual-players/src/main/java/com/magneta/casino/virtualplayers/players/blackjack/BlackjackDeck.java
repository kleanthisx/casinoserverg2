/**
 * 
 */
package com.magneta.casino.virtualplayers.players.blackjack;

import java.util.List;

import com.magneta.casino.virtualplayers.Card;

/**
 * @author User
 *
 */
public class BlackjackDeck {

    public static int sumCards(List<Integer> cards) {
        int sum = 0;
        int noAces = 0;
        
        if (cards != null)
        {
            for (int card: cards){
                int currCard = Card.getCardNumber(card);
                
                if (currCard == 1) {
                    sum++;
                    noAces++;
                } else if (currCard > 10) {
                    sum += 10;
                } else {
                    sum += currCard;
                }
            }
        }
        
        if (noAces > 0) {
            if (sum <= 11) {
                sum += 10;
            }
        }
        
        return sum;
    }
    
    /**
     * Returns the card value applicable to the blackjack game.
     * @param card The card of whose value is to be determined.
     * @return The card value applicable to the blackjack game.
     */
    public static int getCardValue(int card) {
        int cardNo = Card.getCardNumber(card);
        
        if (cardNo <= 10)
            return cardNo;
        
        return 10;
    }
}
