/**
 * 
 */
package com.magneta.casino.virtualplayers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convienience class for logging.
 * It is a thin wrapper around slf4j.
 *
 * @author Nicos
 */
public final class Log {

    private static final Logger log = LoggerFactory.getLogger(Log.class);

    /**
     * Construct a new logger.
     */
    private Log() {
        super();
    }

    /**
     * Returns whether debugging is enabled.
     * @return  Whether debugging is enabled.
     */
    public static final boolean isDebugEnabled() {
    	if (log == null) {
    		return false;
    	}
    	return log.isDebugEnabled();
    }
    
    /**
     * Log a debugging message.
     * @param msg The message to log.
     */
    public static void debug(String msg) {
        if (log != null) {   
            log.debug(msg);
        }
    }

    /**
     * Log a debugging message and the exception information that caused it.
     * @param msg The message to log.
     * @param exception The exception that caused the message.
     */
    public static void debug(String msg, Throwable exception) {
        if (log != null) {   
            log.debug(msg, exception);
        }
    }
    
    /**
     * Log an information message.
     * @param msg The message to log.
     */
    public static void info(String msg) {
        if (log != null)
            log.info(msg);
    }

    /**
     * Log an information message and the exception information that caused it.
     * @param msg The message to log.
     * @param exception The exception that caused the message.
     */
    public static void info(String msg, Throwable exception) {
        if (log != null)
            log.info(msg, exception);
    }

    /**
     * Log a warning message.
     * @param msg The message to log.
     */
    public static void warn(String msg) {
        if (log != null)
            log.warn(msg);
    }

    /**
     * Log a warning message and the exception information that caused it.
     * @param msg The message to log.
     * @param exception The exception that caused the message.
     */
    public static void warn(String msg, Throwable exception) {
        if (log != null)
            log.warn(msg, exception);
    }

    /**
     * Log an error message.
     * @param msg The message to log.
     */
    public static void error(String msg) {
        if (log != null)
            log.error(msg);
    }

    /**
     * Log an error message and the exception information that caused it.
     * @param msg The message to log.
     * @param exception The exception that caused the message.
     */
    public static void error(String msg, Throwable exception) {
        if (log != null)
            log.error(msg, exception);
    }
}