/**
 * 
 */
package com.magneta.casino.virtualplayers.players.poker;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.clients.ClientInterface;

public class VirtualVideoPokerPlusPlayer extends VirtualVideoPokerPlayer {
    
    /**
     * @param client
     * @param gameID
     */
    public VirtualVideoPokerPlusPlayer(ClientInterface client, int gameId) {
        super(client, gameId);
    }

    @Override
    protected void doWinningCombinationProcedure() throws XmlRpcException {
        if (!doubledUp){
            doubleUp();
        }
        
        int round = this.roundId;
        int counter = this.doubleUpTimes;
        
        while ((round == this.roundId) && (counter > 0)){
            if (halfStake){
                stakeHalf();
            }
            
            if (random.nextInt(100) > 50){
                red();
            } else {
                black();
            }
            counter--;
        }
        
        if (round == this.roundId){
            takeScore();
        }
   }
}
