package com.magneta.casino.virtualplayers.players.poker;

import com.magneta.casino.virtualplayers.Card;

public class VideoPokerDeck {
    
    public static final int ACE = 14;
    public static final int JOKER = 15;
    /**
     * Returns the card value applicable to the video poker game.
     * @param card The card of whose value is to be determined.
     * @return The card value applicable to the video poker game.
     */
    public static int getCardValue(int card){
        int cardNo = Card.getCardNumber(card);
        
        if (cardNo == Card.ACE){
            return ACE;
        } else if (cardNo == Card.JOKER){
            return JOKER;
        } else{
            return cardNo;
        }
    }
}
