package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualFortuneTellerPlayer extends AbstractVirtualPlayer {

	private enum RoundMode {
		DEFAULLT,
		BONUS,
		FREE_SPIN,
	}

	private static final String BOX_SELECT_ACTION = "g:SELECT_CARD";
	private static final String START_FREE_SPIN_ACTION = "g:START_FREE_SPIN";
	private static final String FREE_SPIN_ACTION = "g:FREE_SPIN";

	private int lines;

	private RoundMode mode;

	public VirtualFortuneTellerPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.lines = 0;
		this.mode = RoundMode.DEFAULLT;
	}  


	@Setting(description="Number of lines", value="25")
	public void setLines(int lines){
		this.lines = lines;
	}

	private void bet() throws XmlRpcException {
		if (lines > 0) {
			doAction(1, "BET", String.valueOf(lines-1), getBet());
		} else {
			doAction(1, "BET", "", getBet());
		}
	}

	private void startFreeSpin() throws XmlRpcException{
		doAction(1, START_FREE_SPIN_ACTION, "", 0.0);
	}

	private void selectBox(int area) throws XmlRpcException {
		doAction(1, BOX_SELECT_ACTION, String.valueOf(area), 0.0);
	}

	@Override
	protected void roundFinished(Map<String, Object> status) {
		this.mode = RoundMode.DEFAULLT;
	}

	@Override
	protected void onActionReceived(GameAction action) {
		if ("g:TREASURE_BONUS".equals(action.getDescription())){
			this.mode = RoundMode.BONUS;
		} else if (FREE_SPIN_ACTION.equals(action.getDescription())) {
			this.mode = RoundMode.FREE_SPIN;
		}
	}

	@Override
	public void play() throws XmlRpcException {

		while (!shouldStopPlaying()) {
			switch (this.mode) {
			case BONUS:
				selectBox(0);
				break;
			case FREE_SPIN:
				startFreeSpin();
				break;
			default:
				if (getBalance() >= getBet()) {
					bet();
				} else {
					onInfo("Out of money!");
					stop();
					break;
				}
			}
		}
	}
}
