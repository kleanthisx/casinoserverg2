package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;

public class VirtualMexicoPlayer extends AbstractVirtualPlayer {

	private enum RoundMode {
		DEFAULLT,
		SCATTER_SELECT;
	}
	private boolean freeSpin;
	private RoundMode mode;

	public VirtualMexicoPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.freeSpin = false;
		this.mode = RoundMode.DEFAULLT;
	}

	private void bet() throws XmlRpcException{
		doAction(1, "BET", "", getBet());
	}

	private void startFreeSpin() throws XmlRpcException{
		doAction(1, "g:START_FREE_SPIN", "", 0.0);
	}

	private void doScatterSelect() throws XmlRpcException{
		doAction(1, "g:SCATTER_SELECT", "0", 0.0);
	}

	@Override
	protected void roundFinished(Map<String, Object> status) {
		this.freeSpin = false;
	}

	@Override
	protected void onActionReceived(GameAction action) {

		if ("g:FREE_SPIN".equals(action.getDescription())) {
			this.freeSpin = true;
			this.mode = RoundMode.DEFAULLT;
		} else if ("g:FREE_SPINS_WIN".equals(action.getDescription())) {
			this.mode = RoundMode.SCATTER_SELECT;
		}

	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()){
			if (this.mode == RoundMode.SCATTER_SELECT) {
				doScatterSelect();
			} else if (freeSpin){
				startFreeSpin();
			} else {
				if (getBalance() >= getBet()){
					bet();
				} else {
					onInfo("Out of money!");
					break;
				}
			}
		}
	}
}
