package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualReinbowRichesPlayer extends AbstractVirtualPlayer {

	private enum RoundMode {
		DEFAULLT,
		ROAD_PROMPT,
		ROAD_SELECT,
		WELL_PROMPED,
		WELL_SELECT,
		POT_PROMPED,
		POT_SELECT
	}

	private static final String WELL_SELECT_ACTION = "g:WELL_SELECT";
	private static final String ROAD_SELECT_ACTION = "g:ROAD_SELECT";
	private static final String POT_SELECT_ACTION  = "g:POT_SELECT";


	private static final String ROAD_SCATTER_PROMPT = "g:ROAD_PROMPT";
	private static final String POT_SCATTER_PROMPT  = "g:POT_PROMPT";
	private static final String WELL_SCATTER_PROMPT = "g:WELL_PROMPT";

	private int lines;

	private RoundMode mode;

	public VirtualReinbowRichesPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.lines = 0;
		this.mode = RoundMode.DEFAULLT;
	}  

	@Setting(description="Number of lines", value="20")
	public void setLines(int lines){
		this.lines = lines;
	}

	private void bet() throws XmlRpcException {
		if (lines > 0) {
			doAction(1, "BET", String.valueOf(lines-1), getBet());
		} else {
			doAction(1, "BET", "", getBet());
		}
	}

	@Override
	protected void roundFinished(Map<String, Object> status) {
		this.mode = RoundMode.DEFAULLT;
	}

	@Override
	protected void onActionReceived(GameAction action) {
		
		if (WELL_SCATTER_PROMPT.equals(action.getDescription())){
			this.mode = RoundMode.WELL_SELECT;
		} else if (POT_SCATTER_PROMPT.equals(action.getDescription())){
			this.mode = RoundMode.POT_SELECT;
		} else if (ROAD_SCATTER_PROMPT.equals(action.getDescription())) {
			this.mode = RoundMode.ROAD_SELECT;
		}
		
	}

	@Override
	public void play() throws XmlRpcException {

		while (!shouldStopPlaying()) {
			switch (this.mode) {
			case ROAD_SELECT:
				doAction(1, ROAD_SELECT_ACTION, String.valueOf("1"), 0.0);
			break;
			case WELL_SELECT:
				doAction(1, WELL_SELECT_ACTION, "1", 0.0);
				break;
			case POT_SELECT:
				doAction(1, POT_SELECT_ACTION, "1", 0.0);
				break;
			default:
				if (getBalance() >= getBet()){
					bet();
				} else {
					this.stop();
					onInfo("Out of money!");
				}
			}
		}
	}
}
