package com.magneta.casino.virtualplayers.players.slots;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualPharaohSlotPlayer extends AbstractVirtualPlayer {

	private int lines;
	private boolean freeSpin;
	private Map<Integer, Integer> comboSymbolQuantities;
	private int boxesLeft;
	private int treasureNo;

	public VirtualPharaohSlotPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.lines = 0;
		this.freeSpin = false;
		this.comboSymbolQuantities = new HashMap<Integer, Integer>();
		this.boxesLeft = 0;
		this.treasureNo = 0;
	}   

	@Setting(description="Number of lines", value="15")
	public void setLines(int lines){
		this.lines = lines;
	}

	private void bet() throws XmlRpcException{
		doAction(1, "BET", String.valueOf(lines-1), getBet());
	}

	private void startFreeSpin() throws XmlRpcException{
		doAction(1, "g:START_FREE_SPIN", "", 0.0);
	}

	private void selectBox(int area) throws XmlRpcException {
		doAction(1, "g:BOX_SELECT", String.valueOf(area), 0.0);
	}
	
	@Override
	protected void roundFinished(Map<String, Object> status) {
		this.freeSpin = false;
		
		if (status.get("free_spins_left") != null) {
			this.freeSpin = true;
		}
	}

	@Override
	protected void parseStatus(Map<String, Object> status) {
		if ((status.get("combos") != null) && comboSymbolQuantities.isEmpty()){
			Object[] combos = (Object[])status.get("combos");

			for (int i=0;i<combos.length;i++){
				Object[] combo = (Object[])combos[i];

				comboSymbolQuantities.put(i, (Integer)combo[1]);
			}
		}
		
		if (status.get("free_spins_left") != null) {
			this.freeSpin = true;
		}
	}
	
	@Override
	protected void onActionReceived(GameAction action) {
		String actionDesc = action.getDescription();          

		if (actionDesc.equals("g:TREASURE_BONUS")){
			int treasureBonusCombo = Integer.parseInt(action.getValue());
			boxesLeft = this.comboSymbolQuantities.get(treasureBonusCombo);
			this.treasureNo++;
		} else if (actionDesc.equals("g:BOX_SELECT")){
			boxesLeft = Integer.parseInt(action.getValue());
		}
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()){                
			if (freeSpin) {
				startFreeSpin();
			} else if (getBalance() >= getBet()) {
				bet();
			} else {
				onInfo("Out of money!");
				break;
			}

			if (treasureNo > 0){
				Random r = new Random();

				while (boxesLeft > 0){
					int currTreasure = treasureNo;
					List<Integer> avail = new ArrayList<Integer>();

					for (int i=0;i<12;i++){
						avail.add(new Integer(i));
					}

					while (boxesLeft > 0 && treasureNo == currTreasure){
						int next = r.nextInt(avail.size());
						int box = avail.get(next);
						selectBox(box);
						avail.remove(next);
					}
				}
			}

		}
	}
}
