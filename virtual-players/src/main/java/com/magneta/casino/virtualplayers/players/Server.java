/**
 * 
 */
package com.magneta.casino.virtualplayers.players;

public class Server {
    private String name;
    private String url;
    private int port;
    private Boolean ssl;
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public int getPort() {
        return port;
    }
    
    public void setPort(int port) {
        this.port = port;
    }
    
    public Boolean isSsl() {
        return ssl;
    }
    
    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString(){
        return this.name;
    }
}