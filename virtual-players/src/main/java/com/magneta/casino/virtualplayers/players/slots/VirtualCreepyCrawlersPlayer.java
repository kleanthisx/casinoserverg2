package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualCreepyCrawlersPlayer extends AbstractVirtualPlayer {

	private int lines;
	private boolean freeSpin;

	public VirtualCreepyCrawlersPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.lines = 0;
		this.freeSpin = false;
	}   

	@Setting(description="Number of lines", value="15")
	public void setLines(int lines){
		this.lines = lines;
	}

	private void bet() throws XmlRpcException{
		doAction(1, "BET", String.valueOf(lines-1), getBet());
	}

	private void startFreeSpin() throws XmlRpcException{
		doAction(1, "g:START_FREE_SPIN", "", 0.0);
	}
	
	 @Override
		protected void onActionReceived(GameAction action) {
		 if ("g:FREE_SPIN".equals(action.getDescription())) {
	    		this.freeSpin = true;
	    	}
	    }
	
	@Override
	protected void roundFinished(Map<String, Object> status) {
		this.freeSpin = false;
		
		if (status.get("free_spins_left") != null) {
			this.freeSpin = true;
		}
	}

	@Override
	protected void parseStatus(Map<String, Object> status) {
		if (status.get("free_spins_left") != null) {
			this.freeSpin = true;
		}
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()){                
			if (freeSpin) {
				startFreeSpin();
			} else if (getBalance() >= getBet()) {
				bet();
			} else {
				onInfo("Out of money!");
				break;
			}
		}
	}
}
