package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualOttoSlotPlayer extends AbstractVirtualPlayer {

	private int lines;
	private boolean freeSpin;
	private boolean bonusMode;
	private boolean[] openedBoxes = new boolean[7];

	public VirtualOttoSlotPlayer(ClientInterface client, int gameId) {
		super(client, gameId);
		this.lines = 0;
		this.freeSpin = false;
		this.bonusMode = false;
	}  

	private void clearBoxes() {
		for (int i=0; i < openedBoxes.length; i++) {
			openedBoxes[i] = false;
		}
	} 

	@Setting(description="Number of lines", value="25")
	public void setLines(int lines){
		this.lines = lines;
	}

	private void bet() throws XmlRpcException{
		doAction(1, "BET", String.valueOf(lines-1), getBet());
	}

	private void startFreeSpin() throws XmlRpcException{
		doAction(1, "g:START_FREE_SPIN", "", 0.0);
	}

	private void selectBox(int area) throws XmlRpcException {
		doAction(1, "g:BOX_SELECT", String.valueOf(area), 0.0);
	}
	
	@Override
	protected void roundFinished(Map<String, Object> status) {
		this.freeSpin = false;
		this.bonusMode = false;
		clearBoxes();
	}
	
	@Override
	protected void onActionReceived(GameAction action) {
		if ("g:TREASURE_BONUS".equals(action.getDescription())){
			this.bonusMode = true;
			clearBoxes();
		} else if ("g:BOX_SELECT".equals(action.getDescription())){
			int boxNo = Integer.parseInt(action.getArea());
			int moveRow = Integer.parseInt(action.getValue());

			openedBoxes[boxNo] = true;

			if (moveRow > 0) {
				clearBoxes();
			}

		} else if ("g:FREE_SPIN".equals(action.getDescription())) {
			this.freeSpin = true;
		}
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()){
			if (bonusMode) {
				int boxCount = 0;

				for (boxCount = 0; boxCount < openedBoxes.length; boxCount++) {
					if (!openedBoxes[boxCount]) {
						break;
					}
				}

				selectBox(boxCount);

			} else if (freeSpin){
				startFreeSpin();
			} else {
				if (getBalance() >= getBet()) {
					bet();
				} else {
					onInfo("Out of money!");
					break;
				}
			}

		}
	}
}
