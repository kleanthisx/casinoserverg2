/**
 * 
 */
package com.magneta.casino.virtualplayers.gui;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


/**
 * @author anarxia
 *
 */
public class Main {

    public static void main(String args[]) {
        /* Attempt to use the nimbus laf */
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
        }

        VirtualPlayerFrame vp = new VirtualPlayerFrame();
        vp.setSize(800, 600);
        vp.setLocationRelativeTo(null);
        vp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vp.setVisible(true);
    }
}
