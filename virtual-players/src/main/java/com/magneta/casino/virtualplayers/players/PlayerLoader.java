/**
 * 
 */
package com.magneta.casino.virtualplayers.players;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.clients.RemoteClient;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author anarxia
 *
 */
public class PlayerLoader {
	
	private static final String KEYSTORE = "client.jks";
	private static final String KEYSTORE_PASSWORD = "client";
	
	private static final Logger log = LoggerFactory.getLogger(PlayerLoader.class); 
	
    private void setSetting(VirtualPlayer player, String name, Class<?> type, Object value) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        
        if (value == null)
            return;
        
        Method m = player.getClass().getMethod("set" + Character.toUpperCase(name.charAt(0)) + name.substring(1), type);
                
        Setting annotation = m.getAnnotation(Setting.class);
        
        if (annotation != null) {
        	log.debug("Setting {}: type: {}, value: {}", new Object[] {name, type, value});
        	
            m.invoke(player, value);
        }
    }
    
    private void setSettings(VirtualPlayer player, Profile profile, Map<String, Object> settings) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        
        for (Entry<String,ProfileSetting> e: profile.getSettings().entrySet()) {
            ProfileSetting setting = e.getValue();
            log.debug("{}={}", setting.getName(), setting.getType());
            
            /* If the setting does not exist in the map (ie hidden) get it from the profile */
            if (settings.get(setting.getName()) == null) {
            	setSetting(player, setting.getName(), setting.getType(), profile.getSettings().get(setting.getName()).getDef());
            } else {
            	setSetting(player, setting.getName(), setting.getType(), settings.get(setting.getName()));
            }
        }
    }
    
    public VirtualPlayer load(Profile p, Server s, Map<String, Object> settings) throws Exception {
        @SuppressWarnings("unchecked")
        Class<? extends VirtualPlayer> playerClass = (Class<? extends VirtualPlayer>)Class.forName(p.getClassName());
        
        Constructor<? extends VirtualPlayer> constructor = playerClass.getConstructor(ClientInterface.class, int.class);
        
        RemoteClient client = new RemoteClient(s.getUrl(), s.getPort(), s.isSsl(), KEYSTORE, KEYSTORE_PASSWORD);
        
        VirtualPlayer player = constructor.newInstance(client, p.getGameId());
        
        setSettings(player, p, settings);
        
        return player;
    }
    
    public VirtualPlayer load(Profile p, ClientInterface client, Map<String, Object> settings) throws Exception {
        @SuppressWarnings("unchecked")
        Class<? extends VirtualPlayer> playerClass = (Class<? extends VirtualPlayer>)Class.forName(p.getClassName());
        
        Constructor<? extends VirtualPlayer> constructor = playerClass.getConstructor(ClientInterface.class, int.class);
        
        VirtualPlayer player = constructor.newInstance(client, p.getGameId());
        
        setSettings(player, p, settings);
        
        return player;
    }
}
