package com.magneta.casino.virtualplayers.players.poker;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.Card;
import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

public class VirtualIslandStudPokerPlayer extends AbstractVirtualPlayer {

	public static enum Strategy {
		ALWAYS_FOLD,
		ALWAYS_RAISE,
		RAISE_ON_QUALIFY
	}

	private double bet;
	private boolean betOnJackpot;
	private Strategy strategy;

	private Double jackpotBet;
	private RoundState state;

	private static class RoundState {
		private Double playerBet;
		private Double jackpotBet;
		private int[] playerCards;
		private int playerCombo;
	}


	public VirtualIslandStudPokerPlayer(ClientInterface client, int gameId) {
		super(client, gameId);
		this.bet = 1.0;
		this.betOnJackpot = false;
		this.strategy = Strategy.RAISE_ON_QUALIFY;

		this.state = new RoundState();
	}

	@Setting(description="Strategy")
	public void setStrategy(Strategy s) {
		this.strategy = s;
	}

	@Setting(description="Jackpot Bet")
	public void setJackpotBet(boolean betOnJackpot) {
		this.betOnJackpot = betOnJackpot;
	}

	@Override
	protected final void roundFinished(Map<String, Object> status) {
		this.state = new RoundState();
	}

	@Override
	protected final void parseStatus(Map<String,Object> status) {
		if (status.get("jackpot_bet") != null) {
			jackpotBet = (Double)status.get("jackpot_bet");
		}
	}

	@Override
	protected final void onActionReceived(GameAction action) {
		String actionDesc = action.getDescription();           

		if (actionDesc.equals("BET")) {
			state.playerBet = action.getAmount();
		} else if (actionDesc.equals("g:JACKPOT_BET")) {
			state.jackpotBet = action.getAmount();
		} else if (actionDesc.equals("g:HIT")) {
			if (action.getSeatId().equals(1)) {
				String[] sCards = action.getValue().split(" ");

				String[] sArea = action.getArea().split(" ");

				int combo = Integer.parseInt(sArea[0]);

				state.playerCards = new int[sCards.length];

				for (int i=0; i < sCards.length; i++) {
					state.playerCards[i] = Integer.valueOf(sCards[i]);
				}

				state.playerCombo = combo;
			}
		}
	}

	private boolean shouldRaise() {
		if (this.strategy == Strategy.ALWAYS_FOLD) {
			return false;
		} else if (this.strategy == Strategy.ALWAYS_RAISE) {
			return true;
		}

		if (state.playerCombo > 0) {
			return true;
		}

		boolean hasAce = false;
		boolean hasKing = false;

		for (int card: state.playerCards) {
			int num = Card.getCardNumber(card);
			if (num == Card.ACE) {
				hasAce = true;
			} else if (num == Card.KING) {
				hasKing = true;
			}
		}


		return hasAce && hasKing;
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()) {
			if (jackpotBet != null && this.betOnJackpot && state.playerBet == null && state.jackpotBet == null) {
				if (getBalance() >= jackpotBet) {
					jackpotBet();
				} else {
					onInfo("Out of money. Stopping");
					break;
				}
			}

			/* Send bet */
			if (state.playerBet == null) {
				bet();
			} else {
				if (shouldRaise()) {
					raise();
				} else {
					fold();
				}
			}
		}
	}

	private void bet() throws XmlRpcException {
		if (getBalance() >= bet) {
			doAction(1, "BET", "", bet);
		} else {
			onInfo("Out of money. Stopping");
			stop();
		}
	}

	private void fold() throws XmlRpcException{
		doAction(1, "g:FOLD", "", 0.0);
	}

	protected void raise() throws XmlRpcException {
		if (getBalance() >= state.playerBet * 2.0) {
			doAction(1, "g:RAISE", "", 0.0);
		} else {
			onInfo("Out of money. Stopping");			
			stop();
		}
	}

	protected void jackpotBet() throws XmlRpcException {
		if (getBalance() >= jackpotBet) {
			doAction(1, "g:JACKPOT_BET", "", jackpotBet);
		} else {
			onInfo("Out of money. Stopping");
			stop();
		}
	}
}