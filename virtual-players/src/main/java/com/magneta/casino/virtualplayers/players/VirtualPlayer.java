package com.magneta.casino.virtualplayers.players;

import org.apache.xmlrpc.XmlRpcException;

public interface VirtualPlayer {
	
	int getRoundsPlayed();
	
	void addListener(PlayerEventListener listener);
	void removeListener(PlayerEventListener listener);
	
	int login(String username, String password) throws XmlRpcException;
	void logout();
	
	Runnable start() throws XmlRpcException;
	void stop();
}
