package com.magneta.casino.virtualplayers.players;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.Card;
import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.Util;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

public abstract class AbstractVirtualPlayer implements VirtualPlayer, Runnable {

	private ClientInterface client;
	private int gameId;
	private byte[] tableId;
	protected int roundId;

	private double bet;
	private int roundsPlayed;
	private int totalRoundsToPlay;
	private boolean mustStop;

	private List<PlayerEventListener> listeners;

	public AbstractVirtualPlayer(ClientInterface client, int gameId) {
		this.client = client;
		this.bet = 5.0;
		this.roundsPlayed = 0;
		this.mustStop = false;
		this.gameId = gameId;
		listeners = new ArrayList<PlayerEventListener>();
	}

	protected abstract void play() throws XmlRpcException;
	
	/**
	 * @throws XmlRpcException  
	 */
	protected void parseStatus(Map<String,Object> status) throws XmlRpcException {
	}
	
	protected void roundFinished(Map<String, Object> status) {
	}
	
	/**
	 * @throws XmlRpcException  
	 */
	protected void onActionReceived(GameAction action) throws XmlRpcException {
	}
	
	private final void onStatus(Map<String, Object> status) throws XmlRpcException {
		if (status.get("ERROR") != null) {
			throw new XmlRpcException("Error during play: " + status.get("ERROR"));
		}
	
		parseStatus(status);
		
		List<GameAction> actions = getActions(status);
		
		for (GameAction a: actions) {			
			if (this.roundId != a.getRoundId()) {
				onRoundFinished(status);
				for (PlayerEventListener listener: listeners) {
					listener.roundStarted(Util.getLong(tableId), roundId);
				}
			}
			
			onActionReceived(a);
		}

		Integer rId = (Integer)status.get("curr_round_id");
		
		int newRoundId;
		
		if (rId != null) {
			newRoundId = rId;
		} else {
			newRoundId = this.roundId;
		}
		
		/* Client did not call round finished */
		if (newRoundId != this.roundId) {
			onRoundFinished(status);
			for (PlayerEventListener listener: listeners) {
				listener.roundStarted(Util.getLong(tableId), roundId);
			}
		}
	}

	protected final boolean shouldStopPlaying() { 
		return mustStop || (this.totalRoundsToPlay > 0 && (this.totalRoundsToPlay - this.roundsPlayed <= 0));
	}

	public final double getBet() {
		return bet;
	}

	@Setting(description="Bet",prompt=true)
	public void setBet(double bet) {
		this.bet = bet;
	}

	@Setting(description="Rounds (0 for unlimited)", prompt=true)
	public final void setRounds(int rounds) {
		this.totalRoundsToPlay = rounds;
	}

	/**
	 * Starts the player. 
	 * @throws XmlRpcException 
	 */
	@Override
	public final Runnable start() throws XmlRpcException {
			Map<String,Object> info = client.joinGame(gameId);

			if (info.get("ERROR") != null) {
				String error = "Unable to join game: "+ (String)info.get("ERROR");
				onError(error);
				return null;
			}

			this.tableId = (byte[])info.get("table_id");
			this.roundId = (Integer)info.get("curr_round_id");

			for (PlayerEventListener listener: listeners) {
				listener.roundStarted(Util.getLong(tableId), roundId);
			}
			
			onInfo("Joined table.");
			onStatus(info);

			return this;
	}

	/**
	 * Stops the player after the current round ends.
	 */
	@Override
	public final void stop() {
		mustStop = true;
	}

	@Override
	public final void run() {
		try {
			play();
		} catch (XmlRpcException e) {
			onError("Error : " + e.getMessage());
			throw new RuntimeException(e);
		}

		try {
			client.leaveTable(tableId);
		} catch (XmlRpcException e) {
			onError("Error leaving table: " + e.getMessage());
		}

		for (PlayerEventListener listener: listeners) {
			listener.finishedPlay();
		}
	}

	/**
	 * Logs the player into the system.
	 * @param username The player's username.
	 * @param password The player's password.
	 * @return Whether the user was successfully logged in.
	 * @throws XmlRpcException
	 */
	@Override
	public final int login(String username, String password) throws XmlRpcException {
		if (client.verify(1, 8, "VirtualPlayer")) {   
			return client.login(username, password);
		}
		return ClientInterface.SYSTEM_SUSPENDED;
	}

	@Override
	public final void logout() {
		try {
			client.logout();
		} catch (XmlRpcException e) {
		}
	}

	private final void onRoundFinished(Map<String, Object> status) {
		this.roundsPlayed++;

		Integer rId = (Integer)status.get("curr_round_id");
		if (rId != null) {
			this.roundId = rId;
		}
		
		roundFinished(status);
		
		for (PlayerEventListener listener: listeners) {
			listener.finishedRound(roundsPlayed, this.totalRoundsToPlay);
		}
	}

	@SuppressWarnings("unchecked")
	private List<GameAction> getActions(Map<String,Object> status) {

		List<GameAction> actions = new ArrayList<GameAction>(); 

		Object[] rawActions = (Object[])status.get("actions");
		if (rawActions == null) {
			return actions;
		}

		for(Object rawAction: rawActions) {
			actions.add(new GameAction((Map<String,Object>)rawAction));
		}

		return actions;
	}

	/**
	 * Sends the request to the server to do the given action for the player.
	 * @param seatId The ID of the player seat requesting the action.
	 * @param action The action (description) to perform. 
	 * @param actionArea The area of the action.
	 * @param amount The amount of the action
	 * @return The action results in a Map.
	 * @throws XmlRpcException
	 */
	protected final void doAction(int seatId, String action, String actionArea, double amount) throws XmlRpcException {
		Map<String,Object> res = client.doAction(tableId, roundId, seatId, action, 
				actionArea, amount);

		onStatus(res);
	}

	@Override
	public final void addListener(PlayerEventListener listener){
		listeners.add(listener);
	}

	@Override
	public final void removeListener(PlayerEventListener listener){
		listeners.remove(listener);
	}

	protected final String describeCard(int card) {
		String races[] = {" of Clubs", " of Diamonds", " of Hearts", " of Spades", " "};
		String cards[] = new String[14];

		cards[0] = "A";
		for (int i=2;i<=10; i++){
			cards[i-1] = String.valueOf(i);
		}

		cards[10] = "J";
		cards[11] = "Q";
		cards[12] = "K";
		cards[13] = "Joker";

		return cards[Card.getCardNumber(card)-1] + races[Card.getCardRace(card)];
	}

	protected final void onInfo(String info) {
		for (PlayerEventListener listener: listeners) {
			listener.onInfo(info);
		}
	}

	protected final void onError(String info) {
		for (PlayerEventListener listener: listeners) {
			listener.onError(info);
		}
	}
	
	protected double getBalance() throws XmlRpcException {
		return client.getBalance();
	}
	
	@Override
	public final int getRoundsPlayed() {
		return this.roundsPlayed;
	}
}
