package com.magneta.casino.virtualplayers.players;

public interface PlayerEventListener {
    
	public void roundStarted(long tableId, int roundId);
    public void finishedRound(int roundsPlayed, int totalRoundsToPlay);
    public void finishedPlay();
    public void onError(String error);
    public void onInfo(String info);
}
