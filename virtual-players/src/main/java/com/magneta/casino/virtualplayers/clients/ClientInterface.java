package com.magneta.casino.virtualplayers.clients;

import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

/**
 * ClientInterface defines the necessary functions for communicating with the server.
 */
public interface ClientInterface {

    public static final int INVALID_USERNAME_PASSWORD = 1;
    public static final int LOGIN_SUCCESS = 0;
    public static final int UNKNOWN_ERROR = -1;
    public static final int SYSTEM_SUSPENDED = -3;
    public static final int DATABASE_ERROR = -4;
    //private static final int WEB_LOGIN_REQUIRED = -5;
    public static final int ALREADY_LOGGED_IN = -6;
    public static final int USERS_LIMIT_EXCEEDED = -8;
    //private static final int INVALID_DEVICE_HASH = -9;
    public static final int BLOCKED_CARD = -9;

    
    boolean verify(int clientId, int version, String clientName) throws XmlRpcException;
    int login(String username, String password) throws XmlRpcException;
    boolean logout() throws XmlRpcException;
    
    double getBalance() throws XmlRpcException;
    
    Map<String,Object> joinGame(int gameId) throws XmlRpcException;
    boolean leaveTable(byte[] tableId) throws XmlRpcException;
    
    Map<String, Object> doAction(byte[] tableId, int roundId, int seatId,
    		String action, String actionArea, double amount) throws XmlRpcException; 

    void destroy();
}
