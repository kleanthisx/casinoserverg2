package com.magneta.casino.virtualplayers.players.poker;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TexasReqUtil {
	
	private static final Logger log = LoggerFactory.getLogger(TexasReqUtil.class);
    
	private TexasReqUtil() {
	}
	
	private static Map<String,String[]> cachedResults = new ConcurrentHashMap<String,String[]>();

	public static String[] requestProbabs(String playercards,String commonCards){
		
		String cacheKey = playercards+ "|" + commonCards;
		
		String[] res =  cachedResults.get(cacheKey);
		if (res != null) {
			return res;
		}
		
		String content = null;
		String[] r = null;
		try {
			// Construct data
			String data = URLEncoder.encode("player", "UTF-8") + "=" + URLEncoder.encode(playercards,"UTF-8");
			data += "&" + URLEncoder.encode("board", "UTF-8") + "=" + URLEncoder.encode(commonCards,"UTF-8");
			// Send data
			URL url = new URL("http://www.beatingbonuses.com/holdembonus_exec.php");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();
			wr.close();

			// Receave data
			content = readInputStreamAsString(conn.getInputStream());
			content.trim();
			r = content.split("\n");
			
			cachedResults.put(cacheKey, r);
			
		} catch (Exception e) {
			log.info("There was something thatwend very very wrong with this request");
		}
		return r;
	}
	
	
	public static String readInputStreamAsString(InputStream in) 
    throws IOException {

    BufferedInputStream bis = new BufferedInputStream(in);
    ByteArrayOutputStream buf = new ByteArrayOutputStream();
    int result = bis.read();
    while(result != -1) {
      byte b = (byte)result;
      buf.write(b);
      result = bis.read();
    }
    
    return buf.toString();
}


}