/**
 * 
 */
package com.magneta.casino.virtualplayers.players;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * @author anarxia
 *
 */
public class ServerLoader {
    private static final String SERVERS_FILE = "servers.xml";
    
    public Server[] loadServers(){
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(SERVERS_FILE);

        try {
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);

            NodeList serverNodes = doc.getElementsByTagName("server");
            
            Server[] arr = new Server[serverNodes.getLength()]; 
            for (int i = 0; i < serverNodes.getLength(); i++) {
                Element serv = (Element) serverNodes.item(i);
                NodeList servList = serv.getChildNodes();
                
                arr[i] = new Server();
                
                for (int j = 0; j < servList.getLength(); j++) {
                    Node oNode = servList.item(j);

                    if (oNode.getNodeName().equals("name")){
                        arr[i].setName(oNode.getTextContent());
                    } else if (oNode.getNodeName().equals("url")){
                        arr[i].setUrl(oNode.getTextContent());
                    } else if (oNode.getNodeName().equals("ssl")){
                        arr[i].setSsl(Boolean.parseBoolean(oNode.getTextContent()));
                    } else if (oNode.getNodeName().equals("port")){
                        arr[i].setPort(Integer.parseInt(oNode.getTextContent()));
                    }
                }
            }
            return arr;
        }
        catch (Exception e) { 
            System.err.println("Error reading servers file: "+e.getMessage());
        }
        return null;
    }
}
