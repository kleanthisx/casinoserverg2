package com.magneta.casino.virtualplayers.clients;

import java.util.ArrayList;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.xmlrpc.client.SocketXmlRpcClient;

public class RemoteClient implements ClientInterface {
    private SocketXmlRpcClient client;
    
    private Object execute(String func) throws XmlRpcException {

        synchronized(client) {
            return client.execute(func, new ArrayList<Object>());
        }
    }
    
    private Object execute(String func, Object... args) throws XmlRpcException {
        
        synchronized(client) {
            return client.execute(func, args);
        }
    }
        
    public RemoteClient(String host, int port, boolean useSSL) { 
       client = new SocketXmlRpcClient(host, port, useSSL);
    }
    
    public RemoteClient(String host, int port, boolean useSSL, String trustStore, String trustPass) { 
        client = new SocketXmlRpcClient(host, port, useSSL, trustStore, trustPass);
    }
    
    @Override
    public int login(String username, String password) throws XmlRpcException{
        Integer status = (Integer)execute("User.loginWithStatus", username, password, "");
        
        if (status == null) {
            return SYSTEM_SUSPENDED;
        }
        
        return status;
    }
    
    @Override
    public boolean logout() throws XmlRpcException {
        return (Boolean)execute("User.logout");
    }

    @Override
    public double getBalance() throws XmlRpcException {
        return (Double)execute("User.getBalance");
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> joinGame(int gameId) throws XmlRpcException {
        return (Map<String,Object>)execute("Table.joinGame", gameId);
    }

    @Override
    public boolean leaveTable(byte[] tableId) throws XmlRpcException {
        return (Boolean)execute("Table.leaveTable", tableId);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> doAction(byte[] tableId, 
            int roundId, int seatId, String action, String actionArea, double amount) throws XmlRpcException{

    	if (actionArea == null) {
    		actionArea = "";
    	}
    	
        return (Map<String,Object>)execute("Round.doAction", tableId,
                roundId, seatId, action, actionArea, amount);
    }

    @Override
    public boolean verify(int clientId, int version, String clientIdentifier) throws XmlRpcException {
        return (Boolean)execute("Client.verify", clientId, version, clientIdentifier);
    }
    
    @Override
    public void destroy() {
        client.destroy();
    }

}