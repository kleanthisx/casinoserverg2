package com.magneta.casino.virtualplayers.players.blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.Card;
import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.Log;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * VirtualBJPlayer is an automatic Blackjack player. This class provides the basis for
 * creating any type of player and defines the playing behaviour of the dealer-like play
 * strategy.
 */
public class VirtualBlackjackPlayer extends AbstractVirtualPlayer { 
    
    /**
     * SeatStatus keeps the state of each seat in the round where a VirtualBJPlayer participates.
     */
    protected class SeatStatus {
        public boolean ownSeat;
        public final List<Integer> cards[];
        private final boolean finished[];
        private boolean hasDoubled;
        protected boolean placedBet;
        public boolean rejectPending;
        
        /**
         * Constructs a new SeatStatus.
         */
        @SuppressWarnings("unchecked")
        public SeatStatus(){
            ownSeat = false;
            cards = new List[2];
            finished = new boolean[2];
            finished[0] = false;
            finished[1] = false;
            cards[0] = new ArrayList<Integer>();
            hasDoubled = false;
            placedBet = false;
            rejectPending = false;
        }
       
        /**
         * Returns whether the seat has doubled its bet.
         * @return Whether the seat has doubled its bet.
         */
        public boolean hasDoubled() {
            return this.hasDoubled;
        }
       
        /**
         * Returns the overall sum of the current seat's given hand.
         * @param hand The hand whose cards to sum.
         * @return the overall sum of the current seat's given hand.
         */
        public int sumCards(int hand){
            return BlackjackDeck.sumCards(cards[hand]);
        }
        
        /**
         * Returns the overall sum of the current seat's default hand.
         * @return the overall sum of the current seat's default hand.
         */
        public int sumCards() {
            return BlackjackDeck.sumCards(cards[0]);
        }
        
        public void hit(int hand, int card) {
            cards[hand].add(card);
        }
        
        public void stand(int hand) {
            finished[hand] = true;
        }
        
        public void split() {
            cards[1] = new ArrayList<Integer>();
            cards[1].add(cards[0].remove(1));
        }
        
        public boolean isFinished(int hand) {
            return finished[hand];
        }
    }

    protected final SeatStatus seats[];
    private int seatsToPlay;
    
    /**
     * Constructs a new VirtualBJPlayer.
     * @param client The client to be used by the player to connect to the server.
     */
	public VirtualBlackjackPlayer(ClientInterface client, int gameID) {
        super(client, gameID);
        seats = new SeatStatus[6];
        seats[0] = new SeatStatus();
        seatsToPlay = 1;
	}
    
	@Setting(description="Seats to play", value="1")
	public void setSeats(int seatsToPlay){
	    this.seatsToPlay = seatsToPlay;
	}
	
    /**
     * Clears all the seat statuses and the round status.
     */
    @Override
	protected final void roundFinished(Map<String, Object> status) {
    	seats[0] = new SeatStatus();
        for (int i=1;i < seats.length;i++) {
        	seats[i] = null;
        }
    }

    private void play(int seatId) throws XmlRpcException {
        
        SeatStatus mySeat = seats[seatId];

        if (mySeat.rejectPending) {
            reject(seatId);
        }
        
		while((!mySeat.isFinished(0)) && (mySeat.sumCards() < 17)){
            hit(seatId,0);
		}
        
		if (!mySeat.isFinished(0)) {
            stand(seatId,0);
        }
    }
	
	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()) {
            for (int i=1;i<= seatsToPlay;i++){
                bet(i);
            }
            deal();
            
            for (int i=1;i<= seatsToPlay;i++){
                if (seats[i].isFinished(0) && Card.getCardNumber(seats[0].cards[0].get(0)) == Card.ACE) {
                    seats[i].rejectPending = true;
                }
                
                play(i);
            }
        }
	}

    @Override
    protected final void parseStatus(Map<String,Object> status) {
    }

    /**
     * Updates the seat statuses given the latest list of player actions missed.
     * @param actions The list of player missed actions.
     */
    @Override
    protected final void onActionReceived(GameAction action) {
    	int seat = action.getSeatId();
    	String actionDesc = action.getDescription();           

    	if (actionDesc.equals("g:HIT") || actionDesc.equals("g:FLIP")) {
    		if (seats[seat] == null)
    			throw new IllegalStateException("Seat "+seat+" is not occupied but receives cards!");

    		int val = Integer.parseInt(action.getValue());

    		int area = 0;

    		if (seat != 0) {
    			area = Integer.parseInt(action.getArea());
    		}

    		seats[seat].hit(area, val);
    	} else if (actionDesc.equals("BET")) {
    		seats[seat] = new SeatStatus();

    		if (action.isOwnAction()) {
    			seats[seat].ownSeat = true;
    		}

    		Log.debug(seat+" PLACED BET");

    		if (seats[seat] == null)
    			throw new IllegalStateException("Seat "+seat+" is not occupied but placed bet!");
    		
    		seats[seat].placedBet = true;

    	} else if (actionDesc.equals("START")){
    		int seatCount = 0;
    		for (int i = 1; i < seats.length; i++) {
    			if (seats[i] != null) {
    				seatCount++;
    			}
    		}
    		Log.debug(this + ": Occupied seats:" + seatCount);
    	} else if (actionDesc.equals("g:STAND")) {

    		if (seats[seat] == null)
    			throw new IllegalStateException("Seat "+seat+" is not occupied but standed!");

    		int val = 0;

    		if (seat != 0) {
    			val = Integer.parseInt(action.getArea());
    		}

    		seats[seat].stand(val);
    	} else if (actionDesc.equals("g:SPLIT")) {
    		seats[seat].split();

    	} else if (actionDesc.equals("g:DOUBLE")) {
    		seats[seat].hasDoubled = true;
    	} else if (actionDesc.equals("g:INSURANCE")) {
    		seats[seat].rejectPending = false;
    	} else if (actionDesc.equals("g:REJECT")) {
    		seats[seat].rejectPending = false;
    	}
    }
    
    /**
     * Perfmorms the bet action.
     * @param seatID The seat for which the bet is set.
     * @return Whether the bet was placed successfully.
     * @throws XmlRpcException
     */
    private void bet(int seatId) throws XmlRpcException {
        doAction(seatId, "BET", "", getBet());
    }
    
    /**
     * Performs the hit action.
     * @param seatID The seat to hit for.
     * @param hand The hand to hit for.
     * @return Whether the action was performed successfully.
     * @throws XmlRpcException
     */
    protected void hit(int seatId, int hand) throws XmlRpcException{
        doAction(seatId, "g:HIT", String.valueOf(hand), 0.0);
    }
    
    /**
     * Performs the stand action.
     * @param seatID The seat to stand.
     * @param hand The hand to stand.
     * @return Whether the action was performed successfully.
     * @throws XmlRpcException
     */
    protected void stand(int seatID,int hand) throws XmlRpcException{
    	doAction(seatID, "g:STAND", String.valueOf(hand), 0.0);
    }
    
    /**
     * Performs the double action.
     * @param seatID The seat to double for.
     * @param hand The hand to double for.
     * @return Whether the action was performed successfully.
     * @throws XmlRpcException
     */
    protected void doubleBet(int seatID,int hand) throws XmlRpcException{
        doAction(seatID, "g:DOUBLE", String.valueOf(hand), 0.0);
    }
    
    /**
     * Performs the split action.
     * @param seatID The seat to split.
     * @return Whether the action was performed successfully.
     * @throws XmlRpcException
     */
    protected void split(int seatId) throws XmlRpcException{
    	doAction(seatId, "g:SPLIT", null, 0.0);
    }
    
    /**
     * Performs the insurance action.
     * @param seatID The seat to insure.
     * @return Whether the action was performed successfully.
     * @throws XmlRpcException
     */
    protected void insurance(int seatID) throws XmlRpcException {
        doAction(seatID, "g:INSURE", "", 0.0);
    }
    
    /**
     * Performs the reject action.
     * @param seatID The seat which rejects action.
     * @return Whether the action was performed successfully.
     * @throws XmlRpcException
     */
    protected void reject(int seatID) throws XmlRpcException {
    	doAction(seatID, "g:REJECT", null, 0.0);
    }
    
    protected void deal() throws XmlRpcException {
        doAction(1, "g:DEAL", "", 0.0);
    }
}
