/**
 * 
 */
package com.magneta.casino.virtualplayers.players.slots;

import java.util.Map;
import java.util.Random;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.GameAction;
import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualPirates2SlotPlayer extends AbstractVirtualPlayer {
    
    private int lines;
    private boolean betPlaced;
    private boolean alwaysTakeScore;
    
    public VirtualPirates2SlotPlayer(ClientInterface client, int gameID) {
        super(client, gameID);
        this.lines = -1;
    }   
    
    @Setting(description="Number of lines",prompt=false)
    public void setLines(int lines){
        this.lines = lines;
    }
    
    @Override
	protected void roundFinished(Map<String, Object> status) {
    	betPlaced = false;
    }
    
    private void bet() throws XmlRpcException{
        String strLines = null;
        if (lines < 0){
            strLines = "";
        } else {
            strLines = String.valueOf(lines-1);
        }
        
        doAction(1, "BET", strLines, getBet());
    }

    @Override
    protected void onActionReceived(GameAction action) {
    	if ("BET".equals(action.getDescription())) {
    		betPlaced = true;
    	}
    }
    
    @Override
    public void play() throws XmlRpcException {
        
        while (!shouldStopPlaying()){
                if (betPlaced){
                    if (isAlwaysTakeScore()){
                        takeScore();
                    } else {
                        Random r = new Random();
                        if (r.nextInt(100) > 50){
                            if (r.nextInt(100) > 50){
                                black();
                            } else {
                                red();
                            }
                        } else {
                            race(r.nextInt(4));
                        }
                    }
                } else {
                    if (getBalance() >= getBet()) {
                        bet();
                    } else {
                        onInfo("Out of money!");
                        break;
                    }
                }
            
        }
    }
    
    protected void red() throws XmlRpcException{
        doAction(1, "g:RED", "", 0.0);
    }
    
    protected void black() throws XmlRpcException{
        doAction(1, "g:BLACK", "", 0.0);
    }
    
    protected void race(int race) throws XmlRpcException{
        doAction(1, "g:RACE", String.valueOf(race), 0.0);
    }
    
    protected void takeScore() throws XmlRpcException{
        doAction(1, "g:TAKE_SCORE", "", 0.0);
    }
    
    @Setting(description="Always take score")
    public void setAlwaysTakeScore(boolean alwaysTakeScore) {
        this.alwaysTakeScore = alwaysTakeScore;
    }

    private boolean isAlwaysTakeScore() {
        return alwaysTakeScore;
    }
}
