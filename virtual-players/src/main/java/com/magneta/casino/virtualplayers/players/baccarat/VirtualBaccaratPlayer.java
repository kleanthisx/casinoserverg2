/**
 * 
 */
package com.magneta.casino.virtualplayers.players.baccarat;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualBaccaratPlayer extends AbstractVirtualPlayer {
    
    private String bets;
    
    /**
     * @param client
     */
    public VirtualBaccaratPlayer(ClientInterface client, int gameId) {
        super(client, gameId);
    }
    
    @Override
    @Setting(description="Bet",prompt=false, value="0.0")
	public void setBet(double bet) {
    }
    

    @Setting(description="Bets",value="1 1 1.00")
    public void setBets(String bets){
        this.bets = bets;
    }

    private void bet(String betStr) throws XmlRpcException {
        doAction(1, "BET", betStr, 0.0);
    }

    @Override
    public void play() throws XmlRpcException {
    	while (!shouldStopPlaying()){
    		DecimalFormat f = new DecimalFormat("0.00");
    		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    		symbols.setDecimalSeparator('.');
    		symbols.setGroupingSeparator(',');
    		f.setDecimalFormatSymbols(symbols);

    		if (getBalance() >= getBet()) {
    			if (this.bets == null || bets.length() == 0){
    				String allBets = "";
    				for (int i=0;i<37;i++){
    					if (i ==0){
    						allBets += "0_"+i+" "+f.format(getBet());
    					} else {
    						allBets += ";0_"+i+" "+f.format(getBet());
    					}
    				}
    				bet(allBets);
    			} else {
    				bet(bets);
    			}

    		} else {
    			onInfo("Out of money!");
    			break;
    		}
    	}
    }
}
