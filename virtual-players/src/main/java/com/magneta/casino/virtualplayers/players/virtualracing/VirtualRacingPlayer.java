/**
 * 
 */
package com.magneta.casino.virtualplayers.players.virtualracing;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import com.magneta.casino.virtualplayers.clients.ClientInterface;
import com.magneta.casino.virtualplayers.players.AbstractVirtualPlayer;
import com.magneta.casino.virtualplayers.players.annotations.Setting;

/**
 * @author User
 *
 */
public class VirtualRacingPlayer extends AbstractVirtualPlayer implements Runnable {

	public enum Mode {
		FAVORITES
	}
	
	private Object[][] entities;
	private EntityOdds[] odds;
	private Mode mode;

	private class EntityOdds {
		private int betType;
		private int[] betEntities;
		private double betOdds;

		public EntityOdds(int betType, int[] betHorses, double betOdds) {
			this.betType = betType;
			this.betEntities = betHorses;
			this.betOdds = betOdds;
		}
	}

	/**
	 * @param client
	 */
	public VirtualRacingPlayer(ClientInterface client, int gameID) {
		super(client, gameID);
		this.entities = null;
	}

	@Setting
	public void setMode(Mode mode){
		this.mode = mode;
	}
	
	@Override
	protected void roundFinished(Map<String,Object> status) {
		entities = null;
		odds = null;
		parseEntities(status);
	}

	protected void parseEntities(Map<String, Object> status) {
		if (status.get("runners") != null) {
			Object o = status.get("runners");

			if (o instanceof Object[]){
				Object[] res = (Object[])status.get("runners");

				entities = new Object[res.length][];

				for (int i=0;i<entities.length;i++){
					Object[] entity = (Object[])res[i];
					entities[i] = entity;
				}
			} else if (o instanceof String){

			}
		}
		
		if (status.get("odds") != null) {
			Object o = status.get("odds");

			String[] oddsStrs = ((String)o).split(";");
			odds = new EntityOdds[oddsStrs.length];

			for (int i=0;i< oddsStrs.length; i++){
				String[] vals = oddsStrs[i].split(" ");
				String[] horses = vals[1].split("_");
				int[] intHorses = new int[horses.length];
				for (int j = 0;j<horses.length;j++){
					intHorses[j] = Integer.parseInt(horses[j]);
				}
				odds[i] = new EntityOdds(Integer.parseInt(vals[0]),intHorses, Double.parseDouble(vals[2]));
			}
		}
	}

	@Override
	protected void parseStatus(Map<String, Object> status) {
		parseEntities(status);
	}

	private void startRace() throws XmlRpcException{
		doAction(1, "g:START_RACE", "", 0.0);
	}

	private void bets(String betsStr) throws XmlRpcException{
		doAction(1, "g:BETS", betsStr, 0.0);
	}

	@Override
	public void play() throws XmlRpcException {
		while (!shouldStopPlaying()){
			if (getBalance() < getBet()) {
				onInfo("Out of money");
				break;
			}
			switch(mode){

			case FAVORITES:{
				Map<Integer,Double> minOdds = new HashMap<Integer,Double>();

				for (EntityOdds curr: odds){
					if (minOdds.get(curr.betType) != null){
						if (minOdds.get(curr.betType) > curr.betOdds){
							minOdds.put(curr.betType, curr.betOdds);
						}
					} else {
						minOdds.put(curr.betType, curr.betOdds);
					}
				}

				Iterator<Integer> it = minOdds.keySet().iterator();

				String bets = "";
				DecimalFormat f = new DecimalFormat("0.00");
				DecimalFormatSymbols symbols = new DecimalFormatSymbols();
				symbols.setDecimalSeparator('.');
				symbols.setGroupingSeparator(',');
				f.setDecimalFormatSymbols(symbols);

				while(it.hasNext()){
					int key = it.next();

					for (EntityOdds curr: odds){
						if ((curr.betOdds == minOdds.get(key)) && (curr.betType == key)){
							String str = "";
							for (int j=0;j<curr.betEntities.length;j++){
								if (j == 0){
									str += curr.betEntities[j];
								} else {
									str += /*"-"*/"_" + curr.betEntities[j];
								}
							}

							bets += curr.betType+" "+str+(f.format(this.getBet()))+";";
							//bet(curr.betType, str);
						}
					}
				}
				bets(bets);
			}
			break;


			default:


				double minOdds = Double.MAX_VALUE; 

				for (EntityOdds curr: odds){
					if (curr.betOdds < minOdds){
						minOdds = curr.betOdds;
					}
				}

				int horsesToBet = (int)Math.ceil(minOdds);
				horsesToBet = (horsesToBet < entities.length? horsesToBet : entities.length);

				EntityOdds[] oddsCpy = new EntityOdds[entities.length];
				System.arraycopy(odds, 0, oddsCpy, 0, oddsCpy.length);

				//sort the scores
				int n = oddsCpy.length;
				for (int pass=1; pass < n; pass++) {
					for (int i=0; i < n-pass; i++) {
						if (oddsCpy[i].betOdds > oddsCpy[i+1].betOdds) {
							EntityOdds tmp = oddsCpy[i];
							oddsCpy[i] = oddsCpy[i+1];
							oddsCpy[i+1] = tmp;
						}
					}
				}

				String bets = "";
				DecimalFormat f = new DecimalFormat("0.00");
				DecimalFormatSymbols symbols = new DecimalFormatSymbols();
				symbols.setDecimalSeparator('.');
				symbols.setGroupingSeparator(',');
				f.setDecimalFormatSymbols(symbols);

				for (int i=0;i<horsesToBet;i++){
					String str = "";
					for (int j=0;j<oddsCpy[i].betEntities.length;j++){
						if (j == 0){
							str += oddsCpy[i].betEntities[j];
						} else {
							str += /*"-"*/"_" + oddsCpy[i].betEntities[j];
						}
					}

					bets += oddsCpy[i].betType+" "+str+" "+(f.format(this.getBet()))+";";
				}

				bets(bets);
			}

			startRace();

		}
	}
}
