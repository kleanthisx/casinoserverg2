/**
 * 
 */
package com.magneta.casino.virtualplayers.players.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @author anarxia
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Setting {
    String value() default "";
    String description() default "";
    boolean prompt() default true;
}
