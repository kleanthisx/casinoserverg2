# Casino Server #
This project contains all server-side parts of the casino.

## Sub-Projects ##
* gaming-server - Game Server.
* backoffice - Web-Application for system administration.
* partner-webapp - Web-Application for affiliates.

## Build Prerequisites ##
* JDK 7 or above (OpenJDK7 is ok)
* Maven 3.1 or above

### Debian ###
* apt-get install openjdk-7-jdk
* Install maven manually from http://maven.apache.org/download.cgi

## Build ##
The build uses maven and it produces war for webapps, tar.gz for game-server and
zip for virtual players

>./build_all.sh

### Debian package for game-server ###
fakeroot dpkg-buildpackage -b -uc

## Build site for all projects ##

* Create temporary directory
* Enter temporary directory
* mvn -f casino-server/pom.xml site site:stage -DstagingDirectory=&lt;temp full path/casino-server&gt;

Please note that if you forget to append casino-server maven will fill the parent directory with sub-project files.
