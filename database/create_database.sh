#!/bin/sh

set -e

if [ -z "$1" ] ; then
  echo "Usage: ./create_database <dbname>"
  exit 1
fi

dbname=$1
createSql="CREATE DATABASE $dbname ENCODING = 'UTF8' LC_COLLATE = 'en_GB.UTF-8' LC_CTYPE = 'en_GB.UTF-8';"

psql --dbname="postgres" --command="$createSql"

for f in $(ls *.sql) ;
do
  psql --dbname="$dbname" --file="$f"
done

psql "$dbname" << EOF
  VACUUM ANALYZE;
EOF

