--
-- System administator role
--
INSERT INTO roles(role_id, role_desc) VALUES (nextval('role_id_seq'), 'System administrator');
INSERT INTO role_privileges(role_id, privilege_key, allow_write) VALUES(currval('role_id_seq'), 'BACKOFFICE_ENTRY', true);
INSERT INTO role_privileges(role_id, privilege_key, allow_write) VALUES(currval('role_id_seq'), 'ROLE', true);

--
-- Default administator. You MUST change the password as soon as the backoffice is set-up.
-- Username: administrator 
-- Password: casinoadmin
INSERT INTO users (user_id, username, password_hash, password_salt, is_active, first_name, last_name, country, time_zone, register_date, user_type, closed)
VALUES (nextval('user_id_seq'), 'administrator', '130B2D9B63E049D6EBB9D1C1659B0299274055C0', 'pjnim0q7', true, 'System', 'Administator', 'CY', 'Europe/Nicosia', now_utc(), 3, false);

-- Assign administrator to role
INSERT INTO user_roles(user_id, role_id) VALUES (1,1);
