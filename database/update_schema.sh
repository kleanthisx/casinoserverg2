#!/bin/sh

#
# Gets the schema from the given database
#

set -e

if [ -z "$1" ] ; then
  echo "Usage: ./update_schema <dbname>"
  exit 1
fi

dbname=$1

pg_dump --schema-only --no-privileges --no-owner --no-tablespaces --encoding="UTF-8" --dbname="$dbname"
