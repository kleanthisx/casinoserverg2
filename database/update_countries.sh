#!/bin/sh

# Generates sql script to insert all countries in the database.
# The script retrieves the info from iso.org.
# ISO sends all country names in uppercase and the script converts them to title case.

set -e

url="http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements_txt.htm"

curl --silent "$url" | tr -d '\r' | grep -v "^[[:space:]]*$" | grep -v "\;ISO 3166-1-alpha-2 code" | \
	sed 's/.*/\L&/; s/[[:alnum:]]*/\u&/g' | \
	sed 's/\x27S /\x27s /g' | \
	sed 's/ Of/ of/g' | \
	sed 's/ The/ the/g' | \
	sed 's/, the /, The /g' | \
	sed 's/ And/ and/g' | \
	sed 's/\x27/\x27\x27/g' | \
	awk -F";" '{ printf "INSERT INTO countries(country_code, country_name) VALUES(\x27%s\x27, \x27%s\x27);\n", toupper($2), $1; next }'
