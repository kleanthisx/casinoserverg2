--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: casino; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON DATABASE casino IS 'Casino Development Database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: affiliate_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION affiliate_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
     IF NEW.affiliate_rate IS DISTINCT FROM OLD.affiliate_rate THEN  
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	 VALUES (NEW.affiliate_id, 'affiliate_rate', OLD.affiliate_rate, NEW.affiliate_rate, NEW.modifier_user);
     END IF;

     IF NEW.approved IS DISTINCT FROM OLD.approved THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	 VALUES (NEW.affiliate_id, 'approved', OLD.approved, NEW.approved, NEW.modifier_user);
     END IF;

     IF NEW.payment_period IS DISTINCT FROM OLD.payment_period THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	 VALUES (NEW.affiliate_id, 'payment_period', OLD.payment_period, NEW.payment_period, NEW.modifier_user);
     END IF;

     IF NEW.allow_free_credits IS DISTINCT FROM OLD.allow_free_credits THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	 VALUES (NEW.affiliate_id, 'allow_free_credits', OLD.allow_free_credits, NEW.allow_free_credits, NEW.modifier_user);
     END IF;

     IF NEW.free_credits_rate IS DISTINCT FROM OLD.free_credits_rate THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	 VALUES (NEW.affiliate_id, 'free_credits_rate', OLD.free_credits_rate, NEW.free_credits_rate, NEW.modifier_user);
     END IF;

     IF NEW.allow_full_free_credits IS DISTINCT FROM OLD.allow_full_free_credits THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	 VALUES (NEW.affiliate_id, 'allow_full_free_credits', OLD.allow_full_free_credits, NEW.allow_full_free_credits, NEW.modifier_user);
     END IF;

     IF NEW.require_approval IS DISTINCT FROM OLD.require_approval THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	VALUES (NEW.affiliate_id, 'require_approval', OLD.require_approval, NEW.require_approval, NEW.modifier_user);
     END IF;

     IF NEW.requires_super_approval IS DISTINCT FROM OLD.requires_super_approval THEN
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	VALUES (NEW.affiliate_id, 'requires_super_approval', OLD.requires_super_approval, NEW.requires_super_approval, NEW.modifier_user);
     END IF;

     IF NEW.sub_levels IS DISTINCT FROM OLD.sub_levels THEN  
	INSERT INTO affiliate_changes(affiliate_id, field_name, old_value, new_value, modifier_user)
	VALUES (NEW.affiliate_id, 'sub_levels', OLD.sub_levels, NEW.sub_levels, NEW.modifier_user);
     END IF;

     NEW.modifier_user := NULL;

     RETURN NEW;
END;$$;


--
-- Name: change_game_setting(integer, character varying, character varying, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION change_game_setting(g_id integer, set_key character varying, new_val character varying, u_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
    INSERT INTO game_settings_changes(game_id, setting_key, change_user, new_value, old_value)
     VALUES (g_id, set_key, u_id, new_val, (SELECT setting_value FROM game_settings WHERE setting_key = set_key AND game_id = g_id));

    IF new_val IS NULL THEN
	DELETE FROM game_settings WHERE setting_key = set_key AND game_id = g_id;
    ELSE
        UPDATE game_settings
          SET setting_value = new_val
        WHERE setting_key = set_key
        AND game_id = g_id;

        IF NOT FOUND THEN
	  INSERT INTO game_settings(game_id, setting_key, setting_value)
          VALUES(g_id, set_key, new_val);
        END IF;
    END IF;
END;$$;


--
-- Name: change_jackpot_properties(integer, integer, integer, numeric, numeric, numeric, numeric, integer, bigint, boolean, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION change_jackpot_properties(jpot_id integer, conf_id integer, p_count integer, init_amt numeric, b_contrib numeric, b_amt numeric, max_amt numeric, d_number integer, u_id bigint, auto_jpot boolean, exp_rate numeric, min_pyt numeric, min_jpot_bal numeric) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
    old_p_count INTEGER;
    old_init_amt NUMERIC;
    old_b_contrib NUMERIC;
    old_b_amt NUMERIC;
    old_max_amt NUMERIC;
    old_d_number INTEGER;
    old_auto_jpot BOOLEAN;
    old_min_pyt NUMERIC;
    old_min_jpot_bal NUMERIC;
BEGIN
    IF jpot_id IS NOT NULL THEN
        SELECT initial_amount, maximum_amount, pay_count, auto, draw_number, bet_amount, bet_contrib, min_payout, min_jackpot_balance
        INTO old_init_amt, old_max_amt, old_p_count, old_auto_jpot, old_d_number, old_b_amt, old_b_contrib, old_min_pyt, old_min_jpot_bal
        FROM jackpots
        WHERE jackpot_id = jpot_id;
        
        UPDATE jackpots
        SET initial_amount = init_amt, maximum_amount = max_amt, pay_count = p_count, auto = auto_jpot, draw_number = d_number, bet_amount = b_amt, 
         bet_contrib = b_contrib, min_payout = min_pyt, min_jackpot_balance = min_jpot_bal
	WHERE jackpot_id = jpot_id;
    ELSIF conf_id IS NOT NULL THEN
        SELECT initial_amount, maximum_amount, draw_number, bet_contrib, min_payout, min_jackpot_balance
        INTO old_init_amt, old_max_amt, old_d_number, old_b_contrib, old_min_pyt, old_min_jpot_bal
        FROM jackpot_configs
        WHERE config_id = conf_id;
    
	UPDATE jackpot_configs
	SET initial_amount = init_amt, bet_contrib = b_contrib, maximum_amount = max_amt, draw_number = d_number,
	 min_payout = min_pyt, min_jackpot_balance = min_jpot_bal
        WHERE config_id = conf_id;
    ELSE
        RAISE EXCEPTION 'Neither a jackpot id or a config id was provided!';
    END IF;

    INSERT INTO jackpot_properties_changes(jackpot_id, jackpot_config_id, 
     new_pay_count, new_initial_amount, new_bet_contrib, new_bet_amount, new_maximum_amount, new_draw_number, new_auto, new_min_payout,
     new_min_jackpot_balance, 
     old_pay_count, old_initial_amount, old_bet_contrib, old_bet_amount, old_maximum_amount, old_draw_number, old_auto, old_min_payout,
     old_min_jackpot_balance, user_id)
    VALUES (jpot_id, conf_id, p_count, init_amt, b_contrib, b_amt, max_amt, d_number, auto_jpot, min_pyt, min_jpot_bal, old_p_count, old_init_amt, 
     old_b_contrib, old_b_amt, old_max_amt, old_d_number, old_auto_jpot, old_min_pyt, old_min_jpot_bal, u_id);
END;$$;


--
-- Name: change_system_setting(character varying, character varying, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION change_system_setting(set_key character varying, new_val character varying, u_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
    UPDATE system_settings
	SET setting_value = new_val
	WHERE setting_key = set_key;

    IF FOUND THEN
	INSERT INTO system_settings_changes(setting_key, change_user, new_value, old_value)
	VALUES (set_key, u_id, new_val, (SELECT setting_value FROM system_settings WHERE setting_key = set_key));
    ELSE
	INSERT INTO system_settings(setting_key, setting_value)
	VALUES(set_key, new_val);

	INSERT INTO system_settings_changes(setting_key, change_user, new_value, old_value)
	VALUES (set_key, u_id, new_val, NULL);
    END IF;
END;$$;


--
-- Name: clear_corp_user_balance(bigint, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION clear_corp_user_balance(p_id bigint, u_id bigint) RETURNS numeric[]
    LANGUAGE plpgsql
    AS $$DECLARE
    bal NUMERIC;
    play_bal NUMERIC;
    transfer_amounts NUMERIC[2];
BEGIN
    SELECT balance, play_credits
    INTO bal, play_bal
    FROM user_balance
    WHERE user_id = u_id;

    transfer_amounts[0] = 0.0;
    transfer_amounts[1] = 0.0;

    IF bal IS NOT NULL AND bal > 0.0 THEN
        INSERT INTO corporate_deposit_transfers(user_id,transfer_user,amount)
	VALUES(u_id,p_id,bal * (-1.0));

	transfer_amounts[0] = bal * (-1.0);
    END IF;
    
    IF play_bal IS NOT NULL AND play_bal > 0.0 THEN
        INSERT INTO promo_transactions(user_id, amount)
        VALUES(u_id, play_bal * (-1.0));
        transfer_amounts[1] = play_bal * (-1.0);
    END IF;

    RETURN transfer_amounts;
END;$$;


--
-- Name: closed_date_unique(timestamp without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION closed_date_unique(date_time timestamp without time zone) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$BEGIN
IF date_time IS NULL THEN 
  RETURN TRUE;
ELSE
  RETURN NULL;
END IF;

END$$;


--
-- Name: FUNCTION closed_date_unique(date_time timestamp without time zone); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION closed_date_unique(date_time timestamp without time zone) IS 'Function for indexes on the closed date.';


--
-- Name: corp_has_access(bigint, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION corp_has_access(corp_id bigint, u_id bigint) RETURNS boolean
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
primary_aff_id bigint;
curr_aff RECORD;
BEGIN

IF corp_id = u_id THEN
   RETURN TRUE;
END IF;

SELECT affiliate_id INTO primary_aff_id FROM affiliate_users WHERE user_id=u_id;

IF primary_aff_id = corp_id THEN
   RETURN TRUE;
END IF;

FOR curr_aff IN (SELECT subs.a FROM find_sub_corps(corp_id) AS subs(a))
LOOP
	IF curr_aff.a = primary_aff_id THEN
		RETURN TRUE;
	END IF;
END LOOP;

RETURN FALSE;

END;$$;


--
-- Name: corp_has_user(bigint, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION corp_has_user(corp_id bigint, u_id bigint) RETURNS boolean
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
primary_aff_id bigint;
curr_aff RECORD;
BEGIN

SELECT affiliate_id INTO primary_aff_id FROM affiliate_users WHERE user_id=u_id AND affiliation=1;

IF primary_aff_id = corp_id THEN
   RETURN TRUE;
END IF;

FOR curr_aff IN (SELECT subs.a FROM find_sub_corps(corp_id) AS subs(a))
LOOP
	IF curr_aff.a = primary_aff_id THEN
		RETURN TRUE;
	END IF;
END LOOP;

RETURN FALSE;

END;$$;


--
-- Name: corp_sub_accounts(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION corp_sub_accounts(corporate_id bigint) RETURNS SETOF bigint
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
curr_sub RECORD;

BEGIN
FOR curr_sub IN (SELECT affiliate_users.user_id
                  FROM affiliate_users WHERE affiliate_id=corporate_id)
LOOP
  RETURN NEXT curr_sub.user_id;
  RETURN QUERY SELECT * FROM corp_sub_accounts(curr_sub.user_id);
END LOOP;

RETURN;

END;$$;


--
-- Name: FUNCTION corp_sub_accounts(corporate_id bigint); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION corp_sub_accounts(corporate_id bigint) IS 'Returns the user_id for all sub-accounts of the given corporate/affiliate account. This returns all account types.';


--
-- Name: corp_super_accounts(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION corp_super_accounts(u_id bigint) RETURNS SETOF bigint
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
curr_super RECORD;

BEGIN
FOR curr_super IN (SELECT affiliate_users.affiliate_id
                  FROM affiliate_users WHERE user_id=u_id)
LOOP
  RETURN NEXT curr_super.affiliate_id;
  RETURN QUERY SELECT * FROM corp_super_accounts(curr_super.affiliate_id);
END LOOP;

RETURN;

END;$$;


--
-- Name: create_round(bigint, integer, bytea, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION create_round(t_id bigint, r_id integer, seed bytea, c_id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO game_rounds(table_id,round_id,random_seed,game_config_id) VALUES (t_id,r_id,seed,c_id);

	UPDATE game_table_last_rounds
	SET last_round_id = r_id
	WHERE table_id = t_id;

	RETURN r_id;
END;
$$;


--
-- Name: delete_user(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION delete_user(u_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
	DELETE FROM user_transactions WHERE action_table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id) OR user_id = u_id;
	DELETE FROM game_round_actions WHERE table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id) OR user_id = u_id; 
	DELETE FROM game_table_last_rounds WHERE table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id);
	DELETE FROM jackpot_transactions WHERE user_id = u_id OR table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id); 
	DELETE FROM game_period_amounts WHERE user_id = u_id OR table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id);
	DELETE FROM game_rounds WHERE table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id); 
	DELETE FROM game_table_users WHERE table_id IN (SELECT table_id FROM game_tables WHERE table_owner = u_id) OR user_id = u_id;
	DELETE FROM game_tables WHERE table_owner = u_id;
	DELETE FROM user_logins WHERE user_id = u_id;
	DELETE FROM payment_transactions WHERE user_id = u_id;
	DELETE FROM payout_transactions WHERE user_id = u_id;
	DELETE FROM user_balance_adjustments WHERE user_id = u_id;
	DELETE FROM users WHERE user_id = u_id;
END;$$;


--
-- Name: demo_user_login(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION demo_user_login(s_id integer, c_ip character varying, d_c character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	u_id BIGINT;
BEGIN
	SELECT MIN(user_id)
	INTO u_id
	FROM users
	WHERE user_type = 6
	AND is_active = TRUE
	AND closed = FALSE
	AND NOT EXISTS (SELECT user_id FROM user_logins WHERE user_id = users.user_id AND logout_date IS NULL);

	IF u_id IS NOT NULL THEN
		INSERT INTO user_logins(user_id,server_id,client_ip, device_hash_code) VALUES(u_id,s_id,c_ip,d_c);
		RETURN u_id;
	ELSE
		RETURN NULL;
	END IF;
END;$$;


--
-- Name: find_sub_corps(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION find_sub_corps(corporate_id bigint) RETURNS SETOF bigint
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
curr_sub RECORD;

BEGIN
FOR curr_sub IN (SELECT affiliate_users.user_id
                  FROM affiliate_users WHERE affiliate_id=corporate_id AND affiliation=3)
LOOP
  RETURN NEXT curr_sub.user_id;
  RETURN QUERY SELECT * FROM find_sub_corps(curr_sub.user_id);
END LOOP;

RETURN;

END;$$;


--
-- Name: find_super_corp(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION find_super_corp(u_id bigint) RETURNS bigint
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
	do_loop boolean;
	c_id bigint;
	last_u_id bigint;
BEGIN
	
	do_loop := TRUE;
	last_u_id := u_id;
	
	WHILE do_loop = TRUE LOOP

		SELECT affiliate_id INTO c_id
		FROM affiliate_users
		WHERE user_id = last_u_id;
		
		IF NOT FOUND THEN
			do_loop := FALSE;
		ELSE
			last_u_id := c_id;
		END IF;
	END LOOP;		

	RETURN last_u_id;

END;$$;


--
-- Name: FUNCTION find_super_corp(u_id bigint); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION find_super_corp(u_id bigint) IS 'Returns the top user_id of the corporate hierarchy for the given user/corporate';


--
-- Name: finish_round(bigint, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION finish_round(t_id bigint, r_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
	t_owner BIGINT;
	game_id INTEGER;
BEGIN
	UPDATE game_rounds SET date_closed=now_utc()
	WHERE table_id = t_id
	AND round_id = r_id
	AND date_closed IS NULL;
		
	IF NOT FOUND THEN
		RAISE EXCEPTION 'Cannot finish round. Round already closed or invalid round id';
	END IF;

	SELECT game_type_id, table_owner INTO game_id, t_owner
	FROM game_tables WHERE table_id=t_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'Cannot finish round. Could not find table';
	END IF;

	-- Increase the round counter
	UPDATE game_period_amounts SET rounds = rounds + 1
	WHERE period_date = date_trunc('day', now_utc())
	AND table_id = t_id AND user_id = t_owner;

	IF NOT FOUND THEN
		INSERT INTO game_period_amounts(period_date, table_id, user_id, bets, wins, jackpot_wins, rounds)
		VALUES (date_trunc('day', now_utc()), t_id, t_owner, 0.0, 0.0, 0.0, 1);
	END IF;
END;$$;


--
-- Name: get_unique_bigint(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_unique_bigint(value bigint) RETURNS bigint
    LANGUAGE plpgsql IMMUTABLE
    AS $$BEGIN
	IF value IS NULL THEN
		RETURN 0;
	ELSE
		RETURN value;
	END IF;
END;$$;


--
-- Name: get_unique_integer(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_unique_integer(value integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$BEGIN
	IF value IS NULL THEN
		RETURN 0;
	ELSE
		RETURN value;
	END IF;
END;$$;


--
-- Name: handle_amount_jackpot_win(bigint, integer, bigint, integer, numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION handle_amount_jackpot_win(u_id bigint, g_id integer, t_id bigint, r_id integer, jckpt_win_amt numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$DECLARE
	jckpt_id bigint;
	jckpt_initial_amt numeric;
	jckpt_amt numeric;
	jckpt_max_count integer;
	jckpt_count integer;
        jckpt_min_pyt numeric;
	jckpt_min_bal numeric;
BEGIN
	SELECT jackpot_id, pay_count, initial_amount, min_payout, min_jackpot_balance
	INTO jckpt_id, jckpt_max_count, jckpt_initial_amt, jckpt_min_pyt, jckpt_min_bal
	FROM jackpots
	WHERE game_id = g_id
	AND finish_date IS NULL
        AND auto = TRUE;

	IF NOT FOUND OR jckpt_id IS NULL THEN 
		RAISE EXCEPTION 'No jackpot currently exists for this game.';
	END IF;
	
	SELECT balance, winners
	INTO jckpt_amt, jckpt_count
	FROM jackpot_balances
	WHERE jackpot_id = jckpt_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'Jackpot amount not found!';
        ELSIF (jckpt_min_pyt IS NOT NULL AND (jckpt_win_amt < jckpt_min_pyt)) OR (jckpt_min_bal IS NOT NULL AND jckpt_amt < jckpt_min_bal) THEN
		RAISE EXCEPTION 'Jackpot amount insufficient!';
	END IF;

	IF jckpt_count IS NULL THEN 
		jckpt_count := 0;
	END IF;

	IF jckpt_count > jckpt_max_count THEN
		RAISE EXCEPTION 'Jackpot not valid anymore.';
	END IF;

	INSERT INTO jackpot_transactions(jackpot_id, win_id, amount, user_id, table_id, round_id)
	VALUES (jckpt_id, (jckpt_count+1), ((-1.0) * jckpt_win_amt), u_id, t_id, r_id);

	IF jckpt_max_count IS NULL OR (jckpt_count+1) < jckpt_max_count THEN
		SELECT balance
		INTO jckpt_amt
		FROM jackpot_balances
		WHERE jackpot_id = jckpt_id;

		IF jckpt_amt < jckpt_initial_amt THEN
			INSERT INTO jackpot_transactions(jackpot_id, amount)
			VALUES (jckpt_id, (jckpt_initial_amt - jckpt_amt));
		END IF;
	ELSE 
		UPDATE jackpots
		SET finish_date = now_utc()
		WHERE jackpot_id = jckpt_id;
	END IF;

	RETURN jckpt_win_amt;
END;$$;


--
-- Name: handle_jackpot_win(bigint, integer, bigint, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION handle_jackpot_win(u_id bigint, g_id integer, t_id bigint, r_id integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$DECLARE
	jckpt_id bigint;
	jckpt_initial_amt numeric;
	jckpt_amt numeric;
	jckpt_max_count integer;
	jckpt_count integer;
        jckpt_min_pyt numeric;
	jckpt_min_bal numeric;
BEGIN
	SELECT jackpot_id, pay_count, initial_amount, min_payout, min_jackpot_balance
	INTO jckpt_id, jckpt_max_count, jckpt_initial_amt, jckpt_min_pyt, jckpt_min_bal
	FROM jackpots
	WHERE game_id = g_id
	AND finish_date IS NULL
	AND auto = TRUE;

	IF NOT FOUND OR jckpt_id IS NULL THEN 
		RAISE EXCEPTION 'No jackpot currently exists for this game.';
	END IF;

	SELECT max(win_id)
	INTO jckpt_count
	FROM jackpot_transactions
	WHERE jackpot_id = jckpt_id;
	
	IF jckpt_count IS NULL THEN
		jckpt_count := 0;
	END IF;

	IF jckpt_count > jckpt_max_count THEN
		RAISE EXCEPTION 'Jackpot not valid anymore.';
	END IF;

	SELECT balance
	INTO jckpt_amt
	FROM jackpot_balances
	WHERE jackpot_id = jckpt_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'Jackpot amount not found!';
        ELSIF (jckpt_min_pyt IS NOT NULL AND ((jckpt_amt * ratio) < jckpt_min_pyt)) OR (jckpt_min_bal IS NOT NULL AND jckpt_amt < jckpt_min_bal) THEN
		RAISE EXCEPTION 'Jackpot amount insufficient!';
	END IF;

	INSERT INTO jackpot_transactions(jackpot_id, win_id, amount, user_id, table_id, round_id)
	VALUES (jckpt_id, (jckpt_count+1), ((-1.0) * jckpt_amt), u_id, t_id, r_id);

	IF jckpt_max_count IS NULL OR (jckpt_count+1) < jckpt_max_count THEN
		INSERT INTO jackpot_transactions(jackpot_id, amount)
		VALUES (jckpt_id, jckpt_initial_amt);
	ELSE 
		UPDATE jackpots
		SET finish_date = now_utc()
		WHERE jackpot_id = jckpt_id;
	END IF;

	RETURN jckpt_amt;
END;$$;


--
-- Name: handle_mystery_jackpot_win(integer, bigint, integer, bigint, numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION handle_mystery_jackpot_win(j_id integer, t_id bigint, r_id integer, u_id bigint, ratio numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$DECLARE
	jckpt_initial_amt numeric;
	jckpt_win_amt numeric;
	jckpt_amt numeric;
	jckpt_max_count integer;
	jckpt_count integer;
        jckpt_min_pyt numeric;
	jckpt_min_bal numeric;
BEGIN
	SELECT pay_count, initial_amount, min_payout, min_jackpot_balance
	INTO jckpt_max_count, jckpt_initial_amt, jckpt_min_pyt, jckpt_min_bal
	FROM jackpots
	WHERE jackpot_id = j_id
	AND (finish_date IS NULL OR finish_date > now_utc())
	AND auto = FALSE;

	IF NOT FOUND THEN 
		RAISE EXCEPTION 'No mystery jackpot exists for this game.';
	END IF;

	SELECT balance, winners
        INTO jckpt_amt, jckpt_count
        FROM jackpot_balances
        WHERE jackpot_id = j_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'Jackpot amount not found!';
	ELSIF (jckpt_min_pyt IS NOT NULL AND ((jckpt_amt * ratio) < jckpt_min_pyt)) OR (jckpt_min_bal IS NOT NULL AND jckpt_amt < jckpt_min_bal) THEN
		RAISE EXCEPTION 'Jackpot amount insufficient!';
	END IF;

	IF jckpt_count IS NULL THEN 
		jckpt_count := 0;
	END IF;

	jckpt_win_amt := (jckpt_amt * ratio);

	INSERT INTO jackpot_transactions(jackpot_id, win_id, amount, user_id, table_id, round_id)
	VALUES (j_id, (jckpt_count+1), ((-1.0) * jckpt_win_amt), u_id, t_id, r_id);

	IF jckpt_max_count IS NULL OR (jckpt_count+1) < jckpt_max_count THEN
		SELECT balance
		INTO jckpt_amt
		FROM jackpot_balances
		WHERE jackpot_id = j_id;

		IF jckpt_amt < jckpt_initial_amt THEN
			INSERT INTO jackpot_transactions(jackpot_id, amount)
			VALUES (j_id, (jckpt_initial_amt - jckpt_amt));
		END IF;
	ELSE 
		UPDATE jackpots
		SET finish_date = now_utc()
		WHERE jackpot_id = j_id;
	END IF;
	
	RETURN jckpt_win_amt;
END;$$;


--
-- Name: handle_ratio_jackpot_win(bigint, integer, bigint, integer, numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION handle_ratio_jackpot_win(u_id bigint, g_id integer, t_id bigint, r_id integer, ratio numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$DECLARE
	jckpt_id bigint;
	jckpt_initial_amt numeric;
	jckpt_win_amt numeric;
	jckpt_amt numeric;
	jckpt_max_count integer;
	jckpt_count integer;
        jckpt_min_pyt numeric;
	jckpt_min_bal numeric;
BEGIN
	SELECT jackpot_id, pay_count, initial_amount, min_payout, min_jackpot_balance
	INTO jckpt_id, jckpt_max_count, jckpt_initial_amt, jckpt_min_pyt, jckpt_min_bal
	FROM jackpots
	WHERE game_id = g_id
	AND finish_date IS NULL
        AND auto = TRUE;

	IF NOT FOUND OR jckpt_id IS NULL THEN 
		RAISE EXCEPTION 'No jackpot currently exists for this game.';
	END IF;
	
	SELECT balance, winners
	INTO jckpt_amt, jckpt_count
	FROM jackpot_balances
	WHERE jackpot_id = jckpt_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'Jackpot amount not found!';
        ELSIF (jckpt_min_pyt IS NOT NULL AND ((jckpt_amt * ratio) < jckpt_min_pyt)) OR (jckpt_min_bal IS NOT NULL AND jckpt_amt < jckpt_min_bal) THEN
		RAISE EXCEPTION 'Jackpot amount insufficient!';
	END IF;

	IF jckpt_count IS NULL THEN 
		jckpt_count := 0;
	END IF;

	IF jckpt_count > jckpt_max_count THEN
		RAISE EXCEPTION 'Jackpot not valid anymore.';
	END IF;

	jckpt_win_amt := (jckpt_amt * ratio);

	INSERT INTO jackpot_transactions(jackpot_id, win_id, amount, user_id, table_id, round_id)
	VALUES (jckpt_id, (jckpt_count+1), ((-1.0) * jckpt_win_amt), u_id, t_id, r_id);

	IF jckpt_max_count IS NULL OR (jckpt_count+1) < jckpt_max_count THEN
		SELECT balance
		INTO jckpt_amt
		FROM jackpot_balances
		WHERE jackpot_id = jckpt_id;

		IF jckpt_amt < jckpt_initial_amt THEN
			INSERT INTO jackpot_transactions(jackpot_id, amount)
			VALUES (jckpt_id, (jckpt_initial_amt - jckpt_amt));
		END IF;
	ELSE 
		UPDATE jackpots
		SET finish_date = now_utc()
		WHERE jackpot_id = jckpt_id;
	END IF;

	RETURN jckpt_win_amt;
END;$$;


--
-- Name: ibutler_loadavg(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION ibutler_loadavg() RETURNS text
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$DECLARE
load_avg TEXT;
space_pos integer;
BEGIN

BEGIN
CREATE TEMPORARY TABLE loadavg_t(load_avg_text TEXT);
EXCEPTION WHEN OTHERS THEN
/* DO NOTHING */
END;

BEGIN
  COPY loadavg_t FROM '/proc/loadavg';
EXCEPTION WHEN OTHERS THEN
  DROP TABLE IF EXISTS loadavg_t;
  RETURN NULL;
END;

SELECT load_avg_text INTO load_avg FROM loadavg_t LIMIT 1;

DROP TABLE IF EXISTS loadavg_t;

space_pos := position(' ' in load_avg);
load_avg := substring(load_avg from 1 for space_pos - 1);


RETURN load_avg;
END$$;


--
-- Name: FUNCTION ibutler_loadavg(); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION ibutler_loadavg() IS 'This function MUST be owned by postgres or a superuser to work!';


--
-- Name: jackpot_contrib(bigint, bigint, integer, bigint, bigint, numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION jackpot_contrib(u_id bigint, corp_id bigint, g_id integer, t_id bigint, r_id bigint, bet_placed numeric) RETURNS boolean
    LANGUAGE plpgsql
    AS $$DECLARE
    curr_jpot RECORD;
    curr_jackpot_amt numeric;
    contrib_amount numeric;
    contrib_expenses_mode integer;
    corporate_amount numeric;
    user_affiliate bigint;
    affiliate_affiliate bigint;
    user_affiliate_rate numeric;
    prev_corporate_rate numeric;
    old_affiliate_earnings numeric;
    corp_active boolean;
    old_aff_amt numeric;
    old_aff_j_contribs numeric;
    placed_contrib boolean;
BEGIN
    -- MODES: 1 = Casino pays all contributions mode, 2 = Corporates pay their share and casino the rest,
    -- 3 = Last corporate pays his share and the casino the rest
    contrib_expenses_mode := 2;
    placed_contrib := false;

    FOR curr_jpot IN (SELECT jackpot_id, bet_contrib, maximum_amount, jackpots.game_id, jackpots.target_corporate
        FROM jackpots
		WHERE (game_id IS NULL OR game_id = g_id)
		AND (jackpots.target_corporate IS NULL OR (corp_id = jackpots.target_corporate))
		AND finish_date IS NULL)
    LOOP
	IF curr_jpot.maximum_amount IS NOT NULL THEN 
	    SELECT balance
	    INTO curr_jackpot_amt
	    FROM jackpot_balances
	    WHERE jackpot_id = curr_jpot.jackpot_id;

	    IF curr_jackpot_amt >= curr_jpot.maximum_amount THEN
	        placed_contrib := true;
	    END IF;

	    CONTINUE WHEN curr_jackpot_amt >= curr_jpot.maximum_amount;
	END IF;
		
	contrib_amount := ((curr_jpot.bet_contrib * bet_placed) / 100.0);
	corp_active := true;
	    
	--Check for corporate jackpot
	IF (corp_id IS NOT NULL AND corp_id > 0 AND contrib_expenses_mode <> 1) THEN
		SELECT affiliate_rate, users.is_active
		INTO user_affiliate_rate, corp_active
		FROM affiliates
		INNER JOIN users ON affiliates.affiliate_id = users.user_id
		WHERE affiliate_id = corp_id AND approved;
		
                user_affiliate := corp_id;

		IF user_affiliate IS NOT NULL THEN
		    IF corp_active THEN
			    --update direct corporate amount
			    corporate_amount := (-1.0 * contrib_amount) * user_affiliate_rate;
			    PERFORM update_affiliate_period_amounts(user_affiliate,u_id,corporate_amount,(corporate_amount * -1.0));
		    END IF;
		    prev_corporate_rate := user_affiliate_rate;

                    IF contrib_expenses_mode = 2 THEN
		        LOOP
			    SELECT affiliate_users.affiliate_id, affiliate_rate, users.is_active
			    INTO affiliate_affiliate, user_affiliate_rate, corp_active
		            FROM affiliate_users
		            INNER JOIN affiliates ON affiliate_users.affiliate_id = affiliates.affiliate_id
		            INNER JOIN users ON affiliates.affiliate_id = users.user_id
			    WHERE affiliate_users.user_id=user_affiliate AND approved;
						
			    IF NOT FOUND THEN
		                EXIT;
			    ELSE
			        IF corp_active = TRUE THEN
					--update next corporate
					corporate_amount := ((-1.0 * contrib_amount) * (user_affiliate_rate - prev_corporate_rate));
					PERFORM update_affiliate_period_amounts(affiliate_affiliate,user_affiliate,corporate_amount,(corporate_amount * -1.0));
				END IF;
                                user_affiliate := affiliate_affiliate;
				prev_corporate_rate := user_affiliate_rate;
			   END IF;
	               END LOOP;
	          END IF;
	     END IF;
	END IF;
 
	IF (contrib_amount > 0.0) THEN
          INSERT INTO jackpot_transactions(jackpot_id, amount, user_id, table_id, round_id)
	  VALUES (curr_jpot.jackpot_id, contrib_amount, u_id, t_id, r_id);

	  placed_contrib := true;
	END IF;
    END LOOP;

    RETURN placed_contrib;
END;$$;


--
-- Name: now_utc(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION now_utc() RETURNS timestamp without time zone
    LANGUAGE sql STABLE
    AS $$ 
SELECT CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
$$;


--
-- Name: payment_balance_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION payment_balance_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN	
	INSERT INTO user_transactions(user_id, transaction_time, amount, payment_transaction_id)
	VALUES(NEW.user_id, NEW.time_completed, NEW.amount, NEW.transaction_id);

	RETURN NEW;
END;$$;


--
-- Name: trg_affiliate_after_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_affiliate_after_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
INSERT INTO affiliate_earnings(affiliate_id, earnings) VALUES(NEW.affiliate_id, 0.0);
RETURN NEW;
END;$$;


--
-- Name: trg_affiliate_earnings_transaction_before_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_affiliate_earnings_transaction_before_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    UPDATE affiliate_earnings SET earnings = earnings - NEW.amount WHERE affiliate_id = NEW.affiliate_id
    RETURNING earnings INTO NEW.new_earnings;

    RETURN NEW;
END;$$;


--
-- Name: trg_affiliate_earnings_transaction_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_affiliate_earnings_transaction_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    INSERT INTO user_transactions(user_id, transaction_time, amount, affiliate_earnings_transaction_id)
    VALUES (NEW.affiliate_id, NEW.transaction_date, NEW.amount, NEW.transaction_id);

    RETURN NEW;
END;$$;


--
-- Name: trg_before_user_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_before_user_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	IF NEW.closed = TRUE THEN
		NEW.is_active := FALSE;
	END IF;
	
	IF NEW.is_active = FALSE THEN
		IF OLD.date_deactivated IS NULL THEN
			NEW.date_deactivated := now_utc();
		END IF;
	ELSE
		NEW.date_deactivated := NULL;
	END IF;


        INSERT INTO user_changes(user_id, old_username, old_password_hash, old_password_salt, old_email, old_is_active,
		old_first_name, old_last_name, old_middle_name, old_country, old_phone, old_region, old_town, old_postal_code,
		old_nickname, old_time_zone, old_user_type, old_postal_address,	old_date_deactivated,
		new_username, new_password_hash, new_password_salt, new_email, new_is_active, new_first_name,
		new_last_name, new_middle_name, new_country, new_phone, new_region, new_town, new_postal_code, new_nickname, new_time_zone,
		new_user_type, new_postal_address, new_date_deactivated, change_by_user,
		old_closed, new_closed)
	VALUES (OLD.user_id, OLD.username, OLD.password_hash, OLD.password_salt, OLD.email, OLD.is_active,
		OLD.first_name, OLD.last_name, OLD.middle_name, OLD.country, OLD.phone, OLD.region, OLD.town, OLD.postal_code,
		OLD.nickname, OLD.time_zone, OLD.user_type, OLD.postal_address,	OLD.date_deactivated,
		NEW.username, NEW.password_hash, NEW.password_salt, NEW.email, NEW.is_active, NEW.first_name,
		NEW.last_name, NEW.middle_name, NEW.country, NEW.phone, NEW.region, NEW.town, NEW.postal_code, NEW.nickname, NEW.time_zone,
		NEW.user_type, NEW.postal_address, NEW.date_deactivated, NEW.modifier_user,
		OLD.closed, NEW.closed);         

        NEW.modifier_user := NULL;

	RETURN NEW;
END;$$;


--
-- Name: trg_corporate_deposit_transfers_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_corporate_deposit_transfers_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE 
    aff_type int8;
BEGIN
     -- Make sure the transactions are entered in this order: sub, super to avoid deadlocks.
     IF NEW.transfer_user < NEW.user_id THEN
	-- transfer user is super, user is sub
	INSERT INTO user_transactions(user_id, transaction_time, amount, deposit_transfer_id)
	    VALUES (NEW.user_id, NEW.transfer_date, NEW.amount, NEW.transfer_id);

        INSERT INTO user_transactions(user_id, transaction_time, amount, deposit_transfer_id)
	    VALUES (NEW.transfer_user, NEW.transfer_date, NEW.amount * -1.0, NEW.transfer_id);
     ELSE
	INSERT INTO user_transactions(user_id, transaction_time, amount, deposit_transfer_id)
	    VALUES (NEW.transfer_user, NEW.transfer_date, NEW.amount * -1.0, NEW.transfer_id);
	  
        INSERT INTO user_transactions(user_id, transaction_time, amount, deposit_transfer_id)
	   VALUES (NEW.user_id, NEW.transfer_date, NEW.amount, NEW.transfer_id);
     END IF;
     
    RETURN NEW;
END;$$;


--
-- Name: trg_game_round_action_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_game_round_action_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    t_bets numeric;
    t_wins numeric;
    r_bets numeric;
    r_wins numeric;
    j_wins numeric;
    g_category integer;
    g_type integer;
    u_type integer;

    bet_round integer;
    round_t_bets numeric;
    play_credits_amount numeric;

    u_play_credits numeric;
    u_balance numeric;

    user_affiliate bigint;
    user_affiliate_rate numeric;
    prev_corporate_rate numeric;
    affiliate_affiliate bigint;
    affiliate_amount numeric;

    pending_bets numeric;
    current_bonus_bets numeric;
BEGIN
    -- Used from a partial trigger
    -- but make sure we also check here that the action has an amount.
    IF NEW.amount IS NOT NULL THEN
        t_bets := 0;
        t_wins := 0;
        r_bets := 0;
        r_wins := 0;
        j_wins := 0;

        --
        -- Handle win
        --
        IF NEW.action_desc='WIN' THEN
            --
            -- Jackpot win. Always pays real money.
            --
            IF UPPER(NEW.action_value) LIKE '%JACKPOT%' THEN
                j_wins := NEW.amount;
            -- Regular win. 
            ELSE
                t_wins := NEW.amount;
                r_wins := NEW.amount;
                    
            END IF;

            INSERT INTO user_transactions(user_id, transaction_time, amount, action_table_id, action_round_id, action_action_id, action_sub_action_id)
            VALUES(NEW.user_id, NEW.action_time, r_wins+j_wins, NEW.table_id, NEW.round_id, NEW.action_id, NEW.sub_action_id);
        
        --  
        -- Handle bet
        --
        ELSE
            t_bets := NEW.amount;
            r_bets := 0;
            play_credits_amount := 0;

           -- Get the game category
           SELECT game_tables.game_type_id, game_category_id INTO g_type, g_category FROM game_tables
           INNER JOIN game_types ON game_tables.game_type_id=game_types.game_type_id
           WHERE table_id=NEW.table_id;            

           -- Get user type
           SELECT user_type INTO u_type FROM users
           WHERE users.user_id = NEW.user_id;

	   -- Get balances
           SELECT play_credits, balance INTO u_play_credits, u_balance FROM user_balance WHERE user_balance.user_id = NEW.user_id;

           -- Slot.
           IF g_category = 2 THEN
                -- Corporate users use their play credits first (land-based only).
	        IF u_type = 5 THEN
                    play_credits_amount := LEAST(u_play_credits, t_bets);
                    t_bets := t_bets - play_credits_amount;
		END IF;

                -- Use real money first
                r_bets := LEAST(u_balance, t_bets);
                t_bets := t_bets - r_bets;

                -- Use play credits
                IF u_type <> 5 THEN
                    play_credits_amount := LEAST(u_play_credits, t_bets);
                    t_bets := t_bets - play_credits_amount;
                END IF;

           -- Non-slot
           ELSE
               -- Dont allow corporate player to bet on a non-slot game if there are play credits in the account
               -- To ensure that play credits are played first
               IF u_type = 5 AND u_play_credits > 0.0 THEN
                       RAISE EXCEPTION 'Must play free credits on slots first';
               END IF;

               -- Use real money
               r_bets := LEAST(u_balance, t_bets);
               t_bets := t_bets - r_bets;
           END IF;

           IF t_bets > 0 THEN
               RAISE EXCEPTION 'Not enough credits for bet. check_user_balance_positive';
           END IF;
           
           t_bets := NEW.amount;

           -- Commit play credit bet
           IF play_credits_amount > 0 THEN
               INSERT INTO promo_transactions(user_id, amount, table_id, round_id, action_id, sub_action_id)
               VALUES (NEW.user_id, play_credits_amount * -1, NEW.table_id, NEW.round_id, NEW.action_id, NEW.sub_action_id);
           END IF;

           -- Commit real money bet
           IF r_bets > 0 THEN
              INSERT INTO user_transactions(user_id, transaction_time, amount, action_table_id, action_round_id, action_action_id, action_sub_action_id)
              VALUES(NEW.user_id, NEW.action_time, r_bets * -1, NEW.table_id, NEW.round_id, NEW.action_id, NEW.sub_action_id); 

              pending_bets := t_bets;
           END IF;

           -- Update last bet round id.
           UPDATE game_table_last_rounds
           SET last_bet_round_id = NEW.round_id
           WHERE table_id = NEW.table_id
           AND last_bet_round_id IS DISTINCT FROM NEW.round_id;

        END IF;
    
        -- update periodic records
        UPDATE game_period_amounts SET
          bets = COALESCE(bets, 0) + t_bets,  
          wins = COALESCE(wins, 0) + t_wins,
          real_bets = COALESCE(real_bets, 0) + r_bets,
          real_wins = COALESCE(real_wins, 0) + r_wins,
          jackpot_wins = COALESCE(jackpot_wins, 0) + j_wins
        WHERE period_date = date_trunc('day', now_utc())
        AND table_id = NEW.table_id
        AND user_id = NEW.user_id;

        IF NOT FOUND THEN
            INSERT INTO game_period_amounts(period_date, table_id, user_id, bets, wins, real_bets, real_wins, jackpot_wins, rounds)
              VALUES (date_trunc('day', now_utc()), NEW.table_id, NEW.user_id, t_bets, t_wins, r_bets, r_wins, j_wins, 0);
        END IF;

        --
        -- Real-time affiliate commission. Jackpot wins do not affect affiliate commissions
        -- because they already sponsor jackpot contributions so it will be like charging them
        -- twice for the same risk.
        -- A Jackpot win is any WIN which has a value ending in "jackpot".
        IF r_bets > 0 OR r_wins > 0 THEN
            -- Get direct affiliate
            SELECT affiliate_users.affiliate_id, affiliate_rate	
            INTO user_affiliate, user_affiliate_rate
            FROM affiliate_users
            INNER JOIN affiliates ON affiliate_users.affiliate_id = affiliates.affiliate_id
            WHERE affiliate_users.user_id=NEW.user_id 
            AND approved = TRUE;
		
            IF user_affiliate IS NOT NULL
              AND user_affiliate_rate IS NOT NULL
            THEN
                --update direct corporate amount
                affiliate_amount := (r_bets - r_wins) * user_affiliate_rate;
		IF affiliate_amount <> 0 THEN
			PERFORM update_affiliate_period_amounts(user_affiliate,NEW.user_id,affiliate_amount,0.0);
		END IF;

                prev_corporate_rate := user_affiliate_rate;
    
                LOOP
                    SELECT affiliate_users.affiliate_id, affiliate_rate	
                    INTO affiliate_affiliate, user_affiliate_rate
                    FROM affiliate_users
                    INNER JOIN affiliates ON affiliate_users.affiliate_id = affiliates.affiliate_id
                    WHERE affiliate_users.user_id=user_affiliate
                    AND approved = TRUE;
		
                    IF NOT FOUND THEN
                        EXIT;
                    END IF;
                    	
	            IF user_affiliate_rate IS NULL THEN
	                EXIT;
	            END IF;

	            affiliate_amount := (r_bets - r_wins) * (user_affiliate_rate - prev_corporate_rate);

		    IF affiliate_amount <> 0 THEN
			PERFORM update_affiliate_period_amounts(affiliate_affiliate,user_affiliate,affiliate_amount, 0.0);
		    END IF;
                    
	            user_affiliate := affiliate_affiliate;
                    prev_corporate_rate := user_affiliate_rate;
                END LOOP;
            END IF;
        END IF;
        --
        -- End real-time affiliate commission
        --
    END IF;

    RETURN NEW;
END;$$;


--
-- Name: trg_game_round_action_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_game_round_action_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

    -- Sanity check. Do not allow operations that might possibly
    -- affect the consistency of the data.
    IF OLD.amount IS NOT NULL OR NEW.amount IS NOT NULL THEN

        IF NEW.amount IS DISTINCT FROM OLD.amount
          OR NEW.action_value IS DISTINCT FROM OLD.action_value
        THEN
            RAISE EXCEPTION 'Refusing to change amount or value for action';
        END IF;
    END IF;

    RETURN NEW;
END;$$;


--
-- Name: trg_jackpot_config_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_jackpot_config_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
	jpot_id INTEGER;
BEGIN
	UPDATE jackpots
	  SET initial_amount = NEW.initial_amount,
	      bet_contrib = NEW.bet_contrib,
	      maximum_amount = NEW.maximum_amount, 
	      min_payout = NEW.min_payout,
	      min_jackpot_balance = NEW.min_jackpot_balance,
	      draw_number = NEW.draw_number
	WHERE config_id = NEW.config_id;
	
	RETURN NEW;
END;$$;


--
-- Name: trg_jackpot_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_jackpot_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
   
   INSERT INTO jackpot_balances(jackpot_id, balance, winners) VALUES(NEW.jackpot_id, 0, 0);
   RETURN NEW;
   
END;
$$;


--
-- Name: trg_jackpot_transaction(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_jackpot_transaction() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE 
     new_bal NUMERIC;
BEGIN
    
    UPDATE jackpot_balances 
    SET balance=balance + NEW.amount
    WHERE jackpot_balances.jackpot_id = NEW.jackpot_id
    RETURNING balance INTO new_bal;
  
    UPDATE jackpot_period_balances
    SET balance = new_bal
    WHERE jackpot_id = NEW.jackpot_id
    AND period_date = date_trunc('day', now_utc());

    IF NOT FOUND THEN
        BEGIN
            INSERT INTO jackpot_period_balances(period_date, jackpot_id, balance)
             VALUES(date_trunc('day', now_utc()), NEW.jackpot_id, new_bal);
        EXCEPTION WHEN unique_violation THEN
	    UPDATE jackpot_period_balances
            SET balance = new_bal
            WHERE jackpot_id = NEW.jackpot_id
            AND period_date = date_trunc('day', now_utc());
        END;
    END IF;

    IF NEW.user_id IS NOT NULL AND NEW.table_id IS NOT NULL AND NEW.win_id IS NULL THEN
	 UPDATE game_period_amounts SET jackpot_contribs = COALESCE(jackpot_contribs,0.0) + NEW.amount
		WHERE period_date = date_trunc('day', now_utc())
			AND table_id = NEW.table_id
		        AND user_id = NEW.user_id;

	 IF NOT FOUND THEN
		INSERT INTO game_period_amounts(period_date, table_id, user_id, jackpot_contribs, rounds)
		   VALUES (date_trunc('day', now_utc()), NEW.table_id, NEW.user_id, NEW.amount, 0);
	 END IF;
    END IF;
  
    -- Only record winners
    IF NEW.win_id IS NULL THEN
        RETURN NULL;
    ELSE
        RETURN NEW;
    END IF;
END;$$;


--
-- Name: trg_jackpot_transaction_after_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_jackpot_transaction_after_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
IF NEW.win_id IS NOT NULL THEN
	UPDATE jackpot_balances 
	SET winners = winners + 1, last_win_transaction = NEW.jackpot_transaction_id
	WHERE jackpot_balances.jackpot_id = NEW.jackpot_id;
END IF;

RETURN NEW;
END;$$;


--
-- Name: trg_payment_balance_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_payment_balance_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
        card_value numeric;
	amt_rate numeric;
	min_dep numeric;
	final_amt numeric;
        curr_activation RECORD;
        curr_bonus_activation RECORD;
        bonus_claim_amt NUMERIC;
        restriction_val NUMERIC;
        curr_restriction RECORD;
        restrictions_count INTEGER;
        restriction_finish_date TIMESTAMP WITHOUT TIME ZONE;
        deposits_count INTEGER;

BEGIN	
	IF NEW.user_id IS DISTINCT FROM OLD.user_id THEN
		RAISE EXCEPTION 'Cannot change the user for the transaction';
	END IF;

	IF NEW.amount IS DISTINCT FROM OLD.amount THEN
		RAISE EXCEPTION 'Cannot change the amount for the transaction';
	END IF;

	IF NEW.time_completed IS DISTINCT FROM OLD.time_completed THEN
		RAISE EXCEPTION 'Cannot modify time completed once set';
	END IF;

	RETURN NEW;
END;$$;


--
-- Name: trg_payout_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_payout_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
   INSERT INTO user_transactions(user_id, transaction_time, amount, payout_id)
   VALUES(NEW.user_id, NEW.payout_date, NEW.amount * -1.0, NEW.payout_id);

   RETURN NEW;
END;$$;


--
-- Name: trg_payout_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_payout_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    old_user_balance NUMERIC;
BEGIN
 
   IF NEW.time_completed IS DISTINCT FROM OLD.time_completed THEN
	RAISE EXCEPTION 'Cannot modify time completed for withdrawal';
   END IF;

   IF NEW.amount IS DISTINCT FROM OLD.amount THEN
	RAISE EXCEPTION 'Cannot modify amount for withdrawal';
   END IF;

   RETURN NEW;
END;$$;


--
-- Name: trg_promo_transactions(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_promo_transactions() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    new_user_balance numeric;
    new_play_credits numeric;
BEGIN
    
    UPDATE user_balance SET play_credits=play_credits + NEW.amount WHERE user_balance.user_id=NEW.user_id
       RETURNING balance, play_credits INTO NEW.new_balance, NEW.new_play_credits;
    

    RETURN NEW;
END;$$;


--
-- Name: trg_user_balance_adjustments(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_user_balance_adjustments() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
INSERT INTO user_transactions (user_id, transaction_time, amount, adjustment_id) 
  VALUES (NEW.user_id, NEW.adjustment_date, NEW.adjustment_amount, NEW.adjustment_id);

RETURN NEW;
END;$$;


--
-- Name: trg_user_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_user_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
INSERT INTO user_balance(user_id, balance) VALUES(NEW.user_id, 0.0);
RETURN NEW;
END;
$$;


--
-- Name: trg_user_transaction(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_user_transaction() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

IF NEW.payment_transaction_id IS NULL 
   AND NEW.payout_id IS NULL
   AND NEW.adjustment_id IS NULL
   AND (NEW.action_table_id IS NULL 
        OR NEW.action_round_id IS NULL
        OR NEW.action_action_id IS NULL
        OR NEW.action_sub_action_id IS NULL) 
   AND NEW.deposit_transfer_id IS NULL
   AND NEW.affiliate_earnings_transaction_id IS NULL 
   THEN
    RAISE EXCEPTION 'Balance change reason not speficied';
END IF;

UPDATE user_balance SET balance=balance + NEW.amount WHERE user_balance.user_id=NEW.user_id
RETURNING balance, play_credits INTO NEW.new_balance, NEW.new_play_credits;

RETURN NEW;
END;$$;


--
-- Name: trg_user_transaction_after_insert(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trg_user_transaction_after_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	IF NEW.action_table_id IS NULL THEN
		UPDATE latest_non_gaming_transactions
		SET transaction_id = NEW.user_transaction_id,
		amount = NEW.amount
		WHERE user_id = NEW.user_id;
	
		IF NOT FOUND THEN
			INSERT INTO latest_non_gaming_transactions(transaction_id,user_id,amount)
			VALUES(NEW.user_transaction_id, NEW.user_id, NEW.amount);
		END IF;
	ELSE
		UPDATE latest_gaming_transactions
		SET transaction_id = NEW.user_transaction_id,
		amount = NEW.amount
		WHERE user_id = NEW.user_id;
	
		IF NOT FOUND THEN
			INSERT INTO latest_gaming_transactions(transaction_id,user_id,amount)
			VALUES(NEW.user_transaction_id, NEW.user_id, NEW.amount);
		END IF;
	END IF;
	
	RETURN NEW;
END;$$;


--
-- Name: update_affiliate_period_amounts(bigint, bigint, numeric, numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION update_affiliate_period_amounts(aff_id bigint, u_id bigint, earnings_amt numeric, jpot_contrib numeric) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
       curr_earnings_bal numeric;
BEGIN
--- Update affiliate period earnings
    
    
        UPDATE affiliate_earnings SET earnings = earnings + earnings_amt WHERE affiliate_id = aff_id RETURNING earnings INTO curr_earnings_bal;
    
        UPDATE affiliate_period_amounts
        SET amount = amount + earnings_amt, jackpot_contribs = jackpot_contribs + jpot_contrib, curr_earnings = curr_earnings_bal
        WHERE period_date = date_trunc('day', now_utc())
        AND affiliate_id = aff_id
        AND user_id = u_id;

        IF NOT FOUND THEN
	    BEGIN
                INSERT INTO affiliate_period_amounts(period_date, affiliate_id, user_id, amount, jackpot_contribs, curr_earnings)
                VALUES(date_trunc('day', now_utc()), aff_id, u_id, earnings_amt, jpot_contrib, curr_earnings_bal);
            EXCEPTION WHEN unique_violation THEN
		UPDATE affiliate_period_amounts
		SET amount = amount + earnings_amt, jackpot_contribs = jackpot_contribs + jpot_contrib, curr_earnings = curr_earnings_bal
		WHERE period_date = date_trunc('day', now_utc())
		AND affiliate_id = aff_id
		AND user_id = u_id;
            END;
        END IF;

END;$$;


--
-- Name: update_dog_history(integer, bigint, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION update_dog_history(d_id integer, t_id bigint, new_pos integer, runners integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
	i INTEGER;
BEGIN
	FOR i IN 0..9 LOOP
		UPDATE boot_dog_races
		SET race_no = i-1
		WHERE dog_id = d_id
		AND table_id = t_id
		AND race_no = i;
	END LOOP;
	
	UPDATE boot_dog_races
	SET race_no = 9, pos = new_pos, contestants = runners
	WHERE dog_id = d_id
	AND table_id = t_id
	AND race_no = -1;
END;$$;


--
-- Name: update_horse_history(integer, bigint, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION update_horse_history(h_id integer, t_id bigint, new_pos integer, runners integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
	i INTEGER;
BEGIN
	FOR i IN 0..9 LOOP
		UPDATE boot_horse_races
		SET race_no = i-1
		WHERE horse_id = h_id
		AND table_id = t_id
		AND race_no = i;
	END LOOP;
	
	UPDATE boot_horse_races
	SET race_no = 9, pos = new_pos, contestants = runners
	WHERE horse_id = h_id
	AND table_id = t_id
	AND race_no = -1;
END;$$;


--
-- Name: update_roulette_result(bigint, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION update_roulette_result(t_id bigint, r_id integer, res integer) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
	IF (SELECT COUNT(round_id) FROM roulette_results WHERE table_id = t_id) >= 12 THEN
		DELETE FROM roulette_results WHERE table_id = t_id
		AND round_id = (SELECT MIN(round_id) FROM roulette_results WHERE table_id = t_id);
	END IF;

	INSERT INTO roulette_results(table_id, round_id, result)
	VALUES (t_id, r_id, res);
END;$$;


--
-- Name: adjustment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_with_oids = false;

--
-- Name: affiliate_changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE affiliate_changes (
    change_id bigint NOT NULL,
    change_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    affiliate_id bigint NOT NULL,
    field_name character varying(80) NOT NULL,
    old_value character varying(256),
    new_value character varying(256),
    modifier_user bigint
);


--
-- Name: affiliate_changes_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE affiliate_changes_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: affiliate_changes_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE affiliate_changes_change_id_seq OWNED BY affiliate_changes.change_id;


--
-- Name: affiliate_earnings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE affiliate_earnings (
    affiliate_id bigint NOT NULL,
    earnings numeric(16,6) DEFAULT 0.0 NOT NULL
);


--
-- Name: affiliate_earnings_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE affiliate_earnings_transactions (
    transaction_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    affiliate_id bigint NOT NULL,
    amount numeric(16,6) NOT NULL,
    details character varying(256),
    new_earnings numeric(16,6),
    transaction_id bigint NOT NULL,
    initiator_user_id bigint
);


--
-- Name: affiliate_payments_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE affiliate_payments_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: affiliate_payments_payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE affiliate_payments_payment_id_seq OWNED BY affiliate_earnings_transactions.transaction_id;


--
-- Name: affiliate_period_amounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE affiliate_period_amounts (
    period_date timestamp without time zone NOT NULL,
    affiliate_id bigint NOT NULL,
    user_id bigint NOT NULL,
    amount numeric(16,6) DEFAULT 0.0 NOT NULL,
    jackpot_contribs numeric(16,6) DEFAULT 0.0 NOT NULL,
    curr_earnings numeric(16,6) DEFAULT 0 NOT NULL
);


--
-- Name: TABLE affiliate_period_amounts; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE affiliate_period_amounts IS 'Daily corporate account earnings totals.';


--
-- Name: affiliate_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE affiliate_users (
    affiliate_id bigint NOT NULL,
    user_id bigint NOT NULL,
    affiliation integer
);


--
-- Name: TABLE affiliate_users; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE affiliate_users IS 'Mapping between corporates and their users/sub-accounts';


--
-- Name: COLUMN affiliate_users.affiliation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN affiliate_users.affiliation IS '1 = USER
3 = SUB CORPORATE';


--
-- Name: affiliates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE affiliates (
    affiliate_id bigint NOT NULL,
    affiliate_rate numeric(5,4),
    approved boolean DEFAULT false NOT NULL,
    payment_period interval,
    allow_free_credits boolean DEFAULT false NOT NULL,
    free_credits_rate numeric(16,6),
    modifier_user bigint,
    allow_full_free_credits boolean DEFAULT false NOT NULL,
    require_approval boolean DEFAULT false NOT NULL,
    sub_levels integer,
    requires_super_approval boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE affiliates; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE affiliates IS 'Holds corporate accounts info.';


--
-- Name: COLUMN affiliates.allow_full_free_credits; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN affiliates.allow_full_free_credits IS 'Enables/Disables the awarding of free credits on ALL GAMES (not only SLOTS as when the value is set to FALSE)';


--
-- Name: boot_dog_races; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE boot_dog_races (
    table_id bigint NOT NULL,
    dog_id integer NOT NULL,
    race_no integer NOT NULL,
    pos integer NOT NULL,
    contestants integer NOT NULL
);


--
-- Name: TABLE boot_dog_races; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE boot_dog_races IS 'The initial fake races used to create the race statistics for tables with insufficient data.';


--
-- Name: boot_horse_races; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE boot_horse_races (
    table_id bigint NOT NULL,
    horse_id integer NOT NULL,
    race_no integer NOT NULL,
    pos integer NOT NULL,
    contestants integer NOT NULL
);


--
-- Name: TABLE boot_horse_races; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE boot_horse_races IS 'The initial fake races used to create the race statistics for tables with insufficient data.';


--
-- Name: corporate_deposit_transfers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE corporate_deposit_transfers (
    transfer_id bigint NOT NULL,
    transfer_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    transfer_user bigint NOT NULL,
    user_id bigint NOT NULL,
    amount numeric(16,6) NOT NULL,
    transfer_desc character varying(255)
);


--
-- Name: corporate_deposit_transfers_transfer_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE corporate_deposit_transfers_transfer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: corporate_deposit_transfers_transfer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE corporate_deposit_transfers_transfer_id_seq OWNED BY corporate_deposit_transfers.transfer_id;


--
-- Name: corporate_game_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE corporate_game_settings (
    corporate_id integer NOT NULL,
    game_id integer NOT NULL,
    min_bet numeric(16,6),
    max_bet numeric(16,6)
);


--
-- Name: corporate_system_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE corporate_system_settings (
    corporate_id bigint NOT NULL,
    setting_key character varying(50) NOT NULL,
    setting_value character varying(100)
);


--
-- Name: countries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE countries (
    country_code character varying(2) NOT NULL,
    country_name character varying(100) NOT NULL,
    accept boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE countries; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE countries IS 'Countries and country codes';


--
-- Name: COLUMN countries.country_code; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN countries.country_code IS 'ISO 3166 country code';


--
-- Name: dogs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dogs (
    dog_id integer NOT NULL,
    dog_name character varying(100) NOT NULL
);


--
-- Name: game_client_games; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_client_games (
    client_id integer NOT NULL,
    game_id integer NOT NULL,
    enabled boolean DEFAULT true NOT NULL
);


--
-- Name: TABLE game_client_games; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_client_games IS 'Games for each client';


--
-- Name: game_clients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_clients (
    client_id integer NOT NULL,
    client_description character varying(100) NOT NULL,
    client_version integer NOT NULL,
    client_latest_version character varying(100)
);


--
-- Name: TABLE game_clients; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_clients IS 'Supported gaming clients for the system';


--
-- Name: game_config_changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_config_changes (
    game_id bigint NOT NULL,
    modifier bigint NOT NULL,
    change_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    change_id bigint NOT NULL,
    old_config_id character varying(50),
    new_config_id character varying(50) NOT NULL
);


--
-- Name: game_config_changes_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE game_config_changes_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: game_config_changes_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE game_config_changes_change_id_seq OWNED BY game_config_changes.change_id;


--
-- Name: game_config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE game_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: game_configs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_configs (
    game_id integer NOT NULL,
    config_value character varying(8192) NOT NULL,
    config_comment character varying(255),
    config_date timestamp without time zone DEFAULT now_utc(),
    config_id bigint DEFAULT nextval('game_config_id_seq'::regclass) NOT NULL,
    modified_by bigint
);


--
-- Name: TABLE game_configs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_configs IS 'This table holds the configuration profiles for each game.';


--
-- Name: game_current_configs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_current_configs (
    game_id integer NOT NULL,
    config_id bigint NOT NULL
);


--
-- Name: game_fixed_results; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_fixed_results (
    table_id bigint NOT NULL,
    round_id integer NOT NULL,
    result character varying(256) NOT NULL
);


--
-- Name: TABLE game_fixed_results; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_fixed_results IS 'Do NOT install on production servers!';


--
-- Name: game_period_amounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_period_amounts (
    period_date timestamp without time zone NOT NULL,
    user_id bigint NOT NULL,
    table_id bigint NOT NULL,
    bets numeric(16,6) DEFAULT 0 NOT NULL,
    wins numeric(16,6) DEFAULT 0 NOT NULL,
    jackpot_wins numeric(16,6) DEFAULT 0 NOT NULL,
    rounds integer DEFAULT 1 NOT NULL,
    real_bets numeric(16,6) DEFAULT 0,
    real_wins numeric(16,6) DEFAULT 0,
    jackpot_contribs numeric(16,6) DEFAULT 0 NOT NULL
);


--
-- Name: COLUMN game_period_amounts.wins; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_period_amounts.wins IS 'Total wins (not including jackpot wins)';


--
-- Name: COLUMN game_period_amounts.jackpot_wins; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_period_amounts.jackpot_wins IS 'Jackpot wins (real money)';


--
-- Name: COLUMN game_period_amounts.real_wins; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_period_amounts.real_wins IS 'Real money wins (not including jackpot wins)';


--
-- Name: game_round_actions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_round_actions (
    table_id bigint NOT NULL,
    round_id integer NOT NULL,
    action_id integer NOT NULL,
    seat_id integer NOT NULL,
    action_desc character varying(20) NOT NULL,
    action_area character varying(100),
    amount numeric(16,6),
    action_value character varying(100),
    action_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    user_id bigint,
    sub_action_id integer NOT NULL
);


--
-- Name: TABLE game_round_actions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_round_actions IS 'Finished actions for each round.';


--
-- Name: COLUMN game_round_actions.amount; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_round_actions.amount IS 'The amount is automaticaly subtracted from the user balance.
If action_desc is ''WIN'' then the amount is added to the user balance.';


--
-- Name: COLUMN game_round_actions.sub_action_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_round_actions.sub_action_id IS 'Sub action id is 0 for the root actions. >0 for generated actions.';


--
-- Name: game_rounds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_rounds (
    table_id bigint NOT NULL,
    round_id integer NOT NULL,
    date_opened timestamp without time zone DEFAULT now_utc(),
    date_closed timestamp without time zone,
    random_seed bytea,
    round_started boolean DEFAULT false NOT NULL,
    game_config_id bigint
);


--
-- Name: TABLE game_rounds; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_rounds IS 'Game rounds for each table.
Rounds should be created with create_round() and
closed with finish_round().';


--
-- Name: COLUMN game_rounds.date_closed; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_rounds.date_closed IS 'Updated automaticaly when finish_round() is called.
Do NOT update manually.';


--
-- Name: game_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_settings (
    game_id integer NOT NULL,
    setting_key character varying(50) NOT NULL,
    setting_value character varying(100) NOT NULL
);


--
-- Name: TABLE game_settings; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_settings IS 'Per-Game settings';


--
-- Name: game_settings_changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_settings_changes (
    game_id integer NOT NULL,
    setting_key character varying(50) NOT NULL,
    change_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    change_user bigint NOT NULL,
    old_value character varying(100),
    new_value character varying(100),
    change_id bigint NOT NULL
);


--
-- Name: game_settings_changes_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE game_settings_changes_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: game_settings_changes_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE game_settings_changes_change_id_seq OWNED BY game_settings_changes.change_id;


--
-- Name: game_table_last_rounds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_table_last_rounds (
    table_id bigint NOT NULL,
    last_round_id integer,
    last_bet_round_id integer
);


--
-- Name: TABLE game_table_last_rounds; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_table_last_rounds IS 'Keeps last round and last bet round for faster access.';


--
-- Name: COLUMN game_table_last_rounds.last_round_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_table_last_rounds.last_round_id IS 'Last round for the table.';


--
-- Name: COLUMN game_table_last_rounds.last_bet_round_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_table_last_rounds.last_bet_round_id IS 'Last round for the table that the player has placed a bet.';


--
-- Name: game_table_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_table_users (
    table_id bigint NOT NULL,
    user_id bigint NOT NULL,
    is_active boolean DEFAULT true NOT NULL
);


--
-- Name: TABLE game_table_users; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_table_users IS 'Active and inactive viewers for each table';


--
-- Name: game_tables; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_tables (
    table_id bigint DEFAULT nextval(('table_id_seq'::text)::regclass) NOT NULL,
    table_owner bigint NOT NULL,
    date_created timestamp without time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(6) with time zone) NOT NULL,
    date_closed timestamp without time zone,
    game_type_id integer NOT NULL,
    server_id integer NOT NULL
);


--
-- Name: TABLE game_tables; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_tables IS 'Game tables';


--
-- Name: COLUMN game_tables.server_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_tables.server_id IS 'Server Id for the server handling this table';


--
-- Name: game_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE game_types (
    game_type_id integer NOT NULL,
    game_type_variant character varying(100) NOT NULL,
    game_category_id integer NOT NULL,
    min_bet numeric(16,6),
    max_bet numeric(16,6),
    game_template character varying(50) NOT NULL
);


--
-- Name: TABLE game_types; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE game_types IS 'Game type definitions';


--
-- Name: COLUMN game_types.game_type_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_types.game_type_id IS 'Game Id. Ids are global for all systems.';


--
-- Name: COLUMN game_types.game_type_variant; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_types.game_type_variant IS 'The user visible name of the game';


--
-- Name: COLUMN game_types.min_bet; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_types.min_bet IS 'Game Minimum bet.';


--
-- Name: COLUMN game_types.max_bet; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_types.max_bet IS 'Global maximum bet.';


--
-- Name: COLUMN game_types.game_template; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN game_types.game_template IS 'The game template implementing this game.';


--
-- Name: horses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE horses (
    horse_id integer NOT NULL,
    horse_name character varying(100) NOT NULL
);


--
-- Name: hosts_whitelist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE hosts_whitelist (
    item_id bigint NOT NULL,
    item_desc character varying(255),
    item_ip inet NOT NULL
);


--
-- Name: TABLE hosts_whitelist; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE hosts_whitelist IS 'Network addresses allowed access to backoffice. The list is checked if the relevant setting is enabled';


--
-- Name: hosts_whitelist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hosts_whitelist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hosts_whitelist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hosts_whitelist_item_id_seq OWNED BY hosts_whitelist.item_id;


--
-- Name: jackpot_balances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE jackpot_balances (
    jackpot_id integer NOT NULL,
    balance numeric(16,6) NOT NULL,
    winners integer,
    last_win_transaction bigint,
    CONSTRAINT "CHECK_balance_positive" CHECK ((balance >= (0)::numeric))
);


--
-- Name: jackpot_configs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE jackpot_configs (
    initial_amount numeric(16,6) NOT NULL,
    bet_contrib numeric(7,4) NOT NULL,
    maximum_amount numeric(16,6),
    config_id integer NOT NULL,
    draw_number integer NOT NULL,
    min_payout numeric(16,6),
    min_jackpot_balance numeric(16,6)
);


--
-- Name: COLUMN jackpot_configs.min_payout; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpot_configs.min_payout IS 'The minimum amount to be paid to a winner (betting 100% of max bet otherwise % of minimum amount).';


--
-- Name: COLUMN jackpot_configs.min_jackpot_balance; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpot_configs.min_jackpot_balance IS 'Minimum amount to pay jackpot.';


--
-- Name: jackpot_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE jackpot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jackpot_period_balances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE jackpot_period_balances (
    jackpot_id integer NOT NULL,
    period_date timestamp without time zone NOT NULL,
    balance numeric(16,6) NOT NULL
);


--
-- Name: jackpot_properties_changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE jackpot_properties_changes (
    change_id bigint NOT NULL,
    jackpot_id integer,
    jackpot_config_id integer,
    new_pay_count integer,
    new_initial_amount numeric(16,6),
    new_bet_contrib numeric(7,4),
    new_bet_amount numeric(16,6),
    new_maximum_amount numeric(16,6),
    new_draw_number integer,
    user_id bigint NOT NULL,
    new_auto boolean,
    old_pay_count integer,
    old_initial_amount numeric(16,6),
    old_bet_contrib numeric(7,4),
    old_bet_amount numeric(16,6),
    old_maximum_amount numeric(16,6),
    old_draw_number integer,
    old_auto boolean,
    change_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    old_min_payout numeric(16,6),
    new_min_payout numeric(16,6),
    old_min_jackpot_balance numeric(16,6),
    new_min_jackpot_balance numeric(16,6)
);


--
-- Name: jackpot_properties_changes_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE jackpot_properties_changes_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jackpot_properties_changes_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE jackpot_properties_changes_change_id_seq OWNED BY jackpot_properties_changes.change_id;


--
-- Name: jackpot_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE jackpot_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jackpot_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE jackpot_transactions (
    jackpot_id integer NOT NULL,
    win_id integer,
    transaction_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    amount numeric(16,6) NOT NULL,
    user_id bigint,
    table_id bigint,
    round_id integer,
    jackpot_transaction_id bigint DEFAULT nextval('jackpot_transaction_id_seq'::regclass) NOT NULL
);


--
-- Name: TABLE jackpot_transactions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE jackpot_transactions IS 'Jackpot Transactions. Only winners are saved. Contributions and adjustments trigger balance updates etc but are not actually saved.';


--
-- Name: jackpots; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE jackpots (
    jackpot_id integer DEFAULT nextval('jackpot_id_seq'::regclass) NOT NULL,
    game_id integer,
    finish_date timestamp without time zone,
    pay_count integer,
    initial_amount numeric(16,6),
    bet_contrib numeric(7,4),
    bet_amount numeric(16,6),
    start_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    maximum_amount numeric(16,6),
    auto boolean DEFAULT true NOT NULL,
    target_corporate bigint,
    config_id integer,
    draw_number integer,
    min_payout numeric(16,6),
    min_jackpot_balance numeric(16,6)
);


--
-- Name: TABLE jackpots; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE jackpots IS 'Jackpots for each game.';


--
-- Name: COLUMN jackpots.game_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpots.game_id IS 'Game ID can be null. Null indicates a  global jackpot with no regards to the game a.k.a. Mega Jackpot.';


--
-- Name: COLUMN jackpots.auto; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpots.auto IS 'TRUE = Normal jackpot paid from game rules, FALSE = mystery jackpot paid from draw';


--
-- Name: COLUMN jackpots.target_corporate; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpots.target_corporate IS 'Null value indicates a system-wide jackpot, otherwise the jackpot applies only to the specified corporate account specified.';


--
-- Name: COLUMN jackpots.min_payout; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpots.min_payout IS 'The minimum amount to be paid to a winner (betting 100% of max bet otherwise % of minimum amount).';


--
-- Name: COLUMN jackpots.min_jackpot_balance; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN jackpots.min_jackpot_balance IS 'Minimum amount to pay jackpot.';


--
-- Name: latest_gaming_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE latest_gaming_transactions (
    user_id bigint NOT NULL,
    amount numeric(16,6) NOT NULL,
    transaction_id bigint NOT NULL
);


--
-- Name: latest_non_gaming_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE latest_non_gaming_transactions (
    user_id bigint NOT NULL,
    amount numeric(16,6) NOT NULL,
    transaction_id bigint NOT NULL
);


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_transactions (
    transaction_id bigint DEFAULT nextval('transaction_id_seq'::regclass) NOT NULL,
    user_id bigint NOT NULL,
    amount numeric(16,6) NOT NULL,
    ps_user_account character varying(256),
    ps_reference_no character varying(128),
    time_completed timestamp without time zone DEFAULT now_utc() NOT NULL,
    comment character varying(255),
    approve_user_id bigint,
    ps_specific character varying(50)
);


--
-- Name: payment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_type (
    payment_type character varying(50) NOT NULL,
    enabled boolean NOT NULL,
    supports_withdrawals boolean DEFAULT true NOT NULL,
    supports_deposits boolean DEFAULT true NOT NULL,
    weight integer
);


--
-- Name: payout_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payout_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payout_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payout_transactions (
    payout_id bigint DEFAULT nextval('payout_id_seq'::regclass) NOT NULL,
    user_id bigint NOT NULL,
    ps_user_account character varying(256),
    amount numeric(16,6) NOT NULL,
    time_completed timestamp without time zone DEFAULT now_utc(),
    approve_user_id bigint,
    ps_reference_no character varying(128),
    comment character varying(255),
    ps_specific character varying(50)
);


--
-- Name: TABLE payout_transactions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE payout_transactions IS 'Payouts. Withdrawals from the internal account.';


--
-- Name: COLUMN payout_transactions.ps_specific; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN payout_transactions.ps_specific IS 'Payment method (Credit Card, Wire Transfer etc)';


--
-- Name: player_favourite_games; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE player_favourite_games (
    user_id bigint NOT NULL,
    game_id integer NOT NULL,
    date_added timestamp without time zone DEFAULT now_utc() NOT NULL
);


--
-- Name: promo_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE promo_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: promo_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE promo_transactions (
    promo_transaction_id bigint DEFAULT nextval('promo_transaction_id_seq'::regclass) NOT NULL,
    user_id bigint NOT NULL,
    transaction_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    amount numeric(16,6) NOT NULL,
    new_balance numeric(16,6),
    new_play_credits numeric(16,6),
    table_id bigint,
    round_id integer,
    action_id integer,
    sub_action_id integer
);


--
-- Name: TABLE promo_transactions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE promo_transactions IS 'Transactions that affect play credits.';


--
-- Name: report_generation_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE report_generation_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: report_generation_logs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE report_generation_logs (
    user_id bigint NOT NULL,
    application_name character varying(50) NOT NULL,
    report_name character varying(50) NOT NULL,
    from_date timestamp without time zone,
    to_date timestamp without time zone,
    user_ip character varying(50),
    generation_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    report_generation_log_id bigint DEFAULT nextval('report_generation_log_id_seq'::regclass) NOT NULL,
    request_url character varying(4096)
);


--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_privileges; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE role_privileges (
    role_id integer NOT NULL,
    privilege_key character varying(50) NOT NULL,
    allow_write boolean NOT NULL
);


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE roles (
    role_id integer DEFAULT nextval('role_id_seq'::regclass) NOT NULL,
    role_desc character varying(50) NOT NULL
);


--
-- Name: TABLE roles; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE roles IS 'User roles definitions';


--
-- Name: roulette_results; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE roulette_results (
    table_id bigint NOT NULL,
    round_id integer NOT NULL,
    result integer NOT NULL
);


--
-- Name: servers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE servers (
    server_id integer NOT NULL,
    server_desc character varying(50) NOT NULL,
    server_local_address character varying(50) NOT NULL,
    server_local_port integer NOT NULL,
    server_running boolean DEFAULT false NOT NULL
);


--
-- Name: TABLE servers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE servers IS 'Gaming servers';


--
-- Name: COLUMN servers.server_local_address; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN servers.server_local_address IS 'The server''s local address. This is used for administration.';


--
-- Name: COLUMN servers.server_local_port; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN servers.server_local_port IS 'The port for the server''s local interface. (Usually it is 7765)';


--
-- Name: system_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE system_settings (
    setting_key character varying(50) NOT NULL,
    setting_value character varying(100)
);


--
-- Name: TABLE system_settings; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE system_settings IS 'Contains system-wide settings.';


--
-- Name: system_settings_changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE system_settings_changes (
    setting_key character varying(50) NOT NULL,
    change_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    change_user bigint NOT NULL,
    old_value character varying(100),
    new_value character varying(100),
    change_id bigint NOT NULL
);


--
-- Name: system_settings_changes_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE system_settings_changes_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_settings_changes_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE system_settings_changes_change_id_seq OWNED BY system_settings_changes.change_id;


--
-- Name: table_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE table_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_balance; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_balance (
    user_id bigint NOT NULL,
    balance numeric(16,6) DEFAULT 0.0 NOT NULL,
    play_credits numeric(16,6) DEFAULT 0 NOT NULL,
    CONSTRAINT "CHECK_user_balance_positive" CHECK ((balance >= (0)::numeric)),
    CONSTRAINT "CHECK_user_play_credits_positive" CHECK ((play_credits >= (0)::numeric))
);


--
-- Name: TABLE user_balance; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE user_balance IS 'Current balance for each user.
This table should never be changed directly. Insert records into user_transactions instead.';


--
-- Name: user_balance_adjustments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_balance_adjustments (
    user_id bigint NOT NULL,
    adjustment_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    adjustment_user bigint NOT NULL,
    adjustment_description character varying(100),
    adjustment_amount numeric(16,6) NOT NULL,
    adjustment_id bigint DEFAULT nextval('adjustment_id_seq'::regclass) NOT NULL
);


--
-- Name: TABLE user_balance_adjustments; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE user_balance_adjustments IS 'Balance adjustments for users.';


--
-- Name: COLUMN user_balance_adjustments.adjustment_user; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN user_balance_adjustments.adjustment_user IS 'The user who made the adjustment. It can be the same user or an administrator';


--
-- Name: user_changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_changes (
    change_id bigint NOT NULL,
    change_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    user_id bigint,
    old_username character varying(50),
    old_password_hash character varying(40),
    old_password_salt character varying(8),
    old_email character varying(75),
    old_is_active boolean,
    old_first_name character varying(100),
    old_last_name character varying(100),
    old_middle_name character varying(100),
    old_country character varying(2),
    old_phone character varying(40),
    old_region character varying(50),
    old_town character varying(100),
    old_postal_code character varying(20),
    old_nickname character varying(75),
    old_time_zone character varying(32),
    old_user_type integer,
    old_postal_address character varying(255),
    old_date_deactivated timestamp without time zone,
    new_username character varying(50),
    new_password_hash character varying(40),
    new_password_salt character varying(8),
    new_email character varying(75),
    new_is_active boolean,
    new_first_name character varying(100),
    new_last_name character varying(100),
    new_middle_name character varying(100),
    new_country character varying(2),
    new_phone character varying(40),
    new_region character varying(50),
    new_town character varying(100),
    new_postal_code character varying(20),
    new_nickname character varying(75),
    new_time_zone character varying(32),
    new_user_type integer,
    new_postal_address character varying(255),
    new_date_deactivated timestamp without time zone,
    change_by_user bigint,
    old_closed boolean,
    new_closed boolean
);


--
-- Name: user_changes_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_changes_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_changes_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_changes_change_id_seq OWNED BY user_changes.change_id;


--
-- Name: user_comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_comments (
    message_id bigint NOT NULL,
    commenter_id bigint NOT NULL,
    user_id bigint NOT NULL,
    date_created timestamp without time zone DEFAULT now_utc() NOT NULL,
    comment character varying(1024) NOT NULL
);


--
-- Name: user_comments_message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_comments_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_comments_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_comments_message_id_seq OWNED BY user_comments.message_id;


--
-- Name: user_devices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_devices (
    user_id bigint NOT NULL,
    hash_code character varying(256) NOT NULL,
    valid_until timestamp without time zone,
    approved_by_user bigint,
    approved_date timestamp without time zone
);


--
-- Name: user_failed_logins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_failed_logins (
    login_fail_id bigint NOT NULL,
    username character varying(100),
    user_id bigint,
    device_hash_code character varying(256),
    fail_reason integer DEFAULT 0 NOT NULL,
    password_matched boolean,
    ip_address character varying(256),
    fail_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    server_id integer NOT NULL
);


--
-- Name: COLUMN user_failed_logins.fail_reason; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN user_failed_logins.fail_reason IS '0 = uknown
1 = wrong username
2 = wrong password
3 = wrong device hash
4 = user limits exceed
5 = db error
6 = already logged in';


--
-- Name: user_failed_logins_login_fail_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_failed_logins_login_fail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_failed_logins_login_fail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_failed_logins_login_fail_id_seq OWNED BY user_failed_logins.login_fail_id;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_logins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_logins (
    user_id bigint NOT NULL,
    server_id integer NOT NULL,
    login_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    logout_date timestamp without time zone,
    client_ip character varying(30) NOT NULL,
    user_login_id bigint NOT NULL,
    login_token character varying(80),
    device_hash_code character varying(256)
);


--
-- Name: TABLE user_logins; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE user_logins IS 'Records current and previous logins per user for each server.';


--
-- Name: user_logins_user_login_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_logins_user_login_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_logins_user_login_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_logins_user_login_id_seq OWNED BY user_logins.user_login_id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_roles (
    user_id bigint NOT NULL,
    role_id integer NOT NULL
);


--
-- Name: TABLE user_roles; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE user_roles IS 'Assigned roles for each user';


--
-- Name: user_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_transactions (
    user_id bigint NOT NULL,
    transaction_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    amount numeric(16,6) NOT NULL,
    payment_transaction_id bigint,
    payout_id bigint,
    action_table_id bigint,
    action_round_id integer,
    action_action_id integer,
    action_sub_action_id integer,
    adjustment_id bigint,
    new_balance numeric(16,6),
    new_play_credits numeric(16,6),
    user_transaction_id bigint DEFAULT nextval('user_transaction_id_seq'::regclass) NOT NULL,
    deposit_transfer_id bigint,
    affiliate_earnings_transaction_id bigint,
    details character varying(100)
);


--
-- Name: TABLE user_transactions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE user_transactions IS 'Records all balance changing activities for each user.
Automatically updates the user balance and makes necessary balance checks.';


--
-- Name: COLUMN user_transactions.new_balance; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN user_transactions.new_balance IS 'Automatically updated by the trigger.';


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    user_id bigint DEFAULT nextval(('user_id_seq'::text)::regclass) NOT NULL,
    username character varying(50) NOT NULL,
    password_hash character varying(40) NOT NULL,
    password_salt character varying(8) NOT NULL,
    email character varying(75),
    is_active boolean DEFAULT true NOT NULL,
    first_name character varying(100),
    last_name character varying(100),
    middle_name character varying(100),
    country character varying(2),
    phone character varying(40),
    region character varying(50),
    town character varying(100),
    postal_code character varying(20),
    nickname character varying(75),
    time_zone character varying(32) DEFAULT 'UTC'::character varying NOT NULL,
    register_date timestamp without time zone DEFAULT now_utc() NOT NULL,
    user_type integer DEFAULT 0 NOT NULL,
    postal_address character varying(255),
    date_deactivated timestamp without time zone,
    modifier_user bigint,
    closed boolean DEFAULT false NOT NULL,
    CONSTRAINT check_nickname CHECK (((nickname)::text !~ similar_escape('%(:|")%'::text, NULL::text))),
    CONSTRAINT check_user_type CHECK ((user_type = ANY (ARRAY[0, 3, 4, 5, 6])))
);


--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE users IS 'Lists all users in the system';


--
-- Name: COLUMN users.region; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN users.region IS 'User State/Region';


--
-- Name: COLUMN users.user_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN users.user_type IS 'User type:

0 - Player
3 - Staff
4 - Corporate
5 - Corporate Player
6 - Demo user';


--
-- Name: users_blacklist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users_blacklist (
    item_id bigint NOT NULL,
    username character varying(50),
    first_name character varying(100),
    last_name character varying(100),
    email character varying(75),
    country character varying(2)
);


--
-- Name: users_blacklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_blacklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_blacklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_blacklist_item_id_seq OWNED BY users_blacklist.item_id;


--
-- Name: web_login_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE web_login_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: web_logins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE web_logins (
    user_id bigint NOT NULL,
    login_time timestamp without time zone DEFAULT now_utc() NOT NULL,
    web_login_id bigint DEFAULT nextval('web_login_id_seq'::regclass) NOT NULL,
    application character varying,
    client_ip character varying(50)
);


--
-- Name: change_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_changes ALTER COLUMN change_id SET DEFAULT nextval('affiliate_changes_change_id_seq'::regclass);


--
-- Name: transaction_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_earnings_transactions ALTER COLUMN transaction_id SET DEFAULT nextval('affiliate_payments_payment_id_seq'::regclass);


--
-- Name: transfer_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_deposit_transfers ALTER COLUMN transfer_id SET DEFAULT nextval('corporate_deposit_transfers_transfer_id_seq'::regclass);


--
-- Name: change_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_config_changes ALTER COLUMN change_id SET DEFAULT nextval('game_config_changes_change_id_seq'::regclass);


--
-- Name: change_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_settings_changes ALTER COLUMN change_id SET DEFAULT nextval('game_settings_changes_change_id_seq'::regclass);


--
-- Name: item_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY hosts_whitelist ALTER COLUMN item_id SET DEFAULT nextval('hosts_whitelist_item_id_seq'::regclass);


--
-- Name: change_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_properties_changes ALTER COLUMN change_id SET DEFAULT nextval('jackpot_properties_changes_change_id_seq'::regclass);


--
-- Name: change_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY system_settings_changes ALTER COLUMN change_id SET DEFAULT nextval('system_settings_changes_change_id_seq'::regclass);


--
-- Name: change_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_changes ALTER COLUMN change_id SET DEFAULT nextval('user_changes_change_id_seq'::regclass);


--
-- Name: message_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_comments ALTER COLUMN message_id SET DEFAULT nextval('user_comments_message_id_seq'::regclass);


--
-- Name: login_fail_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_failed_logins ALTER COLUMN login_fail_id SET DEFAULT nextval('user_failed_logins_login_fail_id_seq'::regclass);


--
-- Name: user_login_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_logins ALTER COLUMN user_login_id SET DEFAULT nextval('user_logins_user_login_id_seq'::regclass);


--
-- Name: item_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_blacklist ALTER COLUMN item_id SET DEFAULT nextval('users_blacklist_item_id_seq'::regclass);


--
-- Name: PK_affiliate_changes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_changes
    ADD CONSTRAINT "PK_affiliate_changes" PRIMARY KEY (change_id);


--
-- Name: PK_affiliate_payments; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_earnings_transactions
    ADD CONSTRAINT "PK_affiliate_payments" PRIMARY KEY (transaction_id);


--
-- Name: PK_affiliate_period_amounts; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_period_amounts
    ADD CONSTRAINT "PK_affiliate_period_amounts" PRIMARY KEY (period_date, affiliate_id, user_id);


--
-- Name: PK_affiliates; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliates
    ADD CONSTRAINT "PK_affiliates" PRIMARY KEY (affiliate_id);


--
-- Name: PK_corporate_deposit_transfers; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_deposit_transfers
    ADD CONSTRAINT "PK_corporate_deposit_transfers" PRIMARY KEY (transfer_id);


--
-- Name: PK_corporate_game_settings; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_game_settings
    ADD CONSTRAINT "PK_corporate_game_settings" PRIMARY KEY (corporate_id, game_id);


--
-- Name: PK_countries; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT "PK_countries" PRIMARY KEY (country_code);


--
-- Name: PK_game_client_games; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_client_games
    ADD CONSTRAINT "PK_game_client_games" PRIMARY KEY (client_id, game_id);


--
-- Name: PK_game_clients; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_clients
    ADD CONSTRAINT "PK_game_clients" PRIMARY KEY (client_id);


--
-- Name: PK_game_fixed_results; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_fixed_results
    ADD CONSTRAINT "PK_game_fixed_results" PRIMARY KEY (table_id, round_id);


--
-- Name: PK_game_period_amounts; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_period_amounts
    ADD CONSTRAINT "PK_game_period_amounts" PRIMARY KEY (period_date, user_id, table_id);


--
-- Name: PK_game_round_actions; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_round_actions
    ADD CONSTRAINT "PK_game_round_actions" PRIMARY KEY (table_id, round_id, action_id, sub_action_id);


--
-- Name: PK_game_settings_changes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_settings_changes
    ADD CONSTRAINT "PK_game_settings_changes" PRIMARY KEY (change_id);


--
-- Name: PK_game_table; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_tables
    ADD CONSTRAINT "PK_game_table" PRIMARY KEY (table_id);


--
-- Name: PK_hosts_whitelist; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY hosts_whitelist
    ADD CONSTRAINT "PK_hosts_whitelist" PRIMARY KEY (item_id);


--
-- Name: PK_jackpot_period_balances; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_period_balances
    ADD CONSTRAINT "PK_jackpot_period_balances" PRIMARY KEY (jackpot_id, period_date);


--
-- Name: PK_jackpot_properties_changes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_properties_changes
    ADD CONSTRAINT "PK_jackpot_properties_changes" PRIMARY KEY (change_id);


--
-- Name: PK_jackpot_transactions; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_transactions
    ADD CONSTRAINT "PK_jackpot_transactions" PRIMARY KEY (jackpot_transaction_id);


--
-- Name: PK_promo_transactions; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY promo_transactions
    ADD CONSTRAINT "PK_promo_transactions" PRIMARY KEY (promo_transaction_id);


--
-- Name: PK_report_generation_log; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY report_generation_logs
    ADD CONSTRAINT "PK_report_generation_log" PRIMARY KEY (report_generation_log_id);


--
-- Name: PK_roulette_results; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roulette_results
    ADD CONSTRAINT "PK_roulette_results" PRIMARY KEY (table_id, round_id);


--
-- Name: PK_system_settings_changes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY system_settings_changes
    ADD CONSTRAINT "PK_system_settings_changes" PRIMARY KEY (change_id);


--
-- Name: PK_user_balance; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_balance
    ADD CONSTRAINT "PK_user_balance" PRIMARY KEY (user_id);


--
-- Name: PK_user_balance_adjustments; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_balance_adjustments
    ADD CONSTRAINT "PK_user_balance_adjustments" PRIMARY KEY (user_id, adjustment_id);


--
-- Name: PK_user_changes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_changes
    ADD CONSTRAINT "PK_user_changes" PRIMARY KEY (change_id);


--
-- Name: PK_user_devices; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_devices
    ADD CONSTRAINT "PK_user_devices" PRIMARY KEY (user_id, hash_code);


--
-- Name: PK_user_failed_logins; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_failed_logins
    ADD CONSTRAINT "PK_user_failed_logins" PRIMARY KEY (login_fail_id);


--
-- Name: PK_user_logins; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_logins
    ADD CONSTRAINT "PK_user_logins" PRIMARY KEY (user_login_id);


--
-- Name: PK_user_transactions; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "PK_user_transactions" PRIMARY KEY (user_transaction_id);


--
-- Name: PK_users; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "PK_users" PRIMARY KEY (user_id);


--
-- Name: PK_users_blacklist; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_blacklist
    ADD CONSTRAINT "PK_users_blacklist" PRIMARY KEY (item_id);


--
-- Name: PK_web_logins; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY web_logins
    ADD CONSTRAINT "PK_web_logins" PRIMARY KEY (web_login_id);


--
-- Name: PM_PMNT_TYPE; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type
    ADD CONSTRAINT "PM_PMNT_TYPE" PRIMARY KEY (payment_type);


--
-- Name: UNIQUE_country_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT "UNIQUE_country_name" UNIQUE (country_name);


--
-- Name: UNIQUE_dog_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY dogs
    ADD CONSTRAINT "UNIQUE_dog_name" UNIQUE (dog_name);


--
-- Name: corporate_system_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_system_settings
    ADD CONSTRAINT corporate_system_settings_pkey PRIMARY KEY (corporate_id, setting_key);


--
-- Name: game_config_changes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_config_changes
    ADD CONSTRAINT game_config_changes_pkey PRIMARY KEY (change_id);


--
-- Name: game_current_configs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_current_configs
    ADD CONSTRAINT game_current_configs_pkey PRIMARY KEY (game_id);


--
-- Name: latest_gaming_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY latest_gaming_transactions
    ADD CONSTRAINT latest_gaming_transactions_pkey PRIMARY KEY (user_id);


--
-- Name: latest_non_gaming_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY latest_non_gaming_transactions
    ADD CONSTRAINT latest_non_gaming_transactions_pkey PRIMARY KEY (user_id);


--
-- Name: pk_affiliate_earnings; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_earnings
    ADD CONSTRAINT pk_affiliate_earnings PRIMARY KEY (affiliate_id);


--
-- Name: pk_affiliate_users; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_users
    ADD CONSTRAINT pk_affiliate_users PRIMARY KEY (user_id);


--
-- Name: pk_boot_dog_races; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY boot_dog_races
    ADD CONSTRAINT pk_boot_dog_races PRIMARY KEY (table_id, dog_id, race_no);


--
-- Name: pk_boot_horse_races; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY boot_horse_races
    ADD CONSTRAINT pk_boot_horse_races PRIMARY KEY (table_id, horse_id, race_no);


--
-- Name: pk_dogs; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY dogs
    ADD CONSTRAINT pk_dogs PRIMARY KEY (dog_id);


--
-- Name: pk_game_configs; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_configs
    ADD CONSTRAINT pk_game_configs PRIMARY KEY (config_id);


--
-- Name: pk_game_rounds; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_rounds
    ADD CONSTRAINT pk_game_rounds PRIMARY KEY (table_id, round_id);


--
-- Name: pk_game_settings; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_settings
    ADD CONSTRAINT pk_game_settings PRIMARY KEY (game_id, setting_key);


--
-- Name: pk_game_table_last_rounds; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_table_last_rounds
    ADD CONSTRAINT pk_game_table_last_rounds PRIMARY KEY (table_id);


--
-- Name: pk_game_table_users; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_table_users
    ADD CONSTRAINT pk_game_table_users PRIMARY KEY (table_id, user_id);


--
-- Name: pk_game_types; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_types
    ADD CONSTRAINT pk_game_types PRIMARY KEY (game_type_id);


--
-- Name: pk_horses; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY horses
    ADD CONSTRAINT pk_horses PRIMARY KEY (horse_id);


--
-- Name: pk_jackpot_balances; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_balances
    ADD CONSTRAINT pk_jackpot_balances PRIMARY KEY (jackpot_id);


--
-- Name: pk_jackpot_configs; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_configs
    ADD CONSTRAINT pk_jackpot_configs PRIMARY KEY (config_id);


--
-- Name: pk_jackpots; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpots
    ADD CONSTRAINT pk_jackpots PRIMARY KEY (jackpot_id);


--
-- Name: pk_payout_transactions; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payout_transactions
    ADD CONSTRAINT pk_payout_transactions PRIMARY KEY (payout_id);


--
-- Name: pk_role_privileges; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_privileges
    ADD CONSTRAINT pk_role_privileges PRIMARY KEY (role_id, privilege_key);


--
-- Name: pk_roles; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT pk_roles PRIMARY KEY (role_id);


--
-- Name: pk_servers_server_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servers
    ADD CONSTRAINT pk_servers_server_id PRIMARY KEY (server_id);


--
-- Name: pk_system_settings; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY system_settings
    ADD CONSTRAINT pk_system_settings PRIMARY KEY (setting_key);


--
-- Name: pk_transactions; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_transactions
    ADD CONSTRAINT pk_transactions PRIMARY KEY (transaction_id);


--
-- Name: pk_user_roles; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT pk_user_roles PRIMARY KEY (user_id, role_id);


--
-- Name: player_favourite_games_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY player_favourite_games
    ADD CONSTRAINT player_favourite_games_pkey PRIMARY KEY (user_id, game_id);


--
-- Name: unique_jackpot_id_win_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_transactions
    ADD CONSTRAINT unique_jackpot_id_win_id UNIQUE (jackpot_id, win_id);


--
-- Name: unique_role_desc; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT unique_role_desc UNIQUE (role_desc);


--
-- Name: user_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_comments
    ADD CONSTRAINT user_comments_pkey PRIMARY KEY (message_id);


--
-- Name: FKI_affiliate_changes_affiliates; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_affiliate_changes_affiliates" ON affiliate_changes USING btree (affiliate_id);


--
-- Name: FKI_affiliate_period_amounts_affiliates; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_affiliate_period_amounts_affiliates" ON affiliate_period_amounts USING btree (affiliate_id);


--
-- Name: FKI_affiliate_period_amounts_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_affiliate_period_amounts_users" ON affiliate_period_amounts USING btree (user_id);


--
-- Name: FKI_affiliates_modifier_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_affiliates_modifier_users" ON affiliates USING btree (modifier_user);


--
-- Name: FKI_corporate_deposit_transfers_transfer_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_corporate_deposit_transfers_transfer_user" ON corporate_deposit_transfers USING btree (transfer_user);


--
-- Name: FKI_corporate_deposit_transfers_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_corporate_deposit_transfers_users" ON corporate_deposit_transfers USING btree (user_id);


--
-- Name: FKI_country; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_country" ON users USING btree (country);


--
-- Name: FKI_game_table_users_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_game_table_users_user" ON game_table_users USING btree (user_id);


--
-- Name: FKI_jackpot_balances_jackpot_transactions; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_jackpot_balances_jackpot_transactions" ON jackpot_balances USING btree (last_win_transaction);


--
-- Name: FKI_payment_transactions_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_payment_transactions_user" ON payment_transactions USING btree (user_id);


--
-- Name: FKI_payout_transactions_approve_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_payout_transactions_approve_users" ON payout_transactions USING btree (approve_user_id);


--
-- Name: FKI_promo_transactions_game_actions; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_promo_transactions_game_actions" ON promo_transactions USING btree (table_id, round_id, action_id, sub_action_id);


--
-- Name: FKI_promo_transactions_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_promo_transactions_users" ON promo_transactions USING btree (user_id);


--
-- Name: FKI_table_owner; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_table_owner" ON game_tables USING btree (table_owner);


--
-- Name: FKI_user_balance_adjustments_cashier; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_balance_adjustments_cashier" ON user_balance_adjustments USING btree (adjustment_user);


--
-- Name: FKI_user_changes_old_country; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_changes_old_country" ON user_changes USING btree (old_country);


--
-- Name: FKI_user_changes_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_changes_users" ON user_changes USING btree (user_id);


--
-- Name: FKI_user_changes_users_change_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_changes_users_change_user" ON user_changes USING btree (change_by_user);


--
-- Name: FKI_user_logins_server; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_logins_server" ON user_logins USING btree (server_id);


--
-- Name: FKI_user_logins_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_logins_user" ON user_logins USING btree (user_id);


--
-- Name: FKI_user_transactions_affiliate_earnings_transactions; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_transactions_affiliate_earnings_transactions" ON user_transactions USING btree (affiliate_earnings_transaction_id) WHERE (affiliate_earnings_transaction_id IS NOT NULL);


--
-- Name: FKI_user_transactions_corporate_deposit_transfers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_transactions_corporate_deposit_transfers" ON user_transactions USING btree (deposit_transfer_id) WHERE (deposit_transfer_id IS NOT NULL);


--
-- Name: FKI_user_transactions_game_round_actions; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_transactions_game_round_actions" ON user_transactions USING btree (action_table_id, action_round_id, action_action_id, action_sub_action_id);


--
-- Name: FKI_user_transactions_payout_transactions; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_transactions_payout_transactions" ON user_transactions USING btree (payout_id) WHERE (payout_id IS NOT NULL);


--
-- Name: FKI_user_transactions_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_user_transactions_user" ON user_transactions USING btree (user_id);


--
-- Name: FKI_users_modifier_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "FKI_users_modifier_user" ON users USING btree (modifier_user);


--
-- Name: INDEX_device_hash_code; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INDEX_device_hash_code" ON user_failed_logins USING btree (device_hash_code);


--
-- Name: INDEX_game_rounds_date_closed; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INDEX_game_rounds_date_closed" ON game_rounds USING btree (date_closed);


--
-- Name: INDEX_game_rounds_date_opened; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INDEX_game_rounds_date_opened" ON game_rounds USING btree (date_opened);


--
-- Name: INDEX_user_logins_server_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INDEX_user_logins_server_user" ON user_logins USING btree (user_id, server_id);


--
-- Name: INDEX_user_transactions_transaction_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INDEX_user_transactions_transaction_id" ON user_transactions USING btree (user_id, user_transaction_id);


--
-- Name: INDEX_username; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "INDEX_username" ON users USING btree (username);


--
-- Name: UNIQUE_horses_horse_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "UNIQUE_horses_horse_name" ON horses USING btree (horse_name);


--
-- Name: fki_affiliate_earnings_transactions_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_affiliate_earnings_transactions_users ON affiliate_earnings_transactions USING btree (initiator_user_id);


--
-- Name: fki_affiliate_users_affiliates; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_affiliate_users_affiliates ON affiliate_users USING btree (affiliate_id);


--
-- Name: fki_boot_dog_races; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_boot_dog_races ON boot_dog_races USING btree (dog_id);


--
-- Name: fki_boot_horse_races; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_boot_horse_races ON boot_horse_races USING btree (horse_id);


--
-- Name: fki_game_client_games_client; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_client_games_client ON game_client_games USING btree (client_id);


--
-- Name: fki_game_client_games_game; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_client_games_game ON game_client_games USING btree (game_id);


--
-- Name: fki_game_configs_game_types; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_configs_game_types ON game_configs USING btree (game_id);


--
-- Name: fki_game_configs_user; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_configs_user ON game_configs USING btree (modified_by);


--
-- Name: fki_game_period_amounts_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_period_amounts_users ON game_period_amounts USING btree (user_id);


--
-- Name: fki_game_tables_game_types; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_tables_game_types ON game_tables USING btree (game_type_id);


--
-- Name: fki_game_tables_servers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_tables_servers ON game_tables USING btree (server_id);


--
-- Name: fki_game_types_game_categories; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_game_types_game_categories ON game_types USING btree (game_category_id);


--
-- Name: fki_jpt_transactions_game_rounds; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_jpt_transactions_game_rounds ON jackpot_transactions USING btree (table_id, round_id);


--
-- Name: fki_jpt_transactions_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_jpt_transactions_users ON jackpot_transactions USING btree (user_id);


--
-- Name: fki_player_favourite_games_game_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_player_favourite_games_game_type ON player_favourite_games USING btree (game_id);


--
-- Name: fki_user_comments_user_id_fkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_user_comments_user_id_fkey ON user_comments USING btree (user_id);


--
-- Name: fki_user_roles_roles; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_user_roles_roles ON user_roles USING btree (role_id);


--
-- Name: fki_user_transactions_payment_transactions; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_user_transactions_payment_transactions ON user_transactions USING btree (payment_transaction_id) WHERE (payment_transaction_id IS NOT NULL);


--
-- Name: fki_user_transactions_user_balance_adjustment; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_user_transactions_user_balance_adjustment ON user_transactions USING btree (adjustment_id) WHERE (adjustment_id IS NOT NULL);


--
-- Name: fki_web_logins_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_web_logins_users ON web_logins USING btree (user_id);


--
-- Name: index_active_users; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_active_users ON game_table_users USING btree (table_id) WHERE (is_active = true);


--
-- Name: index_jackpot_transactions_win_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_jackpot_transactions_win_id ON jackpot_transactions USING btree (win_id) WHERE (win_id IS NOT NULL);


--
-- Name: index_promo_transactions_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_promo_transactions_date ON promo_transactions USING btree (transaction_time);


--
-- Name: unique_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_email ON users USING btree (lower((email)::text));


--
-- Name: unique_open_jackpot; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_open_jackpot ON jackpots USING btree (get_unique_integer(game_id), get_unique_bigint(target_corporate)) WHERE (finish_date IS NULL);


--
-- Name: uniquei_nickname; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniquei_nickname ON users USING btree (lower((nickname)::text));


--
-- Name: uniquei_user_logins_user_server; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniquei_user_logins_user_server ON user_logins USING btree (user_id, server_id) WHERE (logout_date IS NULL);


--
-- Name: uniquei_username; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniquei_username ON users USING btree (lower((username)::text));


--
-- Name: TRIGGER_affiliate_earnings_transaction_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_affiliate_earnings_transaction_insert" AFTER INSERT ON affiliate_earnings_transactions FOR EACH ROW EXECUTE PROCEDURE trg_affiliate_earnings_transaction_insert();


--
-- Name: TRIGGER_affiliate_earnings_transactions_before_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_affiliate_earnings_transactions_before_insert" BEFORE INSERT ON affiliate_earnings_transactions FOR EACH ROW EXECUTE PROCEDURE trg_affiliate_earnings_transaction_before_insert();


--
-- Name: TRIGGER_affiliate_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_affiliate_update" BEFORE UPDATE ON affiliates FOR EACH ROW EXECUTE PROCEDURE affiliate_update();


--
-- Name: TRIGGER_before_user_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_before_user_update" BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE trg_before_user_update();


--
-- Name: TRIGGER_deposit_transfer_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_deposit_transfer_insert" AFTER INSERT ON corporate_deposit_transfers FOR EACH ROW EXECUTE PROCEDURE trg_corporate_deposit_transfers_insert();


--
-- Name: TRIGGER_jackpot_config_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_jackpot_config_update" AFTER UPDATE ON jackpot_configs FOR EACH ROW EXECUTE PROCEDURE trg_jackpot_config_update();


--
-- Name: TRIGGER_jackpot_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_jackpot_insert" AFTER INSERT ON jackpots FOR EACH ROW EXECUTE PROCEDURE trg_jackpot_insert();


--
-- Name: TRIGGER_jackpot_transactions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_jackpot_transactions" BEFORE INSERT ON jackpot_transactions FOR EACH ROW EXECUTE PROCEDURE trg_jackpot_transaction();


--
-- Name: TRIGGER_jackpot_transactions_after_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_jackpot_transactions_after_insert" AFTER INSERT ON jackpot_transactions FOR EACH ROW EXECUTE PROCEDURE trg_jackpot_transaction_after_insert();


--
-- Name: TRIGGER_payment_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_payment_insert" AFTER INSERT ON payment_transactions FOR EACH ROW EXECUTE PROCEDURE payment_balance_insert();


--
-- Name: TRIGGER_payment_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_payment_update" AFTER UPDATE ON payment_transactions FOR EACH ROW EXECUTE PROCEDURE trg_payment_balance_update();


--
-- Name: TRIGGER_payout_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_payout_insert" AFTER INSERT ON payout_transactions FOR EACH ROW EXECUTE PROCEDURE trg_payout_insert();


--
-- Name: TRIGGER_payout_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_payout_update" AFTER UPDATE ON payout_transactions FOR EACH ROW EXECUTE PROCEDURE trg_payout_update();


--
-- Name: TRIGGER_promo_transaction_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_promo_transaction_insert" BEFORE INSERT ON promo_transactions FOR EACH ROW EXECUTE PROCEDURE trg_promo_transactions();


--
-- Name: TRIGGER_update_action; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_update_action" AFTER UPDATE ON game_round_actions FOR EACH ROW EXECUTE PROCEDURE trg_game_round_action_update();


--
-- Name: TRIGGER_user_balance_adjustment; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_user_balance_adjustment" AFTER INSERT ON user_balance_adjustments FOR EACH ROW EXECUTE PROCEDURE trg_user_balance_adjustments();


--
-- Name: TRIGGER_user_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_user_insert" AFTER INSERT ON users FOR EACH ROW EXECUTE PROCEDURE trg_user_insert();


--
-- Name: TRIGGER_user_transaction; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_user_transaction" BEFORE INSERT ON user_transactions FOR EACH ROW EXECUTE PROCEDURE trg_user_transaction();


--
-- Name: TRIGGER_user_transaction_after_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER "TRIGGER_user_transaction_after_insert" AFTER INSERT ON user_transactions FOR EACH ROW EXECUTE PROCEDURE trg_user_transaction_after_insert();


--
-- Name: trigger_affiliate_after_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trigger_affiliate_after_insert AFTER INSERT ON affiliates FOR EACH ROW EXECUTE PROCEDURE trg_affiliate_after_insert();


--
-- Name: trigger_game_round_action_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trigger_game_round_action_insert AFTER INSERT ON game_round_actions FOR EACH ROW WHEN ((new.amount IS NOT NULL)) EXECUTE PROCEDURE trg_game_round_action_insert();


--
-- Name: FK_affiliate_changes_affiliates; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_changes
    ADD CONSTRAINT "FK_affiliate_changes_affiliates" FOREIGN KEY (affiliate_id) REFERENCES affiliates(affiliate_id) ON DELETE CASCADE;


--
-- Name: FK_affiliate_changes_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_changes
    ADD CONSTRAINT "FK_affiliate_changes_users" FOREIGN KEY (modifier_user) REFERENCES users(user_id);


--
-- Name: FK_affiliate_earnings_transactions_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_earnings_transactions
    ADD CONSTRAINT "FK_affiliate_earnings_transactions_users" FOREIGN KEY (initiator_user_id) REFERENCES users(user_id);


--
-- Name: FK_affiliate_payments_affiliates; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_earnings_transactions
    ADD CONSTRAINT "FK_affiliate_payments_affiliates" FOREIGN KEY (affiliate_id) REFERENCES affiliates(affiliate_id);


--
-- Name: FK_affiliate_period_amounts_affiliates; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_period_amounts
    ADD CONSTRAINT "FK_affiliate_period_amounts_affiliates" FOREIGN KEY (affiliate_id) REFERENCES affiliates(affiliate_id);


--
-- Name: FK_affiliate_period_amounts_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_period_amounts
    ADD CONSTRAINT "FK_affiliate_period_amounts_users" FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: FK_affiliates_modifier_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliates
    ADD CONSTRAINT "FK_affiliates_modifier_users" FOREIGN KEY (modifier_user) REFERENCES users(user_id);


--
-- Name: FK_corporate_deposit_transfers_transfer_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_deposit_transfers
    ADD CONSTRAINT "FK_corporate_deposit_transfers_transfer_user" FOREIGN KEY (transfer_user) REFERENCES users(user_id);


--
-- Name: FK_corporate_deposit_transfers_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_deposit_transfers
    ADD CONSTRAINT "FK_corporate_deposit_transfers_users" FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: FK_corporate_game_settings_affiliates; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_game_settings
    ADD CONSTRAINT "FK_corporate_game_settings_affiliates" FOREIGN KEY (corporate_id) REFERENCES affiliates(affiliate_id) ON DELETE CASCADE;


--
-- Name: FK_corporates_game_settings_game; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_game_settings
    ADD CONSTRAINT "FK_corporates_game_settings_game" FOREIGN KEY (game_id) REFERENCES game_types(game_type_id) ON DELETE CASCADE;


--
-- Name: FK_country; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "FK_country" FOREIGN KEY (country) REFERENCES countries(country_code) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: FK_game_period_amounts_game_tables; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_period_amounts
    ADD CONSTRAINT "FK_game_period_amounts_game_tables" FOREIGN KEY (table_id) REFERENCES game_tables(table_id);


--
-- Name: FK_game_settings_changes_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_settings_changes
    ADD CONSTRAINT "FK_game_settings_changes_users" FOREIGN KEY (change_user) REFERENCES users(user_id);


--
-- Name: FK_jackpot_balances_jackpot_transactions; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_balances
    ADD CONSTRAINT "FK_jackpot_balances_jackpot_transactions" FOREIGN KEY (last_win_transaction) REFERENCES jackpot_transactions(jackpot_transaction_id) ON DELETE SET NULL;


--
-- Name: FK_jackpot_period_balances_jackpots; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_period_balances
    ADD CONSTRAINT "FK_jackpot_period_balances_jackpots" FOREIGN KEY (jackpot_id) REFERENCES jackpots(jackpot_id) ON DELETE CASCADE;


--
-- Name: FK_jackpot_properties_changes_jackpots; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_properties_changes
    ADD CONSTRAINT "FK_jackpot_properties_changes_jackpots" FOREIGN KEY (jackpot_id) REFERENCES jackpots(jackpot_id) ON DELETE CASCADE;


--
-- Name: FK_jackpot_properties_changes_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_properties_changes
    ADD CONSTRAINT "FK_jackpot_properties_changes_users" FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_jackpot_properties_jackpot_configs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_properties_changes
    ADD CONSTRAINT "FK_jackpot_properties_jackpot_configs" FOREIGN KEY (jackpot_config_id) REFERENCES jackpot_configs(config_id) ON DELETE CASCADE;


--
-- Name: FK_payout_transactions_approve_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payout_transactions
    ADD CONSTRAINT "FK_payout_transactions_approve_users" FOREIGN KEY (approve_user_id) REFERENCES users(user_id);


--
-- Name: FK_promo_transactions_game_actions; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY promo_transactions
    ADD CONSTRAINT "FK_promo_transactions_game_actions" FOREIGN KEY (table_id, round_id, action_id, sub_action_id) REFERENCES game_round_actions(table_id, round_id, action_id, sub_action_id);


--
-- Name: FK_promo_transactions_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY promo_transactions
    ADD CONSTRAINT "FK_promo_transactions_users" FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_roulette_results_rounds; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roulette_results
    ADD CONSTRAINT "FK_roulette_results_rounds" FOREIGN KEY (table_id, round_id) REFERENCES game_rounds(table_id, round_id) ON DELETE CASCADE;


--
-- Name: FK_system_settings_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY system_settings_changes
    ADD CONSTRAINT "FK_system_settings_users" FOREIGN KEY (change_user) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_table_owner; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_tables
    ADD CONSTRAINT "FK_table_owner" FOREIGN KEY (table_owner) REFERENCES users(user_id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: FK_user_balance_adjustments_cashier; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_balance_adjustments
    ADD CONSTRAINT "FK_user_balance_adjustments_cashier" FOREIGN KEY (adjustment_user) REFERENCES users(user_id);


--
-- Name: FK_user_balance_adjustments_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_balance_adjustments
    ADD CONSTRAINT "FK_user_balance_adjustments_user" FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: FK_user_balance_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_balance
    ADD CONSTRAINT "FK_user_balance_users" FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_user_changes_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_changes
    ADD CONSTRAINT "FK_user_changes_users" FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_user_changes_users_change_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_changes
    ADD CONSTRAINT "FK_user_changes_users_change_user" FOREIGN KEY (change_by_user) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_user_devices_approved_by_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_devices
    ADD CONSTRAINT "FK_user_devices_approved_by_users" FOREIGN KEY (approved_by_user) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_user_devices_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_devices
    ADD CONSTRAINT "FK_user_devices_users" FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_user_failed_logins_servers; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_failed_logins
    ADD CONSTRAINT "FK_user_failed_logins_servers" FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE;


--
-- Name: FK_user_failed_logins_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_failed_logins
    ADD CONSTRAINT "FK_user_failed_logins_users" FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: FK_user_logins_server; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_logins
    ADD CONSTRAINT "FK_user_logins_server" FOREIGN KEY (server_id) REFERENCES servers(server_id);


--
-- Name: FK_user_logins_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_logins
    ADD CONSTRAINT "FK_user_logins_user" FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: FK_user_transactions_affiliate_earnings_transactions; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "FK_user_transactions_affiliate_earnings_transactions" FOREIGN KEY (affiliate_earnings_transaction_id) REFERENCES affiliate_earnings_transactions(transaction_id);


--
-- Name: FK_user_transactions_corporate_deposit_transfers; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "FK_user_transactions_corporate_deposit_transfers" FOREIGN KEY (deposit_transfer_id) REFERENCES corporate_deposit_transfers(transfer_id);


--
-- Name: FK_user_transactions_game_round_actions; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "FK_user_transactions_game_round_actions" FOREIGN KEY (action_table_id, action_round_id, action_action_id, action_sub_action_id) REFERENCES game_round_actions(table_id, round_id, action_id, sub_action_id);


--
-- Name: FK_user_transactions_payout_transactions; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "FK_user_transactions_payout_transactions" FOREIGN KEY (payout_id) REFERENCES payout_transactions(payout_id);


--
-- Name: FK_user_transactions_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "FK_user_transactions_user" FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: FK_user_transactions_user_balance_adjustment; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT "FK_user_transactions_user_balance_adjustment" FOREIGN KEY (user_id, adjustment_id) REFERENCES user_balance_adjustments(user_id, adjustment_id);


--
-- Name: FK_users_blacklist_countries; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_blacklist
    ADD CONSTRAINT "FK_users_blacklist_countries" FOREIGN KEY (country) REFERENCES countries(country_code);


--
-- Name: FK_users_modifier_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "FK_users_modifier_user" FOREIGN KEY (modifier_user) REFERENCES users(user_id) ON DELETE SET NULL;


--
-- Name: fk_affiliate_earnings_affiliates; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_earnings
    ADD CONSTRAINT fk_affiliate_earnings_affiliates FOREIGN KEY (affiliate_id) REFERENCES affiliates(affiliate_id) ON DELETE CASCADE;


--
-- Name: fk_affiliate_users_affiliates; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_users
    ADD CONSTRAINT fk_affiliate_users_affiliates FOREIGN KEY (affiliate_id) REFERENCES affiliates(affiliate_id);


--
-- Name: fk_affiliate_users_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliate_users
    ADD CONSTRAINT fk_affiliate_users_users FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: fk_affiliates_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY affiliates
    ADD CONSTRAINT fk_affiliates_users FOREIGN KEY (affiliate_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: fk_boot_dog_races; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY boot_dog_races
    ADD CONSTRAINT fk_boot_dog_races FOREIGN KEY (dog_id) REFERENCES dogs(dog_id) ON DELETE CASCADE;


--
-- Name: fk_boot_dog_races_game_tables; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY boot_dog_races
    ADD CONSTRAINT fk_boot_dog_races_game_tables FOREIGN KEY (table_id) REFERENCES game_tables(table_id) ON DELETE CASCADE;


--
-- Name: fk_boot_horse_races; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY boot_horse_races
    ADD CONSTRAINT fk_boot_horse_races FOREIGN KEY (horse_id) REFERENCES horses(horse_id) ON DELETE CASCADE;


--
-- Name: fk_boot_horse_races_game_tables; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY boot_horse_races
    ADD CONSTRAINT fk_boot_horse_races_game_tables FOREIGN KEY (table_id) REFERENCES game_tables(table_id) ON DELETE CASCADE;


--
-- Name: fk_corporate_system_settings_corporate; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_system_settings
    ADD CONSTRAINT fk_corporate_system_settings_corporate FOREIGN KEY (corporate_id) REFERENCES affiliates(affiliate_id) ON DELETE CASCADE;


--
-- Name: fk_game_client_games_client; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_client_games
    ADD CONSTRAINT fk_game_client_games_client FOREIGN KEY (client_id) REFERENCES game_clients(client_id) ON DELETE CASCADE;


--
-- Name: fk_game_client_games_game; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_client_games
    ADD CONSTRAINT fk_game_client_games_game FOREIGN KEY (game_id) REFERENCES game_types(game_type_id) ON DELETE CASCADE;


--
-- Name: fk_game_configs_game_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_configs
    ADD CONSTRAINT fk_game_configs_game_types FOREIGN KEY (game_id) REFERENCES game_types(game_type_id);


--
-- Name: fk_game_configs_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_configs
    ADD CONSTRAINT fk_game_configs_user FOREIGN KEY (modified_by) REFERENCES users(user_id) ON DELETE SET NULL;


--
-- Name: fk_game_period_amounts_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_period_amounts
    ADD CONSTRAINT fk_game_period_amounts_users FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: fk_game_round_actions_round; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_round_actions
    ADD CONSTRAINT fk_game_round_actions_round FOREIGN KEY (table_id, round_id) REFERENCES game_rounds(table_id, round_id) ON DELETE CASCADE;


--
-- Name: fk_game_round_actions_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_round_actions
    ADD CONSTRAINT fk_game_round_actions_user FOREIGN KEY (table_id, user_id) REFERENCES game_table_users(table_id, user_id);


--
-- Name: fk_game_rounds_game_configs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_rounds
    ADD CONSTRAINT fk_game_rounds_game_configs FOREIGN KEY (game_config_id) REFERENCES game_configs(config_id);


--
-- Name: fk_game_rounds_game_tables; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_rounds
    ADD CONSTRAINT fk_game_rounds_game_tables FOREIGN KEY (table_id) REFERENCES game_tables(table_id);


--
-- Name: fk_game_settings_game_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_settings
    ADD CONSTRAINT fk_game_settings_game_types FOREIGN KEY (game_id) REFERENCES game_types(game_type_id) ON DELETE CASCADE;


--
-- Name: fk_game_table_last_rounds_game_tables; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_table_last_rounds
    ADD CONSTRAINT fk_game_table_last_rounds_game_tables FOREIGN KEY (table_id) REFERENCES game_tables(table_id) ON DELETE CASCADE;


--
-- Name: fk_game_table_users_table; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_table_users
    ADD CONSTRAINT fk_game_table_users_table FOREIGN KEY (table_id) REFERENCES game_tables(table_id);


--
-- Name: fk_game_table_users_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_table_users
    ADD CONSTRAINT fk_game_table_users_user FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: fk_game_tables_game_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_tables
    ADD CONSTRAINT fk_game_tables_game_types FOREIGN KEY (game_type_id) REFERENCES game_types(game_type_id);


--
-- Name: fk_game_tables_servers; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_tables
    ADD CONSTRAINT fk_game_tables_servers FOREIGN KEY (server_id) REFERENCES servers(server_id);


--
-- Name: fk_jackpot_balances_jackpots; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_balances
    ADD CONSTRAINT fk_jackpot_balances_jackpots FOREIGN KEY (jackpot_id) REFERENCES jackpots(jackpot_id);


--
-- Name: fk_jackpot_transactions_jackpots; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_transactions
    ADD CONSTRAINT fk_jackpot_transactions_jackpots FOREIGN KEY (jackpot_id) REFERENCES jackpots(jackpot_id);


--
-- Name: fk_jackpots_game_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpots
    ADD CONSTRAINT fk_jackpots_game_types FOREIGN KEY (game_id) REFERENCES game_types(game_type_id);


--
-- Name: fk_jackpots_jackpot_configs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpots
    ADD CONSTRAINT fk_jackpots_jackpot_configs FOREIGN KEY (config_id) REFERENCES jackpot_configs(config_id);


--
-- Name: fk_jackpots_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpots
    ADD CONSTRAINT fk_jackpots_users FOREIGN KEY (target_corporate) REFERENCES users(user_id);


--
-- Name: fk_jpt_transactions_game_rounds; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_transactions
    ADD CONSTRAINT fk_jpt_transactions_game_rounds FOREIGN KEY (table_id, round_id) REFERENCES game_rounds(table_id, round_id);


--
-- Name: fk_jpt_transactions_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY jackpot_transactions
    ADD CONSTRAINT fk_jpt_transactions_users FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: fk_payout_transactions_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payout_transactions
    ADD CONSTRAINT fk_payout_transactions_users FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: fk_report_generation_logs_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY report_generation_logs
    ADD CONSTRAINT fk_report_generation_logs_users FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: fk_role_privileges_roles; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_privileges
    ADD CONSTRAINT fk_role_privileges_roles FOREIGN KEY (role_id) REFERENCES roles(role_id) ON DELETE CASCADE;


--
-- Name: fk_transactions_approve_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_transactions
    ADD CONSTRAINT fk_transactions_approve_user_id FOREIGN KEY (approve_user_id) REFERENCES users(user_id);


--
-- Name: fk_transactions_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_transactions
    ADD CONSTRAINT fk_transactions_users FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: fk_user_roles_roles; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT fk_user_roles_roles FOREIGN KEY (role_id) REFERENCES roles(role_id) ON DELETE CASCADE;


--
-- Name: fk_user_roles_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT fk_user_roles_users FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: fk_user_transactions_payment_transactions; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_transactions
    ADD CONSTRAINT fk_user_transactions_payment_transactions FOREIGN KEY (payment_transaction_id) REFERENCES payment_transactions(transaction_id);


--
-- Name: fk_web_logins_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY web_logins
    ADD CONSTRAINT fk_web_logins_users FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: game_current_configs_config_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_current_configs
    ADD CONSTRAINT game_current_configs_config_id_fkey FOREIGN KEY (config_id) REFERENCES game_configs(config_id);


--
-- Name: game_current_configs_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY game_current_configs
    ADD CONSTRAINT game_current_configs_game_id_fkey FOREIGN KEY (game_id) REFERENCES game_types(game_type_id) ON DELETE CASCADE;


--
-- Name: player_favourite_games_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY player_favourite_games
    ADD CONSTRAINT player_favourite_games_game_id_fkey FOREIGN KEY (game_id) REFERENCES game_types(game_type_id) ON DELETE CASCADE;


--
-- Name: player_favourite_games_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY player_favourite_games
    ADD CONSTRAINT player_favourite_games_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- Name: user_comments_commenter_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_comments
    ADD CONSTRAINT user_comments_commenter_id_fkey FOREIGN KEY (commenter_id) REFERENCES users(user_id) ON DELETE SET NULL;


--
-- Name: user_comments_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_comments
    ADD CONSTRAINT user_comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

