<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Corporate Turnover" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" isSummaryNewPage="true">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="title" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Corporate Turnover Statement"]]></defaultValueExpression>
	</parameter>
	<parameter name="min_date" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="max_date" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="corp_id" class="java.lang.Long">
		<defaultValueExpression><![CDATA[new Long(-1)]]></defaultValueExpression>
	</parameter>
	<parameter name="show_subs" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[new Boolean(false)]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT supers.affiliate_id, users.username, COALESCE(SUM(amount),0.0) AS earnings, COALESCE(SUM(jackpot_contribs),0) AS jackpot_amount,
(SELECT COALESCE(SUM(amount),0.0)
 FROM corporate_deposit_transfers
 INNER JOIN users AS player ON corporate_deposit_transfers.user_id=player.user_id
 WHERE corporate_deposit_transfers.transfer_user
 IN ((SELECT affiliates.affiliate_id) UNION ALL (SELECT * FROM find_sub_corps(affiliates.affiliate_id) WHERE NOT $P{show_subs}))
 AND player.user_type = 5
 AND ($P{min_date}::timestamp IS NULL OR ((transfer_date AT TIME ZONE 'UTC') >= $P{min_date}::timestamp))
 AND ($P{max_date}::timestamp IS NULL OR ((transfer_date AT TIME ZONE 'UTC') <= $P{max_date}::timestamp))
 AND amount > 0) AS to_transfers,
(SELECT COALESCE(SUM(amount * -1.0),0.0)
 FROM corporate_deposit_transfers
 INNER JOIN users AS player ON corporate_deposit_transfers.user_id=player.user_id
 WHERE corporate_deposit_transfers.transfer_user
 IN ((SELECT affiliates.affiliate_id) UNION ALL (SELECT * FROM find_sub_corps(affiliates.affiliate_id) WHERE NOT $P{show_subs}))
 AND player.user_type = 5
 AND ($P{min_date}::timestamp IS NULL OR ((transfer_date AT TIME ZONE 'UTC') >= $P{min_date}::timestamp))
 AND ($P{max_date}::timestamp IS NULL OR ((transfer_date AT TIME ZONE 'UTC') <= $P{max_date}::timestamp))
 AND amount < 0) AS from_transfers

FROM affiliates
INNER JOIN users ON users.user_id = affiliates.affiliate_id
LEFT OUTER JOIN affiliate_period_amounts ON affiliate_period_amounts.affiliate_id
 IN ((SELECT affiliates.affiliate_id) UNION ALL (SELECT * FROM find_sub_corps(affiliates.affiliate_id) WHERE NOT $P{show_subs}))
 AND ($P{max_date}::timestamp IS NULL OR ((period_date AT TIME ZONE 'UTC') < date_trunc('day',$P{max_date}::timestamp)))
 AND ($P{min_date}::timestamp IS NULL OR ((period_date AT TIME ZONE 'UTC') >= date_trunc('day',$P{min_date}::timestamp)))
LEFT OUTER JOIN affiliate_users AS supers ON supers.user_id = affiliates.affiliate_id
WHERE
(supers.affiliate_id IS NULL AND ($P{corp_id}::bigint <= 0 OR affiliates.affiliate_id = $P{corp_id}))
     OR ($P{show_subs} AND $P{corp_id} <= 0)
     OR ($P{show_subs} AND affiliates.affiliate_id IN ((SELECT $P{corp_id}) UNION ALL (SELECT * FROM find_sub_corps($P{corp_id})))
)

GROUP BY supers.affiliate_id, users.username, affiliates.affiliate_id

ORDER BY COALESCE(supers.affiliate_id, 0), users.username]]>
	</queryString>
	<field name="affiliate_id" class="java.lang.Long"/>
	<field name="username" class="java.lang.String"/>
	<field name="earnings" class="java.math.BigDecimal"/>
	<field name="jackpot_amount" class="java.math.BigDecimal"/>
	<field name="to_transfers" class="java.math.BigDecimal"/>
	<field name="from_transfers" class="java.math.BigDecimal"/>
	<variable name="earnings_sum" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[new Double($F{earnings}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="jackpot_contribs_sum" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[new Double($F{jackpot_amount}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="transfers_from_sum" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[new Double($F{from_transfers}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="transfers_to_sum" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[new Double($F{to_transfers}.doubleValue())]]></variableExpression>
	</variable>
	<group name="main_group">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="27" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-23" x="1" y="2" width="79" height="18"/>
					<textElement>
						<font fontName="SansSerif" size="11" isBold="true"/>
					</textElement>
					<text><![CDATA[Totals]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-43" x="80" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{transfers_to_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-44" x="145" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{transfers_from_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-45" x="210" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{transfers_to_sum}.doubleValue()-$V{transfers_from_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-46" x="275" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{earnings_sum}.doubleValue()+$V{jackpot_contribs_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-5" x="1" y="22" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				</line>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-47" x="340" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{jackpot_contribs_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-48" x="405" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{earnings_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-50" x="470" y="2" width="65" height="18"/>
					<textElement textAlignment="Center">
						<font fontName="SansSerif" size="11" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{transfers_to_sum}.doubleValue()-$V{transfers_from_sum}.doubleValue()-$V{earnings_sum}.doubleValue())]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="47" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" x="0" y="6" width="535" height="22"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{title}+" "+($P{min_date} != null && $P{max_date} != null?
	" from "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{min_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})+" to "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{max_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})
	: ($P{min_date} != null?
		" from "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{min_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})
		: ($P{max_date} != null?
			" until "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{max_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})
			: "")))]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="30" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
			</line>
			<line>
				<reportElement key="line-2" x="0" y="3" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
			</line>
		</band>
	</title>
	<pageHeader>
		<band height="47" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-16" mode="Opaque" x="80" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<text><![CDATA[Transfers to Users]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-17" mode="Opaque" x="145" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<text><![CDATA[Transfers from Users]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-18" mode="Opaque" x="210" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[Net Transfers]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-19" mode="Opaque" x="275" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<text><![CDATA[Earnings]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-20" mode="Opaque" x="340" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<text><![CDATA[Jackpot Contributions]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" mode="Opaque" x="405" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[Net Earnings]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" mode="Opaque" x="1" y="9" width="79" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<text><![CDATA[Corporate]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" mode="Opaque" x="470" y="9" width="65" height="35" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[Difference]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="28" splitType="Stretch">
			<printWhenExpression><![CDATA[new Boolean($F{earnings}.doubleValue() > 0.0 || $F{jackpot_amount}.doubleValue() > 0.0 || $F{to_transfers}.doubleValue() > 0.0 || $F{from_transfers}.doubleValue() > 0.0)]]></printWhenExpression>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-36" x="80" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="styled">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{affiliate_id} == null? "<style isBold='true'>" : "")+
com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{to_transfers}.doubleValue())+
($F{affiliate_id} == null? "</style>" : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-37" x="145" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="styled">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{affiliate_id} == null? "<style isBold='true'>" : "")+
com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{from_transfers}.doubleValue())+
($F{affiliate_id} == null? "</style>" : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-38" x="210" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{to_transfers}.doubleValue()-$F{from_transfers}.doubleValue())]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-39" x="275" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="styled">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{affiliate_id} == null? "<style isBold='true'>" : "")+
com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{earnings}.doubleValue()+$F{jackpot_amount}.doubleValue())+
($F{affiliate_id} == null? "</style>" : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-40" x="340" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="styled">
					<font fontName="SansSerif" size="11"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{affiliate_id} == null? "<style isBold='true'>" : "")+
com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{jackpot_amount}.doubleValue())+
($F{affiliate_id} == null? "</style>" : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-41" x="405" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{earnings}.doubleValue())]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-4" x="1" y="26" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-42" x="1" y="0" width="79" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="styled">
					<font fontName="SansSerif" size="11" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{affiliate_id} == null? "<style isBold='true'>" : "")+
$F{username}+
($F{affiliate_id} == null? "</style>" : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-49" x="470" y="0" width="65" height="23" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{to_transfers}.doubleValue()-$F{from_transfers}.doubleValue()-$F{earnings}.doubleValue())]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="29" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="321" y="3" width="170" height="19" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" mode="Transparent" x="494" y="3" width="36" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-3" x="0" y="3" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-4" x="1" y="4" width="209" height="19" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedDate(new Date(), $P{REPORT_TIME_ZONE}, $P{REPORT_LOCALE})]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
	<noData>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-11" x="8" y="4" width="518" height="17"/>
				<textElement textAlignment="Center">
					<font fontName="SansSerif"/>
				</textElement>
				<text><![CDATA[Report is empty. There are no data to display.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
