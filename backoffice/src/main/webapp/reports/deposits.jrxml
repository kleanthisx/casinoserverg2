<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Deposits" pageWidth="820" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="760" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="UsersReportStyle" isDefault="true" fontName="Arial" fontSize="12" pdfFontName="Helvetica" pdfEncoding="Cp1250"/>
	<parameter name="username" class="java.lang.String">
		<defaultValueExpression><![CDATA[$F{username1}]]></defaultValueExpression>
	</parameter>
	<parameter name="min_date" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="max_date" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="title" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Deposits "]]></defaultValueExpression>
	</parameter>
	<parameter name="show_corporate" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[new Boolean(false)]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select transaction_id, users1.username as username1, amount, ps_specific AS ps_name, users2.username as username2, comment, time_completed,
ps_user_account AS user_account, ps_reference_no AS account_no
FROM payment_transactions
INNER JOIN users as users1 ON users1.user_id = payment_transactions.user_id
LEFT JOIN users as users2 ON users2.user_id = payment_transactions.approve_user_id
WHERE ($P{show_corporate} = true OR (users1.user_type <> 5 AND users1.user_type <>4))
AND (($P{username} IS NULL) OR ($P{username} = '') OR (users1.username = $P{username}))
AND (((payment_transactions.time_completed AT TIME ZONE 'UTC')> ($P{min_date}::timestamp with time zone))
     OR ($P{min_date}::timestamp with time zone IS NULL))
AND (((payment_transactions.time_completed AT TIME ZONE 'UTC')< ($P{max_date}::timestamp with time zone))
      OR ($P{max_date}::timestamp with time zone IS NULL))
ORDER BY time_completed DESC]]>
	</queryString>
	<field name="transaction_id" class="java.lang.Long"/>
	<field name="username1" class="java.lang.String"/>
	<field name="amount" class="java.math.BigDecimal"/>
	<field name="ps_name" class="java.lang.String"/>
	<field name="time_completed" class="java.sql.Timestamp"/>
	<field name="username2" class="java.lang.String"/>
	<field name="comment" class="java.lang.String"/>
	<field name="user_account" class="java.lang.String"/>
	<field name="account_no" class="java.lang.String"/>
	<variable name="amount_sum" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[( $F{voided}.equals( "yes" ) ? new Double(0) : new Double($F{amount}.doubleValue()))]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="31" splitType="Stretch">
			<line>
				<reportElement key="line" x="0" y="3" width="757" height="1"/>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-12" x="0" y="5" width="757" height="21"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($P{title} + ((($P{min_date} != null) || ($P{max_date} != null)) ? " for period" : "")+
($P{min_date} != null? " from "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{min_date}, $P{REPORT_TIME_ZONE},$P{REPORT_LOCALE}) : "") +
($P{max_date} != null? " until "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{max_date}, $P{REPORT_TIME_ZONE},$P{REPORT_LOCALE}) : ""))]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="26" width="757" height="1"/>
			</line>
		</band>
	</title>
	<pageHeader>
		<band height="5" splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement key="staticText" mode="Opaque" x="372" y="0" width="163" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Details]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" mode="Opaque" x="157" y="0" width="75" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Username]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" mode="Opaque" x="669" y="0" width="88" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Amount]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-18" mode="Opaque" x="67" y="0" width="90" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-20" mode="Opaque" x="261" y="0" width="73" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Approved by]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" mode="Opaque" x="535" y="0" width="75" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Method]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-18" mode="Opaque" x="3" y="0" width="64" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Ref No.]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="27" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-1" x="67" y="5" width="90" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isStrikeThrough="false"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedDate($F{time_received}, $P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-6" x="157" y="5" width="75" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{username1}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-14" x="669" y="5" width="88" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="12" isStrikeThrough="false"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{amount}.doubleValue())]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-16" x="372" y="5" width="163" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isStrikeThrough="false"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{user_account} != null? "Name: "+$F{user_account}: "")
    + ($F{account_no} != null? ($F{user_account} != null? "\n": "") + "No: " +  $F{account_no}  : "")
    + ($F{comment} != null? ($F{account_no} != null || ($F{user_account} != null && $F{account_no} == null)? "\n" : "") + $F{comment} : "")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-17" x="232" y="5" width="29" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{voided}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-18" x="261" y="5" width="73" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{username2}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-20" x="535" y="5" width="75" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="12" isStrikeThrough="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ps_name}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-2" positionType="FixRelativeToBottom" x="0" y="24" width="757" height="1"/>
			</line>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-1" x="3" y="5" width="64" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isStrikeThrough="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transaction_id}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="36" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="553" y="6" width="170" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="723" y="6" width="36" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left"/>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="3" y="6" width="209" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedDate(new Date(), $P{REPORT_TIME_ZONE}, $P{REPORT_LOCALE})]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="21" splitType="Stretch">
			<textField evaluationTime="Report" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-19" x="670" y="0" width="88" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="12" isBold="true" isStrikeThrough="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{amount_sum}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-24" mode="Opaque" x="3" y="0" width="608" height="17" forecolor="#FFFFFF" backcolor="#999999"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" isItalic="false"/>
				</textElement>
				<text><![CDATA[Totals]]></text>
			</staticText>
		</band>
	</summary>
	<noData>
		<band height="22" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-13" x="8" y="5" width="749" height="17"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Report is empty. There are no data to display.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
