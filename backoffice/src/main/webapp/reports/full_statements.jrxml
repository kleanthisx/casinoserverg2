<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="full_statements_report" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="full_statements_style" isDefault="true" fontName="Arial" fontSize="12" isPdfEmbedded="false"/>
	<parameter name="country_val" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="username" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="min_date" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="max_date" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="title" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Full User Statements"]]></defaultValueExpression>
	</parameter>
	<parameter name="table_id" class="java.lang.Long">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="game_type_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="show_inactive" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[new Boolean(false)]]></defaultValueExpression>
	</parameter>
	<parameter name="show_corporate" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[new Boolean(false)]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[(( SELECT 1 As fake_order, users.user_id, users.username, users.first_name, users.last_name, countries.country_name AS user_country,
(user_transactions.transaction_time AT TIME ZONE 'UTC') AS transaction_time,
(CASE WHEN user_transactions.amount < 0 THEN 0 ELSE user_transactions.amount END) AS credit,
(CASE WHEN user_transactions.amount < 0 THEN user_transactions.amount ELSE 0 END) AS debit,
user_balance_adjustments.adjustment_id, user_balance_adjustments.adjustment_description,
payment_transactions.transaction_id, payout_transactions.payout_id, payment_transactions.ps_specific AS payin_method,
payout_transactions.ps_specific AS payout_method, user_transactions.action_table_id,user_transactions.action_round_id,
game_tables.game_type_id, deposit_transfer_id, affiliate_earnings_transaction_id,
in_users.username AS transfer_in_user, out_users.username AS transfer_out_user, user_transactions.details

FROM user_transactions
INNER JOIN users on user_transactions.user_id = users.user_id
LEFT OUTER JOIN countries ON users.country = countries.country_code
LEFT OUTER JOIN user_balance_adjustments ON user_transactions.user_id = user_balance_adjustments.user_id
AND user_transactions.adjustment_id = user_balance_adjustments.adjustment_id

LEFT OUTER JOIN payment_transactions ON user_transactions.payment_transaction_id = payment_transactions.transaction_id
LEFT OUTER JOIN payout_transactions ON user_transactions.payout_id = payout_transactions.payout_id
LEFT OUTER JOIN game_tables ON user_transactions.action_table_id = game_tables.table_id
LEFT OUTER JOIN corporate_deposit_transfers AS out_transfers ON user_transactions.deposit_transfer_id = out_transfers.transfer_id
AND out_transfers.transfer_user = user_transactions.user_id
LEFT OUTER JOIN users AS out_users ON out_transfers.user_id = out_users.user_id
LEFT OUTER JOIN corporate_deposit_transfers AS in_transfers ON user_transactions.deposit_transfer_id = in_transfers.transfer_id
AND in_transfers.user_id = user_transactions.user_id
LEFT OUTER JOIN users AS in_users ON in_transfers.transfer_user = in_users.user_id

WHERE (($P{country_val} IS NULL) OR ($P{country_val} = '') OR (users.country = $P{country_val}))
AND ($P{show_corporate} = true OR (users.user_type <> 5 AND users.user_type <>4))
AND (($P{username} IS NULL) OR ($P{username} = '') OR (users.username = $P{username}))
AND (($P{max_date}::timestamp IS NULL) OR ((user_transactions.transaction_time AT TIME ZONE 'UTC') <= $P{max_date}::timestamp))
AND (($P{min_date}::timestamp IS NULL) OR ((user_transactions.transaction_time AT TIME ZONE 'UTC') >= $P{min_date}::timestamp))
AND (($P{game_type_id} IS NULL) OR ($P{game_type_id} <= 0) OR (game_tables.game_type_id = $P{game_type_id}))
AND (($P{table_id} IS NULL) OR ($P{table_id} <= 0) OR (user_transactions.action_table_id IS NULL) OR (user_transactions.action_table_id = $P{table_id}))
)

UNION ALL

( SELECT 0 AS fake_order, users.user_id, users.username, users.first_name, users.last_name, countries.country_name as user_country, NULL,
SUM(CASE WHEN user_transactions.amount < 0 THEN 0 ELSE user_transactions.amount END) AS credit,
SUM(CASE WHEN user_transactions.amount < 0 THEN user_transactions.amount ELSE 0 END) AS debit,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
FROM user_transactions
INNER JOIN users on user_transactions.user_id = users.user_id
LEFT OUTER JOIN game_tables ON user_transactions.action_table_id = game_tables.table_id
LEFT OUTER JOIN countries ON users.country = countries.country_code
WHERE (($P{country_val} IS NULL) OR ($P{country_val} = '') OR (users.country = $P{country_val}))
AND ($P{show_corporate} = true OR (users.user_type <> 5 AND users.user_type <>4))
AND (($P{username} IS NULL) OR ($P{username} = '') OR (users.username = $P{username}))
AND (($P{min_date}::timestamp IS NOT NULL) AND ((user_transactions.transaction_time AT TIME ZONE 'UTC') < $P{min_date}::timestamp))
AND (($P{game_type_id} IS NULL) OR ($P{game_type_id} <= 0) OR (game_tables.game_type_id = $P{game_type_id}))
AND (($P{table_id} IS NULL) OR ($P{table_id} <= 0) OR (user_transactions.action_table_id IS NULL) OR (user_transactions.action_table_id = $P{table_id}))
AND ($P{show_inactive} OR ((
	SELECT COUNT(transaction_time)
	FROM user_transactions
	WHERE user_id = users.user_id
	AND user_transactions.transaction_time AT TIME ZONE 'UTC' >= $P{min_date}::timestamp
        AND ($P{max_date}::timestamp IS NULL OR (user_transactions.transaction_time AT TIME ZONE 'UTC') <= $P{max_date}::timestamp)
	LIMIT 1) > 0))
GROUP BY users.user_id, users.username, users.first_name, users.last_name, user_country ))

ORDER BY user_id, fake_order, transaction_time]]>
	</queryString>
	<field name="fake_order" class="java.lang.Integer"/>
	<field name="user_id" class="java.lang.Long"/>
	<field name="username" class="java.lang.String"/>
	<field name="first_name" class="java.lang.String"/>
	<field name="last_name" class="java.lang.String"/>
	<field name="user_country" class="java.lang.String"/>
	<field name="transaction_time" class="java.sql.Timestamp"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="adjustment_id" class="java.lang.Long"/>
	<field name="adjustment_description" class="java.lang.String"/>
	<field name="transaction_id" class="java.lang.Long"/>
	<field name="payout_id" class="java.lang.Long"/>
	<field name="payin_method" class="java.lang.String"/>
	<field name="payout_method" class="java.lang.String"/>
	<field name="action_table_id" class="java.lang.Long"/>
	<field name="action_round_id" class="java.lang.Integer"/>
	<field name="game_type_id" class="java.lang.Integer"/>
	<field name="deposit_transfer_id" class="java.lang.Long"/>
	<field name="affiliate_earnings_transaction_id" class="java.lang.Long"/>
	<field name="transfer_in_user" class="java.lang.String"/>
	<field name="transfer_out_user" class="java.lang.String"/>
	<field name="details" class="java.lang.String"/>
	<variable name="amount_sum" class="java.lang.Double" resetType="Group" resetGroup="userInfo" calculation="Sum">
		<variableExpression><![CDATA[new Double($F{credit}.doubleValue()+$F{debit}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="credit_sum" class="java.lang.Double" resetType="Group" resetGroup="userInfo" calculation="Sum">
		<variableExpression><![CDATA[new Double(Math.abs($F{credit}.doubleValue()))]]></variableExpression>
	</variable>
	<variable name="debit_sum" class="java.lang.Double" resetType="Group" resetGroup="userInfo" calculation="Sum">
		<variableExpression><![CDATA[new Double(Math.abs($F{debit}.doubleValue()))]]></variableExpression>
	</variable>
	<group name="userInfo" isStartNewPage="true">
		<groupExpression><![CDATA[$F{user_id}]]></groupExpression>
		<groupHeader>
			<band height="157" splitType="Stretch">
				<line>
					<reportElement key="line-1" x="0" y="2" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				</line>
				<line>
					<reportElement key="line-2" x="0" y="29" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				</line>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-1" x="0" y="5" width="535" height="22"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="14"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{title}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-2" x="72" y="33" width="271" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{username}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-1" mode="Opaque" x="1" y="140" width="106" height="17" printWhenGroupChanges="userInfo" forecolor="#FFFFFF" backcolor="#999999"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-2" mode="Opaque" x="360" y="140" width="87" height="17" printWhenGroupChanges="userInfo" forecolor="#FFFFFF" backcolor="#999999"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Debit]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-3" mode="Opaque" x="273" y="140" width="87" height="17" printWhenGroupChanges="userInfo" forecolor="#FFFFFF" backcolor="#999999"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Credit]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-4" mode="Opaque" x="107" y="140" width="166" height="17" printWhenGroupChanges="userInfo" forecolor="#FFFFFF" backcolor="#999999"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Description]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-5" x="2" y="33" width="69" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Username: ]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-6" x="2" y="54" width="69" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[First Name:]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-7" x="2" y="75" width="69" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Last Name:]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-3" x="72" y="54" width="271" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{first_name}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-4" x="72" y="75" width="271" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{last_name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-8" mode="Opaque" x="447" y="140" width="87" height="17" printWhenGroupChanges="userInfo" forecolor="#FFFFFF" backcolor="#999999"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Balance]]></text>
				</staticText>
				<line>
					<reportElement key="line-3" x="0" y="29" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				</line>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-5" x="1" y="119" width="532" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{min_date} != null && $P{max_date} != null?
	"Period from "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{min_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})+" to "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{max_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})
	: ($P{min_date} != null?
		"Period from "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{min_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})
		: ($P{max_date} != null?
			"Period until "+com.magneta.casino.reports.ReportUtil.getFormattedDate($P{max_date},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})
			: "")))]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-9" x="2" y="95" width="69" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[Country:]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-6" x="72" y="95" width="271" height="19"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{user_country}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="23" splitType="Stretch">
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-12" x="360" y="2" width="87" height="17" forecolor="#000000"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{debit_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-13" x="273" y="2" width="87" height="17" isRemoveLineWhenBlank="true" forecolor="#000000"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{credit_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-14" x="447" y="2" width="87" height="17" forecolor="#000000"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{amount_sum}.doubleValue())]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-4" x="0" y="21" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				</line>
				<staticText>
					<reportElement key="staticText-10" x="139" y="2" width="134" height="17" forecolor="#000000"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Total]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="23" splitType="Stretch">
			<printWhenExpression><![CDATA[new Boolean((($P{table_id} == null) || ($P{table_id}.intValue() == -1) || ($F{action_table_id} == null) || ($F{action_table_id}.longValue() == $P{table_id}.longValue()))
	&& (($P{game_type_id} == null) || ($P{game_type_id}.intValue() == -1) || ($F{game_type_id} == null) || ($F{game_type_id}.intValue() == $P{game_type_id}.intValue())))]]></printWhenExpression>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-7" x="1" y="2" width="106" height="17" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedDate($F{transaction_time},$P{REPORT_TIME_ZONE},$P{REPORT_LOCALE})

]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-8" x="360" y="2" width="87" height="17" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[((Math.abs($F{debit}.doubleValue()) == 0) ? "" : com.magneta.casino.reports.ReportUtil.getFormattedAmount(Math.abs($F{debit}.doubleValue())))]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-9" x="273" y="2" width="87" height="17" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{credit}.doubleValue() == 0 ? "" : com.magneta.casino.reports.ReportUtil.getFormattedAmount($F{credit}.doubleValue()))]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-10" x="107" y="2" width="166" height="17" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{adjustment_id} != null ? ($F{adjustment_description} == null ? "Adjustment" : $F{adjustment_description})
	: ($F{transaction_id} != null? $F{payin_method}
		: ($F{payout_id} != null ? $F{payout_method}
			: ($F{action_table_id} != null? new String("Game "+$F{action_table_id}+"-"+$F{action_round_id})
					: ($F{deposit_transfer_id} != null ? "Transfer (" + ($F{transfer_in_user} != null? $F{transfer_in_user}+")" : $F{transfer_out_user}+")")
						: ($F{affiliate_earnings_transaction_id} != null? "Earnings Payment"
							: ($F{fake_order}.intValue() == 0 ? "Balance b/f" : "Unknown"))))))]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-11" x="447" y="2" width="87" height="17" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedAmount($V{amount_sum}.doubleValue())]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-6" x="0" y="21" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
			</line>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="26" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-15" x="321" y="5" width="170" height="19" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-16" mode="Transparent" x="494" y="5" width="36" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-5" x="0" y="3" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-17" x="1" y="6" width="209" height="19" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[com.magneta.casino.reports.ReportUtil.getFormattedDate(new Date(), $P{REPORT_TIME_ZONE}, $P{REPORT_LOCALE})]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
	<noData>
		<band height="26">
			<staticText>
				<reportElement key="staticText-11" x="8" y="5" width="518" height="17"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Report is empty. There are no data to display.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
