function checkAge(strDate){
	var newDate = new Date(strDate);
	var oldDate = new Date();
	oldDate.setFullYear(oldDate.getFullYear() - 18);
	return newDate < oldDate;
}

Tapestry.Validator.minAgeLimit = function(field, message) {
	field.addValidator(function(value) {
			if (checkAge(value) == false)
				throw message;
	});
};