package com.magneta.administration.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import com.magneta.casino.common.user.utils.UserBalance;
import com.magneta.casino.services.enums.UserTypeEnum;

public class UserBean implements Serializable, Cloneable {
	
	private static final long serialVersionUID = -7897018879966479542L;

	private Long id;
	private String username;
	private String password;
	private String nickname;
	private String email;
	private String firstName;
	private String middleName;
	private String lastName;
	private String region;
	private String country;
	private String phone;
	private String postCode;
	private String town;
	private TimeZone timeZone;
	private int userType;
	private Date registerDate;
	private boolean allowFreeCredits;

	public UserBean() {
		this.userType = UserTypeEnum.NORMAL_USER.getId();
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public UserBean(UserBean user) {
		this.username = user.username;
		this.password = user.password;
		this.nickname = user.nickname;
		this.email = user.email;
		this.firstName = user.firstName;
		this.middleName = user.middleName;
		this.lastName = user.lastName;
		this.region = user.region;
		this.country = user.country;
		this.phone = user.phone;
		this.postCode = user.postCode;
		this.town = user.town;
		this.id = user.id;
		this.timeZone = user.timeZone;
		this.userType = user.userType;
	}

	@Override
	public Object clone() {
		return new UserBean(this);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * The password for the user. This value
	 * is only available during registration.
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCountry() {
		return this.country;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTown() {
		return this.town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getPostCode() {
		return this.postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getNickname() {
		return nickname;
	}

	public double getBalance() {
		return UserBalance.getBalance(id);
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public boolean isAllowFreeCredits() {
		return allowFreeCredits;
	}

	public void setAllowFreeCredits(boolean allowFreeCredits) {
		this.allowFreeCredits = allowFreeCredits;
	}
}
