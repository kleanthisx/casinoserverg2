package com.magneta.administration.beans;

import java.util.Date;

public class AffiliateBean extends UserBean {

	private static final long serialVersionUID = 1L;

	private double affiliateRate;
	private double affiliateBalance;
	private boolean approved;
	private double deposit;
	private String paymentPeriod;
	private Date lastPayment;
	private boolean paymentDue;
	private Long superAffiliate;
	private boolean active;
	private boolean hasUsers;
	private boolean hasSubCorps;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Long getSuperAffiliate() {
		return superAffiliate;
	}
	public void setSuperAffiliate(Long superAffiliate) {
		this.superAffiliate = superAffiliate;
	}
	public Date getLastPayment() {
		return lastPayment;
	}
	public void setLastPayment(Date lastPayment) {
		this.lastPayment = lastPayment;
	}

	public double getAffiliateRate() {
		return affiliateRate;
	}
	public void setAffiliateRate(double affiliateRate) {
		this.affiliateRate = affiliateRate;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public double getDeposit() {
		return deposit;
	}
	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}
	
	public String getPaymentPeriod() {
		return paymentPeriod;
	}
	public void setPaymentPeriod(String paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}
	
	public double getAffiliateBalance() {
		return affiliateBalance;
	}
	public void setAffiliateBalance(double affiliateBalance) {
		this.affiliateBalance = affiliateBalance;
	}
	public boolean isPaymentDue() {
		return paymentDue;
	}
	public void setPaymentDue(boolean paymentDue) {
		this.paymentDue = paymentDue;
	}

	public boolean isHasUsers() {
		return hasUsers;
	}
	public void setHasUsers(boolean hasUsers) {
		this.hasUsers = hasUsers;
	}
	public boolean isHasSubCorps() {
		return hasSubCorps;
	}
	public void setHasSubCorps(boolean hasSubCorps) {
		this.hasSubCorps = hasSubCorps;
	}
}
