package com.magneta.administration.beans;

import java.util.Date;

public class LoggedUserStateBean {

	private Long userId;
	private String server;
	private int serverId;
	private String status;
	private String clientIp;
	private String clientVersion;
	private Date loginDate;
	
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getServerId() {
		return serverId;
	}
	public void setServerID(int serverId) {
		this.serverId = serverId;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getClientIp() {
		return clientIp;
	}
	
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	public Date getLoginDate() {
		return loginDate;
	}
	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}
	public String getClientVersion() {
		return clientVersion;
	}
	
}
