package com.magneta.administration.beans;

import java.math.BigDecimal;
import java.util.Date;

public class JackpotPropertiesChangeBean {

	private Integer configID;
	private String username;
	private String corporate;
	private String game;
	private Date date;
	private Integer newPayCount;
	private BigDecimal newInitialAmount;
	private BigDecimal newBetContrib;
    private BigDecimal newBetAmount;
    private BigDecimal newMaximumAmount;
    private Integer newDrawNumber;
    private Boolean newAuto;
    private Integer oldPayCount;
	private BigDecimal oldInitialAmount;
	private BigDecimal oldBetContrib;
    private BigDecimal oldBetAmount;
    private BigDecimal oldMaximumAmount;
    private Integer oldDrawNumber;
    private Boolean oldAuto;
    private BigDecimal newMinPayout;
    private BigDecimal oldMinPayout;
	private BigDecimal newMinJackpotBal;
	private BigDecimal oldMinJackpotBal;
    
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getNewPayCount() {
		
		return newPayCount;
	}

	public void setNewPayCount(Integer newPayCount) {
		this.newPayCount = newPayCount;
	}

	public Double getNewInitialAmount() {
		if (newInitialAmount == null){
			return null;
		}

		return newInitialAmount.doubleValue();
	}

	public void setNewInitialAmount(BigDecimal newInitialAmount) {
		this.newInitialAmount = newInitialAmount;
	}

	public Double getNewBetContrib() {
		if (newBetContrib == null){
			return null;
		} 
		
		return newBetContrib.doubleValue();
	}

	public void setNewBetContrib(BigDecimal newBetContrib) {
		this.newBetContrib = newBetContrib;
	}

	public Double getNewBetAmount() {
		if (newBetAmount == null){
			return null;
		}

		return newBetAmount.doubleValue();
	}

	public void setNewBetAmount(BigDecimal newBetAmount) {
		this.newBetAmount = newBetAmount;
	}

	public Double getNewMaximumAmount() {
		if (newMaximumAmount == null){
			return null;
		}

		return newMaximumAmount.doubleValue();
	}

	public void setNewMaximumAmount(BigDecimal newMaximumAmount) {
		this.newMaximumAmount = newMaximumAmount;
	}

	public Integer getNewDrawNumber() {
		return newDrawNumber;
	}

	public void setNewDrawNumber(Integer newDrawNumber) {
		this.newDrawNumber = newDrawNumber;
	}

	public Boolean getNewAuto() {
		return newAuto;
	}

	public void setNewAuto(Boolean newAuto) {
		this.newAuto = newAuto;
	}

	public Integer getOldPayCount() {
		return oldPayCount;
	}

	public void setOldPayCount(Integer oldPayCount) {
		this.oldPayCount = oldPayCount;
	}

	public Double getOldInitialAmount() {
		if (oldInitialAmount == null){
			return null;
		} 
		
		return oldInitialAmount.doubleValue();
	}

	public void setOldInitialAmount(BigDecimal oldInitialAmount) {
		this.oldInitialAmount = oldInitialAmount;
	}

	public Double getOldBetContrib() {
		if (oldBetContrib == null){
			return null;
		}

		return oldBetContrib.doubleValue();
	}

	public void setOldBetContrib(BigDecimal oldBetContrib) {
		this.oldBetContrib = oldBetContrib;
	}

	public Double getOldBetAmount() {
		if (oldBetAmount == null){
			return null;
		}

		return oldBetAmount.doubleValue();
	}

	public void setOldBetAmount(BigDecimal oldBetAmount) {
		this.oldBetAmount = oldBetAmount;
	}

	public Double getOldMaximumAmount() {
		if (oldMaximumAmount == null){
			return null;
		}

		return oldMaximumAmount.doubleValue();
	}

	public void setOldMaximumAmount(BigDecimal oldMaximumAmount) {
		this.oldMaximumAmount = oldMaximumAmount;
	}

	public Integer getOldDrawNumber() {
		return oldDrawNumber;
	}

	public void setOldDrawNumber(Integer oldDrawNumber) {
		this.oldDrawNumber = oldDrawNumber;
	}

	public Boolean getOldAuto() {
		return oldAuto;
	}

	public void setOldAuto(Boolean oldAuto) {
		this.oldAuto = oldAuto;
	}

	public String getCorporate() {
		return corporate;
	}

	public void setCorporate(String corporate) {
		this.corporate = corporate;
	}

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}
	
	public String getJackpotType(){
		if (configID != null || (game == null && corporate != null)){
			return "Corporate";
		} else if (game == null && corporate == null){
			return "Mega";
		} else if (game != null){
			return "Game";
		} else {
			return "?";
		}
	}

	public Integer getConfigID() {
		return configID;
	}

	public void setConfigID(Integer configID) {
		this.configID = configID;
	}

	public BigDecimal getNewMinPayout() {
		return newMinPayout;
	}

	public void setNewMinPayout(BigDecimal newMinPayout) {
		this.newMinPayout = newMinPayout;
	}

	public BigDecimal getOldMinPayout() {
		return oldMinPayout;
	}

	public void setOldMinPayout(BigDecimal oldMinPayout) {
		this.oldMinPayout = oldMinPayout;
	}

	public BigDecimal getNewMinJackpotBal() {
		return newMinJackpotBal;
	}

	public void setNewMinJackpotBal(BigDecimal newMinJackpotBal) {
		this.newMinJackpotBal = newMinJackpotBal;
	}

	public BigDecimal getOldMinJackpotBal() {
		return oldMinJackpotBal;
	}

	public void setOldMinJackpotBal(BigDecimal oldMinJackpotBal) {
		this.oldMinJackpotBal = oldMinJackpotBal;
	}
}
