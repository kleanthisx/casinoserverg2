package com.magneta.administration.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GameClientGameBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private int gameId;
	private int clientId;
	private String gameName;
	private boolean enabled;

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setWasEnabled(boolean wasEnabled) {
		this.enabled = wasEnabled;
	}

	public void setEnabled(boolean enabled) {
		if (this.enabled == enabled){
			return;
		}
		Connection dbConn = ConnectionFactory.getConnection();

		if (dbConn == null){		
			return;
		}

		try
		{
			int rows = 0;
			PreparedStatement statement = null;
			try {
				String sql = 
						"UPDATE game_client_games" +
								" SET enabled = ?"+
										" WHERE client_id = ?" +
										" AND game_id = ?";
				statement = dbConn.prepareStatement(sql);

				statement.setBoolean(1, enabled);
				statement.setInt(2, this.clientId);
				statement.setInt(3, this.gameId);

				rows = statement.executeUpdate();
			} finally {
				DbUtil.close(statement);
			}

			if (enabled && (rows <= 0)){
				PreparedStatement statement2 = null;
				try {
					String sql = 
							"INSERT INTO game_client_games(client_id, game_id, enabled)" +
									" VALUES(?, ?, ?)";
					statement2 = dbConn.prepareStatement(sql);

					statement2.setInt(1, this.clientId);
					statement2.setInt(2, this.gameId);
					statement2.setBoolean(3, enabled);

					rows = statement2.executeUpdate();
				} finally {
					DbUtil.close(statement2);
				}
			}

			this.enabled = enabled;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally{
			DbUtil.close(dbConn);
		}
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
}
