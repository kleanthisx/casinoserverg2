package com.magneta.administration.beans;

import java.io.Serializable;
import java.util.List;

import com.magneta.casino.services.beans.SettingBean;

public class SettingSection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3522312233101636837L;
	private String name;
	private List<SettingSection> subSections;
	private List<SettingBean> settings;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<SettingSection> getSubSections() {
		return subSections;
	}
	public void setSubSections(List<SettingSection> subSections) {
		this.subSections = subSections;
	}
	
	public List<SettingBean> getSettings() {
		return settings;
	}
	public void setSettings(List<SettingBean> settings) {
		this.settings = settings;
	}
	
	
}
