package com.magneta.administration.beans;

import java.io.Serializable;

public class PlayerGameSettingsBean implements Serializable {

	private static final long serialVersionUID = -727249994298610970L;
	
	public static final int PERSONAL_SETTING_SOURCE = 1;
	public static final int CATEGORY_SETTING_SOURCE = 2;
	public static final int GAME_DEFAULT_SETTING_SOURCE = 3;
	
	private int categoryID;
	private int gameID;
	private String gameName;
	private Double minBet;
	private Double maxBet;
	private int minBetSource;
	private int maxBetSource;
	private String categoryDesc;	
	
	public int getCategoryID() {
		return categoryID;
	}
	
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	
	public int getGameID() {
		return gameID;
	}
	
	public void setGameID(int gameID) {
		this.gameID = gameID;
	}
	
	public String getGameName() {
		return gameName;
	}
	
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public Double getMinBet() {
		return minBet;
	}
	
	public void setMinBet(Double minBet) {
		this.minBet = minBet;
	}
	
	public Double getMaxBet() {
		return maxBet;
	}
	
	public void setMaxBet(Double maxBet) {
		this.maxBet = maxBet;
	}

	public int getMinBetSource() {
		return minBetSource;
	}

	public void setMinBetSource(int minBetSource) {
		this.minBetSource = minBetSource;
	}

	public int getMaxBetSource() {
		return maxBetSource;
	}

	public void setMaxBetSource(int maxBetSource) {
		this.maxBetSource = maxBetSource;
	}

	public String getCategoryDesc() {
		return categoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
}