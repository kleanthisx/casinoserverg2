package com.magneta.administration.beans;

import com.magneta.casino.common.utils.FormatUtils;

public class JackpotWinBean {

	private Long userId;
	private String winDate;
	private double amount;
	private Long tableId;
	private int roundId;
	private String jackpotType;
	
	public String getJackpotType() {
		return jackpotType;
	}

	public void setJackpotType(String jackpotType) {
		this.jackpotType = jackpotType;
	}

	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getGameId() {
		return tableId+"-"+roundId;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getWinDate() {
		return winDate;
	}
	
	public void setWinDate(String winDate) {
		this.winDate = winDate;
	}
	
	public String getAmountString(){
		return FormatUtils.getFormattedAmount(amount);
	}

	public int getRoundId() {
		return roundId;
	}

	public void setRoundId(int roundId) {
		this.roundId = roundId;
	}

	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}
}