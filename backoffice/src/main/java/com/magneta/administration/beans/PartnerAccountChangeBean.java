package com.magneta.administration.beans;

import java.util.Date;

public class PartnerAccountChangeBean {
	
	private String username;
	private String changeUser;
	private String field;
	private String oldValue;
	private String newValue;
	private Date changeTime;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getChangeUser() {
		return changeUser;
	}
	
	public void setChangeUser(String changeUser) {
		this.changeUser = changeUser;
	}
	
	public String getField() {
		return field;
	}
	
	public void setField(String field) {
		this.field = field;
	}

	public Date getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
}