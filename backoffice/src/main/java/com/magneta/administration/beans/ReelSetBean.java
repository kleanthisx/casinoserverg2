package com.magneta.administration.beans;

import java.io.Serializable;

public class ReelSetBean implements Serializable {
	private static final long serialVersionUID = -6755758627108480892L;
	private ReelBean[] reels;
	
	public ReelBean[] getReels() {
		return this.reels;
	}
	
	public void setReels(ReelBean[] reels) {
		this.reels = reels;
	}
}
