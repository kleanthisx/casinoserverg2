package com.magneta.administration.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import com.magneta.casino.common.utils.FormatUtils;

public class UsersSearchResultBean implements Serializable {
    
    private static final long serialVersionUID = -1726982554541622920L;
    
    private Long id;
    private String username;
    private String nickname;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String phone;
    private String region;
    private String town;
    private String country;
    private Date registerDate;
    private boolean active;
    private String store;
    private int type;
    
    public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public String getTypeString() {
		switch (type){
			case 0: return "Pl.";
			case 1: return "Loc.";
			case 2: return "Mach.";
			case 3: return "Staff";
			case 4: return "Corp.";
			case 5: return "Corp. Pl.";
			case 6: return "Demo Pl.";
			case 7: return "Loc. Man.";
			case -1: return "Other";
			default: return "?";
		}
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public boolean isActive() {
        return active;
    }

    
    public void setActive(boolean active) {
        this.active = active;
    }

    
    public String getMiddleName() {
        return middleName;
    }

    
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    
    public String getPhone() {
        return phone;
    }

    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    
    public String getRegion() {
        return region;
    }

    
    public void setRegion(String region) {
        this.region = region;
    }

    
    public String getTown() {
        return town;
    }

    
    public void setTown(String town) {
        this.town = town;
    }

    public String getCountry() {
        return country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    
    public Date getRegisterDate() {
        return registerDate;
    }
    
    public String getRegisterDate(TimeZone timeZone, Locale locale){
        return FormatUtils.getFormattedDate(getRegisterDate(),timeZone,locale);
    }
    
    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
    
    public String getStatus(){
        return (active? "Active": "Inactive");
    }
}
