package com.magneta.administration.beans;

public class UserGameBean {

	private int rounds;
	private double bets;
	private double wins;
	private double realBets;
	private double realWins;
	private double jackpotWins;
	private String game;
	
	public int getRounds() {
		return rounds;
	}
	public void setRounds(int rounds) {
		this.rounds = rounds;
	}
	public double getBets() {
		return bets;
	}
	public void setBets(double bets) {
		this.bets = bets;
	}
	public double getWins() {
		return wins;
	}
	public void setWins(double wins) {
		this.wins = wins;
	}
	public double getJackpotWins() {
		return jackpotWins;
	}
	public void setJackpotWins(double jackpotWins) {
		this.jackpotWins = jackpotWins;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public double getRealBets() {
		return realBets;
	}
	public void setRealBets(double realBets) {
		this.realBets = realBets;
	}
	public double getRealWins() {
		return realWins;
	}
	public void setRealWins(double realWins) {
		this.realWins = realWins;
	}
	
}
