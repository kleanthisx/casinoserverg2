/**
 * 
 */
package com.magneta.administration.beans;

public class PlayerGameBean {

    private Long tableId;
    private int roundId;
   
    private String date;
    private String game;
    
    public String getGame() {
        return game;
    }
    
    public void setGame(String game) {
        this.game = game;
    }

    public String getDate() {
        return date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public int getRoundId() {
        return roundId;
    }
    
    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }
    
    public Long getTableId() {
        return tableId;
    }
    
    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }
    
    public String getGameId(){
        return tableId+"-"+roundId;
    }
}
