package com.magneta.administration.beans;

import java.util.Date;

public class UserTransactionBean {

	public static final int PAYMENT_TRANSACTION_TYPE = 1;
	public static final int PAYOUT_TRANSACTION_TYPE = 2;
	public static final int BALANCE_ADJUSTMENT = 4;
	
	private Long transactionId;
	private String paymentMethod;
	private double amount;
	private Date dateCompleted;
	private int transactionType;
	private String comment;
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getDateCompleted() {
		return dateCompleted;
	}
	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}
	public int getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
}
