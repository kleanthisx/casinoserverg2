package com.magneta.administration.beans;

public class SystemSetting  {

	public static final int INTEGER_VALUE = 0;
	public static final int DOUBLE_VALUE = 1;
	public static final int BOOLEAN_VALUE = 2;
	public static final int STRING_VALUE = 3;
	public static final int PERCENTAGE_VALUE = 4;
	public static final int COUNTRY_VALUE = 5;
	public static final int MULTILINE_STRING_VALUE = 6;
	public static final int LONG_VALUE = 7;
	
	private SystemSetting() {}
}
