package com.magneta.administration.beans;

import java.io.Serializable;
import java.util.Date;

public class UserAccountChangesBean implements Serializable {
    
    private static final long serialVersionUID = -1726982554541622920L;
    
    private Long id;
    private Date changeTime;
    private String oldUsername;
    private String oldPasswordHash;
    private String oldPasswordSalt;
    private String oldEmail;
    private boolean oldIsActive;
    private String oldFirstName;
    private String oldLastName;
    private String oldMiddleName;
    private String oldCountry;
    private String oldPhone;
    private String oldRegion;
    private String oldTown;
    private String oldPostalCode;
    private String oldNickname;
    private String oldTimeZone;
    private int oldUserType;
    private String oldPostalAddress;
    private Date oldDateDeactivated;
    private boolean oldClosed;
    
    private String newUsername;
    private String newPasswordHash;
    private String newPasswordSalt;
    private String newEmail;
    private boolean newIsActive;
    private String newFirstName;
    private String newLastName;
    private String newMiddleName;
    private String newCountry;
    private String newPhone;
    private String newRegion;
    private String newTown;
    private String newPostalCode;
    private String newNickname;
    private String newTimeZone;
    private int newUserType;
    private String newPostalAddress;
    private Date newDateDeactivated;
    private boolean newClosed;
    
    private String changeUser;
    private String editUser;
	
	public String getEditUser() {
		return editUser;
	}

	public void setEditUser(String editUser) {
		this.editUser = editUser;
	}

	public String getOldUserTypeString(){
		return getUserTypeString(oldUserType);
	}
	
	public String getNewUserTypeString(){
		return getUserTypeString(newUserType);
	}
	
	private String getUserTypeString(int userType){
		switch (userType){
			case 0: return "U";
			case 1: return "L";
			case 2: return "M";
			case 3: return "LM";
			case 4: return "C";
			case 5: return "CU";
			case -1: return "A";
			default: return "?";
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}

	public String getOldUsername() {
		return oldUsername;
	}

	public void setOldUsername(String oldUsername) {
		this.oldUsername = oldUsername;
	}

	public String getOldPasswordHash() {
		return oldPasswordHash;
	}

	public void setOldPasswordHash(String oldPasswordHash) {
		this.oldPasswordHash = oldPasswordHash;
	}

	public String getOldPasswordSalt() {
		return oldPasswordSalt;
	}

	public void setOldPasswordSalt(String oldPasswordSalt) {
		this.oldPasswordSalt = oldPasswordSalt;
	}

	public String getOldEmail() {
		return oldEmail;
	}

	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}

	public boolean isOldIsActive() {
		return oldIsActive;
	}

	public void setOldIsActive(boolean oldIsActive) {
		this.oldIsActive = oldIsActive;
	}

	public String getOldFirstName() {
		return oldFirstName;
	}

	public void setOldFirstName(String oldFirstName) {
		this.oldFirstName = oldFirstName;
	}

	public String getOldLastName() {
		return oldLastName;
	}

	public void setOldLastName(String oldLastName) {
		this.oldLastName = oldLastName;
	}

	public String getOldMiddleName() {
		return oldMiddleName;
	}

	public void setOldMiddleName(String oldMiddleName) {
		this.oldMiddleName = oldMiddleName;
	}

	public String getOldCountry() {
		return oldCountry;
	}

	public void setOldCountry(String oldCountry) {
		this.oldCountry = oldCountry;
	}

	public String getOldPhone() {
		return oldPhone;
	}

	public void setOldPhone(String oldPhone) {
		this.oldPhone = oldPhone;
	}

	public String getOldRegion() {
		return oldRegion;
	}

	public void setOldRegion(String oldRegion) {
		this.oldRegion = oldRegion;
	}

	public String getOldTown() {
		return oldTown;
	}

	public void setOldTown(String oldTown) {
		this.oldTown = oldTown;
	}

	public String getOldPostalCode() {
		return oldPostalCode;
	}

	public void setOldPostalCode(String oldPostalCode) {
		this.oldPostalCode = oldPostalCode;
	}

	public String getNewPostalCode() {
		return newPostalCode;
	}

	public void setNewPostalCode(String newPostalCode) {
		this.newPostalCode = newPostalCode;
	}

	public String getOldNickname() {
		return oldNickname;
	}

	public void setOldNickname(String oldNickname) {
		this.oldNickname = oldNickname;
	}

	public String getOldTimeZone() {
		return oldTimeZone;
	}

	public void setOldTimeZone(String oldTimeZone) {
		this.oldTimeZone = oldTimeZone;
	}

	public int getOldUserType() {
		return oldUserType;
	}

	public void setOldUserType(int oldUserType) {
		this.oldUserType = oldUserType;
	}

	public String getOldPostalAddress() {
		return oldPostalAddress;
	}

	public void setOldPostalAddress(String oldPostalAddress) {
		this.oldPostalAddress = oldPostalAddress;
	}

	public Date getOldDateDeactivated() {
		return oldDateDeactivated;
	}

	public void setOldDateDeactivated(Date oldDateDeactivated) {
		this.oldDateDeactivated = oldDateDeactivated;
	}

	public String getNewUsername() {
		return newUsername;
	}

	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}

	public String getNewPasswordHash() {
		return newPasswordHash;
	}

	public void setNewPasswordHash(String newPasswordHash) {
		this.newPasswordHash = newPasswordHash;
	}

	public String getNewPasswordSalt() {
		return newPasswordSalt;
	}

	public void setNewPasswordSalt(String newPasswordSalt) {
		this.newPasswordSalt = newPasswordSalt;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}

	public boolean isNewIsActive() {
		return newIsActive;
	}

	public void setNewIsActive(boolean newIsActive) {
		this.newIsActive = newIsActive;
	}

	public String getNewFirstName() {
		return newFirstName;
	}

	public void setNewFirstName(String newFirstName) {
		this.newFirstName = newFirstName;
	}

	public String getNewLastName() {
		return newLastName;
	}

	public void setNewLastName(String newLastName) {
		this.newLastName = newLastName;
	}

	public String getNewMiddleName() {
		return newMiddleName;
	}

	public void setNewMiddleName(String newMiddleName) {
		this.newMiddleName = newMiddleName;
	}

	public String getNewCountry() {
		return newCountry;
	}

	public void setNewCountry(String newCountry) {
		this.newCountry = newCountry;
	}

	public String getNewPhone() {
		return newPhone;
	}

	public void setNewPhone(String newPhone) {
		this.newPhone = newPhone;
	}

	public String getNewRegion() {
		return newRegion;
	}

	public void setNewRegion(String newRegion) {
		this.newRegion = newRegion;
	}

	public String getNewTown() {
		return newTown;
	}

	public void setNewTown(String newTown) {
		this.newTown = newTown;
	}

	public String getNewNickname() {
		return newNickname;
	}

	public void setNewNickname(String newNickname) {
		this.newNickname = newNickname;
	}

	public String getNewTimeZone() {
		return newTimeZone;
	}

	public void setNewTimeZone(String newTimeZone) {
		this.newTimeZone = newTimeZone;
	}

	public int getNewUserType() {
		return newUserType;
	}

	public void setNewUserType(int newUserType) {
		this.newUserType = newUserType;
	}

	public String getNewPostalAddress() {
		return newPostalAddress;
	}

	public void setNewPostalAddress(String newPostalAddress) {
		this.newPostalAddress = newPostalAddress;
	}

	public Date getNewDateDeactivated() {
		return newDateDeactivated;
	}

	public void setNewDateDeactivated(Date newDateDeactivated) {
		this.newDateDeactivated = newDateDeactivated;
	}

	public String getChangeUser() {
		return changeUser;
	}

	public void setChangeUser(String changeUser) {
		this.changeUser = changeUser;
	}

	public boolean isOldClosed() {
		return oldClosed;
	}

	public void setOldClosed(boolean oldClosed) {
		this.oldClosed = oldClosed;
	}

	public boolean isNewClosed() {
		return newClosed;
	}

	public void setNewClosed(boolean newClosed) {
		this.newClosed = newClosed;
	}
}
