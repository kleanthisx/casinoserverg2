/**
 * 
 */
package com.magneta.administration.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@SuppressWarnings("serial")
public class RoleBean implements Serializable {
    
	private static final Logger log = LoggerFactory.getLogger(RoleBean.class);
	
    private int roleId;
    private String roleDesc;
    
    public RoleBean(){
        this.roleId = -1;
        this.roleDesc = "";
    }
    
    public String getRoleDesc() {
        return roleDesc;
    }
    
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }
    
    public int getRoleId() {
        return roleId;
    }
    
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    
    public enum CreateRoleResult {
        DB_ERROR,
        DESC_NOT_UNIQUE,
        OK
    }
    
    public CreateRoleResult createRole(List<RolePrivilege> privileges) {
        Connection dbConn = ConnectionFactory.getConnection();
        CreateRoleResult result = CreateRoleResult.OK;
        
        if (dbConn == null){
			return CreateRoleResult.DB_ERROR;
		}
        
        try
        {   
        	dbConn.setAutoCommit(false);
        	int id = -1;
        	
        	Statement idStmt = null;
        	ResultSet idRs = null;
        	
        	try {
        		idStmt = dbConn.createStatement();
        		idRs = idStmt.executeQuery("SELECT nextval('role_id_seq')");
        		if (idRs.next()){
        			id = idRs.getInt(1);
        		}
        	} finally {
        		DbUtil.close(idRs);
        		DbUtil.close(idStmt);
        	}
        	
        	PreparedStatement roleStatement = null;
            ResultSet roleRs = null;
        	
            try {
	            String sql = 
	                "INSERT INTO roles(role_id, role_desc)" +
	                " VALUES(?,?)";
	            
	            roleStatement = dbConn.prepareStatement(sql);
	            roleStatement.setInt(1, id);
	            roleStatement.setString(2, roleDesc);
	                
                roleStatement.executeUpdate();
            } finally {
            	DbUtil.close(roleRs);
        		DbUtil.close(roleStatement);
            }
            
            PreparedStatement privStatement = null;
            
            try{
	            privStatement = dbConn.prepareStatement(
	            		"INSERT INTO role_privileges(role_id, privilege_key, allow_write)" +
	            		" VALUES (?,?,?)");
	            
	            for (RolePrivilege priv: privileges){
	            	if (priv.getAvailability() != RolePrivilege.NOT_GRANTED_AVAILABILITY) {
		            	privStatement.setInt(1, id);
		            	privStatement.setString(2, priv.getPrivilegeKey());
		            	privStatement.setBoolean(3, priv.getAvailability() == RolePrivilege.READ_WRITE_AVAILABILITY);
		            	
		            	privStatement.execute();
	            	}
	            }
	            
	            dbConn.commit();
	            return CreateRoleResult.OK;
            } finally{
            	DbUtil.close(privStatement);
            }
        } catch (SQLException e) {
        	try {
				dbConn.rollback();
			} catch (SQLException e1) {
				log.error("Error rolling back.",e1);
			}
            if (e.toString().contains("unique_role_desc")){
                result = CreateRoleResult.DESC_NOT_UNIQUE;
            }
            else{
                result = CreateRoleResult.DB_ERROR;
                log.error("Error while creating new role",e);
            }
        } finally{
            DbUtil.close(dbConn);
        }
        
        return result;
    }

    public CreateRoleResult updateRole(List<RolePrivilege> privileges){
        Connection dbConn = ConnectionFactory.getConnection();
        CreateRoleResult result = CreateRoleResult.DB_ERROR;
        
        if (dbConn == null){
			return CreateRoleResult.DB_ERROR;
		}
        
        try
        {            
            dbConn.setAutoCommit(false);
            
            PreparedStatement delStmt = null;
            try {
	            delStmt = dbConn.prepareStatement(
	            		"DELETE FROM role_privileges" +
	            		" WHERE role_id = ?");
	            delStmt.setInt(1,this.roleId);
	            
	            delStmt.execute();
            } finally{
            	DbUtil.close(delStmt);
            }
            
            PreparedStatement insertStmt = null;
            
            try {
            	insertStmt = dbConn.prepareStatement(
            		"INSERT INTO role_privileges(role_id, privilege_key, allow_write)" +
            		" VALUES (?,?,?)");
            	
            	for (RolePrivilege priv: privileges){
	            	if (priv.getAvailability() != RolePrivilege.NOT_GRANTED_AVAILABILITY){
		            	insertStmt.setInt(1, this.roleId);
		            	insertStmt.setString(2, priv.getPrivilegeKey());
		            	insertStmt.setBoolean(3, priv.getAvailability() == RolePrivilege.READ_WRITE_AVAILABILITY);
		            	
		            	insertStmt.execute();
	            	}
	            }
            } finally{
            	DbUtil.close(insertStmt);
            }
            
            dbConn.commit();
            return CreateRoleResult.OK;
        }catch (SQLException e) {
        	try {
				dbConn.rollback();
			} catch (SQLException e1) {
				log.error("Error rolling back.",e1);
			}
        	log.error("Error while updating role",e);
        } finally{
            DbUtil.close(dbConn);
        }
        
        return result;
    }
}
