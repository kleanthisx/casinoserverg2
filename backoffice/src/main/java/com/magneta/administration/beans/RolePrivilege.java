package com.magneta.administration.beans;

import java.io.Serializable;

public class RolePrivilege implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final int NOT_GRANTED_AVAILABILITY = 1;
	public static final int READ_ONLY_AVAILABILITY = 2;
	public static final int READ_WRITE_AVAILABILITY = 3;
	
	private String privilegeKey;
	private String privilegeDesc;
	private int availability = NOT_GRANTED_AVAILABILITY;
	
	public int getAvailability() {
		return availability;
	}

	public void setAvailability(int availability) {
		this.availability = availability;
	}

	public String getPrivilegeDesc() {
		return privilegeDesc;
	}
	
	public void setPrivilegeDesc(String privilegeDesc) {
		this.privilegeDesc = privilegeDesc;
	}
	
	public String getPrivilegeKey() {
		return privilegeKey;
	}
	
	public void setPrivilegeKey(String privilegeKey) {
		this.privilegeKey = privilegeKey;
	}
}
