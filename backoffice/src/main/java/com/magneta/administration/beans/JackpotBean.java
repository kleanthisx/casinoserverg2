package com.magneta.administration.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.casino.common.utils.FormatUtils;

public class JackpotBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer jackpotID;
	private Integer configID;
	private String game;
	private String corporate;
	private int gameID;
	private double initialAmount;
	private double maxAmount;
	private double jackpotAmount;
	private Integer payTimes;
	private String dateStarted;
	private double contribRate;
	private double betAmount;
	private boolean jackpotBetType;
	private int timesPaid;
	private boolean mystery;
	private String jackpotType;
	private double adjustmentAmount;

	public double getAdjustmentAmount() {
		return adjustmentAmount;
	}

	public void setAdjustmentAmount(double adjustmentAmount) {
		if (adjustmentAmount != 0.0){
			Connection dbConn = ConnectionFactory.getConnection();

			if (dbConn == null){
				return;
			}

			String sql = 
				"INSERT INTO jackpot_transactions(jackpot_id,user_id,amount)" +
				" VALUES(?,NULL,?)";
			PreparedStatement statement = null;
			try{
				statement = dbConn.prepareStatement(sql);
				statement.setInt(1, jackpotID);
				statement.setBigDecimal(2, new BigDecimal(adjustmentAmount));
				statement.execute();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				DbUtil.close(statement);
				DbUtil.close(dbConn);
			}
		}
	}

	public boolean isMystery() {
		return mystery;
	}

	public String getCorporate() {
		return corporate;
	}

	public void setCorporate(String corporate) {
		this.corporate = corporate;
	}

	public void setMystery(boolean mystery) {
		this.mystery = mystery;
	}

	public double getBetAmount() {
		return betAmount;
	}
	
	public boolean isJackpotBetType() {
		return jackpotBetType;
	}

	public void setJackpotBetType(boolean jackpotBetType) {
		this.jackpotBetType = jackpotBetType;
	}

	public String getBetAmountString() {
		if (betAmount == 0.0){
			return "-";
		}
		return FormatUtils.getFormattedAmount(betAmount);
	}
	
	public void setBetAmount(double betAmount) {
		this.betAmount = betAmount;
	}
	
	public double getContribRate() {
		return contribRate;
	}
	
	public String getContribRateString() {
		if (contribRate == 0.0){
			return "-";
		} 
		return contribRate + " %";
	}
	
	public void setContribRate(double contribRate) {
		this.contribRate = contribRate;
	}
	
	public String getGame() {
		return game;
	}
	
	public void setGame(String game) {
		this.game = game;
	}
	
	public double getInitialAmount() {
		return initialAmount;
	}
	
	public String getInitialAmountString() {
		return FormatUtils.getFormattedAmount(initialAmount);
	}
	
	public void setInitialAmount(double initialAmount) {
		this.initialAmount = initialAmount;
	}
	
	public double getJackpotAmount() {
		return jackpotAmount;
	}
	
	public String getJackpotAmountString() {
		return FormatUtils.getFormattedAmount(jackpotAmount);
	}
	
	public void setJackpotAmountString(String jackpotAmountString) {
	}
	
	public void setJackpotAmount(double jackpotAmount) {
		this.jackpotAmount = jackpotAmount;
	}
	
	public Integer getJackpotID() {
		return jackpotID;
	}
	
	public Integer getPayTimes() {
		return payTimes;
	}
	
	public void setPayTimes(Integer payTimes) {
		this.payTimes = payTimes;
	}

	public String getDateStarted() {
		return dateStarted;
	}

	public void setDateStarted(String dateStarted) {
		this.dateStarted = dateStarted;
	}

	public int getGameID() {
		return gameID;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public int getTimesPaid() {
		return timesPaid;
	}
	
	public void setTimesPaid(int timesPaid) {
		this.timesPaid = timesPaid;
	}
	
	public String getPayTimesString() {
		if (payTimes == null){
			return "Unlimited";
		}
		return String.valueOf(payTimes);
	}
	
	public double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}
	
	public String getMaxAmountString() {
		if (maxAmount == 0.0){
			return "-";
		}

		return FormatUtils.getFormattedAmount(maxAmount);
	}

	public Integer getConfigID() {
		return configID;
	}

	public void setConfigID(Integer configID) {
		this.configID = configID;
	}

	public void setJackpotID(Integer jackpotID) {
		this.jackpotID = jackpotID;
	}

	public String getJackpotType() {
		return jackpotType;
	}

	public void setJackpotType(String jackpotType) {
		this.jackpotType = jackpotType;
	}
}
