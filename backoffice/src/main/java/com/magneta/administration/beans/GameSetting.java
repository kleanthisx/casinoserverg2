package com.magneta.administration.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.db.DbUtil;

public class GameSetting implements Serializable{

	public static final int INTEGER_VALUE = 0;
	public static final int DOUBLE_VALUE = 1;
	public static final int BOOLEAN_VALUE = 2;
	public static final int STRING_VALUE = 3;
	public static final int PERCENTAGE_VALUE = 4;
	public static final int COUNTRY_VALUE = 5;
	
	private static final long serialVersionUID = 1L;

	private String settingName;
	private Object settingValue;
	private Object oldSettingValue;
	private int settingType;
	private String settingKey;
	private int gameID;
	private boolean resetToDefault;
	
	public String getSettingName() {
		return settingName;
	}

	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}

	public int getSettingType() {
		return settingType;
	}

	public void setSettingType(int settingType) {
		this.settingType = settingType;
	}
	
	public boolean getResetToDefault(){
		return resetToDefault;
	}
	
	public void setResetToDefault(boolean resetToDefault){
		this.resetToDefault = resetToDefault;
	}

	public void setSettingValue(String value, int type){
		this.oldSettingValue = this.settingValue = GameTemplateSetting.parseValue(value, type);
	}
	
	public void setSettingValue(Object settingValue){
		this.settingValue = settingValue;
	}
	
	public Object getSettingValue() {
		return settingValue;
	}
	
	public void update(Connection conn, Long userId) throws SQLException{
		if (conn == null || (!resetToDefault && oldSettingValue.equals(settingValue))){
			return;
		}
		
		PreparedStatement statement = null;
		try {
			String sql = 
				/*"UPDATE game_settings" +
				" SET setting_value = ?" +
				" WHERE setting_key = ?" +
				" AND game_id = ?";*/
				"SELECT change_game_setting(?,?,?,?)";
			statement = conn.prepareStatement(sql);
			
			statement.setInt(1, this.gameID);
			statement.setString(2, this.settingKey);
			
			if (this.resetToDefault) {
				statement.setString(3, null);
			} else {
				statement.setString(3, translateValue());
			}
			statement.setLong(4, userId);
			
			statement.execute();
		} finally {
			DbUtil.close(statement);
		}
	}
	
	private String translateValue(){
		switch (settingType){
			case PERCENTAGE_VALUE:
				return String.valueOf(((Number)this.settingValue).doubleValue() / 100.0);
			default:
				return String.valueOf(this.settingValue);
		}
	}

	public int getGameID() {
		return gameID;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}
}
