package com.magneta.administration.beans;

import java.io.Serializable;

public class CorporateSettingsBean implements Serializable {

	private static final long serialVersionUID = -727249994298610970L;
	private int gameID;
	private String gameName;
	private Double minBet;
	private Double maxBet;
	private Long userID;
	
	public Long getUserID() {
		return userID;
	}
	
	public void setUserID(Long id){
		this.userID = id;
	}
	
	public int getGameID() {
		return gameID;
	}
	
	public void setGameID(int gameID) {
		this.gameID = gameID;
	}
	
	public String getGameName() {
		return gameName;
	}
	
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public Double getMinBet() {
		return minBet;
	}
	
	public void setMinBet(Double minBet) {
		this.minBet = minBet;
	}
	
	public Double getMaxBet() {
		return maxBet;
	}
	
	public void setMaxBet(Double maxBet) {
		this.maxBet = maxBet;
	}
}