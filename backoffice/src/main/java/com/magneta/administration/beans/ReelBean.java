package com.magneta.administration.beans;

import java.io.Serializable;

import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;


public class ReelBean implements Serializable {

	private static final long serialVersionUID = -337398692577283511L;

	private ComboSymbolBean[] reelNumImages;
	private String reel;
	
	
	public ReelBean(String reel, ComboSymbolBean[] symbols) {
		this.reel = reel;
		String[] reelNums = reel.split(",");
		this.reelNumImages = new ComboSymbolBean[reelNums.length];
		for(int i = 0; i<reelNums.length ; i++) {
			ComboSymbolBean symbol = symbols[Integer.parseInt(reelNums[i].trim()) - 1];
			reelNumImages[i] = symbol;
		}
	}

	public ComboSymbolBean[] getReelNumImages() {
		return reelNumImages;
	}

	public void setReelNumImages(ComboSymbolBean [] reelNumImage) {
		this.reelNumImages = reelNumImage;
	}

	public String getReel() {
		return reel;
	}

	public void setReel(String reel) {
		this.reel = reel;
	}
}
