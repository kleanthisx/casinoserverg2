package com.magneta.administration.beans;

import java.io.Serializable;

import com.magneta.casino.services.enums.UserTypeEnum;

public class UserTypeSelectionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7819202908769681389L;

	private UserTypeEnum type;
	private boolean selected;

	public void setType(UserTypeEnum type) {
		this.type = type;
	}
	public UserTypeEnum getType() {
		return type;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isSelected() {
		return selected;
	}
	
	
}
