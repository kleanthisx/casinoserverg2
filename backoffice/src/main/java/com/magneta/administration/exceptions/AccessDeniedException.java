package com.magneta.administration.exceptions;

public class AccessDeniedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4607652782033041635L;

	public AccessDeniedException(String string) {
		super(string);
	}

}

