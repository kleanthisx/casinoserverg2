package com.magneta.administration.exceptions;

public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5006648752345636743L;

	public BadRequestException(String string) {
		super(string);
	}

}
