package com.magneta.administration.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.PaymentTransactionBean;
import com.magneta.administration.beans.PayoutTransactionBean;
import com.magneta.administration.beans.WireTransferBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public abstract class DepositsWithdrawalsUtils {

	/**Returns a list with all the complete deposit transactions
	 * 
	 * @param offset
	 * @param limit
	 * @param sortCol
	 * @param sortAsc
	 * @param showColumns
	 * @return
	 */
	public static List<PaymentTransactionBean> getCompleteDeposits(int offset, int limit, ITableColumn sortCol, boolean sortAsc){
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		List<PaymentTransactionBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumnName = "TransactionId";

		if (sortCol != null)
			sortColumnName = sortCol.getColumnName();

		if (("TransactionId").equalsIgnoreCase(sortColumnName))
			sortColumnName = "transaction_id";
		else if (("TimeCompleted").equalsIgnoreCase(sortColumnName))
			sortColumnName = "time_completed";
		else if (("PaymentMethod").equalsIgnoreCase(sortColumnName))
			sortColumnName = "ps_specific";
		else if (("Amount").equalsIgnoreCase(sortColumnName))
			sortColumnName = "amount";
		else
			sortColumnName = null;
		
		try
		{   
			String sql = 
				"SELECT transaction_id, payment_transactions.user_id, amount, time_completed, ps_specific"+
				" FROM payment_transactions"+
				(sortColumnName != null? " ORDER BY "+sortColumnName : "")+
				(sortAsc? " ASC" : " DESC") +
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			ArrayList<PaymentTransactionBean> myArr = new ArrayList<PaymentTransactionBean>();
			PaymentTransactionBean currRow;

			while (res.next()){
				currRow = new PaymentTransactionBean();
				currRow.setTransactionId(res.getLong("transaction_id"));
				currRow.setUserId(res.getLong("user_id"));
				currRow.setAmount(res.getDouble("amount"));
				currRow.setTimeCompleted(DbUtil.getTimestamp(res, "time_completed"));
				currRow.setPaymentMethod(res.getString("ps_specific"));
				myArr.add(currRow);
			}

			it = myArr;

		}catch (SQLException e) {
			throw new RuntimeException("Error while retrieving latest payments.",e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
		return it;
	}

	/**Returns a list with all the complete withdrawals transactions
	 * 
	 * @param offset
	 * @param limit
	 * @param sortCol
	 * @param sortAsc
	 * @param showColumns
	 * @return
	 */
	public static List<PayoutTransactionBean> getCompleteWithdrawals(int offset, int limit, ITableColumn sortCol, boolean sortAsc){
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		List<PayoutTransactionBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumnName = "TransactionId";

		if (sortCol != null)
			sortColumnName = sortCol.getColumnName();

		if (("TransactionId").equalsIgnoreCase(sortColumnName))
			sortColumnName = "payout_id";
		else if (("TimeCompleted").equalsIgnoreCase(sortColumnName))
			sortColumnName = "payout_date";
		else if (("PaymentMethod").equalsIgnoreCase(sortColumnName))
			sortColumnName = "ps_specific";
		else if (("Amount").equalsIgnoreCase(sortColumnName))
			sortColumnName = "amount";
		else
			sortColumnName = null;
		
		try
		{   
			String sql = 
				"SELECT payout_id,payout_transactions.user_id , amount, time_completed, ps_specific"+
				" FROM payout_transactions"+
				(sortColumnName != null? " ORDER BY "+sortColumnName : "")+
				(sortAsc? " ASC" : " DESC") +
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			List<PayoutTransactionBean> myArr = new ArrayList<PayoutTransactionBean>();
			PayoutTransactionBean currRow;

			while (res.next()){
				currRow = new PayoutTransactionBean();
				currRow.setTransactionId(res.getLong("payout_id"));
				currRow.setUserId(res.getLong("user_id"));
				currRow.setAmount(res.getDouble("amount"));
				currRow.setTimeCompleted(DbUtil.getTimestamp(res, "time_completed"));
				currRow.setPaymentMethod(res.getString("ps_specific"));	
				myArr.add(currRow);
			}

			it = myArr;

		}catch (SQLException e) {
			throw new RuntimeException("Error while retrieving latest payouts.",e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}
	
	/** Returns the number of complete deposit transactions
	 * 
	 * @return
	 */
	public static int getCompleteDepositsCount(){
		Connection dbConn = ConnectionFactory.getReportsConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {
            String sql = 
                "SELECT COUNT(*)"+
                " FROM payment_transactions";
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        }catch (SQLException e) {
            throw new RuntimeException("Error while getting latest payments count.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return count;
	}
	
	/** Returns the number of complete withdrawals transactions
	 * 
	 * @return
	 */
	public static int getCompleteWithdrawalsCount(){
		Connection dbConn = ConnectionFactory.getReportsConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {
            String sql = 
                "SELECT COUNT(*)"+
                " FROM payout_transactions";
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);
            
            if (res.next()) {
                count = res.getInt(1);
            }
            
        }catch (SQLException e) {
            throw new RuntimeException("Error while getting latest payouts count.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        } 
        return count;
	}
	
	/**
	 * Retrieve details for a specific manual deposit
	 *  
	 * @param transactionId
	 * @return
	 */
	public static WireTransferBean getWireTransferDeposit(Long transactionId){
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = conn.prepareStatement(
					"SELECT payment_transactions.amount, users.username," +
					" user_balance.balance," +
					" ps_user_account, ps_reference_no, payment_transactions.comment" +
					" FROM payment_transactions" +
					" INNER JOIN users ON users.user_id = payment_transactions.user_id " +
					" INNER JOIN user_balance ON user_balance.user_id = users.user_id" +
					" WHERE payment_transactions.transaction_id = ?");

			stmt.setLong(1, transactionId);

			rs = stmt.executeQuery();

			if (rs.next()){
				WireTransferBean currRow = new WireTransferBean();
				currRow.setUsername(rs.getString("username"));
				currRow.setBalance(rs.getDouble("balance"));
				currRow.setAmount(rs.getDouble("amount"));
				currRow.setAccountName(rs.getString("ps_user_account"));
				currRow.setAccountNo(rs.getString("ps_reference_no"));
				currRow.setComment(rs.getString("comment"));
				return currRow;
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error getting user and transaction details.",e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return null;
	}

	/**
	 * Retrieve details for a specific manual withdrawal. 
	 * 
	 * @param transactionId
	 * @return
	 */
	public static WireTransferBean getWireTransferWithdrawal(Long transactionId){
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		WireTransferBean currRow = null;
		
		try {
			stmt = conn.prepareStatement(
					"SELECT payout_transactions.amount, users.username," +
					" user_balance.balance," +
					" ps_user_account, ps_reference_no, payout_transactions.comment" +
					" FROM payout_transactions" +
					" INNER JOIN users ON users.user_id = payout_transactions.user_id " +
					" INNER JOIN user_balance ON user_balance.user_id = users.user_id" +
					" WHERE payout_transactions.payout_id = ?");

			stmt.setLong(1, transactionId);

			rs = stmt.executeQuery();

			if (rs.next()){
				currRow = new WireTransferBean();
				currRow.setUsername(rs.getString("username"));
				currRow.setBalance(rs.getDouble("balance"));
				currRow.setAmount(rs.getDouble("amount"));
				currRow.setAccountName(rs.getString("ps_user_account"));
				currRow.setAccountNo(rs.getString("ps_reference_no"));
				currRow.setComment(rs.getString("comment"));
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error getting user and payout transaction details.",e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return currRow;
	}

	/**
	 * Update details for a specific deposit
	 * 
	 * @param transferComment
	 * @param accountName
	 * @param accountNo
	 * @param transactionId
	 * @throws ServiceException 
	 */
	public static void updateWireTransferDeposit(String transferComment,String accountName,String referenceNo,Long transactionId) throws ServiceException{
		String comment = transferComment;
		if (comment != null && comment.trim().isEmpty()) {
			comment = null;
		}
		
		Connection dbConn = ConnectionFactory.getConnection();

		if (dbConn == null){
			throw new ServiceException("Out of database connections");
		}

		try
		{		

			PreparedStatement statement = null;

			try {
				String sql = "UPDATE payment_transactions" +
				" SET ps_user_account = ?, ps_reference_no = ?, comment = ?" +
				"WHERE transaction_id = ?";

				int i = 1;

				statement = dbConn.prepareStatement(sql);
				statement.setString(i++,accountName);
				statement.setString(i++, referenceNo);
				statement.setString(i++, comment);
				statement.setLong(i, transactionId);

				statement.execute();

			} finally {
				DbUtil.close(statement);
			}		

		} catch (SQLException e){
			throw new ServiceException("Error while editing deposit transaction details.",e);
		} finally{
			DbUtil.close(dbConn);
		}
	}
	
	/**
	 * Edit details for a specific withdrawal
	 * 
	 * @param transferComment
	 * @param accountName
	 * @param accountNo
	 * @param transactionId
	 */
	public static void updateWireTransferWithdrawal(String transferComment,String accountName,String referenceNo,Long transactionId){
		Connection dbConn = ConnectionFactory.getConnection();

		if (dbConn == null){
			return;
		}

		try
		{		
			String comment = transferComment;
			if (comment != null && comment.equals("")){
				comment = null;
			}

			PreparedStatement statement = null;

			try {
				String sql = "UPDATE payout_transactions" +
				" SET ps_user_account = ?, ps_reference_no = ?, comment = ?" +
				"WHERE payout_id = ?";

				int i = 1;

				statement = dbConn.prepareStatement(sql);
				statement.setString(i++,accountName);
				statement.setString(i++, referenceNo);
				statement.setString(i++, comment);
				statement.setLong(i, transactionId);

				statement.execute();

			} finally {
				DbUtil.close(statement);
			}		

		} catch (SQLException e){
			throw new RuntimeException("Error while editing withdrawal transaction details.",e);
		} finally{
			DbUtil.close(dbConn);
		}
	}
}
