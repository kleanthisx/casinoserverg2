package com.magneta.administration;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.Config;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;

public final class SystemProperties {
    
	private static final Logger log = LoggerFactory.getLogger(SystemProperties.class);
	
    private static final String gamingServerPass;
    private static final String version;
    
    public static final String APP_NAME = "Backoffice";

	static {
		version = readVersion();
		gamingServerPass = Config.get("server.admin.password", "magneta");
    }
	
	private static String readVersion() {
        String versionNo = "";
        try {
        	InputStream is = SystemProperties.class.getClassLoader().getResourceAsStream("../../META-INF/MANIFEST.MF");
        	if (is != null) {
        		Manifest mf = new Manifest();
        		mf.read(is);
        		Attributes atts = mf.getMainAttributes();
        		versionNo = atts.getValue("Implementation-Build");
        	}
        } catch (IOException e){
        	log.error("Error reading manifest.");
        }
        
        return versionNo;
	}
    
    public static String getGamingServerPassword(){ 
    	return gamingServerPass;
    }

    public static String getBuildNo() {
    	return version;
    }
    
    public static boolean globalCorporateJackpot() throws ServiceException{
    	SettingsService service = ServiceLocator.getService(SettingsService.class);
		
		return service.getSettingValue("system.global_corporate_jackpot", Boolean.TYPE);
    }
    
    public static String getSingleCountry() throws ServiceException{
    	SettingsService service = ServiceLocator.getService(SettingsService.class);
		
		return service.getSettingValue("system.single_country", String.class);
    }
}
