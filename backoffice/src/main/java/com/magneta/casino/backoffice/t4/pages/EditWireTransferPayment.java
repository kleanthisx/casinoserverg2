package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.WireTransferBean;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.administration.utils.DepositsWithdrawalsUtils;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class EditWireTransferPayment extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditWireTransferPayment.class);
	
	@Persist("client")
	public abstract ICallback getCallback();
	public abstract void setCallback(ICallback callback);

	@Persist("client:page")
	public abstract Long getTransactionId();
	public abstract void setTransactionId(Long id);
	
    @Component
	public abstract ErrorDisplay getErrorDisplay();

	public abstract String getUsername();
	public abstract void setUsername(String username);

	public abstract double getBalance();
	public abstract void setBalance(double balance);

	public abstract double getAmount();
	public abstract void setAmount(double amount);

	public abstract String getAccountName();
	public abstract void setAccountName(String accName);

	public abstract String getAccountNo();
	public abstract void setAccountNo(String accNo);

	public abstract String getComment();
	public abstract void setComment(String comment);
	
	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","clientValidationEnabled=true"})
	public abstract Form getEditForm();

	@Component(bindings={"value=accountName","disabled=!allowWrite()","displayName=message:NameOnAccount","validators=validators:maxLength=256"})
	public abstract TextField getChequeName();

	@Component(bindings={"value=accountNo","disabled=!allowWrite()","displayName=message:AccountNo","validators=validators:maxLength=128"})
	public abstract TextField getChequeNo();

	@Component(id="comment",
			bindings={"value=comment","disabled=!allowWrite()","displayName=message:Comment","validators=validators:maxLength=255"})
			public abstract TextArea getCommentText();

	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();

	@Component
	public abstract Button getCancelButton();

	@Component(id="balance",bindings={"value=formatAmount(balance)"})
	public abstract Insert getBalanceInsert();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null) && (params.length > 0)) {
			setTransactionId((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent pEvt) {
		if (getTransactionId() == null) {
			throw new BadRequestException("Invalid/missing transactionId");
		}
		
		WireTransferBean currentTransaction = DepositsWithdrawalsUtils.getWireTransferDeposit(getTransactionId());
		if (currentTransaction == null) {
			throw new BadRequestException("Invalid/missing transactionId");
		}
		
		setUsername(currentTransaction.getUsername());
		setBalance(currentTransaction.getBalance());
		setAmount(currentTransaction.getAmount());
		setAccountName(currentTransaction.getAccountName());
		setAccountNo(currentTransaction.getAccountNo());
		setComment(currentTransaction.getComment());
	}
	
	public void onSubmit(IRequestCycle cycle) {
		if (getTransactionId() == null) {
			throw new BadRequestException("Invalid/missing transactionId");
		}
		
		try {
			DepositsWithdrawalsUtils.updateWireTransferDeposit(getComment(), getAccountName(), getAccountNo(), getTransactionId());
		} catch (ServiceException e) {
			log.error("Unable to update manual deposit", e);
			this.getErrorDisplay().getDelegate().record(null, "Error updating deposit: " + e.getMessage());
			return;
		}

		if(getCallback() == null) {
			redirectToPage("PaymentsCompleted");
		} else {
			getCallback().performCallback(cycle);
		}
	}

	public void onCancel(IRequestCycle cycle){
		if(getCallback() == null) {
			redirectToPage("PaymentsCompleted");
		} else {
			getCallback().performCallback(cycle);
		}
	}

}
