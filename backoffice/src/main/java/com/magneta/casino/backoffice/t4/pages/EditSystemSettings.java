package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.SettingSection;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class EditSystemSettings extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditSystemSettings.class);

	@InjectObject("service:casino.common.Settings")
	public abstract SettingsService getSettingsService();

	public abstract void setSettingsSections(List<SettingSection> settingsSection);
	public abstract List<SettingSection> getSettingsSections();

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract SettingSection getCurrSection();
	public abstract SettingSection getCurrSubSection();
	public abstract SettingBean getCurrSetting();
	
	public void setCurrPercentage(String value) {
		if (value == null || value.trim().isEmpty()) {
			this.getCurrSetting().setValue(null);
		}
		
		if (value != null) {
			Double val = Double.parseDouble(value) / 100.0;
			this.getCurrSetting().setValue(val.toString());
		}
	}
	
	public String getCurrPercentage() {
		String value = this.getCurrSetting().getValue();
		
		if (value == null || value.trim().isEmpty()) {
			return null;
		}

		Double val = Double.parseDouble(value) * 100.0;
		return val.toString();
	}

	@Component(id="editSettingsForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getEditSettingsForm();

	@Component(id="sections", type="For",
			bindings={"source=settingsSections","value=currSection","renderTag=true"})
	public abstract ForBean getSections();

	@Component(id="subSections", type="For",
			bindings={"source=currSection.subSections","value=currSubSection","renderTag=true"})
	public abstract ForBean getSubSections();

	@Component(id="settings", type="For",
			bindings={"source=currSubSection.settings","value=currSetting","element=literal:tr"})
	public abstract ForBean getSettingsFor();
	
	@Component(id="updateSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getUpdateSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void pageBeginRender(PageEvent event) {
		getSettingsService().invalidate();
		List<SettingBean> serviceSettings;
		try {
			ServiceResultSet<SettingBean> result = getSettingsService().getSettings(null, new SortContext<SettingBean>("settingSection,settingSubsection,key"), 0, 0);

			serviceSettings = result.getResult();
		} catch (ServiceException e) {
			log.error("Error while retrieving settings", e);
			getErrorDisplay().getDelegate().record(null, getMsg("db-error"));
			return;
		}

		List<SettingSection> settingsSections = new ArrayList<SettingSection>();

		SettingSection currSection = null;
		SettingSection currSubSection = null;

		for (SettingBean s: serviceSettings) {
			String section = s.getSettingSection();
			String subSection = s.getSettingSubsection();

			if (section == null) {
				section = "";
			}

			if (subSection == null) {
				subSection = "";
			}

			if (currSection == null || !section.equals(currSection.getName())) {
				currSection = new SettingSection();
				currSection.setName(section);

				currSection.setSettings(new ArrayList<SettingBean>());
				currSection.setSubSections(new ArrayList<SettingSection>());

				settingsSections.add(currSection);

				currSubSection = null;
			}

			if (currSubSection == null || !subSection.equals(currSubSection.getName())) {
				currSubSection = new SettingSection();
				currSubSection.setName(subSection);
				currSubSection.setSettings(new ArrayList<SettingBean>());

				currSection.getSubSections().add(currSubSection);
			}

			currSubSection.getSettings().add(new SettingBean(s));
		}

		setSettingsSections(settingsSections);
	}

	public void onSubmit(IRequestCycle cycle) throws ServiceException {
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		getSettingsService().invalidate();
		Connection dbConn = ConnectionFactory.getConnection();
		try {
			dbConn.setAutoCommit(false);

			List<SettingSection> sections = this.getSettingsSections();

			for (SettingSection s: sections) {
				for (SettingBean setting : s.getSettings()) {
					update(dbConn, setting);
				}

				for (SettingSection sub: s.getSubSections()) {
					for (SettingBean setting: sub.getSettings()) {
						update(dbConn, setting);
					}
				}
			}

			dbConn.commit();

		} catch (SQLException e) {
			try {
				dbConn.rollback();
			} catch (SQLException e1) {
				log.error("Error rolling back.",e1);
			}
			delegate.record(null, getMsg("db-error"));
			log.error("Error updating system settings.",e);
			return;
		} finally {
			DbUtil.close(dbConn);
		}


		if (!delegate.getHasErrors()) {
			this.getSettingsService().invalidate();
			redirectHome();
		}
	}

	public void onCancel(IRequestCycle cycle){
		redirectHome();
	}

	public void update(Connection conn, SettingBean bean) throws SQLException, ServiceException{

		if (conn == null) {
			return;
		}

		SettingBean setting = getSettingsService().getSetting(bean.getKey());
		
		String newVal = bean.getValue();
		String oldVal = setting.getValue();

		if (newVal == oldVal) {
			return;
		}

		if (newVal != null && newVal.equals(oldVal)) {
			return;
		}
		
		log.info("Updating setting {} from {} to {}", new Object[] {setting.getKey(), oldVal, newVal});

		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			String sql = 
					"SELECT change_system_setting(?,?,?)";
			statement = conn.prepareStatement(sql);

			statement.setString(1, setting.getKey());
			statement.setString(2, newVal);
			statement.setLong(3, getPrincipalService().getPrincipal().getUserId());

			rs = statement.executeQuery();
			rs.next();

		} finally {
			DbUtil.close(rs);
			DbUtil.close(statement);
		}
	}
}
