package com.magneta.casino.backoffice.components;

import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.pages.GameImage;
import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.poker.VideoPokerActionFilter;
import com.magneta.casino.common.games.filters.poker.VideoPokerPlusActionFilter;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.ServiceException;
import com.magneta.games.configuration.poker.PokerConfiguration;

public class VideoPokerConfigView {
	
	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@Inject
	private GameActionFilterService gameActionFilterService;
		
	@Parameter(required=true)
	private Integer gameId;
	
	@Parameter(required=true)
	private PokerConfiguration configuration;

	@Cached
	public ActionFilter getActionFilter() throws ServiceException {
		return gameActionFilterService.getFilter(gameId, configuration);
	}
	
	@Property
	private ComboSymbolBean currSymbol;
	
	@Property
	private GameComboBean combo;
	
	@Cached
	public GameComboBean[] getPaytable() throws ServiceException {
		if (this.getActionFilter() instanceof VideoPokerActionFilter) {
			return ((VideoPokerActionFilter)getActionFilter()).getPaytable();
		}

		return ((VideoPokerPlusActionFilter)getActionFilter()).getPaytable();
	}
	
	public Link getComboImageUrl() {
		return this.pageRenderLinkSource.createPageRenderLinkWithContext(GameImage.class, new Object[] {gameId, currSymbol.getImage()});		
	}
}
