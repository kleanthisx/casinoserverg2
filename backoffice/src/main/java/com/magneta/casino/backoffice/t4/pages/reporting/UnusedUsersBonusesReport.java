package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.administration.commons.models.CountrySelectionModel;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class UnusedUsersBonusesReport extends DateReportOptions {

		@Component(id = "username",
				bindings = {"value=username", "disabled=false","displayName=message:Username"})
		public abstract TextField getUsernameField();

		@Component(id = "usernameLabel",
				bindings = {"field=component:username"})
		public abstract FieldLabel getUsernameLabel();
		
		@Component(id = "country",
				bindings = {"value=country","model=ognl:countrySelectionModel","displayName=message:Country"})
		public abstract PropertySelection getCountrySelection();

		public CountrySelectionModel getCountrySelectionModel() throws ServiceException{
			return new CountrySelectionModel();
		}
		
		@Component(id = "countryLabel", type = "FieldLabel", bindings = {"field=component:country"})
		public abstract FieldLabel getCountryLabel();
		
		@InitialValue("-1")
		public abstract String getCountry(); 
		public abstract String getUsername();
		
		@Override
		public void fillReportParameters(Map<String,Object> reportParams) {
			super.fillReportParameters(reportParams);
			reportParams.put("username", getUsername());
			reportParams.put("country", getCountry());
		}
}
