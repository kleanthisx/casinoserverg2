package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ExternalCallback;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.SystemProperties;
import com.magneta.administration.Util;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.GameTableTableModel;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.xmlrpc.client.SocketXmlRpcClient;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class AvailableTables extends SecurePage implements PageBeginRenderListener {

	private final static Logger log = LoggerFactory.getLogger(AvailableTables.class);

	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();

	@InjectPage("GeneralConfirmation")
	public abstract GeneralConfirmation getConfirmPage();

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract GameTableBean getCurrTable();

	@Component(type="contrib:Table",
			bindings={"source=ognl:gameTableTableModel","columns=columns","pageSize=50",
			"rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:Id","row=currTable","initialSortOrder=true"})
	public abstract Table getTables();

	public GameTableTableModel getGameTableTableModel(){
		return new GameTableTableModel(getGameId());
	}

	@Component(bindings={"listener=listener:closeTable","parameters={gameId, currTable.tableId, currTable.serverId}"})
	public abstract DirectLink getCloseTableLink();

	@Component(bindings={"page=literal:GameTable","parameters=currTable.tableId"})
	public abstract CasinoLink getEditTableLink();

	@Component(bindings={"page=literal:ViewRound","parameters={currTable.tableId,0}"})
	public abstract CasinoLink getRoundsLink();

	@Component(bindings={"value=currTable.Id"})
	public abstract Insert getTableId();

	@Component(bindings={"page=literal:Games"})
	public abstract CasinoLink getGamesLink();

	@Component(bindings={"page=literal:ClosedTables", "parameters=gameId"})
	public abstract CasinoLink getViewClosedLink();


	@Component(bindings={"userId=ognl:currTable.owner","target=literal:_blank"})
	public abstract UserLink getOwnerLink();


	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		
		if ((params != null) && (params.length > 0)) {    	
			setGameId((Integer)params[0]);
		}
	}

	@Override
	public void pageBeginRender(PageEvent event) {
		if (getGameId() <= 0) {
			throw new BadRequestException("Invalid gameId");
		}

		try {
			GameBean game = getGamesService().getGame(getGameId());

			if(game == null){
				return;
			}

			setGameName(game.getName());

		} catch (ServiceException e) {
			log.error("Error while loading game info");
		}
	}

	@Persist("client:page")
	public abstract int getGameId();
	public abstract void setGameId(int id);

	public abstract void setGameName(String gameName);

	public static class CloseTableConfirmContext extends ConfirmContext {
		private static final long serialVersionUID = 2746595533334782555L;
		private final int serverId;
		private final Long tableId;

		public CloseTableConfirmContext(String okMsg, String cancelMsg, String confirmMsg, ICallback callback,int serverId,Long tableId) {
			super(okMsg, cancelMsg, confirmMsg, callback);
			this.serverId = serverId;
			this.tableId = tableId;
		}

		@Override
		public void onOk(IRequestCycle cycle, ValidationDelegate delegate) throws ServiceException {
			Connection dbConn = ConnectionFactory.getConnection();
			PreparedStatement statement = null;
			ResultSet res = null;
			String serverAddress = null;
			int serverPort = -1;

			if (dbConn == null){
				delegate.record(null, "No database connection");
				return;
			}

			try {
				String sql = 
						"SELECT server_id, server_local_address, server_local_port"+
								" FROM servers" +
								" WHERE server_id = ?";

				statement = dbConn.prepareStatement(sql);
				statement.setInt(1, serverId);

				res = statement.executeQuery();

				while (res.next()){
					serverAddress = res.getString("server_local_address");
					serverPort = res.getInt("server_local_port");
				}

			} catch (SQLException e) {
				delegate.record(null, "Server Error");
				return;
			} finally{
				DbUtil.close(res);
				DbUtil.close(statement);
				DbUtil.close(dbConn);
			}

			SocketXmlRpcClient client = new SocketXmlRpcClient(serverAddress, serverPort, false);
			Object[] args = {
					SystemProperties.getGamingServerPassword(),
					Util.getBytes(tableId),
					serverId
			};

			try {
				if((Boolean)client.execute("Admin.closeTable", args)) {
					returnToPage(cycle);
					return;
				}
			} catch (XmlRpcException e) {
				log.error("Error communicating with server at "+serverAddress+":"+serverPort, e);
			} finally {
				client.destroy();
			}
			delegate.record(null, "Error communicating with server");
			return;
		}

	}

	public void closeTable(IRequestCycle cycle,int gameId,Long tableId,int serverId) {
		ConfirmContext confirmContext = new CloseTableConfirmContext(this.getMsg("ok"), this.getMsg("cancel"), this.getMsg("confirmMessage") + " " + tableId + " ?",new ExternalCallback(this, new Object[] {gameId}),serverId,tableId);
		GeneralConfirmation confirmPage = getConfirmPage();
		confirmPage.setConfirmContext(confirmContext);
		getRequestCycle().activate(confirmPage);
	}

	public String getColumns() { 
		String columns = "TableId, Owner";

		if (getPrincipal().hasPrivilege(PrivilegeEnum.TABLES, true)) {
			columns += ", !edit, !close";
		}
		return columns;
	}
}