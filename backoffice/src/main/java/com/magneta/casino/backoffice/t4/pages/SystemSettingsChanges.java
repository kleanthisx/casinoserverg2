package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.beans.SystemSettingChangeBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.SystemSettingsChangesTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class SystemSettingsChanges extends SecurePage{
	
	public abstract SystemSettingChangeBean getCurrChange();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:systemSettingsChangesTableModel","columns=literal:Date, UserId, !SettingName, !OldValue, !NewValue",
			"pageSize=50","initialSortColumn=literal:Date","row=currChange","initialSortOrder=true"})
	public abstract Table getLogTable();
	
    @Component(bindings={"userId=ognl:currChange.userId","target=literal:_blank"})
    public abstract UserLink getUserIdLink();
	
	public SystemSettingsChangesTableModel getSystemSettingsChangesTableModel(){
		return new SystemSettingsChangesTableModel();
	}
	
	@Component(bindings={"value=formatDate(currChange.Date)"})
	public abstract Insert getDate();
	
}
