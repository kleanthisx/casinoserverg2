package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.PageLink;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.models.PaymentTypeTableModel;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class PaymentTypes extends SecurePage {

	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract PaymentTypeBean getCurrType();
	
	@InjectObject("service:casino.common.PaymentType")
	public abstract PaymentTypeService getPaymentTypeService();
	
	@Component(id="types",type="contrib:Table",
			bindings={"source=ognl:paymentTypeTableModel","columns=columns","pageSize=20","rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:paymentType",
			"row=currType","initialSortOrder=false"})
	public abstract Table getPaymentTypesTable();
	
	@Component(bindings={"page=literal:EditPaymentType","parameters=currType.paymentType"})
	public abstract CasinoLink getEditLink();
	
    @Component(id="createTypeLink",
			bindings={"page=literal:CreatePaymentType"})
	public abstract PageLink getCreatePaymentType();
	
	public PaymentTypeTableModel getPaymentTypeTableModel(){
		return new com.magneta.casino.backoffice.t4.models.PaymentTypeTableModel();
	}

	public String getPaymentTypeName(){
		return getMsg(getCurrType().getPaymentType());
	}
	
	public String getColumns() {
		return "PaymentType, Weight, Enabled,!edit";
	}
}
