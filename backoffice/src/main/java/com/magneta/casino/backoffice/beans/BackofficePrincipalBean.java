package com.magneta.casino.backoffice.beans;

import java.io.Serializable;
import java.util.TimeZone;

import com.magneta.casino.internal.beans.ExtendedPrincipalBeanImpl;
import com.magneta.casino.services.beans.UserAuthenticationBean;

public class BackofficePrincipalBean extends ExtendedPrincipalBeanImpl implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3904154384649179866L;
	private TimeZone tz;
	
    public BackofficePrincipalBean() {
    }

    public BackofficePrincipalBean(BackofficePrincipalBean user) {
        super(user);
        this.tz = user.getTimeZone();
    }

    public BackofficePrincipalBean(UserAuthenticationBean authenticationBean) {
        super(authenticationBean);
    }
    
    public TimeZone getTimeZone() {
    	return tz;
    }
    
    public void setTimeZone(TimeZone timeZone) { 
    	this.tz = timeZone;
    }
    
    @Override
	public Object clone() {
    	return new BackofficePrincipalBean(this);
    }
}
