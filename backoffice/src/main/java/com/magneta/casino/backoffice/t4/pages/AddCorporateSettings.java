package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class AddCorporateSettings extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(AddCorporateSettings.class);
	
	@InjectPage("CorporateSettings")
	public abstract CorporateSettings getCorporateSettingsPage(); 

	@InitialValue("0")
	@Persist("client:page")
	public abstract Long getUserID();
	public abstract void setUserID(Long userID);

	public abstract void setUsername(String username);
	public abstract String getUsername();

	public abstract double getMinBet();
	public abstract double getMaxBet();
	public abstract int getGameID();
	public abstract void setGameName(String gameName);

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();

	@Component(bindings={"value=gameID","model=ognl:gamesSelectionModel","disabled=!allowWrite()"})
	public abstract PropertySelection getGame();

	public GamesSelectionModel getGamesSelectionModel() {
		return new GamesSelectionModel(false);
	}

	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getEditGameForm();

	@Component(bindings={"value=minBet","translator=bean:numTranslator","validators=validators:$minAmount","disabled=!allowWrite()"})
	public abstract TextField getMinBetField();

	@Component(bindings={"value=maxBet","translator=bean:numTranslator","validators=validators:$minAmount","disabled=!allowWrite()"})
	public abstract TextField getMaxBetField();

	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getUpdateSubmit();

	@Component
	public abstract Button getCancelButton();

	@Override
	public boolean pageNeedsWriteAccess() {
		return true;
	}

	@Override
	public void pageBeginRender(PageEvent evt){
		setUsername(getUsername(getUserID()));
	}

	public void onSubmit(IRequestCycle cycle){

		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		if(getMinBet()<=0 && getMaxBet()<=0) {
			delegate.record(null, getMsg("both-empty"));
			return;
		}
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;

		if (dbConn == null){
			delegate.record(null, getMsg("db-error"));
			return;
		}

		String sql = 
			"INSERT INTO corporate_game_settings(corporate_id, game_id, min_bet, max_bet)" +
			" VALUES (?,?,?,?)";

		try {
			statement = dbConn.prepareStatement(sql);

			statement.setLong(1, getUserID());
			statement.setInt(2, getGameID());

			if (getMinBet() > 0.0){
				statement.setBigDecimal(3, new BigDecimal(getMinBet()));
			} else {
				statement.setObject(3, null);
			}

			if (getMaxBet() > 0.0){
				statement.setBigDecimal(4, new BigDecimal(getMaxBet()));
			} else {
				statement.setObject(4, null);
			}

			statement.execute();
		} catch (SQLException e) {
			log.error("Errow while updating corporate game properties.", e);
			if (e.getMessage().toLowerCase().contains("pk_corporate_game_settings")){
				delegate.record(null, getMsg("game-settings-exist"));
			} else {
				delegate.record(null, getMsg("db-error"));
			}
			return;
		} finally {
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
		

		CorporateSettings settings = getCorporateSettingsPage();
		settings.setUserID(getUserID());
		settings.setUsername(getUsername());
		cycle.activate(settings);
	}

	public void onCancel(IRequestCycle cycle){
		CorporateSettings settings = getCorporateSettingsPage();
		settings.setUserID(getUserID());
		cycle.activate(settings);
	}

}
