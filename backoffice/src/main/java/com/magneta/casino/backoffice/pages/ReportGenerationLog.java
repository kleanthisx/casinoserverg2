package com.magneta.casino.backoffice.pages;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.ReportGenerationLogGridDataSource;
import com.magneta.casino.backoffice.selectmodels.ApplicationSelectModel;
import com.magneta.casino.backoffice.selectmodels.GeneratedReportSelectModel;
import com.magneta.casino.services.ReportGenerationLogService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.ReportGenerationLogBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class ReportGenerationLog {

	@Inject
	private ReportGenerationLogService reportGenerationLogService;
	
	@Property
	private ReportGenerationLogBean reportLog;
	
	@Property
	private String username;
	
	@Property
	private String ipAddress;
	
	@Property
	private String application;
	
	@Property
	private String reportName;
	
	
	@Property
	private Date fromDate;
	
	@Property
	private Date toDate;
	
	@OnEvent(EventConstants.ACTIVATE)
	public void activate(String username, String ipAddress, String application, String reportName, Date fromDate, Date toDate) {
		this.username = username;
		this.ipAddress = ipAddress;
		this.application = application;
		this.reportName = reportName;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Object[] passivate() {
		return new Object[] {username, ipAddress, application, reportName, fromDate, toDate};
	}
	
	public ApplicationSelectModel getApplicationSelectModel() {
		return new ApplicationSelectModel();
	}
	
	public GeneratedReportSelectModel getGeneratedReportSelectModel() {
		return new GeneratedReportSelectModel();
	}
	
	public ReportGenerationLogGridDataSource getDataSource() throws ServiceException {
		SearchContext<UserDetailsBean> userSearchContext = FiqlContextBuilder.create(UserDetailsBean.class, "userName~=?", username); 
		
		List<UserDetailsBean> users = userDetailsService.getUsers(userSearchContext, 1, 0, null).getResult();

		SearchContext<ReportGenerationLogBean> searchContext = null;
		
		if (!users.isEmpty()) {
			searchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "userId==?", users.get(0).getId());
		}
		
		if (application != null) {
			SearchContext<ReportGenerationLogBean> appSearchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "applicationName==?", application);
			
			if (searchContext == null) {
				searchContext = appSearchContext;
			} else {
				searchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "?;?", searchContext, appSearchContext);
			}
		}
		
		if (reportName != null) {
			SearchContext<ReportGenerationLogBean> appSearchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "reportName==?", reportName);
			
			if (searchContext == null) {
				searchContext = appSearchContext;
			} else {
				searchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "?;?", searchContext, appSearchContext);
			}
		}
		
		if (ipAddress != null) {
			SearchContext<ReportGenerationLogBean> appSearchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "clientIp==?*", ipAddress);
			
			if (searchContext == null) {
				searchContext = appSearchContext;
			} else {
				searchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "?;?", searchContext, appSearchContext);
			}
		}
		
		if (fromDate != null) {
			SearchContext<ReportGenerationLogBean> appSearchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "fromDate=ge=", fromDate);
			
			if (searchContext == null) {
				searchContext = appSearchContext;
			} else {
				searchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "?;?", searchContext, appSearchContext);
			}
		}
		
		if (toDate != null) {
			SearchContext<ReportGenerationLogBean> appSearchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "toDate=le=", fromDate);
			
			if (searchContext == null) {
				searchContext = appSearchContext;
			} else {
				searchContext = FiqlContextBuilder.create(ReportGenerationLogBean.class, "?;?", searchContext, appSearchContext);
			}
		}
		
		return new ReportGenerationLogGridDataSource(searchContext, reportGenerationLogService);
	}
	
	@Inject
	private UserDetailsService userDetailsService;
	
	public List<String> onProvideCompletionsFromUsername(String partial) throws ServiceException {
		SearchContext<UserDetailsBean> searchContext = FiqlContextBuilder.create(UserDetailsBean.class, "userName~=?*", partial); 
		
		List<UserDetailsBean> userList = userDetailsService.getUsers(searchContext, 20, 0, new SortContext<UserDetailsBean>("userName")).getResult();
		
		List<String> usernames = new ArrayList<String>(userList.size());
		
		for (UserDetailsBean user: userList) {
			usernames.add(user.getUserName());
		}
		
		return usernames;
	 }
	
	@OnEvent(value=EventConstants.SUCCESS, component="reportLogForm")
	public void onSubmit() {
		
	}
}
