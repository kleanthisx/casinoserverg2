/**
 * 
 */
package com.magneta.casino.backoffice.t4.models;

import java.util.ArrayList;
import java.util.List;
import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.casino.services.enums.UserTypeEnum;

public class PlayerTypesSelectionModel implements IPropertySelectionModel {

	private final List<Integer> userTypes;
	private final List<String> userTypeDescs;

	public PlayerTypesSelectionModel(boolean anyOption) {
		userTypes = new ArrayList<Integer>();
		userTypeDescs = new ArrayList<String>();

		if (anyOption){
			userTypes.add(-1);
			userTypeDescs.add("All");
		}
		userTypes.add(UserTypeEnum.NORMAL_USER.getId());
		userTypeDescs.add("Normal Users");
		userTypes.add(UserTypeEnum.CORPORATE_PLAYER.getId());
		userTypeDescs.add("Corporate Users");
	}


	@Override
	public String getLabel(int arg0) {
		return userTypeDescs.get(arg0);
	}

	@Override
	public Object getOption(int arg0) {
		return userTypes.get(arg0);
	}

	@Override
	public int getOptionCount() {
		return userTypes.size();
	}

	@Override
	public String getValue(int arg0) {
		return userTypes.get(arg0).toString();
	}

	@Override
	public Object translateValue(String arg0) {
		return arg0;
	}

	@Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
