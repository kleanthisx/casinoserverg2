package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class UserBalanceReport extends DateReportOptions {

	@Component(id = "username",
			bindings = {"value=username", "disabled=false","displayName=message:Username"})
			public abstract TextField getUsernameField();

	@Component(id = "usernameLabel",
			bindings = {"field=component:username"})
			public abstract FieldLabel getUsernameLabel();

	@Component(id = "country",
			bindings = {"value=country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel()", 
	"displayName=message:Country"})
	public abstract PropertySelection getCountrySelection();

	@Component(id = "countryLabel", 
			bindings = {"field=component:country"})
			public abstract FieldLabel getCountryLabel();

	@Component(id = "minBalance",
			bindings = {"value=minBalance", "disabled=false","displayName=message:minBalance"})
			public abstract TextField getMinBalanceField();

	@Component(id = "minBalanceLabel",
			bindings = {"field=component:minBalance"})
			public abstract FieldLabel getMinBalanceLabel();

	@Component(id = "minCredits",
			bindings = {"value=minCredits", "disabled=false","displayName=message:minCredits"})
			public abstract TextField getMinCreditsField();

	@Component(id = "minCreditsLabel",
			bindings = {"field=component:minCredits"})
			public abstract FieldLabel getMinCreditsLabel();

	@Component(id = "showRestrictedCB",
			bindings={"value=showRestricted", "displayName=message:showRestricted"})
			public abstract Checkbox getShowRestrictedCB();

	@Component(id = "showRestrictedCBLabel",
			bindings={"field=component:showRestrictedCB"})
			public abstract FieldLabel getShowRestrictedCBLabel();

	public abstract boolean getShowRestricted();

	public abstract Integer getMinBalance();
	public abstract Integer getMinCredits();

	public abstract String getCountry(); 
	public abstract String getUsername();

	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);

		if (getMinBalance() != null && getMinBalance()!=0)
			reportParams.put("balance", getMinBalance());
		if (getMinBalance() != null && getMinCredits()!=0)
			reportParams.put("credits", getMinCredits());
		if (getCountry() != null)
			reportParams.put("country", getCountry());

		reportParams.put("show_restricted", getShowRestricted());
		reportParams.put("username", getUsername());
	}
}
