package com.magneta.casino.backoffice.t4.pages.reporting;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class ClientAccountsPeriodReport extends DateReportOptions {

}
