package com.magneta.casino.backoffice.mock.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.magneta.administration.beans.UserTransactionBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class LastUserTransactionsMockUpService {

	
	public List<UserTransactionBean> getLastestTransactions(int limit,Long userId) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;

		if (dbConn == null){
			return null;
		}
		
		try
		{   
			String sql = 
				"SELECT transaction_id, trans_type, amount, time_completed, ps_specific"+
				" FROM (SELECT user_id, transaction_id, 1 AS trans_type, amount, time_completed, ps_specific"+
				"       FROM payment_transactions"+
				" UNION ALL"+
				" SELECT user_id, payout_id, 2 AS trans_type, amount, time_completed, ps_specific"+
				" FROM payout_transactions"+
				" UNION ALL"+
				" SELECT user_id, adjustment_id, 4 AS trans_type, adjustment_amount, adjustment_date, 'Adjustment'" +
				" FROM user_balance_adjustments" +
				" ) AS last_transactions"+
				" WHERE user_id = ?"+
				" ORDER BY time_completed DESC"+
				(limit > 0 ? " LIMIT ?" : "");

			statement = dbConn.prepareStatement(sql);
			
			statement.setLong(1,userId);
			
			if(limit > 0){
				statement.setInt(2, limit);
			}

			res = statement.executeQuery();

			ArrayList<UserTransactionBean> myArr = new ArrayList<UserTransactionBean>();
			UserTransactionBean currRow;

			while (res.next()){
				currRow = new UserTransactionBean();
				currRow.setTransactionId(res.getLong("transaction_id"));
				currRow.setAmount(res.getDouble("amount"));
				currRow.setTransactionType(res.getInt("trans_type"));
				currRow.setPaymentMethod(res.getString("ps_specific"));
				currRow.setDateCompleted(DbUtil.getTimestamp(res, "time_completed"));
				myArr.add(currRow);
			}

			return myArr;
		}catch (SQLException e) {
			throw new RuntimeException("Error while retrieving latest completed transactions.",e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
	}
	
	public int getLatestTransactionCount(int limit,Long userId) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {
            String sql = 
                "SELECT COUNT(transaction_id)"+
            	" FROM (SELECT user_id, transaction_id"+
            	" FROM payment_transactions" +
            	" UNION ALL"+
            	" SELECT user_id, payout_id"+
            	" FROM payout_transactions"+
            	" UNION ALL"+
            	" SELECT user_id, adjustment_id FROM user_balance_adjustments" +
            	" ) AS last_transactions"+
            	" WHERE user_id = ?";
            statement = dbConn.prepareStatement(sql);
            
            statement.setLong(1, userId);
            
           
            res = statement.executeQuery();
            
            if (res.next()){
            	if (limit <= 0 || res.getInt(1) <= limit) {
            		count = res.getInt(1);
            	} else {
            		count = limit;
            	}
            }
        }catch (SQLException e) {
            throw new RuntimeException("Error while getting latest completed transactions count.", e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return count;  
	}
}
