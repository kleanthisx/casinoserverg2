package com.magneta.casino.backoffice.pages;

import java.sql.SQLException;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.UsersBlacklistGridDataSource;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsersBlacklistService;
import com.magneta.casino.services.beans.UserBlacklistBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class UsersBlacklist {

	@Property
	private UserBlacklistBean userBlacklistBean;

	@InjectService("UsersBlacklistService")
	private UsersBlacklistService usersBlacklistService;

	@InjectService("CountryService")
	private CountryService countryService;
	
	public GridDataSource getUsersBlacklistGridDataSource(){
		return new UsersBlacklistGridDataSource(usersBlacklistService);
	}
	
	@OnEvent(value=EventConstants.ACTION, component="deleteMessage")
	public void deleteMessage(Long itemId) throws ServiceException, SQLException {
		UserBlacklistBean userBean = usersBlacklistService.getUserBlacklist(itemId);
		usersBlacklistService.deleteBlacklistUser(userBean);
	}
	
	public String getCountryName(String countryCode) throws ServiceException{
		if(countryCode == null){
			return null;
		}
		return countryService.getCountry(countryCode).getName();
	}
}
