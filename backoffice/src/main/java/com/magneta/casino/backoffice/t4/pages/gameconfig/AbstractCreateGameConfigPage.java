package com.magneta.casino.backoffice.t4.pages.gameconfig;

import java.util.Date;

import org.apache.tapestry.IAsset;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.asset.ExternalAsset;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.pages.SecurePage;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public abstract class AbstractCreateGameConfigPage extends SecurePage implements PageBeginRenderListener{
	
	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();
	
	@InjectObject("t5:GameConfigsService")
	public abstract GameConfigsService getGameConfigsService();
	
	@Persist("client:page")
	public abstract int getGame();
	public abstract void setGame(int game);

	public abstract String getGameName();
	public abstract void setGameName(String gameName);
	
	@Persist("client:page")
	public abstract void setConfigID(Long configID);
	public abstract Long getConfigID();
	
	public abstract void setComment(String comment);
	public abstract String getComment();
	
	@Persist("client:page")
	public abstract void setConfig(String comment);
	public abstract String getConfig();

	public abstract void setDate(Date date);
	
	@Override
	public void pageValidate(PageEvent event) {
		super.pageValidate(event);
		if (!getAccessValidator().checkWriteAccess(this.getPageName())) {
			redirectToPage("AccessDenied");
		}
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if (params != null && params.length >= 1) {
			setGame((Integer)params[0]);
			
			if (params.length >= 2) {
				setConfigID((Long)params[1]);
			}
		}
	}
	
	public IAsset getGameImage(String image) {
		return new ExternalAsset(LinkGenerator.getT5Url("GameImage", getGame(), image), null);
	}
	
	@Override
	public void pageBeginRender(PageEvent pEvt){
	
		GameBean game;
		try {
			game = getGamesService().getGame(getGame());
			setGameName(game.getName());
		} catch (ServiceException e) {
			throw new RuntimeException("Unable to load game", e);
		}
		setComment("New Config");
		
		if(getConfigID() != null && getConfigID() > 1) {
			GameConfigBean bean;
			try {
				bean = getGameConfigsService().getGameConfig(getGame(), getConfigID());
			} catch (ServiceException e1) {
				throw new RuntimeException("Unable to load game config", e1);
			}
			
			setConfig(bean.getConfigValue());
			setComment(bean.getConfigComment());
			setDate(bean.getConfigDate());
		}
	}
	
	public void onCancel(IRequestCycle cycle) {
		String url = LinkGenerator.getPageUrl("GameConfigs", getGame());
		throw new RedirectException(url);
	}
}