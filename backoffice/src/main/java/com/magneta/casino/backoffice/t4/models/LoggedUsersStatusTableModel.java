package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.LoggedUserStateBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class LoggedUsersStatusTableModel implements IBasicTableModel {

	private int serverId;

	public LoggedUsersStatusTableModel(int serverId) {
		this.serverId = serverId;
	}

	@Override
	public Iterator<LoggedUserStateBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean ascending) {
		ServersService serversService = ServiceLocator.getService(ServersService.class);
		
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement preparedStatement = null;
		ResultSet res = null;
		Iterator<LoggedUserStateBean> it;

		if (dbConn == null) {
			throw new RuntimeException("Out of database connections");
		}
		
		String sortColumn = null;
		
		if (sortCol != null) {
			sortColumn = sortCol.getColumnName();
		}

		if (("UserId").equals(sortColumn)){
			sortColumn = "user_id";
		}else if (("Server").equals(sortColumn)){
			sortColumn = "server_id";
		} else if (("ClientIp").equals(sortColumn)){
			sortColumn = "client_ip";
		} else if (("ClientVersion").equals(sortColumn)){
			sortColumn = "device_hash_code";
		} else if (("LoginDate").equals(sortColumn)){
			sortColumn = "login_date";
		} else {
			sortColumn = null;
		}
		
		try
		{   
			String sql = 
				"SELECT user_logins.user_id, user_logins.server_id, game_table_users.table_id, game_types.game_type_variant," +
				" MAX(game_rounds.round_id + game_rounds.round_started::int - game_rounds.round_started::int) AS round_playing, MAX(game_rounds.round_id) AS round_id," +
				" user_logins.client_ip,user_logins.device_hash_code,user_logins.login_date" +
				" FROM user_logins" +
				" LEFT OUTER JOIN game_table_users ON user_logins.user_id = game_table_users.user_id" +
				"  AND game_table_users.is_active = TRUE" +
				" LEFT OUTER JOIN game_tables ON game_tables.table_id = game_table_users.table_id" +
				" LEFT OUTER JOIN game_types ON game_tables.game_type_id = game_types.game_type_id" +
				" LEFT OUTER JOIN game_table_last_rounds ON game_tables.table_id=game_table_last_rounds.table_id" +
				" LEFT OUTER JOIN game_rounds ON game_table_users.table_id = game_rounds.table_id" +
				" AND (game_rounds.round_id = last_round_id OR game_rounds.round_id = last_round_id - 1)" +
				" AND (game_rounds.date_closed IS NULL OR age(now_utc(), game_rounds.date_closed) < INTERVAL '30 sec')" +
				" WHERE logout_date IS NULL" +
				((serverId > 0)?" AND user_logins.server_id = ?": "") +
				" GROUP BY user_logins.user_id, user_logins.server_id, game_table_users.table_id, game_types.game_type_variant,user_logins.client_ip,user_logins.device_hash_code,user_logins.login_date";
				
			
			if (sortColumn != null){
				sql += " ORDER BY "+sortColumn;

				if (ascending){
					sql += " ASC";
				} else {
					sql += " DESC";
				}
			}

			sql += " LIMIT ? OFFSET ?";
			
			preparedStatement = dbConn.prepareStatement(sql);
				
			int stmt_index = 1;
			if (serverId > 0){
				preparedStatement.setInt(stmt_index++, serverId);
			}
			preparedStatement.setInt(stmt_index++, limit);
			preparedStatement.setInt(stmt_index, offset);

			res = preparedStatement.executeQuery();

			List<LoggedUserStateBean> myArr = new ArrayList<LoggedUserStateBean>();
			LoggedUserStateBean currRow;

			while(res.next()) {
				currRow = new LoggedUserStateBean();

				currRow.setUserId(res.getLong("user_id"));
				currRow.setServerID(res.getInt("server_id"));
				currRow.setServer(serversService.getServer(currRow.getServerId()).getServerDescription());

				if (res.getObject("round_id") != null){
					if (res.getObject("round_playing") == null){
						currRow.setStatus("Viewing: "+res.getString("game_type_variant")+" "+res.getLong("table_id")+"-"+res.getLong("round_id")+".");
					} else {
						currRow.setStatus("Playing: "+res.getString("game_type_variant")+" "+res.getLong("table_id")+"-"+res.getLong("round_playing")+".");
					}
				} else {
					currRow.setStatus("In Lobby.");
				}

				currRow.setClientIp(res.getString("client_ip"));
				currRow.setClientVersion(res.getString("device_hash_code"));
				currRow.setLoginDate(res.getDate("login_date"));
				
				myArr.add(currRow);
			}

			it = myArr.iterator();
		}catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(preparedStatement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		int count = 0;
		Connection dbConn = ConnectionFactory.getReportsConnection();

		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}

		try
		{
			if (serverId > 0){
				String sql = 
						"SELECT COUNT (DISTINCT (user_logins.user_id))"+
								" FROM user_logins"+
								" WHERE user_logins.server_id = ?"+
								" AND user_logins.logout_date IS NULL";

				PreparedStatement preparedStatement = null;
				ResultSet res = null;

				try {
					preparedStatement = dbConn.prepareStatement(sql);
					preparedStatement.setInt(1, serverId);

					res = preparedStatement.executeQuery();

					if (res.next()){
						count = res.getInt(1);
					}
				} finally {
					DbUtil.close(res);
					DbUtil.close(preparedStatement);
				}
			} else {
				String sql = 
						"SELECT COUNT (DISTINCT (user_logins.user_id))"+
								" FROM user_logins"+
								" WHERE user_logins.logout_date IS NULL";

				Statement statement = null;
				ResultSet res = null;

				try {
					statement = dbConn.createStatement();

					res = statement.executeQuery(sql);

					if (res.next()){
						count = res.getInt(1);
					}
				} finally {
					DbUtil.close(res);
					DbUtil.close(statement);
				}
			}

		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally{
			DbUtil.close(dbConn);
		}

		return count;
	}


}
