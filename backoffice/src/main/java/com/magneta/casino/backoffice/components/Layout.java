package com.magneta.casino.backoffice.components;

import java.util.List;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.services.ComponentClassResolver;

import com.magneta.casino.backoffice.annotations.DisplayMenu;
import com.magneta.casino.backoffice.beans.MenuBean;
import com.magneta.casino.backoffice.managers.MenuService;
import com.magneta.casino.internal.services.PrincipalService;

@Import(stylesheet="context:css/style.css", library="context:js/t5.js")
public class Layout {
	
	@Parameter(required=true)
	@Property
	private String title;
	
	@InjectService("MenuService")
	private MenuService menuService;

	@InjectService("BackofficePrincipalService")
	private PrincipalService principalService;
	
	@Inject
	private ComponentResources resources;
	
	@Inject
	private ComponentClassResolver resolver; 

	@Property
	private MenuBean menuBean;
	
	public List<MenuBean> getVerticalMenu() {
		return menuService.getVerticalMenu(); 
	}
	
	public List<MenuBean> getHorizontalMenu() {
		return menuService.getHorizontalMenu(); 
	}
	
	public boolean isLoggedIn() {
		return principalService.getPrincipal() != null;
	}
	
	public boolean showMenuLink() {
		return this.menuBean != null && this.menuBean.getPageLink() != null;
	}
	
	public boolean isSpecialLink() {
		return menuBean != null && menuBean.getPageLink() != null && menuBean.getPageLink().startsWith("special:");
	}
	
	public String getSpecialLinkAction() {
		return menuBean.getPageLink().substring("special:".length());
	}
	
	@OnEvent(value="logout")
	void onLogout() {		
		principalService.onUserLogout();
	}

	@Cached
	public boolean isShowMenu() throws ClassNotFoundException {
		String pageName = resources.getPageName();
		
		Class<?> pageKlass = Class.forName(resolver.resolvePageNameToClassName(pageName));

    	DisplayMenu displayMenu = pageKlass.getAnnotation(DisplayMenu.class);
    	
    	if (displayMenu != null) {
    		return displayMenu.value();
    	}
        return true;
    }
}
