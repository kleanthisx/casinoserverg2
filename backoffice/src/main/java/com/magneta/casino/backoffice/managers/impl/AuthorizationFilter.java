// Based on http://tapestryjava.blogspot.com/2009/12/securing-tapestry-pages-with.html

package com.magneta.casino.backoffice.managers.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry5.EventContext;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.apache.tapestry5.services.ComponentRequestFilter;
import org.apache.tapestry5.services.ComponentRequestHandler;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.apache.tapestry5.services.PageRenderRequestParameters;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;

import com.magneta.casino.backoffice.pages.Login;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;

public class AuthorizationFilter implements ComponentRequestFilter {
	private final Request request;
	private final Response response;
	private final PrincipalService principalService;
	private final PageMenuItemAccessValidator accessValidator;
	private final PageRenderLinkSource linkSource;

	/**
	 * Receive all the services needed as constructor arguments. When we bind this service, T5 IoC will provide all the
	 * services!
	 */
	public AuthorizationFilter(Request request, Response response,
			PrincipalService principalService, PageMenuItemAccessValidator accessValidator,
			PageRenderLinkSource linkSource) {
		this.request = request;
		this.response = response;
		this.principalService = principalService;
		this.accessValidator = accessValidator;
		this.linkSource = linkSource;
	}

	@Override
	public void handlePageRender(PageRenderRequestParameters parameters, ComponentRequestHandler handler)
			throws IOException {

		if (isAuthorisedToPage(parameters.getLogicalPageName(), parameters.getActivationContext())) {
			handler.handlePageRender(parameters);
		}
	}

	@Override
	public void handleComponentEvent(ComponentEventRequestParameters parameters, ComponentRequestHandler handler)
			throws IOException {

		if (isAuthorisedToPage(parameters.getActivePageName(), parameters.getEventContext())) {
			handler.handleComponentEvent(parameters);
		}
	}

	public boolean isAuthorisedToPage(String requestedPageName, EventContext eventContext) throws IOException {		
		if (accessValidator.checkAccess(requestedPageName)) {
			return true;
		}

		// If request is AJAX with no session, return an AJAX response that forces reload of the page
		if (request.isXHR() && request.getSession(false) == null) {
			OutputStream os = response.getOutputStream("application/json;charset=UTF-8");
			os.write("{\"script\":\"window.location.reload();\"}".getBytes());
			os.flush();
			return false;
		}

		/**
		 * The user is not logged in. Redirect to login page.
		 */
		if (principalService.getPrincipal() == null) {			
			
			
			
			if (request.getPath().length() > 1) {

				StringBuilder url = new StringBuilder(request.getContextPath() + request.getPath());

				boolean isFirst = true;

				for (String paramName : request.getParameterNames()) {
					String[] values = request.getParameters(paramName);

					for (String value: values) {
						if (isFirst) {
							isFirst = false;
							url.append('?');
						} else {
							url.append('&');
						}

						url.append(URLEncoder.encode(paramName, "UTF-8"));
						url.append('=');
						url.append(URLEncoder.encode(value, "UTF-8"));
					}
				}
				
				
				response.sendRedirect(linkSource.createPageRenderLinkWithContext(Login.class, url.toString()));
			} else {
				response.sendRedirect(linkSource.createPageRenderLink(Login.class));
			}
			return false;
		}
		
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Access denied");
		
		return false;
	}
}