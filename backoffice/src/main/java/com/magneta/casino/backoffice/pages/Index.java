package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.apache.tapestry5.services.Request;

import com.magneta.administration.SystemProperties;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.common.system.SettingNames;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.BACKOFFICE_ENTRY)
public class Index {
	
	@Inject
	private ExtendedPrincipalBean principal;
	
	@Inject
	private UsernameService usernameService;
	
	@Inject
	private SettingsService settingsService;
	
	@Inject
	private PageRenderLinkSource linkSource;
	
	@Inject
	private Messages messages;
	
	@Inject
	private Request request;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public Link onActivate(String page) {
		return linkSource.createPageRenderLinkWithContext("error404", request.getContextPath() + request.getPath());
	}
	
	public String getUsername() throws ServiceException {
		return usernameService.getUsername(principal.getUserId());
	}
	
	public String getLastUpdate() {
		return SystemProperties.getBuildNo();
	}
	
	public String getHelloMsg() throws ServiceException {
		return messages.format("hello", getUsername());
	}
	
	public String getWelcomeMsg() throws ServiceException {
		SettingBean sett = settingsService.getSetting(SettingNames.SYSTEM_NAME);
		String name = null;
		
		if (sett != null) {
			name = sett.getValue();
		}
		
		if (name == null) {
			name = "Casino";
		}
		
		return messages.format("welcome", name);
	}
}
