package com.magneta.casino.backoffice.pages;

import java.util.List;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.WebLoginBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;

@RequiredPrivilege(PrivilegeEnum.PROFILES)
public class SearchDuplicateAccounts {

	@Component(id="searchForm")
	private Form searchForm;

	@InjectService("WebLoginsService")
	private WebLoginsService webLoginsService;
	
	@Property
	private WebLoginBean webLoginBean;
	
	public enum Options
	{
	   IP
	}
	
	@Property
	private Options option;
	
	@Property
	@Persist
	private String value;
	
	@Property
	private Long login;
	
	@OnEvent(value=EventConstants.SUCCESS)
	public void onSuccess(){	
	}
	
	public boolean displayTable() {
		if(value != null && !value.isEmpty()){
			return true;
		}
		return false;
	}
	
	public List<Long> getLogins() throws ServiceException{
		SearchContext<WebLoginBean> searchContext = FiqlContextBuilder.create(WebLoginBean.class, "clientIp==?",value);
		  List<Long> res = webLoginsService.getDistinctLogins(searchContext, 0, 0, null);
		  return res.size() > 0 ? webLoginsService.getDistinctLogins(searchContext, 0, 0, null) : null;
	}
}
