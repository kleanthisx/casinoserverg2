package com.magneta.casino.backoffice.managers.reporting.impl;

import java.io.OutputStream;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;

import com.magneta.casino.backoffice.managers.reporting.ReportExportService;
import com.magneta.casino.backoffice.managers.reporting.ReportFormat;

public class ReportExportServiceImpl implements ReportExportService {

	private JRExporter createExporter(ReportFormat format, String title) throws JRException {
        JRExporter exporter = null;
        
        switch(format) {
        	case DOCX:
            	exporter = new JRDocxExporter();
            	break;
        	case HTML:
        		exporter = new JRHtmlExporter();
                exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
                break;
        	case ODT:
        		exporter = new JROdtExporter();
        		break;
        	case PDF:
        		exporter = new JRPdfExporter();
                exporter.setParameter(JRPdfExporterParameter.METADATA_TITLE, title);
                break;
        	case RTF:
        		exporter = new JRRtfExporter();
        		break;
        	case XLS:
        		exporter = new JRXlsExporter();
                exporter.setParameter(JRXlsAbstractExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
                exporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                exporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
                exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
                break;
        	default:
        		throw new JRException("Unknown exporter for format " + format.getFileExtension());
        }

        return exporter;
    }
	
	@Override
    public void exportReport(JasperPrint jasperPrint, ReportFormat format, OutputStream os) throws JRException {
			JRExporter exporter = createExporter(format, jasperPrint.getName());
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);  
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
			exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
			exporter.exportReport();
    }
}
