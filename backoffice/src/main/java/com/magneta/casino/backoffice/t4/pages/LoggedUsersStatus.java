package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.SystemProperties;
import com.magneta.administration.Util;
import com.magneta.administration.beans.LoggedUserStateBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.LoggedUsersStatusTableModel;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.common.user.utils.ChangeUserStateUtil;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.callback.RedirectExternalCallback;
import com.magneta.xmlrpc.client.SocketXmlRpcClient;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class LoggedUsersStatus extends SecurePage {

	private static final Logger log = LoggerFactory.getLogger(LoggedUsersStatus.class);
	
	@InjectObject("service:casino.common.Servers")
	public abstract ServersService getServersService();

	@InjectPage("GeneralConfirmation")
	public abstract GeneralConfirmation getConfirmPage();

	public abstract int getServerId();
	public abstract void setServerId(int serverId);

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract LoggedUserStateBean getCurrUser();

	@Component(type="contrib:Table",bindings={"source=ognl:loggedUsersStatusTableModel","columns=columns","pageSize=20","row=currUser",
			"initialSortColumn=literal:LoginDate","rowsClass=ognl:beans.evenOdd.next"})
			public abstract Table getUsersTable();

	public LoggedUsersStatusTableModel getLoggedUsersStatusTableModel(){
		return new LoggedUsersStatusTableModel(getServerId());
	}
	
	@Component(bindings={"userId=ognl:currUser.userId","target=literal:_blank"})
	public abstract UserLink getUserIdLink();

	@Component(bindings={"listener=listener:onUserDisconnect","parameters={currUser.userId, currUser.serverId, false}"})
	public abstract DirectLink getDisconnectLink();

	@Component(bindings={"listener=listener:onUserDisconnect","parameters={currUser.userId, currUser.serverId, true}"})
	public abstract DirectLink getBanLink();

	@Component(bindings={"page=literal:SystemStatus"})
	public abstract CasinoLink getBackLink();

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	public static class DisconnectUserConfirmContext extends ConfirmContext {
		private static final long serialVersionUID = 3008189612893331332L;

		private final Long userId;
		private final int serverId;
		private final boolean banUser;
		private final Long loggedInUser;
		private final ServersService serverService;
		
		public DisconnectUserConfirmContext(ServersService serverService, String okMsg, String cancelMsg,String confirmMsg, ICallback callback,Long userId,int serverId,boolean banUser,Long loggedInUser) {
			super(okMsg, cancelMsg, confirmMsg, callback);
			
			this.serverService = serverService;
			this.userId = userId;
			this.serverId = serverId;
			this.banUser = banUser;
			this.loggedInUser = loggedInUser;
		}

		@Override
		public void onOk(IRequestCycle cycle, ValidationDelegate delegate) throws ServiceException {
			
			if (banUser){
				ChangeUserStateUtil.changeUserState(userId, loggedInUser, false);
			}
			
			ServerBean server = serverService.getServer(serverId);
			
			String address = server.getServerLocalAddress();
			int port = server.getServerLocalPort();
			
			if (!disconnectUser(userId, address, port)) {
				delegate.record(null, "The user was not disconnected.");
                return;
			}
			
			returnToPage(cycle);
			return;
		}

	}

	public void onUserDisconnect(IRequestCycle cycle, Long userId, int serverId, boolean banUser) throws ServiceException {
		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		
		if (!getPrincipal().hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			delegate.record(null, this.getMsg("no-access"));
			return;
		}
		
		String username = this.getUsernameService().getUsername(userId);
		
		ConfirmContext confirmContext = new DisconnectUserConfirmContext(getServersService(), this.getMsg("ok"), this.getMsg("cancel"), this.getMsg("confirmMessage") + " " + username + " ?", new RedirectExternalCallback(this, new Object[]{serverId}),userId, serverId, banUser,getPrincipal().getUserId());
		GeneralConfirmation confirmPage = getConfirmPage();
		confirmPage.setConfirmContext(confirmContext);
		getRequestCycle().activate(confirmPage);

	}

	private static boolean disconnectUser(Long userId, String address, int port) {
		SocketXmlRpcClient client = new SocketXmlRpcClient(address, port, false);

		try {
			return (Boolean)client.execute("Admin.banUser", new Object[]{
						SystemProperties.getGamingServerPassword(),
						Util.getBytes(userId) });
		} catch (XmlRpcException e) {
			log.error("Error communicating with server at "+address+":"+port,e);
			return false; 
		} finally {
			client.destroy();
		}
	}

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setServerId((Integer)params[0]);
		}
	}

	public String getColumns(){
		String columns = "UserId, Server, !Status, ClientIp, ClientVersion, LoginDate";

		if (getPrincipal().hasPrivilege(PrivilegeEnum.SYSTEM_ADMIN, true)) {
			columns += ", !disconnect, !ban";
		}
		return columns;
	}
}
