package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class PayoutRatesReportOptions extends DateReportOptions {

	@Component(id = "showDetailCB",
			bindings={"value=ShowDetail", "displayName=message:ShowDetail"})
	public abstract Checkbox getDetailCB();
	
	@Component(id = "showDetailCBLabel",
			bindings={"field=component:showDetailCB"})
	public abstract FieldLabel getDetailCBLabel();
	
	@Component(id = "games",
			bindings = {"value=game","model=ognl:gamesSelectionModel",
			"displayName=message:Game"})
	public abstract PropertySelection getGameSelection();

	public GamesSelectionModel getGamesSelectionModel(){
		return new GamesSelectionModel(true);
	}
	
	@Component(id = "gamesLabel",
			bindings = {"field=component:games"})
	public abstract FieldLabel getGameLabel();
	
	@Component(id = "groupPeriods",
			bindings = {"value=period", "model=new com.magneta.casino.backoffice.t4.models.GroupingPeriodsSelectionModel(true)", 
			"displayName=message:GroupingPeriod"})
	public abstract PropertySelection getGroupPeriods();
			
	@Component(id = "groupPeriodsLabel",
			bindings = {"field=component:groupPeriods"})
	public abstract FieldLabel getGroupPeriodsLabel();
	
	@Component(id = "includeJackpotsCB",
			bindings={"value=IncludeJackpots", "displayName=message:IncludeJackpots"})
	public abstract Checkbox getIncludeJackpotsCB();
	
	@Component(id = "includeJackpotsCBLabel",
			bindings={"field=component:includeJackpotsCB"})
	public abstract FieldLabel getIncludeJackpotsCBLabel();
	
	public abstract boolean getShowDetail();
	public abstract int getGame();
	public abstract String getPeriod(); 
	@InitialValue("true")
	public abstract boolean getIncludeJackpots();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
	
		reportParams.put("show_tables", getShowDetail());
		if (getGame() != -1){
			reportParams.put("game_type_id", getGame());
		}
		reportParams.put("group_period", getPeriod());
		reportParams.put("include_jackpots", getIncludeJackpots());
	}
}
