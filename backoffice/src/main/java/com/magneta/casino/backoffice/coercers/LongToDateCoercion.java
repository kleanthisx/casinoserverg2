package com.magneta.casino.backoffice.coercers;

import java.util.Date;

import org.apache.tapestry5.ioc.services.Coercion;

public class LongToDateCoercion implements Coercion<Long,Date> {

	@Override
	public Date coerce(Long input) {
		return new Date(input);
	}
}
