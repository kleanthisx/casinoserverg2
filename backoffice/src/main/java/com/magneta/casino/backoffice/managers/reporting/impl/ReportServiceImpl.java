package com.magneta.casino.backoffice.managers.reporting.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.tapestry5.services.RequestGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.managers.reporting.ReportExecutionService;
import com.magneta.casino.backoffice.managers.reporting.ReportExportService;
import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportLocatorService;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.services.ServiceException;

public class ReportServiceImpl implements ReportService {
	
	private static final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);
	
	private final BackofficePrincipalService principalService;
	private final ExecutorService executorService;
	private final ReportExecutionService reportExecutionService;
	private final RequestGlobals requestGlobals;
	private final ReportLocatorService reportLocatorService;
	
	public ReportServiceImpl(BackofficePrincipalService principalService,
			ReportExecutionService reportExecutionService,
			 ReportExportService reportExportService,
			 ReportLocatorService reportLocatorService,
			 RequestGlobals requestGlobals) {
		this.principalService = principalService;
		this.executorService = Executors.newSingleThreadExecutor();
		this.reportExecutionService = reportExecutionService;
		this.reportLocatorService = reportLocatorService;
		this.requestGlobals = requestGlobals;
	}
	
	@Override
	public ReportRequest createRequest(JasperReport report, ReportFormat format, TimeZone tz, Locale locale, Map<String,Object> params) {		
		HttpServletRequest request = requestGlobals.getHTTPServletRequest();
		
		return new ReportRequestImpl(UUID.randomUUID().toString(), report, format, params, locale, tz, principalService.getPrincipal(),
				LinkGenerator.getAbsoluteUrl(request), request.getRemoteAddr());
	}

	@Override
	public ReportRequest createRequest(String reportFileName,
			ReportFormat format, TimeZone tz, Locale locale,
			Map<String, Object> params) throws JRException {
		return createRequest(reportLocatorService.getReport(reportFileName), format, tz, locale, params);
	}
    
    private static class ReportCallable implements Callable<URL> {    	
    	private final ReportExecutionService reportService;
    	private final ReportRequest request;
    	private final BackofficePrincipalService principalService;
    	
    	public ReportCallable(ReportExecutionService reportService, BackofficePrincipalService principalService, ReportRequest request) {
    		this.reportService = reportService;
    		this.request = request;
    		this.principalService = principalService;
    	}
    	
		@Override
		public URL call() throws Exception {
			File f = File.createTempFile("REPORT", request.getReportFormat().getFileExtension());
			
			
			OutputStream fileOs = new BufferedOutputStream(new FileOutputStream(f));
			
			principalService.setThreadPrincipal(request.getPrincipal());
			
			try {
				 reportService.executeReport(request, fileOs);
			} catch (Exception e) {
				log.error("Error while executing report",e);
				throw e;
			} finally {
				principalService.setThreadPrincipal(null);
			}
			fileOs.flush();
			fileOs.close();
			
			return f.toURI().toURL();
		}
    }
    
    @Override
    public Future<URL> executeReportAsync(ReportRequest request) {
    	return executorService.submit(new ReportCallable(reportExecutionService, principalService, request));
    }
    
    @Override
    public void exportReport(ReportRequest request, OutputStream os) throws JRException, ServiceException {
    	reportExecutionService.executeReport(request, os);   		
    }
}
