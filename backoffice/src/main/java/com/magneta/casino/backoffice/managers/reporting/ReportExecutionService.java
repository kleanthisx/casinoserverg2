package com.magneta.casino.backoffice.managers.reporting;

import java.io.OutputStream;

import net.sf.jasperreports.engine.JRException;

public interface ReportExecutionService {
	void executeReport(ReportRequest request, OutputStream os) throws JRException;
}
