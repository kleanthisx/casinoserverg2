package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.StringPropertySelectionModel;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.link.PageLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.AccessDeniedException;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.AffiliatePaymentPeriods;
import com.magneta.casino.common.user.utils.ChangeUserStateUtil;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class ExamineCorporate extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(ExamineCorporate.class);
	
	public static final AffiliatePaymentPeriods AFFILIATE_PAYMENT_PERIODS = new AffiliatePaymentPeriods();
	public static final StringPropertySelectionModel AFFILIATE_PAYMENT_TYPES = new StringPropertySelectionModel(new String[]{"Cheque", "Credit Card"});

	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporateService();
	
	@InjectPage("CorporateApplications")
	public abstract CorporateApplications getPartnerApplicationsPage();

	@Persist("client:page")
	public abstract Long getAffiliateID();
	public abstract void setAffiliateID(Long affiliateID);

	@Persist("client:page")
	public abstract String getUsername();
	public abstract String getFirstName();
	public abstract String getLastName();

	@Persist("client:page")
	public abstract String getEmail();
	public abstract double getAffiliateRate();

	public abstract String getPaymentPeriod();
	public abstract void setPaymentPeriod(String paymentPeriod);
	public abstract void setUsername(String username);
	public abstract void setFirstName(String firstName);
	public abstract void setLastName(String lastName);
	public abstract void setEmail(String email);
	public abstract void setAffiliateRate(double affiliateRate);
	public abstract void setMiddleName(String middleName);
	public abstract void setRegisterDate(Date registerDate);
	public abstract void setPhone(String phone);
	public abstract void setAddress(String address);
	public abstract void setRegion(String region);
	public abstract void setTown(String town);
	public abstract void setCountry(String country);

	public abstract List<Long> getCorporateHierarchy();
	public abstract void setCorporateHierarchy(List<Long> hierarchy);
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Bean
	public abstract Min getMinAmount();

	@Bean
	public abstract Max getMaxAmount();
	
	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumZeroTranslator();
	
	@Component(type="For",bindings={"source=corporateHierarchy","value=currCorp","renderTag=false"})
	public abstract ForBean getHierarchyList();
	
	@Component(bindings={"userId=ognl:currCorp","target=literal:_blank"})
	public abstract UserLink getUsernameLink();
	
	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getApproveAffiliateForm();
	
	@Component(bindings={"value=affiliateID"})
	public abstract Hidden getAffiliateIDField();
	
	@Component(bindings={"value=affiliateRate","displayName=getMsg('CorporateRate')","translator=bean:numZeroTranslator","validators=validators:required,$minAmount,$maxAmount","disabled=!allowWrite()"})
	public abstract TextField getAffiliateRateField();
	
	@Component(bindings={"value=paymentPeriod","displayName=message:PaymentPeriod","model=ognl:@com.magneta.casino.backoffice.t4.pages.ExamineCorporate@AFFILIATE_PAYMENT_PERIODS","disabled=!allowWrite()"})
	public abstract PropertySelection getPaymentPeriodField();
	
	@Component(bindings={"value=superApprovalRequired","displayName=message:superRequired","disabled=!allowWrite()"})
	public abstract Checkbox getRequireSuper();
	
	@Component(bindings={"page=literal:CorporateApplications"})
	public abstract PageLink getBackLink();
	
	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Button getCancelButton();

	@InitialValue("true")			
	public abstract void setSuperApprovalRequired(boolean required);
	public abstract boolean isSuperApprovalRequired();

	@InitialValue("100.0")
	@Persist("client:page")
	public abstract void setAllowedMaxRate(double allowedMaxRate);

	public abstract double getAllowedMaxRate();

	public abstract Long getCurrCorp();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		if (params != null && params.length > 0) {
			this.setAffiliateID((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent event) {
		if (getAffiliateID() == null) {
			throw new BadRequestException("Invalid/missing affiliateId");
		}

		if (!getUserAccessService().canAccess(getAffiliateID())) {
			throw new AccessDeniedException("Access denied");
		}
		
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			return;
		}

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(
					"SELECT users.username, users.first_name, users.last_name,users.middle_name,users.register_date, users.phone, users.postal_address,"+
							" users.region, users.town, users.email," +
							" affiliates.payment_period," +
							" affiliates.affiliate_rate, countries.country_name, users.user_type" +
							" FROM users" +
							" INNER JOIN affiliates ON users.user_id = affiliates.affiliate_id" +
							" LEFT OUTER JOIN countries ON users.country = countries.country_code" +
					" WHERE users.user_id = ?");
			stmt.setLong(1, getAffiliateID());

			rs = stmt.executeQuery();

			if (rs.next()) {
				setUsername(rs.getString("username"));
				setFirstName(rs.getString("first_name"));
				setLastName(rs.getString("last_name"));
				setEmail(rs.getString("email"));
				setMiddleName(rs.getString("middle_name"));
				setRegisterDate(DbUtil.getTimestamp(rs, "register_date"));
				setPhone(rs.getString("phone"));
				setAddress(rs.getString("postal_address"));
				setRegion(rs.getString("region"));
				setTown(rs.getString("town"));
				setCountry(rs.getString("country_name"));
				setPaymentPeriod(rs.getString("payment_period"));
				setAffiliateRate(rs.getDouble("affiliate_rate") * 100.00);
			}

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving affiliate information", e);
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}

		List<Long> corporateHierarchy;
		try {
			corporateHierarchy = this.getCorporateService().getCorporateHierarchy(this.getAffiliateID());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}

		setCorporateHierarchy(corporateHierarchy);


		if (corporateHierarchy != null && !corporateHierarchy.isEmpty()) {
			Long superId = corporateHierarchy.get(0);

			com.magneta.casino.services.beans.CorporateBean corporate;
			try {
				corporate = this.getCorporateService().getCorporate(superId);
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}

			setAllowedMaxRate(corporate.getAffiliateRate() * 100);
		}
		
		getMaxAmount().setMax(getAllowedMaxRate());
		getMinAmount().setMin(0.00);

	}

	public void onSubmit(IRequestCycle cycle) {
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"),null);
			return;
		}

		try {
			conn.setAutoCommit(false);

			PreparedStatement stmt = null;
			try {
				stmt = conn.prepareStatement(
						"UPDATE affiliates " +
						" SET affiliate_rate=?, payment_period=CAST (? AS INTERVAL)," +
						" approved = TRUE, modifier_user = ?," +
						" requires_super_approval = ?"+ 
				" WHERE affiliate_id = ?");

				stmt.setDouble(1, getAffiliateRate() / 100.00);
				stmt.setString(2, getPaymentPeriod());
				stmt.setLong(3, getPrincipal().getUserId());
				stmt.setBoolean(4, isSuperApprovalRequired());
				stmt.setLong(5, getAffiliateID());

				stmt.executeUpdate();
			} finally {
				DbUtil.close(stmt);
			}

			createCorporateJackpot(conn, getAffiliateID());			

			if(!ChangeUserStateUtil.changeUserState(conn, getAffiliateID(), getPrincipal().getUserId(), true)) {
				conn.rollback();
			}

			conn.commit();

			redirectToPage("CorporateApplications");
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				log.error("Error rolling back.",e1);
			}
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"),null);
			log.error("Error while approving affiliate.", e);
		} finally {
			DbUtil.close(conn);
		}
	}

	private void createCorporateJackpot(Connection conn, Long corpId) throws SQLException {
		PreparedStatement jackpotCheckStmt = null;
		ResultSet jackpotCheckRs = null;
		boolean jackpotExists = false;
		try {
			jackpotCheckStmt = conn.prepareStatement(
					"SELECT jackpot_id"+
					" FROM jackpots"+
					" WHERE (target_corporate = ? AND finish_date IS NULL)"+
					" OR EXISTS (SELECT affiliates.affiliate_id FROM affiliate_users"+
			" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id WHERE affiliate_users.affiliate_id = ?)");
			jackpotCheckStmt.setLong(1, corpId);
			jackpotCheckStmt.setLong(2, corpId);
			jackpotCheckRs = jackpotCheckStmt.executeQuery();
			if (jackpotCheckRs.next()){
				jackpotExists = true;
			}
		} finally {
			DbUtil.close(jackpotCheckRs);
			DbUtil.close(jackpotCheckStmt);
		}

		if (!jackpotExists){
			Statement configStmt = null;
			ResultSet configRs = null;
			boolean configExists = false;
			double initialAmount = 0.0;
			double betContrib = 0.0;
			double maxAmount = 0.0;
			int drawNumber = -1;
			double minPayout = 0.0;
			double minJackpotBalance = 0.0;

			try {
				configStmt = conn.createStatement();
				configRs = configStmt.executeQuery(
						"SELECT initial_amount, bet_contrib, maximum_amount, draw_number, min_payout, min_jackpot_balance" +
						" FROM jackpot_configs" +
				" WHERE config_id = 0");
				if (configRs.next()){
					configExists = true;
					initialAmount = configRs.getDouble("initial_amount");
					betContrib = configRs.getDouble("bet_contrib");
					maxAmount = configRs.getDouble("maximum_amount");
					drawNumber = configRs.getInt("draw_number");
					minPayout = configRs.getInt("min_payout");
					minJackpotBalance = configRs.getInt("min_jackpot_balance");
				}
			} finally {
				DbUtil.close(configRs);
				DbUtil.close(configStmt);
			}

			if (configExists){
				Statement idStmt = null;
				ResultSet idRs = null;
				PreparedStatement jackpotStmt = null;

				int jackpotId = -1;
				try {
					idStmt = conn.createStatement();
					idRs = idStmt.executeQuery("SELECT nextval('jackpot_id_seq')");

					if (idRs.next()){
						jackpotId = idRs.getInt(1);
					}
				} finally {
					DbUtil.close(idStmt);
					DbUtil.close(idRs);
				}

				try {
					jackpotStmt = conn.prepareStatement(
							"INSERT INTO jackpots(jackpot_id, game_id, target_corporate, initial_amount, maximum_amount, pay_count, " +
							" auto, config_id, draw_number, bet_contrib, min_payout, min_jackpot_balance)"+
					" VALUES (?,?,?,?,?,?,?,0,?,?,?)");

					jackpotStmt.setInt(1, jackpotId);

					jackpotStmt.setObject(2, null);

					jackpotStmt.setLong(3, corpId);

					jackpotStmt.setBigDecimal(4, new BigDecimal(initialAmount));

					if (maxAmount == 0.0){
						jackpotStmt.setObject(5, null);
					} else {
						jackpotStmt.setBigDecimal(5, new BigDecimal(maxAmount));
					}

					jackpotStmt.setObject(6, null);

					jackpotStmt.setBoolean(7, false);

					jackpotStmt.setInt(8, drawNumber);

					jackpotStmt.setBigDecimal(9, new BigDecimal(betContrib));

					if (minPayout > 0.0){
						jackpotStmt.setBigDecimal(10, new BigDecimal(minPayout));
					} else {
						jackpotStmt.setObject(10, null);
					}
					if (minJackpotBalance > 0.0){
						jackpotStmt.setBigDecimal(11, new BigDecimal(minJackpotBalance));
					} else {
						jackpotStmt.setObject(11, null);
					}

					jackpotStmt.execute();
				} finally {
					DbUtil.close(jackpotStmt);
				}

				if (initialAmount > 0.0){
					PreparedStatement transactionStmt = null;
					try{
						transactionStmt = conn.prepareStatement(
								"INSERT INTO jackpot_transactions(jackpot_id, amount)" +
						" VALUES (?,?)");
						transactionStmt.setInt(1, jackpotId);
						transactionStmt.setBigDecimal(2, new BigDecimal(initialAmount));
						transactionStmt.execute();
					} finally {
						DbUtil.close(transactionStmt);
					}
				}
			}
		}
	}

	public void onCancel(IRequestCycle cycle){
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"),null);
			return;
		}

		PreparedStatement stmt2 = null;
		try {
			stmt2 = conn.prepareStatement(
					"DELETE FROM users" + 
					" WHERE user_id = ?");

			stmt2.setLong(1, getAffiliateID());
			stmt2.execute();

			redirectToPage("CorporateApplications");
		} catch (SQLException e) {
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"),null);
			log.error("Error while rejecting affiliate.", e);
		} finally {
			DbUtil.close(stmt2);
			DbUtil.close(conn);
		}
	}
}
