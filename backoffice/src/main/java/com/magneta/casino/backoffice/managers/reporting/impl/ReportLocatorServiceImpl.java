package com.magneta.casino.backoffice.managers.reporting.impl;

import java.io.File;

import org.apache.tapestry5.services.Context;

import com.magneta.casino.backoffice.managers.reporting.ReportLocatorService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class ReportLocatorServiceImpl implements ReportLocatorService {

	/* Path to find reports relative to WEB-INF */
	private final String REPORT_DIR = File.separator + "reports";
	private final String REPORT_WORK_DIR = File.separator + "reports"  + File.separator + "work";

	private final Context context;
	private final Object compileLock;
	
	public ReportLocatorServiceImpl(Context context) {
		this.context = context;
		this.compileLock = new Object();
	}

	@Override
	public JasperReport getReport(String report) throws JRException {
	    File designFile = context.getRealFile(REPORT_DIR + File.separator +  report + ".jrxml");

	    if (designFile == null) {
	    	throw new JRException("Report file not found");
	    }
	    String workPath = context.getRealFile(REPORT_WORK_DIR + File.separator + "PLACEHOLDER").getParentFile().getAbsolutePath();
	    File reportFile = new File(workPath + File.separator + report + ".jasper");

		if (!reportFile.exists() || reportFile.lastModified() < designFile.lastModified()) {
			synchronized(compileLock) {
				if (!reportFile.exists() || reportFile.lastModified() < designFile.lastModified()) {
					JasperDesign design = JRXmlLoader.load(designFile);
					JasperCompileManager.compileReportToFile(design, reportFile.getAbsolutePath());
				}
			}
		}

		return (JasperReport)JRLoader.loadObject(reportFile); 
	}
}
