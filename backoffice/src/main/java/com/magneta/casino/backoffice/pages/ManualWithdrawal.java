package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.PaymentTypeSelectModel;
import com.magneta.casino.backoffice.valueencoders.PaymentTypeEncoder;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserWithdrawalsService;
import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserWithdrawalBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.search.SortContext;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public class ManualWithdrawal {
	
	private static final Logger log = LoggerFactory.getLogger(ManualWithdrawal.class);
	
	@Inject
	private AlertManager alertManager;
	
	@Inject
	private Messages messages;
	
	@Inject
	private PageRenderLinkSource pageRenderLinkSource;
	
	@Inject
	private UserWithdrawalsService userWithdrawalService;

	@Inject
	private PaymentTypeService paymentTypeService;
	
	@Inject
	private UserDetailsService userDetailsService;
	
	@Inject
	private CurrencyParser currencyParser;
	
	@Component(id="withdrawalForm")
	private BeanEditForm form;
	
	@Component(id="amount")
	private TextField amountField;
	
	@Property
	private Long userId;
	
	@Property
	private UserWithdrawalBean withdrawal;
	
	@Property
	private PaymentTypeBean paymentType;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long userId) {
		this.userId = userId;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Long passivate() {
		return userId;
	}
	
	@Cached
	public Iterable<PaymentTypeBean> getPaymentTypes() throws ServiceException {
		return paymentTypeService.getPaymentTypes(null, 0, 0, new SortContext<PaymentTypeBean>("paymentType")).getResult();
	}
	
	public PaymentTypeSelectModel getPaymentTypeSelectModel() throws ServiceException {
		return new PaymentTypeSelectModel(getPaymentTypes());
	}
	
	public PaymentTypeEncoder getPaymentTypeEncoder() throws ServiceException {
		return new PaymentTypeEncoder(getPaymentTypes());
	}
	
	@OnEvent(value=EventConstants.PREPARE, component="withdrawalForm")
	public void onPrepare() {
		withdrawal = new UserWithdrawalBean();
	}
	
	@OnEvent(value=EventConstants.VALIDATE,component="withdrawalForm")
	public void onValidate() throws ServiceException {		
		if (getBalance() < withdrawal.getTransactionAmount()) { 
			form.recordError(amountField, messages.get("insufficient-funds"));
		}
	}
	
	@OnEvent(value=EventConstants.SUCCESS, component="withdrawalForm")
	public Object onSuccess() {
		withdrawal.setUserId(userId);
		withdrawal.setType(paymentType.getPaymentType());

		try {
			userWithdrawalService.createManualWithdrawal(withdrawal);
		} catch (ServiceException e) {
			log.error("Unable to create manual withdrawal", e);
			alertManager.error(messages.format("unable-to-create-withdrawal", e.getMessage()));
			return null;
		}

		return pageRenderLinkSource.createPageRenderLinkWithContext("UserDetails", userId);
	}
	
	@OnEvent(value=EventConstants.CANCELED, component="withdrawalForm")
	public Object onCancelForm(){
		return pageRenderLinkSource.createPageRenderLinkWithContext("UserDetails", userId);
	}
	
	@Cached
	public Double getBalance() throws ServiceException {
		UserBalanceBean balance = userDetailsService.getUserBalance(userId);
		
		return balance.getBalance();
	}
	
	public String formatCurrency(Double amount) {
		return currencyParser.formatCurrency(amount);
	}
}
