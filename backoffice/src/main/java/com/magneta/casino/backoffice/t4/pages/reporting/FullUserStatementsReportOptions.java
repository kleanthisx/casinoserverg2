package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.administration.commons.models.CountrySelectionModel;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class FullUserStatementsReportOptions extends DateReportOptions {
	
	@Component(id = "username",
			bindings = {"value=username", "disabled=false", "displayName=message:Username"})
	public abstract TextField getUsernameField();

	@Component(id = "usernameLabel",
		 bindings = {"field=component:username"})
	public abstract FieldLabel getUsernameLabel();

	@Component(id = "country",
			bindings = {"value=country","model=ognl:countrySelectionModel", 
		"displayName=message:Country"})
	public abstract PropertySelection getCountrySelection();

	public CountrySelectionModel getCountrySelectionModel() throws ServiceException{
		return new CountrySelectionModel();
	}
	
	@Component(id = "countryLabel", 
			bindings = {"field=component:country"})
	public abstract FieldLabel getCountryLabel();
	
	@Component(id = "tableId", 
			bindings = {"value=tableId", "disabled=false","displayName=message:TableID"})
	public abstract TextField getTableIDField();

	@Component(id = "tableIdLabel",
			bindings = {"field=component:tableId"})
	public abstract FieldLabel getTableIdLabel();
	
	@Component(id = "games",
			bindings = {"value=game","model=ognl:gamesSelectionModel", 
			"displayName=message:Game"})
	public abstract PropertySelection getGameSelection();

	public GamesSelectionModel getGamesSelectionModel(){
		return new GamesSelectionModel(true);
	}
	
	@Component(id = "gamesLabel",
			bindings = {"field=component:games"})
	public abstract FieldLabel getGameLabel();
	
	@Component(id = "showInactiveCB",
			bindings={"value=ShowInactive", "displayName=message:ShowInactive"})
	public abstract Checkbox getShowInactiveCB();
	
	@Component(id = "showInactiveCBLabel",
			bindings={"field=component:showInactiveCB"})
	public abstract FieldLabel getShowInactiveCBLabel();
	
	public abstract String getUsername();
	public abstract String getCountry();
	public abstract Long getTableId();
	public abstract int getGame();
	public abstract boolean getShowInactive();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);

		reportParams.put("username",getUsername());
		reportParams.put("country_val",getCountry());
		reportParams.put("table_id", getTableId());
		reportParams.put("show_inactive", getShowInactive());
		if (getGame() != -1){
			reportParams.put("game_type_id", getGame());
		}
	}
}
