package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class WinnersReportOptions extends DateReportOptions {

	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Component(id = "minAmount",
			bindings = {"value=minAmount", "disabled=false","displayName=message:MinAmount", "translator=bean:numTranslator"})
	public abstract TextField getMinAmountField();

	@Component(id = "minAmountLabel",
			bindings = {"field=component:minAmount"})
	public abstract FieldLabel getMinAmountLabel();
	
	@Component(id = "games",
			bindings = {"value=game","model=ognl:gamesSelectionModel","displayName=message:Game"})
	public abstract PropertySelection getGameSelection();

	public GamesSelectionModel getGamesSelectionModel(){
		return new GamesSelectionModel(true);
	}
	
	@Component(id = "gamesLabel",
			bindings = {"field=component:games"})
	public abstract FieldLabel getGameLabel();
	
	@Component(id = "includeJackpotsCB",
			bindings={"value=IncludeJackpots", "displayName=message:IncludeJackpots"})
	public abstract Checkbox getIncludeJackpotsCB();
	
	@Component(id = "includeJackpotsCBLabel",
			bindings={"field=component:includeJackpotsCB"})
	public abstract FieldLabel getIncludeJackpotsCBLabel();
	
	@InitialValue("0.0")
	public abstract double getMinAmount();
	public abstract int getGame();
	@InitialValue("true")
	public abstract boolean getIncludeJackpots();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);

		reportParams.put("min_amount", getMinAmount());
		if (getGame() != -1){
			reportParams.put("game_type_id", getGame());
		}
		reportParams.put("include_jackpots", getIncludeJackpots());
	}
}
