package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class PartnerUsersSelectionModel implements IPropertySelectionModel {
	
    private List<Long> ids;
    private List<String> usernames;
    
    public PartnerUsersSelectionModel(Long partnerId, boolean anyOption,  boolean noneOption){
    	ids = new ArrayList<Long>();
        usernames = new ArrayList<String>();
        
        if (noneOption){
        	ids.add(new Long(-1));
        	usernames.add("--Please select--");
        }
        
        if (anyOption){
        	ids.add(new Long(0));
        	usernames.add("All");
        }
        
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return;
		}
        
        try{
        	 String sql = 
        		 " SELECT users.user_id, users.username" +
                 " FROM affiliate_users" +
                 " INNER JOIN affiliates ON affiliate_users.affiliate_id = affiliates.affiliate_id" +
                 " INNER JOIN users ON affiliate_users.user_id = users.user_id"+
                 " WHERE affiliation = 1 AND affiliate_users.affiliate_id = ?"+
        	 	 " ORDER BY username";
            statement = dbConn.prepareStatement(sql);
         
            statement.setLong(1, partnerId);
            
            res = statement.executeQuery();
            
            while (res.next()){
                ids.add(res.getLong("user_id"));
                usernames.add(res.getString("username"));
            }

        }catch (SQLException e) {
            throw new RuntimeException("Error while getting affiliates.", e);
        }finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
    }
    
    @Override
	public String getLabel(int arg0) {
        return usernames.get(arg0);
    }

    @Override
	public Object getOption(int arg0) {
        return ids.get(arg0);
    }

    @Override
	public int getOptionCount() {
        return ids.size();
    }

    @Override
	public String getValue(int arg0) {
        return String.valueOf(ids.get(arg0));
    }

    @Override
	public Object translateValue(String arg0) {
        return Long.parseLong(arg0);
    }
    
    @Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
