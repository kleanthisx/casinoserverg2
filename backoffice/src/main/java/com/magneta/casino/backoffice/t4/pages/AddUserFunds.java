package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserBalanceAdjustmentsService;
import com.magneta.casino.services.beans.UserBalanceAdjustmentBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class AddUserFunds extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(AddUserFunds.class);
	
	@Override
	public boolean pageNeedsWriteAccess() {
		return true;
	}
	
	@InjectObject("service:casino.common.UserBalanceAdjustments")
	public abstract UserBalanceAdjustmentsService getUserBalanceAdjustmentsService();

	@Persist("client:page")
	public abstract Long getUserID();
	public abstract void setUserID(Long userID);

	public abstract double getAmount();    
	public abstract void setUsername(String username);
	public abstract String getDescription();

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Bean(initializer="min=-1000000.00")
	public abstract Min getMinAmount();

	@Bean(initializer="max=1000000.00")
	public abstract Max getMaxAmount();

	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getAddFundsForm();

	@Component(bindings={"value=amount","disabled=!allowWrite()","translator=bean:numTranslator","validators=validators:required,$minAmount,$maxAmount"})
	public abstract TextField getAmountField();

	@Component(bindings={"value=description","disabled=!allowWrite()","validators=validators:maxLength=255"})
	public abstract TextArea getDesc();

	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();

	@Component
	public abstract Button getCancelButton();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params == null) || (params.length < 0)){
			throw new BadRequestException("Invalid/missing userId");
		}

		setUserID((Long)params[0]);
	}

	@Override
	public void pageBeginRender(PageEvent pEvt){
		setUsername(getUsername(getUserID()));
	}

	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		UserBalanceAdjustmentBean adjustment = new UserBalanceAdjustmentBean();
		
		adjustment.setAdjustmentAmount(getAmount());
		adjustment.setAdjustmentDescription(getDescription());
		adjustment.setUserId(getUserID());
		
		try {
			this.getUserBalanceAdjustmentsService().createAdjustment(adjustment);
		} catch (ServiceException e) {
			log.error("Unable to create balance adjustment", e);
			getErrorDisplay().getDelegate().record(null, getMsg("db-error"));
			return;
		}

		cycle.sendRedirect(LinkGenerator.getT5Url("UserDetails", getUserID()));
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.sendRedirect(LinkGenerator.getT5Url("UserDetails", getUserID()));
	}
}
