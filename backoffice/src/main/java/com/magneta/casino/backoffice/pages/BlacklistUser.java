package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.CountrySelectModel;
import com.magneta.casino.backoffice.valueencoders.CountryEncoder;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsersBlacklistService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.UserBlacklistBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class BlacklistUser {

	@Component(id="userBlacklistForm")
	private BeanEditForm userBlacklistForm;

	@InjectService("UsersBlacklistService")
	private UsersBlacklistService usersBlacklistService;
	
	@InjectService("CountryService")
	private CountryService countryService;
	
	@Property
	private UserBlacklistBean blacklistBean;

	@Property
	private Long userBlacklistId;
	
	@Property
	private CountryBean country;
	
	public void set(Long userBlacklistId){
		this.userBlacklistId = userBlacklistId;
	}
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void onActivate(Long userBlacklistId){
		this.userBlacklistId = userBlacklistId;
	}
	
	@OnEvent(value=EventConstants.PASSIVATE)
	public Object[] onPassivate(){
		return new Object[] {userBlacklistId};
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepare() throws ServiceException{
		if (userBlacklistId == null){
			blacklistBean = new UserBlacklistBean();
		}else{
			blacklistBean = usersBlacklistService.getUserBlacklist(userBlacklistId);
			country = countryService.getCountry(blacklistBean.getCountryCode());
		}
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccess() throws ServiceException{
		if(country != null){
			blacklistBean.setCountryCode(country.getCode());
		}
		 
		if(blacklistBean.getCountryCode() == null && blacklistBean.getEmail() == null && blacklistBean.getFirstName() == null &&
				blacklistBean.getLastName() == null && blacklistBean.getUsername() == null){
			return UsersBlacklist.class;
		}
				
		if(userBlacklistId == null){
			usersBlacklistService.insertBlacklistUser(blacklistBean);
		}else{
			usersBlacklistService.updateBlacklistUser(blacklistBean);
		}
		return UsersBlacklist.class;
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancel(){
		return UsersBlacklist.class;
	}
	
	public CountrySelectModel getCountrySelectModel(){
		return new CountrySelectModel(countryService);
	}
	
	public CountryEncoder getCountryEncoder(){
		return new CountryEncoder(countryService);
	}
}
