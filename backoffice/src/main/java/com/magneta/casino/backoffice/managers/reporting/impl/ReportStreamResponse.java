package com.magneta.casino.backoffice.managers.reporting.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import net.sf.jasperreports.engine.JRException;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.services.ServiceException;

public class ReportStreamResponse implements StreamResponse {

	private static final Logger log = LoggerFactory.getLogger(ReportStreamResponse.class);
	
	private final ReportRequest request;
	private final ReportService reportService;
	private File outFile;
	
	public ReportStreamResponse(ReportRequest request, ReportService reportService) {
		this.request = request;
		this.reportService = reportService;
	}

	@Override
	public String getContentType() {
		return request.getReportFormat().getContentType();
	}

	@Override
	public InputStream getStream() throws IOException {
		return new DeleteOnCloseFileInputStream(outFile);
	}

	@Override
	public void prepareResponse(Response response) {
		try {
			outFile = File.createTempFile("REPORT", request.getReportFormat().getFileExtension());
		} catch (IOException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to create temporary file for report");
			} catch (IOException e1) {
			}
			return;
		}
		
		FileOutputStream os;
		try {
			os = new FileOutputStream(outFile);
		} catch (FileNotFoundException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to create temporary file for report");
			} catch (IOException e1) {
			}
			return;
		}
		try {
			reportService.exportReport(request, os);
		} catch (JRException e) {
			log.error("Error while exporting report", e);
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to export report: " + e.getMessage());
			} catch (IOException e1) {
			}
			return;
		} catch (ServiceException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to export report: " + e.getMessage());
			} catch (IOException e1) {
			}
		}
		
		try {
			os.flush();
			os.close();
		} catch (IOException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to save exported report: " + e.getMessage());
			} catch (IOException e1) {
			}
			return;
		}
		
		response.setHeader("Content-Disposition","inline; filename=\""+request.getReport().getName()+"."+request.getReportFormat().getFileExtension() + "\"");
		response.setHeader("Content-Type", getContentType()+";charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength((int)outFile.length());
	}

	
	private static class DeleteOnCloseFileInputStream extends FileInputStream {
		private File file;

		public DeleteOnCloseFileInputStream(File file) throws FileNotFoundException {
			super(file);
			this.file = file;
		}

		@Override
		public void close() throws IOException {
			try {
				super.close();
			} finally {
				if(file != null) {
					file.delete();
					file = null;
				}
			}
		}
	}
}
