package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.search.SortContext;

public class GamesGridDataSource extends GenericGridDataSource<GameBean> {

	private final GamesService gameService;
	
	public GamesGridDataSource(GamesService gameService) {
		this.gameService = gameService;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return gameService.getGamesSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<GameBean> getData(int limit, int offset,
			SortContext<GameBean> sortContext) throws Exception {
		return gameService.getGames(null, sortContext, limit, offset).getResult();
	}
}
