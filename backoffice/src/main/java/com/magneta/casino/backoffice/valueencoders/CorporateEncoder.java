package com.magneta.casino.backoffice.valueencoders;

import org.apache.tapestry5.ValueEncoder;

import com.magneta.casino.services.beans.CorporateBean;

public class CorporateEncoder implements ValueEncoder<CorporateBean> {

	private final Iterable<CorporateBean> corporates;
	
	public CorporateEncoder(Iterable<CorporateBean> corporates) {
		this.corporates = corporates;
	}
	
	@Override
	public String toClient(CorporateBean value) {
		return value.getAffiliateId().toString();
	}

	@Override
	public CorporateBean toValue(String clientValue) {	
		Long corporateId = Long.valueOf(clientValue);
		for(CorporateBean corporate: corporates) {
			if(corporate.getAffiliateId().equals(corporateId)) {
				return corporate;
			}
		}
		
		throw new IllegalArgumentException("Invalid corporate");
	}

}
