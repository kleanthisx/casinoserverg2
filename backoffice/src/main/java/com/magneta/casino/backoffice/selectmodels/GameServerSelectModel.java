package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.search.SortContext;

public class GameServerSelectModel extends AbstractSelectModel {

	private final ServersService serverService;
	
	public GameServerSelectModel(ServersService serverService) {
		this.serverService = serverService;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> models = new ArrayList<OptionModel>();
		
		List<ServerBean> servers;
		try {
			servers = serverService.getServers(null, new SortContext<ServerBean>("serverDescription"), 0, 0).getResult();
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		for (ServerBean server: servers) {
			models.add(new OptionModelImpl(server.getServerDescription(), server.getServerId()));
		}
		return models;
	}

}
