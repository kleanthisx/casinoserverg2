package com.magneta.casino.backoffice.griddatasource;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import com.magneta.casino.services.search.SortContext;

public abstract class GenericGridDataSource<T> implements GridDataSource
{

	private List<T> collection;
	private int collectionStartIndex;

	@Override
	public abstract int getAvailableRows();

	public abstract List<T> getData(int limit, int offset, SortContext<T> sortConstraints) throws Exception;

	private final SortContext<T> stringSortConstraint(List<SortConstraint> sorts) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		
		for (SortConstraint sort : sorts) {		
			if (sort.getColumnSort() == ColumnSort.ASCENDING) {
				if (isFirst) {
					isFirst = false;
				} else {
					sb.append(',');
				}
				
				sb.append(sort.getPropertyModel().getId());
			} else if (sort.getColumnSort() == ColumnSort.DESCENDING) {
				if (isFirst) {
					isFirst = false;
				} else {
					sb.append(',');
				}
				
				sb.append('!').append(sort.getPropertyModel().getId());
			}			
		}

		return new SortContext<T>(sb.toString());
	}

	@Override
	public final Object getRowValue(int index) {
		if (collection == null)
			return null;

		try {
			return collection.get(index - collectionStartIndex);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public final void prepare(int startIndex, int endIndex, List<SortConstraint> sort) {
		try {
			collection = getData((endIndex - startIndex) + 1, startIndex, stringSortConstraint(sort));
		} catch (Exception e) {
			throw new RuntimeException("Unable to retrieve data for grid", e);
		}
		this.collectionStartIndex = startIndex;
	}

	@Override
	public Class<?> getRowType() {

		ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
		return (Class<?>) (type.getActualTypeArguments()[0]);
	}
}
