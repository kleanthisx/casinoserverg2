package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class EditJackpot extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditJackpot.class);
	
	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();
	
	public abstract Integer getJackpotID();
	public abstract void setJackpotID(Integer jackpotID);

	public abstract Integer getConfigID();
	public abstract void setConfigID(Integer configID);

	public abstract double getInitialAmount();
	public abstract void setInitialAmount(double initialAmount);

	public abstract double getMaxAmount();
	public abstract void setMaxAmount(double maxAmount);

	public abstract Integer getPayTimes();
	public abstract void setPayTimes(Integer payTimes);

	public abstract double getContrib();
	public abstract void setContrib(double contrib);

	public abstract int getDrawNumber();
	public abstract void setDrawNumber(int drawNumber);

	public abstract void setMystery(boolean automatic);
	public abstract boolean isMystery();

	public abstract double getMinPayout();
	public abstract void setMinPayout(double minPayout);

	public abstract double getMinJackpotBalance();
	public abstract void setMinJackpotBalance(double minJackpotBalance);

	public abstract void setGameID(Integer gameID);
	public abstract Integer getGameID();

	@InitialValue("false")
	public abstract void setBetTypeJackpot(boolean betTypeJackpot);
	public abstract boolean getBetTypeJackpot();

	public abstract void setGameName(String gameName);
	public abstract String getGameName();
	public abstract void setCorpUsername(String corpUsername);
	public abstract String getCorpUsername();
	
	public abstract void setJAllowed(boolean isAllowed);
	public abstract boolean isJAllowed();
	public abstract void setMJAllowed(boolean allowed);
	public abstract boolean isMJAllowed();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
    @Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumZeroTranslator();

	@Bean(initializer="min=1")
	public abstract Min getMinPayTimes();

	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();
	
	@Component(id="titleMsg",
			bindings={"value=title"})
	public abstract Insert getTitleMsg();
	
	@Component(id="editJackpotForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getEditJackpotForm();
	
	@Component(id="automaticCB",
			bindings={"value=mystery","displayName=message:Mystery","disabled=GameID == null || !allowWrite()"})
	public abstract Checkbox getAutomaticCB();
	
	@Component(id="automaticCBLabel",
			bindings={"field=component:automaticCB"})
	public abstract FieldLabel getAutomaticCBLabel();
	
	@Component(id="jackpotID",
			bindings={"value=jackpotID"})
	public abstract Hidden getJackpotIDHidden();
	
	@Component(id="configID",
			bindings={"value=configID"})
	public abstract Hidden getConfigIDHidden();
	
	@Component(id="gameID",
			bindings={"value=gameID"})
	public abstract Hidden getGameIDHidden();
	
	@Component(id="betTypeJackpot",
			bindings="value=betTypeJackpot")
	public abstract Hidden getBetTypeJackpotHidden();
	
	@Component(id="initialAmountLabel",
			bindings={"field=component:initialAmount"})
	public abstract FieldLabel getInitialAmountLabel();
	
	@Component(id="initialAmount",
			bindings={"value=initialAmount","disabled=!allowWrite()","displayName=message:InitialAmount","translator=bean:numZeroTranslator","validators=validators:required,$minAmount"})
	public abstract TextField getInitialAmountField();
	
	@Component(id="maxAmountLabel",
			bindings={"field=component:maxAmount"})
	public abstract FieldLabel getMaxAmountLabel();
	
	@Component(id="maxAmount",
			bindings={"value=maxAmount","disabled=!allowWrite()","displayName=message:MaxAmount","translator=bean:numTranslator"})
	public abstract TextField getMaxAmountField();
	
	@Component(id="payTimesLabel",
			bindings={"field=component:payTimes"})
	public abstract FieldLabel getPayTimesLabel();
	
	@Component(id="payTimes",
			bindings={"value=payTimes","displayName=message:TimesToPay","translator=bean:numTranslator","validators=validators:$minPayTimes",
			"disabled=configID != null || (!allowWrite())"})
	public abstract TextField getPayTimesField();
	
	@Component(id="contribLabel",
			bindings={"field=component:contrib"})
	public abstract FieldLabel getContribLabel();
	
	@Component(id="contrib",
			bindings={"value=contrib","displayName=contribLabelMsg","disabled=!allowWrite()","translator=bean:numTranslator","validators=validators:required,$minAmount"})
	public abstract TextField getContribField();
	
	@Component(id="drawNumberLabel",
			bindings={"field=component:drawNumber"})
	public abstract FieldLabel getDrawNumberLabel();
	
	@Component(id="drawNumber",
			bindings={"value=drawNumber","disabled=!allowWrite()","displayName=message:DrawNumber","translator=bean:numTranslator",
			"validators=validators:$minPayTimes"})
	public abstract TextField getDrawNumberField();
	
	@Component(id="minPayoutLabel",
			bindings={"field=component:minPayout"})
	public abstract FieldLabel getMinPayoutLabel();
	
	@Component(id="minPayout",
			bindings={"value=minPayout","disabled=!allowWrite()","displayName=message:MinPayout","translator=bean:numTranslator"})
	public abstract TextField getMinPayoutField();
	
	@Component(id="minJackpotBalanceLabel",
			bindings={"field=component:minJackpotBalance"})
	public abstract FieldLabel getMinJackpotBalanceLabel();
	
	@Component(id="minJackpotBalance",
			bindings={"value=minJackpotBalance","disabled=!allowWrite()","displayName=message:MinJackpotBalance","translator=bean:numTranslator"})
	public abstract TextField getMinJackpotBalanceField();
	
	@Component(id="updateSubmit",
			bindings={"disabled=!allowWrite()"})
			public abstract Submit getUpdateSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	public String getTitle(){
		if (getConfigID() != null){
			return getMsg("Edit")+" "+getMsg("Corporate")+" "+getMsg("Jackpot");
		} else if (getGameName() == null && getCorpUsername() == null){
			return getMsg("Edit")+" Mega "+getMsg("Jackpot");
		} else if (getGameName() != null){
			return getMsg("Edit")+" "+getGameName()+" "+getMsg("Jackpot");
		} else {
			return getMsg("Edit")+" "+getMsg("Corporate")+" "+getCorpUsername()+" "+getMsg("Jackpot");
		}
	}

	@Override
	public void pageBeginRender(PageEvent pEvt){
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {



			try {
				if (getConfigID() != null){
					stmt = conn.prepareStatement(
							"SELECT initial_amount, maximum_amount, bet_contrib, draw_number, min_payout, min_jackpot_balance"+
							" FROM jackpot_configs" +
					" WHERE config_id = ?");
					stmt.setInt(1, getConfigID());

					rs = stmt.executeQuery();

					if (rs.next()){
						setInitialAmount(rs.getDouble("initial_amount"));
						setMaxAmount(rs.getDouble("maximum_amount"));
						setContrib(rs.getDouble("bet_contrib"));
						setDrawNumber(rs.getInt("draw_number"));
						setMinJackpotBalance(rs.getDouble("min_jackpot_balance"));
						setMinPayout(rs.getDouble("min_payout"));
					}
					setMystery(true);
				} else if (getJackpotID() != null){
					
					stmt = conn.prepareStatement(
							"SELECT initial_amount, maximum_amount, pay_count, bet_contrib, auto, bet_amount," +
							" game_id, draw_number, min_payout, min_jackpot_balance," +
							" users.username"+
							" FROM jackpots" +
							" LEFT OUTER JOIN users ON jackpots.target_corporate = users.user_id" +
					" WHERE jackpot_id = ?");
					stmt.setInt(1, getJackpotID());
					
					rs = stmt.executeQuery();

					
					if (rs.next()){
						setInitialAmount(rs.getDouble("initial_amount"));
						setMaxAmount(rs.getDouble("maximum_amount"));
						setPayTimes(rs.getInt("pay_count"));
						setContrib(rs.getDouble("bet_contrib"));
						
						Integer gameId = DbUtil.getInteger(rs, "game_id");
						
						setGameID(gameId);
						setMystery(!rs.getBoolean("auto"));
						if (rs.getObject("bet_amount") != null){
							setBetTypeJackpot(true);
						} else {
							setBetTypeJackpot(false);
						}
						setDrawNumber(rs.getInt("draw_number"));
						setMinJackpotBalance(rs.getDouble("min_jackpot_balance"));
						setMinPayout(rs.getDouble("min_payout"));
						setCorpUsername(rs.getString("username"));
						
						
						
						if (gameId != null) {
							GameBean game = getGamesService().getGame(gameId);
							
							setGameName(game.getName());
																					
							GameTemplateInfo gameInfo = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
							setMJAllowed(gameInfo.supportsMysteryJackpot());
							setJAllowed(gameInfo.supportsGameJackpot());	
						}
						
					}
				}
			} catch (SQLException e) {
				log.error("Error getting jackpot properties.",e);
			} catch (ServiceException e) {
				log.error("Error getting jackpot properties", e);
			} finally {
				DbUtil.close(rs);
				DbUtil.close(stmt);
			}

		}finally {

			DbUtil.close(conn);
		}
	}

	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		if (delegate.getHasErrors()) {
			return;
		} 
		
		if(getJackpotID() == null) {
			delegate.record(null, getMsg("invalid-request"));
			return;
		}
		
		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			delegate.record(null, getMsg("db-error"));
			return;
		}
		
		PreparedStatement jackpotStmt = null;
		ResultSet jackpotRs = null;
		
		try {
			updateJackpotAllowed(conn);
			if (isMJAllowed() && !isJAllowed()) {
				setMystery(true);
			} else if (!isMJAllowed() && isJAllowed()) {
				setMystery(false);
			}
			
			if (isMystery() && getDrawNumber() <= 0){
				delegate.record(getDrawNumberField(), getMsg("draw-num-required"));
				return;
			}
			
			
			if (getConfigID() != null){
				String sql = "SELECT change_jackpot_properties(NULL,?,NULL,?,?,NULL,?,?,?,NULL,?,?,?)";
				jackpotStmt = conn.prepareStatement(sql);
				jackpotStmt.setInt(1, getConfigID());
				jackpotStmt.setBigDecimal(2, new BigDecimal(getInitialAmount()));
				jackpotStmt.setBigDecimal(3, new BigDecimal(getContrib()));

				if (getMaxAmount() == 0.0){
					jackpotStmt.setObject(4, null);
				} else {
					jackpotStmt.setBigDecimal(4, new BigDecimal(getMaxAmount()));
				}

				jackpotStmt.setInt(5, getDrawNumber());
				jackpotStmt.setLong(6, getPrincipal().getUserId());
				if (getMinPayout() > 0){
					jackpotStmt.setBigDecimal(7, new BigDecimal(getMinPayout()));
				} else {
					jackpotStmt.setObject(7, null);
				}
				if (getMinJackpotBalance() > 0){
					jackpotStmt.setBigDecimal(8, new BigDecimal(getMinJackpotBalance()));
				} else {
					jackpotStmt.setObject(8, null);
				}
			} else {
				String sql = "SELECT change_jackpot_properties(?,NULL,?,?,?,?,?,?,?,?,?,?,?)";

				jackpotStmt = conn.prepareStatement(sql);
				jackpotStmt.setInt(1, getJackpotID());
				if (getPayTimes() != null){
					jackpotStmt.setInt(2, getPayTimes());
				} else {
					jackpotStmt.setObject(2, null);
				}
				jackpotStmt.setBigDecimal(3, new BigDecimal(getInitialAmount()));
				if (getBetTypeJackpot()){
					jackpotStmt.setObject(4, null);
					jackpotStmt.setBigDecimal(5, new BigDecimal(getContrib()));
				} else {
					jackpotStmt.setBigDecimal(4, new BigDecimal(getContrib()));
					jackpotStmt.setObject(5, null);
				}
				if (getMaxAmount() == 0.0){
					jackpotStmt.setObject(6, null);
				} else {
					jackpotStmt.setBigDecimal(6, new BigDecimal(getMaxAmount()));
				}
				if (getDrawNumber() > 0){
					jackpotStmt.setInt(7, getDrawNumber());
				} else {
					jackpotStmt.setObject(7, null);
				}
				jackpotStmt.setLong(8, getPrincipal().getUserId());
				jackpotStmt.setBoolean(9, !((getGameID() == null) || isMystery()));
				jackpotStmt.setBigDecimal(10, null);
				if (getMinPayout() > 0){
					jackpotStmt.setBigDecimal(11, new BigDecimal(getMinPayout()));
				} else {
					jackpotStmt.setObject(11, null);
				}
				if (getMinJackpotBalance() > 0){
					jackpotStmt.setBigDecimal(12, new BigDecimal(getMinJackpotBalance()));
				} else {
					jackpotStmt.setObject(12, null);
				}
			}

			jackpotRs = jackpotStmt.executeQuery();

			redirectToPage("Jackpots");
		} catch (SQLException e) {
			log.warn("Error while updating jackpot.", e);
			delegate.setFormComponent(null);
			delegate.record(getMsg("jackpot-update-error"), null);
		} finally {
			DbUtil.close(jackpotRs);
			DbUtil.close(jackpotStmt);
			DbUtil.close(conn);
		}
	}

	private void updateJackpotAllowed(Connection conn) {
		if (getGameID() == null || getGameID() <= 0) {
			setMJAllowed(true);
			setJAllowed(false);
			return;
		}

		GameBean game;
		try {
			game = getGamesService().getGame(getGameID());
		} catch (ServiceException e) {
			log.error("Unable to get game from service)");
			return;
		}

		GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());

		setMJAllowed(gameTemplate.supportsMysteryJackpot());
		setJAllowed(gameTemplate.supportsMysteryJackpot());
	}

	public void onCancel(IRequestCycle cycle){
		redirectToPage("Jackpots");
	}

	public String getContribLabelMsg() {
		if (getBetTypeJackpot()){
			return getMessages().getMessage("BetAmount");
		}
		return getMessages().getMessage("ContributionRate");
	}
}