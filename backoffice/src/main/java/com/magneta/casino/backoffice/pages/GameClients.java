package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.GameClientGridDataSource;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public class GameClients {
	
	@Inject
	private GameClientService gameClientService;
	
	@Inject
	private Messages messages;
	
	@Property
	private GameClientBean gameClient;

	public GameClientGridDataSource getDataSource() {
		return new GameClientGridDataSource(gameClientService);
	}
	
	@OnEvent(value=EventConstants.ACTION, component="delete")
	@RequiredPrivilege(value=PrivilegeEnum.SYSTEM_ADMIN, write=true)
	public void deleteGameClient(Integer clientId) throws ServiceException {
		gameClientService.deleteGameClient(clientId);
	}
	
	public String getConfirmMessage() {
		return messages.format("confirm-message", gameClient.getClientId());		
	}
}
