package com.magneta.casino.backoffice.t4.pages.gameconfig;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.ReelBean;
import com.magneta.administration.beans.ReelSetBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.slots.SlotGameActionFilter;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.slots.SlotConfiguration;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public abstract class SlotCreateConfig extends AbstractCreateGameConfigPage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(SlotCreateConfig.class);
	
	private SlotConfiguration config;

	public abstract ReelSetBean[] getReelSets();
	public abstract void setReelSets(ReelSetBean[] reelSets);
	public abstract ReelSetBean getCurrReelSet();
	public abstract Integer getCurrReelSetIndex();
	public abstract Integer getCurrReelIndex();
	public abstract ReelBean getCurrReel();

	public abstract void setCombos(GameComboBean[] combos);
	public abstract GameComboBean[] getCombos();

	public abstract GameComboBean getCurrCombo();
	public abstract Integer getCurrComboIndex();

	public abstract ComboSymbolBean getCurrSymbol();
	
	public abstract GameTemplateSetting getCurrSetting();
	public abstract Integer getCurrSettingIndex();
	
	public abstract GameTemplateSetting[] getSettings();
	public abstract void setSettings(GameTemplateSetting[] s);

	@Component(id="gameName",bindings={"value=gameName"})
	public abstract Insert getGameNameField();

	@Bean(initializer="min=0.0")
	public abstract Min getMinAmount();

	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Component(id="createConfigForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
			public abstract Form getCreateConfigForm();
	
	@Component(type="For",
			bindings={"source=settings","value=currSetting","index=CurrSettingIndex","renderTag=false"})
			public abstract ForBean getSettingsFor();

	@Component(id="reelsets", type="For",
			bindings={"source=Reelsets","value=CurrReelSet","index=CurrReelSetIndex","renderTag=false"})
			public abstract ForBean getReelSetsFor();

	@Component(id="reels", type="For",
			bindings={"source=CurrReelSet.getReels","value=CurrReel","index=CurrReelIndex","renderTag=false"})
			public abstract ForBean getReelFor();

	@Component(id="comment",
			bindings={"value=comment","displayName=message:Comment","validators=validators:required,maxLength=255","disabled=!allowWrite()"})
			public abstract TextField getCommentField();

	@Component(id="commentLabel",
			bindings={"field=component:comment"})
			public abstract FieldLabel getCommentLabel();

	@Component(id="combos", type="For",
			bindings={"source=combos","value=currCombo", "index=currComboIndex", "renderTag=false"})
			public abstract ForBean getCombosFor();

	@Component(id="comboSymbols", type="For",
			bindings={"source=currCombo.symbols","value=currSymbol","renderTag=false"})
			public abstract ForBean getComboSymbols();

	@Component(id="saveSubmit",
			bindings={"disabled=!allowWrite()"})
			public abstract Submit getSaveSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();

	@Override
	public void pageValidate(PageEvent event) {
		super.pageValidate(event);
		if (!getAccessValidator().checkWriteAccess(this.getPageName())){
			redirectToPage("AccessDenied");
		}
	}

	@Override
	public void pageBeginRender(PageEvent evt){
		super.pageBeginRender(evt);
		int numOfReelSets;
		ComboSymbolBean[] symbols;
		try {
			config = (SlotConfiguration)getGameConfigsService().getFilteredConfiguration(getGame(), getConfigID());
		} catch (ServiceException e) {
			log.error("Unable to load game config", e);
			getErrorDisplay().getDelegate().record(null, getMsg("invalid-symbol-found"));
			return;
		}

		{
			SlotGameActionFilter f;
			try {
				f = (SlotGameActionFilter)ServiceLocator.getService(GameActionFilterService.class).getFilter(this.getGame(), config);
				setCombos(f.getPaytable());
				symbols = f.getSymbols();
			} catch (ServiceException e) {
				log.error("Unable to load game action filter", e);
				getErrorDisplay().getDelegate().record(null, getMsg("invalid-symbol-found"));
				return;
			}
		}

		numOfReelSets = config.getNumberOfReelSets();
		ReelSetBean[] reelSets = new ReelSetBean[numOfReelSets];
		for(int i=0;i<numOfReelSets;i++) {
			int[][] reelsNum = config.getReels(i);
			int numOfReels = config.getNumberOfReels(i);
			reelSets[i] = new ReelSetBean();
			ReelBean[] reels = new ReelBean[numOfReels];
			for(int z=0;z<numOfReels;z++) {
				int[] reel = reelsNum[z];
				StringBuilder builder = new StringBuilder();
				for(int k=0;k<reel.length;k++) {
					if (k > 0) {
						builder.append(",");
					}
					builder.append(reel[k]);
				}
				reels[z] = new ReelBean(builder.toString(),symbols);

			}
			reelSets[i].setReels(reels);
		}
		setReelSets(reelSets);
		
		GameTemplateSetting[] defaultSettings = config.getDefaultSettings();
		List<GameTemplateSetting> settings = new ArrayList<GameTemplateSetting>();
		
		if (defaultSettings != null) {
			for (GameTemplateSetting s: defaultSettings) {
				GameTemplateSetting newS = new GameTemplateSetting(s);
				newS.setDefaultValue(config.getSetting(s.getKey()));
				settings.add(newS);
			}
		}
	
		this.setSettings(settings.toArray(new GameTemplateSetting[settings.size()]));
	}

	public void setReelBeingEdited(String reel) {
		getReelSets()[getCurrReelSetIndex()].getReels()[getCurrReelIndex()].setReel(reel);
	}

	public String getReelBeingEdited() {
		return getReelSets()[getCurrReelSetIndex()].getReels()[getCurrReelIndex()].getReel();
	}

	public void setPayoutBeingEdited(Double payout) {
		getCombos()[getCurrComboIndex()].setPayout(payout);
	}

	public Double getPayoutBeingEdited() {
		return getCombos()[getCurrComboIndex()].getPayout();
	}
	
	public void setSettingBeingEdited(Object val) {
		getSettings()[getCurrSettingIndex()].setDefaultValue(val);
	}

	public Object getSettingBeingEdited() {
		return getSettings()[getCurrSettingIndex()].getDefaultValue();
	}

	public void onSubmit(IRequestCycle cycle){
		if (getErrorDisplay().getDelegate().getHasErrors()){
			return;
		}

		StringBuilder finalConfig = new StringBuilder();
		for (int i = 0;i<getReelSets().length;i++) {
			StringBuilder reelBuilder = new StringBuilder();
			for(ReelBean reel: getReelSets()[i].getReels()) {
				reelBuilder.append(reel.getReel().replaceAll("\\s", ""));
				if (!reel.getReel().endsWith("|")){
					reelBuilder.append("|\n");
				}
			}
			int expLen = config.getNumberOfReels(i);
			String reelString = reelBuilder.toString();
			if (!validateReelsString(reelString, expLen, config.getMinSymbol(), config.getSymbolCount())){
				getErrorDisplay().getDelegate().setFormComponent(null);
				getErrorDisplay().getDelegate().record(getMsg("invalid-symbol-found"), null);
				return;
			}
			finalConfig.append(reelString).append(";\n");
		}

		DecimalFormat f = new DecimalFormat("0.00");
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		f.setDecimalFormatSymbols(symbols);

		for(int i=0;i<getCombos().length;i++) {
			if(i > 0) {
				finalConfig.append(':');
			}
			finalConfig.append(i).append('=').append(f.format(getCombos()[i].getPayout()));
		}
		
		/* Save settings */
		GameTemplateSetting[] settings = this.getSettings();
		if (settings != null && settings.length > 0) {
			
			for (GameTemplateSetting s: settings) {
				finalConfig.append(';');
				finalConfig.append(s.getKey());
				finalConfig.append('=');
				finalConfig.append(s.getDefaultValue());
			}
		}
		
		com.magneta.casino.services.beans.GameConfigBean bean = new com.magneta.casino.services.beans.GameConfigBean();
		bean.setGameId(getGame());
		bean.setConfigComment(getComment());
		bean.setConfigValue(finalConfig.toString());

		try {
			getGameConfigsService().insertConfig(bean);
			this.setConfigID(bean.getConfigId());
		} catch (ServiceException e1) {
			log.error("Error while creating gameConfiguration", e1);
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
			return;
		}

		throw new RedirectException(LinkGenerator.getT5Url("GameConfigs", bean.getGameId()));
	}

	private static boolean validateReelsString(String reelsString, int noOfReels, int minSymbol, int maxSymbol){
		String regex = "[[0-9]{1}[,[0-9]{1}]*\\|{1}]+";
		if (reelsString != null){
			reelsString =  reelsString.replaceAll("\\s", "");
			if (reelsString.matches(regex)){
				String[] tmp = reelsString.split("\\|{1}");
				if (tmp.length < noOfReels){
					return false;
				}
				for (int i=0;i<tmp.length;i++){
					String[] nums = tmp[i].split(",");
					for (int j=0;j<nums.length;j++){
						int sym = Integer.parseInt(nums[j]);
						if (sym < minSymbol || sym > maxSymbol){
							return false;
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	public String reelDisplayName() {
		StringBuilder builder = new StringBuilder("Config reelset:").append(this.getCurrReelSetIndex() + 1).append(" reel:").append(this.getCurrReelIndex() + 1);
		return builder.toString();
	}

	public String comboDisplayName() {
		StringBuilder builder = new StringBuilder("Config combo:").append(this.getCurrComboIndex() + 1);
		return builder.toString();
	}



}
