package com.magneta.casino.backoffice.t4.pages.reporting;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.PropertySelection;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.AffiliatesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(and={PrivilegeEnum.REPORTS,PrivilegeEnum.CORPORATE})
public abstract class CorporateGamingRevenue extends ReportOptions {
	
	@Component(bindings = {"value=corporate", "model=ognl:affiliatesSelectionModel", "displayName=message:Corporate"})
	public abstract PropertySelection getCorporateSelection();

	public AffiliatesSelectionModel getAffiliatesSelectionModel(){
		return new AffiliatesSelectionModel(0L,true,false);
	}
	
	@Component(bindings={"value=ShowSubs", "displayName=message:ShowSubs"})
	public abstract Checkbox getShowSubsCB();
	
	@Component(bindings={"value=minDate","displayName=message:From", "translator=translator:date,pattern=dd/MM/yyyy"})
	public abstract DatePicker getFromDate();

	@Component(bindings={"value=maxDate","displayName=message:To", "translator=translator:date,pattern=dd/MM/yyyy"})
	public abstract DatePicker getToDate();
	
	public abstract Date getMinDate();
	public abstract Date getMaxDate();
	
	@InitialValue("-1")
	public abstract Long getCorporate();
	public abstract boolean getShowSubs();
	
	/* Removes the time part of the min date */
	private Date removeTimeMin(Date d) {
		if (d == null) {
			return null;
		}
		
		Calendar calendar = Calendar.getInstance(getTZ());
    	calendar.setTimeInMillis(d.getTime());        	
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
    	calendar.set(Calendar.MINUTE, 0);
    	calendar.set(Calendar.SECOND, 0);
    	calendar.set(Calendar.MILLISECOND, 0);
    	
    	return calendar.getTime();
	}
	
	/* Removes the time part of the max date */
	private Date removeTimeMax(Date d) {
		if (d == null) {
			return null;
		}
		
		Calendar calendar = Calendar.getInstance(getTZ());
    	calendar.setTimeInMillis(d.getTime());	
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
    	calendar.set(Calendar.MINUTE, 0);
    	calendar.set(Calendar.SECOND, 0);
    	calendar.set(Calendar.MILLISECOND, 0);
    	calendar.add(Calendar.DAY_OF_MONTH, 1);
    	
    	return calendar.getTime();
	}
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);

		TimeZone tz = getTZ();
		
		Date minDate = removeTimeMin(normalizeDate(getMinDate()));
		Date maxDate = removeTimeMax(normalizeDate(getMaxDate()));
		
		//NORMALIZE DATES
        if (minDate != null){       	
        	/* Convert min date to UTC day start.
        	 * This is important since the database daily records are using UTC */
        	long utcMinDate = minDate.getTime() + tz.getOffset(minDate.getTime());
 	
            reportParams.put("min_date", new Timestamp(utcMinDate));
        }
		
        if (maxDate != null){       	
        	/* Convert max date to UTC day start.
        	 * This is important since the database daily records are using UTC */
        	long utcMaxDate = maxDate.getTime() + tz.getOffset(maxDate.getTime());

            reportParams.put("max_date", new Timestamp(utcMaxDate));
        }

		reportParams.put("corp_id",getCorporate());
		reportParams.put("show_subs",getShowSubs());
	}
}
