package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.GameSetting;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class EditGameProperties extends SecurePage implements PageBeginRenderListener {
	
	private static final Logger log = LoggerFactory.getLogger(EditGameProperties.class);
	
	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();
	
	public abstract double getMinBet();
	public abstract void setMinBet(double minBet);
	public abstract double getMaxBet();
	public abstract void setMaxBet(double maxBet);
	
	@Persist("client:page")
	public abstract int getGameID();
	public abstract void setGameID(int gameID);
	
	public abstract void setGameName(String gameName);
	
	public abstract void setSettings(List<GameSetting> settings);
	public abstract List<GameSetting> getSettings();
	
	@Persist("client:page")
	public abstract ICallback getCallback();
	public abstract void setCallback(ICallback callback);
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();
	
	public abstract GameSetting getCurrSetting();
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	@Component(id="editGameForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getEditGameForm();
	
	@Component(id="minBet",
			bindings={"value=minBet","displayName=message:MinBet","translator=bean:numTranslator","validators=validators:$minAmount",
			"disabled=!allowWrite()"})
	public abstract TextField getMinBetField();
	
	@Component(id="minBetLabel",
			bindings={"field=component:minBet"})
	public abstract FieldLabel getMinBetLabel();
	
	@Component(id="maxBet",
			bindings={"value=maxBet","displayName=message:MaxBet","translator=bean:numTranslator","validators=validators:$minAmount",
			"disabled=!allowWrite()"})
	public abstract TextField getMaxBetField();
	
	@Component(id="maxBetLabel",
			bindings={"field=component:maxBet"})
	public abstract FieldLabel getMaxBetLabel();
	
	@Component(id="settings", type="For",
			bindings={"source=settings","value=currSetting","element=literal:tr"})
	public abstract ForBean getSettingsFor();
	
	@Component(id="settingName",
			bindings={"value=currSetting.settingName"})
	public abstract Insert getSettingName();
	
	@Component(id="viewConfigLink",
			bindings={"page=literal:GameConfigs","parameters={gameID}"})
	public abstract CasinoLink getViewConfigLink();
	
	@Component(id="updateSubmit",
			bindings={"disabled=!allowWrite()"})
			public abstract Submit getUpdateSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Override
	public void pageBeginRender(PageEvent event){
		
		int gameId = getGameID();
		
		if (gameId <= 0) {
			throw new BadRequestException("Invalid/missing gameId");
		}
		
		GamesService gameService = this.getGamesService();
		
		GameBean game;
		try {
			game = gameService.getGame(gameId);
		} catch (ServiceException e1) {
			log.error("Unable to get game " + gameId, e1);
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
			return;
		}
		
		setGameName(game.getName());
		
		GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
		
		List<GameTemplateSetting> templateSettings = new ArrayList<GameTemplateSetting>(); 
		
		gameTemplate.getSettings(templateSettings);
		
		List<GameSetting> settings = new ArrayList<GameSetting>();
		for (GameTemplateSetting s: templateSettings) {
			GameSetting setting = new GameSetting();
			setting.setSettingName(s.getDescription());
			setting.setSettingType(s.getType());

			Object defaultValue = s.getDefaultValue();
			
			if (defaultValue != null) {
				setting.setSettingValue(String.valueOf(defaultValue), s.getType());
			}
			setting.setSettingKey(s.getKey());
			setting.setGameID(gameId);
			settings.add(setting);
		}
		setSettings(settings);
		
		Connection dbConn = ConnectionFactory.getConnection();
		
		if (dbConn == null){
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
			return;
		}
		
		try {
			PreparedStatement statement = null;
			ResultSet results = null;
			try {
				String sql = 
					"SELECT min_bet, max_bet" +
					" FROM game_types" +
					" WHERE game_type_id = ?";
				statement = dbConn.prepareStatement(sql);
				statement.setInt(1, getGameID());
				
				results = statement.executeQuery();
				
				if (results.next()){
					setMinBet(results.getDouble("min_bet"));
					setMaxBet(results.getDouble("max_bet"));
				}
			} finally {
				DbUtil.close(results);
				DbUtil.close(statement);
			}
			
			PreparedStatement settingsStatement = null;
			ResultSet settingsResults = null;
			try {
				String sql = 
					"SELECT setting_key, setting_value" +
					" FROM game_settings" +
					" WHERE game_id = ?" +
					" ORDER BY setting_key";
				settingsStatement = dbConn.prepareStatement(sql);
				settingsStatement.setInt(1, getGameID());
				
				settingsResults = settingsStatement.executeQuery();

				while (settingsResults.next()){
					String key = settingsResults.getString("setting_key");
					boolean settingFound = false;
					for (GameSetting setting: settings) {
						if (setting.getSettingKey().equals(key)) {
							setting.setSettingValue(settingsResults.getString("setting_value"), setting.getSettingType());
							settingFound = true;
							break;
						}
					}
					
					if (!settingFound) {
						/* Check if setting is hidden */
						for (GameTemplateSetting s: templateSettings) {
							if (s.getKey().equals(key)) {
								settingFound = true;
								break;
							}
						}
						
						if (!settingFound) {
							log.warn("Unknown game setting " + key + " for game "+ gameId + " in database!");
						}
					}
				}
				
			} finally {
				DbUtil.close(settingsResults);
				DbUtil.close(settingsStatement);
			}
			
		} catch (SQLException e){
			log.error("Error while loading game properties.", e);
		} finally {
			DbUtil.close(dbConn);
		}
	}
	
	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		if (delegate.getHasErrors()){
			return;
		}
		
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		if (dbConn == null){
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
			return;
		}
		
		String sql = 
			"UPDATE game_types" +
			" SET min_bet = ?, max_bet = ?" +
			" WHERE game_type_id = ?";
		
		try {
			dbConn.setAutoCommit(false);
			
			statement = dbConn.prepareStatement(sql);
			
			if (getMinBet() > 0.0){
				statement.setBigDecimal(1, new BigDecimal(getMinBet()));
			} else {
				statement.setObject(1, null);
			}
			
			if (getMaxBet() > 0.0){
				statement.setBigDecimal(2, new BigDecimal(getMaxBet()));
			} else {
				statement.setObject(2, null);
			}
			statement.setInt(3, getGameID());
			statement.execute();
			DbUtil.close(statement);
			
			for (GameSetting setting: getSettings()){
				setting.update(dbConn, getPrincipal().getUserId());
			}
			
			dbConn.commit();
			
			if(getCallback() == null) {
				redirectToPage("Games");
			} else {
				getCallback().performCallback(cycle);
			}
		} catch (SQLException e) {
			try {
				dbConn.rollback();
			} catch (SQLException e1) {
				log.error("Error rolling back",e1);
			}
			log.error("Error while updating game properties.", e);
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
		} finally {
			DbUtil.close(dbConn);
		}
	}
	
	public void onCancel(IRequestCycle cycle){
		if(getCallback() == null) {
			redirectToPage("Games");
		} else {
			getCallback().performCallback(cycle);
		}
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){    	
			setGameID((Integer)params[0]);
    	}
	}

	public boolean isConfApplicable() throws ServiceException {
		
		if (!getPrincipal().hasPrivilege(PrivilegeEnum.GAME_CONFIG, false)) {
			return false;
		}
		
		GameBean game = getGamesService().getGame(getGameID());
		
		if (game == null)
			return false;
		
		GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
		
		return gameTemplate.supportsConfiguration();
	}
}
