package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.enums.GameCategoryEnum;

public class GameCategorySelectModel extends AbstractSelectModel {

	private final Locale locale;
	
	public GameCategorySelectModel(Locale locale) {
		this.locale = locale;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		
		GameCategoryEnum[] categories = GameCategoryEnum.values();

		List<OptionModel> model = new ArrayList<OptionModel>(categories.length);
		
		for (GameCategoryEnum category: categories) {
			model.add(new OptionModelImpl(category.getName(locale), category.getId()));
		}
		
		return model;
	}

}
