package com.magneta.casino.backoffice.pages;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.slf4j.Logger;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public class FinancialChart {
	
	@Inject
	private Logger log;

	private static final int CHART_DAYS = 31;
	
	public JFreeChart getChart() {
		Connection conn = ConnectionFactory.getReportsConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        if (conn == null){
			return null;
		}
        
        TimeSeries bets = new TimeSeries("Bets");
    	TimeSeries wins = new TimeSeries("Wins");
    	TimeSeries revenue = new TimeSeries("Revenue");
    	
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
    	cal.add(Calendar.DAY_OF_YEAR, -CHART_DAYS);
		for (int i=0;i<=CHART_DAYS;i++){
			Day d = new Day(cal.getTime());
			bets.add(d, 0.0);
			wins.add(d, 0.0);
			revenue.add(d, 0.0);
			cal.add(Calendar.DAY_OF_YEAR, 1);
    	}
    	
        try {
        	stmt = conn.prepareStatement(
        			"SELECT period_date, SUM(real_bets) AS bets, COALESCE(SUM(real_wins),0.0) + COALESCE(SUM(jackpot_wins)) AS wins," + 
        			" SUM(real_bets) - COALESCE(SUM(real_wins),0.0) + COALESCE(SUM(jackpot_wins)) AS revenue" +
        			" FROM game_period_amounts" +
        			" WHERE period_date > now_utc() - CAST(? AS INTERVAL)" +
        			" GROUP BY period_date" +
        			" ORDER BY period_date");

        	stmt.setString(1, CHART_DAYS+" days");
        	
        	rs = stmt.executeQuery();
        	
        	while (rs.next()){
        		Day d = new Day(rs.getTimestamp("period_date"));
        		bets.update(d, rs.getDouble("bets"));
        		wins.update(d, rs.getDouble("wins"));
        		revenue.update(d, rs.getDouble("revenue"));
        	}
        } catch (SQLException e) {
        	log.warn("Error while getting chart data.", e);
        } finally {
        	DbUtil.close(rs);
        	DbUtil.close(stmt);
            DbUtil.close(conn);
        }
		
        TimeSeriesCollection xyDataset = new TimeSeriesCollection();
        xyDataset.addSeries(bets);
        xyDataset.addSeries(wins);
        xyDataset.addSeries(revenue);
		
		JFreeChart ch = ChartFactory.createTimeSeriesChart("Bets, wins and gross revenue of the last "+CHART_DAYS+" days", "Day", "Amount", xyDataset,
				true, false, false);
        ch.setBackgroundPaint(Color.white);
        DateAxis axis = (DateAxis)ch.getXYPlot().getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("dd MMM"));
        return ch;
	}
}
