package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import com.magneta.administration.beans.UsersSearchResultBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.AffiliateUsersTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class PartnerUsers extends SecurePage implements PageBeginRenderListener {
	
	public abstract void setCorpUsername(String username);
	
	@InitialValue("0")
	@Persist
	public abstract void setAffiliate(Long affiliate);
	public abstract Long getAffiliate();
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract UsersSearchResultBean getCurrUser();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:affiliateUsersTableModel","columns=literal: !Username, !FirstName, !LastName",
			"pageSize=30","rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:Username","row=currUser","initialSortOrder=true"})
	public abstract Table getUsers();
	
	public AffiliateUsersTableModel getAffiliateUsersTableModel(){
		return new AffiliateUsersTableModel(getAffiliate());	
	}
	
	@Component(bindings={"userId=ognl:currUser.Id","target=literal:_blank"})
	public abstract UserLink getUsernameLink();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setAffiliate((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent evt){
		setCorpUsername(getUsername(getAffiliate()));
	}
}
