package com.magneta.casino.backoffice.t4.pages.gameconfig;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.configuration.poker.texasholdem.WinCombination;
import com.magneta.games.configuration.poker.texasholdem.WinType;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public abstract class TexasHoldEmPokerCreateConfig extends AbstractCreateGameConfigPage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(TexasHoldEmPokerCreateConfig.class);
	
	@Persist("client:page")
	public abstract GameComboBean[] getCombos();
	public abstract void setCombos(GameComboBean[] combos);
	
	@Persist("client:page")
	public abstract GameComboBean[] getSideCombos();
	public abstract void setSideCombos(GameComboBean[] combos);

	public abstract GameComboBean getCurrCombo();
	public abstract ComboSymbolBean getCurrSymbol();
	
	@Bean(initializer="min=0.0")
	public abstract Min getMinAmount();
	
	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Component(id="gameName",
			bindings={"value=gameName"})
			public abstract Insert getGameNameField();
	
	@Component(id="createConfigForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
			public abstract Form getCreateConfigForm();
	
	@Component(id="comment",
			bindings={"value=comment","displayName=message:Comment","validators=validators:required,maxLength=255","disabled=!allowWrite()"})
			public abstract TextField getCommentField();
	
	@Component(id="commentLabel",
			bindings={"field=component:comment"})
			public abstract FieldLabel getCommentLabel();
	
	@Component(id="combos", type="For",
			bindings={"source=combos","value=currCombo","renderTag=false"})
			public abstract ForBean getCombosFor();

	@Component(id="comboPayout",
			bindings={"value=currCombo.payout","validators=validators:required,$minAmount","translator=bean:numTranslator","disabled=!allowWrite()"})
			public abstract TextField getComboPayout();
	
	@Component(id="sideCombos", type="For",
			bindings={"source=sideCombos","value=currCombo","renderTag=false"})
			public abstract ForBean getSideCombosFor();
	
	@Component(id="sideComboPayout",
			bindings={"value=currCombo.payout","validators=validators:required,$minAmount","translator=bean:numTranslator","disabled=!allowWrite()"})
			public abstract TextField getSideComboPayout();

	@Component(id="saveSubmit",
			bindings={"disabled=!allowWrite()"})
			public abstract Submit getSaveSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Override
	public void pageValidate(PageEvent event) {
		super.pageValidate(event);
		if (!getAccessValidator().checkWriteAccess(this.getPageName())){
			redirectToPage("AccessDenied");
		}
	}
	
	private String getComboDescription(int combo) {
		return this.getMsg("combo-" + String.valueOf(combo));
	}

	private String getSideComboDescription(int combo) {
		return this.getMsg("side-combo-" + String.valueOf(combo));
	}

	@Override
	public void pageBeginRender(PageEvent evt){		
		super.pageBeginRender(evt);	
		
		TexasHoldemConfiguration config;
		try {
			config = (TexasHoldemConfiguration)getGameConfigsService().getFilteredConfiguration(getGame(), getConfigID());
		} catch (ServiceException e) {
			log.error("Unable to load game config", e);
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("invalid-symbol-found"), null);
			return;
		}
		
		List<WinCombination> winCombos = config.getWinCombinations();
		
		GameComboBean[] winCs = new GameComboBean[winCombos.size()];
		
		int i=0;
		for (WinCombination c: winCombos) {
			winCs[i] = new GameComboBean(null, getComboDescription(c.getTriggerCombo()), c.getAmount(), true);
					
			i++;
		}

		setCombos(winCs);
		
		List<WinCombination> sideGameWinCombos = config.getSideWinCombinations();
		
		GameComboBean[] jWinCs = new GameComboBean[sideGameWinCombos.size()];
		
		i=0;
		for (WinCombination c: sideGameWinCombos) {
			
			String description = getSideComboDescription(c.getTriggerCombo());
			double amount = c.getAmount();
			
			if (c.getType() == WinType.JACKPOT_POOL_RATIO) {
				description += (" (%)");
				
				amount *= 100.0;
			}
			
			jWinCs[i] = new GameComboBean(null, description, amount, true);	
			i++;
		}

		setSideCombos(jWinCs);
	}

	public void onSubmit(IRequestCycle cycle) {
		if (getErrorDisplay().getDelegate().getHasErrors()){
			return;
		}

		TexasHoldemConfiguration config = new TexasHoldemConfiguration();
		
		List<WinCombination> originalSideWinCombinations = config.getSideWinCombinations();
		List<WinCombination> originalWinCombinations = config.getWinCombinations();
		
		GameComboBean[] winCombos = this.getCombos();
		GameComboBean[] sideCombos = this.getSideCombos();
		
		List<WinCombination> sideCombinations = new ArrayList<WinCombination>(sideCombos.length);
		List<WinCombination> winCombinations = new ArrayList<WinCombination>(winCombos.length);
		
		for (int i=0; i < winCombos.length; i++) {
			WinCombination orig = originalWinCombinations.get(i);
			winCombinations.add(new WinCombination(orig.getTriggerCombo(), orig.getType(), winCombos[i].getPayout()));
		}
		
		for (int i=0; i < sideCombos.length; i++) {
			WinCombination orig = originalSideWinCombinations.get(i);
			
			double payout = sideCombos[i].getPayout();
			if (orig.getType() == WinType.JACKPOT_POOL_RATIO) {
				payout = payout / 100.0;
			}
			
			sideCombinations.add(new WinCombination(orig.getTriggerCombo(), orig.getType(), payout));
		}
		
		config.setWinCombinations(winCombinations);
		config.setSideWinCombinations(sideCombinations);
		
		
		String conf;
		try {
			conf = config.serialize();
		} catch (JAXBException e) {
			getErrorDisplay().getDelegate().record(null, "Unable to serialize configuration");
			log.error("Unable to serialize configuration", e);
			return;
		}

		com.magneta.casino.services.beans.GameConfigBean bean = new com.magneta.casino.services.beans.GameConfigBean();
		bean.setGameId(getGame());
		bean.setConfigComment(getComment());
		bean.setConfigValue(conf);
		
		try {
			getGameConfigsService().insertConfig(bean);
		} catch (ServiceException e1) {
			log.error("Error while creating game Configuration", e1);
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
			return;
		}
		
		String url = LinkGenerator.getPageUrl("GameConfigs", getGame());
		throw new RedirectException(url);
	}
}
