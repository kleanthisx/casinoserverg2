package com.magneta.casino.backoffice.pages;

import java.io.IOException;
import java.io.InputStream;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Context;
import org.apache.tapestry5.services.HttpError;
import org.apache.tapestry5.services.Response;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.GameImagesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameSymbolBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.BACKOFFICE_ENTRY)
public class GameImage {

	@Inject
	private GameImagesService gameImagesService;
	
	@Inject
	private Context context;
	
	private static class ImageStreamResponse implements StreamResponse {

		private final InputStream is;
		private final String contentType;
		
		public ImageStreamResponse(InputStream is, String contentType) {
			this.is = is;
			this.contentType = contentType;
		}
		
		@Override
		public String getContentType() {
			return contentType;
		}

		@Override
		public InputStream getStream() throws IOException {
			return is;
		}

		@Override
		public void prepareResponse(Response response) {			
		}
		
	}
	
	
	@OnEvent(EventConstants.ACTIVATE)
	public Object activate(Integer gameId, String image) throws ServiceException, IOException {
		
		GameSymbolBean symbol = gameImagesService.getGameImage(gameId, image);
		
		if (symbol == null) {
			return new HttpError(404, "Image not found");
		}
		
		InputStream is = symbol.getSymbolUrl().openStream();
	    if (is == null) {
	    	return new HttpError(404, "Image not found");
	    }
	    
	    String mimeType = context.getMimeType(symbol.getSymbolUrl().getFile());

	    /* Unknown MIME type */
	    if (mimeType == null) {
	    	mimeType = "application/octet-stream";
	     }

	     return new ImageStreamResponse(is, mimeType);
	}
	
	@OnEvent(EventConstants.ACTIVATE)
	public Object activate() {
		return new HttpError(404, "Image not found");
	}
}
