package com.magneta.casino.backoffice.validators;

import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.ioc.MessageFormatter;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.validator.AbstractValidator;

public class MaxDoubleValidator extends AbstractValidator<Double, Double> {
        public MaxDoubleValidator() {
        	super(Double.class, Double.class, "max-double");
        }

        @Override
        public void validate(Field field, Double constraintValue, MessageFormatter formatter, Double value)
                        throws ValidationException {
        	if (constraintValue == null) {
        		return;
        	}
        	
        	if (constraintValue >= value)
        		throw new ValidationException(buildMessage(formatter, field, constraintValue));
        }

        private String buildMessage(MessageFormatter formatter, Field field, Double constraintValue) {
                return formatter.format(constraintValue, field.getLabel());
        }

        @Override
        public void render(Field field, Double constraintValue, MessageFormatter formatter, MarkupWriter writer,
                                                                                 FormSupport formSupport) {
                formSupport.addValidation(field, "foo", buildMessage(formatter, field, constraintValue), null);
        }
}