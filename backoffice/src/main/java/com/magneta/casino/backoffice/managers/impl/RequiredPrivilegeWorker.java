package com.magneta.casino.backoffice.managers.impl;

import javax.annotation.security.PermitAll;

import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.alerts.Duration;
import org.apache.tapestry5.alerts.Severity;
import org.apache.tapestry5.model.MutableComponentModel;
import org.apache.tapestry5.plastic.MethodAdvice;
import org.apache.tapestry5.plastic.MethodInvocation;
import org.apache.tapestry5.plastic.PlasticClass;
import org.apache.tapestry5.plastic.PlasticMethod;
import org.apache.tapestry5.services.transform.ComponentClassTransformWorker2;
import org.apache.tapestry5.services.transform.TransformationSupport;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.annotations.RequiredSetting;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

public class RequiredPrivilegeWorker implements ComponentClassTransformWorker2 {
	
	private final PrincipalService principalService;
	private final AlertManager alertManager;
	private final SettingsService settingsService;
	
	public RequiredPrivilegeWorker(PrincipalService principalService,AlertManager alertManager,
			SettingsService settingsService) {
		this.principalService = principalService;
		this.alertManager = alertManager;
		this.settingsService = settingsService;
	}

	@Override
	public void transform(PlasticClass arg0, TransformationSupport arg1,MutableComponentModel arg2) {
		for(PlasticMethod method:arg0.getMethodsWithAnnotation(RequiredPrivilege.class)) {
			method.addAdvice(new RequiredPrivilegeAdvice(alertManager, principalService, settingsService));
		}
	}
	
	private static class RequiredPrivilegeAdvice implements MethodAdvice {

		private final PrincipalService principalService;
		private final AlertManager alertManager;
		private final SettingsService settingsService;
		
		public RequiredPrivilegeAdvice(AlertManager alertManager, PrincipalService principalService,
				SettingsService settingsService) {
			this.principalService = principalService;
			this.alertManager = alertManager;
			this.settingsService = settingsService;
		}
		
		@Override
		public void advise(MethodInvocation arg0) {
			PermitAll pAll = arg0.getAnnotation(PermitAll.class);
			if (pAll != null) {
				arg0.proceed();
				return;
			}
			
			RequiredSetting settingAnnotation = arg0.getAnnotation(RequiredSetting.class);
			if (settingAnnotation != null) {					
					String value = settingAnnotation.val().trim();
					
					SettingBean setting;
					principalService.pushSystem();
					try {
						setting = settingsService.getSetting(settingAnnotation.value());
					} catch (ServiceException e) {
						alertManager.alert(Duration.SINGLE, Severity.ERROR, "Internal server error");
						return;
					} finally {
						principalService.popSystem();
					}
					if (setting == null || setting.getValue() == null) {
						if (!value.isEmpty()) {
							alertManager.alert(Duration.SINGLE, Severity.ERROR, "Access denied");
							return;
						}
					} else {
						if (!setting.getValue().trim().toLowerCase().equals(value.toLowerCase())) {
							alertManager.alert(Duration.SINGLE, Severity.ERROR, "Access denied");
							return;
						}
					}
			}
			
			
			RequiredPrivilege annon = arg0.getAnnotation(RequiredPrivilege.class);
			if (annon == null) {
				arg0.proceed();
				return;
			}
			
			if (!principalService.getPrincipal().hasPrivilege(annon.value(), annon.write())) {
				alertManager.alert(Duration.SINGLE, Severity.ERROR, "Access denied");
				return;
			}

			PrivilegeEnum[] privs = annon.and();

			if (privs != null) {
				for (PrivilegeEnum p: privs) {
					if (!principalService.getPrincipal().hasPrivilege(p, annon.write())) {
						alertManager.alert(Duration.SINGLE, Severity.ERROR, "Access denied");
						return;
					}
				}
			}
			
			PrivilegeEnum[] orPrivs = annon.or();
			
			if (orPrivs != null && orPrivs.length > 0) {
				boolean hasOne = false;
				
				for (PrivilegeEnum p: orPrivs) {
					if (principalService.getPrincipal().hasPrivilege(p, annon.write())) {
						hasOne = true;
						break;
					}
				}
				
				if (!hasOne) {
					alertManager.alert(Duration.SINGLE, Severity.ERROR, "Access denied");
					return;
				}
			}
			arg0.proceed();
		}
		
	}

}
