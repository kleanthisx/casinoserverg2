package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.GameTableTableModel;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class ClosedTables extends SecurePage implements PageBeginRenderListener {

	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();

	@Component(id="gameName",
			bindings={"value=gameName"})
			public abstract Insert getGameNameField();

	@Component(id="tables", type="contrib:Table",
			bindings={"source=ognl:gameTableTableModel","columns=literal:TableId, Owner",
			"pageSize=50","rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:TableId","row=currTable","initialSortOrder=true"})
			public abstract Table getTables();

	public GameTableTableModel getGameTableTableModel(){
		return new GameTableTableModel(getGameId(), null, true);
	}

	@Component(id="roundsLink",
			bindings={"page=literal:GameDetails","parameters={currTable.tableId, 0}"})
	public abstract CasinoLink getRoundsLink();

	@Component(id="tableId",
			bindings={"value=currTable.Id"})
			public abstract Insert getTableId();
	
    @Component(bindings={"userId=ognl:currTable.owner","target=literal:_blank"})
    public abstract UserLink getOwnerLink();

	@Component(id="gamesLink",
			bindings={"page=literal:Games"})
			public abstract CasinoLink getGamesLink();

	@Component(id="backLink",
			bindings={"page=literal:AvailableTables", "parameters=gameId" })
			public abstract CasinoLink getBackLink();

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract GameTableBean getCurrTable();
	
	@Persist("client:page")
	public abstract int getGameId();
	public abstract void setGameId(int id);

	public abstract void setGameName(String gameName);

	@Override
	public void pageBeginRender(PageEvent event) {
		if (getGameId() > 0){

			try {
				GameBean game = getGamesService().getGame(getGameId());
				if(game == null){
					return;
				}
				
				setGameName(game.getName());

			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){    	
			setGameId((Integer)params[0]);
		}
	}
}
