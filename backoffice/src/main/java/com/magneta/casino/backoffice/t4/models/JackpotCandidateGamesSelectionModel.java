package com.magneta.casino.backoffice.t4.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.search.SortContext;

public class JackpotCandidateGamesSelectionModel implements IPropertySelectionModel {

    private List<JackpotGame> gameIds;
    private List<String> gameVariants;
 
    // TODO get only the games without jackpot
    public JackpotCandidateGamesSelectionModel(GamesService gamesService) throws ServiceException {
    	gameIds = new ArrayList<JackpotGame>();
    	gameVariants = new ArrayList<String>();

    	ServiceResultSet<GameBean> games = gamesService.getGames(null, new SortContext<GameBean>("name"), 0, 0);

    	List<GameBean> gameList = games.getResult();

    	for (GameBean g: gameList) {
    		GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(g.getGameTemplate());

    		if (gameTemplate == null) {
    			continue;
    		}

    		if (!gameTemplate.supportsGameJackpot() && !gameTemplate.supportsMysteryJackpot()) {
    			continue;
    		}
    		
    		JackpotGame game = new JackpotGame();
    		game.setGameID(g.getGameId());
    		game.setJackpotBetType(false);
    	    		
    		gameIds.add(game);
    		gameVariants.add(g.getName());
    	}
    }
    
    @Override
	public String getLabel(int arg0) {
        return gameVariants.get(arg0);
    }

    @Override
	public Object getOption(int arg0) {
        return gameIds.get(arg0);
    }

    @Override
	public int getOptionCount() {
        return gameIds.size();
    }

    @Override
	public String getValue(int arg0) {
        return gameVariants.get(arg0);
    }

    @Override
	public Object translateValue(String arg0) {
        int i = gameVariants.indexOf(arg0);
    	
    	return gameIds.get(i);
    }
    
    public class JackpotGame {
    	private int gameID;
    	private boolean jackpotBetType;
    	
		public int getGameID() {
			return gameID;
		}
		
		public void setGameID(int gameID) {
			this.gameID = gameID;
		}
		
		public boolean isJackpotBetType() {
			return jackpotBetType;
		}
		
		public void setJackpotBetType(boolean jackpotBetType) {
			this.jackpotBetType = jackpotBetType;
		}
		
    }
    
    @Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
