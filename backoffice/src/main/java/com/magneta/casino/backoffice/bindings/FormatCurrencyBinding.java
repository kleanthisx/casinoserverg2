package com.magneta.casino.backoffice.bindings;

import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.Binding;
import org.apache.tapestry5.internal.bindings.AbstractBinding;
import org.apache.tapestry5.ioc.Location;

import com.magneta.casino.services.CurrencyParser;

public class FormatCurrencyBinding extends AbstractBinding {
	private final boolean invariant;
	private final List<Binding> bindings;
	private final CurrencyParser currencyParser;
	private final Locale locale;

	public FormatCurrencyBinding(Location location, boolean invariant,List<Binding> bindings, CurrencyParser currencyParser, Locale locale) {
		super(location);
		this.invariant = invariant;
		this.bindings = bindings;
		this.currencyParser = currencyParser;
		this.locale = locale;
	}

	@Override
	public boolean isInvariant() {
		return this.invariant;
	}


	@Override
	public Class<?> getBindingType() {
		return String.class;
	}
	
	@Override
	public Object get() {
		Double amount = (Double) bindings.get(0).get();
		if(amount == null) {
			return null;
		}
		
		String currency = bindings.get(1).get().toString();
		boolean showCurrency = Boolean.parseBoolean(currency); 
		
		if (showCurrency) {
			return currencyParser.formatCurrency(amount, locale);
		}
		return currencyParser.formatDouble(amount, locale);		
	}

}
