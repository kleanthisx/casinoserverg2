package com.magneta.casino.backoffice.managers.reporting;

import java.net.URL;
import java.util.Date;
import java.util.concurrent.Future;

public interface RepositoryReport {
	String getRequestId();
	Long getUserId();
	
	String getFilename();
	ReportFormat getFormat();
	Date getRequestDate();

	Future<URL> getFuture();
}