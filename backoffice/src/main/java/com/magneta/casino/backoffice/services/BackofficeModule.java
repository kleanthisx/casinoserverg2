package com.magneta.casino.backoffice.services;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.tapestry5.MetaDataConstants;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.Validator;
import org.apache.tapestry5.beaneditor.DataTypeConstants;
import org.apache.tapestry5.beanvalidator.BeanValidatorConfigurer;
import org.apache.tapestry5.beanvalidator.BeanValidatorSource;
import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.ObjectLocator;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.annotations.Startup;
import org.apache.tapestry5.ioc.services.CoercionTuple;
import org.apache.tapestry5.ioc.services.PropertyShadowBuilder;
import org.apache.tapestry5.ioc.services.TypeCoercer;
import org.apache.tapestry5.services.BeanBlockContribution;
import org.apache.tapestry5.services.BindingFactory;
import org.apache.tapestry5.services.BindingSource;
import org.apache.tapestry5.services.ComponentRequestFilter;
import org.apache.tapestry5.services.DisplayBlockContribution;
import org.apache.tapestry5.services.HttpServletRequestFilter;
import org.apache.tapestry5.services.transform.ComponentClassTransformWorker2;

import com.magneta.Config;
import com.magneta.casino.backoffice.bindings.FormatCurrencyBindingFactory;
import com.magneta.casino.backoffice.coercers.DateToLongCoercion;
import com.magneta.casino.backoffice.coercers.DateToStringCoercion;
import com.magneta.casino.backoffice.coercers.DateToTimestampCoercion;
import com.magneta.casino.backoffice.coercers.IntegerToServerBeanCoercion;
import com.magneta.casino.backoffice.coercers.LocaleToStringCoercion;
import com.magneta.casino.backoffice.coercers.LongToDateCoercion;
import com.magneta.casino.backoffice.coercers.LongToTimestampCoercion;
import com.magneta.casino.backoffice.coercers.LongToUserDetailsBeanCoercion;
import com.magneta.casino.backoffice.coercers.ServerBeanToStringCoercion;
import com.magneta.casino.backoffice.coercers.StringToLocaleCoercion;
import com.magneta.casino.backoffice.coercers.StringToTimeZoneCoercion;
import com.magneta.casino.backoffice.coercers.TimeZoneToStringCoercion;
import com.magneta.casino.backoffice.coercers.TimestampToLongCoercion;
import com.magneta.casino.backoffice.coercers.UserDetailsBeanToLongCoercion;
import com.magneta.casino.backoffice.managers.MenuService;
import com.magneta.casino.backoffice.managers.ObjectLocatorLocator;
import com.magneta.casino.backoffice.managers.impl.AuthorizationFilter;
import com.magneta.casino.backoffice.managers.impl.MenuServiceImpl;
import com.magneta.casino.backoffice.managers.impl.ObjectLocatorLocatorImpl;
import com.magneta.casino.backoffice.managers.impl.PrincipalServiceImpl;
import com.magneta.casino.backoffice.managers.impl.RequiredPrivilegeWorker;
import com.magneta.casino.backoffice.managers.impl.Tapestry4RequestFilter;
import com.magneta.casino.backoffice.managers.reporting.ReportExecutionService;
import com.magneta.casino.backoffice.managers.reporting.ReportExportService;
import com.magneta.casino.backoffice.managers.reporting.ReportList;
import com.magneta.casino.backoffice.managers.reporting.ReportLocatorService;
import com.magneta.casino.backoffice.managers.reporting.ReportRepository;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportExecutionServiceImpl;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportExportServiceImpl;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportListImpl;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportLocatorServiceImpl;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportRepositoryImpl;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportServiceImpl;
import com.magneta.casino.backoffice.mock.services.LastUserTransactionsMockUpService;
import com.magneta.casino.backoffice.mock.services.RolesMockUpService;
import com.magneta.casino.backoffice.t4.services.PageMenuItemAccessValidatorImpl;
import com.magneta.casino.backoffice.validators.MinimumAgePeriodValidator;
import com.magneta.casino.common.games.filters.ActionFilterFactory;
import com.magneta.casino.common.games.filters.ActionFilterFactoryImpl;
import com.magneta.casino.common.games.filters.GameActionFilterServiceImpl;
import com.magneta.casino.common.services.CorporateEarningsServiceImpl;
import com.magneta.casino.common.services.CorporateSystemSettingsServiceImpl;
import com.magneta.casino.common.services.CorporatesServiceImpl;
import com.magneta.casino.common.services.CountryServiceImpl;
import com.magneta.casino.common.services.CurrencyParserImpl;
import com.magneta.casino.common.services.CurrencyServiceImpl;
import com.magneta.casino.common.services.DatabaseServiceImpl;
import com.magneta.casino.common.services.GameClientServiceImpl;
import com.magneta.casino.common.services.GameConfigsServiceImpl;
import com.magneta.casino.common.services.GameImagesServiceImpl;
import com.magneta.casino.common.services.GameTableServiceImpl;
import com.magneta.casino.common.services.GamesHistoryServiceImpl;
import com.magneta.casino.common.services.GamesServiceImpl;
import com.magneta.casino.common.services.GeoIpServiceImpl;
import com.magneta.casino.common.services.HostsWhitelistServiceImpl;
import com.magneta.casino.common.services.PaymentTypeServiceImpl;
import com.magneta.casino.common.services.ReportGenerationLogServiceImpl;
import com.magneta.casino.common.services.ServersServiceImpl;
import com.magneta.casino.common.services.SettingsServiceImpl;
import com.magneta.casino.common.services.UserAccessServiceImpl;
import com.magneta.casino.common.services.UserAuthenticationServiceImpl;
import com.magneta.casino.common.services.UserBalanceAdjustmentsServiceImpl;
import com.magneta.casino.common.services.UserCommentServiceImpl;
import com.magneta.casino.common.services.UserDepositsServiceImpl;
import com.magneta.casino.common.services.UserDetailsServiceImpl;
import com.magneta.casino.common.services.UserLoginsServiceImpl;
import com.magneta.casino.common.services.UserRegistrationServiceImpl;
import com.magneta.casino.common.services.UserRoundsServiceImpl;
import com.magneta.casino.common.services.UserWithdrawalsServiceImpl;
import com.magneta.casino.common.services.UsernameServiceImpl;
import com.magneta.casino.common.services.UsersBlacklistServiceImpl;
import com.magneta.casino.common.services.WebLoginsServiceImpl;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.DatabaseService;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.reports.ReportUtil;
import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.CorporateSystemSettingsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GameImagesService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.GamesHistoryService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.GeoIpService;
import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.ReportGenerationLogService;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.TimeZoneService;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.UserBalanceAdjustmentsService;
import com.magneta.casino.services.UserCommentService;
import com.magneta.casino.services.UserDepositsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserLoginsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.UserRoundsService;
import com.magneta.casino.services.UserWithdrawalsService;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.UsersBlacklistService;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;
public class BackofficeModule {

	@Startup
	public static void startup(ObjectLocator locator,
			CurrencyParser currencyParser,
			UsernameService usernameService) {
		
		T5ServiceLocator.setObjectLocator(locator);
		ReportUtil.init(currencyParser, usernameService);
	}

	public static void bind(ServiceBinder binder) {
		binder.bind(ObjectLocatorLocator.class, ObjectLocatorLocatorImpl.class);
		binder.bind(HttpServletRequestFilter.class, Tapestry4RequestFilter.class).withId("Tapestry4RequestFilter");
		
		binder.bind(ActionFilterFactory.class, ActionFilterFactoryImpl.class);
		binder.bind(BackofficePrincipalService.class, PrincipalServiceImpl.class);
		binder.bind(CorporatesService.class, CorporatesServiceImpl.class);
		binder.bind(CorporateEarningsService.class, CorporateEarningsServiceImpl.class);
		binder.bind(CorporateSystemSettingsService.class,CorporateSystemSettingsServiceImpl.class);
		binder.bind(CountryService.class,CountryServiceImpl.class);
		binder.bind(CurrencyService.class, CurrencyServiceImpl.class);
		binder.bind(CurrencyParser.class, CurrencyParserImpl.class);
		binder.bind(DatabaseService.class, DatabaseServiceImpl.class);
		binder.bind(GamesService.class, GamesServiceImpl.class);
		binder.bind(GameActionFilterService.class, GameActionFilterServiceImpl.class);
		binder.bind(GameClientService.class, GameClientServiceImpl.class);
		binder.bind(GameConfigsService.class, GameConfigsServiceImpl.class);
		binder.bind(GameImagesService.class, GameImagesServiceImpl.class);
		binder.bind(GamesHistoryService.class,GamesHistoryServiceImpl.class);
		binder.bind(GeoIpService.class,GeoIpServiceImpl.class);
		binder.bind(GameTableService.class, GameTableServiceImpl.class);
		binder.bind(HostsWhitelistService.class, HostsWhitelistServiceImpl.class);
		binder.bind(PaymentTypeService.class, PaymentTypeServiceImpl.class);
		binder.bind(ReportGenerationLogService.class, ReportGenerationLogServiceImpl.class);
		binder.bind(ServersService.class, ServersServiceImpl.class);
		binder.bind(SettingsService.class, SettingsServiceImpl.class);
		binder.bind(TimeZoneService.class);
		binder.bind(UserAccessService.class, UserAccessServiceImpl.class);
		binder.bind(UserAuthenticationService.class, UserAuthenticationServiceImpl.class);
		binder.bind(UserCommentService.class, UserCommentServiceImpl.class);
		binder.bind(UserBalanceAdjustmentsService.class, UserBalanceAdjustmentsServiceImpl.class);
		binder.bind(UserDepositsService.class, UserDepositsServiceImpl.class);
		binder.bind(UserDetailsService.class, UserDetailsServiceImpl.class);
		binder.bind(UserLoginsService.class,UserLoginsServiceImpl.class);
		binder.bind(UserWithdrawalsService.class, UserWithdrawalsServiceImpl.class);
		binder.bind(UsernameService.class,UsernameServiceImpl.class);
		binder.bind(UserRegistrationService.class,UserRegistrationServiceImpl.class);
		binder.bind(UserRoundsService.class, UserRoundsServiceImpl.class);
		binder.bind(UsersBlacklistService.class,UsersBlacklistServiceImpl.class);
		binder.bind(WebLoginsService.class, WebLoginsServiceImpl.class);
		
		binder.bind(MenuService.class, MenuServiceImpl.class);
		binder.bind(PageMenuItemAccessValidator.class, PageMenuItemAccessValidatorImpl.class);
		
		binder.bind(ReportList.class, ReportListImpl.class);
		binder.bind(ReportExecutionService.class, ReportExecutionServiceImpl.class);
		binder.bind(ReportExportService.class, ReportExportServiceImpl.class);
		binder.bind(ReportLocatorService.class, ReportLocatorServiceImpl.class);
		binder.bind(ReportRepository.class, ReportRepositoryImpl.class);
		binder.bind(ReportService.class, ReportServiceImpl.class);
		
		
		binder.bind(BindingFactory.class,FormatCurrencyBindingFactory.class).withSimpleId();
		
		binder.bind(LastUserTransactionsMockUpService.class);
		binder.bind(RolesMockUpService.class);
		
	}

	public static void contributeFieldValidatorSource(@SuppressWarnings("rawtypes") MappedConfiguration<String, Validator> configuration) {
		configuration.add("minAgeLimit", new MinimumAgePeriodValidator());
	}
	
	public void contributeComponentMessagesSource(OrderedConfiguration<String> configuration) {
		configuration.add("CustomValidationMessages","com/magneta/casino/backoffice/validators/ValidationMessages","after:ValidationMessages");
	}
	
	public static void contributeBeanBlockOverrideSource(Configuration<BeanBlockContribution> configuration){
		 configuration.add(new DisplayBlockContribution(DataTypeConstants.DATE, "PropertyDisplayBlockOverrides", DataTypeConstants.DATE));
		 configuration.add(new DisplayBlockContribution(DataTypeConstants.BOOLEAN, "PropertyDisplayBlockOverrides", DataTypeConstants.BOOLEAN));
		// configuration.add(new DisplayBlockContribution(DataTypeConstants.NUMBER,"PropertyDisplayBlockOverrides",DataTypeConstants.NUMBER));
	}
	
	public static void contributeBindingSource(MappedConfiguration<String, BindingFactory> configuration,
			BindingSource bindingSource,
			@InjectService("FormatCurrencyBindingFactory") BindingFactory formatCurrencyBindingFactory) {
		
		configuration.add("money", formatCurrencyBindingFactory);
	}
	
	public static ObjectLocator buildObjectLocator(@Inject ObjectLocatorLocator serviceLocator) {
		return serviceLocator.getObjectLocator();
	}
	
	public static ExtendedPrincipalBean buildExtendedPrincipalBean(@Inject PropertyShadowBuilder shadowBuilder, @Inject PrincipalService principalService) {
		return shadowBuilder.build(principalService, "principal", ExtendedPrincipalBean.class);
	}

	public static void contributeHttpServletRequestHandler(OrderedConfiguration<HttpServletRequestFilter> configuration,
			@InjectService("Tapestry4RequestFilter") HttpServletRequestFilter requestFilter) {
		configuration.add("Tapestry4RequestFilter", requestFilter, "after:StoreIntoGlobals", "before:EndOfRequest");
	}
	
	public static void contributeComponentRequestHandler(OrderedConfiguration<ComponentRequestFilter> configuration) {
		configuration.addInstance("AuthorizationFilter", AuthorizationFilter.class);
	}

	public static void contributeFactoryDefaults(MappedConfiguration<String, Object> configuration) {
		// The application version number is incorprated into URLs for some
		// assets. Web browsers will cache assets because of the far future
		// expires
		// header. If existing assets are changed, the version number should
		// also
		// change, to force the browser to download new versions. This overrides
		// Tapesty's default
		// (a random hexadecimal number), but may be further overriden by
		// DevelopmentModule or
		// QaModule.
		configuration.override(SymbolConstants.APPLICATION_VERSION, "0.0.1-SNAPSHOT");
	}

	public static void contributeApplicationDefaults(MappedConfiguration<String, Object> configuration) {
		// Contributions to ApplicationDefaults will override any contributions
		// to
		// FactoryDefaults (with the same key). Here we're restricting the
		// supported
		// locales to just "en" (English). As you add localised message catalogs
		// and other assets,
		// you can extend this list of locales (it's a comma separated series of
		// locale names;
		// the first locale name is the default when there's no reasonable
		// match).
		configuration.add(SymbolConstants.SUPPORTED_LOCALES, "en");
		configuration.add(SymbolConstants.HMAC_PASSPHRASE, "ssr54twqwgjelh");
	}
	
	public void contributeMetaDataLocator(MappedConfiguration<String,String> configuration) {
		if (Config.getBoolean("server.ssl", false) == true) { 
			configuration.add(MetaDataConstants.SECURE_PAGE, "true");
		}
	}
	
	@Contribute(ComponentClassTransformWorker2.class)
	public static void contributeWorkers(OrderedConfiguration<ComponentClassTransformWorker2> workers) {
		   workers.addInstance("RequiredPrivilegeWorker", RequiredPrivilegeWorker.class);
	}
	
	@Contribute(TypeCoercer.class)
	public static void contributeCoercers(Configuration<CoercionTuple<?,?>> configuration,
			@InjectService("ServersService") ServersService serversService,
			@InjectService("UserDetailsService") UserDetailsService userDetailsService) {
		
		configuration.add(new CoercionTuple<ServerBean, String>(ServerBean.class, String.class, new ServerBeanToStringCoercion()));
		configuration.add(new CoercionTuple<Integer, ServerBean>(Integer.class, ServerBean.class, new IntegerToServerBeanCoercion(serversService)));
		
		configuration.add(new CoercionTuple<UserDetailsBean, Long>(UserDetailsBean.class, Long.class, new UserDetailsBeanToLongCoercion()));
		configuration.add(new CoercionTuple<Long, UserDetailsBean>(Long.class, UserDetailsBean.class, new LongToUserDetailsBeanCoercion(userDetailsService)));
		
		configuration.add(new CoercionTuple<String,TimeZone>(String.class, TimeZone.class, new StringToTimeZoneCoercion()));
		configuration.add(new CoercionTuple<TimeZone,String>(TimeZone.class, String.class, new TimeZoneToStringCoercion()));
		
		configuration.add(new CoercionTuple<Date, String>(Date.class, String.class, new DateToStringCoercion()));
		configuration.add(new CoercionTuple<Long,Date>(Long.class, Date.class, new LongToDateCoercion()));
		configuration.add(new CoercionTuple<Date,Long>(Date.class, Long.class, new DateToLongCoercion()));
		
		configuration.add(new CoercionTuple<Long, Timestamp>(Long.class, Timestamp.class, new LongToTimestampCoercion()));
		configuration.add(new CoercionTuple<Timestamp, Long>(Timestamp.class, Long.class, new TimestampToLongCoercion()));
		
		configuration.add(new CoercionTuple<Locale, String>(Locale.class, String.class, new LocaleToStringCoercion()));
		configuration.add(new CoercionTuple<String, Locale>(String.class, Locale.class, new StringToLocaleCoercion()));
		
		configuration.add(new CoercionTuple<Date, Timestamp>(Date.class, Timestamp.class, new DateToTimestampCoercion()));
	}
	
	public static void contributeIgnoredPathsFilter(Configuration<String> configuration) {
		configuration.add("/t4/.*");
	    configuration.add("/report_image/.*");
	}

	@Contribute(BeanValidatorSource.class)
	public static void provideBeanValidatorConfigurer(OrderedConfiguration<BeanValidatorConfigurer> configuration) {
		configuration.add("MyConfigurer", new BeanValidatorConfigurer()
		{
			@Override
			public void configure(javax.validation.Configuration<?> configuration)
			{
				configuration.ignoreXmlConfiguration();
			}
		});
	}
}
