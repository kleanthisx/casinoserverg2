package com.magneta.casino.backoffice.managers.reporting;

import java.util.List;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.search.SearchContext;

public interface ReportRepository {

	RepositoryReport addScheduledReportRequest(ReportRequest request);
	void removeReport(String requestId);
	void cancelReport(String requestId);

	List<RepositoryReport> getReports(SearchContext<RepositoryReport> searchContext) throws ServiceException;
	int getReportsSize(SearchContext<RepositoryReport> searchContext) throws ServiceException;
	
	RepositoryReport getReport(String requestId) throws ServiceException;
}
