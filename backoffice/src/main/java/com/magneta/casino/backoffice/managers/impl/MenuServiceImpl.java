package com.magneta.casino.backoffice.managers.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.magneta.casino.backoffice.beans.MenuBean;
import com.magneta.casino.backoffice.managers.MenuService;

public class MenuServiceImpl implements MenuService {
	
	private static final Logger log = LoggerFactory.getLogger(MenuServiceImpl.class);
	
	private List<MenuBean> verticalMenu;
	private List<MenuBean> horizontalMenu;
	private Map<Integer,MenuBean> menuMap;
	private Integer uniqueId;
	
	public MenuServiceImpl() {
		this.uniqueId = 0;
		this.menuMap = new HashMap<Integer,MenuBean>();
		this.verticalMenu = parseFile(this.getClass().getClassLoader().getResourceAsStream("vr_menu.xml"));
		this.horizontalMenu = parseFile(this.getClass().getClassLoader().getResourceAsStream("hr_menu.xml"));
	}
	
	private void addItem(List<MenuBean> items,MenuBean menuBean) {	
		menuBean.setId(uniqueId);
		items.add(menuBean);
		menuMap.put(uniqueId, menuBean);
		uniqueId++;
	}
	
    @SuppressWarnings("unchecked")
	private List<MenuBean> parseFile(InputStream is){
        DocumentBuilder builder;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            
            if (doc.getFirstChild() == null){
                return null;
            }
            List<MenuBean> items = new ArrayList<MenuBean>();
       
            NodeList nodes = doc.getFirstChild().getChildNodes();
            
            for (int i = 0; i < nodes.getLength(); i++){
                Node item = nodes.item(i);
                
                if (!item.getNodeName().toLowerCase().equals("menuitem")){
                    if (item.getNodeName().toLowerCase().equals("seperator")){
                    	MenuBean mnuItem = new MenuBean();
                    	mnuItem.setDescription("@seperator@");
                        addItem(items,mnuItem);
                    } else {
                        continue;
                    }
                } else {
                    Element menuItem = (Element)item;
                    MenuBean mnuItem = new MenuBean();
                    addItem(items,mnuItem);
                    
                    NodeList children = menuItem.getChildNodes();
                    
                    for (int j = 0; j < children.getLength(); j++){
                        Node child = children.item(j);
                        if (child.getNodeName().toLowerCase().equals("seperator")){
                        	MenuBean subItem = new MenuBean();
                        	subItem.setDescription("@seperator@");
                        	List<MenuBean> subItems = (List<MenuBean>) mnuItem.getSubMenu();
                        	if(subItems == null) {
                        		subItems = new ArrayList<MenuBean>();
                                mnuItem.setSubMenu(subItems);
                        	}
                        	subItems.add(subItem);                        	
                        } else if (child.getNodeName().toLowerCase().equals("description")){
                            mnuItem.setDescription(child.getTextContent());
                        } else if (child.getNodeName().toLowerCase().equals("page")){
                            mnuItem.setPageLink(child.getTextContent());
                        } else if (child.getNodeName().toLowerCase().equals("message")){
                            mnuItem.setMessageCatalogMessage(child.getTextContent());
                        } else if (child.getNodeName().toLowerCase().equals("tooltip")){
                            mnuItem.setToolTip(child.getTextContent());
                        } else if (child.getNodeName().toLowerCase().equals("menuitem")){
                            createItem((Element)child, mnuItem);
                        } else if (child.getNodeName().toLowerCase().equals("special")) {
                        	mnuItem.setPageLink("special:" + child.getTextContent());
                        }
                    }
                }
            }
            return items;
        } catch (ParserConfigurationException e) {
            log.error("Error parsing menu file.", e);
        } catch (SAXException e) {
            log.error("Error parsing menu file.", e);
        } catch (IOException e) {
            log.error("Error parsing menu file.", e);
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
	private void createItem(Element menuItem, MenuBean parentMenu){
    	MenuBean item = new MenuBean();
    	List<MenuBean> subItems = (List<MenuBean>) parentMenu.getSubMenu();
    	if(subItems == null) {
    		subItems = new ArrayList<MenuBean>();
    		parentMenu.setSubMenu(subItems);
    	}
    	subItems.add(item);
        NodeList children = menuItem.getChildNodes();

        for (int j = 0; j < children.getLength(); j++){
            Node child = children.item(j);
            if (child.getNodeName().toLowerCase().equals("seperator")){
            	MenuBean subItem = new MenuBean();
            	subItem.setDescription("@seperator@");
            } else if (child.getNodeName().toLowerCase().equals("description")){
                item.setDescription(child.getTextContent());
            } else if (child.getNodeName().toLowerCase().equals("page")){
                item.setPageLink(child.getTextContent());
            } else if (child.getNodeName().toLowerCase().equals("message")){
                item.setMessageCatalogMessage(child.getTextContent());
            } else if (child.getNodeName().toLowerCase().equals("tooltip")){
                item.setToolTip(child.getTextContent());
            } else if (child.getNodeName().toLowerCase().equals("menuitem")){
                createItem((Element)child, item);
            }
        }
    }
  	
	@Override
	public List<MenuBean> getVerticalMenu() {
		return this.verticalMenu;
	}
	
	@Override
	public List<MenuBean> getHorizontalMenu() {
		return this.horizontalMenu;
	}
	
	public MenuBean getMenu(Integer id) {
		return this.menuMap.get(id);
	}
}
