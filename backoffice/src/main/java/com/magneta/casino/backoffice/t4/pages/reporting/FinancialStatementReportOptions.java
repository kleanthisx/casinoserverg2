package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.event.PageAttachListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class FinancialStatementReportOptions extends DateReportOptions implements PageAttachListener {
	
	@Component(id = "groupPeriods",
			bindings = {"value=period","model=new com.magneta.casino.backoffice.t4.models.GroupingPeriodsSelectionModel()", 
		"displayName=message:GroupingPeriod"})
	public abstract PropertySelection getGroupPeriods();
	
	@Component(id = "groupPeriodsLabel",
			bindings = {"field=component:groupPeriods"})
	public abstract FieldLabel getGroupPeriodsLabel();
	
	public abstract String getPeriod(); 
	
	@Override
	public void pageAttached(PageEvent event){
		getGroupPeriods().getBinding("value").setObject(new com.magneta.casino.backoffice.t4.models.GroupingPeriodsSelectionModel().getLabel(1));
	}
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		
		reportParams.put("grouping_period", getPeriod().toLowerCase());
	}
}
