package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;

public class UserTypesSelectModel extends AbstractSelectModel {

	private final PrincipalService principalService;
	private final Messages messages;
	
	public UserTypesSelectModel(PrincipalService principalService, Messages messages) {
		this.principalService = principalService;
		this.messages = messages;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}
	
	private boolean hasUserAccess(ExtendedPrincipalBean principal, UserTypeEnum userType) {		
		if (userType == UserTypeEnum.CORPORATE_ACCOUNT 
				|| userType == UserTypeEnum.CORPORATE_PLAYER) {
			return principal.hasPrivilege(PrivilegeEnum.CORPORATE, false);
		}

		return principal.hasPrivilege(PrivilegeEnum.PROFILES, false);
	}

	@Override
	public List<OptionModel> getOptions() {
		ExtendedPrincipalBean principal = principalService.getPrincipal();
		List<OptionModel> types = new ArrayList<OptionModel>();
		
		for (UserTypeEnum type: UserTypeEnum.values()) {
			if (type.isSynthetic())
				continue;
			
			if (hasUserAccess(principal, type)) {
				types.add(new OptionModelImpl(messages.get(type.name()), type));
			}
		}
		
		return types;
	}

}
