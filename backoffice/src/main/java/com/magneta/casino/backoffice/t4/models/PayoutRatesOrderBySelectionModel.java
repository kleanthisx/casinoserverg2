package com.magneta.casino.backoffice.t4.models;

import org.apache.tapestry.form.IPropertySelectionModel;

public class PayoutRatesOrderBySelectionModel implements IPropertySelectionModel{

	private String[] labels = {"Game","Rate","Rounds"};
	private String[] values = {"game_type_variant","rate","rounds"};
	
	@Override
	public String getLabel(int arg0) {
		return labels[arg0];
	}

	@Override
	public Object getOption(int arg0) {
		return labels[arg0];
	}

	@Override
	public int getOptionCount(){
		return values.length;
	}

	@Override
	public String getValue(int arg0) {
		return values[arg0];
	}

	@Override
	public boolean isDisabled(int arg0) {
		return false;
	}

	@Override
	public Object translateValue(String arg0) {
		return arg0;
	}
}
