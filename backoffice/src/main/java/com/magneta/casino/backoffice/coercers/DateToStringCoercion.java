package com.magneta.casino.backoffice.coercers;

import java.util.Date;

import org.apache.tapestry5.ioc.services.Coercion;

public class DateToStringCoercion implements Coercion<Date, String> {

	@Override
	public String coerce(Date input) {
		return Long.toString(input.getTime());
	}
}
