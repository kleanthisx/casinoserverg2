package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.PlayerTypesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class UserDuplicateLoginsReport extends DateReportOptions {

	@Component(id = "role",
			bindings = {"value=role","model=ognl:playerTypesSelectionModel","displayName=message:Role"})
	public abstract PropertySelection getRoleSel();
	
	public PlayerTypesSelectionModel getPlayerTypesSelectionModel() {
		return new PlayerTypesSelectionModel(true);
	}

	@Component(id = "roleLabel",
			bindings = {"field=component:role"})
	public abstract FieldLabel getStoreSelLabel();

	@InitialValue("1")
	public abstract Long getRole();

	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		if (getRole() !=-1) {
			reportParams.put("user_type", getRole());
		}
	}
}
