package com.magneta.casino.backoffice.managers.reporting.impl;

import java.io.OutputStream;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

import org.apache.tapestry5.ioc.services.TypeCoercer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.managers.reporting.ReportExecutionService;
import com.magneta.casino.backoffice.managers.reporting.ReportExportService;
import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.ReportGenerationLogService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ReportGenerationLogBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class ReportExecutionServiceImpl implements ReportExecutionService {

	private final Logger log = LoggerFactory.getLogger(ReportExecutionServiceImpl.class);
	
	private final TypeCoercer typeCoercer;
	private final ReportGenerationLogService reportLogService;
	private final ReportExportService reportExportService;
	
	public ReportExecutionServiceImpl(TypeCoercer typeCoercer,
			ReportGenerationLogService reportLogService,
			ReportExportService reportExportService) {
		this.typeCoercer = typeCoercer;
		this.reportLogService = reportLogService;
		this.reportExportService = reportExportService;
	}
	
    protected Map<String,Object> getReportParameterValues(ReportRequest request) {
		
		Map<String,Class<?>> reportParamTypes = new HashMap<String,Class<?>>();

		for (JRParameter param: request.getReport().getParameters()) {
			log.debug("Report Parameter '"+param.getName()+"' of type "+param.getValueClassName());
			reportParamTypes.put(param.getName(), param.getValueClass());
		}

		Map<String,Object> params = new HashMap<String,Object>();

    	if (request.getReportFormat() == ReportFormat.HTML || request.getReportFormat() == ReportFormat.XLS) {
    		params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
    	}
    	
    	if (request.getLocale() != null) {
    		params.put(JRParameter.REPORT_LOCALE, request.getLocale());
    	}
		
    	if (request.getReportParameters() != null) {
    		for (String paramName: reportParamTypes.keySet()) {
    			Object val = request.getReportParameters().get(paramName);

    			if (val == null) {
    				continue;
    			}

    			params.put(paramName, typeCoercer.coerce(val, reportParamTypes.get(paramName)));
    		}
    	}
		
		ExtendedPrincipalBean principal = request.getPrincipal();

    	Long userId = principal.getUserId();

    	if (userId != null) {
    		params.put("request_user", userId);
    	} else {
    		params.remove("request_user");
    	}

    	params.put("show_corporate", principal.hasPrivilege(PrivilegeEnum.CORPORATE, false));

		return params;
	}
	
	@Override
    public void executeReport(ReportRequest request, OutputStream os) throws JRException {
    	if (request.getPrincipal() == null) {
    		throw new JRException("Access denied");
    	}
    	
    	Map<String,Object> reportParams = getReportParameterValues(request);

    	ReportGenerationLogBean reportLog = new ReportGenerationLogBean();
    	
    	reportLog.setUserId(request.getPrincipal().getUserId());
    	reportLog.setApplicationName("Backoffice");
    	
    	reportLog.setReportName(request.getReport().getName());
    	reportLog.setRequestUrl(request.getRequestUrl());
    	reportLog.setClientIp(request.getClientHost());
    	
    	if (request.getReportParameters() != null) {
    		reportLog.setFromDate((Date)request.getReportParameters().get("from_date"));
    		reportLog.setToDate((Date)request.getReportParameters().get("to_date"));
    	}
    	
    	try {
			reportLogService.logReportGeneration(reportLog);
		} catch (ServiceException e) {
			log.error("Unable to log report generation", e);
		}
    	
    	JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer(100, new JRSwapFile(System.getProperty("java.io.tmpdir"), 1024, 100), true);
    	reportParams.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
    	reportParams.put(JRFileVirtualizer.PROPERTY_TEMP_FILES_SET_DELETE_ON_EXIT, false);
    	
    	try {
    		JasperPrint jasperPrint;
    		Connection connection = ConnectionFactory.getReportsConnection();
    		try {
    			jasperPrint = JasperFillManager.fillReport(request.getReport(), reportParams, connection);
    		} finally {
    			DbUtil.close(connection);
    		}
    		
    		reportExportService.exportReport(jasperPrint, request.getReportFormat(), os);
    		
    	} finally {
    		virtualizer.cleanup();
    	}
    }
}
