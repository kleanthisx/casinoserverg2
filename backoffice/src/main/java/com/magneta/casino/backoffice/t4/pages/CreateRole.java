package com.magneta.casino.backoffice.t4.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.Radio;
import org.apache.tapestry.form.RadioGroup;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.administration.beans.RoleBean;
import com.magneta.administration.beans.RoleBean.CreateRoleResult;
import com.magneta.administration.beans.RolePrivilege;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class CreateRole extends SecurePage implements PageBeginRenderListener{

	@Override
	public boolean pageNeedsWriteAccess() {
		return true;
	}
    
    public abstract RoleBean getRole();
    public abstract void setRole(RoleBean role);

    public abstract void setAvailPrivileges(List<RolePrivilege> availPrivileges);
    public abstract List<RolePrivilege> getAvailPrivileges();

	@Component
	public abstract ErrorDisplay getErrorDisplay();
  
    public abstract RolePrivilege getCurrPrivilege();
    
    @Component(id="createRoleForm",
    		bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
    public abstract Form getCreateRoleForm();
    
    @Component(id="description",
    		bindings={"value=role.RoleDesc","disabled=!allowWrite()","displayName=literal:Description","validators=validators:required,minLength=2,maxLength=50"})
    public abstract TextField getDescription();
    
    @Component(id="descriptionLabel",
    		bindings={"field=component:description"})
    public abstract FieldLabel getDescriptionLabel();
    
    @Component(id="availPrivilegesFor",type="For",
    		bindings={"source=availPrivileges","value=currPrivilege","element=literal:tr"})
    public abstract ForBean getAvailPrivilegesFor();
    
    @Component(id="availabilityRadios",
    		bindings={"selected=currPrivilege.availability","validators=validators:required","disabled=!allowWrite()"})
    public abstract RadioGroup getAvailabilityRadios();
    
    @Component(id="notGrantedRadio",
    		bindings={"value=@com.magneta.administration.beans.RolePrivilege@NOT_GRANTED_AVAILABILITY","disabled=!allowWrite()"})
    public abstract Radio getNotGrantedRadio();
    
    @Component(id="readOnlyRadio",
    		bindings={"value=@com.magneta.administration.beans.RolePrivilege@READ_ONLY_AVAILABILITY","disabled=!allowWrite()"})
    public abstract Radio getReadOnlyRadio();
    
    @Component(id="readWriteRadio",
    		bindings={"value=@com.magneta.administration.beans.RolePrivilege@READ_WRITE_AVAILABILITY","disabled=!allowWrite()"})
    public abstract Radio getReadWriteRadio();
    
    @Component(id="availPrivileges",
    		bindings={"value=availPrivileges"})
    public abstract Hidden getAvailablePrivileges();
    
    @Component(id="createSubmit",
    		bindings={"disabled=!allowWrite()"})
    public abstract Submit getCreateSubmit();
    
    @Component(id="cancelButton")
    public abstract Button getCancelButton();
    
    @Override
	public void pageBeginRender(PageEvent event) {
    	RoleBean role = new RoleBean();
        setRole(role);
        
        List<RolePrivilege> availPrivileges = new ArrayList<RolePrivilege>();
        
        for (PrivilegeEnum p: PrivilegeEnum.values()) {
        	RolePrivilege currPriv = new RolePrivilege();
        	currPriv.setAvailability(RolePrivilege.NOT_GRANTED_AVAILABILITY);
        	currPriv.setPrivilegeKey(p.getId());
        	currPriv.setPrivilegeDesc(p.toString());
        	availPrivileges.add(currPriv);
        }

        setAvailPrivileges(availPrivileges);
    }
    
    public void onSubmit(IRequestCycle cycle){
        ValidationDelegate delegate = getErrorDisplay().getDelegate();
        
        if (delegate.getHasErrors()){
            return;
        }
        
        RoleBean role = getRole();
        boolean selected = false;
        
        for (RolePrivilege priv: getAvailPrivileges()){
        	if (priv.getAvailability() != RolePrivilege.NOT_GRANTED_AVAILABILITY){
        		selected = true;
        		break;
        	}
        }
        
        if (!selected){
            delegate.setFormComponent(null);
            delegate.record(getMsg("no-priv-error"), null);
            return;
        }
        
        CreateRoleResult result = role.createRole(getAvailPrivileges());
        
        switch (result){
            
            case DB_ERROR:
                delegate.record(null, getMsg("db-error"));
                break;
            
            case DESC_NOT_UNIQUE:
                delegate.setFormComponent((IFormComponent) getComponent("description"));
                delegate.recordFieldInputValue(role.getRoleDesc());
                delegate.record(getMsg("role-exists"), null);
                break;
            case OK: 
            	redirectToPage("RoleAdministration");
        }
    }
    
    public void onCancel(IRequestCycle cycle){
    	redirectToPage("RoleAdministration");
    }
}
