package com.magneta.casino.backoffice.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.administration.beans.ReelBean;
import com.magneta.administration.beans.ReelSetBean;
import com.magneta.casino.backoffice.pages.GameImage;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.slots.SlotGameActionFilter;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.ServiceException;
import com.magneta.games.configuration.slots.SlotConfiguration;

public class SlotConfigView {

	@Inject
	private GameActionFilterService gameActionFilterService;
	
	@Inject
	private PageRenderLinkSource pageRenderLinkSource;
		
	@Parameter(required=true)
	private Integer gameId;
	
	@Parameter(required=true)
	private SlotConfiguration configuration;

	@Cached
	public SlotGameActionFilter getActionFilter() throws ServiceException {
		return (SlotGameActionFilter)gameActionFilterService.getFilter(gameId, configuration);
	}
	
	@Property
	private GameTemplateSetting setting;
	
	@Cached
	public List<GameTemplateSetting> getSettings() {
		GameTemplateSetting[] defaultSettings = configuration.getDefaultSettings();
		List<GameTemplateSetting> settings = new ArrayList<GameTemplateSetting>();
		
		if (defaultSettings != null) {
			for (GameTemplateSetting s: defaultSettings) {
				GameTemplateSetting newS = new GameTemplateSetting(s);
				newS.setDefaultValue(configuration.getSetting(s.getKey()));
				settings.add(newS);
			}
		}
		return settings;
	}
	
	@Cached
	public ComboSymbolBean[] getSymbols() throws ServiceException {
		return getActionFilter().getSymbols();
	}
	
	@Property
	private ComboSymbolBean currSymbol;
	
	@Property
	private GameComboBean combo;
	
	@Cached
	public GameComboBean[] getPaytable() throws ServiceException {
		return getActionFilter().getPaytable();
	}
	
	@Property
	private ReelBean reel;
	
	public ReelBean[] getReels() {
		return reelSet.getReels();
	}
	
	@Property
	private ComboSymbolBean reelImage;
	
	@Property
	private Integer reelImageIndex;
	
	public String getReelImageTitle() {
		StringBuilder builder = new StringBuilder("Symbol:").append(reelImage.getAlt()).append(" Position:").append(reelImageIndex.intValue());
		return builder.toString();
	}
	
	public Link getReelImageUrl() {
		return this.pageRenderLinkSource.createPageRenderLinkWithContext(GameImage.class, gameId, reelImage.getImage());
	}
	
	public Link getComboImageUrl() {
		return this.pageRenderLinkSource.createPageRenderLinkWithContext(GameImage.class, gameId, currSymbol.getImage());
	}
	
	public ComboSymbolBean[] getReelImages() {
		return reel.getReelNumImages();
	}
	
	@Property
	private ReelSetBean reelSet;
	
	@Cached
	public ReelSetBean[] getReelSets() throws ServiceException {
		SlotConfiguration config = configuration;

		ReelSetBean[] reelSets = new ReelSetBean[config.getNumberOfReelSets()];
		for(int i=0; i < reelSets.length; i++) {
			int[][] reelsNum = config.getReels(i);
			int numOfReels = config.getNumberOfReels(i);
			reelSets[i] = new ReelSetBean();
			ReelBean[] reels = new ReelBean[numOfReels];
			for(int z=0;z<numOfReels;z++) {
				int[] reel = reelsNum[z];
				StringBuilder builder = new StringBuilder();
				for(int k=0;k<reel.length;k++) {
					builder.append(reel[k]).append(",");
				}
				reels[z] = new ReelBean(builder.toString(), getSymbols());
			}
			reelSets[i].setReels(reels);
		}
		
		return reelSets;
	}
}
