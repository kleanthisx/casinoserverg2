package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CountryBean;

public class CountrySelectModel extends AbstractSelectModel {

	private final CountryService countryService;
	
	public CountrySelectModel(CountryService countryService) {
		this.countryService = countryService;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> models = new ArrayList<OptionModel>();
		
		
		List<CountryBean> countries;
		try {
			countries = countryService.getCountries();
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		for (CountryBean country: countries) {
			models.add(new OptionModelImpl(country.getName(), country));
		}
		return models;
	}
}
