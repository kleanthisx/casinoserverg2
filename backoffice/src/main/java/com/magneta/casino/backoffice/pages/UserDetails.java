package com.magneta.casino.backoffice.pages;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.PageActivationContext;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;

import com.magneta.administration.beans.RoleBean;
import com.magneta.administration.beans.UserTransactionBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.backoffice.griddatasource.LastUserTransactionsGridDataSource;
import com.magneta.casino.backoffice.mock.services.LastUserTransactionsMockUpService;
import com.magneta.casino.backoffice.mock.services.RolesMockUpService;
import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.GamesHistoryService;
import com.magneta.casino.services.GeoIpService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserBalanceAdjustmentsService;
import com.magneta.casino.services.UserDepositsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserLoginsService;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.AffiliateUsersBean;
import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.GameSummaryHistoryBean;
import com.magneta.casino.services.beans.LocationBean;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.UserLoginBean;
import com.magneta.casino.services.beans.WebLoginBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.DatabaseException;
import com.magneta.casino.services.exceptions.FinancialInconsistencyException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

@RequiredPrivilege(PrivilegeEnum.PROFILES)
public class UserDetails {

	@Component(id = "detailsForm")
	private Form detailsForm;

	@PageActivationContext
	private Long userId;
	
	public Long getUserId() {
		return this.userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Property
	private UserDetailsBean user;

	@Property
	private CountryBean country;

	@Inject
	private UserDetailsService userDetailsService;

	@Inject
	private CountryService countryService;

	@Inject
	private WebLoginsService webLoginsService;

	@Inject
	private UserLoginsService userLoginService;

	@Inject
	private BackofficePrincipalService principalService;

	@Inject
	private CorporatesService corporatesService;
	
	@Inject
	private CorporateEarningsService corporateEarningsService;
	
	@Inject
	private GeoIpService geoIpService;

	@Inject
	private GamesHistoryService gameHistoryService;
	
	@Inject
	private CurrencyService currencyService;
	
	@Inject
	private CurrencyParser currencyParser;
	
	@Inject
	private RolesMockUpService rolesMockUpService;
	
	@Inject
	private UserDepositsService userDepositService;
	
	@Inject
	private UserBalanceAdjustmentsService balanceAdjustmentsService;
	
	@Inject
	private Locale locale;

	@Property
	@Inject
	private Messages messages;

	@Inject
	private Logger log;

	@Inject
	private LastUserTransactionsMockUpService lastUserTransactionsMockUpService;
	
	@Property
	private WebLoginBean webLogin;

	@Property
	private UserLoginBean userLogin;

	@Property
	private Double userBalance;

	@Property
	private Double bonusCredits;

	@Property
	private Long rounds = 0L;

	@Property
	private Double bets = 0.0;
	
	@Property
	private Double wins = 0.0;
	
	@Property
	private Double realBets = 0.0;
	
	@Property
	private Double realWins = 0.0;
	
	@Property
	private Double jackpotWins = 0.0;
	
	@Property
	private Double payout = 0.0;
	
	@Property
	private List<Long> hierarchy;
	
	@Property
	private Long corporateId;
	
	@Property
	private UserTransactionBean userTransactionBean;
	
	@Property
	private String currency;
	
	@Property
	private RoleBean role;
	
	@Property
	private Double earnings;
	
	@SetupRender
	public void renderPage() throws ServiceException {
		user = userDetailsService.getUser(userId);
		if (user == null) {
			throw new ServiceException("User not found");
		}
		
		country = countryService.getCountry(user.getCountryCode());
		
		UserBalanceBean balanceBean = userDetailsService.getUserBalance(userId);
		userBalance = balanceBean.getBalance();
		bonusCredits = balanceBean.getPlayCredits();

		if(viewLast30DaysGamingActivity()){
			retrieveLastGamingActivity();
		}

		if(viewCorporateHierarchy()){
			hierarchy = corporatesService.getCorporateHierarchy(userId);
			Collections.reverse(hierarchy);
		}
		currency = currencyParser.getCurrencySymbol(locale);
		
		if(isCorporate()){
			CorporateEarningsBean earningsBean = corporateEarningsService.getEarnings(userId);
			
			earnings = earningsBean.getEarnings();
			
			if(earnings==null) {
				earnings = 0.00;
			}
		}
	}

	public String getStatus(){
		if (user.getIsClosed()) {
			return messages.get("account-closed");
		} 
		if (user.getIsActive()) {
			return messages.get("active");
		}
		return messages.get("inactive");
	}
	
	public String getWebLoginLine1() {
		return formatDate(webLogin.getLoginDate()) + " - " + webLogin.getApplicationName();
	}
	
	public String getWebLoginLine2() throws ServiceException {
		StringBuilder location =  new StringBuilder();
		
		location.append(webLogin.getClientIp());


		LocationBean loc = geoIpService.getLocationFromIp(webLogin.getClientIp());

		if (loc != null) {
			if (loc.getCity() != null) {
				location.append(loc.getCity()).append(", ");
			}

			location.append(loc.getName());
		}
		
		return location.toString();
	}

	public List<WebLoginBean> getWebLogins() throws ServiceException{
		return webLoginsService.getLogins(userId, null, 5, 0, new SortContext<WebLoginBean>("!webLoginId")).getResult();
	}
	
	public String getUserLoginLine1() {
		return formatDate(userLogin.getLoginDate()) + " - " + userLogin.getMacAddess();
	}
	
	public String getUserLoginLine2() throws ServiceException {
		StringBuilder location = new StringBuilder();
		
		location.append(userLogin.getLoginIp());
		
		LocationBean loc = geoIpService.getLocationFromIp(userLogin.getLoginIp());

		if (loc != null) {
			location.append(" - ");

			if (loc.getCity() != null) {
				location.append(loc.getCity()).append(", ");
			}

			location.append(loc.getName());
		}
		
		return location.toString();
	}

	public List<UserLoginBean> getUserLogins() throws ServiceException{
		return userLoginService.getLogins(userId, null, 5, 0, new SortContext<UserLoginBean>("!loginDate")).getResult();
	}

	public String formatDate(Date d) {
		return FormatUtils.getFormattedDate(d, ((BackofficePrincipalBean)principalService.getPrincipal()).getTimeZone(), locale);
	}

	public String formatAmount(Double amount) {
		if (amount == null)
			return "";

		return currencyParser.formatDouble(amount, locale);
	}
	
	public String formatCurrency(Double amount) {
		if (amount == null)
			return "";

		return currencyParser.formatCurrency(amount, locale);
	}

	public boolean viewEditUserLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.PROFILES, true) || 
				principalService.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true);
	}

	public boolean viewResetPassLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.PROFILES, true) || 
				principalService.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true);
	}

	public boolean viewActivateDeactivateLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.PROFILES, true) &&
				!user.getIsClosed();
	}

	public boolean viewCloseLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true) &&
				!user.getIsClosed();
	}

	public boolean viewDeleteLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true);
	}

	public boolean viewGameSettingsLinkForCorp() {
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true) &&
				isCorporate();
	}

	public boolean viewGameTablesLink(){
		return (isNormalUser() || isCorporateUser()) && principalService.getPrincipal().hasPrivilege(PrivilegeEnum.TABLES, false);
	}

	public boolean viewPlayerHistoryLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.PROFILES, false);
	}

	public boolean viewModifyFundsLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true);
	}

	public boolean viewUserCommentsLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.COMMENTS,false);
	}

	public boolean viewUserChangesLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE,false);
	}

	public boolean viewLast30DaysGamingActivity(){
		return isNormalUser() || isCorporateUser();
	}
	
	public boolean viewCorporateHierarchy(){
		return isCorporate() || isCorporateUser();
	}
	
	public boolean viewCorporateTasks(){
		return isCorporate();
	}
	
	public boolean viewCorporateSettingsLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true);
	}
	
	public boolean viewGameClientsLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true); 
	}

	public boolean viewSettingsChangesLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true);
	}
	
	public boolean viewSubAccountsLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true) && 
				principalService.getPrincipal().hasPrivilege(PrivilegeEnum.CORPORATE,true);
	}
	
	public boolean viewCorporateUsersLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true);
	}
	
	public boolean viewTransactionTypeLink(UserTransactionBean bean) {
		return bean.getTransactionType() != 4 && 
				principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true); 
	}
	
	public boolean isDepositTransaction(int transactionType){
		return transactionType == UserTransactionBean.PAYMENT_TRANSACTION_TYPE;
	}
	
	public boolean isWithdrawalTransaction(int transactionType){
		return transactionType == UserTransactionBean.PAYOUT_TRANSACTION_TYPE;
	}
	
	public boolean viewTransferEarningsLink(){
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.FUNDS,true) &&
				principalService.getPrincipal().hasPrivilege(PrivilegeEnum.CORPORATE,true);
	}
	
	@OnEvent(value=EventConstants.ACTION, component="activateUser")
	public void activateUser(UserDetailsBean user) throws ServiceException, ConditionalUpdateException {
		if (user.getIsActive()) {
			return;
		}

		user.setIsActive(true);
		userDetailsService.updateUser(user);
	}
	
	public String getConfirmActivateMessage(UserDetailsBean user) {
		return messages.format("confirm-activate-message", user.getUserName());
	}
	
	@OnEvent(value=EventConstants.ACTION, component="deactivateUser")
	public void deactivateUser(UserDetailsBean user) throws ServiceException, ConditionalUpdateException {
		if (!user.getIsActive()) {
			return;
		}

		user.setIsActive(false);
		userDetailsService.updateUser(user);
	}
	
	public String getConfirmDeactivateMessage(UserDetailsBean user) {
		return messages.format("confirm-deactivate-message", user.getUserName());
	}

	@OnEvent(value=EventConstants.ACTION, component="deleteUser")
	public Object deleteUser(UserDetailsBean userDetailsBean) throws ServiceException {
		try {
			userDetailsService.deleteUser(userDetailsBean);
			return SearchUsers.class;
		} catch (ServiceException e) {
			if (e instanceof DatabaseException) {
				if (e.getCause() != null && e.getCause().getMessage().toLowerCase().contains("fk")){
					detailsForm.recordError("error-delete-user");
				} else {
					detailsForm.recordError("db-error");
				}
				return null;
			} 
			throw e;
		}
	}

	public String getConfirmDeleteMessage() throws ServiceException {
		UserDetailsBean userDetailsBean = userDetailsService.getUser(userId);
		return messages.format("confirm-delete-message", userDetailsBean.getUserName());
	}

	public boolean isCorporate(){
		return user.getUserType() == UserTypeEnum.CORPORATE_ACCOUNT.getId() ? true : false;
	}

	public boolean isNormalUser(){
		return user.getUserType() == UserTypeEnum.NORMAL_USER.getId();
	}

	public boolean isCorporateUser(){
		return user.getUserType() == UserTypeEnum.CORPORATE_PLAYER.getId() ? true : false;
	}


	// Close user
	@OnEvent(value=EventConstants.ACTION, component="closeUser")
	public void closeUser(Long userId) {

		try {
			userDetailsService.closeAccount(userId);
		} catch (ServiceException e) {
			if (e instanceof FinancialInconsistencyException) {
				detailsForm.recordError(messages.get("error-close-account"));
				return;
			}

			log.error("Unable to close account " + userId, e);
			detailsForm.recordError("db-error");
			return;
		}
	}
	
	public String getConfirmCloseMessage() throws ServiceException {
		UserDetailsBean userDetailsBean = userDetailsService.getUser(userId);
		return messages.format("confirm-close-message", userDetailsBean.getUserName());
	}
	
	@OnEvent(value=EventConstants.ACTION, component="clearBalance")
	public void clearBalance(Long userId) throws ServiceException {
		balanceAdjustmentsService.clearBalance(userId);
	}
	
	public String getConfirmClearBalanceMessage(UserDetailsBean userDetailsBean) {
		return messages.format("confirm-clear-balance-message", userDetailsBean.getUserName());
	}

	public void retrieveLastGamingActivity(){
		List<GameSummaryHistoryBean> gameHistory;
		Date beginDate;
		Date endDate = new Date();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);

		beginDate = cal.getTime();

		SearchContext<GameSummaryHistoryBean> searchContext = FiqlContextBuilder.create(GameSummaryHistoryBean.class, "periodDate=ge=?;periodDate=le=?",beginDate,endDate);

		try {
			gameHistory = gameHistoryService.getSummaryGameHistory(userId, searchContext, null, 0, 0).getResult();
		} catch (ServiceException e) {
			log.error("Error while getting game history");
			return;
		}

		if(gameHistory.isEmpty()){
			return;
		}
		
		for(GameSummaryHistoryBean bean:gameHistory){
			rounds += bean.getRounds();
			bets += bean.getBets();
			wins += bean.getWins();
			realBets += bean.getRealBets();
			realWins += bean.getRealWins();
			jackpotWins += bean.getJackpotWins();
			payout = ((wins / bets)*100);
		}
	}
	
	public GridDataSource getLastTenTransactions(){
		return new LastUserTransactionsGridDataSource(lastUserTransactionsMockUpService, 10,userId);	
	}
	
	public String getPaymentMethod(String method){
		if(method == null){
			return null;
		}
		
		return messages.get(method);
	}
	
	public int countSubAccounts() throws ServiceException{
		SearchContext<AffiliateUsersBean> searchContext = FiqlContextBuilder.create(AffiliateUsersBean.class, "affiliation==3");
		int numberOfSubs = 0;

		numberOfSubs =  corporatesService.getSubAccountsSize(userId, searchContext);
	
		return numberOfSubs;
	}
	
	public int hasUsers() throws ServiceException{
		SearchContext<AffiliateUsersBean> searchContext = FiqlContextBuilder.create(AffiliateUsersBean.class, "affiliation==1");
		int numberOfUsers = 0;
		
		numberOfUsers =  corporatesService.getSubAccountsSize(userId, searchContext);
		
		return numberOfUsers;
	}
	
	public String getTransactionType(Integer transactionType) {
		if (transactionType == null) {
			return null;
		}
		
		switch (transactionType) {
			case UserTransactionBean.BALANCE_ADJUSTMENT:
				return messages.get("Adjustment");
			case UserTransactionBean.PAYMENT_TRANSACTION_TYPE:
				return messages.get("Deposit");
			case UserTransactionBean.PAYOUT_TRANSACTION_TYPE:
				return messages.get("Withdrawal");
			default:
				return "Unknown";
		}
	}
	
	public List<RoleBean> getRoles(){
		return rolesMockUpService.getRoles(userId);
	}
	
	public String getFields(){
		switch (UserTypeEnum.valueOf(user.getUserType())) {
		case NORMAL_USER:
		case CORPORATE_PLAYER:
		case DEMO_USER:
		case STAFF_USER:
		case CORPORATE_ACCOUNT:
			return "userName,nickName,firstName,middleName,lastName,registeredDate,phone,email," +
					"town,region,postalAddress,postalCode,countryCode,userTypeEnum";
		default:
			break;
		}
		return currency;
	}
	
	public String getAddedFields(){
		String values = "lastGameLogins,lastWebLogins,status,userBalance,memberOf";
		
		if (user.getUserTypeEnum() == UserTypeEnum.CORPORATE_ACCOUNT) {
			values += ",earnings";
		}
		
		return values;
	}
}
