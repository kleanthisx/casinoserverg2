/**
 * 
 */
package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.casino.common.utils.BeanLoader;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GameTableTableModel implements IBasicTableModel {

    private Integer gameId;
    private Long userId;
    private boolean closed;

    public GameTableTableModel(Integer gameId) {
        this(gameId, null, false);
    }

    public GameTableTableModel(Integer gameId, Long userId, boolean closed) {    	
        this.gameId = gameId;
        this.userId = userId;
        this.closed = closed;
    }

    @Override
    public Iterator<GameTableBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<GameTableBean> it = null;

        if (dbConn == null){
            return null;
        }

        try
        {
            String sortColumnName = "Id";

            if (sortCol != null)
                sortColumnName = sortCol.getColumnName();

            if (("TableId").equals(sortColumnName))
                sortColumnName = "table_id";
            else if (("Owner").equals(sortColumnName))
                sortColumnName = "table_owner";
            else if (("GameId").equals(sortColumnName))
                sortColumnName = "game_type_id";
            else
                sortColumnName = null;

            String sql = 
                "SELECT * "+
                " FROM game_tables";

            if (closed) {
            	sql += " WHERE date_closed IS NOT NULL";
            } else {
            	sql += " WHERE date_closed IS NULL";
            }

            if(gameId != null) {
                sql += " AND game_type_id = ?";
            }
            
            if (userId != null) {
            	sql += " AND table_owner = ?";
            }

            if (sortColumnName != null){
                sql += " ORDER BY "+sortColumnName;

                if (sortAsc)
                    sql += " ASC";
                else
                    sql += " DESC";
            } 

            sql += " LIMIT ? OFFSET ?";

            statement = dbConn.prepareStatement(sql);

            int i = 1;

            if (gameId != null) {
                statement.setInt(i++, gameId);
            }
            
            if(userId != null) {
                statement.setLong(i++, userId);
               
            }
            
            statement.setInt(i++,limit);
            statement.setInt(i,offset);

            res = statement.executeQuery();

            List<GameTableBean> myArr = new ArrayList<GameTableBean>();
            BeanLoader<GameTableBean> loader = new BeanLoader<GameTableBean>(GameTableBean.class);

            while(res.next()) {
                myArr.add(loader.fill(new GameTableBean(), res));
            }

            it = myArr.iterator();

        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }

        return it;
    }

    @Override
    public int getRowCount() {
        int count = 0;
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn == null){
            return 0;
        }

        try
        {
            String sql = 
                "SELECT COUNT(*)" +
                " FROM game_tables" + 
                " WHERE 1=1";

            if(gameId != null) {
                sql +=   " AND game_tables.game_type_id = ?";
            } 
            
            if (userId != null) {
                sql +=   " AND game_tables.table_owner = ?";
            }
            
            sql += (closed ? " AND game_tables.date_closed IS NOT NULL" : " AND game_tables.date_closed IS NULL");

            statement = dbConn.prepareStatement(sql);
            
            if(gameId != null) {
                statement.setInt(1,gameId);
            }
            
            if (userId != null) {
                statement.setLong(1, userId);
            }

            res = statement.executeQuery();

            if (res.next()){
                count = res.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }

        return count;
    }
}
