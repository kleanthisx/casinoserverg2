package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.ReportGenerationLogService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ReportGenerationLogBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class ReportGenerationLogGridDataSource extends GenericGridDataSource<ReportGenerationLogBean> {

	private final ReportGenerationLogService reportGenerationLogService;
	private final SearchContext<ReportGenerationLogBean> searchContext;
	
	public ReportGenerationLogGridDataSource(SearchContext<ReportGenerationLogBean> searchContext, ReportGenerationLogService reportGenerationLogService) {
		this.searchContext = searchContext;
		this.reportGenerationLogService = reportGenerationLogService;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return reportGenerationLogService.getReportLogsSize(searchContext);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ReportGenerationLogBean> getData(int limit, int offset,
			SortContext<ReportGenerationLogBean> sortContext)
			throws Exception {
		
		return reportGenerationLogService.getReportLogs(searchContext, limit, offset, sortContext).getResult();
	}

}
