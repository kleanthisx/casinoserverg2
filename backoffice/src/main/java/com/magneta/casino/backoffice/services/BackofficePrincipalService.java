package com.magneta.casino.backoffice.services;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;

public interface BackofficePrincipalService extends PrincipalService {
	void setThreadPrincipal(ExtendedPrincipalBean principal);
}
