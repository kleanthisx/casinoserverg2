package com.magneta.casino.backoffice.validators;

import java.util.Calendar;
import java.util.Date;

import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.ioc.MessageFormatter;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.validator.AbstractValidator;

public class MinimumAgePeriodValidator extends AbstractValidator<Void,Date>{

	public MinimumAgePeriodValidator() {
		super(null, Date.class, "invalid-age");
	}

	private Date getMaxPeriod() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -18);
		return cal.getTime();
	}
	
	@Override
	public void validate(Field field, Void constraintValue,
			MessageFormatter formatter, Date value) throws ValidationException {
		if(value != null){
			if(value.after(getMaxPeriod())){
				throw new ValidationException(formatter.format(field.getLabel()));
			}
		}
	}
	
	@Override
	public void render(Field field, Void constraintValue,
			MessageFormatter formatter, MarkupWriter writer,
			FormSupport formSupport) {
		
		formSupport.addValidation(field, "minAgeLimit", formatter.format(field.getLabel()), null);
		
	}

}
