package com.magneta.casino.backoffice.managers.impl;

import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.magneta.Config;
import com.magneta.administration.SystemProperties;
import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.beans.SystemPrincipalBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.WebLoginsService;
import com.magneta.casino.services.beans.UserAuthenticationBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.WebLoginBean;

public class PrincipalServiceImpl implements BackofficePrincipalService {

	private static final ExtendedPrincipalBean systemBean = new SystemPrincipalBean();
	private static final String PRINCIPAL_KEY = "casino:principal";
	private static final String PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY = "casino:user.system";

	private final HttpServletRequest request;
	private final ThreadLocal<ExtendedPrincipalBean> threadPrincipal;
	private final UserDetailsService userDetailsService;
	private final WebLoginsService webLoginsService;
	
	public PrincipalServiceImpl(HttpServletRequest request,
			UserDetailsService userDetailsService,
			WebLoginsService webLoginsService) {
		this.request = request;
		this.userDetailsService = userDetailsService;
		this.webLoginsService = webLoginsService;	
		this.threadPrincipal = new ThreadLocal<ExtendedPrincipalBean>();
	}
	
	@Override
	public ExtendedPrincipalBean getPrincipal() {
		if (this.threadPrincipal.get() != null) {
			return this.threadPrincipal.get();
		}
		
		Integer counter = (Integer)request.getAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY);
		
		if (counter != null && counter > 0) {
			return systemBean;
		}
		
		HttpSession session = request.getSession(false);
		
		if (session == null) {
			return null;
		}
		
		try {
			return (ExtendedPrincipalBean)session.getAttribute(PRINCIPAL_KEY);
		} catch (IllegalStateException e) {
			return null;
		}
	}
	
	@Override
	public void setThreadPrincipal(ExtendedPrincipalBean principal) {
		if (principal == null) {
			this.threadPrincipal.remove();
		} else {
			this.threadPrincipal.set(principal);
		}
	}

	private boolean isTrustedProxy(String ip) {
		String proxyList = Config.get("server.proxies");
		
		if (proxyList == null) {
			return false;
		}

		String[] proxies =  proxyList.split(" ");
		
		for (String proxy : proxies) {
			if (proxy.trim().equals(ip)) {
				return true;
			}
		}
		
		return false;
	}
	
	private String getRemoteIp() {
		String forwardedFor = request.getHeader("X-Forwarded-For");
		
		if (forwardedFor == null || !isTrustedProxy(request.getRemoteAddr())) {
			return request.getRemoteAddr();
		}
		
		String[] forwarders = forwardedFor.split(",");

		if (forwarders.length == 1) {
			return request.getRemoteAddr();
		}
		
		for (int i=1; i < forwarders.length; i++) {
			if (!isTrustedProxy(forwarders[i].trim())) {
				return request.getRemoteAddr();
			}
		}
		
		return forwarders[0].trim();
	}
	
	@Override
	public void onUserAuthenticated(UserAuthenticationBean authenticatedUser) throws ServiceException {
		BackofficePrincipalBean user = new BackofficePrincipalBean(authenticatedUser);
		try {
			pushSystem();
			UserDetailsBean userDetails = userDetailsService.getUser(authenticatedUser.getUserId());
			
			if (userDetails != null && userDetails.getTimezone() != null) {
				user.setTimeZone(TimeZone.getTimeZone(userDetails.getTimezone()));
			}
			WebLoginBean loginBean = new WebLoginBean();
			
			loginBean.setApplicationName(SystemProperties.APP_NAME);
			loginBean.setClientIp(getRemoteIp());
			loginBean.setUserId(authenticatedUser.getUserId());
			webLoginsService.recordLogin(loginBean);			
		} finally {
			popSystem();
		}

		request.getSession(true).setAttribute(PRINCIPAL_KEY, user);
	}

	@Override
	public void onUserLogout() {
		HttpSession session = request.getSession(false);
		
		if (session == null) {
			return;
		}
		
		session.removeAttribute(PRINCIPAL_KEY);
	}

	@Override
	public void pushSystem() {
		Integer counter = (Integer)request.getAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY);
		
		if (counter == null) {
			counter = 0;
		}
		
		counter++;
		
		request.setAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY, counter);
	}

	@Override
	public void popSystem() {
		Integer counter = (Integer)request.getAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY);
		
		if (counter == null || counter.equals(0)) {
			throw new RuntimeException("Unbalanced push/popSystem");
		}
		
		counter--;
		
		if (counter.equals(0)) {
			request.removeAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY);
		} else {
			request.setAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY, counter);
		}
	}

	@Override
	public void resetSystem() {
		request.removeAttribute(PRINCIPAL_REQUEST_SYSTEM_COUNTER_KEY);
	}
}
