package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.HostWhitelistBean;
import com.magneta.casino.services.search.SortContext;

public class HostsWhitelistGridDataSource extends GenericGridDataSource<HostWhitelistBean> {

	private final HostsWhitelistService hostsWhitelistService;
	
	public HostsWhitelistGridDataSource(HostsWhitelistService hostsWhitelistService){
		this.hostsWhitelistService = hostsWhitelistService;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return hostsWhitelistService.getHostsSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException();
		}
	}

	@Override
	public List<HostWhitelistBean> getData(int limit, int offset,
			SortContext<HostWhitelistBean> sortContext) throws Exception {
		return hostsWhitelistService.getHosts(null, limit, offset, sortContext).getResult();
	}
}
