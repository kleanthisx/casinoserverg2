package com.magneta.casino.backoffice.managers.reporting.impl;

import java.net.URL;
import java.util.Date;
import java.util.concurrent.Future;

import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.RepositoryReport;

public class RepositoryReportImpl implements RepositoryReport {

	private final Long userId;
	private final String requestId;
	private final String filename;
	private final ReportFormat format;
	private final Future<URL> future;
	private final Date requestDate;
	
	public RepositoryReportImpl(ReportRequest request, Future<URL> future) {
		this.userId = request.getPrincipal().getUserId();
		this.requestId = request.getId();
		this.filename = request.getReport().getName() + "." + request.getReportFormat().getFileExtension();
		this.format = request.getReportFormat();
		this.future = future;
		this.requestDate = new Date();
	}
	
	@Override
	public String getRequestId() {
		return requestId;
	}

	@Override
	public Long getUserId() {
		return userId;
	}

	@Override
	public String getFilename() {
		return filename;
	}

	@Override
	public ReportFormat getFormat() {
		return format;
	}

	@Override
	public Future<URL> getFuture() {
		return future;
	}

	@Override
	public Date getRequestDate() {
		return requestDate;
	}
}
