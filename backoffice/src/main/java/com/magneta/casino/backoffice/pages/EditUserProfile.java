package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.CountrySelectModel;
import com.magneta.casino.backoffice.selectmodels.TimeZoneSelectModel;
import com.magneta.casino.backoffice.valueencoders.CountryEncoder;
import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.TimeZoneService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(or={PrivilegeEnum.PROFILES,PrivilegeEnum.ROLE})
public class EditUserProfile {
	
	@Inject
	private PageRenderLinkSource linkSource;

	@Component(id="editProfileForm")
	private BeanEditForm editProfileForm;
	
	@InjectService("UserDetailsService")
	private UserDetailsService userDetailsService;
	
	@InjectService("CountryService")
	private CountryService countryService;
	
	@InjectService("TimeZoneService")
	private TimeZoneService timeZoneService;
	
	@Property
	private CountryBean country;
	
	@Property
	private UserDetailsBean userDetailsBean;
	
	private Long userId;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long userId) {
		this.userId = userId;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Long passivate() {
		return userId;
	}
	
	@OnEvent(value = EventConstants.PREPARE)
	public void onPrepare() throws ServiceException{
		userDetailsBean = userDetailsService.getUser(userId);
		country = countryService.getCountry(userDetailsBean.getCountryCode());
	}
	
	@OnEvent(value = EventConstants.SUCCESS)
	public Object onSuccess(){
		userDetailsBean.setCountryCode(country.getCode());
		
		try {
			userDetailsService.updateUser(userDetailsBean);
		} catch (ServiceException e) {
			editProfileForm.recordError(e.getMessage());
			return null;
		} catch (ConditionalUpdateException e) {
			editProfileForm.recordError(e.getMessage());
			return null;
		}
		
		return linkSource.createPageRenderLinkWithContext(UserDetails.class, userId);
	}
	
	
	public CountrySelectModel getCountrySelectModel(){
		return new CountrySelectModel(countryService);
	}
	
	public CountryEncoder getCountryEncoder(){
		return new CountryEncoder(countryService);
	}
	
	public TimeZoneSelectModel getTimeZoneSelectModel(){
		return new TimeZoneSelectModel(timeZoneService);
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return linkSource.createPageRenderLinkWithContext(UserDetails.class, userId);
	}
}
