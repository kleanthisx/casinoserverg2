package com.magneta.casino.backoffice.t4.services;

import org.apache.tapestry.IDirect;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.engine.DirectServiceParameter;
import org.apache.tapestry.engine.IEngineService;

import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.tapestry4.components.pagemenu.PageMenuItem;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemLinkGenerator;

public class PageMenuItemLinkGeneratorImpl implements PageMenuItemLinkGenerator {

	@Override
	public String getUrl(IRequestCycle cycle, IDirect menuComponent, PageMenuItem item) {
    	if (item.getTargetPage() != null) {
    		return LinkGenerator.getPageUrl(item.getTargetPage(), item.getParams());
        } else if ("logout".equals(item.getSpecialFunction())) {
            IEngineService s = cycle.getInfrastructure().getServiceMap().getService(Tapestry.DIRECT_SERVICE);
            return s.getLink(true, new DirectServiceParameter(menuComponent, new Object[]{"logout"})).getURL();
        }
    	
    	return null;
	}
}
