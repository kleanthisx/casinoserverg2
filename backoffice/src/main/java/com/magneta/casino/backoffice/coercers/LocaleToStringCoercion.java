package com.magneta.casino.backoffice.coercers;

import java.util.Locale;

import org.apache.tapestry5.ioc.services.Coercion;

public class LocaleToStringCoercion implements Coercion<Locale,String> {

	@Override
	public String coerce(Locale input) {
		if (input == null) {
			return null;
		}

		return input.toString();
	}
}
