package com.magneta.casino.backoffice.t4.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.t4.utils.TableSortContext;
import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.HostWhitelistBean;

public class HostsWhitelistTableModel implements IBasicTableModel {
	
	HostsWhitelistService hostWhitelistService = ServiceLocator.getService(HostsWhitelistService.class);
	
	@Override
	public Iterator<HostWhitelistBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		List<HostWhitelistBean> hostWhitelists = null;
		
		try {
			hostWhitelists = hostWhitelistService.getHosts(null, limit, offset, new TableSortContext<HostWhitelistBean>(sortCol, sortAsc)).getResult();
		} catch (ServiceException e) {
			throw new RuntimeException("Error while retrieving hosts.",e);
		}
		
		if(hostWhitelists == null){
			return new ArrayList<HostWhitelistBean>().iterator(); 
		}
		
		Iterator<HostWhitelistBean> it = hostWhitelists.iterator();
		return it;
	}

	@Override
	public int getRowCount() {
		int size = 0;
		try {
			size = hostWhitelistService.getHostsSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException("Error while retrieving hosts size.",e);
		}
		return size;
	}
}
