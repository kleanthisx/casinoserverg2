package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.link.ExternalLink;

import com.magneta.administration.beans.PartnerAccountChangeBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.PartnerAccountChangesTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.CORPORATE)
public abstract class PartnerChanges extends SecurePage implements PageBeginRenderListener {

	@InitialValue("-1")
	@Persist("client:page")
	public abstract void setPartnerId(Long partnerId);
	public abstract Long getPartnerId();
	
	public abstract void setUsername(String username);

	public abstract PartnerAccountChangeBean getCurrChange();
	
	@Component(bindings={"value=username+getMsg('page-title')"})
	public abstract Insert getTitleMsg();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:partnerAccountChangesTableModel",
			"columns=literal:ChangeTime, !Username, !Field, !OldValue, !NewValue, ChangeUser","pageSize=20",
			"initialSortColumn=literal:ChangeTime","row=currChange","initialSortOrder=false"})
	public abstract Table getLogTable();
	
	public PartnerAccountChangesTableModel getPartnerAccountChangesTableModel(){
		return new PartnerAccountChangesTableModel(getPartnerId());
	}
	
	@Component(bindings={"page=literal:CorporateAccounts", "parameters={partnerId}"})
	public abstract ExternalLink getBackLink();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setPartnerId((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent evt){
		if (getPartnerId() != null) {
			setUsername(getUsername(getPartnerId()));
    	} else {
    		setUsername("");
    	}
	}
}
