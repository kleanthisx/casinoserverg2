package com.magneta.casino.backoffice.pages;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.services.UploadedFile;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.GameConfigsGridDataSource;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.poker.PokerConfiguration;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class GameConfigs {

	@Inject
	private GameConfigsService gameConfigsService;
	
	@Inject
	private GamesService gamesService;
	
	@Inject
	private Messages messages;
	
	@Inject
	private ExtendedPrincipalBean principal;
	
	@Property
	private GameConfigBean gameConfig;
	
	@Property
	private Integer gameId;
	
	@Property
	private UploadedFile file;
	
	@Component
	private Form importForm;
	
	@Cached(watch="gameConfig")
	public boolean isActiveConfig() throws ServiceException {
		GameConfigBean activeConfig = gamesService.getGameActiveConfig(gameId);
		
		if (activeConfig == null) {
			return false;
		}

		return activeConfig.getConfigId().equals(gameConfig.getConfigId());
	}
	
	public boolean isShowActivateLink() throws ServiceException {
		if (gameConfig.getConfigId() == null || gameConfig.getConfigId().equals(0l) || isActiveConfig()) {
			return false;
		}
		
		return true;
	}
	
	@Cached
	public boolean isCanEditConfigs() {
		return principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true);
	}
	
	public GameConfigsGridDataSource getDataSource() {
		return new GameConfigsGridDataSource(gameId, gameConfigsService);
	}
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Integer gameId) {
		this.gameId = gameId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Integer onPassivate() {
		return gameId;
	}
	
	public String getActivateConfirmMessage(){
		return messages.format("activate-confirm-message", gameConfig.getConfigId());		
	}
	
	@OnEvent(value=EventConstants.ACTION, component="activate")
	public void activateConfig(Long configId) throws ServiceException {
		gamesService.setGameActiveConfig(gameId, configId);
	}

	@OnEvent(value=EventConstants.VALIDATE, component="importForm")
	public void onSuccess()
	{
		GameConfigBean config;
		try {
			JAXBContext context = JAXBContext.newInstance(GameConfigBean.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			config = (GameConfigBean)unmarshaller.unmarshal(file.getStream());
		} catch (JAXBException e) {			
			String message = e.getMessage();
			
			Throwable ex = e;
			
			while (ex.getCause() != null) {
				message = ex.getCause().getMessage();
				ex = ex.getCause();
			}
			
			importForm.recordError("Invalid file: " + message);
			return;
		}
		
		if (!gameId.equals(config.getGameId())) {
			importForm.recordError("Configuration is for another game");
			return;
		}
		
		try {
			gameConfigsService.insertConfig(config);
		} catch (ServiceException e) {
			String message = e.getMessage();
			
			Throwable ex = e;
			
			while (ex.getCause() != null) {
				message = e.getCause().getMessage();
				ex = ex.getCause();
			}
			
			importForm.recordError("Unable to import config file: " + message);
		}
	}
	
	public String getCreatePage() throws ServiceException  {
		GameConfiguration c;
		try {
			c = gameConfigsService.getFilteredConfiguration(gameId, gameConfig.getConfigId());
		} catch (ServiceException e) {
			c = gameConfigsService.getFilteredConfiguration(gameId, 0l);
		}

		if (c instanceof SlotConfiguration) {
			return "gameconfig/SlotCreateConfig";
		} else if (c instanceof PokerConfiguration) {
			return "gameconfig/VideoPokerCreateConfig";
		} else if (c instanceof IslandStudPokerConfiguration) {
			return "gameconfig/StudPokerCreateConfig";
		} else if (c instanceof TexasHoldemConfiguration) {
			return "gameconfig/TexasHoldEmPokerCreateConfig";
		}

		return "CreateGameConfig";
	}
	
	public Object[] getPageActivationContext() {
		return new Object[] {gameId, gameConfig.getConfigId()};
	}
	
	public Object[] getCreatePageActivationContext() {
		return new Object[] {gameId, 0l};
	}
	
	@Cached
	public String getPageTitle() throws ServiceException {
		GameBean game = gamesService.getGame(gameId);
		
		return messages.format("page-title", game.getName());
	}
	
	public String getCssRowClass() throws ServiceException {
		if (isActiveConfig()) {
			return "active";
		}
		return null;
	}
}
