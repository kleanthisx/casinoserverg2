package com.magneta.casino.backoffice.t4.models;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.beans.ServiceResultSet;

public class GameClientsSelectionModel implements IPropertySelectionModel {

	private GameClientService gameClientService = ServiceLocator.getService(GameClientService.class);
	private ServiceResultSet<GameClientBean> gameClients;
	
	public GameClientsSelectionModel() {
		GameClientBean gameClientBean = new GameClientBean();
		gameClientBean.setClientId(null);
		gameClientBean.setClientDescription("--Select Game Client--");
		
		try {
			gameClients = gameClientService.getGameClients(0, 0);	
			gameClients.getResult().add(0,gameClientBean);
			
		} catch (ServiceException e) {
			throw new RuntimeException("Unable to load categories", e);
		}
	}

	@Override
	public String getLabel(int index) {
		return gameClients.getResult().get(index).getClientDescription();
	}


	@Override
	public Object getOption(int index) {
		 return gameClients.getResult().get(index).getClientId();
	}


	@Override
	public int getOptionCount() {
		return gameClients.getResult().size();
	}


	@Override
	public String getValue(int index) {
		GameClientBean client = gameClients.getResult().get(index);
		
		if (client == null || client.getClientId() == null) {
			return null;
		}
		
		return client.getClientId().toString();
	}


	@Override
	public boolean isDisabled(int index) {
		return false;
	}

	@Override
	public Object translateValue(String val) {
		if (val == null || val.trim().isEmpty()) {
			return null;
		}
		return Integer.parseInt(val);
	}
}
