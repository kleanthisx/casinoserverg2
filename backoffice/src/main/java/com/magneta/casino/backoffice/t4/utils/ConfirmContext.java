package com.magneta.casino.backoffice.t4.utils;

import java.io.Serializable;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.services.ServiceException;

public abstract class ConfirmContext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6798203109403227547L;
	private String okMsg;
	private String cancelMsg;
	private String confirmMsg;
	private ICallback callback;
	
	public ConfirmContext(String okMsg,String cancelMsg,String confirmMsg, ICallback callback){
		this.okMsg = okMsg;
		this.cancelMsg = cancelMsg;
		this.confirmMsg = confirmMsg;
		this.callback = callback;
	}
	
	public abstract void onOk(IRequestCycle cycle, ValidationDelegate delegate) throws ServiceException;
	
	public void onCancel(IRequestCycle cycle, ValidationDelegate delegate) {
		returnToPage(cycle);
	}

	protected final void returnToPage(IRequestCycle cycle) {
		cycle.forgetPage(cycle.getPage().getPageName());
		callback.performCallback(cycle);
	}
	
	public String[] messages(){
		return new String[]{confirmMsg,okMsg,cancelMsg}; 
	}
	
}
