package com.magneta.casino.backoffice.managers.reporting.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.magneta.casino.backoffice.managers.reporting.ReportRepository;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.backoffice.managers.reporting.RepositoryReport;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;

public class ReportRepositoryImpl implements ReportRepository {

	private final ReportService reportService;
	private final List<RepositoryReport> reportList;
	
	public ReportRepositoryImpl(ReportService reportService) {
		this.reportService = reportService;
		this.reportList = new CopyOnWriteArrayList<RepositoryReport>();
	}
	
	@Override
	public RepositoryReport addScheduledReportRequest(ReportRequest request) {
		RepositoryReport report = new RepositoryReportImpl(request, reportService.executeReportAsync(request));
		reportList.add(report);
		return report;
	}
	
	@Override
	public void removeReport(String requestId) {
		for (RepositoryReport report: reportList) {			
			if (report.getRequestId().equals(requestId)) {
				report.getFuture().cancel(true);
				reportList.remove(report);
				break;
			}
		}
	}
	
	@Override
	public void cancelReport(String requestId) {
		for (RepositoryReport report: reportList) {			
			if (report.getRequestId().equals(requestId)) {
				report.getFuture().cancel(true);
				break;
			}
		}
	}

	@Override
	public List<RepositoryReport> getReports(SearchContext<RepositoryReport> searchContext) throws ServiceException {
		
		if (searchContext == null) {
			List<RepositoryReport> reports = new ArrayList<RepositoryReport>(reportList.size());
			
			for (RepositoryReport report: reportList) {			
				reports.add(report);
			}
			
			return reports;
		}

		return searchContext.getCondition().findAll(reportList);
	}
	
	@Override
	public int getReportsSize(SearchContext<RepositoryReport> searchContext) throws ServiceException {
		
		if (searchContext == null) {
			return reportList.size();
		}

		return searchContext.getCondition().countAll(reportList);
	}

	@Override
	public RepositoryReport getReport(String requestId) throws ServiceException {
		SearchContext<RepositoryReport> searchContext = FiqlContextBuilder.create(RepositoryReport.class, "requestId==?", requestId);
		
		List<RepositoryReport> reports = searchContext.getCondition().findAll(reportList);
		
		if (reports.isEmpty()) {
			throw new ServiceException("Report not found");
		}
		
		return reports.get(0);
	}
}
