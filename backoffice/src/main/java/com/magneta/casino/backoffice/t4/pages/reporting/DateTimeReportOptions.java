package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.dojo.form.DropdownTimePicker;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.valid.FieldLabel;

public abstract class DateTimeReportOptions extends ReportOptions {
	
	@Component(id = "fromTime",
			bindings = {"value=minTime", "disabled=false","displayName=message:Time", "translator=translator:date,pattern=HH:mm"})
	public abstract DropdownTimePicker getFromTime();
	
	@Component(id = "toTime",
			bindings = {"value=maxTime", "disabled=false","displayName=message:Time", "translator=translator:date,pattern=HH:mm"})
		public abstract DropdownTimePicker getToTime();
	
	@Component(id = "fromDate",
			bindings={"value=minDate","displayName=message:From", "translator=translator:date,pattern=dd/MM/yyyy"})
	public abstract DatePicker getFromDate();
		
	@Component(id = "toDate",
			bindings={"value=maxDate","displayName=message:To", "translator=translator:date,pattern=dd/MM/yyyy"})
	public abstract DatePicker getToDate();
		
	@Component(id = "fromDateLabel",
			bindings={"field=component:fromDate"})
	public abstract FieldLabel getFromDateLabel();
		
	@Component(id = "toDateLabel",
			bindings={"field=component:toDate"})
	public abstract FieldLabel getToDateLabel();
		
	@Component(id = "periodMsg",
			bindings = {"value=message:Period"})
	public abstract Insert getPeriodMsg();
		
	public abstract Date getMinDate();
	public abstract Date getMaxDate();
	
	public abstract Date getMinTime();
	public abstract Date getMaxTime();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		
		Date maxDate = normalizeDate(getMaxDate());
		Date minDate = normalizeDate(getMinDate());
        
		//NORMALIZE DATES
        if (minDate != null){
        	Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
        	
        	int hour = 0;
        	int minute = 0;
        	
        	Date minTime = normalizeDate(getMinTime());
        	
        	if (minTime != null){
        		calendar.setTimeInMillis(minTime.getTime());
        	
        		hour = calendar.get(Calendar.HOUR_OF_DAY);
        		minute = calendar.get(Calendar.MINUTE);
        	}
        	
        	calendar.setTimeInMillis(minDate.getTime());
        	calendar.set(Calendar.HOUR_OF_DAY, hour);
        	calendar.set(Calendar.MINUTE, minute);
        	
            reportParams.put("min_date", calendar.getTime());
        }
		
        if (maxDate != null){
        	Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
        	
        	Date maxTime = normalizeDate(getMaxTime());
        	
        	if (maxTime != null){
        		calendar.setTimeInMillis(maxTime.getTime());
            	
        		int hour = calendar.get(Calendar.HOUR_OF_DAY);
        		int minute = calendar.get(Calendar.MINUTE);
        		
        		calendar.setTimeInMillis(maxDate.getTime());
        		calendar.set(Calendar.HOUR_OF_DAY, hour);
            	calendar.set(Calendar.MINUTE, minute);
        	} else {
        		calendar.setTimeInMillis(maxDate.getTime());
        		calendar.set(Calendar.HOUR_OF_DAY, 24);
        		calendar.set(Calendar.MINUTE, 0);
        		calendar.add(Calendar.MILLISECOND, -1);
        	}
        	
            reportParams.put("max_date", calendar.getTime());
        }
	}
}
