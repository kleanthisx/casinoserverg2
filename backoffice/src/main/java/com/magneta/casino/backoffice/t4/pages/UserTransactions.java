package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.beans.UserTransactionBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.LatestUserTransactionsTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.PROFILES)
public abstract class UserTransactions extends SecurePage{

	@Persist("client:page")
	public abstract Long getUserId();
	public abstract void setUserId(Long userId);
	
	public abstract UserTransactionBean getCurrTransaction();
	
	@Component(type="contrib:Table",bindings={"source=ognl:latestUserTransactionsTableModel",
			"columns=literal:!DateCompleted, !transactionId, !TransactionType, !PaymentMethod,!Amount","pageSize=20",
			"rowsClass=ognl:beans.evenOdd.next","row=currTransaction"})
	public abstract Table getLastTransactions();
	
	public String getCurrTransactionType() {
		switch (this.getCurrTransaction().getTransactionType()) {
			case UserTransactionBean.BALANCE_ADJUSTMENT:
				return getMsg("Adjustment");
			case UserTransactionBean.PAYMENT_TRANSACTION_TYPE:
				return getMsg("Deposit");
			case UserTransactionBean.PAYOUT_TRANSACTION_TYPE:
				return getMsg("Withdrawal");
			default:
				return "Unknown";
		}
	}
	
	public LatestUserTransactionsTableModel getLatestUserTransactionsTableModel(){
		return new LatestUserTransactionsTableModel(getUserId(),0);
	}

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setUserId((Long)params[0]);
		}
	}
	
}
