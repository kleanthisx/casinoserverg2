package com.magneta.casino.backoffice.enums;

public enum TriState {
	ALL(null), TRUE(true), FALSE(false);
	
	private final Boolean value;
	
	private TriState(Boolean value) {
		this.value = value;
	}
	
	public Boolean getValue() {
		return value;
	}
}