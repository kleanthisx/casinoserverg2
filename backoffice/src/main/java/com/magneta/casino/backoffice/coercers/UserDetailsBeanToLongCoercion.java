package com.magneta.casino.backoffice.coercers;

import org.apache.tapestry5.ioc.services.Coercion;

import com.magneta.casino.services.beans.UserDetailsBean;

public class UserDetailsBeanToLongCoercion implements Coercion<UserDetailsBean, Long> {

	@Override
	public Long coerce(UserDetailsBean arg0) {
		if (arg0 == null) {
			return null;
		}
		
		return arg0.getId();
	}
}
