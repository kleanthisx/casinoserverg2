package com.magneta.casino.backoffice.t4.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hivemind.Registry;
import org.apache.tapestry.ApplicationServlet;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.internal.services.RequestImpl;
import org.apache.tapestry5.internal.services.ResponseImpl;
import org.apache.tapestry5.internal.services.TapestrySessionFactory;
import org.apache.tapestry5.ioc.ObjectLocator;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.services.T5ServiceLocator;

/**
 * Tapestry 4 servlet that also set's up tapestry5 RequestGlobals so that
 * you can use all tapestry5 services within tapestry4.
 * 
 * You have to add the servlet path to your ignored paths in tapestry 5.
 * 
 * @author anarxia
 *
 */
public class Tapestry4Servlet extends ApplicationServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8983436444127796835L;
	private static final Logger log = LoggerFactory.getLogger(Tapestry4Servlet.class);

	private HttpServletRequestHandlerTerminator requestHandlerTerminator;
	
	public static class HttpServletRequestHandlerTerminator {

		private final String applicationCharset;
		private final TapestrySessionFactory sessionFactory;
		private final RequestGlobals requestGlobals;

		public HttpServletRequestHandlerTerminator(@Symbol(SymbolConstants.CHARSET) String applicationCharset,
				TapestrySessionFactory sessionFactory, RequestGlobals requestGlobals) {
			this.applicationCharset = applicationCharset;
			this.sessionFactory = sessionFactory;
			this.requestGlobals = requestGlobals;
		}

		public void prepareRequestGlobals(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
			requestGlobals.storeServletRequestResponse(servletRequest, servletResponse);

			Request request = new RequestImpl(servletRequest, applicationCharset, sessionFactory);
			Response response = new ResponseImpl(servletRequest, servletResponse);

			// TAP5-257: Make sure that the "initial guess" for request/response
			// is available, even if
			// some filter in the RequestHandler pipeline replaces them.
			requestGlobals.storeRequestResponse(request, response);
		}
	}

	@Override
	protected Registry constructRegistry(ServletConfig config) {
		log.info("Initializing Tapestry 5 support for Tapestry 4.1");
		Registry registry = super.constructRegistry(config);
		ServiceLocator.setRegistry(registry);
		
		requestHandlerTerminator = T5ServiceLocator.getService(ObjectLocator.class).autobuild(HttpServletRequestHandlerTerminator.class);

		log.info("Tapestry 5 support for Tapestry 4.1 initialized");
		return registry;
	}

	@Override
	public void doService(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Tapestry4Servlet service begin: {}", request.getRequestURI());
		try {
			requestHandlerTerminator.prepareRequestGlobals(request, response);
			super.doService(request, response);
		} finally {
			log.debug("Tapestry4Servlet service end: {}", request.getRequestURI());
		}
	}
}