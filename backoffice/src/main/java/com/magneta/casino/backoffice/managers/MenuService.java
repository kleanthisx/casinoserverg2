package com.magneta.casino.backoffice.managers;

import java.util.List;

import com.magneta.casino.backoffice.beans.MenuBean;

public interface MenuService {
	public List<MenuBean> getVerticalMenu();
	public List<MenuBean> getHorizontalMenu();
}
