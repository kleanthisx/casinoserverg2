package com.magneta.casino.backoffice.t4.pages;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry.PageNotFoundException;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.event.PageDetachListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.event.PageValidateListener;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.AccessDeniedException;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.DisplayMenu;

@PermitAll
@DisplayMenu(false)
public abstract class ExceptionPage extends PublicPage implements PageValidateListener, PageDetachListener {

	private static final Logger log = LoggerFactory.getLogger(ExceptionPage.class);
	
	@InjectObject("service:tapestry.globals.HttpServletResponse")
    public abstract HttpServletResponse getResponse();

    private Throwable t;
    
    public abstract String getExceptionMessage();
    public abstract void setExceptionMessage(String msg);
    
    public abstract String getExceptionTitle();
    public abstract void setExceptionTitle(String title);
    
    @Override
    public String getPageTitle() {
        return getExceptionTitle();
    }

    public boolean exceptionContains(Class<? extends Throwable> exceptionType) {
    	Throwable curr = t;
    	
    	for (curr = t; curr != null; curr = curr.getCause()) {
    		if (exceptionType.isInstance(curr)) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public void setException(Throwable t)
    {
    	this.t = t;
    	boolean logException = true;
    	
    	if (exceptionContains(BadRequestException.class)
        	|| exceptionContains(PageNotFoundException.class)
        	|| exceptionContains(NoClassDefFoundError.class)
            || exceptionContains(AccessDeniedException.class)) {
            logException= false;
        }

        if (logException) {
            log.error("an exception occured", t);
        }
    }

    @Override
    public void pageValidate(PageEvent event) {

    	WebResponse response = event.getRequestCycle().getInfrastructure().getResponse();
 	
        if (exceptionContains(BadRequestException.class)) {
        	setExceptionTitle(getMsg("bad-request"));
            setExceptionMessage(t.getLocalizedMessage());
        	response.setStatus(400);
        } else if (exceptionContains(AccessDeniedException.class)) {
        	setExceptionTitle(getMsg("access-denied"));
        	setExceptionMessage(getMsg("access-denied-msg"));
        	response.setStatus(403);
        } else if(exceptionContains(PageNotFoundException.class)
                	|| exceptionContains(NoClassDefFoundError.class)) {
                WebRequest request = event.getRequestCycle().getInfrastructure().getRequest();
                
                setExceptionTitle(getMsg("page-not-found"));
                setExceptionMessage(getFormatMessage("page-not-found-msg", request.getRequestURI()));
                
                response.setStatus(404);
        } else {
        	setExceptionTitle(getMsg("server-error"));
        	setExceptionMessage(getMsg("server-error-msg"));
        	response.setStatus(500);
        }
    }
	
}
