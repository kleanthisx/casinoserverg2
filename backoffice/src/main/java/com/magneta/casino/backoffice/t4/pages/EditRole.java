package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Radio;
import org.apache.tapestry.form.RadioGroup;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.RoleBean;
import com.magneta.administration.beans.RoleBean.CreateRoleResult;
import com.magneta.administration.beans.RolePrivilege;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class EditRole extends SecurePage {

	private static final Logger log = LoggerFactory.getLogger(EditRole.class);
	
    public abstract int getRoleID();
    public abstract void setRoleID(int roleID);
    
    public abstract String getRoleDesc();
    public abstract void setRoleDesc(String roleDesc);
    
    public abstract void setAvailPrivileges(List<RolePrivilege> availPrivileges);
    public abstract List<RolePrivilege> getAvailPrivileges();
    
    public abstract RolePrivilege getCurrPrivilege();
    
    @Component
	public abstract ErrorDisplay getErrorDisplay();
    
    @Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","beans.delegate=true"})
    public abstract Form getCreateRoleForm();
    
    @Component(bindings={"value=roleDesc","disabled=true","displayName=literal:Description","validators=validators:required,minLength=2,maxLength=50"})
    public abstract TextField getDescription();
    
    @Component(bindings={"field=component:description"})
    public abstract FieldLabel getDescriptionLabel();
    
    @Component(type="For",bindings={"source=availPrivileges","value=currPrivilege","element=literal:tr"})
    public abstract ForBean getAvailPrivilegesFor();
    
    @Component(bindings={"value=availPrivileges"})
    public abstract Hidden getAvailablePrivileges();
    
    @Component(bindings={"value=roleID"})
    public abstract Hidden getRoleIDField();
    
    @Component(bindings={"value=roleDesc"})
    public abstract Hidden getRoleDescription();
    
    @Component(bindings={"selected=currPrivilege.availability","validators=validators:required","disabled=!allowWrite()"})
    public abstract RadioGroup getAvailabilityRadios();
    
    @Component(bindings={"value=@com.magneta.administration.beans.RolePrivilege@NOT_GRANTED_AVAILABILITY","disabled=!allowWrite()"})
    public abstract Radio getNotGrantedRadio();
    
    @Component(bindings={"value=@com.magneta.administration.beans.RolePrivilege@READ_ONLY_AVAILABILITY","disabled=!allowWrite()"})
    public abstract Radio getReadOnlyRadio();
    
    @Component(bindings={"value=@com.magneta.administration.beans.RolePrivilege@READ_WRITE_AVAILABILITY","disabled=!allowWrite()"})
    public abstract Radio getReadWriteRadio();
    
    @Component(bindings={"disabled=!allowWrite()"})
    public abstract Submit getUpdateSubmit();
    
    @Component
    public abstract Button getCancelButton();
    
    @Override
	public void prepareForRender(IRequestCycle cycle) {
        List<RolePrivilege> availPrivileges = new ArrayList<RolePrivilege>();
        
        
        for (PrivilegeEnum p: PrivilegeEnum.values()) {
        	RolePrivilege currPriv = new RolePrivilege();
        	currPriv.setAvailability(RolePrivilege.NOT_GRANTED_AVAILABILITY);
        	currPriv.setPrivilegeKey(p.getId());
        	currPriv.setPrivilegeDesc(p.toString());
        	availPrivileges.add(currPriv);
        }
        
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn != null){
        	try {
        		statement = dbConn.prepareStatement(
        				"SELECT privilege_key, allow_write" +
        				" FROM role_privileges" +
        				" WHERE role_id= ?"+
        				" ORDER BY privilege_key");
        		statement.setInt(1, getRoleID());

        		res = statement.executeQuery();

        		while (res.next()){
        			String key = res.getString("privilege_key");

        			for (RolePrivilege p: availPrivileges){
        				if (p.getPrivilegeKey().equals(key)){
        					p.setAvailability((res.getBoolean("allow_write")? RolePrivilege.READ_WRITE_AVAILABILITY :
        						RolePrivilege.READ_ONLY_AVAILABILITY));
        					break;
        				}
        			}
        		}
        	} catch (SQLException e) {
        		log.error("Error while loading role privileges.",e);
        	} finally{
        		DbUtil.close(res);
        		DbUtil.close(statement);
        		DbUtil.close(dbConn);
        	}
        }
        
        setAvailPrivileges(availPrivileges);
    }
    
    public void onSubmit(IRequestCycle cycle){
        ValidationDelegate delegate = getErrorDisplay().getDelegate();

        if (delegate.getHasErrors()){
            return;
        }

        RoleBean editRole = new RoleBean();
        editRole.setRoleId(getRoleID());
        editRole.setRoleDesc(getRoleDesc());
        
        boolean selected = false;
        
        for (RolePrivilege priv: getAvailPrivileges()){
        	if (priv.getAvailability() != RolePrivilege.NOT_GRANTED_AVAILABILITY){
        		selected = true;
        		break;
        	}
        }
        
        if (!selected){
            delegate.setFormComponent(null);
            delegate.record(getMsg("no-priv-error"), null);
            return;
        }
        
        if (editRole.updateRole(getAvailPrivileges()) == CreateRoleResult.OK){
        	redirectToPage("RoleAdministration");
        } else {
            delegate.setFormComponent(null);
            delegate.record(getMsg("db-error"), null);
        }
    }
    
    public void onCancel(IRequestCycle cycle){
    	redirectToPage("RoleAdministration");
    }
}
