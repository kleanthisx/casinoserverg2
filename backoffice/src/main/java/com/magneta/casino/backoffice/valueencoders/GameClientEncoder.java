package com.magneta.casino.backoffice.valueencoders;

import org.apache.tapestry5.ValueEncoder;

import com.magneta.casino.services.beans.GameClientBean;

public class GameClientEncoder implements ValueEncoder<GameClientBean> {

	private final Iterable<GameClientBean> clients;
	
	public GameClientEncoder(Iterable<GameClientBean> clients) {
		this.clients = clients;
	}
	
	@Override
	public String toClient(GameClientBean value) {
		return value.getClientId().toString();
	}

	@Override
	public GameClientBean toValue(String clientValue) {	
		Integer clientId = Integer.valueOf(clientValue);
		for(GameClientBean client:clients) {
			if(client.getClientId().equals(clientId)) {
				return client;
			}
		}
		
		throw new IllegalArgumentException("Invalid client");
	}

}
