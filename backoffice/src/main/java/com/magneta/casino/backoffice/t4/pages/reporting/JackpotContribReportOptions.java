package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class JackpotContribReportOptions extends DateTimeReportOptions {
	
	@Component(id = "username",
			bindings = {"value=username", "disabled=false","displayName=message:Username"})
	public abstract TextField getUsernameField();

	@Component(id = "usernameLabel",
			bindings = {"field=component:username"})
	public abstract FieldLabel getUsernameLabel();

	@Component(id = "games",
			bindings = {"value=game","model=ognl:gamesSelectionModel", 
			"displayName=message:Game"})
	public abstract PropertySelection getGameSelection();

	public GamesSelectionModel getGamesSelectionModel(){
		return new GamesSelectionModel(true);
	}
	
	@Component(id = "gamesLabel", 
			bindings = {"field=component:games"})
	public abstract FieldLabel getGameLabel();
	
	@Component(id = "showClosedCB",
			bindings={"value=ShowClosed", "displayName=message:ShowClosed"})
	public abstract Checkbox getClosedCB();
	
	@Component(id = "showClosedCBLabel",
			bindings={"field=component:showClosedCB"})
	public abstract FieldLabel getClosedCBLabel();
	
	public abstract boolean getShowClosed();
	public abstract String getUsername();	
	public abstract String getCountry();
	public abstract int getGame();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		
		reportParams.put("username", getUsername());
		reportParams.put("country_val", getCountry());
		
		if (getGame() != -1){
			reportParams.put("game_id", getGame());
		}
		reportParams.put("show_closed", getShowClosed());
	}
}
