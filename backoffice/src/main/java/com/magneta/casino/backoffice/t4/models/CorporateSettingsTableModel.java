package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.administration.beans.CorporateSettingsBean;

public class CorporateSettingsTableModel implements IBasicTableModel {

	private Long userId;
	
	public CorporateSettingsTableModel(Long userId){
		this.userId = userId;
	}
	
    @Override
	public Iterator<CorporateSettingsBean> getCurrentPageRows(int arg0, int arg1, ITableColumn arg2, boolean arg3) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<CorporateSettingsBean> it = null;
        if (dbConn == null){
			return null;
		}
        
        try
        {
        	String sql =
            	"SELECT game_types.game_type_id, game_types.game_type_variant, corporate_game_settings.min_bet, corporate_game_settings.max_bet"+
            	" FROM corporate_game_settings"+
            	" INNER JOIN game_types ON corporate_game_settings.game_id = game_types.game_type_id"+
            	" WHERE corporate_game_settings.corporate_id = ?";
            	
        
            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, userId);
            
            res = statement.executeQuery();

            ArrayList<CorporateSettingsBean> myArr = new ArrayList<CorporateSettingsBean>();
            CorporateSettingsBean currRow;

            while (res.next()){
                currRow = new CorporateSettingsBean();
                currRow.setGameID(res.getInt("game_type_id"));
                currRow.setGameName(res.getString("game_type_variant"));
                if (res.getObject("min_bet") != null){
                	currRow.setMinBet(res.getDouble("min_bet"));
                } else {
                	currRow.setMinBet(null);
                }
                if (res.getObject("max_bet") != null){
                	currRow.setMaxBet(res.getDouble("max_bet"));
                } else {
                	currRow.setMaxBet(null);
                }
                myArr.add(currRow);
            }

            it = myArr.iterator();
                
        } catch (SQLException e) {
        	throw new RuntimeException("Error while retrieving player categories list.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {
            String sql = 
            		"SELECT count(*)" +
            		" FROM corporate_game_settings" +
            		" WHERE corporate_id = ?";

            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, userId);
            
            res = statement.executeQuery();

            if (res.next()){
                count = res.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving player categories count.", e);
        } finally {
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }
}
