package com.magneta.casino.backoffice.managers.impl;

import org.apache.tapestry5.ioc.ObjectLocator;

import com.magneta.casino.backoffice.managers.ObjectLocatorLocator;

public class ObjectLocatorLocatorImpl implements ObjectLocatorLocator {

	private final ObjectLocator objectLocator;
	
	public ObjectLocatorLocatorImpl(ObjectLocator objectLocator) {
		this.objectLocator = objectLocator;
	}
	
	@Override
	public ObjectLocator getObjectLocator() {
		return this.objectLocator;
	}
}
