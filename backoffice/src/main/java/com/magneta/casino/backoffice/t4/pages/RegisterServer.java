package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class RegisterServer extends SecurePage {

	@InjectObject("service:casino.common.Servers")
	public abstract ServersService getServersService();
	
	@Override
	public boolean pageNeedsWriteAccess() {
		return true;
	}
	
	public abstract int getServerID();
	public abstract String getServerDesc();
	public abstract String getLocalAddress();
	public abstract int getLocalPort();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();

    @Bean(initializer="omitZero=true")
    public abstract SimpleNumberTranslator getNumTranslator();
	
	@Component(id="registerServerForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getRegisterServerForm();
	
	@Component(id="serverID",
			bindings={"value=serverID","displayName=message:ServerID","translator=bean:numTranslator","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getServerIDField();
	
	@Component(id="serverIDLabel",
			bindings={"field=component:serverID"})
	public abstract FieldLabel getServerIDLabel();
	
	@Component(id="serverDesc",
			bindings={"value=serverDesc","displayName=message:ServerDesc","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getServerDescField();
	
	@Component(id="serverDescLabel",
			bindings={"field=component:serverDesc"})
	public abstract FieldLabel getServerDescLabel();
	
	@Component(id="localAddress",
			bindings={"value=localAddress","displayName=message:LocalAddress","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getLocalAddressField();
	
	@Component(id="localAddressLabel",
			bindings={"field=component:localAddress"})
	public abstract FieldLabel getLocalAddressLabel();
	
	@Component(id="localPort",
			bindings={"value=localPort","displayName=message:LocalPort","translator=bean:numTranslator","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getLocalPortField();
	
	@Component(id="localPortLabel",
			bindings={"field=component:localPort"})
	public abstract FieldLabel getLocalPortLabel();
	
	@Component(id="okSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}
		
		ServerBean server = new ServerBean();
		
		server.setServerId(getServerID());
		server.setServerDescription(getServerDesc());
		server.setServerLocalAddress(getLocalAddress());
		server.setServerLocalPort(getLocalPort());
		server.setServerRunning(false);
		
		
		try {
			getServersService().addServer(server);
		} catch (ServiceException e) {
			delegate.record(null, e.getMessage());
			return;
		}
		
		cycle.forgetPage(this.getPageName());
		redirectToPage("SystemStatus");
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.forgetPage(this.getPageName());
		redirectToPage("SystemStatus");
	}
}
