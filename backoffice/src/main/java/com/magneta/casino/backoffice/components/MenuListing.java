package com.magneta.casino.backoffice.components;

import java.util.List;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.beans.MenuBean;
import com.magneta.casino.backoffice.beans.MenuBeanInterface;
import com.magneta.casino.backoffice.managers.MenuService;

public class MenuListing {
	
	@Inject
	private MenuService menuService;
	
	@Inject
	private ComponentResources resources;
	
	@Property
	private MenuBean menuBean;
	
	private List<? extends MenuBeanInterface> getMenuItems(MenuBeanInterface item, String pageName) {
		if (item.isSeperator()) {
			return null;
		}
		
		if (item.getPageLink() == null) {
			return null;
		}
		
		if (pageName.equals(item.getPageLink())) {
			return item.getSubMenu();
		}
		
		if (item.getSubMenu() != null) {
			for (MenuBeanInterface subItem: item.getSubMenu()) {
				List<? extends MenuBeanInterface> items = getMenuItems(subItem, pageName);
				
				if (items != null) {
					return items;
				}
			}
		}
		
		return null;
	}
	
	@Cached
	public List<? extends MenuBeanInterface> getMenuItems() {
		String pageName = resources.getPageName();
		
		for (MenuBean item: menuService.getVerticalMenu()) {
			List<? extends MenuBeanInterface> items = getMenuItems(item, pageName);
			
			if (items != null) {
				return items;
			}
			
		}
		return null;
	}
	
	public boolean showMenuLink() {
		return this.menuBean != null && this.menuBean.getPageLink() != null;
	}
}
