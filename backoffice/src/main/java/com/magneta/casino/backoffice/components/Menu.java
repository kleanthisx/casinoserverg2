package com.magneta.casino.backoffice.components;

import java.util.List;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.CleanupRender;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import com.magneta.casino.backoffice.beans.MenuBeanInterface;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;


@Import(library="PageMenu.js")
public class Menu<T extends MenuBeanInterface> {

	/**
	 * Defines the source Tree to walk over.
	 */
	@Parameter(required = true)
	@Property
	private List<T> source;
	
	@Parameter(required = false, defaultPrefix=BindingConstants.LITERAL)
	private String orientation;

	/**
	 * Defines the current node of the tree
	 */
	@Parameter
	@Property
	private T value;
	
	@Inject
	private JavaScriptSupport javaScriptSupport;

	@Inject
	private PageMenuItemAccessValidator accessValidator;

	@Inject 
	private RequestGlobals requestGlobals;
	
	private boolean isHorizontal() {
		return this.orientation != null && this.orientation.equalsIgnoreCase("horizontal");
	}
	
	public String getCssClass() {
		if (isHorizontal()) {
			return "hr_menu";
		} 
		
		return "vr_menu";
	}
	
	public String getId() {
		if (isHorizontal()) {
			return "hrPageMenu";
		}

		return "vrPageMenu";
	}
	
	public List<? extends MenuBeanInterface> getSubMenuSource() {
		return value.getSubMenu();
	}
	
	public String getItemId() {
		String classId = value.getDescription();
		classId = classId.replace(" ", "_").toLowerCase();
		
		return getId() +"_mni_" + classId;
	}
	
	public String getItemClass() {
		boolean isCurrent = 
				value != null 
				&& value.getPageLink() != null 
				&& value.getPageLink().contains(requestGlobals.getActivePageName());
		
		return isCurrent?"activeLink": "menuItem";
	}
	
	public String getSubMenuClass() {
		return getCssClass() + "_sub1";
	}
	
	public boolean isItemHasMenu() {
		return value.getSubMenu() != null && !value.getSubMenu().isEmpty();
	}

	@CleanupRender
	void cleanupRender(MarkupWriter writer) {
		
		if (!this.isHorizontal()) {
			javaScriptSupport.addScript("mbSet('%s', '%s');",
				getId(), "mbv");
		}
	}
	
	private boolean hasAccess(T value) {
		String pageLink = value.getPageLink();
		
		if (pageLink == null) {
			return true;
		}
		
		if (pageLink.startsWith("special:")) {
			return accessValidator.checkSpecial(pageLink.substring("special:".length()));
		}
		
		return accessValidator.checkAccess(pageLink);
	}
	
	public boolean hasAccess() {
		return value != null && hasAccess(value);
	}
	
	public boolean isSeparator() {
		return value != null && value.isSeperator();
	}
}
