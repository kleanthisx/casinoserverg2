package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.UserGameBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class LatestUserGamesTableModel implements IBasicTableModel {
	
	private Long userId;
	
	public LatestUserGamesTableModel(Long userId){
		if(userId == null){
			this.userId = 0L;
		}else{
			this.userId = userId;
		}
	}
	
	@Override
	public Iterator<UserGameBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<UserGameBean> it = null;

		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
		
		try
		{   
			String sql = "SELECT SUM(rounds) AS rounds, SUM(bets) AS bets, SUM(wins) AS wins, SUM(real_bets) AS real_bets, SUM(real_wins) AS real_wins," +
					" SUM(jackpot_wins) AS jackpot_wins, game_types.game_type_variant" +
					" FROM users" +
					" LEFT OUTER JOIN game_period_amounts ON users.user_id = game_period_amounts.user_id" +
					" LEFT JOIN game_tables ON game_period_amounts.table_id = game_tables.table_id" +
					" LEFT JOIN game_types ON game_tables.game_type_id = game_types.game_type_id" +
					" WHERE age(period_date) <= '1 mon'" +
					" AND users.user_id=?" +
					" GROUP BY game_types.game_type_variant" +
					" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			
			statement.setLong(1, userId);
			statement.setInt(2, limit);
			statement.setInt(3, offset);

			res = statement.executeQuery();

			ArrayList<UserGameBean> myArr = new ArrayList<UserGameBean>();
			UserGameBean currRow;

			while (res.next()){
				currRow = new UserGameBean();
				currRow.setBets(res.getDouble("bets"));
				currRow.setRounds(res.getInt("rounds"));
				currRow.setWins(res.getDouble("wins"));
				currRow.setJackpotWins(res.getDouble("jackpot_wins"));
				currRow.setGame(res.getString("game_type_variant"));
				currRow.setRealBets(res.getDouble("real_bets"));
				currRow.setRealWins(res.getDouble("real_wins"));
				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving latest completed transactions.",e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		String sql = "SELECT COUNT(DISTINCT game_tables.game_type_id) AS game_types" +
		" FROM game_period_amounts" +
		" INNER JOIN game_tables ON game_period_amounts.table_id = game_tables.table_id" +
		" WHERE game_period_amounts.user_id=?" +
		" AND age(period_date) <= '1 mon'";

		Connection dbConn = ConnectionFactory.getReportsConnection();
		if (dbConn == null) {
			throw new RuntimeException("Out of database exceptions");
		}

		PreparedStatement statement = null;
		ResultSet res = null;

		try {
			statement = dbConn.prepareStatement(sql);

			statement.setLong(1, userId);

			res = statement.executeQuery();
			if (res.next()) {
				return (int)res.getLong(1);
			}
		}catch (SQLException e) {
			throw new RuntimeException("Error while retrieving count for last 30 days gaming history.",e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
		
		return 0;
	}
}
