package com.magneta.casino.backoffice.t4.pages;

import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.tapestry.IExternalPage;
import org.apache.tapestry.IPage;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.html.BasePage;

import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.common.utils.DateUtils;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CurrencyParser;
import com.magneta.casino.services.CurrencyService;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.enums.UserTypeEnum;

public abstract class PublicPage extends BasePage implements IExternalPage {

	@InjectObject("service:casino.common.UserAuthentication")
	public abstract UserAuthenticationService getUserAuthenticationService();
	
	@InjectObject("service:casino.common.Principal")
	public abstract PrincipalService getPrincipalService();
	
	@InjectObject("service:casino.common.Currency")
	public abstract CurrencyService getCurrencyService();

	@InjectObject("service:casino.common.CurrencyParser")
	public abstract CurrencyParser getCurrencyParser();
	
	public ExtendedPrincipalBean getPrincipal() {
		return getPrincipalService().getPrincipal();
	}

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
	}
    
    public String formatCurrency(Double amount) {
    	return getCurrencyParser().formatCurrency(amount, getLocale());
    }

	public Date normalizeDate(Date d) {
		return DateUtils.getNormalizedDate(getTZ(), d);
	}

	public Date denormalizeDate(Date d) {
		return DateUtils.getDenormalizedDate(getTZ(), d);
	}

	public String formatDate(Date d){
		return FormatUtils.getFormattedDate(d, getTZ(), getLocale());
	}

	/**
	 * Gets the timezone effective for this request.
	 * @return
	 */
	public TimeZone getTZ() {		
		BackofficePrincipalBean principal = (BackofficePrincipalBean)this.getPrincipal();
		
		if (principal == null || principal.getTimeZone() == null) {
			return TimeZone.getDefault();
		}
		
		return principal.getTimeZone();
	}
    
    public String getPageTitle() {
    	return getMsg("page-title");
    }
    
    public String formatAmount(Double amount) {
    	if (amount == null)
    		return "";
    	
    	return getCurrencyParser().formatDouble(amount, getLocale());
    }
    
    public String formatPercentage(Double amount) {
    	if (amount == null) {
    		return "";
    	}
    	
    	NumberFormat nf = NumberFormat.getNumberInstance(getLocale());

        nf.setRoundingMode(RoundingMode.DOWN);
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(0);

		return nf.format(amount * 100.0d) + "%";
	}
    
    public String getMsg(String msg) {
    	if (msg == null) {
    		return null;
    	}
    	
        return this.getMessages().getMessage(msg);
    }
    
    public String getLanguage()
    {
        try {
            return this.getLocale().getLanguage();
        } catch (Exception ex) {
            return "en";
        }
    }
    
    public void onLogout(){
    	logout();
    }
    
    public void logout() {
    	getUserAuthenticationService().logoutUser();
    	
    	IPage page = this.getRequestCycle().getPage();
    	
    	if (page != null) {
    		this.getRequestCycle().forgetPage(page.getPageName());
    	}
        redirectToPage("Login");
    }

	public void redirectToPage(String page) {
		String url = LinkGenerator.getPageUrl(page);
		throw new RedirectException(url);
	}
	
	public RedirectException redirectException(String page) {
		String url = LinkGenerator.getPageUrl(page);
		return new RedirectException(url);
	}
	
	public RedirectException redirectException(String page, Object... params) {
		String url = LinkGenerator.getPageUrl(page, params);
		return new RedirectException(url);
	}
	
	public String getFormatMessage(String msgName, Object ... params) {
		return new MessageFormat(getMsg(msgName)).format(params);
	}
	
	public boolean isPlayer(UserTypeEnum userType){
		return (userType.equals(UserTypeEnum.NORMAL_USER) 
				|| userType.equals(UserTypeEnum.DEMO_USER) 
				|| userType.equals(UserTypeEnum.CORPORATE_PLAYER));
	}
}
