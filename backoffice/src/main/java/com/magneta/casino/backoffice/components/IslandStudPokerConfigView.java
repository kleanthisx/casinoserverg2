package com.magneta.casino.backoffice.components;

import java.util.List;

import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;
import com.magneta.games.configuration.poker.islandstudpoker.WinCombination;
import com.magneta.games.configuration.poker.islandstudpoker.WinType;

public class IslandStudPokerConfigView {	
	@Inject
	private Messages messages;
		
	@Parameter(required=true)
	private Integer gameId;

	@Parameter(required=true)
	private IslandStudPokerConfiguration configuration;

	private String getComboDescription(int combo) {
		return messages.get("combo-" + String.valueOf(combo));
	}
	
	@Property
	private GameComboBean combo;
	
	public GameComboBean[] getCombos() {
		List<WinCombination> winCombos = configuration.getWinCombinations();
		
		GameComboBean[] winCs = new GameComboBean[winCombos.size()];
		
		int i=0;
		for (WinCombination c: winCombos) {
			winCs[i] = new GameComboBean(null, getComboDescription(c.getTriggerCombo()), c.getAmount(), true);
					
			i++;
		}

		return winCs;
	}
	
	@Property
	private GameComboBean jackpotCombo;
	
	public GameComboBean[] getJackpotCombos() {
		List<WinCombination> jackpotWinCombos = configuration.getJackpotWinCombinations();
		
		GameComboBean[] jWinCs = new GameComboBean[jackpotWinCombos.size()];
		
		int i=0;
		for (WinCombination c: jackpotWinCombos) {
			
			String description = getComboDescription(c.getTriggerCombo());
			double amount = c.getAmount();
			
			if (c.getType() == WinType.JACKPOT_POOL_RATIO) {
				description += (" (%)");
				
				amount *= 100.0;
			}
			
			jWinCs[i] = new GameComboBean(null, description, amount, true);	
			i++;
		}

		return jWinCs;
	}
}
