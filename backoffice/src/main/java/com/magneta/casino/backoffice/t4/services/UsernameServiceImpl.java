package com.magneta.casino.backoffice.t4.services;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;

public class UsernameServiceImpl implements UsernameService {

	private UsernameService usernameService;
	
	public void setUsernameService(UsernameService usernameService) {
		this.usernameService = usernameService;
	}
	
	@Override
	public String getUsername(Long userId) throws ServiceException {
		return usernameService.getUsername(userId);
	}

}
