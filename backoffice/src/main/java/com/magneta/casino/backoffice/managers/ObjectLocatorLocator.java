package com.magneta.casino.backoffice.managers;

import org.apache.tapestry5.ioc.ObjectLocator;

public interface ObjectLocatorLocator {

	ObjectLocator getObjectLocator();
}
