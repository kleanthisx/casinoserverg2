package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.AffiliatesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(and={PrivilegeEnum.REPORTS,PrivilegeEnum.CORPORATE})
public abstract class CorporateAccountPeriodReport extends DateReportOptions {

		@Component(bindings = {"value=corporate","model=ognl:affiliatesSelectionModel", "displayName=message:Corporate"})
		public abstract PropertySelection getCorporateSelection();
		
		public AffiliatesSelectionModel getAffiliatesSelectionModel(){
			return new AffiliatesSelectionModel(0L,false,false);
		}
		
		@Component(bindings = {"field=component:corporateSelection"})
		public abstract FieldLabel getCorporateLabel();
		
		@Component(bindings={"value=showSubs"})
		public abstract Checkbox getSubCorporates();
		
		public abstract Long getCorporate();
		
		@InitialValue("false")
		public abstract boolean getShowSubs();
		
		@Override
		public void fillReportParameters(Map<String,Object> reportParams) {
			super.fillReportParameters(reportParams);
			reportParams.put("corp_id",getCorporate());
			reportParams.put("view_subs",getShowSubs());
		}
}
