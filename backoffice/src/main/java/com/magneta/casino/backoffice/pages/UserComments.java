package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.UserCommentsGridDataSource;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserCommentService;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.beans.UserCommentBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.COMMENTS)
public class UserComments {

	private static final Logger log = LoggerFactory.getLogger(UserComments.class);
	
	@Inject
	private UserCommentService userCommentService;
	
	@Inject
	private Messages messages;
	
	@Inject
	private UsernameService usernameService;
	
	@Inject
	private AlertManager alertManager;
	
	@Inject
	private PrincipalService principalService;
	
	@Property
	private Long userId;
	
	@Property
	private String newComment;
	
	@Property
	private UserCommentBean comment;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long userId) {
		this.userId = userId;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Long passivate() {
		return userId;
	}
	
	public UserCommentsGridDataSource getUserCommentsGridDataSource() {
		return new UserCommentsGridDataSource(userCommentService, userId, null);
	}
	
	@OnEvent(value=EventConstants.ACTION, component="deleteComment")
	public void deleteComment(Long commentId) {
		try {
			userCommentService.deleteComment(userId, commentId);
			alertManager.success("Comment deleted");
		} catch (ServiceException e) {
			alertManager.error("Comment not deleted: " + e.getMessage());
			log.error("Unable to delete comment", e);
		}
	}

	@OnEvent(value=EventConstants.SUCCESS, component="create")
	public void createComment() {
		
		UserCommentBean comment = new UserCommentBean();
		comment.setUserId(userId);
		comment.setComment(newComment);
		
		try {
			userCommentService.createComment(comment);
			alertManager.success("Comment added");
		} catch (ServiceException e) {
			alertManager.error("Comment not added: " + e.getMessage());
			log.error("Unable to add comment", e);
		}
	}
	
	
	public String getConfirmMessage() {
		return messages.format("confirm-message", comment.getMessageId());
	}
	
	public String getPageTitle() throws ServiceException {
		return messages.format("page-title", usernameService.getUsername(userId));
	}
	
	public String getExtraColumns() {
		return principalService.getPrincipal().hasPrivilege(PrivilegeEnum.COMMENTS, true)? "actions":"";
	}
}
