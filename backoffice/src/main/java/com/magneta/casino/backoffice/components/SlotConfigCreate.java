package com.magneta.casino.backoffice.components;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Events;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.slf4j.Logger;

import com.magneta.administration.beans.ReelBean;
import com.magneta.administration.beans.ReelSetBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.pages.GameImage;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.slots.SlotGameActionFilter;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.games.configuration.GameConfigLoadingException;
import com.magneta.games.configuration.GameConfigurationFactory;
import com.magneta.games.configuration.slots.SlotConfiguration;

@Events({EventConstants.PREPARE_FOR_RENDER, EventConstants.VALIDATE})
public class SlotConfigCreate {

	@Inject
	private Logger log;
	
	@Inject
	private GamesService gameService;
	
	@Inject
	private PageRenderLinkSource pageRenderLinkSource;
	
	@Environmental(false)
    private FormSupport formSupport;
	
    @Inject
    private ComponentResources resources;
	
	@Parameter(required=true)
	private Integer gameId;
	
	@Parameter(required=true, autoconnect=true)
	private String value;
	
	@Property
	private ComboSymbolBean symbol;
	
	public Link getSymbolImageUrl() {
		return this.pageRenderLinkSource.createPageRenderLinkWithContext(GameImage.class, new Object[] {gameId, symbol.getImage()});		
	}
	
	@Property
	private GameComboBean combo;
	
	@Property
	private GameComboBean[] combos;
	
	@Property
	private ReelBean reel;
	
	public void setReelValue(String value) {
		reel.setReel(value);
	}
	
	public String getReelValue() {
		return reel.getReel();
	}
	
	@Property
	private ReelSetBean reelSet;
	
	@Property
	private Integer reelSetIndex;
	
	@Property
	private ReelSetBean[] reelSets;
	
	@Property
	private GameTemplateSetting setting;
	
	@Property
	private List<GameTemplateSetting> settings;
	
	public boolean isSettingBoolean() {
		return setting.getType() == GameTemplateSetting.BOOLEAN;
	}
	
	public void setBooleanSetting(Boolean value) {
		setting.setDefaultValue(value);
	}
	
	public Boolean getBooleanSetting() {
		return (Boolean)setting.getDefaultValue();
	}
	
	public boolean isSettingInt() {
		return setting.getType() == GameTemplateSetting.INTEGER;
	}
	
	public void setIntSetting(Integer value) {
		setting.setDefaultValue(value);
	}
	
	public Integer getIntSetting() {
		return (Integer)setting.getDefaultValue();
	}
	
	public boolean isSettingDouble() {
		return setting.getType() == GameTemplateSetting.DOUBLE;
	}
	
	public void setDoubleSetting(Double value) {
		setting.setDefaultValue(value);
	}
	
	public Double getDoubleSetting() {
		return (Double)setting.getDefaultValue();
	}
	
	public boolean isSettingPercentage() {
		return setting.getType() == GameTemplateSetting.PERCENTAGE;
	}
	
	public void setPercentageSetting(Double value) {
		if (value == null) {
			setting.setDefaultValue(null);
		} else {
			setting.setDefaultValue(value / 100.0);
		}
	}
	
	public void setStringSetting(String value) {
		setting.setDefaultValue(value);
	}
	
	public String getStringSetting() {
		return (String)setting.getDefaultValue();
	}
 	
	public Double getPercentageSetting() {
		Double value = (Double)setting.getDefaultValue();
		if (value == null) {
			return null;
		}
		
		return value * 100.0;
	}
	
	private SlotConfiguration config;
	
	@SetupRender
	public void onSetupRender() throws ServiceException {
		 if (formSupport == null)
			 throw new RuntimeException(String.format("Component %s must be enclosed by a Form component.",
	                    resources.getCompleteId()));
		
		GameBean game = gameService.getGame(gameId);

		try {
			config = (SlotConfiguration)GameConfigurationFactory.loadConfiguration(game.getGameTemplate(), -1, value);
		} catch (GameConfigLoadingException e) {
			throw new ServiceException("Invalid configuration", e);
		}
		
		ComboSymbolBean[] symbols;
		{
			SlotGameActionFilter f = (SlotGameActionFilter)ServiceLocator.getService(GameActionFilterService.class).getFilter(gameId, config);
				combos = f.getPaytable();
				symbols = f.getSymbols();
		}

		reelSets = new ReelSetBean[config.getNumberOfReelSets()];
		
		for (int i=0; i < reelSets.length; i++) {
			int[][] reelsNum = config.getReels(i);
			int numOfReels = config.getNumberOfReels(i);
			reelSets[i] = new ReelSetBean();
			ReelBean[] reels = new ReelBean[numOfReels];
			for(int z=0;z<numOfReels;z++) {
				int[] reel = reelsNum[z];
				StringBuilder builder = new StringBuilder();
				for(int k=0;k<reel.length;k++) {
					if (k > 0) {
						builder.append(",");
					}
					builder.append(reel[k]);
				}
				reels[z] = new ReelBean(builder.toString(),symbols);

			}
			reelSets[i].setReels(reels);
		}
		
		GameTemplateSetting[] defaultSettings = config.getDefaultSettings();
		settings = new ArrayList<GameTemplateSetting>();
		
		if (defaultSettings != null) {
			for (GameTemplateSetting s: defaultSettings) {
				GameTemplateSetting newS = new GameTemplateSetting(s);
				newS.setDefaultValue(config.getSetting(s.getKey()));
				settings.add(newS);
			}
		}
	}
	
	private static boolean validateReelsString(String reelsString, int noOfReels, int minSymbol, int maxSymbol){
		String regex = "[[0-9]{1}[,[0-9]{1}]*\\|{1}]+";
		if (reelsString != null){
			reelsString =  reelsString.replaceAll("\\s", "");
			if (reelsString.matches(regex)){
				String[] tmp = reelsString.split("\\|{1}");
				if (tmp.length < noOfReels){
					return false;
				}
				for (int i=0;i<tmp.length;i++){
					String[] nums = tmp[i].split(",");
					for (int j=0;j<nums.length;j++){
						int sym = Integer.parseInt(nums[j]);
						if (sym < minSymbol || sym > maxSymbol){
							return false;
						}
					}
				}
				return true;
			}
		}
		return false;
	}
	
	@OnEvent(value=EventConstants.VALIDATE)
	public void onValidate() {
		log.info("Validate");
		
		StringBuilder finalConfig = new StringBuilder();
		for (int i = 0; i < reelSets.length;i++) {
			StringBuilder reelBuilder = new StringBuilder();
			for(ReelBean reel: reelSets[i].getReels()) {
				reelBuilder.append(reel.getReel().replaceAll("\\s", ""));
				if (!reel.getReel().endsWith("|")){
					reelBuilder.append("|\n");
				}
			}
			String reelString = reelBuilder.toString();
			if (!validateReelsString(reelString, 
					config.getNumberOfReels(i),
					config.getMinSymbol(),
					config.getSymbolCount())) {
				throw new RuntimeException("Invalid symbol");
			}
			finalConfig.append(reelString).append(";\n");
		}

		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		
		DecimalFormat f = new DecimalFormat("0.00");
		f.setGroupingUsed(false);
		f.setDecimalFormatSymbols(symbols);

		for(int i=0; i < combos.length; i++) {
			if(i > 0) {
				finalConfig.append(':');
			}
			finalConfig.append(i).append('=').append(f.format(combos[i].getPayout()));
		}
		
		/* Save settings */
		if (settings != null && !settings.isEmpty()) {
			
			for (GameTemplateSetting s: settings) {
				finalConfig.append(';');
				finalConfig.append(s.getKey());
				finalConfig.append('=');
				finalConfig.append(s.getDefaultValue());
			}
		}
		
		
		this.value = finalConfig.toString();
	}
}