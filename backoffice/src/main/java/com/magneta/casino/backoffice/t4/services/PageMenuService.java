package com.magneta.casino.backoffice.t4.services;

import java.util.List;
import java.util.Locale;

import com.magneta.tapestry4.components.pagemenu.PageMenuItem;

public interface PageMenuService {

	List<PageMenuItem> getVerticalMenuItems(Locale locale);
	List<PageMenuItem> getHorizontalMenuItems(Locale locale);
}
