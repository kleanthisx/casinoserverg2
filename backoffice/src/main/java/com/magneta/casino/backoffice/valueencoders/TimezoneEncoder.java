package com.magneta.casino.backoffice.valueencoders;

import java.util.TimeZone;

import org.apache.tapestry5.ValueEncoder;

public class TimezoneEncoder implements ValueEncoder<TimeZone> {
	
	public TimezoneEncoder(){
	}
	
	@Override
	public String toClient(TimeZone value) {
		return value.getID();
	}

	@Override
	public TimeZone toValue(String clientValue) {
		return TimeZone.getTimeZone(clientValue);
	}
}
