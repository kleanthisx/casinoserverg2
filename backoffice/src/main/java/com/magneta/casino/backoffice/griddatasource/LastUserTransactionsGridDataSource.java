package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.administration.beans.UserTransactionBean;
import com.magneta.casino.backoffice.mock.services.LastUserTransactionsMockUpService;
import com.magneta.casino.services.search.SortContext;

public class LastUserTransactionsGridDataSource extends GenericGridDataSource<UserTransactionBean>{

	private final LastUserTransactionsMockUpService lastUserTransactionsMockUpService;
	private final int limit;
	private final Long userId;
	
	public LastUserTransactionsGridDataSource(LastUserTransactionsMockUpService lastUserTransactionsMockUpService,int limit,Long userId){
		this.lastUserTransactionsMockUpService = lastUserTransactionsMockUpService;
		this.limit = limit;
		this.userId = userId;
	}
	
	@Override
	public int getAvailableRows() {
		return lastUserTransactionsMockUpService.getLatestTransactionCount(limit,userId);
	}

	@Override
	public List<UserTransactionBean> getData(int limit, int offset,
			SortContext<UserTransactionBean> sortConstraints) throws Exception {
		
		return lastUserTransactionsMockUpService.getLastestTransactions(limit,userId);
	}
}
