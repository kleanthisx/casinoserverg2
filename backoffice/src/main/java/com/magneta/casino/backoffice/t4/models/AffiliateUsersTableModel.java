package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.UsersSearchResultBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class AffiliateUsersTableModel implements IBasicTableModel {
    
    private Long affiliateId;
    
    public AffiliateUsersTableModel(Long affiliateId){
    	if(affiliateId == null){
    		this.affiliateId = 0L;
    	}else{
    		this.affiliateId = affiliateId;
    	}
    }
    
    @Override
	public Iterator<UsersSearchResultBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<UsersSearchResultBean> it = null;
        
        if (dbConn == null){
			return null;
		}
        
        try
        {   
            
            String sql = 
                "SELECT users.user_id, users.username, users.first_name, users.last_name, is_active"+
				" FROM affiliate_users"+
				" INNER JOIN users ON affiliate_users.user_id = users.user_id"+
				" WHERE affiliate_id = ?"+
				" AND affiliation = 1"+
            	" ORDER BY users.username" +
            	" LIMIT ? OFFSET ?";
            
                statement = dbConn.prepareStatement(sql);
                statement.setLong(1, affiliateId);
                statement.setInt(2, limit);
                statement.setInt(3, offset);
                
                res = statement.executeQuery();
                
                ArrayList<UsersSearchResultBean> myArr = new ArrayList<UsersSearchResultBean>();
                UsersSearchResultBean currRow;
                
                while (res.next()){
                    currRow = new UsersSearchResultBean();
                    
                    currRow.setId(res.getLong("user_id"));
                    currRow.setUsername(res.getString("username"));
                    currRow.setFirstName(res.getString("first_name"));
                    currRow.setLastName(res.getString("last_name"));
                    currRow.setActive(res.getBoolean("is_active"));
                    
                    myArr.add(currRow);
                }
               
                it = myArr.iterator();
                
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving affiliates list.",e);
        } finally {
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
        int count = 0;
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {            
            String sql = 
                "SELECT COUNT(*)"+
				" FROM affiliate_users"+
				" WHERE affiliate_id = ?"+
				" AND affiliation = 1";
            
                statement = dbConn.prepareStatement(sql);
                statement.setLong(1, affiliateId);
                
                res = statement.executeQuery();
                
                if (res.next()){
                    count = res.getInt(1);
                }
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving affiliates count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }
    
}
