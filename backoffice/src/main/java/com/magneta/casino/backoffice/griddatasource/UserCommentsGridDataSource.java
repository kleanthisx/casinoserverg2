package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserCommentService;
import com.magneta.casino.services.beans.UserCommentBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class UserCommentsGridDataSource extends GenericGridDataSource<UserCommentBean> {

	private final UserCommentService userCommentService;
	private final Long userId;
	private final SearchContext<UserCommentBean> searchContext;
	
	public UserCommentsGridDataSource(UserCommentService userCommentService, Long userId, SearchContext<UserCommentBean> searchContext) {
		this.userCommentService = userCommentService;
		this.userId = userId;
		this.searchContext = searchContext;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return userCommentService.getCommentsSize(userId, searchContext);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<UserCommentBean> getData(int limit, int offset,
			SortContext<UserCommentBean> sortContext) throws Exception {
		return userCommentService.getComments(userId, searchContext, limit, offset, sortContext).getResult();
	}
}
