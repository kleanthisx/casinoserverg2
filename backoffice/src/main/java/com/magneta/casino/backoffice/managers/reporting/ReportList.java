package com.magneta.casino.backoffice.managers.reporting;

import java.util.List;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.casino.services.ServiceException;

public interface ReportList {
	List<ReportBean> getReports(String category) throws ServiceException;
	List<String> getCategories();
	
	ReportBean getReport(String reportName) throws ServiceException;
}
