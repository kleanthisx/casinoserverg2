/**
 * 
 */
package com.magneta.casino.backoffice.services;

import org.apache.tapestry5.ioc.ObjectLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author anarxia
 *
 */
public class T5ServiceLocator {
	
	private static final Logger log = LoggerFactory.getLogger(T5ServiceLocator.class);

    private static ObjectLocator registry;
    
    public static void setObjectLocator(ObjectLocator registry) {
    	T5ServiceLocator.registry = registry;
    }
    
    public static <T> T getService(Class<T> arg0) {
        if (registry == null) {
            log.error("Registry is null!!!!");
        }
        
        return registry.getService(arg0);
    }
    
    public static <T> T getService(Class<T> serviceInterface, String serviceId) {
        if (registry == null) {
            log.error("Registry is null!!!!");
        }
        
        return registry.getService(serviceId, serviceInterface);
    }
}
