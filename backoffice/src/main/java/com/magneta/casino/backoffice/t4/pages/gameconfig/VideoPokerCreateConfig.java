package com.magneta.casino.backoffice.t4.pages.gameconfig;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.poker.VideoPokerActionFilter;
import com.magneta.casino.common.games.filters.poker.VideoPokerPlusActionFilter;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.poker.PokerConfiguration;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public abstract class VideoPokerCreateConfig extends AbstractCreateGameConfigPage implements PageBeginRenderListener {
	
	private static final Logger log = LoggerFactory.getLogger(VideoPokerCreateConfig.class);
	
	@Persist("client:page")
	public abstract GameComboBean[] getCombos();
	public abstract void setCombos(GameComboBean[] combos);

	public abstract GameComboBean getCurrCombo();
	public abstract ComboSymbolBean getCurrSymbol();
	
	@Bean(initializer="min=0.0")
	public abstract Min getMinAmount();
	
	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Component(id="gameName",
			bindings={"value=gameName"})
			public abstract Insert getGameNameField();
	
	@Component(id="createConfigForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
			public abstract Form getCreateConfigForm();
	
	@Component(id="comment",
			bindings={"value=comment","displayName=message:Comment","validators=validators:required,maxLength=255","disabled=!allowWrite()"})
			public abstract TextField getCommentField();
	
	@Component(id="commentLabel",
			bindings={"field=component:comment"})
			public abstract FieldLabel getCommentLabel();
	
	@Component(id="combos", type="For",
			bindings={"source=combos","value=currCombo","renderTag=false"})
			public abstract ForBean getCombosFor();

	@Component(id="comboPayout",
			bindings={"value=currCombo.payout","validators=validators:required,$minAmount","translator=bean:numTranslator","disabled=!allowWrite()"})
			public abstract TextField getComboPayout();
	
	@Component(id="comboSymbols", type="For",
			bindings={"source=currCombo.symbols","value=currSymbol","renderTag=false"})
			public abstract ForBean getComboSymbols();

	@Component(id="saveSubmit",
			bindings={"disabled=!allowWrite()"})
			public abstract Submit getSaveSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Override
	public void pageValidate(PageEvent event) {
		super.pageValidate(event);
		if (!getAccessValidator().checkWriteAccess(this.getPageName())){
			redirectToPage("AccessDenied");
		}
	}

	@Override
	public void pageBeginRender(PageEvent evt){		
		super.pageBeginRender(evt);	
		
		PokerConfiguration config;
		try {
			config = (PokerConfiguration)getGameConfigsService().getFilteredConfiguration(getGame(), getConfigID());
		} catch (ServiceException e) {
			log.error("Unable to load game config", e);
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("invalid-symbol-found"), null);
			return;
		}
		
		ActionFilter f;
		try {
			f = ServiceLocator.getService(GameActionFilterService.class).getFilter(this.getGame(), config);
		} catch (ServiceException e) {
			log.error("Unable to load game action filter", e);
			getErrorDisplay().getDelegate().record(null, getMsg("invalid-symbol-found"));
			return;
		}
		if (f instanceof VideoPokerActionFilter){
			setCombos(((VideoPokerActionFilter)f).getPaytable());
		} else if (f instanceof VideoPokerPlusActionFilter){
			setCombos(((VideoPokerPlusActionFilter)f).getPaytable());
		}
	}

	public void onSubmit(IRequestCycle cycle) throws ServiceException {
		if (getErrorDisplay().getDelegate().getHasErrors()){
			return;
		}
		
		PokerConfiguration config = (PokerConfiguration)getGameConfigsService().getFilteredConfiguration(this.getGame(),0l);
		
		GameComboBean[] combos = getCombos();
		double[] paytable = new double[combos.length];
		
		for (int i=0; i < combos.length; i++) {
			paytable[i] = combos[i].getPayout();
		}
		
		config.setPaytable(paytable);

		String value = config.serialize();
		
		com.magneta.casino.services.beans.GameConfigBean bean = new com.magneta.casino.services.beans.GameConfigBean();
		bean.setGameId(getGame());
		bean.setConfigComment(getComment());
		bean.setConfigValue(value);
		
		try {
			getGameConfigsService().insertConfig(bean);
		} catch (ServiceException e1) {
			log.error("Error while creating game Configuration", e1);
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
			return;
		}

		String url = LinkGenerator.getPageUrl("GameConfigs", getGame());
		throw new RedirectException(url);
	}
}
