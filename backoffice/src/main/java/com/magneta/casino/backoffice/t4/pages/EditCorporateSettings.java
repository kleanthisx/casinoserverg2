package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class EditCorporateSettings extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditCorporateSettings.class);
	
	@InjectPage("CorporateSettings")
	public abstract CorporateSettings getCorporateSettingsPage(); 

	public abstract double getMinBet();
	public abstract void setMinBet(double minBet);
	public abstract double getMaxBet();
	public abstract void setMaxBet(double maxBet);
	public abstract int getGameID();
	public abstract void setGameID(int gameID);
	public abstract void setGameName(String gameName);
	
	@Persist("client:page")
	public abstract Long getUserID();
	public abstract void setUserID(Long userID);
	public abstract String getUsername();
	public abstract void setUsername(String username);

	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();

	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();

	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Component(bindings={"value=gameName"})
	public abstract Insert getGameNameField();

	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getEditGameForm();
	
	@Component(id="userID",
			bindings={"value=userID"})
	public abstract Hidden getUserIDHidden();
	
	@Component(id="gameID",
			bindings={"value=gameID"})
	public abstract Hidden getGameIDHidden();

	@Component(bindings={"value=minBet","translator=bean:numTranslator","validators=validators:$minAmount","disabled=!allowWrite()"})
	public abstract TextField getMinBetField();
	
	@Component(bindings={"value=maxBet","translator=bean:numTranslator","validators=validators:$minAmount","disabled=!allowWrite()"})
	public abstract TextField getMaxBetField();
	
	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getUpdateSubmit();

	@Component
	public abstract Button getCancelButton();
	
	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		if(getMinBet()<=0 && getMaxBet()<=0) {
			delegate.setFormComponent(null);
			delegate.record(getMsg("both-empty"), null);
			return;
		}

		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;

		if (dbConn == null){
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
			return;
		}

		String sql = 
			"UPDATE corporate_game_settings" +
			" SET min_bet = ?, max_bet = ?" +
			" WHERE game_id = ?" +
			" AND corporate_id = ?";

		try {
			dbConn.setAutoCommit(false);

			statement = dbConn.prepareStatement(sql);

			if (getMinBet() > 0.0){
				statement.setBigDecimal(1, new BigDecimal(getMinBet()));
			} else {
				statement.setObject(1, null);
			}

			if (getMaxBet() > 0.0){
				statement.setBigDecimal(2, new BigDecimal(getMaxBet()));
			} else {
				statement.setObject(2, null);
			}
			statement.setInt(3, getGameID());
			statement.setLong(4, getUserID());

			statement.execute();
			dbConn.commit();

			CorporateSettings settings = getCorporateSettingsPage();
			settings.setUserID(getUserID());
			cycle.activate(settings);
		} catch (SQLException e) {
			try {
				dbConn.rollback();
			} catch (SQLException e1) {
				log.error("Error rolling back",e1);
			}
			log.error("Error while updating game properties.", e);
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
		} finally {
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
	}


	public void onCancel(IRequestCycle cycle){
		CorporateSettings settings = getCorporateSettingsPage();
		settings.setUsername(getUsername());
		settings.setUserID(getUserID());
		cycle.activate(settings);
	}
	
	@Override
	public void pageValidate(PageEvent event) {
		super.pageValidate(event);
		
		if (getUserID() == null) {
			throw new BadRequestException("Invalid/missing userId");
		}
	}

	@Override
	public void pageBeginRender(PageEvent evt){
		setUsername(getUsername(getUserID()));
	}

}
