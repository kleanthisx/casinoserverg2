package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.Checkbox;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class BankFundsReportOptions extends ReportOptions {

	@Component(bindings={"value=analyseNormalUsers", "displayName=message:AnalyseNormalUsers"})
	public abstract Checkbox getAnalyseNormalUsersCB();
	
	@Component(bindings={"value=analyseCorporateAccountUsers", "displayName=message:AnalyseCorporateAccountUsers"})
	public abstract Checkbox getAnalyseCorporateAccountUsersCB();

	@Component(bindings={"value=analyseCorporateUsers", "displayName=message:AnalyseCorporateUsers"})
	public abstract Checkbox getAnalyseCorporateUsersCB();
	
	public abstract boolean getAnalyseNormalUsers();
	public abstract boolean getAnalyseCorporateAccountUsers();
	public abstract boolean getAnalyseCorporateUsers();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		if (getPrincipal().hasPrivilege(PrivilegeEnum.CORPORATE, false)) {
			reportParams.put("analyze_corporate_account_users", getAnalyseCorporateAccountUsers());
			reportParams.put("analyze_corporate_users", getAnalyseCorporateUsers());
		}
		reportParams.put("analyze_normal_users", getAnalyseNormalUsers());
	}
}
