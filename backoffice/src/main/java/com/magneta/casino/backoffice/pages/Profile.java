package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.CountrySelectModel;
import com.magneta.casino.backoffice.selectmodels.TimeZoneSelectModel;
import com.magneta.casino.backoffice.valueencoders.CountryEncoder;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.TimeZoneService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.BACKOFFICE_ENTRY)
public class Profile {
	
	@Component(id = "userForm")
	private BeanEditForm form;
	
	@Inject
	private ExtendedPrincipalBean principal;
	
	@InjectService("UserDetailsService")
	private UserDetailsService userDetailsService;
	
	@InjectService("CountryService")
	private CountryService countryService;
	
	@InjectService("TimeZoneService")
	private TimeZoneService timeZoneService;
	
	@Property
	private UserDetailsBean userDetailsBean;
	
	@Property
	private CountryBean country;
	
	@OnEvent(value=EventConstants.PREPARE)
	public void prepareProfileForm() throws ServiceException {
		userDetailsBean = userDetailsService.getUser(principal.getUserId());
		country = countryService.getCountry(userDetailsBean.getCountryCode());
	}

	public CountrySelectModel getCountrySelectModel() {
		return new CountrySelectModel(countryService);
	}
	
	public CountryEncoder getCountryEncoder() {
		return new CountryEncoder(countryService);
	}
	
	public TimeZoneSelectModel getTimeZoneSelectModel() {
		return new TimeZoneSelectModel(timeZoneService);
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() {
		userDetailsBean.setCountryCode(country.getCode());
	
		try {
			userDetailsService.updateUser(userDetailsBean);
		} catch (ServiceException e) {
			form.recordError(e.getMessage());
			return null;
		} catch (ConditionalUpdateException e) {
			form.recordError(e.getMessage());
			return null;
		}
		return Index.class;
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return Index.class;
	}
}

