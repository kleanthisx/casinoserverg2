package com.magneta.casino.backoffice.t4.pages;


import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.beans.PayoutTransactionBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.LatestCompletePayoutsTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class PayoutsCompleted extends SecurePage {

	@Persist("client:page")
	public abstract void setResult(String result);

	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract PayoutTransactionBean getCurrPayout();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:latestCompletePayoutsTableModel",
			"columns=literal: TransactionId, TimeCompleted, Amount, PaymentMethod, !UserId","pageSize=20","row=currPayout","initialSortOrder=true","initialSortColumn=literal:TransactionId",
			"rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getLatestPayouts();
	
	public LatestCompletePayoutsTableModel getLatestCompletePayoutsTableModel(){
		return new LatestCompletePayoutsTableModel();
	} 
	
	@Component(id="result",bindings={"value=result"})
	public abstract Insert getResultField();
	
	@Component(bindings={"value=formatAmount(currPayout.amount)"})
	public abstract Insert getAmount();
	
	@Component(bindings={"value=formatDate(currPayout.timeCompleted)"})
	public abstract Insert getTimeCompleted();
	
	@Component(bindings={"userId=ognl:currPayout.userId","target=literal:_blank"})
	public abstract UserLink getUsernameLink();
	
	@Component(bindings={"page=literal:EditWireTransferPayout","parameters=currPayout.transactionId"})
	public abstract CasinoLink getEditTransactionLink();
}
