package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class UserDetailsGridDataSource extends GenericGridDataSource<UserDetailsBean> {

	private final UserDetailsService userDetailsService;
	private final SearchContext<UserDetailsBean> searchContext;
	
	public UserDetailsGridDataSource(SearchContext<UserDetailsBean> searchContext, UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
		this.searchContext = searchContext;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return userDetailsService.getUsersSize(searchContext);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<UserDetailsBean> getData(int limit, int offset,
			SortContext<UserDetailsBean> sortContext) throws Exception {
		return userDetailsService.getUsers(searchContext, limit, offset, sortContext).getResult();
	}
}
