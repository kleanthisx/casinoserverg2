package com.magneta.casino.backoffice.coercers;

import java.util.Date;

import org.apache.tapestry5.ioc.services.Coercion;

public class DateToLongCoercion implements Coercion<Date,Long> {

	@Override
	public Long coerce(Date input) {
		return input.getTime();
	}
}
