package com.magneta.casino.backoffice.pages;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(or={PrivilegeEnum.TABLES, PrivilegeEnum.GAME_CONFIG})
public class GamesAdmin {
	
}
