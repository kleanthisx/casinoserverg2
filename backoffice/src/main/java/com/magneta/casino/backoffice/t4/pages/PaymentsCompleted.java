package com.magneta.casino.backoffice.t4.pages;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.link.ExternalLink;

import com.magneta.administration.beans.PaymentTransactionBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.LatestCompletePaymentsTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class PaymentsCompleted extends SecurePage implements PageBeginRenderListener{
	
	@InjectPage("GeneralConfirmation")
	public abstract GeneralConfirmation getConfirmPage();
	
	@Persist("client:page")
	public abstract void setResult(String result);

	@InjectObject("service:tapestry.globals.HttpServletRequest")
	public abstract HttpServletRequest getRequest();
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract PaymentTransactionBean getCurrPayment();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:latestCompletePaymentsTableModel",
			"columns=columns","pageSize=20","row=currPayment","initialSortOrder=true","initialSortColumn=literal:TransactionId","rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getLatestPayments();
	
	public LatestCompletePaymentsTableModel getLatestCompletePaymentsTableModel(){
		return new LatestCompletePaymentsTableModel();
	}
	
	@Component(id="result",
			bindings={"value=result"})
	public abstract Insert getResultField();
	
	@Component(bindings={"userId=currPayment.userId","target=literal:_blank"})
	public abstract UserLink getUsernameLink();
	
	@Component(bindings={"page=literal:EditWireTransferPayment","parameters={currPayment.transactionId}"})
	public abstract ExternalLink getEditTransactionLink();
	
	@Override
	public void pageBeginRender(PageEvent event){
		String success = getRequest().getParameter("success");
		if (success != null && (!Boolean.parseBoolean(success))){
			String errorMsg = getRequest().getParameter("message");
			if (errorMsg != null && errorMsg.length() > 0){
				setResult(errorMsg);
			} else {
				setResult(getMsg("void-rejected-error"));
			}
		}
	}

	public String getColumns() {
		String columns = "TransactionId, TimeCompleted, Amount, PaymentMethod, !UserId";

		return columns;
	}
}
