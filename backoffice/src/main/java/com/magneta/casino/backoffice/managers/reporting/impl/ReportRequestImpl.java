package com.magneta.casino.backoffice.managers.reporting.impl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JasperReport;

import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;

public class ReportRequestImpl implements ReportRequest {

	private final JasperReport report;
	private final ExtendedPrincipalBean principal;
	private final ReportFormat reportFormat;
	private final Locale locale;
	private final Map<String,Object> reportParameters;
	private final TimeZone timeZone;
	private final String requestId;
	private final String requestUrl;
	private final String clientHost;
	
	public ReportRequestImpl(String requestId, JasperReport report, ReportFormat reportFormat, Map<String,Object> parameters, Locale locale, TimeZone timeZone, ExtendedPrincipalBean principal,
			String requestUrl, String clientHost) {
		this.requestId = requestId;
		this.report = report;
		this.reportFormat = reportFormat;
		this.locale = locale;
		this.principal = principal;
		this.timeZone = timeZone;
		this.requestUrl = requestUrl;
		this.clientHost = clientHost;

		if (parameters != null) {
			this.reportParameters = parameters;
		} else {
			this.reportParameters = new HashMap<String,Object>();
		}
	}
	
	@Override
	public JasperReport getReport() {
		return report;
	}
	
	/**
	 * User who requested the report.
	 * @return
	 */
	@Override
	public ExtendedPrincipalBean getPrincipal() {
		return principal;
	}
	
	@Override
	public ReportFormat getReportFormat() {
		return reportFormat;
	}
	
	@Override
	public Locale getLocale() {
		return this.locale;
	}
	
	@Override
	public Map<String,Object> getReportParameters() {
		return this.reportParameters;
	}

	@Override
	public TimeZone getTimezone() {
		return this.timeZone;
	}
	
	@Override
	public String getId() {
		return this.requestId;
	}

	@Override
	public String getRequestUrl() {
		return requestUrl;
	}

	@Override
	public String getClientHost() {
		return clientHost;
	}
}
