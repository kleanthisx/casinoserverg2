package com.magneta.casino.backoffice.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.engine.ExternalServiceParameter;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;

import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.services.T5ServiceLocator;

public class LinkGenerator {
	
	private static String encode(String value) {
		try {
			return URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String getAbsoluteUrl(HttpServletRequest req) {
		StringBuilder urlBuilder = new StringBuilder();
		
		urlBuilder.append(req.getScheme())
		.append("://")
		.append(req.getServerName());
		
		if ("http".equals(req.getScheme()) && req.getLocalPort() != 80) {
			urlBuilder.append(":");
			urlBuilder.append(req.getLocalPort());
		} else if ("https".equals(req.getScheme()) && req.getLocalPort() != 443) {
			urlBuilder.append(":");
			urlBuilder.append(req.getLocalPort());
		}
		
		urlBuilder.append(req.getContextPath());
		
		if (req.getPathInfo() != null) {
			urlBuilder.append(req.getPathInfo());
		}
		
		StringBuffer buf = req.getRequestURL();
		urlBuilder = new StringBuilder();
		urlBuilder.append(buf);
		
		char queryParam = '?';
		
		Enumeration<?> paramNames = req.getParameterNames();
		
		while(paramNames.hasMoreElements()) {
			String paramName = (String)paramNames.nextElement();
			
			for (String value: req.getParameterValues(paramName)) {
				urlBuilder.append(queryParam);
				urlBuilder.append(encode(paramName));
				urlBuilder.append('=');
				urlBuilder.append(encode(value));
				queryParam = '&';
			}
		}
		
		
		return urlBuilder.toString();
	}
	
	public static URL getAbsoluteUrl(String pageName, Object... params) {
		String pageUrl = getPageUrl(pageName, params);
		
		RequestGlobals requestGlobals = T5ServiceLocator.getService(RequestGlobals.class);
		
		HttpServletRequest req = requestGlobals.getHTTPServletRequest();
		
		StringBuilder urlBuilder = new StringBuilder();
		
		urlBuilder.append(req.getScheme())
		.append("://")
		.append(req.getServerName());
		
		if ("http".equals(req.getScheme()) && req.getLocalPort() != 80) {
			urlBuilder.append(":");
			urlBuilder.append(req.getLocalPort());
		} else if ("https".equals(req.getScheme()) && req.getLocalPort() != 443) {
			urlBuilder.append(":");
			urlBuilder.append(req.getLocalPort());
		}
		
		urlBuilder.append(pageUrl);
		
		try {
			return new URL(urlBuilder.toString());
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	/**
	 * Returns a URL for a T4 or T5 page.
	 * 
	 * The URL is relative and it includes the context path.
	 * 
	 * @param pageName
	 * @param parameters
	 * @return
	 */
	public static String getPageUrl(String pageName, Object...parameters) {
		try {
			return getT5Url(pageName, parameters);
		} catch (Throwable e) {
		}

		return getT4Url(pageName, parameters);
	}

	public static String getT5Url(String pageName, Object...parameters) {
		PageRenderLinkSource linkSource = T5ServiceLocator.getService(PageRenderLinkSource.class);
		Link link;
		
		if (parameters != null) {
			link = linkSource.createPageRenderLinkWithContext(pageName, parameters);
		} else {
			link = linkSource.createPageRenderLink(pageName);
		}

		String uri = link.toRedirectURI();		
		return uri;
	}

	public static String getT4Url(String pageName, Object... parameters) {
		IRequestCycle cycle = ServiceLocator.getService(IRequestCycle.class);
		
		IEngineService service = cycle.getInfrastructure().getServiceMap().getService(Tapestry.EXTERNAL_SERVICE);
		
		ILink link = service.getLink(false, new ExternalServiceParameter(pageName,
				parameters)); 
		
		return link.getURL();
	}

}
