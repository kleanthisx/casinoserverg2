package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.HostWhitelistBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class AddWhitelistHost {

	@Component(id="addHostForm")
	private BeanEditForm addHostForm;

	@InjectService("HostsWhitelistService")
	private HostsWhitelistService hostsService;
	
	@Property
	private HostWhitelistBean hostWhitelistBean;
	
	@Property
	private Long hostId;
	
	public void set(Long hostId){
		this.hostId = hostId;
	}
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long hostId) {
		this.hostId = hostId;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Object[] passivate() {
		return new Object[] {hostId};
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepare() throws ServiceException{
		if(hostId == null){
			hostWhitelistBean = new HostWhitelistBean();
		}else{
			hostWhitelistBean = hostsService.getHost(hostId);
		}
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() throws ServiceException{
		if(hostId == null){
			hostsService.insertHost(hostWhitelistBean);
		}else{
			hostsService.updateHost(hostWhitelistBean);
		}
		return HostsWhitelist.class;
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return HostsWhitelist.class;
	}
}
