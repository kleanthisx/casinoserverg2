package com.magneta.casino.backoffice.coercers;

import java.util.TimeZone;

import org.apache.tapestry5.ioc.services.Coercion;

public class StringToTimeZoneCoercion implements Coercion<String, TimeZone> {

	@Override
	public TimeZone coerce(String timeZoneId) {
		if (timeZoneId == null) {
			return null;
		}
		return TimeZone.getTimeZone(timeZoneId);
	}
}
