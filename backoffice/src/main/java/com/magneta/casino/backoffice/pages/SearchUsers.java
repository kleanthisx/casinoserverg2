package com.magneta.casino.backoffice.pages;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.enums.TriState;
import com.magneta.casino.backoffice.griddatasource.UserDetailsGridDataSource;
import com.magneta.casino.backoffice.selectmodels.CountrySelectModel;
import com.magneta.casino.backoffice.selectmodels.UserTypesSelectModel;
import com.magneta.casino.backoffice.valueencoders.CountryEncoder;
import com.magneta.casino.backoffice.valueencoders.UserTypeValueEncoder;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;

@RequiredPrivilege(PrivilegeEnum.PROFILES)
public class SearchUsers {

	@Inject
	private UserDetailsService userDetailsService;
	
	@Inject
	private CountryService countryService;
	
	@Property
	private UserDetailsBean user;
	
	@Inject
	private Messages messages;

	@Property
	@Persist
	private String username;
	
	@Property
	@Persist
	private String firstName;
	
	@Property
	@Persist
	private String middleName;
	
	@Property
	@Persist
	private String lastName;
	
	@Property
	@Persist
	private String email;
	
	@Property
	@Persist
	private String phone;
	
	@Property
	@Persist
	private String town;

	@Property
	@Persist
	private String region;
	
	@Property
	@Persist
	private CountryBean countryBean;
	
	@Property
	@Persist
	private Date minRegisterDate;
	
	@Property
	@Persist
	private Date maxRegisterDate;
	
	@Property
	@Persist
	private TriState activeStatus;
	
	@Property
	@Persist
	private TriState closed;

	@Property
	@Persist
	private List<UserTypeEnum> selectedUserTypes;
	
	@Inject
	private PrincipalService principalService;
	
	public UserTypesSelectModel getUserTypesSelectModel() {
		return new UserTypesSelectModel(principalService, messages);
	}
	
	public UserTypeValueEncoder getUserTypeEncoder() {
		return new UserTypeValueEncoder();
	}
	
	
	public TriState getAllActive() {
		return TriState.ALL;
	}
	
	public TriState getActive() {
		return TriState.TRUE;
	}
	
	public TriState getInactive() {
		return TriState.FALSE;
	}

	private SearchContext<UserDetailsBean> appendCondition(SearchContext<UserDetailsBean> currContext, String condition, Object value) {
		if (value == null) {
			return currContext;
		}
		
		if (currContext == null) {
			return FiqlContextBuilder.create(UserDetailsBean.class, condition, value);
		}

		return FiqlContextBuilder.create(UserDetailsBean.class, "?;" + condition, currContext, value);
	}

	private SearchContext<UserDetailsBean> createSearchContext() {
		SearchContext<UserDetailsBean> searchContext = null;
		
		searchContext = appendCondition(searchContext, "userName~=?", username);
		searchContext = appendCondition(searchContext, "firstName~=?", firstName);
		searchContext = appendCondition(searchContext, "middleName~=?", middleName);
		searchContext = appendCondition(searchContext, "lastName~=?", lastName);
		searchContext = appendCondition(searchContext, "email~=?", email);
		searchContext = appendCondition(searchContext, "phone~=?", phone);
		searchContext = appendCondition(searchContext, "town~=?", town);
		searchContext = appendCondition(searchContext, "region~=?", region);
		
		if (countryBean != null) {
			searchContext = appendCondition(searchContext, "countryCode==?", countryBean.getCode());
		}
		
		searchContext = appendCondition(searchContext, "registeredDate=ge=?", minRegisterDate);
		searchContext = appendCondition(searchContext, "registeredDate=le=?", maxRegisterDate);
		
		searchContext = appendCondition(searchContext, "isActive==?", activeStatus.getValue());
		searchContext = appendCondition(searchContext, "isClosed==?", closed.getValue());
		
		List<Integer> selectedTypes = new ArrayList<Integer>();
		
		if (selectedUserTypes != null) {
			for (UserTypeEnum type: selectedUserTypes) {
				selectedTypes.add(type.getId());
			}
		}
		searchContext = appendCondition(searchContext, "userType=in=?", selectedTypes);
		
		return searchContext;
	}
	
	@OnEvent(value=EventConstants.PREPARE, component="search")
	public void prepareSearchForm() {
		
		if (activeStatus == null) {
			activeStatus = TriState.ALL;
		}
		
		if (closed == null) {
			closed = TriState.ALL;
		}
	}
	
	public UserDetailsGridDataSource getUserDetailsGridDataSource() {
		return new UserDetailsGridDataSource(createSearchContext(), userDetailsService);
	}
	
	public CountrySelectModel getCountrySelectModel(){
		return new CountrySelectModel(countryService);
	}
	
	public CountryEncoder getCountryEncoder(){
		return new CountryEncoder(countryService);
	}
	
	@Cached(watch="user.countryCode")
	public String getCountry() throws ServiceException {
		if (user.getCountryCode() == null) {
			return null;
		}
		
		return countryService.getCountry(user.getCountryCode()).getName();
	}
	
	public String getUserType() {
		return messages.get(user.getUserTypeEnum().name());
	}
	
}
