package com.magneta.casino.backoffice.coercers;

import java.sql.Timestamp;

import org.apache.tapestry5.ioc.services.Coercion;

public class TimestampToLongCoercion implements Coercion<Timestamp, Long> {

	@Override
	public Long coerce(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}
		
		return timestamp.getTime();
	}
}
