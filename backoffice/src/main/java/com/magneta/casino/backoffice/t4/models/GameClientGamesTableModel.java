package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.GameClientGameBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GameClientGamesTableModel implements IBasicTableModel {
	
	private int clientId;
	
	public GameClientGamesTableModel(int clientId){
		this.clientId = clientId;
	}
	
	@Override
	public Iterator<GameClientGameBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<GameClientGameBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		try
		{   
			String sql = 
				"SELECT game_types.game_type_variant, game_types.game_type_id," +
				" COALESCE(game_client_games.enabled, FALSE) AS enabled"+
				" FROM game_types"+
				" LEFT OUTER JOIN game_client_games ON game_client_games.game_id = game_types.game_type_id"+
				" AND client_id = ?"+
				" ORDER BY game_types.game_type_variant"+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, clientId);
			statement.setInt(2, limit);
			statement.setInt(3, offset);

			res = statement.executeQuery();

			ArrayList<GameClientGameBean> myArr = new ArrayList<GameClientGameBean>();
			GameClientGameBean currRow;

			while (res.next()){
				currRow = new GameClientGameBean();
				
				currRow.setClientId(clientId);
				currRow.setGameId(res.getInt("game_type_id"));
				currRow.setGameName(res.getString("game_type_variant"));
				currRow.setWasEnabled(res.getBoolean("enabled"));
				
				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		GamesService gameService = ServiceLocator.getService(GamesService.class);
		int count = 0;
		
		try {
			count = gameService.getGamesSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		return count;
		
	}
}
