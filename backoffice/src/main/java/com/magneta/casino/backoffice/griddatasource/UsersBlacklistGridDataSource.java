package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsersBlacklistService;
import com.magneta.casino.services.beans.UserBlacklistBean;
import com.magneta.casino.services.search.SortContext;

public class UsersBlacklistGridDataSource extends GenericGridDataSource<UserBlacklistBean>{

	private final UsersBlacklistService usersBlacklistService;
	
	public UsersBlacklistGridDataSource(UsersBlacklistService usersBlacklistService){
		this.usersBlacklistService = usersBlacklistService;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return usersBlacklistService.getUserBlackListsSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<UserBlacklistBean> getData(int limit, int offset,
			SortContext<UserBlacklistBean> sortContext) throws Exception {
		return usersBlacklistService.getUserBlackLists(null, limit, offset, sortContext).getResult();
	}	
}
