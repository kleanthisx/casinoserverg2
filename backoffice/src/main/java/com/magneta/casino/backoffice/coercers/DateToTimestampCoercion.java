package com.magneta.casino.backoffice.coercers;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.tapestry5.ioc.services.Coercion;

public class DateToTimestampCoercion implements Coercion<Date,Timestamp> {

	@Override
	public Timestamp coerce(Date input) {
		return new Timestamp(input.getTime());
	}

}
