package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.RoleBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class RolesTableModel implements IBasicTableModel {

    @Override
	public Iterator<RoleBean> getCurrentPageRows(int arg0, int arg1, ITableColumn arg2, boolean arg3) {
        Connection dbConn = ConnectionFactory.getConnection();
        Statement statement = null;
        ResultSet res = null;
        Iterator<RoleBean> it = null;
        
        if (dbConn == null) {
			throw new RuntimeException("Out of database connections");
		}
        
        try
        {
            String sql = 
                "SELECT * FROM roles" +
                " ORDER BY role_desc";

            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);

            ArrayList<RoleBean> myArr = new ArrayList<RoleBean>();
            RoleBean currRow;

            while (res.next()){
                currRow = new RoleBean();
                currRow.setRoleId(res.getInt("role_id"));
                currRow.setRoleDesc(res.getString("role_desc"));
                myArr.add(currRow);
            }

            it = myArr.iterator();
                
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving roles list.", e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
        Connection dbConn = ConnectionFactory.getConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
        
        try
        {
            String sql = "SELECT count(*) FROM roles";

            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);

            if (res.next()){
                count = res.getInt(1);
            }
                
        }catch (SQLException e) {
            throw new RuntimeException("Error while retrieving roles count.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }

}
