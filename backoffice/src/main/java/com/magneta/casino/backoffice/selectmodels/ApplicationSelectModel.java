package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

public class ApplicationSelectModel extends AbstractSelectModel {

	private static final String[] APPLICATIONS = {
		"Backoffice",
		"Partner Web-Application"
	};
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> models = new ArrayList<OptionModel>(APPLICATIONS.length);
		
		for (String app: APPLICATIONS) {
			models.add(new OptionModelImpl(app));
		}
		return models;
	}
}
