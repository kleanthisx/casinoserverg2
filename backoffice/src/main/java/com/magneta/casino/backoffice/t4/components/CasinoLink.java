package com.magneta.casino.backoffice.t4.components;

import org.apache.tapestry.AbstractComponent;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.link.DirectLink;

import com.magneta.casino.backoffice.util.LinkGenerator;

/**
 * Link component which works with tapestry5 pages and tapestry4
 * external pages. 
 * @author anarxia
 *
 */
public abstract class CasinoLink extends AbstractComponent {

	public abstract String getTargetPage();
	public abstract Object getParameters();
	public abstract String getTarget();
	
	public abstract boolean isDisabled();
	
	@Override
	public void renderComponent(IMarkupWriter writer, IRequestCycle cycle) {
		boolean renderLink = !isDisabled();

		if (renderLink) {	
			Object[] params = DirectLink.constructServiceParameters(getParameters());
			
			String url = LinkGenerator.getPageUrl(getTargetPage(), params);

			writer.begin("a");
			writer.attribute("href", url);

			if (getTarget() != null) {
				writer.attribute("target", getTarget());
			}
			
			renderInformalParameters(writer, cycle);
		}

		renderBody(writer, cycle);

		if (renderLink) {
			writer.end();
		}
	}
	
}
