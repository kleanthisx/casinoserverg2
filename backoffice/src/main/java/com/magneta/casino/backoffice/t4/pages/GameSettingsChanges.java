package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.beans.GameSettingChangeBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.models.GameSettingsChangesTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class GameSettingsChanges extends SecurePage {
	
	public abstract GameSettingChangeBean getCurrChange();
	
	@Component(id="logTable", type="contrib:Table",
			bindings={"source=GameSettingsChangesTableModel","columns=literal:Date, Game, Username, !SettingName, !OldValue, !NewValue",
			"pageSize=30","initialSortColumn=literal:Date","row=currChange","initialSortOrder=false"})
	public abstract Table getLogTable();
	
	public GameSettingsChangesTableModel getGameSettingsChangesTableModel(){
		return new GameSettingsChangesTableModel();
	}
	
	@Component(id="date",
			bindings={"value=formatDate(currChange.Date)"})
	public abstract Insert getDate();
	
	@Component(id="viewNewLink",
			bindings={"page=literal:ViewGameConfig","parameters={currChange.gameId, currChange.newValue}"})
	public abstract CasinoLink getViewNewLink();
	
	@Component(id="viewOldLink",
			bindings={"page=literal:ViewGameConfig","parameters={currChange.gameId, currChange.oldValue}"})
	public abstract CasinoLink getViewOldLink();
}
