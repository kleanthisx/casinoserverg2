package com.magneta.casino.backoffice.t4.models;

import org.apache.tapestry.form.IPropertySelectionModel;

public class GroupingPeriodsSelectionModel implements IPropertySelectionModel {

	private String[] groupPeriods;
	
	public GroupingPeriodsSelectionModel(){
		this(false);
	}
	
	public GroupingPeriodsSelectionModel(boolean noneOption) {
		if (noneOption){
			groupPeriods = new String[4];
			groupPeriods[0] = "None";
			groupPeriods[1] = "Year";
			groupPeriods[2] = "Month";
			groupPeriods[3] = "Day";
		} else {
			groupPeriods = new String[3];
			groupPeriods[0] = "Year";
			groupPeriods[1] = "Month";
			groupPeriods[2] = "Day";
		}
	}
	
	@Override
	public String getLabel(int arg0) {
		return groupPeriods[arg0];
	}

	@Override
	public Object getOption(int arg0) {
		String option = groupPeriods[arg0].toLowerCase();
		
		if (option.equals("none")){
			return null;
		}

		return option;
	}

	@Override
	public int getOptionCount() {
		return groupPeriods.length;
	}

	@Override
	public String getValue(int arg0) {
		String option = groupPeriods[arg0].toLowerCase();
		
		if (option.equals("none")){
			return null;
		}
		return option;
	}

	@Override
	public Object translateValue(String arg0) {
		return arg0.toLowerCase();
	}

	@Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
