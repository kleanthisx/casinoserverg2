package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ExternalCallback;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.link.PageLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.JackpotBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.models.JackpotPropertiesTableModel;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class Jackpots extends SecurePage {

	private static final Logger log = LoggerFactory.getLogger(Jackpots.class);
	
	@InjectPage("EditJackpot")
	public abstract EditJackpot getEditJackpotPage();
	
    @InjectPage("GeneralConfirmation")
    public abstract GeneralConfirmation getConfirmPage();
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract JackpotBean getCurrJackpot();
	
	@Component(type="contrib:Table",bindings={"source=ognl:jackpotPropertiesTableModel","columns=columns","pageSize=20","row=currJackpot","rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getJackpotsTable();
	
	public JackpotPropertiesTableModel getJackpotPropertiesTableModel(){
		return new JackpotPropertiesTableModel(getTZ(), getLocale());
	} 
	
	@Component(bindings={"page=literal:ViewJackpots"})
	public abstract PageLink getViewJackpotsLink();
	
	@Component(bindings={"listener=listener:onEditJackpot","parameters={currJackpot.jackpotID, currJackpot.configID}"})
	public abstract DirectLink getEditJackpotLink();
	
	@Component(bindings={"listener=listener:onCloseJackpot","parameters={currJackpot.JackpotID, currJackpot.ConfigID, currJackpot.Game, currJackpot.JackpotType}"})
	public abstract DirectLink getCloseJackpotLink();
	
	@Component(id="jackpotsPaidLink",
			bindings={"page=literal:JackpotWins"})
	public abstract PageLink getJackpotsPaidLink();
	
	@Component(bindings={"page=literal:JackpotPropertiesChanges"})
	public abstract PageLink getJackpotChangesLink();
	
	@Component(bindings={"page=literal:JackpotGameSelect"})
	public abstract PageLink getCreateJackpotLink();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	public void onEditJackpot(IRequestCycle cycle){
		Object params[] = cycle.getListenerParameters();
		
		EditJackpot editJackpot = getEditJackpotPage();
		editJackpot.setJackpotID((Integer)params[0]);
		editJackpot.setConfigID((Integer)params[1]);
		cycle.activate(editJackpot);
	}
	public static class CloseJackpotConfirmContext extends ConfirmContext {
		private static final long serialVersionUID = -3116823093633321483L;
		private final Integer configId;
		private final int jackpotId;
		
		public CloseJackpotConfirmContext(String okMsg, String cancelMsg,String confirmMsg, ICallback callback,Integer configId,int jackpotId) {
			super(okMsg, cancelMsg, confirmMsg, callback);
			this.configId = configId;
			this.jackpotId = jackpotId;
		}

		@Override
		public void onOk(IRequestCycle cycle, ValidationDelegate delegate) throws ServiceException {
			Connection dbConn = ConnectionFactory.getConnection();
	        PreparedStatement statement = null;
	        
	        if (dbConn == null){
	        	delegate.record(null, "No database connection");
				return;
			}
	        
	        try{
	        	String sql = "";
	        	if (configId != null){
	        		sql = 
	        			"UPDATE jackpots" +
	                	" SET finish_date = now_utc()" +
	                 	" WHERE config_id = ?";
	        		statement = dbConn.prepareStatement(sql);
	                statement.setInt(1, configId);
	        	} else {
	        		sql = 
	                    "UPDATE jackpots" +
	                    " SET finish_date = now_utc()" +
	                    " WHERE jackpot_id = ?";
	        		statement = dbConn.prepareStatement(sql);
	                statement.setInt(1, jackpotId);
	        	}
	            statement.execute();
	            returnToPage(cycle);
	        }catch (SQLException e) {
	        	log.error("Error while closing jackpot.", e);
	        }finally{
	            DbUtil.close(statement);
	            DbUtil.close(dbConn);
	        }
	        delegate.record(null, "Error while closing jackpot");
		}
	}
	
	public void onCloseJackpot(IRequestCycle cycle,int jackpotId,Integer configId,String game,String jackpotType) {
		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		
		if (!getPrincipal().hasPrivilege(PrivilegeEnum.TABLES, true)) {
			delegate.record(null, this.getMsg("no-access"));
			return;
		}
		
		ConfirmContext confirmContext = new CloseJackpotConfirmContext(this.getMsg("ok"), this.getMsg("cancel"), this.getMsg("confirmMessage") + " " + jackpotType + " jackpot " + (jackpotType.equalsIgnoreCase("game") ? "for " + game + " ?" : " ?"),new ExternalCallback(this, null),configId,jackpotId);
		GeneralConfirmation confirmPage = getConfirmPage();
		confirmPage.setConfirmContext(confirmContext);
    	getRequestCycle().activate(confirmPage);
	}
	
	public String getColumns(){
        String columns = "Game, Corporate, !JackpotType, !Mystery, InitialAmountString, MaxAmountString, !PayTimesString, DateStarted, ContribRate, BetAmountString";

        if (getAccessValidator().checkAccess("EditJackpot")){
            columns += ", !edit";
        }
        if (getPrincipal().hasPrivilege(PrivilegeEnum.TABLES, true)) {
        	columns += ", !close";
        }
        return columns;
    }
}
