package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.AffiliatesSelectionModel;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(and={PrivilegeEnum.REPORTS,PrivilegeEnum.CORPORATE})
public abstract class CorporatePayoutRatesReportOptions extends DateReportOptions {
	
	@Component(bindings = {"value=corporate","model=ognl:affiliatesSelectionModel", "displayName=message:Corporate"})
	public abstract PropertySelection getCorporatesSelection();
	
	public AffiliatesSelectionModel getAffiliatesSelectionModel(){
		return new AffiliatesSelectionModel(0L,true,false);
	}

	@Component(bindings = {"value=game", "model=ognl:gamesSelectionModel", "displayName=message:Game"})
	public abstract PropertySelection getGameSelection();
	
	public GamesSelectionModel getGamesSelectionModel(){
		return new GamesSelectionModel(true);
	}
	
	@Component(bindings = {"value=period","model=new com.magneta.casino.backoffice.t4.models.GroupingPeriodsSelectionModel(true)", "displayName=message:GroupingPeriod"})
	public abstract PropertySelection getGroupPeriods();
	
	@Component(bindings={"value=ShowDetails", "displayName=message:ShowDetails"})
	public abstract Checkbox getShowDetailsCB();
	
	@Component(bindings={"value=IncludeJackpots", "displayName=message:IncludeJackpots"})
	public abstract Checkbox getIncludeJackpotsCB();
	
	public abstract String getUsername();	
	public abstract String getCountry();
	public abstract int getGame();
	public abstract Long getCorporate();
	public abstract String getPeriod(); 
	public abstract boolean getShowDetails();
	@InitialValue("true")
	public abstract boolean getIncludeJackpots();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		
		reportParams.put("username", getUsername());
		reportParams.put("country_val", getCountry());
		if (getGame() != -1){
			reportParams.put("game_type_id", getGame());
		}
		reportParams.put("group_period",getPeriod());
		reportParams.put("show_details", getShowDetails());
		reportParams.put("include_jackpots", getIncludeJackpots());
		reportParams.put("corporate_id",getCorporate());
	}
}
