package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.ExternalLink;

import com.magneta.administration.beans.AffiliateBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.AffiliatesTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class CorporateApplications extends SecurePage {

	public abstract void setNoticeMsg(String noticeMsg);

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract AffiliateBean getCurrApplication();

	@Component(type="contrib:Table",bindings={"source=ognl:affiliatesTableModel","columns=literal: !Username, !FirstName, !LastName, !AccountType, !Applied, !examine",
			"pageSize=30","rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:Username","row=currApplication","initialSortOrder=true"})
			public abstract Table getAffiliateApplicants();

	public AffiliatesTableModel getAffiliatesTableModel(){
		return new AffiliatesTableModel(true, false,-1L);
	}

	@Component(bindings={"userId=ognl:currApplication.Id","target=literal:_blank"})
	public abstract UserLink getUsernameLink();
	
	@Component(bindings={"value=formatDate(currApplication.RegisterDate)"})
	public abstract Insert getAppliedDate();

	@Component(bindings={"page=literal:ExamineCorporate","parameters={currApplication.Id}"})
	public abstract ExternalLink getExamineLink();
}
