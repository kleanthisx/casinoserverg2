package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserCommentService;
import com.magneta.casino.services.beans.UserCommentBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(value=PrivilegeEnum.COMMENTS,write=true)
public class EditUserComment {

	@Property
	private Long userId;
	
	@Property
	private Long commentId;
	
	@Inject
	private PageRenderLinkSource linkSource;
	
	@Inject
	private UserCommentService userCommentService;
	
	@Inject
	private AlertManager alertManager;
	
	@Inject
	private Messages messages;
	
	@Property
	private UserCommentBean comment;
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Long userId, Long commentId) {
		this.userId = userId;
		this.commentId = commentId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Object[] onPassivate() {
		return new Object[] {this.userId, this.commentId};
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepareForm() throws ServiceException{
		comment = userCommentService.getComment(userId, commentId);
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() throws ServiceException {
		userCommentService.editComment(comment);
		alertManager.success(messages.format("comment-updated", this.commentId));
		return linkSource.createPageRenderLinkWithContext(UserComments.class, userId);
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Link onCancelForm(){
		return linkSource.createPageRenderLinkWithContext(UserComments.class, userId);		
	}
}
