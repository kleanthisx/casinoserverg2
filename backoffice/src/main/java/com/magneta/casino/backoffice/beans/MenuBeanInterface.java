package com.magneta.casino.backoffice.beans;

import java.util.List;

public interface MenuBeanInterface {
	Integer getId();
	List<? extends MenuBeanInterface> getSubMenu();
	String getPageLink();
	Boolean isSeperator();
	String getDescription();
}
