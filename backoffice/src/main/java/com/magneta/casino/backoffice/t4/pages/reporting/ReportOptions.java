package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry5.ioc.services.TypeCoercer;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportList;
import com.magneta.casino.backoffice.managers.reporting.ReportRepository;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.pages.SecurePage;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.util.UrlBuilder;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class ReportOptions extends SecurePage {
	
	@InjectObject("service:tapestry.globals.HttpServletRequest")
    public abstract HttpServletRequest getRequest();
	
	@InjectObject("service:casino.common.Settings")
	public abstract SettingsService getSettingsService();
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@InjectObject("t5:ReportList")
	public abstract ReportList getReportListService();

	@InjectObject("t5:ReportRepository")
	public abstract ReportRepository getReportRepository();
	
	@InjectObject("t5:ReportService")
	public abstract ReportService getReportService();
	
	@InjectObject("t5:TypeCoercer")
	public abstract TypeCoercer getTypeCoercer();

	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Component(id = "reportDescMsg",
			bindings = {"value=ReportDesc"})
	public abstract Insert getReportDescMsg();
	
	@Component(id = "formats",
			bindings = {"value=format","model=ognl:new com.magneta.administration.commons.models.ReportFormatsSelectionModel()", 
		"displayName=message:ReportFormat"})
	public abstract PropertySelection getFormats();
	
	@Component(id = "formatsLabel",
			bindings = {"field=component:formats"})
	public abstract FieldLabel getFormatsLabel();
	
	@Component(id = "zones",
			bindings = {"value=timeZone","model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel(TZ)", 
			"displayName=message:ReportTimeZone"})
	public abstract PropertySelection getZones();
	
	@Component(id = "zoneLabel",
			bindings = {"field=component:zones"})
	public abstract FieldLabel getZoneLabel();
	
	@Component(id = "okSubmit")
	public abstract Submit getOkSubmit(); 
	
	@Component(id = "cancelButton")
	public abstract Button getCancelButton();
	
	@Component(id = "result",
			bindings = {"value=Result"})
	public abstract Insert getResultInsert();
	
	@Component(id="reportDataForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.delegate","clientValidationEnabled=true"})
	public abstract Form getReportDataForm();
	
	public abstract String getFormat();
	public abstract String getTimeZone();
		
	
	public abstract String getReportId();
	public abstract void setReportId(String reportId);
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		if (params != null && params.length > 0) {
			setReportId((String)params[0]);
			
			try {
				setReport(getReportListService().getReport(getReportId()));
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	@Persist("client:page")
	public abstract ReportBean getReport();
	public abstract void setReport(ReportBean report);
	
	public abstract void setResult(String result);
	
	public void setError(String message) {
		getErrorDisplay().getDelegate().record(null, message);
	}
	
	public ILink onSubmit(IRequestCycle cycle) throws ServiceException, JRException {
		if (getErrorDisplay().getDelegate().getHasErrors()){
			return null;
		}
		
		if (getReport() == null) {
			setError("Report is null");
			return null;
		}
		
		Map<String,Object> reportParams = new HashMap<String,Object>();
		fillReportParameters(reportParams);
		
		if (isQueuedReport()) {
			ReportRequest request = getReportService().createRequest(getReportName(), ReportFormat.valueOf(getFormat()), TimeZone.getTimeZone(getTimeZone()), getLocale(), reportParams);
			getReportRepository().addScheduledReportRequest(request);
			setResult("Your report will be executed in the background. Please check the reports page.");
			return null;
		}

		UrlBuilder urlBuilder = new UrlBuilder(LinkGenerator.getT5Url("reports/execute", getReportName(), ReportFormat.valueOf(getFormat())));

		urlBuilder.appendParameter("tz", getTimeZone());

		for (Map.Entry<String, Object> param: reportParams.entrySet()) {
			urlBuilder.appendParameter(param.getKey(), getTypeCoercer().coerce(param.getValue(), String.class));
		}

		throw new RedirectException(urlBuilder.toString());
	}
    
	/**
	 * Fills the parameter values of the report with the values supplied by the form of the page.
	 * Should be overriden by subclasses when they have report-specific parameters to add.
 	 * @param reportParams The list to fill with the parameters and values. Should never be null.
	 */
	public void fillReportParameters(Map<String,Object> params) {
	}
	
	/**
	 * Gets the file name of the report the page configures.
	 * @return The name of the report the page configures.
	 */
	public String getReportName() {
		return getReport().getReport();
	}
	
	public String getReportDesc() {
		return getReport().getReportDesc();
	}
	
	@Override
	public String getPageTitle() {
		return getReport().getReportName();
	}
	
    public void onCancel(IRequestCycle cycle) {        
        throw new RedirectException(LinkGenerator.getPageUrl("reports/Index"));
    }
    
    public boolean isQueuedReport() throws ServiceException{
    	return getReport() != null && getReport().isQueueReport() && getSettingsService().getSettingValue("system.reports.use_report_queue", Boolean.TYPE);
    }
}