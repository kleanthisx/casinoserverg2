package com.magneta.casino.backoffice.t4.models;

import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.t4.utils.TableSortContext;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserLoginsService;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.beans.UserLoginBean;

public class LatestUserLoginsTableModel implements IBasicTableModel{

	private Long userId;

	private final UserLoginsService service = ServiceLocator.getService(UserLoginsService.class);

	public LatestUserLoginsTableModel(Long userId){
		if(userId == null){
			this.userId = 0L;
		}else{
			this.userId = userId;
		}
	}

	@Override
	public Iterator<UserLoginBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {

		try {
			ServiceResultSet<UserLoginBean> logins = service.getLogins(userId, null, limit, offset, new TableSortContext<UserLoginBean>(sortCol, sortAsc));

			if(logins != null){
				return logins.getResult().iterator();
			}
		} catch (ServiceException e) {
			throw new RuntimeException("Error while getting user logins",e);
		}

		return null;
	}

	@Override
	public int getRowCount() {
		return 0;
	}
}
