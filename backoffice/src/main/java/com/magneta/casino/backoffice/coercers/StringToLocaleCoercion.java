package com.magneta.casino.backoffice.coercers;

import java.util.Locale;

import org.apache.tapestry5.ioc.services.Coercion;

public class StringToLocaleCoercion implements Coercion<String, Locale> {

	@Override
	public Locale coerce(String input) {
		if (input == null)
			return null;
		
		String[] parts = input.split("_");
		
		if (parts.length == 1) {
			return new Locale(input);
		}
		
		if (parts.length == 2) {
			return new Locale(parts[0], parts[1]);
		}
		
		if (parts.length == 3) {
			return new Locale(parts[0], parts[1], parts[2]);
		}
		
		throw new RuntimeException("Invalid locale: " + input);
	}

}
