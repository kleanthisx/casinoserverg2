package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.SystemSettingChangeBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class SystemSettingsChangesTableModel implements IBasicTableModel {
	
	@Override
	public Iterator<SystemSettingChangeBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		
		SettingsService settingsService = ServiceLocator.getService(SettingsService.class);
		
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<SystemSettingChangeBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumnName = "Username";

        if (sortCol != null){
            sortColumnName = sortCol.getColumnName();
        }
        
        if (("UserId").equals(sortColumnName))
            sortColumnName = "change_user";
        else if (("SettingName").equals(sortColumnName))
            sortColumnName = "setting_key";
        else
            sortColumnName = "change_time";
		
        List<SystemSettingChangeBean> myArr = new ArrayList<SystemSettingChangeBean>();
        
		try {
			String sql = 
				" SELECT change_user, setting_key, change_time, new_value, old_value"+
				" FROM system_settings_changes"+
				" WHERE old_value IS DISTINCT FROM new_value" +
				" ORDER BY "+sortColumnName+(sortAsc? " ASC": " DESC")+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			
			SystemSettingChangeBean currRow;

			while (res.next()){
				currRow = new SystemSettingChangeBean();
				
				currRow.setUserId(DbUtil.getLong(res, "change_user"));
				currRow.setDate(DbUtil.getTimestamp(res, "change_time"));
				currRow.setNewValue(res.getString("new_value"));
				currRow.setOldValue(res.getString("old_value"));
				currRow.setSettingName(res.getString("setting_key"));
				myArr.add(currRow);
			}
			

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving system settings changes.",e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		try {
		for (SystemSettingChangeBean bean: myArr) {
			SettingBean setting;

			setting = settingsService.getSetting(bean.getSettingName());			
			if (setting != null) {
				bean.setSettingName(setting.getDescription());
			}
		}
		
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		it = myArr.iterator();
		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try {
            String sql = 
                "SELECT COUNT(*)"+
            	" FROM system_settings_changes" +
            	" WHERE old_value IS DISTINCT FROM new_value";
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting system settings changes count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
