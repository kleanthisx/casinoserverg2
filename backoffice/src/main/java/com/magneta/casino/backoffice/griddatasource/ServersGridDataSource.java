package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.search.SortContext;

public class ServersGridDataSource extends GenericGridDataSource<ServerBean> {
	
	private final ServersService serversService;
	
	public ServersGridDataSource(ServersService serversService) {
		this.serversService = serversService;
	}

	@Override
	public int getAvailableRows() {
		try {
			return serversService.getServersSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ServerBean> getData(int limit, int offset, SortContext<ServerBean> sortContext) throws Exception {
		return serversService.getServers(null, sortContext, limit, offset).getResult();
	}

}
