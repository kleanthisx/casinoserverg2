package com.magneta.casino.backoffice.pages.reports;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.RepositoryReportGridDataSource;
import com.magneta.casino.backoffice.managers.reporting.ReportList;
import com.magneta.casino.backoffice.managers.reporting.ReportRepository;
import com.magneta.casino.backoffice.managers.reporting.RepositoryReport;
import com.magneta.casino.backoffice.managers.reporting.impl.RepositoryReportStreamResponse;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public class Index {

	@Inject
	private ReportRepository repository;
	
	@Inject
	private PrincipalService principalService;
	
	@Inject
	private ReportList reportList;
	
	@Inject
	private Messages messages;
	
	@Property
	private String currCategory;
	
	public List<String> getCategories() {
		return reportList.getCategories();
	}
	
	public String getCategoryName() {
		return messages.get(currCategory);
	}
	
	@Property
	private ReportBean report;
	
	public List<ReportBean> getCategoryReports() throws ServiceException {
		return reportList.getReports(currCategory);
	}

	@Property
	private RepositoryReport currRepositoryReport;
	
	public RepositoryReportGridDataSource getRepositoryReportsGridDataSource() {
		return new RepositoryReportGridDataSource(repository, principalService);
	}

	@Cached(watch="currRepositoryReport")
	public String getReportStatus() {
		if (currRepositoryReport.getFuture().isCancelled()) {
			return "CANCELLED";
		}
		
		if (!currRepositoryReport.getFuture().isDone()) {
			return "PENDING";
		}

		try {
			currRepositoryReport.getFuture().get();
		} catch (InterruptedException e) {
			Thread.interrupted();
			return "FINISHED";
		} catch (ExecutionException e) {
			return "FAILED";
		}
		
		return "FINISHED";
	}
	
	public boolean isShowDownloadAction() {
		return getReportStatus().equals("FINISHED");
	}
	
	public boolean isShowCancelAction() {
		return false;
		
		/* We cannot really cancel the reports for now */
		//return getReportStatus().equals("PENDING");
	}
	
	@OnEvent(value=EventConstants.ACTION, component="downloadReport")
	public Object downloadReport(String requestId) throws ServiceException {
		RepositoryReport report = repository.getReport(requestId);
		return new RepositoryReportStreamResponse(report);
	}
	
	@OnEvent(value=EventConstants.ACTION, component="cancelReport")
	public void cancelReport(String requestId) {
		repository.cancelReport(requestId);
	}
	
	@OnEvent(value=EventConstants.ACTION, component="deleteReport")
	public void removeReport(String requestId) {
		repository.removeReport(requestId);
	}
}
