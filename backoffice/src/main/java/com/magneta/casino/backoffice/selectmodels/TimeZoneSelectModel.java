package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.TimeZoneService;

public class TimeZoneSelectModel  extends AbstractSelectModel {

	private final TimeZoneService timezoneService;
	
	public TimeZoneSelectModel(TimeZoneService timezoneService) {
		this.timezoneService = timezoneService;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<String> timezones = timezoneService.getTimeZones();
		List<OptionModel> models = new ArrayList<OptionModel>(timezones.size());
		
		for (String tz: timezones) {
			models.add(new OptionModelImpl(tz));
		}
		
		return models;
	}
}
