package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class DateReportOptions extends ReportOptions {

	@Component(id = "fromDate", bindings={"value=minDate","displayName=message:From", "translator=translator:date,pattern=dd/MM/yyyy"})
	public abstract DatePicker getFromDate();
	
	@Component(id = "toDate", 
			bindings={"value=maxDate","displayName=message:To", "translator=translator:date,pattern=dd/MM/yyyy"})
	public abstract DatePicker getToDate();
	
	@Component(id = "fromDateLabel",
			bindings={"field=component:fromDate"})
	public abstract FieldLabel getFromDateLabel();
	
	@Component(id = "toDateLabel",
			bindings={"field=component:toDate"})
	public abstract FieldLabel getToDateLabel();
	
	@Component(id = "periodMsg",
			bindings = {"value=message:Period"})
	public abstract Insert getPeriodMsg();
	
	public abstract Date getMinDate();
	public abstract Date getMaxDate();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
	
		Date maxDate = normalizeDate(getMaxDate());
		Date minDate = normalizeDate(getMinDate());
        
		//NORMALIZE DATES
        if (minDate != null) {
        	Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
        	calendar.setTimeInMillis(minDate.getTime());
        	calendar.set(Calendar.HOUR_OF_DAY, 0);
        	calendar.set(Calendar.MINUTE, 0);
        	
            reportParams.put("min_date", calendar.getTime());
        }
		
        if (maxDate != null) {
        	Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
        	calendar.setTimeInMillis(maxDate.getTime());
        	calendar.set(Calendar.HOUR_OF_DAY, 24);
        	calendar.set(Calendar.MINUTE, 0);
        	calendar.add(Calendar.MILLISECOND, -1);
        	
            reportParams.put("max_date", calendar.getTime());
        }
	}
}
