package com.magneta.casino.backoffice.beans;

import java.util.List;

public class MenuBean implements MenuBeanInterface {  
	private String description;
	private String messageCatalogMessage;
	private String pageLink;
	private String toolTip;
	private Integer id;
	private List<? extends MenuBeanInterface> subMenu;
	
	public void setSubMenu(List<? extends MenuBeanInterface> subMenu) {
		this.subMenu = subMenu;
	}
	
	@Override
	public List<? extends MenuBeanInterface> getSubMenu() {
		return this.subMenu;
	}

	public void setToolTip(String toolTip) {
		this.toolTip = toolTip;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getToolTip() {
		return toolTip;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	@Override
	public String getPageLink() {
		return pageLink;
	}

	public void setMessageCatalogMessage(String messageCatalogMessage) {
		this.messageCatalogMessage = messageCatalogMessage;
	}

	public String getMessageCatalogMessage() {
		return messageCatalogMessage;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Boolean isSeperator() {
		if(this.getDescription() == null) {
			return false;
		}
		if(this.getDescription().equals("@seperator@")) {
			return true;
		}
		return false;
	}
	@Override
	public Integer getId() {
		return this.id;

	}
}
