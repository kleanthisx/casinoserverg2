package com.magneta.casino.backoffice.valueencoders;

import org.apache.tapestry5.ValueEncoder;

import com.magneta.casino.services.beans.PaymentTypeBean;

public class PaymentTypeEncoder implements ValueEncoder<PaymentTypeBean> {

	private final Iterable<PaymentTypeBean> paymentTypes;
	
	public PaymentTypeEncoder(Iterable<PaymentTypeBean> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	@Override
	public String toClient(PaymentTypeBean value) {
		return value.getPaymentType();
	}

	@Override
	public PaymentTypeBean toValue(String clientValue) {	
		for(PaymentTypeBean p: paymentTypes) {
			if(p.getPaymentType().equals(clientValue)) {
				return p;
			}
		}
		
		throw new IllegalArgumentException("Invalid payment type");
	}

}
