package com.magneta.casino.backoffice.bindings;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.tapestry5.Binding;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.ioc.Location;
import org.apache.tapestry5.services.BindingFactory;
import org.apache.tapestry5.services.BindingSource;

import com.magneta.casino.services.CurrencyParser;

public class FormatCurrencyBindingFactory implements BindingFactory {
	
	private final BindingSource bindingSource;
	private final CurrencyParser currencyParser;
	
	public FormatCurrencyBindingFactory(BindingSource bindingSource, CurrencyParser currencyParser) {
		this.currencyParser = currencyParser;
		this.bindingSource = bindingSource;
	}

	@Override
	public Binding newBinding(String description, ComponentResources container,ComponentResources component, String expression, Location location) {
		List<String> parts = new ArrayList<String>(Arrays.asList(expression.split(",")));
		if(parts.size() < 2) {
			parts.add("true");
		}
        ArrayList<Binding> bindings = new ArrayList<Binding>(parts.size());
        for (String part : parts)
        {
            String prefix = BindingConstants.PROP;
            part = part.trim();

            if (part.equals("false") || part.equals("true")) {
                prefix = BindingConstants.LITERAL;
            }

            bindings.add(bindingSource.newBinding(description, container, component, prefix, part, location));
        }
		
        boolean invariant = true;
        for (Binding binding : bindings) {
            if (!binding.isInvariant()) {
                invariant = false;
                break;
            }
        }
        
		return new FormatCurrencyBinding(location, invariant, bindings, currencyParser, container.getLocale());
	}
}
