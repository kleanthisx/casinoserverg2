package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

public class GameTemplateSelectModel extends AbstractSelectModel {

	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<String> templates = com.magneta.casino.games.templates.GameTemplateInfoFactory.getGameTemplates();

		List<OptionModel> model = new ArrayList<OptionModel>(templates.size());
		
		for (String template: templates) {
			model.add(new OptionModelImpl(template));
		}
		
		return model;
	}

}
