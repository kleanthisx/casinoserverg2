package com.magneta.casino.backoffice.valueencoders;

import org.apache.tapestry5.ValueEncoder;

import com.magneta.casino.services.enums.UserTypeEnum;

public class UserTypeValueEncoder implements ValueEncoder<UserTypeEnum> {

	@Override
	public String toClient(UserTypeEnum value) {
		return value.name();
	}

	@Override
	public UserTypeEnum toValue(String clientValue) {
		return UserTypeEnum.valueOf(clientValue);
	}

}
