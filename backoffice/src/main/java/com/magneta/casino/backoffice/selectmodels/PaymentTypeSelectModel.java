package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.beans.PaymentTypeBean;

public class PaymentTypeSelectModel extends AbstractSelectModel {

	private final Iterable<PaymentTypeBean> paymentTypes; 
	
	public PaymentTypeSelectModel(Iterable<PaymentTypeBean> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> result = new ArrayList<OptionModel>();

		for(PaymentTypeBean bean: paymentTypes) {
			result.add(new OptionModelImpl(bean.getPaymentType(), bean));
		}
		
		return result;
	}
}
