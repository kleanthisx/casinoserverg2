package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.administration.SystemProperties;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class JackpotTypeSelect extends SecurePage {

	public abstract int getType();
	
	@InjectPage("JackpotGameSelect")
	public abstract JackpotGameSelect getGamesSelPage();
	
	@InjectPage("JackpotCorporateSelect")
	public abstract JackpotCorporateSelect getCorpSelPage();
	
	@InjectPage("CreateJackpot")
	public abstract CreateJackpot getCreateJackpotPage();
	
	@Component(id="jackpotGameFrm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel"})
	public abstract Form getJackpotGameFrm();
	
	@Component(id="typeLabel",
			bindings={"field=component:type"})
	public abstract FieldLabel getTypeLabel();
	
	@Component(id="type",
			bindings={"value=type","displayName=message:Type","model=ognl:new com.magneta.casino.backoffice.t4.models.JackpotTypesSelectionModel()",
			"disabled=!allowWrite()"})
	public abstract PropertySelection getTypeSelection();
	
	@Component(id="okSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	public void onSubmit(IRequestCycle cycle) throws ServiceException{
		switch(getType()){
			case 1:{
				JackpotGameSelect selPage = getGamesSelPage();
				selPage.setJackpotType(getType());
				cycle.activate(selPage);
				break;
			}
			case 2:{
				CreateJackpot createPage = getCreateJackpotPage();
				createPage.setJackpotType(getType());
				cycle.activate(createPage);
				break;
			}
			case 3:{
				if (SystemProperties.globalCorporateJackpot()){
					CreateJackpot createJackpot = getCreateJackpotPage();
					createJackpot.setJackpotType(getType());
					cycle.activate(createJackpot);
				} else {
					JackpotCorporateSelect selPage = getCorpSelPage();
					selPage.setJackpotType(getType());
					cycle.activate(selPage);
				}
				break;
			} 
		}
	}
	
	public void onCancel(){
		redirectToPage("Jackpots");
	}
}
