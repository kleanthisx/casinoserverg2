/**
 * 
 */
package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.contrib.ajax.XTile;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.link.PageLink;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.administration.beans.JackpotBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.ActiveJackpotsTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.GAME_HISTORY)
public abstract class ViewJackpots extends SecurePage {

    @Bean
    public abstract ValidationDelegate getDelegate();
    
    @Bean
    public abstract EvenOdd getEvenOdd();
    

    @Bean(initializer="omitZero=true")
    public abstract SimpleNumberTranslator getNumTranslator();
    
    public abstract JackpotBean getCurrJackpot();
    
    @Component(type="contrib:Table",
    		bindings={"source=ognl:activeJackpotsTableModel","columns=literal: !JackpotID, !JackpotType, !Game, !Corporate, !JackpotAmountString, !TimesPaid, !adjustment",
    		"pageSize=30","row=currJackpot","rowsClass=ognl:beans.evenOdd.next","persist=literal:session","volatile=true"})
    public abstract Table getJackpotsTable();
    
    public ActiveJackpotsTableModel getActiveJackpotsTableModel(){
    	return new ActiveJackpotsTableModel();
    }
    
    @Component(type="contrib:XTile",
    		bindings={"listener=listener:handleJackpotsRefresh","sendName=literal:requestJackpots","receiveName=literal:getRefreshedJackpots","disableCaching=true"})
    public abstract XTile getJackpotsRefreshTile();
    
    @Component(bindings={"listener=listener:onSubmit","delegate=beans.delegate","clientValidationEnabled=true"})
    public abstract Form getJackpotsForm();
    
    @Component(bindings={"value=currJackpot.jackpotAmountString","disabled=!allowWrite()"})
    public abstract TextField getJackpotAmount();
    
    @Component(bindings={"page=literal:Jackpots"})
    public abstract PageLink getJackpotPropertiesLink();
    
    @Component(bindings={"value=currJackpot.adjustmentAmount","disabled=!allowWrite()","displayName=message:Amount","translator=bean:numTranslator"})
    public abstract TextField getAdjustmentAmount();
    
    @Component(bindings={"value=currJackpot.jackpotID"})
    public abstract Hidden getJackpotID();
    
    @Component(bindings={"value=currJackpot.timesPaid","disabled=!allowWrite()"})
    public abstract TextField getPaidTimes();
    
    public void onSubmit(IRequestCycle cycle){
    	redirectToPage("ViewJackpots");
    }
    
    public void handleJackpotsRefresh(IRequestCycle cycle){
        try{
            String jackpotIds = (String)cycle.getListenerParameters()[0];
            StringTokenizer tokenizer = new StringTokenizer(jackpotIds,",");            
            String[] amounts = new String[tokenizer.countTokens()];
            
            Map<Integer, String> jackpots = new HashMap<Integer, String>();
            Connection conn = ConnectionFactory.getReportsConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {	
            	String sql = 
            		"SELECT jackpot_id, balance AS jackpot_amount, winners"+
                	" FROM jackpot_balances"+
                	" ORDER BY jackpot_id";
            	
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
                
                while (rs.next()) {
                    String amount = getCurrencyParser().formatDouble(rs.getDouble("jackpot_amount"));
                    jackpots.put(rs.getInt("jackpot_id"), amount+"_"+rs.getString("winners"));
                }
                
                int i = 0;
                while (tokenizer.hasMoreTokens()){
                    int nextId = Integer.parseInt(tokenizer.nextToken());
                    String s = jackpots.get(nextId);
                    if (s != null){
                    	amounts[i] = s;
                    } else {
                    	amounts[i] = "_";
                    }
                    i++;
                }
                
                cycle.setListenerParameters(amounts);
            } catch (SQLException e) {
                throw new RuntimeException("Error retrieving jackpot amounts.", e);
            } finally {
                DbUtil.close(rs);
                DbUtil.close(stmt);
                DbUtil.close(conn);
            }
        } catch (Throwable e){
            throw new RuntimeException("Error updating jackpots.",e);
        }
    }
}
