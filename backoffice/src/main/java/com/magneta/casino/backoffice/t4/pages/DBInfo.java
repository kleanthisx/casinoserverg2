package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.ForBean;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class DBInfo extends SecurePage implements PageBeginRenderListener {
	
	private static final Logger log = LoggerFactory.getLogger(DBInfo.class);
	
	public class DBReplicationInfo {
		private String description;
		private String lagTime;
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getLagTime() {
			return lagTime;
		}
		public void setLagTime(String lagTime) {
			this.lagTime = lagTime;
		}
	}
	
	public abstract DBReplicationInfo getCurrRep();
	
	public abstract void setDBMSName(String dbmsName);
	public abstract void setDBSize(String dbSize);
	public abstract void setUptime(String uptime);
	public abstract void setReplicationDBInfo(List<DBReplicationInfo> replicationDBInfo);
	public abstract void setReplicatorModuleVersion(String replicatorModuleVersion);
	public abstract void setReplicatorSchemaVersion(String replicatorSchemaVersion);
	
	@Bean
	public abstract EvenOdd getEvenOdd();

	@Component(id="dbSize",
			bindings={"value=DBSize"})
	public abstract Insert getDbSize();
	
	@Component(id="uptime",
			bindings={"value=uptime"})
	public abstract Insert getUptimeField();
	
	@Component(id="dbmsVersion",
			bindings={"value=DBMSName"})
	public abstract Insert getDbmsVersion();
	
	@Component(id="repModuleVersion",
			bindings={"value=replicatorModuleVersion"})
	public abstract Insert getRepModuleVersion();
	
	@Component(id="repSchemaVersion",
			bindings={"value=ReplicatorSchemaVersion"})
	public abstract Insert getRepSchemaVersion();
	
	@Component(id="replications", type="For",
			bindings={"source=ReplicationDBInfo","value=currRep","element=literal:tr"})
	public abstract ForBean getReplications();
	
	@Component(id="description",
			bindings={"value=currRep.Description"})
	public abstract Insert getDescription();
	
	@Component(id="lagTime",
			bindings={"value=currRep.LagTime"})
	public abstract Insert getLagTime();
	
	@Override
	public void pageBeginRender(PageEvent pEvt){
		Connection conn = ConnectionFactory.getConnection();
		if (conn == null) {
			throw new RuntimeException("Out of database connections");
		}

		try {
			Statement stmt = null;
			ResultSet rs = null;
			try {
				stmt = conn.createStatement();

				rs = stmt.executeQuery(
					"SELECT pg_catalog.version() AS db_name," +
					" pg_catalog.pg_size_pretty(pg_catalog.pg_database_size(pg_catalog.current_database())) AS size,"+ 
					" (extract('epoch' from CURRENT_TIMESTAMP - pg_catalog.pg_postmaster_start_time()))::bigint AS uptime");

				if (rs.next()){
					setDBMSName(rs.getString("db_name"));
					setDBSize(rs.getString("size"));
					String uptime = "";
					long secs = rs.getLong("uptime");
					long days = secs / (24 * 60 * 60);
					secs -= days * (24 * 60 * 60);
					if (days > 0){
						uptime += days+"d ";
					}
					long hours = secs / (60 * 60);
					secs -= hours * (60 * 60);
					uptime += hours+"h ";
					long mins = secs / 60;
					secs -= mins * (60);
					uptime += mins+"m "+secs+"s";
					setUptime(uptime);
				}
			} finally {
				DbUtil.close(stmt);
				DbUtil.close(rs);
			}
			
			String schema = null;
			Statement stmt3 = null;
			ResultSet rs3 = null;
			try {
				stmt3 = conn.createStatement();

				rs3 = stmt3.executeQuery(
					"SELECT table_schema" +
					" FROM INFORMATION_SCHEMA.tables"+
					" WHERE table_name='sl_status'; ");

				if (rs3.next()){
					schema = rs3.getString(1);
				}
			} finally {
				DbUtil.close(stmt3);
				DbUtil.close(rs3);
			}
			
			List<DBReplicationInfo> repDBInf = new ArrayList<DBReplicationInfo>();
			
			if (schema != null){
				Statement stmt4 = null;
				ResultSet rs4 = null;
				try {
					stmt4 = conn.createStatement();

					rs4 = stmt4.executeQuery(
						"SELECT "+schema+".getmoduleversion(), "+schema+".slonyversion();");

					if (rs4.next()){
						setReplicatorModuleVersion(rs4.getString(1));
						setReplicatorSchemaVersion(rs4.getString(2));
					}
				} finally {
					DbUtil.close(stmt4);
					DbUtil.close(rs4);
				}
				
				Statement stmt5 = null;
				ResultSet rs5 = null;
				try {
					stmt5 = conn.createStatement();

					rs5 = stmt5.executeQuery(
						"SELECT no_comment, st_lag_time "+
						" FROM "+schema+".sl_status"+
						" INNER JOIN "+schema+".sl_node ON st_received=no_id"); 

					while (rs5.next()){
						DBReplicationInfo inf = new DBReplicationInfo();
						inf.setDescription(rs5.getString("no_comment"));
						inf.setLagTime(rs5.getString("st_lag_time"));
						repDBInf.add(inf);
					}
				} finally {
					DbUtil.close(stmt5);
					DbUtil.close(rs5);
				}
			}
			
			setReplicationDBInfo(repDBInf);
		} catch (SQLException e){
			log.error("Error while getting DB info.", e);
		} finally {
			DbUtil.close(conn);
		}
	}
}
