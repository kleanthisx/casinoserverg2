package com.magneta.casino.backoffice.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.dom.Element;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.pages.UserDetails;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;

@SupportsInformalParameters
public class UserLink {
	
	@InjectService("UsernameService")
	private UsernameService usernameService;
	
	@Inject
	private ComponentResources resources;
	
	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@Parameter(required=true, defaultPrefix = BindingConstants.PROP)
	private Long userId;
	
	@Parameter(value="false", required=false)
	private boolean disabled;
	
	@Parameter(required=false)
	private String target;
	
	void beginRender(MarkupWriter writer) throws ServiceException {
		if (userId == null) {
			return;
		}
		
		boolean renderLink = !disabled;

		if (renderLink) {
			Element a = writer.element("a");
			
			a.attribute("href", this.pageRenderLinkSource.createPageRenderLinkWithContext(UserDetails.class, userId).toURI());

			if (target != null) {
				a.attribute("target", target);
			}
			
			resources.renderInformalParameters(writer);
			
			a.text(usernameService.getUsername(userId));
		}
	}
	
	void afterRender(MarkupWriter writer) {
		if (userId == null) {
			return;
		}
		writer.end();
	}
}
