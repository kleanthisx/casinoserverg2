package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ExternalCallback;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.link.ExternalLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.CorporateSettingsBean;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.CorporateSettingsTableModel;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class CorporateSettings extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(CorporateSettings.class);
	
	@InjectPage("AddCorporateSettings")
	public abstract AddCorporateSettings getAddCorporateSettingsPage();
	
    @InjectPage("GeneralConfirmation")
    public abstract GeneralConfirmation getConfirmPage();

	@InjectPage("EditCorporateSettings")
	public abstract EditCorporateSettings getCorporateSettingsPage();

	@InitialValue("0")
	@Persist("client:page")
	public abstract void setUserID(Long userID);
	public abstract Long getUserID();


	public abstract void setUsername(String username);
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract CorporateSettingsBean getCurrSettings();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:corporateSettingsTableModel","columns=literal:!GameName, !MinBet, !MaxBet, !edit, !delete",
			"pageSize=20","rowsClass=ognl:beans.evenOdd.next","row=currSettings"})
	public abstract Table getSettings();
	
	public CorporateSettingsTableModel getCorporateSettingsTableModel(){
		return new CorporateSettingsTableModel(getUserID());
	}
	
	@Component(bindings={"listener=listener:editSettings","parameters={currSettings.gameID, userID, currSettings.gameName}"})
	public abstract DirectLink getEditLink();
	
	@Component(bindings={"listener=listener:addSettings","parameters={userID}"})
	public abstract DirectLink getAddLink();
	
	@Component(bindings={"page=literal:UserDetails","parameters=userID"})
	public abstract ExternalLink getBackLink();
	
	@Component(bindings={"listener=listener:deleteSettings","parameters={currSettings.gameID, currSettings.gameName, userID,username}"})
	public abstract DirectLink getDeleteLink();
	
	@Component(bindings={"value=currSettings.MinBet != null ? formatAmount(currSettings.MinBet) : ''"})
	public abstract Insert getMinBet();
	
	@Component(bindings={"value=currSettings.MaxBet != null ? formatAmount(currSettings.MaxBet) : ''"})
	public abstract Insert getMaxBet();
	
	public void editSettings(IRequestCycle cycle, int gameID, Long userID, String gameName){
		EditCorporateSettings settings = getCorporateSettingsPage();
		settings.setGameID(gameID);
		settings.setUserID(userID);
		settings.setGameName(gameName);
		cycle.activate(settings);
	}

	public void addSettings(IRequestCycle cycle, Long userID){
		AddCorporateSettings settings = getAddCorporateSettingsPage();
		settings.setUserID(userID);
		cycle.activate(settings);
	}

	public static class RemoveCorpSettingsConfirmContext extends ConfirmContext {
		private static final long serialVersionUID = -5124938004430525566L;
		private final Long userId;
		private final int gameId;
		
		public RemoveCorpSettingsConfirmContext(String okMsg, String cancelMsg,String confirmMsg, ICallback callback,Long userId,int gameId) {
			super(okMsg, cancelMsg, confirmMsg, callback);
			this.userId = userId;
			this.gameId = gameId;
		}

		@Override
		public void onOk(IRequestCycle cycle, ValidationDelegate delegate) throws ServiceException {
			 Connection dbConn = ConnectionFactory.getConnection();
		        PreparedStatement statement = null;
		        
		        if (dbConn == null){
		        	delegate.record(null, "No database connection");
					return;
				}
		        
		        try {            
		            String sql = 
		                "DELETE FROM corporate_game_settings" +
		                " WHERE corporate_id = ?" +
		                " AND game_id = ?";
		            
		                statement = dbConn.prepareStatement(sql);
		                statement.setLong(1, userId);
		                statement.setInt(2, gameId);
		                
		                if (statement.executeUpdate() == 1){
		                	 returnToPage(cycle);
			                 return;
		                }
		                
		        } catch (SQLException e) {
		            log.error("Error while removing user from category.",e);
		        } finally{
		            DbUtil.close(statement);
		            DbUtil.close(dbConn);
		        }
		        delegate.record(null, "Error while removing user from category");
		        return;
		}
		
	}
	
	public void deleteSettings(IRequestCycle cycle, int gameId, String gameName, Long userId,String username){
		ConfirmContext confirmContext = new RemoveCorpSettingsConfirmContext(this.getMsg("ok"), this.getMsg("cancel"), this.getMsg("confirmMessage") + " " + gameName + " setttings from corporate " + username + " ?",new ExternalCallback(this, new Object[] {userId}),userId,gameId);
		GeneralConfirmation confirmPage = getConfirmPage();
		confirmPage.setConfirmContext(confirmContext);
    	getRequestCycle().activate(confirmPage);
	}

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setUserID((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent evt){
		if (getUserID() == 0L) {
			throw new BadRequestException("Invalid/missing userId");
		}
		setUsername(getUsername(getUserID()));
	}
}

