package com.magneta.casino.backoffice.managers.reporting;

import java.io.OutputStream;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Future;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import com.magneta.casino.services.ServiceException;

public interface ReportService {
	
	ReportRequest createRequest(String reportFileName, ReportFormat format, TimeZone tz, Locale locale, Map<String, Object> params) throws JRException;
	ReportRequest createRequest(JasperReport report, ReportFormat format, TimeZone tz, Locale locale, Map<String, Object> params);

	void exportReport(ReportRequest request, OutputStream os) throws JRException, ServiceException;
	
	/**
	 * Executes the report asynchronously. Returns a URL to a serialized JasperPrint object.
	 * @param request
	 * @return
	 */
	Future<URL> executeReportAsync(ReportRequest request);
}
