package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.IFieldTracking;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.CorporateEarningsService;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.CorporateEarningsBean;
import com.magneta.casino.services.beans.CorporateEarningsTransactionBean;
import com.magneta.casino.services.beans.UserBalanceBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(and={PrivilegeEnum.FUNDS,PrivilegeEnum.CORPORATE})
public abstract class PayCorporate extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(PayCorporate.class);
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporatesService();
	
	@InjectObject("service:casino.common.CorporateEarnings")
	public abstract CorporateEarningsService getCorporateEarningsService();
	
	@Persist("client")
	public abstract ICallback getCallback();
	public abstract void setCallback(ICallback callback);

	@Override
	public boolean pageNeedsWriteAccess() {
		return true;
	}

	@Persist("client:page")
	public abstract Long getAffiliateID();
	public abstract void setAffiliateID(Long affiliateID);
	public abstract double getAmount();
	public abstract String getDetails();
	public abstract void setEarnings(double earnings);
	public abstract void setBalance(double balance);

	public abstract void setUsername(String username);
	public abstract String getUsername();

	public abstract double getEarnings();
	
	public abstract boolean getIsActive();
	public abstract void setIsActive(boolean isActive);

	@Bean(initializer="omitZero=true")
	public abstract SimpleNumberTranslator getNumTranslator();
	
	@Bean
	public abstract Min getMinAmount();
	
	@Bean
	public abstract Max getMaxAmount();
	
	public abstract IFieldTracking getCurrentFieldTracking();

	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Component(id="payForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getPayForm();
	
	@Component(id="earnings",
			bindings={"value=formatCurrency(earnings)"})
	public abstract Insert getEarningsField();
	
	@Component(id="balance",
			bindings={"value=formatCurrency(balance)"})
	public abstract Insert getBalanceField();
	
	@Component(id="username",
			bindings={"value=username"})
	public abstract Insert getUsernameField();
	
	@Component(id="amountLabel",
			bindings={"field=component:amount"})
	public abstract FieldLabel getAmountLabel();
	
	@Component(id="amount",
			bindings={"value=amount","disabled=!allowWrite()","displayName=message:Amount","translator=bean:numTranslator",
			"validators=validators:required,$minAmount,$maxAmount"})
	public abstract TextField getAmountField();
	
	@Component(id="detailsLabel",
			bindings={"field=component:details"})
	public abstract FieldLabel getDetailsLabel();
	
	@Component(id="details",
			bindings={"value=details","disabled=!allowWrite()","displayName=message:Details","validators=validators:required"})
	public abstract TextArea getDetailsField();
	
	@Component(id="affiliateID",
			bindings={"value=affiliateID"})
	public abstract Hidden getAffiliateIDField();
	
	@Component(id="okSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setAffiliateID((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent evt){
		if (getAffiliateID() == null || getAffiliateID() <= 0) {
			throw new BadRequestException("Invalid/missing Corporate Id");
		}
		
		try {
			CorporateEarningsBean earnings = getCorporateEarningsService().getEarnings(getAffiliateID());
			
			if(earnings == null) {
				return;
			}
			
			setEarnings(earnings.getEarnings());
			
		} catch (ServiceException e) {
			log.error("Error while getting affiliate details");
			return;
		}
		
		try {
			UserBalanceBean userBalance = getUserDetailsService().getUserBalance(getAffiliateID());
			UserDetailsBean userDetails = getUserDetailsService().getUser(getAffiliateID());
			
			if(userBalance == null || userDetails == null){
				return;
			}
			setBalance(userBalance.getBalance());
			setIsActive(userDetails.getIsActive());
			setUsername(userDetails.getUserName());
			
		} catch (ServiceException e) {
			log.error("Error while getting user details");
			return;
		}
		
		getMinAmount().setMin(getEarnings() < 0.0? getEarnings() : 0.0);
		getMaxAmount().setMax(getEarnings() > 0.0? getEarnings() : 0.0);
	}

	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		if (payAffiliate()) {
			if(getCallback() == null) {
				redirectToPage("CorporateAccounts");
			} else {
				getCallback().performCallback(cycle);
			}
		}
	}

	private boolean payAffiliate() {		
		if(!getIsActive()) {
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("account-deactivated-error"), null);
			return false;
		}
		
		CorporateEarningsTransactionBean transaction = new CorporateEarningsTransactionBean();
		transaction.setCorporateId(getAffiliateID());
		transaction.setAmount(getAmount());
		transaction.setDetails(getDetails());
		
		try {   
			this.getCorporateEarningsService().transferEarnings(transaction);
			return true;
		} catch (ServiceException e) {
			log.error("Error while inserting affiliate payment.",e);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
		}
		return false;
	}

	public void onCancel(IRequestCycle cycle){
		if( getCallback() == null) {
			redirectToPage("CorporateAccounts");
		} else {
			getCallback().performCallback(cycle);
		}
	}
}
