package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class WebLoginsReport extends DateReportOptions {

		@Component(id = "username",
				bindings = {"value=username", "disabled=false","displayName=message:Username"})
		public abstract TextField getUsernameField();

		@Component(id = "usernameLabel",
				bindings = {"field=component:username"})
		public abstract FieldLabel getUsernameLabel();
		
		@Component(id = "application",
				bindings = {"value=application","model=ognl:new com.magneta.casino.backoffice.t4.models.ApplicationSelectionModel(true,-1)", 
				"displayName=message:Application"})
		public abstract PropertySelection getApplicationSelection();

		@Component(id = "applicationLabel",
				bindings = {"field=component:application"})
		public abstract FieldLabel getApplicationLabel();
		
		public abstract String getUsername();
		public abstract String getApplication(); 
		
		@Override
		public void fillReportParameters(Map<String,Object> reportParams) {
			super.fillReportParameters(reportParams);

			reportParams.put("username", getUsername());
			if (!this.getApplication().equals("All")) {
				reportParams.put("application", getApplication());
			}
		}
}
