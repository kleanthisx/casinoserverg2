package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.PlayerGameBean;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.backoffice.t4.models.PlayerGamesTableModel;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.PROFILES)
public abstract class PlayerHistory extends SecurePage implements PageBeginRenderListener {
    
	private static final Logger log = LoggerFactory.getLogger(PlayerHistory.class);
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
    @Persist("client")
    public abstract void setUserID(Long userID);
    public abstract Long getUserID();
    
    public abstract void setUsername(String username);
    
    public abstract void setUserType(int userType);
    public abstract int getUserType();
    
    @InitialValue("0")
    @Persist("client:page")
    public abstract int getGameID();
    public abstract void setGameID(int gameID);
    
    @Bean
    public abstract EvenOdd getEvenOdd();

    public abstract PlayerGameBean getCurrRound();
    
    @Component(type="contrib:Table",
    		bindings={"source=ognl:playerGamesTableModel","columns=literal: GameId, Date, Game, !select",
    		"pageSize=30","row=currRound","initialSortColumn=literal:Date","initialSortOrder=false","rowsClass=ognl:beans.evenOdd.next",
    		"volatile=true","pagesDisplayed=20"})
    public abstract Table getPlayerHistory();
    
    public PlayerGamesTableModel getPlayerGamesTableModel(){
    	return new PlayerGamesTableModel(getUserID(), getTZ(), getLocale(), getGameID());
    }
    
    @Component(bindings={"listener=listener:onSubmit"})
    public abstract Form getGameSelectForm();
    
    @Component(bindings={"value=gameID","displayName=message:Game","model=ognl:gameSelectionModel"})
    public abstract PropertySelection getGame();
    
    public GamesSelectionModel getGameSelectionModel() {
    	return new GamesSelectionModel(false);
    }
    
    @Component(bindings={"field=component:game"})
    public abstract FieldLabel getGameLabel();
    
    @Component
    public abstract Submit getSearchSubmit();
    
    @Component(bindings={"page=literal:viewround","parameters={currRound.tableId, currRound.roundId}"})
    public abstract CasinoLink getSelectRound();
    
    @Component(id="username",
    		bindings={"value=username"})
    public abstract Insert getUsernameField();

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null) && (params.length > 0)) {
			setUserID((Long)params[0]);
			if (params.length > 1) {
				setGameID((Integer)params[1]);
			}
		}
	}
    
    @Override
	public void pageBeginRender(PageEvent pEvt) {
		if (getUserID() == null) {
			throw new BadRequestException("Invalid/missing userId");
		}
    	
    	try {
			UserDetailsBean user = getUserDetailsService().getUser(getUserID());
			
			if(user == null){
				throw new BadRequestException("Invalid userId");
			}
			this.setUserType(user.getUserType());
			this.setUsername(user.getUserName());
		} catch (ServiceException e) {
			log.error("Error while getting user details");
		}
    }

	public void onSubmit(){}
}
