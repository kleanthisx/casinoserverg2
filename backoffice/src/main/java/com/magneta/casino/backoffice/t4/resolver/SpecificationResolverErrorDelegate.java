/**
 * 
 */
package com.magneta.casino.backoffice.t4.resolver;

import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry.INamespace;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.resolver.ISpecificationResolverDelegate;
import org.apache.tapestry.spec.IComponentSpecification;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;

import com.magneta.casino.backoffice.util.LinkGenerator;

/**  
* Custom SpecificationResolverDelegate that throws a page redirect exception  
* when an application page cannot be found. This improves upon the normal  
* Tapestry action of raising an application runtime exception.  
*/  
public class SpecificationResolverErrorDelegate implements ISpecificationResolverDelegate {  
   
    /* The tapestry page for 404 errors */
    public static final String ERROR_PAGE = "Error404";  
 
    /*  
     * Throws a Page Redirect exception to report that a requested page  
     * specification could not be found. The target error page name is  
     * the value configured in the application specification file or the  
     * default given by DEFAULT_SPECIFICATION_ERROR_PAGE.  
     */  
    @Override
	public IComponentSpecification findPageSpecification(IRequestCycle cycle,  
            INamespace namespace, String simplePageName) {  

        WebRequest request = cycle.getInfrastructure().getRequest();
        WebResponse response = cycle.getInfrastructure().getResponse();
        
        /* Store information about the error in the request similar to Tomcat */  
        request.setAttribute("javax.servlet.error.status_code", new Integer(HttpServletResponse.SC_NOT_FOUND));  
        request.setAttribute("javax.servlet.error.message", "Unable to find the requested page");  
        request.setAttribute("javax.servlet.error.request_uri", request.getRequestURI());  
        request.setAttribute("javax.servlet.error.servlet_name", request.getServerName());  
        request.setAttribute("javax.servlet.error.exception_type", null);  
 
        /* Set the http response code to 404 */  
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);  
        
        String url = LinkGenerator.getPageUrl(ERROR_PAGE, request.getRequestURI());
		throw new RedirectException(url);
    }  
 
    @Override
	public IComponentSpecification findComponentSpecification(  
            IRequestCycle cycle, INamespace namespace, String type) {  
        
       return null;
    }  
}  