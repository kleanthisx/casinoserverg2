package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.hivemind.util.PropertyUtils;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.SystemProperties;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class CreateJackpot extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(CreateJackpot.class);
	
	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();
	
	public abstract double getInitialAmount();
	public abstract double getMaxAmount();

	public abstract Integer getPayTimes();
	public abstract void setPayTimes(Integer payTimes);

	public abstract double getContrib();
	public abstract int getDrawNumber();

	@Persist("client:page")
	public abstract void setGame(int game);
	public abstract int getGame();

	public abstract void setCorporate(Long corporate);
	public abstract Long getCorporate();

	public abstract void setMystery(boolean automatic);
	public abstract boolean isMystery();

	@InitialValue("false")
	public abstract void setBetTypeJackpot(boolean betTypeJackpot);
	public abstract boolean getBetTypeJackpot();

	@Persist("client:page")
	public abstract void setJackpotType(int jackpotType);
	public abstract int getJackpotType();

	public abstract double getMinPayout();
	public abstract double getMinJackpotBalance();
	
	public abstract boolean isMysteryJackpotAllowed();
	public abstract boolean isJackpotAllowed();
	
	public abstract void setMysteryJackpotAllowed(boolean allowed);
	public abstract void setJackpotAllowed(boolean allowed);
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
    @Bean(initializer="omitZero=false")
    public abstract SimpleNumberTranslator getNumZeroTranslator();
    
    @Bean(initializer="omitZero=true")
    public abstract SimpleNumberTranslator getNumTranslator();
    
    @Bean(initializer="min=1")
    public abstract Min getMinPayTimes();
    
    @Bean(initializer="min=0.01")
    public abstract Min getMinAmount();
    
    @Bean(initializer="min=0.00")
    public abstract Min getMinZeroAmount();
    
    @Component(id="createJackpotFrm",
    		bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
    public abstract Form getCreateJackpotFrm();
    
    @Component(id="game",
    		bindings={"value=game"})
    public abstract Hidden getGameHidden();
    
    @Component(id="corp",
    		bindings={"value=corporate"})
    public abstract Hidden getCorp();
    
    @Component(id="jackpotBetType",
    		bindings={"value=betTypeJackpot"})
    public abstract Hidden getJackpotBetType();
    
    @Component(id="jackpotType",
    		bindings={"value=jackpotType"})
    public abstract Hidden getJackpotTypeHidden();
    
    @Component(id="initialAmountLabel",
    		bindings={"field=component:initialAmount"})
    public abstract FieldLabel getInitialAmountLabel();
    
    @Component(id="initialAmount",
    		bindings={"value=initialAmount","displayName=message:InitialAmount","translator=bean:numZeroTranslator","validators=validators:required,$minZeroAmount",
    		"disabled=!allowWrite()"})
    public abstract TextField getInitialAmountField();
    
    @Component(id="maxAmountLabel",
    		bindings={"field=component:maxAmount"})
    public abstract FieldLabel getMaxAmountLabel();
    
    @Component(id="maxAmount",
    		bindings={"value=maxAmount","displayName=message:MaxAmount","translator=bean:numTranslator","disabled=!allowWrite()"})
    public abstract TextField getMaxAmountField();
    
    @Component(id="payTimesLabel",
    		bindings={"field=component:payTimes"})
    public abstract FieldLabel getPayTimesLabel();
    
    @Component(id="payTimes",
    		bindings={"value=payTimes","displayName=message:TimesToPay","translator=bean:numTranslator","validators=validators:$minPayTimes",
    		"disabled=jackpotType != 1 || (!allowWrite())"})
    public abstract TextField getPayTimesField();
    
    @Component(id="contribLabel",
    		bindings={"field=component:contrib"})
    public abstract FieldLabel getContribLabel();
    
    @Component(id="contrib",
    		bindings={"value=contrib","displayName=contribLabelMsg","translator=bean:numTranslator","validators=validators:required,$minAmount",
    		"disabled=!allowWrite()"})
    public abstract TextField getContribField();
    
    @Component(id="drawNumberLabel",
    		bindings={"field=component:drawNumber"})
    public abstract FieldLabel getDrawNumberLabel();
    
    @Component(id="drawNumber",
    		bindings={"value=drawNumber","displayName=message:DrawNumber","translator=bean:numTranslator","disabled=!allowWrite()",
    		"validators=validators:$minPayTimes"})
    public abstract TextField getDrawNumberField();
    
    @Component(id="mysteryCBLabel",
    		bindings={"field=component:mysteryCB"})
    public abstract FieldLabel getMysteryCBLabel();
    
    @Component(id="mysteryCB",
    		bindings={"value=mystery","displayName=message:Mystery","disabled=0 >= game || !allowWrite()"})
    public abstract Checkbox getMysteryCB();
    
    @Component(id="minPayoutLabel",
    		bindings={"field=component:minPayout"})
    public abstract FieldLabel getMinPayoutLabel();
	
    @Component(id="minPayout",
    		bindings={"value=minPayout","disabled=!allowWrite()","displayName=message:MinPayout","translator=bean:numTranslator"})
    public abstract TextField getMinPayoutField();
    
    @Component(id="minJackpotBalanceLabel",
    		bindings={"field=component:minJackpotBalance"})
    public abstract FieldLabel getMinJackpotBalanceLabel();
    
    @Component(id="minJackpotBalance",
    		bindings={"value=minJackpotBalance","disabled=!allowWrite()","displayName=message:MinJackpotBalance","translator=bean:numTranslator"})
    public abstract TextField getMinJackpotBalanceField();
    
	@Component(id="createSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getCreateSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	private void updateJackpotAllowed(Connection conn) {
		if (getJackpotType() != 1) {
			setMysteryJackpotAllowed(true);
      	   	setJackpotAllowed(false);
			return;
		}
		
		GameBean game;
		try {
			game = getGamesService().getGame(getGame());
	
		} catch (ServiceException e) {
			log.error("Unable to get game from service");
			return;
		}
		GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
		
		setMysteryJackpotAllowed(gameTemplate.supportsMysteryJackpot());
 	   	setJackpotAllowed(gameTemplate.supportsGameJackpot());
	}
	
	@Override
	public void pageBeginRender(PageEvent event){

		Connection conn = ConnectionFactory.getConnection();
		
		updateJackpotAllowed(conn);

		setMystery(isMysteryJackpotAllowed() && !isJackpotAllowed());
		
		try {
			if ((getJackpotType() == 3) && SystemProperties.globalCorporateJackpot()){

				Statement configStmt = null;
				ResultSet configRs = null;

				try {
					configStmt = conn.createStatement();
					configRs = configStmt.executeQuery(
							"SELECT initial_amount, bet_contrib, maximum_amount, draw_number, min_payout, min_jackpot_balance" +
							" FROM jackpot_configs" +
					" WHERE config_id = 0");
					if (configRs.next()){
						PropertyUtils.write(this, "initialAmount", configRs.getDouble("initial_amount"));
						PropertyUtils.write(this, "contrib", configRs.getDouble("bet_contrib"));
						PropertyUtils.write(this, "maxAmount", configRs.getDouble("maximum_amount"));
						PropertyUtils.write(this, "drawNumber", configRs.getInt("draw_number"));
						PropertyUtils.write(this, "minPayout", configRs.getDouble("min_payout"));
						PropertyUtils.write(this, "minJackpotBalance", configRs.getDouble("min_jackpot_balance"));
					}
				} catch (SQLException e) {
					throw new RuntimeException(e);
				} finally {
					DbUtil.close(configRs);
					DbUtil.close(configStmt);
					DbUtil.close(conn);
				}

			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}finally {
			DbUtil.close(conn);
		}
	}

	@Override
	public void pageValidate(PageEvent event) {
		super.pageValidate(event);
		if (!getAccessValidator().checkWriteAccess(this.getPageName())){
			redirectToPage("AccessDenied");
		} else if (getJackpotType() <= 0){
			redirectToPage("JackpotTypeSelect");
		}
	}

	public void onSubmit(IRequestCycle cycle) throws ServiceException {

		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		
		Connection myConn = ConnectionFactory.getConnection();
		if (myConn == null) {
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
			return;
		}
		
		try {
			updateJackpotAllowed(myConn);
		} finally {
			DbUtil.close(myConn);
		}

		if (getGame() <= 0 || (isMysteryJackpotAllowed() && !isJackpotAllowed())) {
			setMystery(true);
		}
		
		if (delegate.getHasErrors()){
			return;
		} else if (isMystery() && getDrawNumber() <= 0){
			delegate.setFormComponent((IFormComponent) this.getComponent("drawNumber"));
			delegate.record(getMsg("draw-num-required"),null);
			return;
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null){
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
			return;
		}

		try {

			conn.setAutoCommit(false);

			/* Global corporate jackpot configuration creation */
			if ((getJackpotType() == 3) && SystemProperties.globalCorporateJackpot()){
				Statement configStmt = null;
				ResultSet configRs = null;
				boolean configExists = false;
				double initialAmount = 0.0;
				double betContrib = 0.0;
				double maxAmount = 0.0;
				int drawNumber = -1;
				double minPayout = 0.0;
				double minJackpotBalance = 0.0;

				try {
					configStmt = conn.createStatement();
					configRs = configStmt.executeQuery(
							"SELECT initial_amount, bet_contrib, maximum_amount, draw_number, min_payout, min_jackpot_balance" +
							" FROM jackpot_configs" +
					" WHERE config_id = 0");
					if (configRs.next()){
						configExists = true;
						initialAmount = configRs.getDouble("initial_amount");
						betContrib = configRs.getDouble("bet_contrib");
						maxAmount = configRs.getDouble("maximum_amount");
						drawNumber = configRs.getInt("draw_number");
						minPayout = configRs.getDouble("min_payout");
						minJackpotBalance = configRs.getDouble("min_jackpot_balance");
					}
				} finally {
					DbUtil.close(configRs);
					DbUtil.close(configStmt);
				}

				if (!configExists){
					PreparedStatement configInsertStmt = null;
					try {
						configInsertStmt = conn.prepareStatement(
								"INSERT INTO jackpot_configs(initial_amount, bet_contrib, maximum_amount, draw_number, config_id)" +
						"VALUES (?,?,?,?,0)");
						configInsertStmt.setBigDecimal(1, new BigDecimal(getInitialAmount()));
						configInsertStmt.setBigDecimal(2, new BigDecimal(getContrib()));
						if (getMaxAmount() == 0.0){
							configInsertStmt.setObject(3, null);
						} else {
							configInsertStmt.setBigDecimal(3, new BigDecimal(getMaxAmount()));
						}
						configInsertStmt.setInt(4, getDrawNumber());
						configInsertStmt.execute();

						initialAmount = getInitialAmount();
						betContrib = getContrib();
						maxAmount = getMaxAmount();
						drawNumber = getDrawNumber();
						minPayout = getMinPayout();
						minJackpotBalance = getMinJackpotBalance();
					} finally {
						DbUtil.close(configInsertStmt);
					}
				}

				List<Long> corporates = new ArrayList<Long>();

				Statement corporatesStmt = null;
				ResultSet corporatesRs = null;
				try {
					corporatesStmt = conn.createStatement();
					corporatesRs = corporatesStmt.executeQuery(         		
							"SELECT affiliate_id"+
							" FROM affiliates"+
							" LEFT OUTER JOIN jackpots ON jackpots.target_corporate = affiliates.affiliate_id"+
							" AND jackpots.finish_date IS NULL"+
							" WHERE approved = TRUE" +
					" AND jackpots.jackpot_id IS NULL");
					while (corporatesRs.next()){
						corporates.add(corporatesRs.getLong("affiliate_id"));
					}
				} finally {
					DbUtil.close(corporatesRs);
					DbUtil.close(corporatesStmt);
				}

				Statement idStmt = null;
				ResultSet idRs = null;
				PreparedStatement jackpotStmt = null;
				PreparedStatement transactionStmt = null;

				for (Long corporate :corporates){
					int jackpotId = -1;
					try {
						idStmt = conn.createStatement();
						idRs = idStmt.executeQuery("SELECT nextval('jackpot_id_seq')");

						if (idRs.next()){
							jackpotId = idRs.getInt(1);
						}
					} finally {
						DbUtil.close(idStmt);
						DbUtil.close(idRs);
					}

					try {
						jackpotStmt = conn.prepareStatement(
								"INSERT INTO jackpots(jackpot_id, game_id, target_corporate, initial_amount, maximum_amount, pay_count, auto, config_id, draw_number,"+
								(getBetTypeJackpot() ?"bet_amount, " : "bet_contrib, ")+"min_payout, min_jackpot_balance)"+
						" VALUES (?,?,?,?,?,?,?,0,?,?,?,?)");

						jackpotStmt.setInt(1, jackpotId);

						jackpotStmt.setObject(2, null);

						jackpotStmt.setLong(3, corporate);

						jackpotStmt.setBigDecimal(4, new BigDecimal(initialAmount));

						if (maxAmount == 0.0){
							jackpotStmt.setObject(5, null);
						} else {
							jackpotStmt.setBigDecimal(5, new BigDecimal(maxAmount));
						}

						jackpotStmt.setObject(6, null);

						jackpotStmt.setBoolean(7, !isMystery());

						jackpotStmt.setInt(8, drawNumber);

						jackpotStmt.setBigDecimal(9, new BigDecimal(betContrib));

						if (minPayout > 0.0){
							jackpotStmt.setBigDecimal(10, new BigDecimal(minPayout));
						} else {
							jackpotStmt.setObject(10, null);
						}
						if (minJackpotBalance > 0.0){
							jackpotStmt.setBigDecimal(11, new BigDecimal(minJackpotBalance));
						} else {
							jackpotStmt.setObject(11, null);
						}

						jackpotStmt.execute();
					} finally {
						DbUtil.close(jackpotStmt);
					}

					if (getInitialAmount() > 0.0){
						try{
							transactionStmt = conn.prepareStatement(
									"INSERT INTO jackpot_transactions(jackpot_id, amount)" +
							" VALUES (?,?)");
							transactionStmt.setInt(1, jackpotId);
							transactionStmt.setBigDecimal(2, new BigDecimal(getInitialAmount()));
							transactionStmt.execute();
						} finally {
							DbUtil.close(transactionStmt);
						}
					}
				}
			} else {
				Statement idStmt = null;
				ResultSet idRs = null;
				int jackpotId = -1;
				try {
					idStmt = conn.createStatement();
					idRs = idStmt.executeQuery("SELECT nextval('jackpot_id_seq')");

					if (idRs.next()){
						jackpotId = idRs.getInt(1);
					}
				} finally {
					DbUtil.close(idStmt);
					DbUtil.close(idRs);
				}

				PreparedStatement jackpotStmt = null;

				try{
					jackpotStmt = conn.prepareStatement(
							"INSERT INTO jackpots(jackpot_id, game_id, target_corporate, initial_amount, maximum_amount, pay_count, auto, draw_number,"+
							(getBetTypeJackpot() ?"bet_amount, " : "bet_contrib, ")+"min_payout, min_jackpot_balance)"+
					" VALUES (?,?,?,?,?,?,?,?,?,?,?)");

					jackpotStmt.setInt(1, jackpotId);

					if (getJackpotType() == 1){
						jackpotStmt.setInt(2, getGame());
					} else {
						jackpotStmt.setObject(2, null);
					}

					if (getJackpotType() == 3 && (!SystemProperties.globalCorporateJackpot())){
						jackpotStmt.setLong(3, getCorporate());
					} else {
						jackpotStmt.setObject(3, null);
					}

					jackpotStmt.setBigDecimal(4, new BigDecimal(getInitialAmount()));

					if (getMaxAmount() == 0.0){
						jackpotStmt.setObject(5, null);
					} else {
						jackpotStmt.setBigDecimal(5, new BigDecimal(getMaxAmount()));
					}

					if (getPayTimes() != null && getPayTimes() > 0){
						jackpotStmt.setInt(6, getPayTimes());
					} else {
						jackpotStmt.setObject(6, null);
					}

					jackpotStmt.setBoolean(7, (!isMystery()));

					if (isMystery()){
						jackpotStmt.setInt(8, getDrawNumber());
					} else {
						jackpotStmt.setObject(8, null);
					}

					jackpotStmt.setBigDecimal(9, new BigDecimal(getContrib()));
					
					if (getMinPayout() > 0.0){
						jackpotStmt.setBigDecimal(10, new BigDecimal(getMinPayout()));
					} else {
						jackpotStmt.setObject(10, null);
					}
					if (getMinJackpotBalance() > 0.0){
						jackpotStmt.setBigDecimal(11, new BigDecimal(getMinJackpotBalance()));
					} else {
						jackpotStmt.setObject(11, null);
					}

					jackpotStmt.execute();
				} finally {
					DbUtil.close(jackpotStmt);
				}

				if (getInitialAmount() > 0.0){
					PreparedStatement transactionStmt = null;
					try{
						transactionStmt = conn.prepareStatement(
								"INSERT INTO jackpot_transactions(jackpot_id, amount)" +
						" VALUES (?,?)");
						transactionStmt.setInt(1, jackpotId);
						transactionStmt.setBigDecimal(2, new BigDecimal(getInitialAmount()));
						transactionStmt.execute();
					} finally {
						DbUtil.close(transactionStmt);
					}

				}
			}
			conn.commit();

			redirectToPage("Jackpots");
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				log.info("Error while rolling back", e1);
			}
			log.warn("Error while creating jackpot.", e);

			if (e.getMessage().contains("unique_open_jackpot")){
				delegate.setFormComponent(null);
				delegate.record(getMsg("jackpot-exists-error"), null);
			} else {
				delegate.setFormComponent(null);
				delegate.record(getMsg("jackpot-create-error"), null);
			}
		} finally {
			DbUtil.close(conn);
		}
	}

	public void onCancel(IRequestCycle cycle){
		redirectToPage("JackpotGameSelect");
	}

	public String getContribLabelMsg(){
		if (getBetTypeJackpot()){
			return getMessages().getMessage("BetAmount");
		} 
		
		return getMessages().getMessage("ContributionRate");
	}
}