package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.link.ExternalLink;

import com.magneta.administration.beans.AffiliateBean;
import com.magneta.administration.commons.models.CountrySelectionModel;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.AffiliatesTableModel;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(and={PrivilegeEnum.FUNDS,PrivilegeEnum.CORPORATE})
public abstract class CorporateAccounts extends SecurePage implements PageBeginRenderListener{
	
	@InjectPage("CorporateAccounts")
	public abstract CorporateAccounts getCorporatesPage();
	
	@Bean 
	public abstract EvenOdd getEvenOdd();
	
	@Component(type="contrib:Table",
			bindings={"source=ognl:affiliatesTableModel",
			"columns=corpColumns","pageSize=30","rowsClass=ognl:beans.evenOdd.next","initialSortColumn=literal:Username","row=currAffiliate","initialSortOrder=true"})
	public abstract Table getCorporates();
	
	public AffiliatesTableModel getAffiliatesTableModel() {
		return new AffiliatesTableModel(getShowIsInactive(), true, true, getSuperCorp(), getUsernameSearch(), getEmail(), getCountry());
	}

	@Component(bindings={"value=showIsInactive"})
	public abstract Checkbox getShowInactive();
	
	@Component(bindings={"listener=listener:onSubmit"})
	public abstract Form getAffForm();
	
	@Component(bindings={"value=superCorp"})
	public abstract Hidden getSuperCorporate();
	
	@Component(bindings={"value=superCorpUsername"})
	public abstract Hidden getSuperCorpUsernameField();
	
	@Component(bindings={"value=usernameSearch"})
	public abstract TextField getUsernameField();
	
	@Component(bindings={"value=email"})
	public abstract TextField getEmailField();
	
	@Component(bindings={"value=country","model=ognl:countrySelectionModel","validators=validators:minLength=2"})
	public abstract PropertySelection getCountryField();
	
	public CountrySelectionModel getCountrySelectionModel() throws ServiceException{
		return new CountrySelectionModel();
	}
	
	@Component
	public abstract Submit getUpdateSubmit();
	
	@Component(bindings={"value=(superCorp > 0 ? superCorpUsername+' '+getMsg('subcorporate-accounts') : getMsg('page-title'))"})
	public abstract Insert getTitleMsg();
	
	@Component(bindings={"value=formatDate(currAffiliate.LastPayment)"})
	public abstract Insert getLastPayment();
	
	@Component(bindings={"value=formatAmount(currAffiliate.affiliateBalance)"})
	public abstract Insert getAffiliateBalance();
	
	@Component(bindings={"value=formatAmount(currAffiliate.Deposit)"})
	public abstract Insert getAffiliateDeposit();
	
	@Component(bindings={"value=formatPercentage(currAffiliate.affiliateRate)"})
	public abstract Insert getAffiliateRate();
	
	@Component(bindings={"value=message:TransferToBalance","renderTag=currAffiliate.paymentDue"})
	public abstract Insert getTransfer2BalanceMsg();
	
	@Component(bindings={"page=literal:PayCorporate","parameters=currAffiliate.Id"})
	public abstract ExternalLink getTransfer2BalanceLink();
	
	@Component(bindings={"userId=ognl:currAffiliate.Id","target=literal:_blank"})
	public abstract UserLink getUsernameLink();
	
	@Component(bindings={"page=literal:CreateSubPartner","parameters={superCorp}"})
	public abstract ExternalLink getCreateSubLink();
	
	@Component(bindings={"page=literal:EditPartner","parameters=currAffiliate.Id"})
	public abstract ExternalLink getEditLink();
	
	@Component(bindings={"page=literal:CorporateAccounts","parameters=currAffiliate.Id"})
	public abstract ExternalLink getViewSubsLink();
	
	@Component(bindings={"page=literal:PartnerUsers","parameters=currAffiliate.Id"})
	public abstract ExternalLink getViewUsersLink();
	
	@Component(bindings={"page=literal:CreateSubPartner"})
	public abstract CasinoLink getCreateCorpLink();
	
	@Component(bindings={"page=literal:PartnerChanges","parameters={currAffiliate.Id}"})
	public abstract ExternalLink getChangeLink();

	public abstract AffiliateBean getCurrAffiliate();
	
	public abstract String getCode();

	@InitialValue("0")
	@Persist("client:page")
	public abstract void setSuperCorp(Long superCorp);
	public abstract Long getSuperCorp();
	
	public abstract void setSuperCorpUsername(String superCorpUsername);
	
	@Persist("client:page")
	@InitialValue("false")
	public abstract boolean getShowIsInactive();
	public abstract void setShowIsInactive(boolean showIsInactive);
	
	@Persist("client:page")
	public abstract String getUsernameSearch();
	public abstract void setUsernameSearch(String usernameSearch);
	
	@Persist("client:page")
	public abstract String getEmail();
	public abstract void setEmail(String email);
	
	@Persist("client:page")
	public abstract String getCountry();
	public abstract void setCountry(String country);
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setSuperCorp((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent evt){
		setSuperCorpUsername(getUsername(getSuperCorp()));		
	}
	
	public String getCorpColumns() {
		String columns = "Username,  Deposit, AffiliateBalance, AffiliateRate, !PaymentPeriod, !LastPayment,";
		if(hasWriteAccess("EditPartner")) {
			columns += ", !edit";
		}
		if(hasWriteAccess("PayCorporate")) {
			columns += ", !transferEarnings";
		}
		columns += ", !subs, !users, !changes";
		return columns;
	}
	
	public void onSubmit() {	
	}
}