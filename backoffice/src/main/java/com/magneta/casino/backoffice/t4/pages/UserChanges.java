package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.UserAccountChangesBean;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.UserAccountChangesTableModel;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(and={PrivilegeEnum.ROLE,PrivilegeEnum.PROFILES})
public abstract class UserChanges extends SecurePage implements PageBeginRenderListener{

	private static final Logger log = LoggerFactory.getLogger(UserChanges.class);
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getUserDetailsService();
	
	@Bean
	public abstract ValidationDelegate getDelegate();

	public abstract Date getMaxDate();

	@InitialValue("0")
	@Persist("client:page")
	public abstract void setUserId(Long userId);
	public abstract Long getUserId();

	public abstract void setUsername(String username);

	@Persist("client:page")
	public abstract void setUserType(int userType);
	public abstract int getUserType();

	public abstract UserAccountChangesBean getCurrChange();

	@Component(type="contrib:Table",
			bindings={"source=ognl:userAccountChangesTableModel","columns=literal:ChangeTime, ChangeUser, !Username, !Password, !Email, !IsActive, !DateOfBirth, !FirstName, !LastName, !MiddleName, !Country, !Phone, !Region, !Town, !PostalCode, !Nickname, !TimeZone, !UserType, !PostalAddress, !DateDeactivated, !Closed",
			"pageSize=20","initialSortColumn=literal:ChangeTime","row=currChange","initialSortOrder=false"})
			public abstract Table getLogTable();

	public UserAccountChangesTableModel getUserAccountChangesTableModel(){
		return new UserAccountChangesTableModel(getUserId());
	}
	
	@Component(bindings={"value=formatDate(currChange.changeTime)"})
	public abstract Insert getChangeTime();

	@Component(id="username",
			bindings={"value=currChange.oldUsername+' '+getMsg('ChangedTo')+' '+currChange.newUsername",
			"renderTag=currChange.oldUsername == currChange.newUsername","style='display:none;'"})
			public abstract Insert getUsernameField();

	@Component(bindings={"value=message:Changed","renderTag=currChange.oldPasswordHash == currChange.newPasswordHash and currChange.oldPasswordSalt == currChange.newPasswordSalt",
	"style='display:none;'"})
	public abstract Insert getPassword();

	@Component(bindings={"value=(currChange.oldEmail != null ? currChange.oldEmail : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newEmail != null ? currChange.newEmail : 'Null')",
			"renderTag=currChange.oldEmail == currChange.newEmail","style='display:none;'"})
			public abstract Insert getEmail();

	@Component(id="isActive",
			bindings={"value=currChange.oldIsActive+' '+getMsg('ChangedTo')+' '+currChange.newIsActive","renderTag=currChange.oldIsActive == currChange.newIsActive",
	"style='display:none;'"})
	public abstract Insert getIsActivate();

	@Component(bindings={"value=(currChange.oldFirstName != null ?currChange.oldFirstName : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newFirstName != null ? currChange.newFirstName : 'Null')",
			"renderTag=currChange.oldFirstName == currChange.newFirstName","style='display:none;'"})
			public abstract Insert getFirstName();

	@Component(bindings={"value=(currChange.oldLastName != null ?currChange.oldLastName : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newLastName != null ? currChange.newLastName : 'Null')",
			"renderTag=currChange.oldLastName == currChange.newLastName","style='display:none;'"})
			public abstract Insert getLastName();

	@Component(bindings={"value=(currChange.oldMiddleName != null? currChange.oldMiddleName : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newMiddleName != null? currChange.newMiddleName : 'Null')",
			"renderTag=currChange.oldMiddleName == currChange.newMiddleName","style='display:none;'"})
			public abstract Insert getMiddleName();

	@Component(bindings={"value=(currChange.oldCountry != null ? currChange.oldCountry : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newCountry != null ? currChange.newCountry : 'Null')",
			"renderTag=currChange.oldCountry == currChange.newCountry","style='display:none;'"})
			public abstract Insert getCountry();

	@Component(bindings={"value=(currChange.oldPhone != null ? currChange.oldPhone : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newPhone != null ? currChange.newPhone : 'Null')",
			"renderTag=currChange.oldPhone == currChange.newPhone","style='display:none;'"})
			public abstract Insert getPhone();

	@Component(bindings={"value=(currChange.oldRegion != null? currChange.oldRegion: 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newRegion != null? currChange.newRegion: 'Null')",
			"renderTag=currChange.oldRegion == currChange.newRegion","style='display:none;'"})
			public abstract Insert getRegion();

	@Component(bindings={"value=(currChange.oldTown ? currChange.oldTown : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newTown ? currChange.newTown : 'Null')",
			"renderTag=currChange.oldTown == currChange.newTown","style='display:none;'"})
			public abstract Insert getTown();

	@Component(bindings={"value=(currChange.oldPostalCode != null ? currChange.oldPostalCode : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newPostalCode != null ? currChange.newPostalCode : 'Null')",
			"renderTag=currChange.oldPostalCode == currChange.newPostalCode","style='display:none;'"})
			public abstract Insert getPostalCode();

	@Component(bindings={"value=(currChange.oldNickname != null ? currChange.oldNickname : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newNickname != null ? currChange.newNickname : 'Null')",
			"renderTag=currChange.oldNickname == currChange.newNickname","style='display:none;'"})
			public abstract Insert getNickname();

	@Component(id="userType",
			bindings={"value=currChange.oldUserTypeString+' '+getMsg('ChangedTo')+' '+currChange.newUserTypeString","renderTag=currChange.oldUserType == currChange.newUserType",
	"style='display:none;'"})
	public abstract Insert getUserTypeField();

	@Component(bindings={"value=(currChange.oldPostalAddress != null? currChange.oldPostalAddress : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newPostalAddress != null? currChange.newPostalAddress : 'Null')",
			"renderTag=currChange.oldPostalAddress == currChange.newPostalAddress","style='display:none;'"})
			public abstract Insert getPostalAddress();

	@Component(bindings={"value=(currChange.oldDateDeactivated != null ? formatDate(currChange.oldDateDeactivated) : 'Null')+' '+getMsg('ChangedTo')+' '+(currChange.newDateDeactivated != null ? formatDate(currChange.newDateDeactivated) : 'Null')",
			"renderTag=currChange.oldDateDeactivated == currChange.newDateDeactivated","style='display:none;'"})
			public abstract Insert getDateDeactivated();

	@Component(bindings={"value=currChange.oldClosed+' '+getMsg('ChangedTo')+' '+currChange.newClosed","renderTag=currChange.oldClosed == currChange.newClosed",
	"style='display:none;'"})
	public abstract Insert getClosed();

	@Component(bindings={"listener=listener:onSubmit","delegate=beans.delegate","clientValidationEnabled=true"})
			public abstract Form getClearForm();

	@Component(id="maxDate",
			bindings={"value=maxDate","displayName=message:clear-msg","translator=translator:date,pattern=dd/MM/yyyy",
			"validators=validators:required","disabled=!allowWrite()"})
			public abstract DatePicker getMaxDateField();

	@Component(bindings={"field=component:maxDate"})
	public abstract FieldLabel getMaxDateLabel();

	@Component(bindings={"value=currChange.oldTimeZone+' '+getMsg('ChangedTo')+' '+currChange.newTimeZone","renderTag=currChange.oldTimeZone == currChange.newTimeZone",
	"style='display:none;'"})
	public abstract Insert getTimeZone();

	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();

	@Override
	public void pageBeginRender(PageEvent pEvt) {
		if (getUserId() == null || getUserId() <= 0) {
			throw new BadRequestException("Invalid/missing userId");
		}
		
		try {
			UserDetailsBean userBean = getUserDetailsService().getUser(getUserId());
			
			if(userBean == null){
				return;
			}

			this.setUserType(userBean.getUserType());
			this.setUsername(userBean.getUserName());

		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	public void onSubmit(IRequestCycle cycle){
		if (getDelegate().getHasErrors()){
			return;
		}

		Date maxDate = normalizeDate(getMaxDate());
		int offset = TimeZone.getDefault().getOffset(maxDate.getTime());
		Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
		calendar.setTimeInMillis(maxDate.getTime()+offset);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.add(Calendar.MILLISECOND, 0);
		maxDate = new Date(calendar.getTimeInMillis());

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(
					"DELETE FROM user_changes" +
					" WHERE ((change_time AT TIME ZONE 'UTC') <= ?::timestamp)" +
			" AND user_id = ?");


			stmt.setTimestamp(1, new Timestamp(getMaxDate().getTime()));
			stmt.setLong(2, getUserId());
			stmt.execute();

			redirectToPage("UserChanges");
		} catch (SQLException e){
			getDelegate().record(null, getMsg("db-error"));
			log.error("Error while getting user info.",e);
		}
		finally{
			DbUtil.close(conn);
		}
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setUserId((Long)params[0]);
		}
	}
}
