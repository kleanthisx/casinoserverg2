package com.magneta.casino.backoffice.t4.models;

import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.t4.utils.TableSortContext;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.beans.ServiceResultSet;

public class PaymentTypeTableModel implements IBasicTableModel{

	private final PaymentTypeService paymentTypeService = ServiceLocator.getService(PaymentTypeService.class);

	@Override
	public Iterator<PaymentTypeBean> getCurrentPageRows(int nFirst, int nPageSize,ITableColumn objSortColumn, boolean bSortOrder) {

		ServiceResultSet<PaymentTypeBean> paymentTypes = null;
		try {
			paymentTypes = paymentTypeService.getPaymentTypes(null, nPageSize, nFirst, new TableSortContext<PaymentTypeBean>(objSortColumn, bSortOrder));
			if(paymentTypes == null){
				return null;
			}
		} catch (ServiceException e) {
			throw new RuntimeException("Error while getting payment types", e);
		}
		return paymentTypes.getResult().iterator();
	}

	@Override
	public int getRowCount() {
		int count = 0;
		try {
			count = paymentTypeService.getPaymentTypesSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException("Error while getting payment types count", e);
		}	
		return count;
	}
}
