package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ExternalCallback;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.link.ExternalLink;
import org.apache.tapestry.link.PageLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.RoleBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.models.RolesTableModel;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class RoleAdministration extends SecurePage {

	private static final Logger log = LoggerFactory.getLogger(RoleAdministration.class);
	
	@InjectPage("EditRoleUsers")
	public abstract EditRoleUsers getUsersEditPage();

	@InjectPage("EditRole")
	public abstract EditRole getEditRolePage();

	@InjectPage("GeneralConfirmation")
	public abstract GeneralConfirmation getConfirmPage();

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract RoleBean getCurrRole();

	@Component(type="contrib:Table",
			bindings={"source=ognl:rolesTableModel","columns=columns","pageSize=20",
			"row=currRole","rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getRolesTable();

	public RolesTableModel getRolesTableModel(){
		return new RolesTableModel();
	}
	
	@Component(bindings={"listener=listener:deleteRole","parameters={currRole.roleId,currRole.roleDesc}"})
	public abstract DirectLink getDeleteRole();

	@Component(bindings={"listener=listener:editRole","parameters=currRole"})
	public abstract DirectLink getEditRole();

	@Component(bindings={"page=literal:EditRoleUsers","parameters={currRole.roleId}"})
	public abstract ExternalLink getEditUsers();

	@Component(bindings={"page=literal:CreateRole"})
	public abstract PageLink getNewRole();

	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	public static class DeleteRoleConfirmContext extends ConfirmContext {

		private static final long serialVersionUID = 771174225525823457L;
		private final int roleId;
		
		public DeleteRoleConfirmContext(String okMsg, String cancelMsg,String confirmMsg, ICallback callback,int roleId) {
			super(okMsg, cancelMsg, confirmMsg, callback);
			this.roleId = roleId;
		}

		@Override
		public void onOk(IRequestCycle cycle, ValidationDelegate delegate) throws ServiceException {
			Connection dbConn = ConnectionFactory.getConnection();
			PreparedStatement statement = null;

			if (dbConn == null){
				delegate.record(null, "No database connection");
				return;
			}

			try {            
				String sql = 
					"DELETE FROM roles" +
					" WHERE role_id = ?";

				statement = dbConn.prepareStatement(sql);
				statement.setInt(1, roleId);

				if (statement.executeUpdate() == 1){
					returnToPage(cycle);
				} else {
					delegate.record(null, "Role not found");
				}

			} catch (SQLException e) {
				delegate.record(null, "Database error");
				log.error("Error while removing role.",e);
			} finally{
				DbUtil.close(statement);
				DbUtil.close(dbConn);
			}
		}
	}
	public void deleteRole(IRequestCycle cycle,int roleId,String roleDesc){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		
		if (!getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true)) {
			delegate.record(null, this.getMsg("no-access"));
			return;
		}
		
		ConfirmContext confirmContext = new DeleteRoleConfirmContext(this.getMsg("ok"), this.getMsg("cancel"), this.getMsg("confirmMessage") + " " + roleDesc + " ?",new ExternalCallback(this, null),roleId);
		GeneralConfirmation confirmPage = getConfirmPage();
		confirmPage.setConfirmContext(confirmContext);
		getRequestCycle().activate(confirmPage);
	}

	public void editRole(IRequestCycle cycle){
		Object[] params = cycle.getListenerParameters();
		RoleBean role = (RoleBean)params[0];

		EditRole roleEditPage = getEditRolePage();
		roleEditPage.setRoleID(role.getRoleId());
		roleEditPage.setRoleDesc(role.getRoleDesc());

		cycle.activate(roleEditPage);
	}

	public String getColumns() {
		String columns =  "!RoleDesc";

		if (getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true)) {
			columns += ", !delete, !edit, !editUsers";
		}
		return columns;
	}
}
