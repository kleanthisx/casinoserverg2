package com.magneta.casino.backoffice.t4.services;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.security.PermitAll;

import org.apache.tapestry.IPage;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.services.Infrastructure;
import org.apache.tapestry5.ioc.util.UnknownValueException;
import org.apache.tapestry5.services.ComponentClassResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.annotations.RequiredSetting;
import com.magneta.casino.backoffice.services.T5ServiceLocator;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.beans.SettingBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;

/**
 * This static class holds all the access privileges to the pages.
 */
public class PageMenuItemAccessValidatorImpl implements PageMenuItemAccessValidator {

	private static final Logger log = LoggerFactory.getLogger(PageMenuItemAccessValidatorImpl.class);

	private static final Object[] ALLOW_ALL = new Object[] {};
	private static final Object[] DENY_ALL = new Object[] {};

	private final Map<String, Object[]> pagePrivileges;
	private final SettingsService settingsService;
	private final PrincipalService principalService;

	public PageMenuItemAccessValidatorImpl(SettingsService settingsService, PrincipalService principalService) {
		this.settingsService = settingsService;
		this.principalService = principalService;
		pagePrivileges = new ConcurrentHashMap<String, Object[]>();
	}

	private Annotation getSecurityAnnotation(Class<?> pageKlass) {
		RequiredPrivilege allow = null;

		for (Class<?> c = pageKlass; c != null; c = c.getSuperclass()) {
			PermitAll permitAll = c.getAnnotation(PermitAll.class);

			if (permitAll != null) {
				return permitAll;
			}

			allow = c.getAnnotation(RequiredPrivilege.class);

			if (allow != null) {
				return allow;
			}
		}

		return null;
	}

	private RequiredSetting getRequiredSettingAnnotation(Class<?> pageKlass) {
		RequiredSetting setting = null;

		for (Class<?> c = pageKlass; c != null; c = c.getSuperclass()) {
			setting = c.getAnnotation(RequiredSetting.class);

			if (setting != null) {
				return setting;
			}
		}

		return null;
	}

	private Class<?> getPageClass(String page) {
		ComponentClassResolver resolver = T5ServiceLocator.getService(ComponentClassResolver.class);

		try {
			String className = resolver.resolvePageNameToClassName(page);
			return Class.forName(className);

		} catch (UnknownValueException e) {

		} catch (ClassNotFoundException e) {
			log.error("Unable to find class for page " + page, e);
			return null;
		}

		Infrastructure infra = ServiceLocator.getService(Infrastructure.class);	
		IPage iPage = null;

		try {
			iPage = infra.getRequestCycle().getPage(page);
		} catch (RedirectException e) {
			iPage = null;
		}

		if (iPage == null) {
			return null;
		}

		return iPage.getClass();
	}

	private Object[] calculatePageAccess(Class<?> pageKlass) throws ServiceException {
		log.debug("Checking page class " + pageKlass.getName());

		RequiredSetting settingAnnotation = getRequiredSettingAnnotation(pageKlass);
		if (settingAnnotation != null) {

			String value = settingAnnotation.val().trim();

			SettingBean setting = settingsService.getSetting(settingAnnotation.value());
			if (setting == null || setting.getValue() == null) {
				if (!value.isEmpty()) {
					return DENY_ALL;
				}
			} else {
				if (!setting.getValue().trim().toLowerCase().equals(value.toLowerCase())) {
					return DENY_ALL;
				}
			}
		}

		Annotation securityAnnotation = getSecurityAnnotation(pageKlass);

		if (securityAnnotation == null) {
			return DENY_ALL;
		} else if (securityAnnotation instanceof PermitAll) {
			return ALLOW_ALL;
		} else if (securityAnnotation instanceof RequiredPrivilege) {
			RequiredPrivilege allow = (RequiredPrivilege) securityAnnotation;

			if (allow.and() != null && allow.and().length > 0) {
				return allow.and();
			} else if (allow.or() != null && allow.or().length > 0) {
				return new Object[]{allow.or()};
			} else {
				return new Object[] {allow.value()};
			}
		} else {
			throw new RuntimeException("Unsupported security annotation " + securityAnnotation);
		}
	}

	private boolean isLoggedIn(ExtendedPrincipalBean user) {
		return user != null && user.getUserId() != null && user.getUserId() > 0;
	}

	private boolean checkUserPageAccess(String page, ExtendedPrincipalBean user, boolean write) throws ServiceException{
		if (page == null || "".equals(page)) {
			return true;
		}

		Object[] privs = pagePrivileges.get(page);
		if (privs == null) {
			Class<?> pageKlass = getPageClass(page);

			if (pageKlass != null) {
				privs = calculatePageAccess(pageKlass);
			}

			if (privs != null) {
				pagePrivileges.put(page, privs);
			}
		}


		if (privs == null || privs == DENY_ALL) {
			return false;
		}

		if (privs == ALLOW_ALL) {
			return true;
		}

		if (!isLoggedIn(user)) {
			return false;
		}

		for (Object priv: privs){
			if (priv instanceof PrivilegeEnum) {
				if (!user.hasPrivilege((PrivilegeEnum)priv, write)) {
					return false;
				}
			} else if (priv instanceof PrivilegeEnum[]) {
				PrivilegeEnum[] oredPrivs = (PrivilegeEnum[])priv;
				boolean hasOne = false;

				if (oredPrivs.length == 0) {
					hasOne = true;
				}

				for (PrivilegeEnum myPriv: oredPrivs) {
					if (user.hasPrivilege(myPriv, false)){
						hasOne = true;
						break;
					}
				}

				if (!hasOne)
					return false;
			}
		}
		return true;
	}



	public boolean checkUserAccess(String page, ExtendedPrincipalBean user) {
		try {
			return checkUserPageAccess(page, user, false);
		} catch (ServiceException e) {
			log.error("Unable to calculate user access", e);
			return false;
		}
	}

	@Override
	public boolean checkWriteAccess(String page) {
		try {
			return checkUserPageAccess(page, principalService.getPrincipal(), true);
		} catch (ServiceException e) {
			log.error("Unable to calculate user access", e);
			return false;
		}
	}

	@Override
	public boolean checkAccess(String page) {		
		return checkUserAccess(page, principalService.getPrincipal());
	}

	@Override
	public boolean checkSpecial(String special) {
		if (special == null)
			return true;

		if (special.equals("logout")) {
			return principalService.getPrincipal() != null && isLoggedIn(principalService.getPrincipal());
		}

		return true;
	}
}
