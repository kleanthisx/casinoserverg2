/**
 * 
 */
package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.PlayerGameBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class PlayerGamesTableModel implements IBasicTableModel{

    private Long id;
    private TimeZone timeZone;
    private Locale locale;
    private int gameId;
    
    public PlayerGamesTableModel(Long id, TimeZone timeZone, Locale locale, int gameId) {
        this.id = id;
        this.timeZone = timeZone;
        this.locale = locale;
        this.gameId = gameId;
    }
    
    @Override
    public Iterator<PlayerGameBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        
        if (gameId <= 0)
            return new ArrayList<PlayerGameBean>().iterator();
        
        GamesService gamesService = ServiceLocator.getService(GamesService.class);
        GameBean game;
        
        try {
            game = gamesService.getGame(gameId);
            if(game == null){
            	throw new RuntimeException("Unable to find game " + gameId);
            }
        } catch (ServiceException e) {
        	throw new RuntimeException("Unable to find game " + gameId, e);
        }
        
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<PlayerGameBean> it = null;
        
        if (dbConn == null){
            throw new RuntimeException("Out of database connections");
        }

        try
        {
            String sortColumnName = "rounds.date_opened";
            
            if (sortCol != null)
                sortColumnName = sortCol.getColumnName();
            
            if (("Date").equals(sortColumnName)) {
                sortColumnName = "rounds.date_closed";
            } else if (("TableNo").equals(sortColumnName)) {
                sortColumnName = "tables.table_id";
            } else if (("GameId").equals(sortColumnName)) {
                sortColumnName = "tables.table_id, rounds.round_id";
            } else {
                sortColumnName = null;
            }
            
            String sql = 
                "SELECT tables.table_id, rounds.round_id, rounds.date_opened," +
                " rounds.date_closed"+
                " FROM game_tables AS tables"+
                " INNER JOIN game_rounds AS rounds ON tables.table_id = rounds.table_id"+
                " WHERE tables.table_owner = ?" +
                " AND tables.game_type_id = ?";

                if (sortColumnName != null){
                    sql += " ORDER BY "+sortColumnName;
                    
                    if (sortAsc)
                        sql += " ASC";
                    else
                        sql += " DESC";
                }
                
                sql += " LIMIT ? OFFSET ?";
            
                statement = dbConn.prepareStatement(sql);
                int i = 1;
                statement.setLong(i++, id);
                statement.setInt(i++, gameId);
                statement.setInt(i++,limit);
                statement.setInt(i,offset);

                res = statement.executeQuery();
                
                List<PlayerGameBean> myArr = new ArrayList<PlayerGameBean>();
                PlayerGameBean currRow;
                
                while (res.next()){
                    currRow = new PlayerGameBean();
                    
                    currRow.setDate(FormatUtils.getFormattedDate(DbUtil.getTimestamp(res, "date_closed"),timeZone,locale));
                    currRow.setGame(game.getName());
                    currRow.setTableId(res.getLong("table_id"));
                    currRow.setRoundId(res.getInt("round_id"));
                    
                    myArr.add(currRow);
                }
                
                it = myArr.iterator();
                
        }catch (SQLException e) {
        	throw new RuntimeException("Error while retrieving players history list.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
    public int getRowCount() {
        if (gameId <= 0) {
            return 0;
        }
        
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
            return 0;
        }
        
        try
        {
            String sql = 
                "SELECT SUM(game_table_last_rounds.last_round_id + 1)" +
                " FROM game_tables" +
                " INNER JOIN game_table_last_rounds ON game_tables.table_id = game_table_last_rounds.table_id" +
                " WHERE game_tables.table_owner=?"+
                " AND game_tables.game_type_id = ?";
            
            statement = dbConn.prepareStatement(sql);
            statement.setLong(1, id);
            statement.setInt(2, gameId);
            
            res = statement.executeQuery();
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        }catch (SQLException e) {
            throw new RuntimeException("Error while retrieving players history list count.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }
}
