package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.bingo.BingoActionFilter;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.bingo.BingoConfiguration;


@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class BingoCreateConfig {

	@Component(id="create")
	private Form create;

	@Inject
	private GameConfigsService gameConfigService;
	
	@Inject
	private PageRenderLinkSource linkSource;

	@Inject
	private Messages messages;

	@Inject
	private GamesService gameService;

	@Property
	private Integer gameId;

	@Property
	private Long configId;

	@Property
	private GameComboBean combo;

	@Property
	private GameComboBean[] paytable;

	@Inject
	private GameActionFilterService gameActionFilterService;

	@Property
	private BingoConfiguration config;
	@Property
	private String configComment;

	GameConfigBean conf;

	private GameConfigBean origConfig;

	@OnEvent(value=EventConstants.PREPARE_FOR_RENDER)
	public void prepare() throws ServiceException{
		origConfig = gameConfigService.getGameConfig(gameId, configId);
		config =  new BingoConfiguration(origConfig.getConfigId(),origConfig.getConfigValue());
		configComment = origConfig.getConfigComment();
		paytable = ((BingoActionFilter)getActionFilter()).getPaytable();
		conf = new GameConfigBean();
		conf.setGameId(origConfig.getGameId());

	}

	@Cached
	public ActionFilter getActionFilter() throws ServiceException {
		return gameActionFilterService.getFilter(gameId, config);
	}



	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Integer gameId, Long configId) {
		this.gameId = gameId;
		this.configId = configId;
	}

	@OnEvent(EventConstants.PASSIVATE)
	public Object[] onPassivate() {
		return new Object[]{gameId, configId};
	}

	@OnEvent(value=EventConstants.SUBMIT, component="create")
	public Object onSubmit() throws ServiceException {

		conf.setConfigComment(configComment);
		conf.setConfigValue(serialize());
		gameConfigService.insertConfig(conf);
		
		return linkSource.createPageRenderLinkWithContext(GameConfigs.class, gameId);
	}

	@Cached
	public String getPageTitle() throws ServiceException {
		return messages.format("page-title", gameService.getGame(gameId).getName());
	}

	@Cached
	public GameConfiguration getConfiguration() throws ServiceException {
		return gameConfigService.getFilteredConfiguration(gameId, configId);
	}


	public String serialize() {
		StringBuilder sb = new StringBuilder();

		int counter = 0;
		for(GameComboBean combo : paytable) {
			//Mean amount class;
			Double value = combo.getPayout();
			sb.append(Integer.toString(counter)).append("=").append(value).append(":");
			counter++;
		}

		return sb.toString();
	}

	public ComboEncoder getComboEncoder(){
		return new ComboEncoder();
	}

	private class ComboEncoder implements ValueEncoder<GameComboBean>{

		@Override
		public String toClient(GameComboBean arg0) {
			return (Double.toString(arg0.getPayout()));
		}

		@Override
		public GameComboBean toValue(String arg0) {
			return new GameComboBean(null, Double.parseDouble(arg0));
		}

	}

}
