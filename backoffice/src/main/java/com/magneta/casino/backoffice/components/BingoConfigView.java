package com.magneta.casino.backoffice.components;

import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.pages.GameImage;
import com.magneta.casino.common.games.filters.ActionFilter;
import com.magneta.casino.common.games.filters.beans.ComboSymbolBean;
import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.casino.common.games.filters.bingo.BingoActionFilter;
import com.magneta.casino.services.GameActionFilterService;
import com.magneta.casino.services.ServiceException;
import com.magneta.games.configuration.bingo.BingoConfiguration;

public class BingoConfigView {

	@Inject
	private GameActionFilterService gameActionFilterService;
	
	@Inject
	private PageRenderLinkSource linkSource;
		
	@Parameter(required=true)
	private Integer gameId;
	
	@Parameter(required=true)
	private BingoConfiguration configuration;

	@Cached
	public ActionFilter getActionFilter() throws ServiceException {
		return gameActionFilterService.getFilter(gameId, configuration);
	}
	
	@Property
	private ComboSymbolBean currSymbol;
	
	@Property
	private GameComboBean combo;
	
	@Cached
	public GameComboBean[] getPaytable() throws ServiceException {
		if (this.getActionFilter() instanceof BingoActionFilter) {
			return ((BingoActionFilter)getActionFilter()).getPaytable();
		}
		return null;
	}
	
	public String getComboImageUrl() {
		return linkSource.createPageRenderLinkWithContext(GameImage.class, gameId, currSymbol.getImage()).toURI();
	}
}
