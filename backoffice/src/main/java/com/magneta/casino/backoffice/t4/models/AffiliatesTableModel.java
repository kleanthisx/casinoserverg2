package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.AffiliateBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class AffiliatesTableModel implements IBasicTableModel {
	
    private boolean showApproved;
    private boolean showOnlyApproved;
    private Long superCorp;
    private boolean showInactive;
    private String corpEmail;
    private String corpUsername;
    private String corpCountry;
    
    /**
     * @param showApproved
     * @param superCorp The id of the super affiliate of the list of affiliates to show. A value of 0 or null will show only top
     *  level affiliates, while a negative value will show all affiliates.
     */
    public AffiliatesTableModel(boolean showInactive, boolean showApproved, Long superCorp){
    	this(showInactive, showApproved, showApproved, superCorp, null,null,null);
    }
    
    public AffiliatesTableModel(boolean showInactive, boolean showApproved, boolean showOnlyApproved,Long superCorp, String username, String email, String country) {
    	
    	if (superCorp == null){
    		superCorp = 0L;
    	}
    	
        this.showApproved = showApproved;
        this.showOnlyApproved = showOnlyApproved;
        this.superCorp = superCorp;
        this.showInactive = showInactive;
        this.corpUsername = username;
        this.corpEmail = email;
        this.corpCountry = country;
    }
    
    @Override
	public Iterator<AffiliateBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<AffiliateBean> it = null;
        
        if (dbConn == null){
			return null;
		}
        
        try
        {   
            String sortColumn = sortCol.getColumnName();
            
            String sql = 
                "SELECT users.user_id, username, country, users.is_active, email, first_name AS firstName, last_name AS lastName, register_date, affiliate_rate AS affiliateRate,"+
            	" affiliate_earnings.earnings as affiliateBalance, approved, user_balance.balance AS deposit, payment_period AS paymentPeriod," +
            	" MAX(transaction_date) as lastPayment, (now_utc() - (COALESCE(MAX(transaction_date),register_date) + INTERVAL '1 day') > payment_period AND earnings > 0.0) as payment_due," +
            	" affiliate_users.affiliate_id AS super_affiliate, " +
            	" (CASE WHEN(SELECT COUNT(affiliate_id) FROM affiliate_users WHERE users.user_id = affiliate_users.affiliate_id AND affiliation = 3) > 0 THEN TRUE ELSE FALSE END) AS has_sub_corps,"+
            	" (CASE WHEN(SELECT COUNT(affiliate_id) FROM affiliate_users WHERE users.user_id = affiliate_users.affiliate_id AND affiliation = 1) > 0 THEN TRUE ELSE FALSE END) AS has_users"+
            	" FROM affiliates"+
            	" INNER JOIN affiliate_earnings ON affiliates.affiliate_id=affiliate_earnings.affiliate_id" +
            	" LEFT OUTER JOIN affiliate_earnings_transactions ON affiliate_earnings_transactions.affiliate_id = affiliates.affiliate_id"+
            	" INNER JOIN users ON affiliates.affiliate_id = users.user_id" +(showInactive? "" : " AND users.is_active = TRUE")+
            	" INNER JOIN user_balance ON users.user_id = user_balance.user_id" +
            	" LEFT OUTER JOIN affiliate_users ON affiliate_users.user_id = affiliates.affiliate_id";
            
             	sql += " WHERE 1=1";
            		
            	if (superCorp == 0){
            		sql += " AND affiliate_users.affiliate_id IS NULL";
            	} else if (superCorp > 0){
            		sql += " AND affiliate_users.affiliate_id = ?";
            	}
        
            	if (!showApproved){
            		sql += " AND approved = FALSE";
            	} else if (showOnlyApproved) {
            		sql += " AND approved = TRUE"; 
            	}
            	
            	if (corpUsername != null){
            		sql += " AND users.username = ?";
            	}
            	
            	if(corpEmail != null){
            		sql += " AND users.email = ?";
            	}
            	
            	if(corpCountry != null){
            		sql +=" AND users.country = ?";
            	}
            	
            	sql += 
            		" GROUP BY users.user_id, username, country, users.is_active, email, firstName, lastName, register_date, affiliateRate,"+
            		" affiliateBalance, approved, deposit, paymentPeriod, " +
            		" super_affiliate";
            	
            	if (sortColumn != null){
            		sql += " ORDER BY "+sortColumn;

            		if (sortAsc){
            			sql += " ASC ";
            		} else {
            			sql += " DESC ";
            		}
            		sql += "LIMIT ? OFFSET ?";
            		
            	}
            	else{
            		sql += "ORDER BY username ASC LIMIT ? OFFSET ?";
            	}
                statement = dbConn.prepareStatement(sql);
                int i = 1;
                if (superCorp > 0){
                	statement.setLong(i++, superCorp);
            	}
                if (corpUsername != null){
            		statement.setString(i++, corpUsername);
            	}
                if(corpEmail != null){
                	statement.setString(i++, corpEmail);
                }
                if(corpCountry != null){
                	statement.setString(i++, corpCountry);
                }
                statement.setInt(i++, limit);
                statement.setInt(i, offset);
                
                res = statement.executeQuery();
                
                ArrayList<AffiliateBean> myArr = new ArrayList<AffiliateBean>();
                AffiliateBean currRow;
                
                while (res.next()){
                    currRow = new AffiliateBean();
                    
                    currRow.setId(res.getLong("user_id"));
                    currRow.setUsername(res.getString("username"));
                    currRow.setEmail(res.getString("email"));
                    currRow.setFirstName(res.getString("firstName"));
                    currRow.setLastName(res.getString("lastName"));
                    currRow.setRegisterDate(DbUtil.getTimestamp(res, "register_date"));
                    currRow.setAffiliateRate(res.getDouble("affiliateRate"));
                    currRow.setAffiliateBalance(res.getDouble("affiliateBalance"));
                    currRow.setApproved(res.getBoolean("approved"));
                    
                    currRow.setDeposit(res.getDouble("deposit"));
                    
                    currRow.setPaymentPeriod(res.getString("paymentPeriod"));
                    
                    currRow.setLastPayment(DbUtil.getTimestamp(res, "lastPayment"));
                    currRow.setPaymentDue(res.getBoolean("payment_due"));
                    currRow.setSuperAffiliate(res.getLong("super_affiliate"));
                    currRow.setActive(res.getBoolean("is_active"));
                    
                    currRow.setHasUsers(res.getBoolean("has_users"));
                    currRow.setHasSubCorps(res.getBoolean("has_sub_corps"));
                    
                    myArr.add(currRow);
                }
               
                it = myArr.iterator();
               
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving affiliates list.",e);
        } finally {
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
        int count = 0;
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {            
            String sql = 
                "SELECT count(*)"+
                " FROM affiliates" +
                " INNER JOIN users ON affiliates.affiliate_id = users.user_id" +(showInactive? "" : " AND users.is_active = TRUE") +
                " LEFT OUTER JOIN affiliate_users ON affiliate_users.user_id = affiliates.affiliate_id";
                
            	sql += " WHERE 1=1";
	        	
	        	if (superCorp == 0){
            		sql += " AND affiliate_users.affiliate_id IS NULL";
            	} else if (superCorp > 0){
            		sql += " AND affiliate_users.affiliate_id = ?";
            	}
	        	
	        	if (!showApproved){
	        		sql += " AND approved = FALSE";
	        	} else if (showOnlyApproved) {
	        		sql += " AND approved = TRUE"; 
	        	}
	        	
	        	if (corpUsername != null){
	        		sql += " AND users.username = ?";
	        	}
	        	
	        	if(corpEmail != null){
	        		sql += " AND users.email = ?";
	        	}
	        	
	        	if(corpCountry != null){
	        		sql += " AND users.country = ?";
	        	}
	        	
                statement = dbConn.prepareStatement(sql);
                int i = 1;

                if (superCorp > 0) {
                	statement.setLong(i++, superCorp);
            	}

                if(corpUsername != null){
                	statement.setString(i++, corpUsername);
                }
                if(corpEmail != null){
                	statement.setString(i++, corpEmail);
                }
                if(corpCountry != null){
                	statement.setString(i++, corpCountry);
                }
                
                res = statement.executeQuery();
                
                if (res.next()){
                    count = res.getInt(1);
                }
        } catch (SQLException e) {
            throw new RuntimeException("Error while retrieving affiliates count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }  
}
