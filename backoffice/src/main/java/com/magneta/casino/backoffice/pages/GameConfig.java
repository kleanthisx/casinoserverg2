package com.magneta.casino.backoffice.pages;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class GameConfig {

	@Inject
	private GameConfigsService gameConfigService;
	
	private static class GameConfigStreamResponse implements StreamResponse {

		private final InputStream is;
		private final Integer gameId;
		private final Long configId;
		
		public GameConfigStreamResponse(InputStream is, Integer gameId, Long configId) {
			this.is = is;
			this.gameId = gameId;
			this.configId = configId;
		}
		
		@Override
		public String getContentType() {
			return "application/xml;charset=utf-8";
		}

		@Override
		public InputStream getStream() throws IOException {
			return is;
		}

		@Override
		public void prepareResponse(Response response) {
			response.setHeader("Content-Disposition","inline; filename="+"game_" + gameId + "_" + configId +".xml");			
		}
		
	}
	
	@OnEvent(EventConstants.ACTIVATE)
	public Object activate(Integer gameId, Long configId) throws ServiceException, JAXBException {
		GameConfigBean gameConfig = gameConfigService.getGameConfig(gameId, configId);
		
		if (gameConfig == null) {
			return Error404.class;
		}
		
		JAXBContext context = JAXBContext.newInstance(gameConfig.getClass());
        Marshaller marshaller = context.createMarshaller();
        
        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		marshaller.marshal(gameConfig, outputStream);

		return new GameConfigStreamResponse(new ByteArrayInputStream(outputStream.toByteArray()), gameId, configId);
	}
}
