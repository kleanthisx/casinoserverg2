package com.magneta.casino.backoffice.components;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

public class Chart {
	@Inject
	private ComponentResources resources;

	@Parameter(required=true)
	private JFreeChart chart;

	@Persist
	private JFreeChart context;

	@Parameter(required=true)
	private int width;

	@Parameter(required=true)
	private int height;

	void beginRender(MarkupWriter writer) {
		context= chart;
		Object[] params = { new Integer(width),
				new Integer(height)};

		//generate action link
		Link link = resources.createEventLink("chart", params);
		writer.element("img", "src", link);
		resources.renderInformalParameters(writer);
	}

	void afterRender(MarkupWriter writer) {
		writer.end();
	}

	public StreamResponse onChart(final int width, final int height/*, Object ...rest*/) {


		return new StreamResponse(){
			@Override
			public String getContentType(){
				return "image/png";
			}
			@Override
			public InputStream getStream() throws IOException {
				ByteArrayOutputStream byteArray = new ByteArrayOutputStream() ;
				ChartUtilities.writeChartAsPNG(byteArray, context, width, height) ;
				return new ByteArrayInputStream(byteArray.toByteArray());
			}
			@Override
			public void prepareResponse(Response response){}
		};
	}}