package com.magneta.casino.backoffice.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.GameClientSelectModel;
import com.magneta.casino.backoffice.valueencoders.GameClientEncoder;
import com.magneta.casino.services.CorporateSystemSettingsService;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.search.SortContext;

@RequiredPrivilege(PrivilegeEnum.CORPORATE)
public class CorporateGameClients {
	
	@Inject
	private Logger log;
	
	@Inject
	private GameClientService gameClientsService;
	
	@Inject
	private AlertManager alertManager;
	
	@Inject
	private CorporateSystemSettingsService corporateSystemSettingsService;
	
	@InjectComponent
	private Form clientsForm;
	
	@Property
	private List<GameClientBean> clients;
	
	@Property
	private Long corporateId;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long corporateId) {
		this.corporateId = corporateId;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Long passivate() {
		return corporateId;
	}
	
	@Cached(watch="corporateId")
	public List<GameClientBean> getClientList() throws ServiceException {
		return gameClientsService.getGameClients(null, 0, 0, new SortContext<GameClientBean>("clientDescription")).getResult();
	}
	
	public GameClientEncoder getGameClientsEncoder() throws ServiceException {
		return new GameClientEncoder(getClientList());
	}
	
	public GameClientSelectModel getGameClientsModel() throws ServiceException {
		return new GameClientSelectModel(getClientList());
	}
	
	@OnEvent(value=EventConstants.PREPARE, component="clientsForm")
	public void prepareForm() throws ServiceException {
		clients = new ArrayList<GameClientBean>();
		
		String clientString = corporateSystemSettingsService.getSetting(corporateId, "game.clients");
		if (clientString != null && !clientString.isEmpty()) {
			
			List<GameClientBean> clientList = getClientList();
			String[] cIds = clientString.split(" ");
			
			for (String cId: cIds) {
				Integer clientId = Integer.valueOf(cId);
				GameClientBean currClient = null;
				
				for (GameClientBean client: clientList) {
					if (client.getClientId().equals(clientId)) {
						currClient = client;
						break;
					}
				}

				if (currClient == null) {
					throw new RuntimeException("Found invalid client id in corporate list");
				}
				
				clients.add(currClient);
			}
		}
	}
	
	@OnEvent(value=EventConstants.SUCCESS, component="clientsForm")
	public void formSuccess() {
		
		System.out.println("clients size is " + clients.size());
		
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		
		for (GameClientBean client: clients) {
			if (first) {
				first = false;
			} else {
				sb.append(' ');
			}
				
			sb.append(client.getClientId());
		}
		
		String value = sb.toString();
		
		if (value.isEmpty()) {
			value = null;
		}
		
		try {
			corporateSystemSettingsService.setSetting(corporateId, "game.clients", value);
			alertManager.info("Corporate Client list updated");
		} catch (ServiceException e) {
			log.error("Unable to update corporate client list", e);
			alertManager.error("Corporate client list not saved");
		}
	}
}
