package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.search.SortContext;

public class GameConfigsGridDataSource extends GenericGridDataSource<GameConfigBean> {

	private final GameConfigsService gameConfigService;
	private final Integer gameId;
	
	public GameConfigsGridDataSource(Integer gameId, GameConfigsService gameConfigService) {
		this.gameId = gameId;
		this.gameConfigService = gameConfigService;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return gameConfigService.getGameConfigsSize(gameId, null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<GameConfigBean> getData(int limit, int offset,
			SortContext<GameConfigBean> sortContext) throws Exception {
		return gameConfigService.getGameConfigs(gameId, null, sortContext, limit, offset).getResult();
	}
}
