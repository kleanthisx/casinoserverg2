package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;

import com.magneta.administration.beans.GameClientGameBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.GameClientGamesTableModel;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class GameClientGames extends SecurePage implements PageBeginRenderListener {
	
	@InjectObject("service:casino.common.GameClient")
	public abstract GameClientService getGameClientService();
	
	@Persist("client:page")
	public abstract void setGameClientId(int gameClientId);
	public abstract int getGameClientId();
	
	public abstract void setGameClientDesc(String clientDesc);
	
	@Bean
	public abstract EvenOdd getEvenOdd();
	
	public abstract GameClientGameBean getCurrGame();
	
	@Component(id="clientDesc",
			bindings={"value=gameClientDesc"})
	public abstract Insert getClientDesc();
	
	@Component(id="gamesTable", type="contrib:Table",
			bindings={"source=ognl:gameClientGamesTableModel","columns=literal: !GameName, !enabled",
			"pageSize=20","row=currGame","rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getGamesTable();
	
	public GameClientGamesTableModel getGameClientGamesTableModel(){
		return new GameClientGamesTableModel(getGameClientId());
	}
	
	@Component(id="gameEnableCB",
			bindings={"value=currGame.enabled","disabled=!allowWrite()"})
	public abstract Checkbox getGameEnableCB();
	
	@Component(id="saveSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getSaveSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Component(id="gamesForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel"})
	public abstract Form getGamesForm();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if (params != null && params.length > 0) {
			this.setGameClientId((Integer)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent event) {
		try {
			GameClientBean client = this.getGameClientService().getGameClient(this.getGameClientId());
			this.setGameClientDesc(client.getClientDescription());
			
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void onSubmit(IRequestCycle cycle){
		//redirectToPage("GameClients");
	}
	
	public void onCancel(IRequestCycle cycle){
		redirectToPage("GameClients");
	}
}
