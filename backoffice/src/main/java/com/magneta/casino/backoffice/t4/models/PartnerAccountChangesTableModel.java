package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;
import com.magneta.administration.beans.PartnerAccountChangeBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.casino.common.utils.FormatUtils;

public class PartnerAccountChangesTableModel implements IBasicTableModel {
	
	private Long corporateId;
	
	public PartnerAccountChangesTableModel(Long corporateId) {
		this.corporateId = corporateId;
	}
	
	@Override
	public Iterator<PartnerAccountChangeBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<PartnerAccountChangeBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumnName = "Username";

        if (sortCol != null){
            sortColumnName = sortCol.getColumnName();
        }
        
        if (("ChangeUser").equals(sortColumnName))
            sortColumnName = "change_user";
        else
            sortColumnName = "affiliate_changes.change_time";
		
		try {
			String sql = 
				" SELECT change_time, affiliate_user.username, field_name, old_value, new_value, users.username AS change_user"+
				" FROM affiliate_changes"+
				" INNER JOIN users AS affiliate_user ON affiliate_changes.affiliate_id = affiliate_user.user_id"+
				" LEFT OUTER JOIN users ON affiliate_changes.modifier_user = users.user_id" +
				" WHERE affiliate_changes.affiliate_id = ?" +
				" ORDER BY "+sortColumnName+(sortAsc? " ASC": " DESC")+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			int i = 1;
			statement.setLong(i++, corporateId);

			statement.setInt(i++, limit);
			statement.setInt(i, offset);

			res = statement.executeQuery();

			ArrayList<PartnerAccountChangeBean> myArr = new ArrayList<PartnerAccountChangeBean>();
			PartnerAccountChangeBean currRow;

			while (res.next()){
				currRow = new PartnerAccountChangeBean();
				currRow.setUsername(res.getString("username"));
				currRow.setChangeUser(res.getString("change_user"));
				currRow.setChangeTime(res.getTimestamp("change_time"));
				currRow.setField(res.getString("field_name"));
				String field = res.getString("field_name");
				String oldValue = res.getString("old_value");
				String newValue = res.getString("new_value");
				if ("affiliate_rate".equals(field) || "free_credits_rate".equals(field)){
					if (oldValue != null){
						currRow.setOldValue(FormatUtils.getFormattedAmount(Double.parseDouble(oldValue) * 100.0)+" %");
					}
					if (newValue != null){
						currRow.setNewValue(new String(FormatUtils.getFormattedAmount(Double.parseDouble(newValue) * 100.0)+" %"));
					}
				} else if ("credit_limit".equals(field)){
					if (oldValue != null){
						currRow.setOldValue(new String(FormatUtils.getFormattedAmount(Double.parseDouble(oldValue))));
					}
					if (newValue != null){
						currRow.setNewValue(new String(FormatUtils.getFormattedAmount(Double.parseDouble(newValue))));
					}
				} else {
					currRow.setOldValue(oldValue);
					currRow.setNewValue(newValue);
				}
				
				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving affiliate changes.",e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try {
            String sql = 
                "SELECT COUNT(*)"+
            	" FROM affiliate_changes" +
				" WHERE affiliate_changes.affiliate_id = ?";
            statement = dbConn.prepareStatement(sql);
            
            int i = 1;

			statement.setLong(i, corporateId);
            
            res = statement.executeQuery();
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting affiliate changes count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
