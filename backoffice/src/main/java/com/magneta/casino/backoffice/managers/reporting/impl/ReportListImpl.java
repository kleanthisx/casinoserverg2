package com.magneta.casino.backoffice.managers.reporting.impl;

import java.util.ArrayList;
import java.util.List;

import com.magneta.administration.commons.beans.ReportBean;
import com.magneta.casino.backoffice.managers.reporting.ReportList;
import com.magneta.casino.services.ServiceException;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;

public class ReportListImpl implements ReportList {

	public static final String FINANCIAL = "Financial";
	public static final String GAMING = "Gaming";
	public static final String OTHER = "Other";

	private static final String[] CATEGORIES = {FINANCIAL, GAMING, OTHER};

	private static final ReportBean[] REPORTS = {
		new ReportBean("users", "Users Report",
				"Prints the personal information of the selected user(s).",
				"reporting/UsersReportOptions", OTHER),

		new ReportBean("user_statements", "User Statements Report",
				"Prints statements for the selected user(s).",
				"reporting/UserStatementsReportOptions", FINANCIAL),
				
		new ReportBean("full_statements",
				"Full User Statements Report",
				"Prints statements for the selected user(s) which include transactions initiated by betting and winning in games.",
				"reporting/FullUserStatementsReportOptions",
				FINANCIAL, true),
		
				new ReportBean("payout_rates_per_game",
						"Payout Rates Per Game Report",	
						"Prints the total input and output amounts and rates for each game.",
						"reporting/PayoutRatesPerGameReportOptions",
						GAMING, true),

				new ReportBean("user_payout_rates",
						"Payout Rates Per User Report",	
						"Prints the total input and output amounts and rates for the selected user.",
						"reporting/UserPayoutRatesReportOptions",
						GAMING, true),

				new ReportBean("config_payout_rates",
						"Payout Rates Per Configuration Report",	
						"Prints the total input and output amounts and rates for each each game subdivided by configuration.",
						"reporting/PayoutRatesReportOptions",
						GAMING, true),

				new ReportBean("user_logins",
						"User Logins Report", 	
						"Prints a detailed report of the system logins.", 					
						"reporting/UserLoginsReportOptions",OTHER),
				new ReportBean("financial_statement",
						"Financial Statement Report", 	
						"Prints the in and out amounts for the entire system for the period given.", 					
						"reporting/FinancialStatementReportOptions", FINANCIAL),

				new ReportBean("jackpot_statement",
						"Jackpot Statement",
						"Prints the total contribution,balance and wins.",
						"reporting/JackpotStatement",GAMING),
						
				new ReportBean("winners",
						"Winners Report", 	
						"Prints a list of the players who have won over a specified amount during the specified period.", 					
						"reporting/WinnersReportOptions",
						GAMING, true),
						
				new ReportBean("duplicate_account",
						"Duplicate Accounts Report", 	
						"Prints a list of the users who have a set of similar personal details which include: the first name, middle name, last name, address, town, region, postal code and country.", 					
						"reporting/ReportOptions", OTHER),
				new ReportBean("inactive_users",
						"Inactive Users Report", 	
						"Prints a list of the users who have been inactive for 2 or more years.", 					
						"reporting/InactiveUsersReport",OTHER),
				new ReportBean("staff_users",
						"Staff Users Report", 	
						"Prints a list of the staff users who belong to the specified role.", 					
						"reporting/StaffUsersReportOptions",OTHER),
				new ReportBean("inactive_staff_users",
						"Inactive Staff Users Report", 	
						"Prints a list of the staff users who belong to the specified role and have been inactive for 1 or more months.", 					
						"reporting/StaffUsersReportOptions",OTHER),
				new ReportBean("bank_funds",
						"Bank Funds Report", 	
						"Prints the amounts required to cover the funds due to users.", 					
						"reporting/BankFundsReportOptions",FINANCIAL),

				new ReportBean("corporate_turnover",
						"Corporate Turnover Report", 	
						"Prints the turnover amounts of the system corporates.", 					
						"reporting/CorporateTurnoverReportOptions",FINANCIAL, true),

				new ReportBean("corporate_gaming_revenue",
						"Corporate Gaming Revenue", 	
						"Prints the gaming revenue amounts of the system corporates.", 					
						"reporting/CorporateGamingRevenue",FINANCIAL),

				new ReportBean("corporate_payout_rates",
						"Payout Rates Per Corporate Report",	
						"Prints the total input and output amounts and rates for the selected corporate(s).",
						"reporting/CorporatePayoutRatesReportOptions",GAMING, true),

				new ReportBean("trial_balance",
						"Trial Balance Report", 	
						"Shows accounting data for the chosen date.",
						"reporting/TrialBalanceReportOptions",FINANCIAL, true),

				new ReportBean("deposits",
						"Deposits Report",	
						"Prints the list of completed deposits",
						"reporting/PaymentsReportOptions",
						FINANCIAL, true),
				new ReportBean("payout_transactions",
						"Withdrawals Report",	
						"Prints the list of successful withdrawals",
						"reporting/PayoutsReportOptions",
						FINANCIAL, true),
				new ReportBean("user_balance_adjustments",
						"User Balance Adjustments Report",	
						"Prints the list of user balance adjustments",
						"reporting/UserBalanceAdjustmentsReportOptions",
						FINANCIAL, true),	
				new ReportBean("duplicate_user_logins",
						"Duplicate User Logins",	
						"Prints the list of the logins from the same ip for different users",
						"reporting/UserDuplicateLoginsReport",
						OTHER, true),
				new ReportBean("user_balance",
						"Users Balance Report",	
						"Prints the balance report for users",
						"reporting/UserBalanceReport",
						FINANCIAL, true),
				new ReportBean("web_logins",
						"Web logins",	
						"Prints the lists of the web logins for users",
						"reporting/WebLoginsReport",
						OTHER, true),
				new ReportBean("client_accounts_period",
						"Client Accounts Period Report",	
						"Prints the totals for client accounts",
						"reporting/ClientAccountsPeriodReport",
						FINANCIAL, true),
				new ReportBean("corporate_account_period",
						"Corporate Account Period Report",	
						"Prints the totals for a specific corporate account",
						"reporting/CorporateAccountPeriodReport",
						FINANCIAL, true)
	};

	private final PageMenuItemAccessValidator accessValidator;

	public ReportListImpl(PageMenuItemAccessValidator accessValidator) {
		this.accessValidator = accessValidator;
	}

	@Override
	public List<ReportBean> getReports(String category) throws ServiceException {
		List<ReportBean> catReports = new ArrayList<ReportBean>();

		for(ReportBean r : REPORTS) {
			if (r.getCategory().equals(category) &&
					accessValidator.checkAccess(r.getOptionsPage())) {
				catReports.add(r);
			}
		}
		
		return catReports;
	}

	@Override
	public List<String> getCategories() {
		List<String> cats = new ArrayList<String>();

		for (String s : CATEGORIES) {
			cats.add(s);
		}

		return cats;
	}

	@Override
	public ReportBean getReport(String reportName) throws ServiceException {
		for(ReportBean r : REPORTS) {
			if (r.getReport().equals(reportName)) {
				return r;
			}
		}
		
		throw new ServiceException("Report  " + reportName + " not found");
	}
}
