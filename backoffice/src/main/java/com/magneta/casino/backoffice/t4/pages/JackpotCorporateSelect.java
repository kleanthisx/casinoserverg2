package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.AffiliatesSelectionModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class JackpotCorporateSelect extends SecurePage {

	private static final int JACKPOT_TYPE = 3;
	
	@InjectPage("CreateJackpot")
	public abstract CreateJackpot getCreateJackpotPage();
	
	@Persist("client:page")
	public abstract int getJackpotType();
	public abstract void setJackpotType(int type);
	public abstract Long getCorporate();	

	@Component(id="jackpotGameFrm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel"})
	public abstract Form getJackpotFameFrm();
	
	@Component(id="corpLabel",
			bindings={"field=component:corp"})
	public abstract FieldLabel getCorpLabel();
	
	@Component(id="corp",
			bindings={"value=corporate","displayName=message:Corporate","model=ognl:affSelectionModel",
			"disabled=!allowWrite()"})
	public abstract PropertySelection getCorp();
	
	public AffiliatesSelectionModel getAffSelectionModel(){
		return new AffiliatesSelectionModel(false,false);
	}
	
	@Component(id="okSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Override
	public void pageValidate(PageEvent event) {
        super.pageValidate(event);
        if (getJackpotType() != JACKPOT_TYPE){
        	redirectToPage("JackpotTypeSelect");
        }
	}
	
	public void onSubmit(IRequestCycle cycle){
		CreateJackpot createJackpot = getCreateJackpotPage();
		createJackpot.setCorporate(getCorporate());
		createJackpot.setJackpotType(getJackpotType());
		cycle.activate(createJackpot);
	}
	
	public void onCancel(IRequestCycle cycle){
		redirectToPage("JackpotTypeSelect");
	}
}