package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.SystemProperties;
import com.magneta.administration.beans.JackpotBean;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.services.ServiceException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class JackpotPropertiesTableModel implements IBasicTableModel {

	private TimeZone timeZone;
	private Locale locale;
	
	public JackpotPropertiesTableModel(TimeZone tz, Locale locale){
		this.timeZone = tz;
		this.locale = locale;
	}
	
	@Override
	public Iterator<JackpotBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<JackpotBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumn = null;

		if (sortCol != null) {
			sortColumn = sortCol.getColumnName();
		}
		
		
		if(("Game").equals(sortColumn)){
			sortColumn = "game_type_variant";
		} else if (("Corporate").equals(sortColumn)){
			sortColumn = "corporate";
		} else if (("InitialAmountString").equals(sortColumn)) {
			sortColumn = "initial_amount";
		} else if (("MaxAmountString").equals(sortColumn)){
			sortColumn = "maximum_amount";
		} else if (("DateStarted").equals(sortColumn)){
			sortColumn = "date_started";
		} else if (("ContribRate").equals(sortColumn)){
			sortColumn = "bet_contrib";
		} else if (("BetAmountString").equals(sortColumn)){
			sortColumn = "bet_amount";
		} else {
			sortColumn = null;
		}
		
		try
		{   
			String sql = 
				" SELECT (CASE WHEN jackpots.game_id IS NULL AND jackpots.target_corporate IS NULL THEN 'Mega' ELSE"+
				" CASE WHEN jackpots.game_id IS NOT NULL THEN 'Game' ELSE"+
				" CASE WHEN jackpots.target_corporate IS NOT NULL THEN 'Corporate' ELSE '?' END END END) AS j_type,"+
				" jackpot_configs.config_id, game_types.game_type_id, game_types.game_type_variant,"+
				" (CASE WHEN jackpot_configs.config_id IS NULL THEN jackpots.jackpot_id ELSE NULL END) AS jpot_id,"+
				" jackpots.initial_amount, jackpots.maximum_amount, jackpots.pay_count,"+
				" (CASE WHEN jackpot_configs.config_id IS NULL THEN jackpots.start_date AT TIME ZONE 'UTC' ELSE NULL END) as date_started,"+
				" jackpots.bet_contrib, jackpots.bet_amount,jackpots.auto, users.username AS corporate"+
				" FROM jackpots"+
				" LEFT OUTER JOIN game_types ON jackpots.game_id = game_types.game_type_id"+
				" LEFT OUTER JOIN jackpot_configs ON jackpots.config_id = jackpot_configs.config_id"+
				" AND "+SystemProperties.globalCorporateJackpot()+
				" LEFT OUTER JOIN users ON jackpots.target_corporate = users.user_id"+
				" AND "+!SystemProperties.globalCorporateJackpot()+
				" WHERE finish_date IS NULL"+
				" GROUP BY j_type, jackpot_configs.config_id, game_types.game_type_id, game_types.game_type_variant,"+
				" jpot_id, jackpots.initial_amount, jackpots.auto, jackpots.maximum_amount,"+ 
				" jackpots.pay_count, date_started, jackpots.bet_contrib, jackpots.bet_amount, corporate";
				
				if (sortColumn == null){
					sql += " ORDER BY j_type, COALESCE(game_types.game_type_variant,'')";
				}
			
				if (sortColumn != null){
					sql += " ORDER BY "+sortColumn;

					if (sortAsc){
						sql += " ASC";
					} else {
						sql += " DESC";
					}
				}
				
				sql+=" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			ArrayList<JackpotBean> myArr = new ArrayList<JackpotBean>();
			JackpotBean currRow;

			while (res.next()){
				currRow = new JackpotBean();
				
				currRow.setJackpotID((Integer)res.getObject("jpot_id"));
				currRow.setConfigID((Integer)res.getObject("config_id"));
				currRow.setGameID(res.getInt("game_type_id"));
				currRow.setGame(res.getString("game_type_variant"));
				currRow.setInitialAmount(res.getDouble("initial_amount"));
				currRow.setMaxAmount(res.getDouble("maximum_amount"));
				
				if (res.getObject("pay_count") != null){
					currRow.setPayTimes(res.getInt("pay_count"));
				} else {
					currRow.setPayTimes(null);
				}
				
				currRow.setDateStarted(FormatUtils.getFormattedDate(
						res.getTimestamp("date_started"), timeZone, locale));
				currRow.setContribRate(res.getDouble("bet_contrib"));
				currRow.setBetAmount(res.getDouble("bet_amount"));
				currRow.setJackpotBetType(false);
				currRow.setMystery(!res.getBoolean("auto"));

				if (res.getObject("game_type_id") != null){
					currRow.setGameID(res.getInt("game_type_id"));
					currRow.setGame(res.getString("game_type_variant"));
				} else {
					currRow.setGame("All");
				}
				
				if (res.getObject("Corporate") != null){
					currRow.setCorporate(res.getString("corporate"));
				} else {
					currRow.setCorporate("-");
				}
				
				currRow.setJackpotType(res.getString("j_type"));
				
				myArr.add(currRow);

			}

			it = myArr.iterator();

		}catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try {
            String sql = 
                "SELECT COUNT(DISTINCT "+
                (SystemProperties.globalCorporateJackpot() ? "(CASE WHEN config_id IS NULL THEN jackpot_id ELSE config_id END))" : "jackpot_id)")+
                " FROM jackpots"+
            	" WHERE finish_date IS NULL";
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        } catch (ServiceException e) {
			throw new RuntimeException(e);
		} finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
