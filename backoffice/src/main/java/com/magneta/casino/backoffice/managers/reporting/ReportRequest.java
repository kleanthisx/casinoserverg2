package com.magneta.casino.backoffice.managers.reporting;

import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JasperReport;

import com.magneta.casino.internal.beans.ExtendedPrincipalBean;

public interface ReportRequest {

	String getId();
	
	JasperReport getReport();
	
	/**
	 * User who requested the report.
	 * @return
	 */
	ExtendedPrincipalBean getPrincipal();
	
	ReportFormat getReportFormat();
	
	Locale getLocale();
	
	TimeZone getTimezone();
	
	Map<String,Object> getReportParameters();
	
	String getRequestUrl();
	String getClientHost();
}
