package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.beans.CorporateBean;

public class CorporateSelectModel extends AbstractSelectModel {
	
	private final Iterable<CorporateBean> corporates;
	private final UsernameService usernameService;
	
	public CorporateSelectModel(Iterable<CorporateBean> corporates, UsernameService usernameService) {
		this.corporates = corporates;
		this.usernameService = usernameService;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> model = new ArrayList<OptionModel>();
		try {
			for (CorporateBean corporate: corporates) {

				model.add(new OptionModelImpl(usernameService.getUsername(corporate.getAffiliateId()), corporate));

			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		return model;
	}
}
