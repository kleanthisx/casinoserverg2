package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public abstract class EditServer extends SecurePage implements PageBeginRenderListener {
	
	private static final Logger log = LoggerFactory.getLogger(EditServer.class);
	
	public abstract int getServerID();
	public abstract void setServerID(int serverID);

	public abstract String getServerDesc();
	public abstract void setServerDesc(String serverDesc);
	
	public abstract String getLocalAddress();
	public abstract void setLocalAddress(String localAddress);
	
	public abstract int getLocalPort();
	public abstract void setLocalPort(int localPort);
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();

    @Bean(initializer="omitZero=true")
    public abstract SimpleNumberTranslator getNumTranslator();
    
	@Component(id="registerServerForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getRegisterServerForm();
	
	@Component(id="serverID",
			bindings={"value=serverID","displayName=message:ServerID","translator=bean:numTranslator","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getServerIDField();
	
	@Component(id="serverIDLabel",
			bindings={"field=component:serverID"})
	public abstract FieldLabel getServerIDLabel();
	
	@Component(id="serverDesc",
			bindings={"value=serverDesc","displayName=message:ServerDesc","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getServerDescField();
	
	@Component(id="serverDescLabel",
			bindings={"field=component:serverDesc"})
	public abstract FieldLabel getServerDescLabel();
	
	@Component(id="localAddress",
			bindings={"value=localAddress","displayName=message:LocalAddress","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getLocalAddressField();
	
	@Component(id="localAddressLabel",
			bindings={"field=component:localAddress"})
	public abstract FieldLabel getLocalAddressLabel();
	
	@Component(id="localPort",
			bindings={"value=localPort","displayName=message:LocalPort","translator=bean:numTranslator","validators=validators:required",
			"disabled=!allowWrite()"})
	public abstract TextField getLocalPortField();
	
	@Component(id="localPortLabel",
			bindings={"field=component:localPort"})
	public abstract FieldLabel getLocalPortLabel();
	
	@Component(id="okSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setServerID((Integer)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent pEvt){
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = conn.prepareStatement(
					"SELECT server_desc, server_local_address, server_local_port" +
					" FROM servers" +
					" WHERE server_id = ?");
			
			stmt.setInt(1, getServerID());
			rs = stmt.executeQuery();
			
			if (rs.next()){
				setServerDesc(rs.getString("server_desc"));
				setLocalAddress(rs.getString("server_local_address"));
				setLocalPort(rs.getInt("server_local_port"));
			}
		} catch (SQLException e) {
			log.error("Error while getting server info.",e);
		}
		finally
		{
			DbUtil.close(conn);
		}
	}
	
	public void onSubmit(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}
		
		if (updateServer()){
			redirectToPage("SystemStatus");
		}
	}

	public void onCancel(IRequestCycle cycle){
		redirectToPage("SystemStatus");
	}

	private boolean updateServer(){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;

		if (dbConn == null){
			delegate.setFormComponent(null);
			delegate.record(getMsg("db-error"), null);
			return false;
		}
		
		try {

			String sql = 
				"UPDATE servers " +
				" SET server_desc = ?, server_local_address = ?, server_local_port = ?" +
				" WHERE server_id = ?";

			statement = dbConn.prepareStatement(sql);

			statement.setString(1, getServerDesc());
			statement.setString(2, getLocalAddress());
			statement.setInt(3, getLocalPort());
			statement.setInt(4, getServerID());
			statement.execute();

			return true;
		} catch (SQLException e) {
			if (e.getMessage().contains("pk_servers_server_id")){
				delegate.setFormComponent((IFormComponent) getComponent("serverID"));
				delegate.record(getMsg("server-id-error"), null);
			} else {
				delegate.setFormComponent(null);
				delegate.record(getMsg("db-error"), null);
			}
			
			log.error("Error while registering server.", e);
		} finally{
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return false;
	}
}
