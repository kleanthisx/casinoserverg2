package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.backoffice.managers.reporting.ReportFormat;

public class ReportFormatSelectModel extends AbstractSelectModel {

	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> modelList = new ArrayList<OptionModel>();
		
		for (ReportFormat format: ReportFormat.values()) {
			modelList.add(new OptionModelImpl(format.toString(), format));
		}
		
		return modelList;
	}
}
