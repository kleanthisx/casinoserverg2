package com.magneta.casino.backoffice.coercers;

import java.sql.Timestamp;

import org.apache.tapestry5.ioc.services.Coercion;

public class LongToTimestampCoercion implements Coercion<Long, Timestamp> {

	@Override
	public Timestamp coerce(Long millis) {
		if (millis == null) {
			return null;
		}
		
		return new Timestamp(millis);
	} 
}
