package com.magneta.casino.backoffice.t4.services;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.tapestry4.components.pagemenu.PageMenuItem;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemsFilter;
import com.magneta.tapestry4.components.pagemenu.PageMenuParser;

public class PageMenuServiceImpl implements PageMenuService {

	private final List<PageMenuItem> VR_MENU_ITEMS;
	private final List<PageMenuItem> HR_MENU_ITEMS;

	private final PageMenuItemAccessValidator accessValidator;
	
	public PageMenuServiceImpl(PrincipalService principalService,
			PageMenuItemAccessValidator accessValidator) throws ParserConfigurationException, SAXException, IOException {
		this.accessValidator = accessValidator;
		
		this.VR_MENU_ITEMS = PageMenuParser.parseFile(getClass().getClassLoader().getResourceAsStream("vr_menu.xml"));
		this.HR_MENU_ITEMS = PageMenuParser.parseFile(getClass().getClassLoader().getResourceAsStream("hr_menu.xml"));
	}
	
	@Override
	public List<PageMenuItem> getVerticalMenuItems(Locale locale) {
		PageMenuItemsFilter f = new PageMenuItemsFilter(accessValidator);
		return f.filterItems(VR_MENU_ITEMS, locale, "vr_menu");
	}

	@Override
	public List<PageMenuItem> getHorizontalMenuItems(Locale locale) {
		PageMenuItemsFilter f = new PageMenuItemsFilter(accessValidator);
		return f.filterItems(HR_MENU_ITEMS, locale, "hr_menu");
	}
}
