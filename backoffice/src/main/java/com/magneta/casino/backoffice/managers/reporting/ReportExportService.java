package com.magneta.casino.backoffice.managers.reporting;

import java.io.OutputStream;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

public interface ReportExportService {

	void exportReport(JasperPrint jasperPrint, ReportFormat format, OutputStream os) throws JRException;
}
