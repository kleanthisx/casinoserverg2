package com.magneta.casino.backoffice.components;

import java.util.List;

import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.common.games.filters.beans.GameComboBean;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.configuration.poker.texasholdem.WinCombination;
import com.magneta.games.configuration.poker.texasholdem.WinType;

public class TexasHoldEmPokerConfigView {
	
	@Inject
	private Messages messages;
		
	@Parameter(required=true)
	private Integer gameId;
	
	@Parameter(required=true)
	private TexasHoldemConfiguration configuration;

	private String getComboDescription(int combo) {
		return messages.get("combo-" + String.valueOf(combo));
	}

	private String getSideComboDescription(int combo) {
		return messages.get("side-combo-" + String.valueOf(combo));
	}
	
	@Property
	private GameComboBean combo;
	
	public GameComboBean[] getCombos() {
		List<WinCombination> winCombos = configuration.getWinCombinations();
		
		GameComboBean[] winCs = new GameComboBean[winCombos.size()];
		
		int i=0;
		for (WinCombination c: winCombos) {
			winCs[i] = new GameComboBean(null, getComboDescription(c.getTriggerCombo()), c.getAmount(), true);
					
			i++;
		}

		return winCs;
	}
	
	@Property
	private GameComboBean sideWinCombo;
	
	public GameComboBean[] getSideWinCombos() {
		List<WinCombination> sideWinCombos = configuration.getSideWinCombinations();
		
		GameComboBean[] jWinCs = new GameComboBean[sideWinCombos.size()];
		
		int i=0;
		for (WinCombination c: sideWinCombos) {
			
			String description = getSideComboDescription(c.getTriggerCombo());
			double amount = c.getAmount();
			
			if (c.getType() == WinType.JACKPOT_POOL_RATIO) {
				description += (" (%)");
				
				amount *= 100.0;
			}
			
			jWinCs[i] = new GameComboBean(null, description, amount, true);	
			i++;
		}

		return jWinCs;
	}
}
