package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.CountrySelectModel;
import com.magneta.casino.backoffice.selectmodels.TimeZoneSelectModel;
import com.magneta.casino.backoffice.valueencoders.CountryEncoder;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.TimeZoneService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.UserRegistrationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class CreateStaff {
	
	@Component(id="createStaffForm")
	private BeanEditForm createStaffForm;
	
	@InjectService(value="CountryService")
	private CountryService countryService;
	
	@InjectService(value="TimeZoneService")
	private TimeZoneService timeZoneService;
	
	@InjectService(value="UserRegistrationService")
	private UserRegistrationService registrationService;
	
	@Inject
	private Messages messages;
	
	@Property
	private UserRegistrationBean userBean;
	
	@Property
	private String password;
	
	@Property
	private String confirmPassword;
	
	@Property
	private CountryBean country;
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepareForm(){
		userBean = new UserRegistrationBean();
	}
	
	
	@OnEvent(value=EventConstants.VALIDATE,component="createStaffForm")
	public void onValidateForm(){
		if(createStaffForm.getHasErrors()) {
			return;
		}
		
		if(!password.equals(confirmPassword)){
			createStaffForm.recordError(messages.get("not-equal"));
			return;
		}
		
		if(password.equalsIgnoreCase(userBean.getUserName())){
			createStaffForm.recordError(messages.get("pass-eq-username"));
			return;
		}
	}

	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() throws ServiceException {
		userBean.setCountryCode(country.getCode());
		userBean.setPassword(password);
		userBean.setUserTypeEnum(UserTypeEnum.STAFF_USER);
		
		try {
			registrationService.registerUser(userBean);
		} catch (DuplicateEmailException e) {
			createStaffForm.recordError(messages.get("email-exists"));
		} catch (DuplicateUsernameException e) {
			createStaffForm.recordError(messages.get("username-exists"));
		} catch (ServiceException e) {
			throw e;		
		}
		return Index.class;
	}
	
	public CountrySelectModel getCountrySelectModel(){
		return new CountrySelectModel(countryService);
	}
	
	public CountryEncoder getCountryEncoder(){
		return new CountryEncoder(countryService);
	}
	
	public TimeZoneSelectModel getTimeZoneSelectModel(){
		return new TimeZoneSelectModel(timeZoneService);
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return Index.class;
	}

}
