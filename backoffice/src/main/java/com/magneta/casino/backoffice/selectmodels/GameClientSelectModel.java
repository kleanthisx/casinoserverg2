package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.beans.GameClientBean;

public class GameClientSelectModel extends AbstractSelectModel {
	
	private final Iterable<GameClientBean> clients;
	
	public GameClientSelectModel(Iterable<GameClientBean> clients) {
		this.clients = clients;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> clientModel = new ArrayList<OptionModel>();
		for (GameClientBean client:clients) {
			clientModel.add(new OptionModelImpl(client.getClientDescription(), client));
		}	
		return clientModel;
	}

}
