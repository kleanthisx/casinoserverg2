package com.magneta.casino.backoffice.t4.encoders;

import org.apache.tapestry.INamespace;
import org.apache.tapestry.engine.ServiceEncoder;
import org.apache.tapestry.engine.ServiceEncoding;
import org.apache.tapestry.services.ServiceConstants;

public class PageServiceEncoder implements ServiceEncoder {

	private String _extension;

	private String service;

	private String path;

	@Override
	public void encode(ServiceEncoding encoding)
	{
		String service = encoding.getParameterValue(ServiceConstants.SERVICE);

		if (!service.equals(this.service)) return;

		String pageName = encoding.getParameterValue(ServiceConstants.PAGE);

		// Only handle pages in the application namespace (not from a library).

		if (pageName.indexOf(INamespace.SEPARATOR) >= 0) return;

		StringBuilder buffer = new StringBuilder("/");

		if (path != null) {
			buffer.append(path);
			buffer.append('/');
		}
		buffer.append(pageName);
		buffer.append('.');
		buffer.append(_extension);

		encoding.setServletPath(buffer.toString());

		encoding.setParameterValue(ServiceConstants.SERVICE, null);
		encoding.setParameterValue(ServiceConstants.PAGE, null);
	}

	@Override
	public void decode(ServiceEncoding encoding)
	{
		String servletPath = encoding.getServletPath();
		
		if (path != null) {
			if (servletPath.startsWith('/' + path)) {
				servletPath = encoding.getPathInfo();
			} else {
				return;
			}
		}
		
		int dotx = servletPath.lastIndexOf('.');
		if (dotx < 0) return;

		String extension = servletPath.substring(dotx + 1);

		if (!extension.equals(_extension)) return;

		// Skip the slash and the dot.

		String page = servletPath.substring(1, dotx);

		encoding.setParameterValue(ServiceConstants.SERVICE, service);
		encoding.setParameterValue(ServiceConstants.PAGE, page);
	}

	public void setExtension(String extension)
	{
		_extension = extension;
	}

	public void setService(String service)
	{
		this.service = service;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
