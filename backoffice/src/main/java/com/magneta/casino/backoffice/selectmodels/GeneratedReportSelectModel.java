package com.magneta.casino.backoffice.selectmodels;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

/**
 * Selection Model for generated reports. 
 * @author anarxia
 */
public class GeneratedReportSelectModel extends AbstractSelectModel {

	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {

		List<OptionModel> reportModel = new ArrayList<OptionModel>();

		Connection conn = ConnectionFactory.getReportsConnection();
		if (conn == null) {
			throw new RuntimeException("Out of database connections");
		}

		Statement statement = null;
		ResultSet res = null;

		try{
			String sql =
					"SELECT DISTINCT report_name"+
							" FROM report_generation_logs";

			statement = conn.createStatement();
			res = statement.executeQuery(sql);

			while(res.next()) {
				reportModel.add(new OptionModelImpl(res.getString(1)));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(conn);
		}
		
		return reportModel;
	}

}
