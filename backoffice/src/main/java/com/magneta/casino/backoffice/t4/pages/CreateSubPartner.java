package com.magneta.casino.backoffice.t4.pages;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimeZone;

import org.apache.tapestry.IComponent;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.models.CountrySelectionModel;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.beans.UserRegistrationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.casino.services.utils.PhoneUtils;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.components.ExFieldLabel;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class CreateSubPartner extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(CreateSubPartner.class);
	
	@InitialValue("100.0")
	@Persist("client:page")
	public abstract double getAllowedMaxRate();
	public abstract void setAllowedMaxRate(double allowedMaxRate);
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getDetailsService();
	
	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporateService();
	
	@InjectObject("service:casino.common.Settings")
	public abstract SettingsService getSettingsService();
	
	@InjectObject("service:casino.common.UserRegistration")
	public abstract UserRegistrationService getUserRegistrationService();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumZeroTranslator();
	
	@Bean(initializer="min=0.00")
	public abstract Min getMinAmount();
	
	@Bean
	public abstract Max getMaxAmount();
	
	@Bean(initializer="min=0")
	public abstract Min getMinLevelAmount();
	
	@Component(id="titleMsg",
			bindings={"value=superPartner > 0 ? getMsg('page-title-sub') : getMsg('page-title')"})
	public abstract Insert getTitleMsg();
	
	@Component(id="superPartner",
			bindings={"value=superPartner"})
	public abstract Hidden getSuperPartnerHidden();
	
	@Component(id="freeCreditsRateLabel",
			bindings={"field=component:freeCreditsRate"})
	public abstract FieldLabel getFreeCreditsRateLabel();
	
	@Component(id="freeCreditsRate",
			bindings={"value=freeCreditsRate","displayName=message:FreeCreditsRate","translator=bean:numZeroTranslator","disabled=!allowWrite()"})
	public abstract TextField getFreeCreditsRateField();
	
	@Component(id="requireApprovalCBLabel",
			bindings={"field=component:requireApprovalCB"})
	public abstract FieldLabel getRequireApprovalCBLabel();
	
	@Component(id="requireApprovalCB",
			bindings={"value=approvalRequired","displayName=message:approval","disabled=!allowWrite()"})
	public abstract Checkbox getRequireApprovalCB();
	
	@Component(id="requireSuper",
			bindings={"value=superApprovalRequired","displayName=message:superRequired","disabled=!allowWrite()"})
	public abstract Checkbox getRequireSuper();
	
	@Component(id="requireSuperLabel",
			bindings={"field=component:requireSuper"})
	public abstract FieldLabel getRequireSuperLabel();
	
	@Component(id="subLevelsLabel",
			bindings={"field=component:subLevels"})
	public abstract FieldLabel getSubLevelsLabel();
	
	@Component(id="subLevels",
			bindings={"value=subLevels","displayName=message:SubLevels","translator=bean:numZeroTranslator","validators=validators:$minLevelAmount","disabled=!allowWrite()"})
	public abstract TextField getSubLevelsField();
	
	@Component(id="registerForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate",
			"clientValidationEnabled=true"})
	public abstract Form getRegisterForm();
	
	@Component(id="username",
			bindings={"value=username","displayName=message:Username","validators=validators:required,minLength=6","disabled=!allowWrite()"})
	public abstract TextField getUsernameField();
	
	@Component(id="usernameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:username"})
	public abstract ExFieldLabel getUsernameLabel();
	
	@Component(id="password",
			bindings={"value=password","hidden=true","displayName=message:Password","validators=validators:required,minLength=6",
			"disabled=!allowWrite()"})
	public abstract TextField getPasswordField();
	
	@Component(id="passwordConfirm",
			bindings={"value=passwordConfirm","hidden=true","displayName=message:ConfirmPassword","disabled=!allowWrite()",
			"validators=validators:required"})
	public abstract TextField getPasswordConfirmField();
	
	@Component(id="passwordLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:password"})
	public abstract ExFieldLabel getPasswordLabel();
	
	@Component(id="passwordConfirmLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:passwordConfirm"})
	public abstract ExFieldLabel getPasswordConfirmLabel();
	
	@Component(id="email",
			bindings={"value=email","displayName=literal:E-mail","validators=validators:email","disabled=!allowWrite()"})
	public abstract TextField getEmailField();
	
	@Component(id="emailLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:email"})
	public abstract ExFieldLabel getEmailLabel();
	
	@Component(id="firstName",
			bindings={"value=firstName","displayName=message:first-name","validators=validators:required","disabled=!allowWrite()"})
	public abstract TextField getFirstNameField();
	
	@Component(id="firstNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:firstName"})
	public abstract ExFieldLabel getFirstNameLabel();
	
	@Component(id="middleName",
			bindings={"value=middleName","displayName=literal:Middle Name","disabled=!allowWrite()"})
	public abstract TextField getMiddleNameField();
	
	@Component(id="middleNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:middleName"})
	public abstract ExFieldLabel getMiddleNameLabel();

	@Component(id="lastName",
			bindings={"value=lastName","displayName=message:last-name","validators=validators:required","disabled=!allowWrite()"})
	public abstract TextField getLastNameField();
	
	@Component(id="lastNameLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:lastName"})
	public abstract ExFieldLabel getLastNameLabel();
	
	@Component(id="town",
			bindings={"value=town","displayName=message:Town","disabled=!allowWrite()"})
	public abstract TextField getTownField();
	
	@Component(id="townLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:town"})
	public abstract ExFieldLabel getTownLabel();
	
	@Component(id="postCode",
			bindings={"value=postCode","displayName=message:postal-code","disabled=!allowWrite()"})
	public abstract TextField getPostCodefield();
	
	@Component(id="postCodeLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:postCode"})
	public abstract ExFieldLabel getPostCodeLabel();
	
	@Component(id="region",
			bindings={"value=region","displayName=message:Region","disabled=!allowWrite()"})
	public abstract TextField getRegionField();
	
	@Component(id="regionLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:region"})
	public abstract ExFieldLabel getRegionLabel();
	
	@Component(id="country",
			bindings={"value=country","displayName=message:Country","model=ognl:countrySelectionModel",
			"validators=validators:required,minLength=2","disabled=!allowWrite()"})
	public abstract PropertySelection getCountryField();
	
	   public CountrySelectionModel getCountrySelectionModel() throws ServiceException{
	    	return new CountrySelectionModel();
	    }
	
	@Component(id="countryLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:country"})
	public abstract ExFieldLabel getCountryLabel();
	
	@Component(id="zone",
			bindings={"value=timeZone","displayName=message:TimeZone",
			"model=ognl:new com.magneta.administration.commons.models.TimeZonesSelectionModel()","validators=validators:minLength=2","disabled=!allowWrite()"})
	public abstract PropertySelection getZone();
	
	@Component(id="zoneLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:zone"})
	public abstract ExFieldLabel getZoneLabel();
	
	@Component(id="phone",
			bindings={"value=phone","displayName=message:Phone","disabled=!allowWrite()"})
	public abstract TextField getPhoneField();
	
	@Component(id="phoneLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:phone"})
	public abstract ExFieldLabel getPhoneLabel();
	
	@Component(id="registerSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getRegisterSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Component(id="allowFreeCreditsCBLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:allowFreeCreditsCB"})
	public abstract ExFieldLabel getAllowFreeCreditsCBLabel();
	
	@Component(id="allowFreeCreditsCB",
			bindings={"value=allowFreeCredits","displayName=message:AllowFreeCredits","disabled=!allowWrite()"})
	public abstract Checkbox getAllowFreeCreditsCB();
	
	@Component(id="allowFullFreeCreditsCBLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:allowFullFreeCreditsCB"})
	public abstract ExFieldLabel getAllowFullFreeCreditsCBLabel();
	
	@Component(id="allowFullFreeCreditsCB",
			bindings={"value=allowFullFreeCredits","displayName=message:AllowFullFreeCredits","disabled=!allowWrite()"})
	public abstract Checkbox getAllowFullFreeCreditCB();
	
	@Component(id="affiliateRateLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:affiliateRate"})
	public abstract ExFieldLabel getAffiliateRateLabel();
	
	@Component(id="affiliateRate",
			bindings={"value=affiliateRate","displayName=getMsg('ProfitRate')","translator=bean:numZeroTranslator","validators=validators:required,$minAmount,$maxAmount",
			"disabled=!allowWrite()"})
	public abstract TextField getAffiliateRateField();
	
	@Component(id="paymentPeriodLabel",
			type="magneta:ExFieldLabel",
			bindings={"field=component:paymentPeriod"})
	public abstract ExFieldLabel getPaymentPeriodLabel();
	
	@Component(id="paymentPeriod",
			bindings={"value=paymentPeriod","displayName=message:PaymentPeriod","model=ognl:@com.magneta.casino.backoffice.t4.pages.ExamineCorporate@AFFILIATE_PAYMENT_PERIODS",
			"disabled=!allowWrite()"})
	public abstract PropertySelection getPaymentPeriodField();

	public abstract String getUsername();
	public abstract String getPassword();
	public abstract String getPasswordConfirm();
	public abstract String getNickname();
	public abstract String getEmail();
	public abstract String getFirstName();
	public abstract String getLastName();
	public abstract String getMiddleName();
	@Persist("client:page")
	public abstract String getCountry();
	public abstract String getPhone();
	public abstract String getRegion();
	public abstract String getTown();
	public abstract String getPostCode();
	@Persist("client:page")
	public abstract String getTimeZone();

	@InitialValue("0")
	@Persist("client:page")
	public abstract Long getSuperPartner();
	public abstract void setSuperPartner(Long superPartner);

	public abstract double getAffiliateRate();
	public abstract String getPaymentPeriod();
	public abstract boolean getAllowFreeCredits();
	public abstract boolean getAllowFullFreeCredits();

	public abstract Double getFreeCreditsRate();
	public abstract Integer getSubLevels();
	public abstract void setSubLevels(Integer levels);

	@InitialValue("false")
	public abstract void setApprovalRequired(boolean required);
	public abstract boolean isApprovalRequired();
	
	@InitialValue("false")			
	public abstract void setSuperApprovalRequired(boolean required);
	public abstract boolean isSuperApprovalRequired();
	
	@Override
	public boolean pageNeedsWriteAccess() {
		return true;
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		if (params != null && params.length > 0) {
			this.setSuperPartner((Long)params[0]);
		}
	}

	@Override
	public void pageBeginRender(PageEvent event){
		Long superPartner = getSuperPartner();
		if (superPartner!= null && superPartner > 0L) {
			CorporateBean superCorp;
			try {
				superCorp = this.getCorporateService().getCorporate(superPartner);
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
						
			setAllowedMaxRate(superCorp.getAffiliateRate());
		} else {
			setAllowedMaxRate(100.0);
		}

		IComponent component;
		if(getCountry() == null) {
			if(getSuperPartner() > 0) {
				Long superId = getSuperPartner();
				
				UserDetailsBean superDetails;
				try {
					superDetails = this.getDetailsService().getUser(superId);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}

				component = this.getComponent("country");
				component.getBinding("value").setObject(superDetails.getCountryCode());

				if (superDetails.getTimezone() != null) {
					component = this.getComponent("zone");
					component.getBinding("value").setObject(superDetails.getTimezone());
				}

			} else {
				component = this.getComponent("zone");
				component.getBinding("value").setObject(TimeZone.getDefault().getID());
			}
		}
		
		if(getSubLevels() != null && getSubLevels() == -1) {
			setSubLevels(null);
		}
		getMaxAmount().setMax(getAllowedMaxRate());
	}

	public void onSubmit(IRequestCycle cycle) {
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors())
			return;
		else if (!getPassword().equals(getPasswordConfirm())){
			delegate.setFormComponent((IFormComponent) getComponent("passwordConfirm"));
			delegate.record(getMsg("confirm-password-not-equal"), null);
			return;
		}

		UserRegistrationBean usr = new UserRegistrationBean();

		usr.setUserName(getUsername());
		usr.setPassword(getPassword());
		usr.setEmail(getEmail());
		usr.setFirstName(getFirstName());
		usr.setLastName(getLastName());
		usr.setMiddleName(getMiddleName());
		usr.setCountryCode(getCountry());
		
		if (PhoneUtils.isValidPhone(getPhone())) {
        	usr.setPhone(PhoneUtils.normalizePhone(getPhone()));  
        } else {
        	delegate.record(getMsg("phone-error"),null);
        	return;
        }    
		
		usr.setRegion(getRegion());
		usr.setTown(getTown());
		usr.setPostalCode(getPostCode());
		usr.setUserType(UserTypeEnum.CORPORATE_ACCOUNT.getId());
		usr.setTimezone(getTimeZone());
		
		if (getSuperPartner() != null && getSuperPartner() > 0) {
			usr.setCorporateId(getSuperPartner());
		}
		
		CorporateBean corporateInfo = new CorporateBean();
		
		corporateInfo.setAffiliateRate(getAffiliateRate() / 100.0d);
		corporateInfo.setPaymentPeriod(getPaymentPeriod());
		corporateInfo.setAllowFreeCredits(getAllowFreeCredits());
		corporateInfo.setAllowFullFreeCredits(getAllowFullFreeCredits());
		
		if (getFreeCreditsRate() != null) {
			corporateInfo.setFreeCreditsRate(getFreeCreditsRate() / 100.0d);
		}
		
		corporateInfo.setRequiresApproval(isApprovalRequired());
		corporateInfo.setRequiredSuperApproval(isSuperApprovalRequired());
		
		corporateInfo.setSubLevels(getSubLevels());
		
		try {
			getUserRegistrationService().registerCorporate(usr, corporateInfo);
		} catch (DuplicateUsernameException e) {
			delegate.record(this.getUsernameField(), getMsg("username-exists"));
			return;
		} catch (DuplicateEmailException e) {
			delegate.record(this.getEmailField(), getMsg("email-exists"));
			return;
		} catch (ServiceException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Unable to create corporate account", e);
			return;
		}
		
		try {
			if (getSettingsService().getSettingValue("system.jackpots.corporate_jackpots_enabled", Boolean.TYPE)) {
				createCorporateJackpot(usr.getId());
			}

		} catch (ServiceException e) {
			log.error("Unable to create corporate jackpot", e);
		}
		
		redirectToPage("CorporateAccounts");
	}

	private void createCorporateJackpot(Long corpId) {
		Connection conn = ConnectionFactory.getConnection();
		try {
			PreparedStatement jackpotCheckStmt = null;
			ResultSet jackpotCheckRs = null;
			boolean jackpotExists = false;
			try {

				jackpotCheckStmt = conn.prepareStatement(
						"SELECT jackpot_id"+
						" FROM jackpots"+
						" WHERE (target_corporate = ? AND finish_date IS NULL)"+
						" OR EXISTS (SELECT affiliates.affiliate_id FROM affiliate_users"+
				" INNER JOIN affiliates ON affiliate_users.user_id = affiliates.affiliate_id WHERE affiliate_users.affiliate_id = ?)");

				jackpotCheckStmt.setLong(1, corpId);
				jackpotCheckStmt.setLong(2, corpId);
				jackpotCheckRs = jackpotCheckStmt.executeQuery();
				if (jackpotCheckRs.next()){
					jackpotExists = true;
				}

			} finally {
				DbUtil.close(jackpotCheckRs);
				DbUtil.close(jackpotCheckStmt);
			}

			if (!jackpotExists){
				Statement configStmt = null;
				ResultSet configRs = null;
				boolean configExists = false;
				double initialAmount = 0.0;
				double betContrib = 0.0;
				double maxAmount = 0.0;
				int drawNumber = -1;
				double minPayout = 0.0;
				double minJackpotBalance = 0.0;

				try {
					configStmt = conn.createStatement();
					configRs = configStmt.executeQuery(
							"SELECT initial_amount, bet_contrib, maximum_amount, draw_number, min_payout, min_jackpot_balance" +
							" FROM jackpot_configs" +
					" WHERE config_id = 0");
					if (configRs.next()){
						configExists = true;
						initialAmount = configRs.getDouble("initial_amount");
						betContrib = configRs.getDouble("bet_contrib");
						maxAmount = configRs.getDouble("maximum_amount");
						drawNumber = configRs.getInt("draw_number");
						minPayout = configRs.getInt("min_payout");
						minJackpotBalance = configRs.getInt("min_jackpot_balance");
					}
				} finally {
					DbUtil.close(configRs);
					DbUtil.close(configStmt);
				}

				if (configExists){
					Statement idStmt = null;
					ResultSet idRs = null;
					PreparedStatement jackpotStmt = null;

					int jackpotId = -1;
					try {
						idStmt = conn.createStatement();
						idRs = idStmt.executeQuery("SELECT nextval('jackpot_id_seq')");

						if (idRs.next()){
							jackpotId = idRs.getInt(1);
						}
					} finally {
						DbUtil.close(idStmt);
						DbUtil.close(idRs);
					}

					try {
						jackpotStmt = conn.prepareStatement(
								"INSERT INTO jackpots(jackpot_id, game_id, target_corporate, initial_amount, maximum_amount, pay_count, " +
								" auto, config_id, draw_number, bet_contrib,min_payout, min_jackpot_balance)"+
						" VALUES (?,?,?,?,?,?,?,0,?,?,?,?)");

						jackpotStmt.setInt(1, jackpotId);

						jackpotStmt.setObject(2, null);

						jackpotStmt.setLong(3, corpId);

						jackpotStmt.setBigDecimal(4, new BigDecimal(initialAmount));

						if (maxAmount == 0.0){
							jackpotStmt.setObject(5, null);
						} else {
							jackpotStmt.setBigDecimal(5, new BigDecimal(maxAmount));
						}

						jackpotStmt.setObject(6, null);

						jackpotStmt.setBoolean(7, false);

						jackpotStmt.setInt(8, drawNumber);

						jackpotStmt.setBigDecimal(9, new BigDecimal(betContrib));

						if (minPayout > 0.0){
							jackpotStmt.setBigDecimal(10, new BigDecimal(minPayout));
						} else {
							jackpotStmt.setObject(10, null);
						}
						if (minJackpotBalance > 0.0){
							jackpotStmt.setBigDecimal(11, new BigDecimal(minJackpotBalance));
						} else {
							jackpotStmt.setObject(11, null);
						}

						jackpotStmt.execute();
					} finally {
						DbUtil.close(jackpotStmt);
					}

					if (initialAmount > 0.0){
						PreparedStatement transactionStmt = null;
						try{
							transactionStmt = conn.prepareStatement(
									"INSERT INTO jackpot_transactions(jackpot_id, amount)" +
							" VALUES (?,?)");
							transactionStmt.setInt(1, jackpotId);
							transactionStmt.setBigDecimal(2, new BigDecimal(initialAmount));
							transactionStmt.execute();
						} finally {
							DbUtil.close(transactionStmt);
						}
					}
				}
			}
		}catch (SQLException e) {
			log.error("Error creating corporate jackpot", e);
		} finally {
			DbUtil.close(conn);
		}
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.forgetPage(this.getPageName());
		redirectToPage("CorporateAccounts");
	}
}
