package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class UserLoginsReportOptions extends DateTimeReportOptions {
	
	@Component(id = "showGroupByUserCB",
			bindings={"value=groupByUser", "displayName=message:GroupByUser"})
	public abstract Checkbox getGroupByUserCB();
	
	@Component(id = "showGroupByUserCBLabel",
			bindings={"field=component:showGroupByUserCB"})
	public abstract FieldLabel getGroupByUserCBLabel();
	
	@Component(id = "username",
			bindings = {"value=username", "disabled=false","displayName=message:Username"})
	public abstract TextField getUsernameField();

	@Component(id = "usernameLabel",
			bindings = {"field=component:username"})
	public abstract FieldLabel getUsernameLabel();

	@Component(id = "ipAddress",
			bindings = {"value=ipAddress", "disabled=false","displayName=message:IPAddress"})
	public abstract TextField getIPAddressField();

	@Component(id = "ipAddressLabel",
			bindings = {"field=component:ipAddress"})
	public abstract FieldLabel getIPAddressLabel();
	
	@Component(id = "country",
			bindings = {"value=country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel()","displayName=message:Country"})
	public abstract PropertySelection getCountrySelection();

	@Component(id = "countryLabel",
			bindings = {"field=component:country"})
	public abstract FieldLabel getCountryLabel();
	
	public abstract String getUsername();
	public abstract String getCountry();
	public abstract boolean getGroupByUser();
	public abstract String getIpAddress();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);

		reportParams.put("username", getUsername());
		reportParams.put("country_val", getCountry());
		reportParams.put("ip_address", getIpAddress());
		reportParams.put("group_by_user", getGroupByUser());
	}
}
