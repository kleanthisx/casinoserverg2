package com.magneta.casino.backoffice.pages;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.HttpError;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserRoundsService;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.beans.UserRoundBean;
import com.magneta.casino.services.beans.UserRoundFilteredActionBean;
import com.magneta.casino.services.beans.UserRoundFilteredActionValueBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_HISTORY)
public class ViewRound {
	
	@Inject
	private UserRoundsService userRounds;
	
	@Inject
	private GameTableService gameTableService;

	@Inject
	private GamesService gamesService;
	
	@Inject
	private GameConfigsService gameConfigService;
	
	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@Property
	private UserRoundFilteredActionBean roundAction;
	
	@Inject
	private Messages messages;
	
	@Inject
	private ComponentResources componentResources;
	
	@Inject
	private BackofficePrincipalService principalService;

	@Property
	private UserRoundFilteredActionValueBean actionValue;

	private Integer roundId;
	private Long tableId;
	
	@Property
	private GameTableBean table;
	
	@Property
	private UserRoundBean round;
	
	@Property
	private GameBean game;
	
	/* Current round id for the table */
	private Integer currentRoundId;
	
	private List<UserRoundFilteredActionBean> actions;
	private List<UserRoundFilteredActionBean> emptyActions;
	

	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long tableId,Integer roundId) {
		this.tableId = tableId;
		this.roundId = roundId;
	}

	@OnEvent(value=EventConstants.ACTIVATE)
	public Object activate() {
		if(this.roundId == null || this.roundId < 0 || this.tableId == null || this.tableId < 0) {
			return new HttpError(404, "Round not found");
		}
		return null;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Object[] passivate() {
		return new Object[] {this.tableId,this.roundId};
	}
	
	@SetupRender
	public void getRound() throws ServiceException {
		table = gameTableService.getTable(tableId);
		round = userRounds.getRound(table.getOwner(), tableId,roundId);
		game = gamesService.getGame(round.getGameId());
		
		currentRoundId = gameTableService.getCurrentRoundId(round.getTableId());
	}
	
	public boolean getIsLiteral() {
		return actionValue.getType() == UserRoundFilteredActionValueBean.ValueType.LITERAL;
	}
	
	public boolean getIsCard() {
		return actionValue.getType() == UserRoundFilteredActionValueBean.ValueType.CARD;
	}
	
	public Link getImageUrl() {
		return this.pageRenderLinkSource.createPageRenderLinkWithContext(GameImage.class, new Object[] {game.getGameId(), actionValue.getValue()});
	}
		
	public List<UserRoundFilteredActionBean> getRoundActions() throws ServiceException {
		if(actions == null) {
			actions = userRounds.getFilteredRoundActions(table.getOwner(), round);
			emptyActions = new ArrayList<UserRoundFilteredActionBean>();
			Iterator<UserRoundFilteredActionBean> iter = actions.iterator();
			while(iter.hasNext()) {
				UserRoundFilteredActionBean action = iter.next();
				if(action.getActionDescription() == null || action.getActionDescription().isEmpty()) {
					emptyActions.add(action);

				}
			}
		}
		return actions;
	}
	
	public String getLocalizedMessage() {
		return this.messages.get(roundAction.getActionDescription().replace(":","-"));
	}
	
	public String getConfigDescription() throws ServiceException {
		GameConfigBean config = this.gameConfigService.getGameConfig(round.getGameId(), round.getGameConfigId()==null?0:round.getGameConfigId());
		return config.getConfigComment();
	}
	
	public boolean getShowPrevLink() {
		return this.round.getRoundId() > 0;
	}

	public boolean getShowNextLink() {
		if (currentRoundId == null) {
			return false;
		}
		
		return this.round.getRoundId() < currentRoundId;
	}
	
	public Integer getPrevRoundId() {
		return round.getRoundId()-1;
	}

	public Integer getNextRoundId() {
		return round.getRoundId()+1;
	}
	
	public Integer getLastRoundId() {
		return currentRoundId;
	}
	
	public String getFormatActionTime() {
		if (this.roundAction.getActionTime() == null) {
			return null;
		}
		
		BackofficePrincipalBean principal = (BackofficePrincipalBean)this.principalService.getPrincipal();
		
		return FormatUtils.getFormattedDate(this.roundAction.getActionTime(), principal.getTimeZone(), componentResources.getLocale());
	}
}
