package com.magneta.casino.backoffice.griddatasource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.magneta.casino.backoffice.beans.DbServerBean;
import com.magneta.casino.services.search.SortContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class DbServersGridDataSource extends GenericGridDataSource<DbServerBean> {

	
	public DbServersGridDataSource() {
		super();
	}

	@Override
	public int getAvailableRows() {
		return 1;
	}

	@Override
	public List<DbServerBean> getData(int limit, int offset, SortContext<DbServerBean> sortContext) throws Exception {
		List<DbServerBean> list = new ArrayList<DbServerBean>(1);

		if (offset > 1) {
			return list;
		}

		DbServerBean masterBean = new DbServerBean();
		masterBean.setName("Master");

		Connection conn = ConnectionFactory.getConnection();

		try {
			updateStatus(conn, masterBean);
		} finally {
			DbUtil.close(conn);
		}
		list.add(masterBean);
		
		return list;
	}

	private void updateStatus(Connection conn, DbServerBean bean) throws SQLException {
    	Statement masterStmt = null;
    	ResultSet masterRs = null;
    	try {
    		masterStmt = conn.createStatement();
    		masterRs = masterStmt.executeQuery(
    				"SELECT ibutler_loadavg() AS load_avg, " +
    				" pg_catalog.pg_size_pretty(pg_catalog.pg_database_size(pg_catalog.current_database())) AS size,"+
    				" (extract('epoch' from CURRENT_TIMESTAMP - pg_catalog.pg_postmaster_start_time()))::bigint AS uptime"
    				);
    		if (masterRs.next()){
    			bean.setLoadAvg(masterRs.getDouble("load_avg"));
    			bean.setUptime(masterRs.getInt("uptime") * 1000l);
    			bean.setSize(masterRs.getString("size"));
    		}
		} finally {
			DbUtil.close(masterRs);
			DbUtil.close(masterStmt);
		}
	}
}
