package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(and={PrivilegeEnum.REPORTS,PrivilegeEnum.ROLE})
public abstract class StaffUsersReportOptions extends ReportOptions {
	
	@Component(id = "role",
			bindings = {"value=role","model=ognl:new com.magneta.casino.backoffice.t4.models.RolesSelectionModel(true)","displayName=message:Role"})
	public abstract PropertySelection getRoleSel();

	@Component(id = "roleLabel",
			bindings = {"field=component:role"})
	public abstract FieldLabel getStoreSelLabel();
	
	@InitialValue("1")
	public abstract Long getRole();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
		reportParams.put("role", getRole());
	}
}
