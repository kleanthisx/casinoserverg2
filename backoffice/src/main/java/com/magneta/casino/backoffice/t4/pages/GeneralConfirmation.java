package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.BACKOFFICE_ENTRY)
public abstract class GeneralConfirmation extends SecurePage {

	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Component(id="confirmForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
			public abstract Form getAddHostForm();

	@Component(id="confirmSubmit",
			bindings={"disabled=!allowWrite()"})
			public abstract Submit getAddSubmit();

	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	
	public String confirmMessage;
	
	@Persist("client:page")
	public abstract ConfirmContext getConfirmContext();
	public abstract void setConfirmContext(ConfirmContext ctx);
	
    @Override
	public void prepareForRender(IRequestCycle cycle){
     if(getConfirmContext() == null){
    	 redirectHome();
     }
     confirmMessage = getConfirmContext().messages()[0];
    }
    
	public void onSubmit(IRequestCycle cycle) throws ServiceException {
		getConfirmContext().onOk(cycle, getErrorDisplay().getDelegate());
	}

	public void onCancel(IRequestCycle cycle){
		getConfirmContext().onCancel(cycle, getErrorDisplay().getDelegate());
	} 
}
