package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.AffiliateUsersBean;
import com.magneta.casino.services.beans.CorporateBean;
import com.magneta.casino.services.beans.ServiceResultSet;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserAffiliationEnum;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class EditPartner extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditPartner.class);

	@InjectObject("service:casino.common.Corporates")
	public abstract CorporatesService getCorporateService();
	
	@Persist("client:page")
	public abstract Long getUserId();
	public abstract void setUserId(Long userId);

	public abstract String getUsername();
	public abstract void setUsername(String username);

	public abstract double getAffiliateRate();
	public abstract void setAffiliateRate(double affiliateRate);

	public abstract String getPaymentPeriod();
	public abstract void setPaymentPeriod(String paymentPeriod);

	public abstract boolean getAllowFreeCredits();
	public abstract void setAllowFreeCredits(boolean allowFreeCredits);

	public abstract boolean getAllowFullFreeCredits();
	public abstract void setAllowFullFreeCredits(boolean allowFullFreeCredits);

	public abstract Double getFreeCreditsRate();
	public abstract void setFreeCreditsRate(Double freeCreditsRate);

	public abstract void setApprovalRequired(boolean required);
	public abstract boolean isApprovalRequired();

	public abstract Integer getSubLevels();
	public abstract void setSubLevels(Integer levels);
	
	public abstract void setSuperApprovalRequired(boolean required);
	public abstract boolean isSuperApprovalRequired();

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumZeroTranslator();
	
	@Bean
	public abstract Min getMinAmount();
	
	@Bean
	public abstract Max getMaxAmount();
	
	@Component(bindings={"success=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getRegisterForm();
	
	@Component(id="username",
			bindings={"value=username"})
	public abstract Insert getUsernameField();
	
	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getRegisterSubmit();

	@Component
	public abstract Button getCancelButton();
	
	@Component(bindings={"field=component:affiliateRate"})
	public abstract FieldLabel getAffiliateRateLabel();
	
	@Component(id="affiliateRate",
			bindings={"value=affiliateRate","displayName=message:AffiliateRate","translator=bean:numZeroTranslator",
			"validators=validators:required,$minAmount,$maxAmount","disabled=!allowWrite()"})
	public abstract TextField getAffiliateRateField();
	
	@Component(bindings={"field=component:paymentPeriod"})
	public abstract FieldLabel getPaymentPeriodLabel();
	
	@Component(id="paymentPeriod",
			bindings={"value=paymentPeriod","displayName=message:PaymentPeriod",
			"model=ognl:@com.magneta.casino.backoffice.t4.pages.ExamineCorporate@AFFILIATE_PAYMENT_PERIODS","disabled=!allowWrite()"})
	public abstract PropertySelection getPaymentPeriodField();

	@Component(bindings={"field=component:allowFreeCreditsCB"})
	public abstract FieldLabel getAllowFreeCreditsCBLabel();
	
	@Component(bindings={"value=allowFreeCredits","displayName=message:AllowFreeCredits","disabled=!allowWrite()"})
	public abstract Checkbox getAllowFreeCreditsCB();
	
	@Component(bindings={"field=component:allowFullFreeCreditsCB"})
	public abstract FieldLabel getAllowFullFreeCreditsCBLabel();
	
	@Component(bindings={"value=allowFullFreeCredits","displayName=message:AllowFullFreeCredits","disabled=!allowWrite()"})
	public abstract Checkbox getAllowFullFreeCreditsCB();
	
	@Component(bindings={"field=component:requireApprovalCB"})
	public abstract FieldLabel getRequireApprovalCBLabel();
	
	@Component(bindings={"value=approvalRequired","displayName=message:approval","disabled=!allowWrite()"})
	public abstract Checkbox getRequireApprovalCB();
	
	@Component(bindings={"value=superApprovalRequired","displayName=message:superRequired","disabled=!allowWrite()"})
	public abstract Checkbox getRequireSuper();
	
	@Component(bindings={"field=component:requireSuper"})
	public abstract FieldLabel getRequireSuperLabel();
	
	@Component(bindings={"field=component:subLevels"})
	public abstract FieldLabel getSubLevelsLabel();
	
	@Component(id="subLevels",
			bindings={"value=subLevels","displayName=message:SubLevels","translator=bean:numZeroTranslator",
			"validators=validators:min=0","disabled=!allowWrite()"})
	public abstract TextField getSubLevelsField();
	
	@Component(bindings={"field=component:freeCreditsRate"})
	public abstract FieldLabel getFreeCreditsRateLabel();
	
	@Component(id="freeCreditsRate",
			bindings={"value=freeCreditsRate","displayName=message:FreeCreditsRate","translator=bean:numZeroTranslator",
			"disabled=!allowWrite()"})
	public abstract TextField getFreeCreditsRateField();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)) {
			setUserId((Long)params[0]);
		}
		
		if (getUserId() == null) {
			throw new BadRequestException("Invalid/missing userId");
		}
	}
	
	private static final Double toPercent(Double rate) {
		if (rate == null) {
			return null;
		}
		
		return rate * 100.0d;
	}
	
	private static final Double toRate(Double percent) {
		if (percent == null) {
			return null;
		}
		
		return percent / 100.0d;
	}
	
	@Override
	public void pageBeginRender(PageEvent event) {
		if (getUserId() == null) {
			throw new BadRequestException("Invalid/missing userId");
		}
		
		setUsername(getUsername(getUserId()));
		
		if (!event.getRequestCycle().isRewinding()) {
			CorporateBean corporate;
			try {
				corporate = this.getCorporateService().getCorporate(getUserId());
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
			
			if (corporate == null) {
				throw new BadRequestException("Invalid userId");
			}

			setAffiliateRate(toPercent(corporate.getAffiliateRate()));
			setAllowFreeCredits(corporate.getAllowFreeCredits());
			setAllowFullFreeCredits(corporate.getAllowFullFreeCredits());
			setFreeCreditsRate(toPercent(corporate.getFreeCreditsRate()));
			setSubLevels(corporate.getSubLevels());
			setApprovalRequired(corporate.getRequiresApproval());
			setSuperApprovalRequired(corporate.getRequiredSuperApproval());
		}
		
		CorporateBean superCorp;
		try {
			superCorp = getCorporateService().getSuperCorporate(getUserId());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		if (superCorp == null) {
			getMaxAmount().setMax(100.d);
		} else {
			getMaxAmount().setMax(toPercent(superCorp.getAffiliateRate()));
		}
		
		List<AffiliateUsersBean> subAccounts;
		try {
			subAccounts = getCorporateService().getSubAccounts(getUserId(), 
					FiqlContextBuilder.create(AffiliateUsersBean.class, "affiliation==?", UserAffiliationEnum.SUB_CORPORATE_AFFILIATION.getId()), null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		if (subAccounts.isEmpty()) {
			getMinAmount().setMin(0.0d);
		} else {
			List<Long> subIds = new ArrayList<Long>(subAccounts.size());
			
			for (AffiliateUsersBean sub: subAccounts) {
				subIds.add(sub.getAffiliateId());
			}
			
			ServiceResultSet<CorporateBean> subCorporates;
			try {
				subCorporates = getCorporateService().getCorporates(
						FiqlContextBuilder.create(CorporateBean.class, "affiliateId=in=?", subIds),
						null, 0, 0);
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
			
			double maxRate = 0.0;
			
			for (CorporateBean sub: subCorporates.getResult()) {
				maxRate = Math.max(maxRate, sub.getAffiliateRate());
			}
			
			getMinAmount().setMin(maxRate * 100.0d);
		}
	}

	public void onSubmit(IRequestCycle cycle) {
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}

		Connection conn = ConnectionFactory.getConnection();

		if (conn == null) {
			return;
		}

		PreparedStatement stmt = null;

		try {
			stmt = conn.prepareStatement(
					"UPDATE affiliates SET"+
					" affiliate_rate = ?," +
					" payment_period = CAST (? AS INTERVAL)," +
					" allow_free_credits = ?, " +
					" free_credits_rate = ?," +
					" allow_full_free_credits = ?," +
					" sub_levels = ?," +
					" require_approval = ?," +
					" requires_super_approval = ?," +
					" modifier_user = ?" +
					" WHERE affiliate_id = ?");


			stmt.setDouble(1, getAffiliateRate() / 100.0);
			stmt.setString(2, getPaymentPeriod());
			stmt.setBoolean(3, getAllowFreeCredits());
			
			
			DbUtil.setDouble(stmt, 4, toRate(getFreeCreditsRate()));
			stmt.setBoolean(5, getAllowFullFreeCredits());
			DbUtil.setInteger(stmt, 6, getSubLevels());
			stmt.setBoolean(7, isApprovalRequired());
			stmt.setBoolean(8, isSuperApprovalRequired());
			
			stmt.setLong(9, getPrincipal().getUserId());
			stmt.setLong(10, getUserId());

			stmt.executeUpdate();
			
		} catch (SQLException e) {
			delegate.record(null, getMsg("db-error"));
			log.error("Error while updating affiliate", e);
			return;
		} finally {
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		
		cycle.forgetPage(getPageName());
		redirectToPage("CorporateAccounts");
	}

	public void onCancel(IRequestCycle cycle) {
		cycle.forgetPage(getPageName());
		redirectToPage("CorporateAccounts");
	}
}
