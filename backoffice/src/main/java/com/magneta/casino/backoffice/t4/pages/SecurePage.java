package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.RedirectException;
import org.apache.tapestry.Tapestry;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.engine.ExternalServiceParameter;
import org.apache.tapestry.engine.IEngineService;
import org.apache.tapestry.engine.ILink;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.event.PageValidateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.exceptions.AccessDeniedException;
import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserAccessService;
import com.magneta.casino.services.UsernameService;
import com.magneta.tapestry4.components.pagemenu.PageMenuItemAccessValidator;

/**
 * Parent class for all pages requiring the user to login before
 * they can be accessed
 * 
 * @author Nicos
 *
 */
public abstract class SecurePage extends PublicPage implements PageValidateListener {
	
	private static final Logger log = LoggerFactory.getLogger(SecurePage.class);
	
	@InjectObject("service:casino.common.Username")
	public abstract UsernameService getUsernameService();

	@InjectObject("service:casino.common.UserAccess")
	public abstract UserAccessService getUserAccessService();

	@InjectObject("service:t4.common.PageMenuItemAccessValidator")
	public abstract PageMenuItemAccessValidator getAccessValidator();
	
	public boolean pageNeedsWriteAccess() {
		return false;
	}

	public boolean hasWriteAccess(String page) {
		return getAccessValidator().checkWriteAccess(page);
	}

	@Override
	public void pageValidate(PageEvent event) {		
		if (getPrincipal() == null || getPrincipal().getUserId() == null || getPrincipal().getUserId().equals(0L)) {
			IEngineService service =
					event.getRequestCycle().getInfrastructure().getServiceMap().getService(Tapestry.EXTERNAL_SERVICE);

			ILink redirectLink = service.getLink(false, new ExternalServiceParameter(this.getPageName(), event.getRequestCycle().getListenerParameters())); 
			String redirectUrl = redirectLink.getURL();
			
			throw new RedirectException(LinkGenerator.getPageUrl("login", redirectUrl));
		}
		
		if (!getAccessValidator().checkAccess(this.getPageName())){
			throw new AccessDeniedException("Access denied");
		}

		if (pageNeedsWriteAccess() && !allowWrite()){
			throw new AccessDeniedException("Access denied");
		}
	}

	public boolean allowWrite() {
		return getAccessValidator().checkWriteAccess(this.getPageName());
	}

	public void redirectHome(){
		throw new RedirectException(LinkGenerator.getPageUrl("index"));
	}
	
    public String getUsername(Long userId) {
    	try {
			return getUsernameService().getUsername(userId);
		} catch (ServiceException e) {
			log.error("Unable to retrieve username", e);
		}
		
		return null;
    }
}
