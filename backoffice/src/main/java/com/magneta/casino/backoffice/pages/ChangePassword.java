package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.BACKOFFICE_ENTRY)
public class ChangePassword {

	@Component(id = "changeForm")
	private Form changeForm;

	@Property
	private String oldPassword;

	@Property
	private String newPassword;

	@Property
	private String confirmPassword;

	@InjectService("UserDetailsService")
	private UserDetailsService userDetailsService;

	@InjectService("UserRegistrationService")
	private UserRegistrationService userRegistrationService;

	@Inject
	private ExtendedPrincipalBean principal;

	@Inject
	private Messages messages;
	
	@Property
	private UserDetailsBean userBean;

	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepareForm() throws ServiceException{
		userBean = userDetailsService.getUser(principal.getUserId());
	}

	@OnEvent(value=EventConstants.VALIDATE, component="changeForm")
	public void onValidateForm() throws ServiceException{
		boolean validPassword;

		if(changeForm.getHasErrors()) {
			return;
		}

		if(!newPassword.equals(confirmPassword)) {
			changeForm.recordError(messages.get("not-equal"));
			return;
		}

		if(newPassword.equalsIgnoreCase(userBean.getUserName())){
			changeForm.recordError(messages.get("pass-eq-username"));
			return;
		}

		validPassword = userRegistrationService.verifyPassword(userBean.getId(), oldPassword);

		if(!validPassword){
			changeForm.recordError(messages.get("invalid-old-pass"));
		}
	}

	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() throws ServiceException{
		userRegistrationService.resetPassword(userBean.getId(), oldPassword, newPassword);
		return Profile.class;
	} 

	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return Profile.class;
	}
}
