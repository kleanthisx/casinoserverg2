package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.JackpotBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.services.JackpotsService;
import com.magneta.casino.services.ServiceException;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class ActiveJackpotsTableModel implements IBasicTableModel {
	
	private JackpotsService jackpotService = ServiceLocator.getService(JackpotsService.class);
	
	@Override
	public Iterator<JackpotBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;

		if (dbConn == null) {
			throw new RuntimeException("Out of database connections");
		}
		
		try
		{   
			String sql = 
				"SELECT (CASE WHEN jackpots.game_id IS NULL AND jackpots.target_corporate IS NULL THEN 'Mega' ELSE"+
				" CASE WHEN jackpots.game_id IS NOT NULL THEN 'Game' ELSE"+
				" CASE WHEN jackpots.target_corporate IS NOT NULL THEN 'Corporate' ELSE '?' END END END) AS j_type,"+
				" game_types.game_type_id, game_types.game_type_variant,"+
				" users.username AS corporate, jackpots.jackpot_id,"+
				" jackpot_balances.balance AS jackpot_amount, jackpot_balances.winners"+
				" FROM jackpots"+
				" INNER JOIN jackpot_balances ON jackpots.jackpot_id = jackpot_balances.jackpot_id"+
				" LEFT OUTER JOIN game_types ON jackpots.game_id = game_types.game_type_id"+
				" LEFT OUTER JOIN users ON jackpots.target_corporate = users.user_id"+
				" WHERE finish_date IS NULL"+
				" GROUP BY j_type, game_types.game_type_id, game_types.game_type_variant, corporate, jackpots.jackpot_id, jackpot_balances.balance, jackpot_balances.winners"+
				" ORDER BY j_type, COALESCE(game_types.game_type_variant,''), corporate"+
				" LIMIT ? OFFSET ?";
			
			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			ArrayList<JackpotBean> myArr = new ArrayList<JackpotBean>();
			JackpotBean currRow;

			while (res.next()){
				currRow = new JackpotBean();
				
				currRow.setJackpotID((Integer)res.getObject("jackpot_id"));
				currRow.setGameID(res.getInt("game_type_id"));
				currRow.setTimesPaid(res.getInt("winners"));
				currRow.setJackpotAmount(res.getDouble("jackpot_amount"));
				
				if (res.getObject("game_type_id") != null){
					currRow.setGameID(res.getInt("game_type_id"));
					currRow.setGame(res.getString("game_type_variant"));
				} else {
					currRow.setGame("All");
				}
				
				if (res.getObject("corporate") != null){
					currRow.setCorporate(res.getString("corporate"));
				} else {
					currRow.setCorporate("All");
				}
				currRow.setJackpotType(res.getString("j_type"));
				myArr.add(currRow);
			}

			return myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving jackpots.",e);
		} finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
	}

	@Override
	public int getRowCount() {
		try {
			return jackpotService.getActiveJackpotsSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException("Error while count active jackpots");
		}
	}
}
