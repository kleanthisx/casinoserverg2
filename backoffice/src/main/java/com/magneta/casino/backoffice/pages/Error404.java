package com.magneta.casino.backoffice.pages;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletResponse;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;

import com.magneta.casino.backoffice.annotations.DisplayMenu;

@PermitAll
@DisplayMenu(false)
public class Error404 {
	@Property
    @Inject
    private Request request;

	@Property
    @Inject
    @Symbol(SymbolConstants.PRODUCTION_MODE)
    private boolean productionMode;
    
	@Property
	private String requestUrl;
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public String onPassivate() {
		return this.requestUrl;
	}
    
    @Inject
    private RequestGlobals requestGlobals;

    public void setupRender() {
    	requestGlobals.getHTTPServletResponse().setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
}
