package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.ValidationDelegate;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.callback.CasinoPageCallback;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.tapestry4.translators.SimpleNumberTranslator;

@RequiredPrivilege(PrivilegeEnum.GAME_HISTORY)
public abstract class ViewGame extends SecurePage {
	
	@Bean
    public abstract ValidationDelegate getDelegate(); 
	
	public abstract Long getTableID();
	public abstract int getRoundID();
	
	@Bean(initializer="omitZero=false")
	public abstract SimpleNumberTranslator getNumTranslator();
	
	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=beans.delegate","clientValidationEnabled=true"})
	public abstract Form getViewGameForm();
	
	@Component(bindings={"value=tableID","disabled=!allowWrite()","displayName=message:GameID","translator=bean:numTranslator","validators=validators:required"})
	public abstract TextField getTableIDField();
	
	@Component(bindings={"value=roundID","disabled=!allowWrite()","translator=bean:numTranslator","validators=validators:required"})
	public abstract TextField getRoundIDField();
	
	@Component
	public abstract Submit getOkSubmit();
	
	@Component
	public abstract Button getCancelButton();
	
	public void onSubmit(IRequestCycle cycle) {
		if (getDelegate().getHasErrors()){
			return;
		}
		
		new CasinoPageCallback("viewround", new Object[] {getTableID(), getRoundID()}).performCallback(cycle);
	}
	
	public void onCancel(IRequestCycle cycle){
		redirectHome();
	}
}
