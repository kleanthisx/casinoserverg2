package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.link.PageLink;

import com.magneta.administration.beans.JackpotWinBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.CasinoLink;
import com.magneta.casino.backoffice.t4.components.UserLink;
import com.magneta.casino.backoffice.t4.models.JackpotWinsTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_HISTORY)
public abstract class JackpotWins extends SecurePage {
	@Component(type="contrib:Table",
			bindings={"source=ognl:jackpotWinsTableModel","columns=literal: !WinDate, !UserId, !JackpotType, !GameId, !AmountString",
			"pageSize=20","row=currJackpot","rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getJackpotsTable();
	
	public JackpotWinsTableModel getJackpotWinsTableModel(){
		return new JackpotWinsTableModel(getTZ(), getLocale());
	}
	
	@Component(bindings={"userId=currJackpot.userId"})
	public abstract UserLink getUserInfoLink();
	
	@Component(bindings={"value=currJackpot.gameId"})
	public abstract Insert getGameId();
	
	@Component(bindings={"page=literal:ViewRound","parameters={currJackpot.tableId, currJackpot.roundId}"})
	public abstract CasinoLink getRoundInfoLink();
	
	@Component(bindings={"page=literal:Jackpots"})
	public abstract PageLink getJackpotsLink();

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract JackpotWinBean getCurrJackpot();
}
