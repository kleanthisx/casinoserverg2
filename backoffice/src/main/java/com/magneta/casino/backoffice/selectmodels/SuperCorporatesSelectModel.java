package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

import com.magneta.casino.services.CorporatesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;
import com.magneta.casino.services.beans.CorporateBean;

public class SuperCorporatesSelectModel extends AbstractSelectModel{

	private CorporatesService corporatesService;
	private UsernameService usernameService;
	
	public SuperCorporatesSelectModel(CorporatesService corporatesService,UsernameService usernameService){
		this.corporatesService = corporatesService;
		this.usernameService = usernameService;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
			return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> result = new ArrayList<OptionModel>();
		List<CorporateBean> corporates;
		
		try {
			corporates = corporatesService.getSuperCorporates(null, null, 0, 0).getResult();
			
		} catch (ServiceException e) {
			throw new RuntimeException();
		}
		
		for(CorporateBean corporate:corporates){
			try {
				result.add(new OptionModelImpl(usernameService.getUsername(corporate.getAffiliateId()), corporate));
				
			} catch (ServiceException e) {
				throw new RuntimeException();
			}
		}	
		return result;
	}
}
