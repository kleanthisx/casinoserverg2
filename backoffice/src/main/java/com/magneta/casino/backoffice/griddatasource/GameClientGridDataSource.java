package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.search.SortContext;

public class GameClientGridDataSource extends GenericGridDataSource<GameClientBean> {

	private final GameClientService gameClientService;
	
	public GameClientGridDataSource(GameClientService gameClientService) {
		this.gameClientService = gameClientService;
	}
	
	@Override
	public int getAvailableRows() {
		try {
			return gameClientService.getGameClientsSize(null);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<GameClientBean> getData(int limit, int offset,
			SortContext<GameClientBean> sortContext) throws Exception {
		return gameClientService.getGameClients(null, limit, offset, sortContext).getResult();
	}
}
