package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.PROFILES)
public class SearchUserById {
	
	@Component(id = "search")
	private Form form;

	@Component(id = "userId")
	private TextField userIdField;

	
	@Inject
	private Messages messages;
	
	@InjectPage
	private UserDetails userDetailsPage;
	
	@Property
	private Long userId;
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onFormSuccess() {
		userDetailsPage.setUserId(userId);
		return userDetailsPage;
	}
}
