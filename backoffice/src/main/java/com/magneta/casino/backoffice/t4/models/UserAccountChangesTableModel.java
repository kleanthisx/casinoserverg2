package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.UserAccountChangesBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class UserAccountChangesTableModel implements IBasicTableModel {
	
	private Long userId;
	
	public UserAccountChangesTableModel(Long userId){
		if(userId == null){
			this.userId = 0L;
		}else{
			this.userId = userId;
		}
	}
	
	@Override
	public Iterator<UserAccountChangesBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<UserAccountChangesBean> it = null;

		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
		
		String sortColumnName = "Username";

        if (sortCol != null){
            sortColumnName = sortCol.getColumnName();
        }
        
        if (("ChangeUser").equals(sortColumnName))
            sortColumnName = "users.username";
        else
            sortColumnName = "user_changes.change_time";
		
		try {
			String sql = 
				" SELECT change_time, edit_user.username AS edit_user, user_changes.user_id, old_username, old_password_hash, old_password_salt, old_email, old_is_active, old_first_name, old_last_name,"+
				" old_middle_name, old_country.country_name AS old_country, old_phone, old_region, old_town, old_postal_code, old_nickname, old_time_zone," + 
				" old_user_type, old_postal_address, old_date_deactivated, new_username, new_password_hash, new_password_salt,"+
				" new_email, new_is_active, new_first_name, new_last_name, new_middle_name, new_country.country_name AS new_country, new_phone, new_region, new_town, new_postal_code,"+
				" new_nickname, new_time_zone, new_user_type, new_postal_address," +
				" new_date_deactivated, users.username AS change_user, old_closed, new_closed" +
				" FROM user_changes"+
				" LEFT OUTER JOIN users ON users.user_id = user_changes.change_by_user"+
				" LEFT OUTER JOIN users AS edit_user ON edit_user.user_id = user_changes.user_id"+
				" LEFT OUTER JOIN countries AS old_country ON old_country.country_code = user_changes.old_country"+
				" LEFT OUTER JOIN countries AS new_country ON new_country.country_code = user_changes.new_country" +
				(userId > 0? " WHERE user_changes.user_id = ?" : "")+
				" ORDER BY "+sortColumnName+(sortAsc? " ASC": " DESC")+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			if (userId > 0){
				statement.setLong(1, userId);
				statement.setInt(2, limit);
				statement.setInt(3, offset);
			} else {
				statement.setInt(1, limit);
				statement.setInt(2, offset);
			}

			res = statement.executeQuery();

			ArrayList<UserAccountChangesBean> myArr = new ArrayList<UserAccountChangesBean>();
			UserAccountChangesBean currRow;

			while (res.next()){
				currRow = new UserAccountChangesBean();
				
				currRow.setId(res.getLong("user_id"));
				currRow.setChangeTime(res.getTimestamp("change_time"));
				currRow.setOldUsername(res.getString("old_username"));
				currRow.setOldPasswordHash(res.getString("old_password_hash"));
				currRow.setOldPasswordSalt(res.getString("old_password_salt"));
				currRow.setOldEmail(res.getString("old_email"));
				currRow.setOldIsActive(res.getBoolean("old_is_active"));
				currRow.setOldFirstName(res.getString("old_first_name"));
				currRow.setOldLastName(res.getString("old_last_name"));
				currRow.setOldMiddleName(res.getString("old_middle_name"));
				currRow.setOldCountry(res.getString("old_country"));
				currRow.setOldPhone(res.getString("old_phone"));
				currRow.setOldRegion(res.getString("old_region"));
				currRow.setOldTown(res.getString("old_town"));
				currRow.setOldPostalCode(res.getString("old_postal_code"));
				currRow.setOldNickname(res.getString("old_nickname"));
				currRow.setOldTimeZone(res.getString("old_time_zone"));
				currRow.setOldUserType(res.getInt("old_user_type"));
				currRow.setOldPostalAddress(res.getString("old_postal_address"));
				currRow.setOldDateDeactivated(res.getTimestamp("old_date_deactivated"));
				currRow.setOldClosed(res.getBoolean("old_closed"));
			    
				currRow.setNewUsername(res.getString("new_username"));
				currRow.setNewPasswordHash(res.getString("new_password_hash"));
				currRow.setNewPasswordSalt(res.getString("new_password_salt"));
				currRow.setNewEmail(res.getString("new_email"));
				currRow.setNewIsActive(res.getBoolean("new_is_active"));
				currRow.setNewFirstName(res.getString("new_first_name"));
				currRow.setNewLastName(res.getString("new_last_name"));
				currRow.setNewMiddleName(res.getString("new_middle_name"));
				currRow.setNewCountry(res.getString("new_country"));
				currRow.setNewPhone(res.getString("new_phone"));
				currRow.setNewRegion(res.getString("new_region"));
				currRow.setNewTown(res.getString("new_town"));
				currRow.setNewPostalCode(res.getString("new_postal_code"));
				currRow.setNewNickname(res.getString("new_nickname"));
				currRow.setNewTimeZone(res.getString("new_time_zone"));
				currRow.setNewUserType(res.getInt("new_user_type"));
				currRow.setNewPostalAddress(res.getString("new_postal_address"));
				currRow.setNewDateDeactivated(res.getTimestamp("new_date_deactivated"));
				currRow.setNewClosed(res.getBoolean("new_closed"));
				
				currRow.setChangeUser(res.getString("change_user"));
				currRow.setEditUser(res.getString("edit_user"));
				
				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving user changes.",e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
        
        try {
            String sql = 
                "SELECT COUNT(*)"+
            	" FROM user_changes" +
            	(userId > 0? " WHERE user_changes.user_id = ?" : "");
            statement = dbConn.prepareStatement(sql);
            
            if (userId > 0){
				statement.setLong(1, userId);
            }
            
            res = statement.executeQuery();
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting user changes count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
