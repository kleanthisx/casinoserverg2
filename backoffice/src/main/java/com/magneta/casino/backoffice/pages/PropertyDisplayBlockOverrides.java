package com.magneta.casino.backoffice.pages;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PropertyOutputContext;

import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.internal.services.PrincipalService;

public class PropertyDisplayBlockOverrides {

	@Inject
	private Locale locale;

	@Inject
	private PrincipalService principalService;

	@Environmental
	private PropertyOutputContext context;	

	public PropertyOutputContext getContext() {
		return context;
	}

	public DateFormat getDateFormat() {
		TimeZone tz = null;
		BackofficePrincipalBean principal = (BackofficePrincipalBean)principalService.getPrincipal();
		if (principal == null){
			tz = TimeZone.getDefault();
		}else{
			tz = principal.getTimeZone();
		}

		DateFormat dateFormat;
		
		if (locale == null) {
			dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
		} else {
			dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
		}
		dateFormat.setTimeZone(tz);

		return dateFormat;
	}

	public Date getCustomDate() {
		Date dateTime = (Date)context.getPropertyValue();

		if (dateTime == null)
			return null;

		return dateTime;
	}
}