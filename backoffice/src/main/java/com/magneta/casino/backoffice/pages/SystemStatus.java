package com.magneta.casino.backoffice.pages;

import java.util.Map;

import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.SystemProperties;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.beans.DbServerBean;
import com.magneta.casino.backoffice.griddatasource.DbServersGridDataSource;
import com.magneta.casino.backoffice.griddatasource.ServersGridDataSource;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.xmlrpc.client.SocketXmlRpcClient;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public class SystemStatus {

	private static final Logger log = LoggerFactory.getLogger(SystemStatus.class);

	@Inject
	private ServersService serversService;

	@Inject
	private AlertManager alertManager;

	@Property
	private ServerBean server;

	@Property
	private DbServerBean dbServer;
	
	@Inject
	private Messages messages;

	@SuppressWarnings("unchecked")
	@Cached(watch="server")
	private Map<String,Object> getServerStatus() {
		if (!server.getServerRunning()) {
			return null;
		}

		Object[] args = { SystemProperties.getGamingServerPassword() };

		SocketXmlRpcClient client = new SocketXmlRpcClient(server.getServerLocalAddress(), server.getServerLocalPort(), false);
		try {
			try {
				return (Map<String,Object>)client.execute("Admin.getSystemStatus", args);
			} catch (XmlRpcException e) {
				log.debug("Error getting server status: {}", e.getMessage());
				return null;
			}
		} finally {
			client.destroy();
		}
	}


	public ServersGridDataSource getServers() {
		return new ServersGridDataSource(serversService);
	}

	public Integer getActiveTables() {
		if (this.getServerStatus() == null) {
			return null;
		}

		return (Integer)getServerStatus().get("active_tables");
	}

	public Integer getActiveUsers() {
		if (this.getServerStatus() == null) {
			return null;
		}		
		return (Integer)getServerStatus().get("active_users");
	}

	public String getUptime() {
		if (this.getServerStatus() == null) {
			return null;
		}		
		return FormatUtils.getDuration((Integer)this.getServerStatus().get("uptime"));
	}

	public String getLoadAvg() {
		if (this.getServerStatus() == null) {
			return null;
		}		
		return this.getServerStatus().get("load_avg").toString();
	}

	public DbServersGridDataSource getDbServers() {
		return new DbServersGridDataSource();
	}

	public String getDbUptime() {
		return FormatUtils.getDuration((int)(dbServer.getUptime() / 1000l));
	}

	@RequiredPrivilege(value=PrivilegeEnum.SYSTEM_ADMIN, write=true)
	@OnEvent(value="DELETE_SERVER") 
	public void deleteServer(ServerBean server) {
		try {
			serversService.deleteServer(server);
		} catch (ServiceException e1) {
			alertManager.error("Game server not deleted: " + e1.getMessage());
		}
	}
	
	public String getConfirmDeleteMessage() {
		return messages.format("confirm-delete", server.getServerDescription());
	}

	@RequiredPrivilege(value=PrivilegeEnum.SYSTEM_ADMIN, write=true)
	@OnEvent(value="SUSPEND_SERVER")
	public void suspendServer(ServerBean server) {
		SocketXmlRpcClient client = new SocketXmlRpcClient(server.getServerLocalAddress(), server.getServerLocalPort(), false);

		try {
			boolean suspended = (Boolean)client.execute("Admin.suspendServer", new Object[] {
					SystemProperties.getGamingServerPassword(),
					""
			});
			if (suspended) {
				alertManager.success("Server "+ server.getServerDescription() + " suspended");
			} else {
				alertManager.error("Suspend rejected by the server");
			}

		} catch (XmlRpcException e) {
			alertManager.error("Suspend failed: could not contact server");
		} finally {
			client.destroy();
		}
	}
	
	public String getConfirmSuspendMessage() {
		return messages.format("confirm-suspend", server.getServerDescription());
	}
	
	@OnEvent("SHUTDOWN_SERVER")
	public void shutdownServer(ServerBean server) {
		SocketXmlRpcClient client = new SocketXmlRpcClient(server.getServerLocalAddress(), server.getServerLocalPort(), false);

		try {
			boolean shutdown = (Boolean)client.execute("Admin.stopServer", new Object[] {SystemProperties.getGamingServerPassword()});
			if (shutdown) {
				alertManager.success("Server "+ server.getServerDescription() + " shutdown");
			} else {
				alertManager.error("Shutdown rejected by the server");
			}
			
		} catch (XmlRpcException e) {
			alertManager.error("Shutdown failed: could not contact server");
		} finally {
			client.destroy();
		}
	}
	
	public String getConfirmShutdownMessage() {
		return messages.format("confirm-shutdown", server.getServerDescription());
	}
}
