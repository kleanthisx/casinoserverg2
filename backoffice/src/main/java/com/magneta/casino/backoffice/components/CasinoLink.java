package com.magneta.casino.backoffice.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.dom.Element;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.util.LinkGenerator;

@SupportsInformalParameters
public class CasinoLink {
	
	@Inject
	private ComponentResources resources;

	@Parameter(required=true, defaultPrefix = BindingConstants.LITERAL)
	private String page;
	
	@Parameter(value="false", required=false)
	private boolean disabled;
	
	@Parameter(required=false)
	private String target;
	
	@Parameter(required=false)
	private Object[] parameters;
	
	void beginRender(MarkupWriter writer) {
		boolean renderLink = !disabled;

		if (renderLink) {
			Element a = writer.element("a");
			
			a.attribute("href", LinkGenerator.getPageUrl(page, parameters));

			if (target != null) {
				a.attribute("target", target);
			}
			
			resources.renderInformalParameters(writer);
		}
	}
	
	void afterRender(MarkupWriter writer) {
		writer.end();
	}
}
