package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.PageRedirectException;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class EditPaymentType extends SecurePage {
	
	private static final Logger log = LoggerFactory.getLogger(EditPaymentType.class);
	
	@InjectPage("PaymentTypes")
	public abstract PaymentTypes getPaymentTypes();
	
	@InjectObject("service:casino.common.PaymentType")
	public abstract PaymentTypeService getPaymentTypeService();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Component(id="editPaymentTypeForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
			public abstract Form getEditPaymentTypeForm();

    @Component(id="paymentType",
    		bindings={"value=PaymentType"})
    public abstract Insert getPaymentTypeField();
    
    @Component(id="enabled",
			bindings={"value=enabled","disabled=!allowWrite()"})
			public abstract Checkbox getEnabledCheckBox();
    
    @Component(id="submitButton")
    public abstract Submit getSubmitButton();
    
    @Component(id="cancelButton")
    public abstract Button getCancelButton();
    
    @Persist("client:page")
	public abstract String getPaymentType();
	public abstract void setPaymentType(String paymentType);

	public abstract Boolean getEnabled();
	public abstract void setEnabled(Boolean enabled);
	
	public PaymentTypeBean paymentTypeBean;
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null) && (params.length > 0)) {
			setPaymentType((String)params[0]);
		}
	}
	
	@Override
	public void prepareForRender(IRequestCycle cycle) {
		try {
			paymentTypeBean = getPaymentTypeService().getPaymentType(getPaymentType());
			
			if(paymentTypeBean == null){
				return;
			}
			setEnabled(paymentTypeBean.isEnabled());
		} catch (ServiceException e) {
			log.error("Error while getting payment types");
		}
	}
	
    public void onSubmit(IRequestCycle cycle) throws Exception {
    	PaymentTypeBean bean = new PaymentTypeBean();
    	bean.setPaymentType(paymentTypeBean.getPaymentType());
    	bean.setEnabled(getEnabled());
    	getPaymentTypeService().updatePaymentType(bean);

    	throw new PageRedirectException("PaymentTypes");
    }
    
    public void onCancel(IRequestCycle cycle) {
    	throw new PageRedirectException("PaymentTypes");
    }
}
