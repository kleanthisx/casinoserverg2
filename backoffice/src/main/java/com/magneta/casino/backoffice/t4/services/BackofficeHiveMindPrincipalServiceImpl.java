package com.magneta.casino.backoffice.t4.services;

import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.UserAuthenticationBean;

public class BackofficeHiveMindPrincipalServiceImpl implements BackofficePrincipalService {

	private BackofficePrincipalService _realService;
	
	public void setPrincipalService(BackofficePrincipalService realService) {
		this._realService = realService;
	}
	
	@Override
	public ExtendedPrincipalBean getPrincipal() {
		return _realService.getPrincipal();
	}

	@Override
	public void onUserAuthenticated(UserAuthenticationBean authenticatedUser)
			throws ServiceException {
		_realService.onUserAuthenticated(authenticatedUser);
	}

	@Override
	public void onUserLogout() {
		_realService.onUserLogout();
	}

	@Override
	public void pushSystem() {
		_realService.pushSystem();
	}

	@Override
	public void popSystem() {
		_realService.popSystem();
	}

	@Override
	public void resetSystem() {
		_realService.resetSystem();
	}

	@Override
	public void setThreadPrincipal(ExtendedPrincipalBean principal) {
		_realService.setThreadPrincipal(principal);
	}

}
