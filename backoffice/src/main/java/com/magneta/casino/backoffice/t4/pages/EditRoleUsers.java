package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.callback.ExternalCallback;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.link.DirectLink;
import org.apache.tapestry.valid.FieldLabel;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.RoleUserBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.backoffice.t4.models.RoleUsersTableModel;
import com.magneta.casino.backoffice.t4.utils.ConfirmContext;
import com.magneta.casino.services.RoleService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.RoleBean;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public abstract class EditRoleUsers extends SecurePage implements PageBeginRenderListener {

	private static final Logger log = LoggerFactory.getLogger(EditRoleUsers.class);
	
	@InjectObject("service:casino.common.UserDetails")
	public abstract UserDetailsService getDetailsService();
	
	@InjectObject("service:casino.common.Role")
	public abstract RoleService getRoleService();
	
	@Persist("client:page")
	public abstract int getRoleId();
	public abstract void setRoleId(int roleId);

	public abstract String getRoleDesc();
	public abstract void setRoleDesc(String roleDesc);
	
	@Override
	public void pageBeginRender(PageEvent event){
		try {
			RoleBean role = getRoleService().getRole(getRoleId());
			setRoleDesc(role.getDescription());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	

	public abstract String getUsername();

	@InjectPage("GeneralConfirmation")
	public abstract GeneralConfirmation getConfirmPage();

	@Bean
	public abstract EvenOdd getEvenOdd();

	public abstract RoleUserBean getCurrUser();

	@Component
	public abstract ErrorDisplay getErrorDisplay();

	@Component(id="usersTable", type="contrib:Table",
			bindings={"source=ognl:roleUsersTableModel","columns=literal:Username, Email, !delete",
			"pageSize=40","row=currUser","initialSortColumn=literal:Username","initialSortOrder=true","rowsClass=ognl:beans.evenOdd.next"})
	public abstract Table getUsersTable();

	public RoleUsersTableModel getRoleUsersTableModel(){
		return new RoleUsersTableModel(getRoleId());
	}
	
	@Component(bindings={"listener=listener:deleteUser","parameters={currUser.userId,roleId,currUser.username}"})
	public abstract DirectLink getDeleteUser();

	@Component(bindings={"listener=listener:addUser","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
	public abstract Form getAddUserForm();

	@Component(bindings={"field=component:username"})
	public abstract FieldLabel getUsernameLabel();

	@Component(id="username",
			bindings={"value=username","disabled=!allowWrite()","displayName=message:Username","validators=validators:required"})
			public abstract TextField getUsernameField();

	@Component(bindings={"listener=listener:onBack"})
	public abstract DirectLink getBackLink();

	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getAddSubmit();

	public void onBack(IRequestCycle cycle){
		redirectToPage("RoleAdministration");
	}

	public void addUser(IRequestCycle cycle){
		ValidationDelegate delegate = getErrorDisplay().getDelegate();

		if (delegate.getHasErrors()){
			return;
		}
		addUser();
	}

	public static class RemoveUserRoleConfirmContext extends ConfirmContext {
		private static final long serialVersionUID = 8772903408863576269L;
		private final Long userId;
		private final int roleId;
		private final Long loggedInUserId;

		public RemoveUserRoleConfirmContext(String okMsg, String cancelMsg,String confirmMsg, ICallback callback,Long userId,int roleId,Long loggedInUserId) {
			super(okMsg, cancelMsg, confirmMsg, callback);
			this.userId = userId;
			this.roleId = roleId;
			this.loggedInUserId = loggedInUserId;
		}

		@Override
		public void onOk(IRequestCycle cycle, ValidationDelegate delegate)throws ServiceException {
			if(userId == null){
				return;
			}

			if (userId.equals(loggedInUserId)) {
				delegate.record(null, "You cannot delete yourself");
				return;
			}

			Connection dbConn = ConnectionFactory.getConnection();

			if (dbConn == null){
				delegate.record(null, "No database connection");
				return;
			}

			PreparedStatement statement = null;

			try {            
				String sql = 
						"DELETE FROM user_roles" +
								" WHERE user_id = ?" +
								" AND role_id = ?";

				statement = dbConn.prepareStatement(sql);
				statement.setLong(1, userId);
				statement.setInt(2, roleId);

				if (statement.executeUpdate() == 1){
					returnToPage(cycle);
					return;
				}

			} catch (SQLException e) {
				log.error("Error while removing user from role.",e);
				delegate.record(null, "Unable to remove user from role: Database error.");
				return;
			} finally{
				DbUtil.close(statement);
				DbUtil.close(dbConn);
			}
			delegate.record(null, "User was not removed from role.");
			return;
		}
	}

	public void deleteUser(IRequestCycle cycle,Long userId,int roleId,String username){
		ConfirmContext confirmContext = new RemoveUserRoleConfirmContext(this.getMsg("ok"), this.getMsg("cancel"), this.getMsg("confirmMessage") + " " + username + " from role " + getRoleDesc() + " ?" ,new ExternalCallback(this, new Object[]{roleId, getRoleDesc()}),userId,roleId,getPrincipal().getUserId());
		GeneralConfirmation confirmPage = getConfirmPage();
		confirmPage.setConfirmContext(confirmContext);
		getRequestCycle().activate(confirmPage);
	}

	private boolean addUser(){
		SearchContext<UserDetailsBean> searchContext = FiqlContextBuilder.create(UserDetailsBean.class,
					"userName==?;userType==?",
					getUsername(), UserTypeEnum.STAFF_USER.getId());
		
		List<UserDetailsBean> usersList = null;
		
		try {
			usersList = getDetailsService().getUsers(searchContext, 0, 0, null).getResult();
		} catch (ServiceException e) {
			log.error("Error while getting user details",e);
			return false;
		}
		
		if(usersList.size() <= 0){
			getErrorDisplay().getDelegate().record(getMsg("no-user"),null);
			return false;
		}
		
		Long userId = usersList.get(0).getId();
		
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet res = null;

		if (dbConn == null){
			getErrorDisplay().getDelegate().setFormComponent(null);
			getErrorDisplay().getDelegate().record(getMsg("db-error"), null);
			return false;
		}
		
		try{
			String sql = 
				"INSERT INTO user_roles(user_id,role_id)" +
				" VALUES(?,?)";

			statement = dbConn.prepareStatement(sql);
			statement.setLong(1, userId);
			statement.setInt(2, getRoleId());

			if (statement.executeUpdate() == 1){
				return true;
			}

		} catch (SQLException e) {
			log.error("Error while adding user to role.",e);
		} finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return false;
	}

	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setRoleId((Integer)params[0]);
		}
	}
}
