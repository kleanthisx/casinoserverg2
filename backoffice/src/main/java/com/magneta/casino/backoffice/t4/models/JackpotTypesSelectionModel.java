package com.magneta.casino.backoffice.t4.models;

import org.apache.tapestry.form.IPropertySelectionModel;

public class JackpotTypesSelectionModel implements IPropertySelectionModel {

	private static final String[] JACKPOT_TYPE_NAMES = {"Game", "Mega", "Corporate"};
	private static final int[] JACKPOT_TYPE_NO = {1,2,3};

	@Override
	public String getLabel(int arg0) {
        return JACKPOT_TYPE_NAMES[arg0];
    }

    @Override
	public Object getOption(int arg0) {
        return JACKPOT_TYPE_NO[arg0];
    }

    @Override
	public int getOptionCount() {
        return JACKPOT_TYPE_NO.length;
    }

    @Override
	public String getValue(int arg0) {
        return String.valueOf(JACKPOT_TYPE_NO[arg0]);
    }

    @Override
	public Object translateValue(String arg0) {
        return Integer.parseInt(arg0);
    }
    
    @Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
