package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InitialValue;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.t4.models.GamesSelectionModel;
import com.magneta.casino.backoffice.t4.models.PayoutRatesOrderBySelectionModel;

public abstract class PayoutRatesPerGameReportOptions extends DateReportOptions{

	public abstract String getSortBy();
	
	@Component(id = "games",
			bindings = {"value=game","model=ognl:gamesSelectionModel", 
			"displayName=message:Game"})
	public abstract PropertySelection getGameSelection();

	public GamesSelectionModel getGamesSelectionModel(){
		return new GamesSelectionModel(true);
	}
	
	@Component(id = "gamesLabel",
			bindings = {"field=component:games"})
	public abstract FieldLabel getGameLabel();
	
	@Component(id = "includeJackpotsCB",
			bindings={"value=IncludeJackpots", "displayName=message:IncludeJackpots"})
	public abstract Checkbox getIncludeJackpotsCB();
	
	@Component(id = "includeJackpotsCBLabel",
			bindings={"field=component:includeJackpotsCB"})
	public abstract FieldLabel getIncludeJackpotsCBLabel();
	
	@Component(bindings = {"value=sortBy","model=ognl:payoutRatesOrderBySelectionModel"})
	public abstract PropertySelection getSortBySelect();
	
	public PayoutRatesOrderBySelectionModel getPayoutRatesOrderBySelectionModel(){
		return new PayoutRatesOrderBySelectionModel();
	}
	
	@InitialValue("true")
	public abstract boolean getIncludeJackpots();
	
	public abstract int getGame();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);
	
		if (getGame() != -1){
			reportParams.put("game_type_id", getGame());
		}
		reportParams.put("include_jackpots", getIncludeJackpots());
		reportParams.put("sort_by", getSortBy());
	}
}
