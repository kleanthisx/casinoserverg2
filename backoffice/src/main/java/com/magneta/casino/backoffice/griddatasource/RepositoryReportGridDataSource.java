package com.magneta.casino.backoffice.griddatasource;

import java.util.List;

import com.magneta.casino.backoffice.managers.reporting.ReportRepository;
import com.magneta.casino.backoffice.managers.reporting.RepositoryReport;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.search.FiqlContextBuilder;
import com.magneta.casino.services.search.SearchContext;
import com.magneta.casino.services.search.SortContext;

public class RepositoryReportGridDataSource extends GenericGridDataSource<RepositoryReport> {

	private final ReportRepository repository;
	private final PrincipalService principalService;
	
	public RepositoryReportGridDataSource(ReportRepository repository,PrincipalService principalService) {
		this.repository = repository;
		this.principalService = principalService;
	}
	
	@Override
	public int getAvailableRows() {
		SearchContext<RepositoryReport> searchContext = FiqlContextBuilder.create(RepositoryReport.class, "userId==?", principalService.getPrincipal().getUserId());
		try {
			return repository.getReportsSize(searchContext);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<RepositoryReport> getData(int limit, int offset,
			SortContext<RepositoryReport> sortConstraints) throws Exception {
		SearchContext<RepositoryReport> searchContext = FiqlContextBuilder.create(RepositoryReport.class, "userId==?", principalService.getPrincipal().getUserId());
		try {
			return repository.getReports(searchContext);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

}
