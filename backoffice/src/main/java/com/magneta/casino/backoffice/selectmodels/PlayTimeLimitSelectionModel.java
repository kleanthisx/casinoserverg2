package com.magneta.casino.backoffice.selectmodels;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;

public class PlayTimeLimitSelectionModel<E> extends AbstractSelectModel{

	String[] labels;
	Integer[] values;
	
	public PlayTimeLimitSelectionModel<Integer> create(String name) {
		if(name.equalsIgnoreCase("days")){
			labels = new String[28];
			values = new Integer[28];
		}else if(name.equalsIgnoreCase("hours")){
			labels = new String[24];
			values = new Integer[24];
		}else if(name.equalsIgnoreCase("minutes")){
			labels = new String[12];
			values = new Integer[12];
		}
		
		for (int i=0;i<values.length;i++){
			if(name.equalsIgnoreCase("minutes")){
				labels[i] = i*5 + "";
				values[i] = i*5;
			}else{
				labels[i] = i + "";
				values[i] = i;
			}
		}
		return new PlayTimeLimitSelectionModel<Integer>(labels, values);
	}
	
	public PlayTimeLimitSelectionModel(String[] labels, Integer[] values){
		this.labels = labels;
		this.values = values;
	}
	
	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> result = new ArrayList<OptionModel>();
		for(int i=0;i<values.length;i++){
			result.add(new OptionModelImpl(labels[i].toString(), values[i]));
		}
		return result;
	}
	
}
