package com.magneta.casino.backoffice.managers.reporting;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

/**
 * Service responsible for retrieving reports.
 * @author anarxia
 *
 */
public interface ReportLocatorService {
	JasperReport getReport(String report) throws JRException;
}