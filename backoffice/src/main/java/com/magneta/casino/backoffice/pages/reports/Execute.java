package com.magneta.casino.backoffice.pages.reports;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JRException;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.backoffice.managers.reporting.impl.ReportStreamResponse;
import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public class Execute {
	
	@Inject
	private BackofficePrincipalService principalService;

	@Inject
	private ReportService reportService;
	
	@Inject
	private Locale locale;
	
	@Inject
	private Request request;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public Object activate(String report, ReportFormat format) throws JRException {
		TimeZone tz = ((BackofficePrincipalBean)principalService.getPrincipal()).getTimeZone();
		
		Map<String,Object> params = new HashMap<String,Object>();
		
		for (String paramName: request.getParameterNames()) {
			String paramValue = request.getParameter(paramName);
			
			if (paramValue == null || paramValue.isEmpty()) {
				continue;
			}
			
			params.put(paramName, paramValue);
		}
		
		ReportRequest request = reportService.createRequest(report, format, tz, locale, params);
		
		return new ReportStreamResponse(request, reportService);
	}
}
