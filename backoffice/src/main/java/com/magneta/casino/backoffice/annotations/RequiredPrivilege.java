package com.magneta.casino.backoffice.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.magneta.casino.services.enums.PrivilegeEnum;

@Retention(RetentionPolicy.RUNTIME)
@Target( {ElementType.TYPE, ElementType.METHOD})
public @interface RequiredPrivilege {
	
	public PrivilegeEnum value() default PrivilegeEnum.BACKOFFICE_ENTRY;
	public PrivilegeEnum[] and() default {};
	public PrivilegeEnum[] or() default {};
	
	public boolean write() default false;
}
