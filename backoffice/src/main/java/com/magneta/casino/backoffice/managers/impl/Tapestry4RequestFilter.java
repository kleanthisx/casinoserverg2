package com.magneta.casino.backoffice.managers.impl;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hivemind.ErrorHandler;
import org.apache.hivemind.impl.DefaultErrorHandler;
import org.apache.hivemind.service.ThreadLocale;
import org.apache.tapestry.Constants;
import org.apache.tapestry.IEngine;
import org.apache.tapestry.engine.RequestCycle;
import org.apache.tapestry.engine.RequestCycleEnvironment;
import org.apache.tapestry.record.PropertyPersistenceStrategySource;
import org.apache.tapestry.services.AbsoluteURLBuilder;
import org.apache.tapestry.services.Infrastructure;
import org.apache.tapestry.services.RequestGlobals;
import org.apache.tapestry.util.QueryParameterMap;
import org.apache.tapestry.web.ServletWebRequest;
import org.apache.tapestry.web.ServletWebResponse;
import org.apache.tapestry.web.WebRequest;
import org.apache.tapestry.web.WebResponse;
import org.apache.tapestry5.services.HttpServletRequestFilter;
import org.apache.tapestry5.services.HttpServletRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.backoffice.services.T5ServiceLocator;

/**
 * Filter which sets up the tapestry 4 RequestGlobals
 * to allow us to use allow tapesty 4 services from a tapestry 5 context
 */
public class Tapestry4RequestFilter implements HttpServletRequestFilter {
	
	private static final Logger log = LoggerFactory
			.getLogger(Tapestry4RequestFilter.class);

	private static class DummyEngine implements IEngine {

		private final Infrastructure infrastructure;
		private Locale locale;
		
		public DummyEngine(Infrastructure infrastructure) {
			this.infrastructure = infrastructure;
		}
		
		@Override
		public void service(WebRequest request, WebResponse response)
				throws IOException {
			return;
		}

		@Override
		public Locale getLocale() {
			return locale;
		}

		@Override
		public void setLocale(Locale value) {
			this.locale = value;
			infrastructure.setLocale(locale);
		}

		@Override
		public String getOutputEncoding() {
			return infrastructure.getOutputEncoding();
		}

		@Override
		public Infrastructure getInfrastructure() {
			return infrastructure;
		}
		
	}

	private void setupT4RequestGlobals(HttpServletRequest request, HttpServletResponse response) {
        ThreadLocale threadLocal = ServiceLocator.getService(ThreadLocale.class);   
        threadLocal.setLocale(T5ServiceLocator.getService(org.apache.tapestry5.ioc.services.ThreadLocale.class).getLocale());
        
        RequestGlobals reqGlobals = ServiceLocator.getService(org.apache.tapestry.services.RequestGlobals.class);
        reqGlobals.store(request, response);
        reqGlobals.store(new ServletWebRequest(request, response), new ServletWebResponse(response));
        
        Infrastructure infrastructure = ServiceLocator.getService(Infrastructure.class);
        request.setAttribute(Constants.INFRASTRUCTURE_KEY, infrastructure);

        IEngine engine = new DummyEngine(infrastructure);
        engine.setLocale(threadLocal.getLocale());
        
        QueryParameterMap parameters = new QueryParameterMap();        
        ErrorHandler errorHandler = new DefaultErrorHandler();
        
        RequestCycleEnvironment environment = new RequestCycleEnvironment(errorHandler, infrastructure, 
        						ServiceLocator.getService(PropertyPersistenceStrategySource.class), 
        						ServiceLocator.getService(AbsoluteURLBuilder.class));

        RequestCycle requestCycle = new RequestCycle(engine, parameters, null, environment);
        reqGlobals.store(requestCycle);        
	}

	private void tearDownT4RequestGlobals(HttpServletRequest request) {
		Infrastructure infrastructure = (Infrastructure)request.getAttribute(Constants.INFRASTRUCTURE_KEY);
		if (infrastructure == null) {
			return;
		}
		
		infrastructure.getApplicationStateManager().flush();
		request.removeAttribute(Constants.INFRASTRUCTURE_KEY);
	}

	@Override
	public boolean service(HttpServletRequest request,
			HttpServletResponse response, HttpServletRequestHandler handler)
					throws IOException {

		log.debug("T5 Request: {}", request.getRequestURI());
		ServiceLocator.getRegistry().setupThread();
		try {
			setupT4RequestGlobals(request, response);
			try {
				return handler.service(request, response);
			} finally {
				tearDownT4RequestGlobals(request);
			}
		} finally {
			ServiceLocator.getRegistry().cleanupThread();
			log.debug("End T5 Request: {}", request.getRequestURI());
		}
	}
}
