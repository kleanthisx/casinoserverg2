package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.GameServerSelectModel;
import com.magneta.casino.services.GameTableService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameTableBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public class GameTable {

	@Inject
	private GameTableService gameTableService;
	
	@Inject
	private GamesService gameService;
	
	@Inject
	private ServersService serverService;
	
	@Property
	private Long tableId;
	
	@Property
	private GameTableBean table;
	
	@Cached
	public GameBean getGame() throws ServiceException {
		return gameService.getGame(table.getGameId());
	}
	
	public GameServerSelectModel getServerSelectModel() {
		return new GameServerSelectModel(serverService);
	}
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Long tableId) {
		this.tableId = tableId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Long onPassivate() {
		return this.tableId;
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepareForm() throws ServiceException {
		table = gameTableService.getTable(tableId);		
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public void onSuccessForm() throws ServiceException {
		gameTableService.moveTable(tableId, table.getServerId());
	}
}
