package com.magneta.casino.backoffice.coercers;

import java.util.TimeZone;

import org.apache.tapestry5.ioc.services.Coercion;

public class TimeZoneToStringCoercion implements Coercion<TimeZone,String> {

	@Override
	public String coerce(TimeZone tz) {
		if (tz == null) {
			return null;
		}
		
		return tz.getID();
	}
}
