package com.magneta.casino.backoffice.coercers;

import org.apache.tapestry5.ioc.services.Coercion;

import com.magneta.casino.services.beans.ServerBean;

public class ServerBeanToStringCoercion implements Coercion<ServerBean, String> {

	@Override
	public String coerce(ServerBean arg0) {
		if (arg0 == null) {
			return null;
		}
		
		return arg0.getServerId().toString();
	}
}
