package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.GameClientService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameClientBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.SYSTEM_ADMIN)
public class GameClient {

	@Inject
	private GameClientService gameClientService;

	@Property
	private GameClientBean gameClient;
	
	@Property
	private Integer clientId;
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Integer clientId) {
		this.clientId = clientId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Integer onPassivate() {
		return this.clientId;
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepareForm() throws ServiceException{
		if (clientId == null){
			gameClient = new GameClientBean();
		}else{
			gameClient = gameClientService.getGameClient(clientId);
		}
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() throws ServiceException {
		if(clientId == null) {
			gameClientService.addGameClient(gameClient);
			clientId = gameClient.getClientId();
		}else{
			gameClientService.updateGameClient(gameClient);
		}
		
		return GameClients.class;
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return GameClients.class;
	}
	
	public boolean isAllowIdEdit() {
		return clientId != null;
	}
}
