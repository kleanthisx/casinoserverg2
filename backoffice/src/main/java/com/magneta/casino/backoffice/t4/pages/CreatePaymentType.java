package com.magneta.casino.backoffice.t4.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.StringPropertySelectionModel;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextField;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.components.ErrorDisplay;
import com.magneta.casino.services.PaymentTypeService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.PaymentTypeBean;
import com.magneta.casino.services.enums.PaymentMethod;
import com.magneta.casino.services.enums.PrivilegeEnum;

import edu.emory.mathcs.backport.java.util.Arrays;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class CreatePaymentType extends SecurePage{

	public abstract String getPaymentType();
	public abstract void setPaymentType(String paymentType);  
	public abstract Double getMinDeposit();
	public abstract void setMinDeposit(Double minDeposit);
	public abstract Double getMinWithdrawal();
	public abstract void setMinWithdrawal(Double minWithdrawal);
	public abstract double getDepositFee();
	public abstract void setDepositFee(double depositFee);
	public abstract double getWithdrawalFee();
	public abstract void setWithdrawalFee(double withdrawalFee);
	public abstract double getWithdrawalFeeRate();
	public abstract void setWithdrawalFeeRate(double withdrawalFeeRate);
	public abstract double getDepositFeeRate();
	public abstract void setDepositFeeRate(double depositFeeRate);
	public abstract Boolean getEnabled();
	public abstract void setEnabled(Boolean enabled);
	
	@InjectPage("PaymentTypes")
	public abstract PaymentTypes getPaymentTypes();
	
	@InjectObject("service:casino.common.PaymentType")
	public abstract PaymentTypeService getPaymentTypeService();
	
	@Component
	public abstract ErrorDisplay getErrorDisplay();
	
	@Component(id="createPaymentTypeForm",
			bindings={"listener=listener:onSubmit","cancel=listener:onCancel","delegate=errorDisplay.beans.delegate","clientValidationEnabled=true"})
			public abstract Form getCreatePaymentTypeForm();
	
    @Component(id="minDeposit",
    		bindings={"value=minDeposit","displayName=message:min-Deposit","validators=validators:required"})
    public abstract TextField getMinDepositText();
    
    @Component(id="minWithdrawal",
    		bindings={"value=minWithdrawal","displayName=message:min-withdrawal","validators=validators:required"})
    public abstract TextField getMinWithdrawalText();
    
    @Component(id="depositFee",
    		bindings={"value=depositFee"})
    public abstract TextField getDepositFeeText();
    
    @Component(id="depositFeeRate",
    		bindings={"value=depositFeeRate"})
    public abstract TextField getDepositFeeRateText();
    
    @Component(id="withdrawalFee",
    		bindings={"value=withdrawalFee"})
    public abstract TextField getWithdrawalFeeText();
    
    @Component(id="withdrawalFeeRate",
    		bindings={"value=withdrawalFeeRate"})
    public abstract TextField getWithdrawalFeeRateText();
    
    @Component(id="enabled",
			bindings={"value=enabled","disabled=!allowWrite()"})
			public abstract Checkbox getEnabledCheckBox();
    
    @Component(id="createButton")
    public abstract Submit getCreateButton();
    
    @Component(id="cancelButton")
    public abstract Button getCancelButton();
    
    private boolean typeExists(String type, Iterable<PaymentTypeBean> existing) {
    	for (PaymentTypeBean b: existing) {
    		if (b.getPaymentType().equals(type)) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
	public StringPropertySelectionModel getPaymentTypeNamesModel() throws ServiceException {
		PaymentMethod[] methods = PaymentMethod.values();
		List<String> types = new ArrayList<String>(methods.length);
		
		List<PaymentTypeBean> existing = this.getPaymentTypeService().getPaymentTypes(null, 0, 0, null).getResult();
		
		for (int i=0; i < methods.length; i++) {
			if (!typeExists(methods[i].name(), existing)) {
				types.add(methods[i].name());
			}
		}
		
		String[] t = types.toArray(new String[0]);
		
		Arrays.sort(t);
		
		return new StringPropertySelectionModel(t);
	}
	
    public void onSubmit(IRequestCycle cycle) throws Exception {
    	PaymentTypeBean bean = new PaymentTypeBean();
    	PaymentTypes paymentTypes = getPaymentTypes();
    	bean.setPaymentType(getPaymentType());
    	bean.setEnabled(getEnabled());
    	getPaymentTypeService().createPaymentType(bean);
    	cycle.activate(paymentTypes);
    }
    
    public void onCancel(IRequestCycle cycle){
    	PaymentTypes paymentMethods = getPaymentTypes();
    	cycle.activate(paymentMethods);
    }
}
