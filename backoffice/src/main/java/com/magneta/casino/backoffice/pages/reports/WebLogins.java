package com.magneta.casino.backoffice.pages.reports;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JRException;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.beans.BackofficePrincipalBean;
import com.magneta.casino.backoffice.managers.reporting.ReportFormat;
import com.magneta.casino.backoffice.managers.reporting.ReportRepository;
import com.magneta.casino.backoffice.managers.reporting.ReportRequest;
import com.magneta.casino.backoffice.managers.reporting.ReportService;
import com.magneta.casino.backoffice.selectmodels.ApplicationSelectModel;
import com.magneta.casino.backoffice.selectmodels.ReportFormatSelectModel;
import com.magneta.casino.backoffice.selectmodels.TimeZoneSelectModel;
import com.magneta.casino.backoffice.services.BackofficePrincipalService;
import com.magneta.casino.backoffice.valueencoders.TimezoneEncoder;
import com.magneta.casino.common.utils.DateUtils;
import com.magneta.casino.services.TimeZoneService;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public class WebLogins {
	
	@Inject
	private BackofficePrincipalService principalService;
	
	@Inject
	private ReportService reportService;
	
	@Inject
	private ReportRepository reportRepository;
	
	@Inject
	private TimeZoneService timeZoneService;

	@Inject
	private Locale locale;

	@Property
	private ReportFormat reportFormat;
	
	@Property
	private TimeZone reportTimeZone;
	
	@Property
	private String application;

	@Property
	private Date minDate;
	
	@Property
	private Date maxDate;
	
	public TimezoneEncoder getTimezoneEncoder() {
		return new TimezoneEncoder();
	}
	
	public TimeZoneSelectModel getTimeZoneSelectModel() {
		return new TimeZoneSelectModel(timeZoneService);
	}

	public ReportFormatSelectModel getReportFormatSelectModel() {
		return new ReportFormatSelectModel();
	}
	
	public ApplicationSelectModel getApplicationSelectModel() {
		return new ApplicationSelectModel();
	}
	
	@OnEvent(value=EventConstants.PREPARE, component="reportForm")
	public void prepareReportForm() {
		this.reportTimeZone = ((BackofficePrincipalBean)principalService.getPrincipal()).getTimeZone();
		this.reportFormat = ReportFormat.HTML;
	}
	
	private Date normalizeEndDate(Date d) {
		if (d == null) {
			return null;
		}

		TimeZone tz = ((BackofficePrincipalBean)principalService.getPrincipal()).getTimeZone();
		
		Date newDate = DateUtils.getNormalizedDate(tz, d);
		
		Calendar cal = Calendar.getInstance(tz);
		cal.setTime(newDate);
		cal.add(Calendar.DATE, 1);
		
		return cal.getTime();
	}
	
	@OnEvent(value=EventConstants.SUCCESS, component="reportForm")
	public Object onSubmit() throws JRException {
		Map<String,Object> params = new HashMap<String,Object>();
		
		if (minDate != null) {
			params.put("min_date", minDate);
		}
		
		if (maxDate != null) {
			params.put("max_date", normalizeEndDate(maxDate));
		}
		
		if (application != null) {
			params.put("application", application);
		}
		
		ReportRequest request = reportService.createRequest("web_logins", reportFormat, reportTimeZone, locale, params);

		reportRepository.addScheduledReportRequest(request);
		return Index.class;
	}
}
