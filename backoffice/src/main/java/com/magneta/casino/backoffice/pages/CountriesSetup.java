package com.magneta.casino.backoffice.pages;

import java.util.List;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.InjectService;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class CountriesSetup {

	@Component(id="countryForm")
	private Form countryForm;

	@InjectService("CountryService")
	private CountryService countryService;

	@Property
	private List<CountryBean> countries;

	@Property
	private CountryBean countryBean;
	
	@Cached
	public List<CountryBean> getDbCountries() throws ServiceException {
		return countryService.getCountries();
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepare() throws ServiceException{
		countries = countryService.getCountries();
	}

	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccess() throws ServiceException, ConditionalUpdateException{	
		for(int i=0;i<countries.size();i++){
			if(!countries.get(i).getName().equals(getDbCountries().get(i).getName()) ||
					countries.get(i).isEnabled() != getDbCountries().get(i).isEnabled()){
				countryService.update(countries.get(i));
			}
		}
		return null;
	}
}
