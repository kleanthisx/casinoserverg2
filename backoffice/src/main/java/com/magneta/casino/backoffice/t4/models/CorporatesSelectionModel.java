package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class CorporatesSelectionModel implements IPropertySelectionModel {
	
    private List<Long> ids;
    private List<String> usernames;
        
    public CorporatesSelectionModel(boolean onlySupers){
    	ids = new ArrayList<Long>();
        usernames = new ArrayList<String>();
             
        	ids.add(new Long(-1));
        	usernames.add("--Please select--");
        
        
        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return;
		}
        
        try{
        	String sql = 
        		 "SELECT DISTINCT users.user_id, username"+
            	 " FROM affiliates"+
            	 " INNER JOIN users ON affiliates.affiliate_id = users.user_id AND users.is_active = TRUE"+
            	 " WHERE 1 = 1"+
            	 (onlySupers? " AND NOT EXISTS (SELECT user_id FROM affiliate_users WHERE affiliate_users.user_id = affiliates.affiliate_id )" : "")+
            	 " ORDER BY username ASC"; 

        	statement = dbConn.prepareStatement(sql);
        	
        	res = statement.executeQuery();

        	while (res.next()){
        		ids.add(res.getLong("user_id"));
        		usernames.add(res.getString("username"));
        	}
        } catch (SQLException e) {
        	throw new RuntimeException("Error while getting corporates.", e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
    }
    
    @Override
	public String getLabel(int arg0) {
        return usernames.get(arg0);
    }

    @Override
	public Object getOption(int arg0) {
        return ids.get(arg0);
    }

    @Override
	public int getOptionCount() {
        return ids.size();
    }

    @Override
	public String getValue(int arg0) {
        return String.valueOf(ids.get(arg0));
    }

    @Override
	public Object translateValue(String arg0) {
        return Long.parseLong(arg0);
    }
    
    @Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
