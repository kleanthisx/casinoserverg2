package com.magneta.casino.backoffice.beans;

import java.io.Serializable;

public class DbServerBean implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 203258736514240902L;
	private String name;
	private Double loadAvg;
	private Long uptime;
	private String size;

	public DbServerBean() {
		super();
	}
	
	public DbServerBean(DbServerBean orig) {
		this.name = orig.getName();
		this.loadAvg = orig.getLoadAvg();
		this.uptime = orig.getUptime();
		this.size = orig.getSize();
	}
	
	@Override
	public Object clone() {
		return new DbServerBean(this);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getLoadAvg() {
		return loadAvg;
	}
	public void setLoadAvg(Double loadAvg) {
		this.loadAvg = loadAvg;
	}
	public Long getUptime() {
		return uptime;
	}
	public void setUptime(Long uptime) {
		this.uptime = uptime;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}
