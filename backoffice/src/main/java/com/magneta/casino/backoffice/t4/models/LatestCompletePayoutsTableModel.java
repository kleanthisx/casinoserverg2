package com.magneta.casino.backoffice.t4.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;
import com.magneta.administration.beans.PayoutTransactionBean;

import com.magneta.administration.utils.DepositsWithdrawalsUtils;

public class LatestCompletePayoutsTableModel implements IBasicTableModel {
	
	@Override
	public Iterator<PayoutTransactionBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		List<PayoutTransactionBean> completeWithdrawals = DepositsWithdrawalsUtils.getCompleteWithdrawals(offset, limit, sortCol, sortAsc);

		if(completeWithdrawals == null){
			return new ArrayList<PayoutTransactionBean>().iterator();
		}
		
		Iterator<PayoutTransactionBean> it = completeWithdrawals.iterator();	


		return it;
	}

	@Override
	public int getRowCount() {
		return DepositsWithdrawalsUtils.getCompleteWithdrawalsCount();
	}
}
