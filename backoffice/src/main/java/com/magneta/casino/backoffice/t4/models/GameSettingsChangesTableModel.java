package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.GameSettingChangeBean;
import com.magneta.administration.commons.services.ServiceLocator;
import com.magneta.casino.games.templates.GameTemplateInfo;
import com.magneta.casino.games.templates.GameTemplateInfoFactory;
import com.magneta.casino.games.templates.GameTemplateSetting;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GameSettingsChangesTableModel implements IBasicTableModel {
	
	private static final Logger log = LoggerFactory.getLogger(GameSettingsChangesTableModel.class);
	
	@Override
	public Iterator<GameSettingChangeBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		
		GamesService gameService = ServiceLocator.getService(GamesService.class);
		
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<GameSettingChangeBean> it = null;

		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
		
		String sortColumnName = "Username";

        if (sortCol != null){
            sortColumnName = sortCol.getColumnName();
        }
        
        if (("Username").equals(sortColumnName))
            sortColumnName = "username";
        else if (("SettingName").equals(sortColumnName))
            sortColumnName = "setting_description";
        else if (("Game").equals(sortColumnName))
            sortColumnName = "game_type_variant";
        else
            sortColumnName = "change_time";
		
		try {
			String sql = 
				"(SELECT 0 As change_type, game_id, setting_key, users.username," +
				" change_time, new_value, old_value"+
				" FROM game_settings_changes"+
				" LEFT OUTER JOIN users ON game_settings_changes.change_user = users.user_id"+
				" WHERE new_value IS DISTINCT FROM old_value"+
				" UNION"+
				" SELECT 1, game_configs.game_id, 'Created Game Configuration', users.username,"+
				" game_configs.config_date, CAST(game_configs.config_id AS VARCHAR), NULL"+
				" FROM game_configs"+
				" LEFT OUTER JOIN users ON game_configs.modified_by = users.user_id" +
				" UNION" +
				" SELECT 2, game_config_changes.game_id AS game_id, 'Applied New Configuration', users.username," +
				" game_config_changes.change_time, game_config_changes.new_config_id, game_config_changes.old_config_id" +
				" FROM game_config_changes" +
				" LEFT OUTER JOIN users ON game_config_changes.modifier = users.user_id)"+
				" ORDER BY "+sortColumnName+(sortAsc? " ASC": " DESC")+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			ArrayList<GameSettingChangeBean> myArr = new ArrayList<GameSettingChangeBean>();
			GameSettingChangeBean currRow;

			while (res.next()){
				currRow = new GameSettingChangeBean();
				
				int gameId = res.getInt("game_id");
				
				if (res.getInt("change_type") > 0) {
					currRow.setGameId(gameId);
				}
				currRow.setSettingName(res.getString("setting_key"));
				
				try {
					GameTemplateSetting setting = null;
					
					GameBean game = gameService.getGame(gameId);
					if (game != null) {
						currRow.setGame(game.getName());
						GameTemplateInfo gameTemplate = GameTemplateInfoFactory.getGameTemplateInfo(game.getGameTemplate());
						if (gameTemplate != null) {
							setting = gameTemplate.getSetting(res.getString("setting_key"));
						}
					}

					if (setting != null) {
						currRow.setSettingName(setting.getDescription());
					}
					
				} catch (ServiceException e) {
					log.warn("Unable to find game " + gameId, e);
				}

				currRow.setUsername(res.getString("username"));
				currRow.setDate(DbUtil.getTimestamp(res, "change_time"));
				currRow.setNewValue(res.getString("new_value"));
				currRow.setOldValue(res.getString("old_value"));

				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving game settings changes.",e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		if (dbConn == null){
			throw new RuntimeException("Out of database connections");
		}
		
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        try {
            	 String sql = "SELECT SUM(count_res) FROM"+ 
                 " (SELECT COUNT(*) AS count_res"+
                 " FROM game_settings_changes"+ 
                 " WHERE new_value IS DISTINCT FROM old_value"+
                 " UNION"+
                 " SELECT COUNT(*)"+
                 " FROM game_configs"+
                 " UNION"+
                 " SELECT COUNT(*)"+
                 " FROM game_config_changes)"+ 
                 " AS changes";
     
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);

            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting game settings changes count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        return count;
	}
}
