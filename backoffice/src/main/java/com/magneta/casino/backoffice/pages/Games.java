package com.magneta.casino.backoffice.pages;

import java.util.Locale;

import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.GamesGridDataSource;
import com.magneta.casino.internal.beans.ExtendedPrincipalBean;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.enums.GameCategoryEnum;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class Games {

	@Inject
	private GamesService gamesService;
	
	@Inject
	private ExtendedPrincipalBean principal;
	
	@Inject
	private Locale locale;
	
	@Property
	private GameBean game;
	
	public GamesGridDataSource getDataSource() {
		return new GamesGridDataSource(gamesService);
	}
	
	public String getGameCategory() {
		return GameCategoryEnum.valueOf(game.getCategoryId()).getName(locale);		
	}
	
	@Cached
	public boolean isCanEditGames() {
		return principal.hasPrivilege(PrivilegeEnum.GAME_CONFIG, true);
	}
}
