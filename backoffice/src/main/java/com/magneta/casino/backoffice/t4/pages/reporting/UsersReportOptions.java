package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class UsersReportOptions extends ReportOptions {
	
	@Component(id = "username",
			bindings = {"value=username", "disabled=false","displayName=message:Username"})
	public abstract TextField getUsernameField();

	@Component(id = "usernameLabel",
			bindings = {"field=component:username"})
	public abstract FieldLabel getUsernameLabel();
	
	@Component(id = "country",
			bindings = {"value=country","model=ognl:new com.magneta.administration.commons.models.CountrySelectionModel()","displayName=message:Country"})
	public abstract PropertySelection getCountrySelection();
	
	@Component(id = "countryLabel",
			bindings = {"field=component:country"})
	public abstract FieldLabel getCountryLabel();
	
	public abstract String getUsername();
	public abstract String getCountry();
	
	@Override
	public void fillReportParameters(Map<String,Object> reportParams) {
		super.fillReportParameters(reportParams);

		reportParams.put("username",getUsername());
		reportParams.put("country_val",getCountry());
	}
}
