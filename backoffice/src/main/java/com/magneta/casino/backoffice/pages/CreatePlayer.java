package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.CountrySelectModel;
import com.magneta.casino.backoffice.selectmodels.TimeZoneSelectModel;
import com.magneta.casino.backoffice.valueencoders.CountryEncoder;
import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.TimeZoneService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.CountryBean;
import com.magneta.casino.services.beans.UserRegistrationBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.DuplicateEmailException;
import com.magneta.casino.services.exceptions.DuplicateNicknameException;
import com.magneta.casino.services.exceptions.DuplicateUsernameException;
import com.magneta.casino.services.utils.PhoneUtils;

@Import(library = "context:js/Validators.js")
@RequiredPrivilege(PrivilegeEnum.ROLE)
public class CreatePlayer {

	@Component(id="createForm")
	private BeanEditForm createForm;
	
	@InjectService("UserRegistrationService")
	private UserRegistrationService registrationService;
	
	@InjectService("CountryService")
	private CountryService countryService;
	
	@InjectService("TimeZoneService")
	private TimeZoneService timeZoneService;

	@Inject
	private Messages messages;
	
	@Property
	private UserRegistrationBean usersDetailsBean;
	
	@Property
	private String password;
	
	@Property
	private String confirmPassword;
	
	@Property
	private CountryBean country;
	
	@OnEvent(value=EventConstants.PREPARE)
	public void prepareForm(){
		usersDetailsBean = new UserRegistrationBean();
	}
	
	public CountrySelectModel getCountrySelectModel(){
		return new CountrySelectModel(countryService);
	}
	
	public CountryEncoder getCountryEncoder(){
		return new CountryEncoder(countryService);
	}
	
	public TimeZoneSelectModel getTimeZoneSelectModel(){
		return new TimeZoneSelectModel(timeZoneService);
	}
	
	@OnEvent(value=EventConstants.VALIDATE,component="createForm")
	public void onValidateForm(){
		if(createForm.getHasErrors()) {
			return;
		}
		
		if(!password.equals(confirmPassword)){
			createForm.recordError(messages.get("not-equal"));
			return;
		}
		
		if(password.equalsIgnoreCase(usersDetailsBean.getUserName())){
			createForm.recordError(messages.get("pass-eq-username"));
			return;
		}
		
		if(PhoneUtils.isValidPhone(usersDetailsBean.getPhone())){
			usersDetailsBean.setPhone(PhoneUtils.normalizePhone(usersDetailsBean.getPhone()));
		}else{
			createForm.recordError(messages.get("invalid-phone"));
		}
	}
	
	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccessForm() throws ServiceException {
		usersDetailsBean.setCountryCode(country.getCode());
		usersDetailsBean.setPassword(password);
		usersDetailsBean.setUserType(UserTypeEnum.NORMAL_USER.getId());
		
		try {
			registrationService.registerUser(usersDetailsBean);
		} catch (DuplicateUsernameException e) {
			createForm.recordError(messages.get("username-exists"));
		} catch (DuplicateEmailException e) {
			createForm.recordError(messages.get("email-exists"));
		} catch (DuplicateNicknameException e) {
			createForm.recordError(messages.get("nickname-exists"));
		} catch (ServiceException e) {
				throw e;
		}
		return Index.class;
	}
	
	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm(){
		return Index.class;
	}
}
