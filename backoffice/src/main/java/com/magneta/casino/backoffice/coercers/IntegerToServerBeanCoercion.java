package com.magneta.casino.backoffice.coercers;

import org.apache.tapestry5.ioc.services.Coercion;

import com.magneta.casino.services.ServersService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.ServerBean;

public class IntegerToServerBeanCoercion implements Coercion<Integer, ServerBean> {

	private final ServersService serversService;
	
	public IntegerToServerBeanCoercion(ServersService serversService) {
		this.serversService = serversService;
	}
	
	@Override
	public ServerBean coerce(Integer serverId) {
		if (serverId == null) {
			return null;
		}
		
		try {
			return serversService.getServer(serverId);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

}
