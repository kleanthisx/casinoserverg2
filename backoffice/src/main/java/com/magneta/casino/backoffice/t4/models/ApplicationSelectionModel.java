package com.magneta.casino.backoffice.t4.models;

import java.util.ArrayList;
import java.util.List;
import org.apache.tapestry.form.IPropertySelectionModel;


public class ApplicationSelectionModel implements IPropertySelectionModel {

    public static final int ALL_GAMES_OPTION = 0;
       
    private List<String> application;
   
    
    public ApplicationSelectionModel(boolean noneOption, int showOption){
        application = new ArrayList<String>();
        if (noneOption)
               application.add("All");
        application.add("Partner Web-Application");
        application.add("Backoffice");        
        
    }
    
    @Override
	public Object getOption(int arg0) {
        return application.get(arg0);
    }

    @Override
	public int getOptionCount() {
        return application.size();
    }

    @Override
	public String getValue(int arg0) {
        return application.get(arg0).toString();
    }

    @Override
	public Object translateValue(String arg0) {
        return arg0;
    }
    
    @Override
	public boolean isDisabled(int arg0) {
        if (application.get(arg0).length()<0 ){
            return true;
        }
        return false;
    }

	@Override
	public String getLabel(int arg0) {
	    return application.get(arg0);
	}
}
