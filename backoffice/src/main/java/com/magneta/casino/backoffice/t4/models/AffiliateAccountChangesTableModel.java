package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.AffiliateAccountChangesBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class AffiliateAccountChangesTableModel implements IBasicTableModel {
	
	private Long userId;
	
	public AffiliateAccountChangesTableModel(Long userId){
		if(userId == null){
			this.userId = 0L;
		}else{
			this.userId = userId;
		}
	}
	
	@Override
	public Iterator<AffiliateAccountChangesBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<AffiliateAccountChangesBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumnName = "User";

        if (sortCol != null){
            sortColumnName = sortCol.getColumnName();
        }
        
        if (("Modifier").equals(sortColumnName))
            sortColumnName = "modifiers.username";
        else
            sortColumnName = "affiliate_changes.change_time";
		
		try {
			String sql = 
				" SELECT users.username, modifiers.username AS modifier,affiliate_changes.*"+
				" FROM affiliate_changes" +
				" INNER JOIN users ON users.user_id = affiliate_changes.affiliate_id" +
				" LEFT JOIN users AS modifiers ON modifiers.user_id = affiliate_changes.modifier_user"+
				(userId > 0? " WHERE affiliate_changes.affiliate_id = ?" : "")+
				" ORDER BY "+sortColumnName+(sortAsc? " ASC": " DESC")+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			if (userId > 0){
				statement.setLong(1, userId);
				statement.setInt(2, limit);
				statement.setInt(3, offset);
			} else {
				statement.setInt(1, limit);
				statement.setInt(2, offset);
			}

			res = statement.executeQuery();

			ArrayList<AffiliateAccountChangesBean> myArr = new ArrayList<AffiliateAccountChangesBean>();
			AffiliateAccountChangesBean currRow;

			while (res.next()){
				currRow = new AffiliateAccountChangesBean();
				
				currRow.setChanged(res.getString("field_name"));
				currRow.setModifier(res.getString("modifier"));
				currRow.setUser(res.getString("username"));
				currRow.setDate(DbUtil.getTimestamp(res, "change_time"));
				currRow.setOldValue(res.getString("old_value"));
				currRow.setNewValue(res.getString("new_value"));
								
				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving affiliate changes.",e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try {
            String sql = 
                "SELECT COUNT(*)"+
            	" FROM affiliate_changes" +
            	(userId > 0? " WHERE affiliate_changes.affiliate_id = ?" : "");
            statement = dbConn.prepareStatement(sql);
            
            if (userId > 0){
				statement.setLong(1, userId);
            }
            
            res = statement.executeQuery();
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting affiliate changes count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
