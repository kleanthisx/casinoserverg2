package com.magneta.casino.backoffice.t4.models;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.JackpotPropertiesChangeBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class JackpotPropertiesChangesTableModel implements IBasicTableModel {
	
	@Override
	public Iterator<JackpotPropertiesChangeBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<JackpotPropertiesChangeBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		String sortColumnName = "Username";

        if (sortCol != null){
            sortColumnName = sortCol.getColumnName();
        }
        
        if (("Username").equals(sortColumnName))
            sortColumnName = "users.username";
        else
            sortColumnName = "change_time";
		
		try {
			String sql = 
				" SELECT config_id, game_types.game_type_variant, corporates.username AS corporate, jackpot_config_id, new_pay_count, new_initial_amount, new_bet_contrib,"+
				" new_bet_amount, new_maximum_amount, new_draw_number, users.username, new_auto, old_pay_count, old_initial_amount, old_bet_contrib, old_bet_amount,"+
				" old_maximum_amount, old_draw_number, old_auto, change_time, new_min_payout, old_min_payout, new_min_jackpot_balance," +
				" old_min_jackpot_balance"+
				" FROM jackpot_properties_changes"+
				" INNER JOIN users ON jackpot_properties_changes.user_id = users.user_id"+
				" LEFT OUTER JOIN jackpots ON jackpot_properties_changes.jackpot_id = jackpots.jackpot_id"+
				" LEFT OUTER JOIN game_types ON jackpots.game_id = game_types.game_type_id"+
				" LEFT OUTER JOIN users AS corporates ON jackpots.target_corporate = corporates.user_id" +
				" ORDER BY "+sortColumnName+(sortAsc? " ASC": " DESC")+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			ArrayList<JackpotPropertiesChangeBean> myArr = new ArrayList<JackpotPropertiesChangeBean>();
			JackpotPropertiesChangeBean currRow;

			while (res.next()){
				currRow = new JackpotPropertiesChangeBean();
				
				currRow.setUsername(res.getString("username"));
				currRow.setCorporate(res.getString("corporate"));
				currRow.setGame(res.getString("game_type_variant"));
				currRow.setConfigID((Integer)res.getObject("config_id"));
				currRow.setDate(DbUtil.getTimestamp(res, "change_time"));
				currRow.setNewPayCount((Integer)res.getObject("new_pay_count"));
				currRow.setNewInitialAmount((BigDecimal)res.getObject("new_initial_amount"));
				currRow.setNewBetContrib((BigDecimal)res.getObject("new_bet_contrib"));
				currRow.setNewBetAmount((BigDecimal)res.getObject("new_bet_amount"));
				currRow.setNewMaximumAmount((BigDecimal)res.getObject("new_maximum_amount"));
				currRow.setNewDrawNumber((Integer)res.getObject("new_draw_number"));
				currRow.setNewAuto((Boolean)res.getObject("new_auto"));
				currRow.setOldPayCount((Integer)res.getObject("old_pay_count"));
				currRow.setOldInitialAmount((BigDecimal)res.getObject("old_initial_amount"));
				currRow.setOldBetContrib((BigDecimal)res.getObject("old_bet_contrib"));
				currRow.setOldBetAmount((BigDecimal)res.getObject("old_bet_amount"));
				currRow.setOldMaximumAmount((BigDecimal)res.getObject("old_maximum_amount"));
				currRow.setOldDrawNumber((Integer)res.getObject("old_draw_number"));
				currRow.setOldAuto((Boolean)res.getObject("old_auto"));
				currRow.setNewMinPayout((BigDecimal)res.getObject("new_min_payout"));
				currRow.setOldMinPayout((BigDecimal)res.getObject("old_min_payout"));
				currRow.setNewMinJackpotBal((BigDecimal)res.getObject("new_min_jackpot_balance"));
				currRow.setOldMinJackpotBal((BigDecimal)res.getObject("old_min_jackpot_balance"));
				
				myArr.add(currRow);
			}

			it = myArr.iterator();

		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving jackpot properties changes.", e);
		} finally {
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try {
            String sql = 
                "SELECT COUNT(*)"+
            	" FROM jackpot_properties_changes";
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting jackpot properties changes count.",e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
