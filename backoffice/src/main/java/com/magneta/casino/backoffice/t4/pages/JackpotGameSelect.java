package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.InjectObject;
import org.apache.tapestry.annotations.InjectPage;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.JackpotCandidateGamesSelectionModel;
import com.magneta.casino.backoffice.t4.models.JackpotCandidateGamesSelectionModel.JackpotGame;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class JackpotGameSelect extends SecurePage {
	
	private static final int JACKPOT_TYPE = 1;
	
	@InjectPage("CreateJackpot")
	public abstract CreateJackpot getCreateJackpotPage();
	
	@InjectObject("service:casino.common.Game")
	public abstract GamesService getGamesService();
	
	@Persist("client:page")
	public abstract int getJackpotType();
	public abstract void setJackpotType(int type);

	public abstract JackpotGame getGame();
	
	@Component(id="jackpotGameFrm",
			bindings={"success=listener:onSubmit","cancel=listener:onCancel"})
	public abstract Form getJackpotFameFrm();

	@Component(id="gameLabel",
			bindings={"field=component:game"})
	public abstract FieldLabel getGameLabel();
	
	@Component(id="game",
			bindings={"value=game","displayName=message:Game","model=ognl:jackpotCandidateGamesSelectionModel",
			"disabled=!allowWrite()"})
	public abstract PropertySelection getGameSelection();

	public JackpotCandidateGamesSelectionModel getJackpotCandidateGamesSelectionModel() throws ServiceException{
		return new JackpotCandidateGamesSelectionModel(getGamesService());
	}
	
	@Component(id="okSubmit",
			bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();
	
	@Component(id="cancelButton")
	public abstract Button getCancelButton();
	
	@Override
	public void pageValidate(PageEvent event) {
        super.pageValidate(event);
        if (!getAccessValidator().checkWriteAccess(this.getPageName())){
			redirectToPage("AccessDenied");
		} else if (getJackpotType() != JACKPOT_TYPE){
        	redirectToPage("JackpotTypeSelect");
        }
	}
	
	public void onSubmit(IRequestCycle cycle){
		CreateJackpot createJackpot = getCreateJackpotPage();
		if (getGame() != null){
			createJackpot.setGame(getGame().getGameID());
			createJackpot.setBetTypeJackpot(getGame().isJackpotBetType());
		}
		createJackpot.setJackpotType(JACKPOT_TYPE);
		cycle.activate(createJackpot);
	}
	
	public void onCancel(IRequestCycle cycle){
		redirectToPage("JackpotTypeSelect");
	}
}