package com.magneta.casino.backoffice.pages;

import java.util.Locale;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.selectmodels.GameCategorySelectModel;
import com.magneta.casino.backoffice.selectmodels.GameTemplateSelectModel;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class Game {
	
	@Inject
	private GamesService gameService;
	
	@Inject
	private Locale locale;
	
	@Property
	private Integer gameId;
	
	@Property
	private GameBean game;
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Integer gameId) {
		this.gameId = gameId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Integer onPassivate() {
		return this.gameId;
	}
	
	@OnEvent(value=EventConstants.PREPARE)
	public void onPrepareForm() throws ServiceException{
		if (gameId == null){
			game = new GameBean();
			game.setCategoryId(2);
		}else{
			game = gameService.getGame(gameId);
			
			if (game == null) {
				throw new ServiceException("Invalid gameId");
			}
		}
	}

	@OnEvent(value=EventConstants.SUCCESS)
	public Object onSuccess() throws ServiceException {
		if (this.gameId == null) {
			gameService.insertGame(game);
			this.gameId = game.getGameId();
		} else {
			gameService.updateGame(game);
		}
		return Games.class;
	}

	@OnEvent(value=EventConstants.CANCELED)
	public Object onCancelForm() {
		return Games.class;
	}
	
	public boolean isAllowIdEdit() {
		return gameId != null;
	}
	
	public GameTemplateSelectModel getGameTemplateSelectModel() {
		return new GameTemplateSelectModel();
	}
	
	public GameCategorySelectModel getGameCategorySelectModel() {
		return new GameCategorySelectModel(locale);
	}
}
