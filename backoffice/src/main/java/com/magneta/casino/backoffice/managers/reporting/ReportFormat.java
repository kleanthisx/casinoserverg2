package com.magneta.casino.backoffice.managers.reporting;

public enum ReportFormat {
	
	DOCX("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
	HTML("html", "text/html"),
	ODT("odt", "application/vnd.oasis.opendocument.text"),
	PDF("pdf", "application/pdf"),
	PPTX("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"),
	RTF("rtf", "application/rtf"),
	XLS("xls", "application/vnd.ms-excel");
	
	private final String extension;
	private final String contentType;
	
	ReportFormat(String extension, String contentType) {
		this.extension = extension;
		this.contentType = contentType;
	}
	
	public String getFileExtension() {
		return extension;
	}
	
	public String getContentType() {
		return contentType;
	}
}
