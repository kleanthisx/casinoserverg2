package com.magneta.casino.backoffice.pages;

import java.sql.SQLException;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.griddatasource.HostsWhitelistGridDataSource;
import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.HostWhitelistBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.ROLE)
public class HostsWhitelist {

	@InjectService("HostsWhitelistService")
	private HostsWhitelistService hostsWhitelistService;
	
	@Inject
	private Messages messages;
	
	@Property
	private HostWhitelistBean hostWhitelistbean;
	
	public GridDataSource getHostsWhitelistGridDataSource(){
		return new HostsWhitelistGridDataSource(hostsWhitelistService);
	}

	@OnEvent(value=EventConstants.ACTION, component="deleteMessage")
	public void deleteMessage(Long itemId) throws ServiceException, SQLException {
		HostWhitelistBean hostBean = hostsWhitelistService.getHost(itemId);
		hostsWhitelistService.deleteHost(hostBean);
	}
	
	public String getConfirmMessage(){
		return messages.format("confirm-message",hostWhitelistbean.getIp());
	}
}