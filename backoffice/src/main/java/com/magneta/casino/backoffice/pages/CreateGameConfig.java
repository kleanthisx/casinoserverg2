package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.bingo.BingoConfiguration;
import com.magneta.games.configuration.keno.KenoConfiguration;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.configuration.poker.videopoker.VideoPokerConfiguration;
import com.magneta.games.configuration.poker.videopoker.VideoPokerPlusConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class CreateGameConfig {
	
	@Inject
	private GameConfigsService gameConfigService;
	
	@Inject
	private Messages messages;
	
	@Inject
	private GamesService gameService;
	
	@Inject
	private PageRenderLinkSource linkSource;

	@Property
	private Integer gameId;

	private Long configId;

	@Property
	private GameConfigBean config;
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Integer gameId, Long configId) {
		this.gameId = gameId;
		this.configId = configId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Object[] onPassivate() {
		return new Object[]{gameId, configId};
	}
	
	@OnEvent(EventConstants.PREPARE)
	public void prepareForm() throws ServiceException {
		GameConfigBean origConfig = gameConfigService.getGameConfig(gameId, configId);
		
		GameConfigBean conf = new GameConfigBean();
		conf.setConfigComment(origConfig.getConfigComment());
		conf.setConfigValue(origConfig.getConfigValue());
		conf.setGameId(origConfig.getGameId());
		
		config = conf;
	}
	
	@OnEvent(EventConstants.SUBMIT)
	public Object onSubmit() throws ServiceException {
		gameConfigService.insertConfig(config);
		
		return linkSource.createPageRenderLinkWithContext(GameConfigs.class, gameId);
	}
	
	@Cached
	public String getPageTitle() throws ServiceException {
		return messages.format("page-title", gameService.getGame(gameId).getName());
	}
	
	@Cached
	public GameConfiguration getConfiguration() throws ServiceException {
		return gameConfigService.getFilteredConfiguration(gameId, configId);
	}
	
	@Inject
    private Block slot, videoPoker, islandStudPoker, texasHoldEmPoker,keno,bingo, other;

    public Block getConfigCase() throws ServiceException
    {
    	GameConfiguration configuration = getConfiguration();
    	
    	if (configuration instanceof SlotConfiguration) {
    		return slot;
    	} else if (configuration instanceof VideoPokerConfiguration
    		|| configuration instanceof VideoPokerPlusConfiguration) {
    		return videoPoker;
    	} else if (configuration instanceof IslandStudPokerConfiguration) {
    		return islandStudPoker;
    	} else if (configuration instanceof TexasHoldemConfiguration) {
    		return texasHoldEmPoker;
    	} else if (configuration instanceof KenoConfiguration) {
    		return keno;
    	}else if (configuration instanceof BingoConfiguration) {
    		return bingo;
    	} else {
    		return other;
    	}
    }
}
