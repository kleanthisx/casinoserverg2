package com.magneta.casino.backoffice.t4.callback;

import java.io.Serializable;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.RedirectException;
import org.apache.tapestry.callback.ICallback;

import com.magneta.casino.backoffice.util.LinkGenerator;

/**
 * Callback to a backoffice page. The page can be tapestry4 or 5
 * @author anarxia
 *
 */
public class CasinoPageCallback implements Serializable, ICallback {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3449464514770655376L;
	private final String pageName;
	private final Object[] parameters;
	
	public CasinoPageCallback(String pageName, Object[] parameters) {
		this.pageName = pageName;
		this.parameters = parameters;
	}
	
	public CasinoPageCallback(String pageName) {
		this.pageName = pageName;
		this.parameters = null;
	}

	@Override
	public void performCallback(IRequestCycle cycle) {
		String url = LinkGenerator.getPageUrl(pageName, parameters);
		throw new RedirectException(url);
	}
}
