package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.beans.JackpotPropertiesChangeBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.JackpotPropertiesChangesTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.TABLES)
public abstract class JackpotPropertiesChanges extends SecurePage {

	public abstract JackpotPropertiesChangeBean getCurrChange();
	
	@Component(id="logTable", type="contrib:Table",
			bindings={"source=ognl:new com.magneta.casino.backoffice.t4.models.JackpotPropertiesChangesTableModel()","columns=literal:!Date, !JackpotType, !Game, !Corporate, !Username, !PayCount, !InitialAmount, !BetContrib, !BetAmount, !MaximumAmount, !DrawNumber, !Auto, !MinPayout, !MinJackpotBal",
			"pageSize=50","initialSortColumn=literal:Date","row=currChange","initialSortOrder=false"})
	public abstract Table getLogTable();
	
	public JackpotPropertiesChangesTableModel getJackpotPropertiesChangesTableModel(){
		return new JackpotPropertiesChangesTableModel();
	}
	
	@Component(bindings={"value=formatDate(currChange.Date)"})
	public abstract Insert getDate();
	
	@Component(bindings={"value=getPayCountValue()",
			"renderTag=currChange.oldPayCount == currChange.newPayCount","style='display:none;'"})
	public abstract Insert getPayCount();
	
	public Object getPayCountValue(){
		return ((getCurrChange().getOldPayCount() != null? getCurrChange().getOldPayCount() : getMsg("Unlimited"))+" "+getMsg("ChangedTo")+' '+(getCurrChange().getNewPayCount() != null? getCurrChange().getNewPayCount() : getMsg("Unlimited")));
	}
	
	@Component(bindings={"value=formatAmount(currChange.oldInitialAmount)+' '+getMsg('ChangedTo')+' '+formatAmount(currChange.newInitialAmount)",
			"renderTag=currChange.oldInitialAmount == currChange.newInitialAmount","style='display:none;'"})
	public abstract Insert getInitialAmount();
	
	@Component(bindings={"value=formatAmount(currChange.oldBetContrib)+' '+getMsg('ChangedTo')+' '+formatAmount(currChange.newBetContrib)",
			"renderTag=currChange.oldBetContrib == currChange.newBetContrib","style='display:none;'"})
	public abstract Insert getBetContrib();
	
	@Component(bindings={"value=formatAmount(currChange.oldBetAmount)+' '+getMsg('ChangedTo')+' '+formatAmount(currChange.newBetAmount)",
			"renderTag=currChange.oldBetAmount == currChange.newBetAmount","style='display:none;'"})
	public abstract Insert getBetAmount();
	
	@Component(bindings={"value=formatAmount(currChange.oldMaximumAmount)+' '+getMsg('ChangedTo')+' '+formatAmount(currChange.newMaximumAmount)",
			"renderTag=currChange.oldMaximumAmount == currChange.newMaximumAmount","style='display:none;'"})
	public abstract Insert getMaximumAmount();
	
	@Component(bindings={"value=getDrawNumberValue()",
			"renderTag=currChange.oldDrawNumber == currChange.newDrawNumber","style='display:none;'"})
	public abstract Insert getDrawNumber();
	
	public Object getDrawNumberValue(){
		return (getCurrChange().getOldDrawNumber() != null? getCurrChange().getOldDrawNumber() : getMsg("null"))+" "+getMsg("ChangedTo")+' '+(getCurrChange().getNewDrawNumber() != null? getCurrChange().getNewDrawNumber() : getMsg("null"));
	}
	
	@Component(bindings={"value=getAutoValue()",
			"renderTag=currChange.oldAuto == currChange.newAuto","style='display:none;'"})
	public abstract Insert getAuto();
	
	public Object getAutoValue(){
		return (getCurrChange().getOldAuto() != null? getCurrChange().getOldAuto() : getMsg("Empty")) +" "+getMsg("ChangedTo")+' '+(getCurrChange().getNewAuto() != null? getCurrChange().getNewAuto() : getMsg("Empty"));
	}
	
	@Component(bindings={"value=formatAmount(currChange.oldMinPayout)+' '+getMsg('ChangedTo')+' '+formatAmount(currChange.newMinPayout)",
			"renderTag=currChange.oldMinPayout == currChange.newMinPayout","style='display:none;'"})
	public abstract Insert getMinPayout();
	
	@Component(bindings={"value=formatAmount(currChange.oldMinJackpotBal)+' '+getMsg('ChangedTo')+' '+formatAmount(currChange.newMinJackpotBal)",
			"renderTag=currChange.oldMinJackpotBal == currChange.newMinJackpotBal","style='display:none;'"})
	public abstract Insert getMinJackpotBal();
	
}
