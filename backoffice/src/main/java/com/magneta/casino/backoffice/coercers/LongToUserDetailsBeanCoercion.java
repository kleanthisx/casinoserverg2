package com.magneta.casino.backoffice.coercers;

import org.apache.tapestry5.ioc.services.Coercion;

import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.beans.UserDetailsBean;

public class LongToUserDetailsBeanCoercion implements Coercion<Long, UserDetailsBean> {

	private final UserDetailsService userDetailsService;
	
	public LongToUserDetailsBeanCoercion(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
	
	@Override
	public UserDetailsBean coerce(Long userId) {
		if (userId == null) {
			return null;
		}
		
		try {
			return userDetailsService.getUser(userId);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
}
