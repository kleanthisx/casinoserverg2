/**
 * 
 */
package com.magneta.casino.backoffice.t4.utils;

import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.casino.services.search.SortContext;


/**
 *
 */
public class TableSortContext<T> extends SortContext<T> {
    
    /**
     * @param expression
     */
    public TableSortContext(ITableColumn sortCol, boolean sortAsc) {
        super("");
        
        if (sortCol == null) {
            return;
        }
        
        String col = sortCol.getColumnName();
        
        this.expression = (sortAsc?"":"!") + col;
    }
}
