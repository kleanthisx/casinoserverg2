package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.IPropertySelectionModel;

import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class GamesSelectionModel implements IPropertySelectionModel {


    private List<Integer> gameIds;
    private List<String> gameVariants;

    public GamesSelectionModel() {
        this(false);
    }

    public GamesSelectionModel(boolean noneOption) {
        gameIds = new ArrayList<Integer>();
        gameVariants = new ArrayList<String>();

        Connection dbConn = ConnectionFactory.getReportsConnection();
        PreparedStatement statement = null;
        ResultSet res = null;

        if (dbConn == null){
            throw new RuntimeException("Out of database connections");
        }

        try {
            String sql = 
                "SELECT game_types.game_type_id,game_types.game_type_variant" +
                " FROM game_types" +
            	" ORDER BY game_types.game_type_variant";

            
            statement = dbConn.prepareStatement(sql);

            res = statement.executeQuery();

            if (noneOption){
                gameIds.add(-1);
                gameVariants.add("All");
            }

            while (res.next()){
                gameIds.add(res.getInt("game_type_id"));
                gameVariants.add(res.getString("game_type_variant"));
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while getting game categories.", e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
    }

    @Override
    public String getLabel(int arg0) {
        return gameVariants.get(arg0);
    }

    @Override
    public Object getOption(int arg0) {
        return gameIds.get(arg0);
    }

    @Override
    public int getOptionCount() {
        return gameIds.size();
    }

    @Override
    public String getValue(int arg0) {
        return gameIds.get(arg0).toString();
    }

    @Override
    public Object translateValue(String arg0) {
        return arg0;
    }

    @Override
    public boolean isDisabled(int arg0) {
        return false;
    }
}
