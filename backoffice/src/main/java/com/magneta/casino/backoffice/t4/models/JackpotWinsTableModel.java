package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.JackpotWinBean;
import com.magneta.casino.common.utils.FormatUtils;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class JackpotWinsTableModel implements IBasicTableModel {

	private TimeZone timeZone;
	private Locale locale;
	
	public JackpotWinsTableModel(TimeZone tz, Locale locale){
		this.timeZone = tz;
		this.locale = locale;
	}
	
	@Override
	public Iterator<JackpotWinBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;
		Iterator<JackpotWinBean> it = null;

		if (dbConn == null){
			return null;
		}
		
		try
		{   
			String sql = 
				"SELECT (CASE WHEN jackpots.game_id IS NULL AND jackpots.target_corporate IS NULL THEN 'Mega' ELSE"+
				" CASE WHEN jackpots.game_id IS NOT NULL THEN 'Game' ELSE"+
				" CASE WHEN jackpots.target_corporate IS NOT NULL THEN 'Corporate' ELSE '?' END END END) AS j_type,"+
				" jackpot_transactions.user_id, (-jackpot_transactions.amount) as win_amount," +
				" jackpot_transactions.transaction_time AT TIME ZONE 'UTC' as win_time,"+ 
				" jackpot_transactions.table_id, jackpot_transactions.round_id"+
				" FROM jackpot_transactions"+
				" INNER JOIN jackpots ON jackpot_transactions.jackpot_id = jackpots.jackpot_id"+
				" WHERE jackpot_transactions.win_id > 0"+
				" ORDER BY jackpot_transactions.transaction_time DESC"+
				" LIMIT ? OFFSET ?";

			statement = dbConn.prepareStatement(sql);
			statement.setInt(1, limit);
			statement.setInt(2, offset);

			res = statement.executeQuery();

			ArrayList<JackpotWinBean> myArr = new ArrayList<JackpotWinBean>();
			JackpotWinBean currRow;

			while (res.next()){
				currRow = new JackpotWinBean();
				currRow.setJackpotType(res.getString("j_type"));
				currRow.setUserId(res.getLong("user_id"));
				currRow.setAmount(res.getDouble("win_amount"));
				currRow.setTableId(res.getLong("table_id"));
				currRow.setRoundId(res.getInt("round_id"));
				currRow.setWinDate(FormatUtils.getFormattedDate(res.getTimestamp("win_time"), timeZone, locale));
				myArr.add(currRow);
			}

			it = myArr.iterator();

		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}

		return it;
	}

	@Override
	public int getRowCount() {
		Connection dbConn = ConnectionFactory.getReportsConnection();
        Statement statement = null;
        ResultSet res = null;
        int count = 0;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {
            String sql = 
                "SELECT COUNT(*)"+
            	" FROM jackpot_transactions"+
            	" INNER JOIN jackpots ON jackpot_transactions.jackpot_id = jackpots.jackpot_id"+
            	" WHERE jackpot_transactions.win_id > 0";
            statement = dbConn.createStatement();
            
            res = statement.executeQuery(sql);
            
            if (res.next()){
                count = res.getInt(1);
            }
            
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
	}
}
