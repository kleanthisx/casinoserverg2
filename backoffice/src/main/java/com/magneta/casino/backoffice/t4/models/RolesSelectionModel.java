/**
 * 
 */
package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.tapestry.form.IPropertySelectionModel;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class RolesSelectionModel implements IPropertySelectionModel {

    private List<Integer> roleIds;
    private List<String> roleDescs;
    
    public RolesSelectionModel(boolean anyOption){
        roleIds = new ArrayList<Integer>();
        roleDescs = new ArrayList<String>();
        
        Connection dbConn = ConnectionFactory.getReportsConnection();
        Statement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return;
		}

        if (anyOption){
        	roleIds.add(new Integer(-1));
        	roleDescs.add("All");
        } else {
        	roleIds.add(new Integer(-1));
        	roleDescs.add("--Please select--");
        }
        
        try {
            statement = dbConn.createStatement();

            String sql = 
                "SELECT role_id,role_desc"+
                " FROM roles" +
                " ORDER BY role_id";

            res = statement.executeQuery(sql);

            while (res.next()){
                roleIds.add(res.getInt("role_id"));
                roleDescs.add(res.getString("role_desc"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting roles list.", e);
        } finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
    }
    
    @Override
	public String getLabel(int arg0) {
        return roleDescs.get(arg0);
    }

    @Override
	public Object getOption(int arg0) {
        return roleIds.get(arg0);
    }

    @Override
	public int getOptionCount() {
        return roleIds.size();
    }

    @Override
	public String getValue(int arg0) {
        return roleIds.get(arg0).toString();
    }

    @Override
	public Object translateValue(String arg0) {
        return arg0;
    }
    
    @Override
	public boolean isDisabled(int arg0) {
		return false;
	}
}
