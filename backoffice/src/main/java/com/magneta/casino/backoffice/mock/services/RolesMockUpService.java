package com.magneta.casino.backoffice.mock.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.magneta.administration.beans.RoleBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;


public class RolesMockUpService {

	public List<RoleBean> getRoles(Long userId){
		Connection dbConn = ConnectionFactory.getReportsConnection();
		PreparedStatement statement = null;
		ResultSet res = null;

		if (dbConn == null){
			return null;
		}
		
		try
		{   
			String sql = 
					"SELECT roles.role_id, roles.role_desc"+
					" FROM roles"+
					" INNER JOIN user_roles ON user_roles.role_id = roles.role_id"+
					" AND user_roles.user_id = ?" +
			" ORDER BY role_desc";
			statement = dbConn.prepareStatement(sql);
			
			statement.setLong(1, userId);
			res = statement.executeQuery();

			List<RoleBean> roles = new ArrayList<RoleBean>();
			
			while (res.next()){
				RoleBean role = new RoleBean();
				role.setRoleDesc(res.getString("role_desc"));
				role.setRoleId(res.getInt("role_id"));
				roles.add(role);
			}
			
			return roles;
		}catch (SQLException e) {
			throw new RuntimeException("Error while retrieving latest completed transactions.",e);
		}
		finally{
			DbUtil.close(res);
			DbUtil.close(statement);
			DbUtil.close(dbConn);
		}
	}
}
