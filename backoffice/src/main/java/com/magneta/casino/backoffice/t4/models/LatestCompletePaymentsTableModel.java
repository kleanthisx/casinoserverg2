package com.magneta.casino.backoffice.t4.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;
import com.magneta.administration.beans.PaymentTransactionBean;
import com.magneta.administration.utils.DepositsWithdrawalsUtils;


public class LatestCompletePaymentsTableModel implements IBasicTableModel {
	
	@Override
	public Iterator<PaymentTransactionBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
		List<PaymentTransactionBean> completeDeposits = DepositsWithdrawalsUtils.getCompleteDeposits(offset, limit, sortCol, sortAsc);

		if(completeDeposits == null){
			return new ArrayList<PaymentTransactionBean>().iterator();
		}
		Iterator<PaymentTransactionBean> it = completeDeposits.iterator();	
		return it;
	}

	@Override
	public int getRowCount() {
		return DepositsWithdrawalsUtils.getCompleteDepositsCount();
	}
}
