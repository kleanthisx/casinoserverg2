package com.magneta.casino.backoffice.t4.components;

import org.apache.tapestry.AbstractComponent;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.InjectObject;

import com.magneta.casino.backoffice.util.LinkGenerator;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UsernameService;

public abstract class UserLink extends AbstractComponent {

	@InjectObject("service:casino.common.Username")
	public abstract UsernameService getUsernameService();
	
	public abstract Long getUserId();
	public abstract boolean isDisabled();
	public abstract String getTarget();

	@Override
	public void renderComponent(IMarkupWriter writer, IRequestCycle cycle) {
		boolean renderLink = !isDisabled();
		
		if (renderLink) {
			String url = LinkGenerator.getPageUrl("UserDetails", getUserId());

			writer.begin("a");
			writer.attribute("href", url);

			if (getTarget() != null) {
				writer.attribute("target", getTarget());
			}
			
			renderInformalParameters(writer, cycle);
		}

		try {
			String username = this.getUsernameService().getUsername(getUserId());
			writer.print(username);
		} catch (ServiceException e) {
			writer.print("(unknown)");
		}

		renderBody(writer, cycle);

		if (renderLink) {
			writer.end();
		}
	}
}
