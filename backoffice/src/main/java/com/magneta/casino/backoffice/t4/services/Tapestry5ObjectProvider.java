package com.magneta.casino.backoffice.t4.services;

import javax.servlet.ServletContext;

import org.apache.hivemind.Location;
import org.apache.hivemind.internal.Module;
import org.apache.hivemind.service.ObjectProvider;
import org.apache.tapestry5.TapestryFilter;
import org.apache.tapestry5.ioc.Registry;

/**
 * HiveMind service which provides objects from tapestry5-ioc.
 * 
 * @author anarxia
 *
 */
public class Tapestry5ObjectProvider implements ObjectProvider {
	
	private ServletContext context;

	public void setServletContext(ServletContext context) {
		this.context = context;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object provideObject(Module module, @SuppressWarnings("rawtypes") Class serviceInterface, String serviceId,
			Location location) {
		
		Registry registry = (Registry)context.getAttribute(TapestryFilter.REGISTRY_CONTEXT_NAME);

		return registry.getService(serviceId, serviceInterface);
	}

}
