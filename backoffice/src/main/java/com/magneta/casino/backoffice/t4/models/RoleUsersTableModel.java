package com.magneta.casino.backoffice.t4.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.tapestry.contrib.table.model.IBasicTableModel;
import org.apache.tapestry.contrib.table.model.ITableColumn;

import com.magneta.administration.beans.RoleUserBean;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

public class RoleUsersTableModel implements IBasicTableModel {
    
    private int roleId;
    
    public RoleUsersTableModel(int roleId){
        this.roleId = roleId;
    }

    @Override
	public Iterator<RoleUserBean> getCurrentPageRows(int offset, int limit, ITableColumn sortCol, boolean sortAsc) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Iterator<RoleUserBean> it = null;
        
        if (dbConn == null){
			return null;
		}
        
        try
        {   
            String sortColumn = sortCol.getColumnName();
            
            if (("Username").equals(sortColumn)){
                sortColumn = "username";
            } else if (("Email").equals(sortColumn)){
                sortColumn = "email";
            } else {
                sortColumn = null;
            }
                
            
            String sql = 
                "SELECT user_roles.user_id, users.username, users.email" +
                " FROM user_roles" +
                " JOIN users ON user_roles.user_id = users.user_id" +
                " WHERE role_id = ?";
            
                if (sortColumn != null){
                   sql += " ORDER BY "+sortColumn;
                   
                   if (sortAsc){
                       sql += " ASC";
                   } else {
                       sql += " DESC";
                   }
                }
                
                sql += " LIMIT ? OFFSET ?";
            
                statement = dbConn.prepareStatement(sql);
                statement.setInt(1, roleId);
                statement.setInt(2, limit);
                statement.setInt(3, offset);
                
                res = statement.executeQuery();
                
                ArrayList<RoleUserBean> myArr = new ArrayList<RoleUserBean>();
                RoleUserBean currRow;
                
                while (res.next()){
                    currRow = new RoleUserBean();
                    
                    currRow.setUserId(res.getLong("user_id"));
                    currRow.setUsername(res.getString("username"));
                    currRow.setEmail(res.getString("email"));
                    
                    myArr.add(currRow);
                }
               
                it = myArr.iterator();
                
        }catch (SQLException e) {
            throw new RuntimeException("Error while retrieving role users list.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return it;
    }

    @Override
	public int getRowCount() {
        int count = 0;
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        
        if (dbConn == null){
			return 0;
		}
        
        try
        {            
            String sql = 
                "SELECT count(*)"+
                " FROM user_roles"+
                " WHERE role_id = ?";
            
                statement = dbConn.prepareStatement(sql);
                statement.setInt(1, roleId);
                
                res = statement.executeQuery();
                
                if (res.next()){
                    count = res.getInt(1);
                }
                
        }catch (SQLException e) {
            throw new RuntimeException("Error while retrieving role users count.",e);
        }
        finally{
            DbUtil.close(res);
            DbUtil.close(statement);
            DbUtil.close(dbConn);
        }
        
        return count;
    }
    
}
