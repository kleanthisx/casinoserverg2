package com.magneta.casino.backoffice.t4.pages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.contrib.table.components.Table;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.link.ExternalLink;
import org.apache.tapestry.valid.ValidationDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magneta.administration.beans.AffiliateAccountChangesBean;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.backoffice.t4.models.AffiliateAccountChangesTableModel;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.db.ConnectionFactory;
import com.magneta.db.DbUtil;

@RequiredPrivilege(PrivilegeEnum.CORPORATE)
public abstract class AffiliateChanges extends SecurePage implements PageBeginRenderListener {
		
	private static final Logger log = LoggerFactory.getLogger(AffiliateChanges.class);
	
	@Bean
	public abstract ValidationDelegate getDelegate();
	
	public abstract Date getMaxDate();
	
	@Persist("client:page")
	public abstract void setUserId(Long userId);
	public abstract Long getUserId();

    public abstract void setUsername(String username);
    public abstract String getUsername();
	
    public abstract AffiliateAccountChangesBean getCurrChange();
    
    @Component(type="contrib:Table",
    		bindings={"source=ognl:affiliateAccountChangesTableModel","columns=literal:Date, Modifier, !Changed, !OldValue, !NewValue",
    		"pageSize=20","initialSortColumn=literal:Date","row=currChange","initialSortOrder=false"})
    public abstract Table getLogTable();
    
    public AffiliateAccountChangesTableModel getAffiliateAccountChangesTableModel(){
    	return new AffiliateAccountChangesTableModel(getUserId());
    }
    
    @Component(bindings={"value=formatDate(currChange.date)"})
    public abstract Insert getChangeDate();
    
    @Component(bindings={"listener=listener:onSubmit","delegate=beans.delegate","clientValidationEnabled=true"})
    public abstract Form getClearForm();
    
    @Component(bindings={"value=maxDate","translator=translator:date,pattern=dd/MM/yyyy","validators=validators:required","disabled=!allowWrite()"})
    public abstract DatePicker getMaxDateField();
    
    @Component(
			bindings={"page=literal:UserDetails","parameters={userId}"})
    public abstract ExternalLink getBackLink();
    
    @Component(bindings={"disabled=!allowWrite()"})
    public abstract Submit getOkSubmit();
    
    @Override
	public void pageBeginRender(PageEvent pEvt) {
    	setUsername(getUsername(getUserId()));
    }
    
    @Override
	public String getPageTitle() {
    	return new MessageFormat(getMsg("page-title")).format(new Object[]{getUsername()});
    }
    
	public void onSubmit(IRequestCycle cycle){
		if (getDelegate().getHasErrors()){
			return;
		}
		
		if (!this.getPrincipal().hasPrivilege(PrivilegeEnum.ROLE, true)) {
			getDelegate().record(null, "Access denied");
			return;
		}

		Date maxDate = normalizeDate(getMaxDate());
		int offset = TimeZone.getDefault().getOffset(maxDate.getTime());
		Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
		calendar.setTimeInMillis(maxDate.getTime()+offset);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.add(Calendar.MILLISECOND, 0);
		maxDate = new Date(calendar.getTimeInMillis());

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
    	try {
    		stmt = conn.prepareStatement(
    				"DELETE FROM affiliate_changes" +
    				" WHERE ((change_time AT TIME ZONE 'UTC') <= ?::timestamp)" +
    				" AND affiliate_id = ?");
    		
    		stmt.setTimestamp(1, new Timestamp(getMaxDate().getTime()));
    		stmt.setLong(2,getUserId());
    		
    		stmt.execute();
    		
    		redirectToPage("AffiliateChanges");
    	} catch (SQLException e){
    		getDelegate().record(getMsg("db-error"),null);
    		log.error("Error while getting user info.",e);
    	}
    	finally{
    		DbUtil.close(conn);
    	}
	}
	
	public String getAffRate() {
		return getMsg("affiliate-rate");
	}
	
	public String getPosition() {
		return getMsg("position");
	}
	
	public String getPaymentPeriod() {
		return getMsg("payment-period");
	}
	
	public String getCompany() {
		return getMsg("is-company");
	}
	
	public String getWebsite() {
		return getMsg("web-site");
	}
	
	public String getCreditLimit() {
		return getMsg("credit-limit");
	}
	
	public String getFreeCredits() {
		return getMsg("allow-free-credits");
	}
	
	public String getFullFreeCredits() {
		return getMsg("allow-full-free-credits");
	}
	
	public String getCreditsRate() {
		return getMsg("free-credits-rate");
	}
	
	public String getApproved() {
		return getMsg("approved");
	}
	
	public String getPaymentType() {
		return getMsg("payment-type");
	}
	
	public String getSignedContract() {
		return getMsg("contract-signed");
	}
	
	public String getCheque() {
		return getMsg("cheque-name");
	}
	
	public String getAffType() {
		return getMsg("affiliate-type");
	}
	
	public String getReqApproval() {
		return getMsg("require-approval");
	}
	
	public String getSubLevels() {
		return getMsg("sub-levels");
	}
	
	public String getReqSuperApproval() {
		return getMsg("requires-super-approval");
	}
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setUserId((Long)params[0]);
		}
		
		if (getUserId() == null) {
			throw new BadRequestException("Invalid/missing userId");
		}
	}
}
