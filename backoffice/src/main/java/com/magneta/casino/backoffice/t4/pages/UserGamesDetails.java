package com.magneta.casino.backoffice.t4.pages;


import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Bean;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.bean.EvenOdd;
import org.apache.tapestry.contrib.table.components.Table;

import com.magneta.administration.beans.UserGameBean;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.GAME_HISTORY)
public abstract class UserGamesDetails extends SecurePage  {
    
	@Persist("client:page")
    public abstract Long getUserID();
    public abstract void setUserID(Long userID);
    
    @Bean
    public abstract EvenOdd getEvenOdd();
    
    public abstract UserGameBean getCurrGame();
    
    @Component(type="contrib:Table",bindings={"source=ognl:new com.magneta.casino.backoffice.t4.models.LatestUserGamesTableModel(userID)","columns=literal:!Game, !Rounds, !Bets, !Wins, !RealBets, !RealWins, !JackpotWins",
    		"pageSize=50","rowsClass=ognl:beans.evenOdd.next","row=currGame"})
    public abstract Table getLastGames();
    
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null)  && (params.length > 0)){
			setUserID((Long)params[0]);
		}
	}
    
    public void onSubmit(IRequestCycle cycle){
    	if (!allowWrite()) {
    		return;
    	}
    }    
}