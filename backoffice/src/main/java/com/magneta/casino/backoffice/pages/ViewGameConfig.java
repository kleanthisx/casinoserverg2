package com.magneta.casino.backoffice.pages;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ConditionalUpdateException;
import com.magneta.casino.services.GameConfigsService;
import com.magneta.casino.services.GamesService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.GameBean;
import com.magneta.casino.services.beans.GameConfigBean;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.games.configuration.GameConfiguration;
import com.magneta.games.configuration.bingo.BingoConfiguration;
import com.magneta.games.configuration.keno.KenoConfiguration;
import com.magneta.games.configuration.poker.islandstudpoker.IslandStudPokerConfiguration;
import com.magneta.games.configuration.poker.texasholdem.TexasHoldemConfiguration;
import com.magneta.games.configuration.poker.videopoker.VideoPokerConfiguration;
import com.magneta.games.configuration.poker.videopoker.VideoPokerPlusConfiguration;
import com.magneta.games.configuration.slots.SlotConfiguration;

@RequiredPrivilege(PrivilegeEnum.GAME_CONFIG)
public class ViewGameConfig {

	@Inject
	private GameConfigsService gameConfigService;
	
	@Inject
	private GamesService gameService;
	
	@Inject
	private Messages messages;
	
	@Property
	private Integer gameId;
	
	@Property
	private Long configId;
	
	@Property
	private GameConfigBean config;
	
	@Cached
	public GameConfiguration getConfiguration() throws ServiceException {
		return gameConfigService.getFilteredConfiguration(gameId, configId);
	}
	
	public String getPageTitle() throws ServiceException {
		return messages.format("page-title", getGame().getName(), configId);
	}
	
	@Cached
	public GameBean getGame() throws ServiceException {
		return gameService.getGame(gameId);
	}
	
	@OnEvent(EventConstants.ACTIVATE)
	public void onActivate(Integer gameId, Long configId) {
		this.gameId = gameId;
		this.configId = configId;
	}
	
	@OnEvent(EventConstants.PASSIVATE)
	public Object[] onPassivate() {
		return new Object[]{gameId, configId};
	}
	
	@OnEvent(EventConstants.PREPARE)
	public void prepareForm() throws ServiceException {
		this.config = gameConfigService.getGameConfig(gameId, configId);
	}
	
	@OnEvent(value=EventConstants.SUBMIT, component="update")
	public void onSubmit() throws ServiceException, ConditionalUpdateException {
		this.gameConfigService.updateConfig(config);
	}
	
	@Inject
    private Block slot, videoPoker, islandStudPoker, texasHoldEmPoker, keno ,bingo, other;

    public Block getConfigCase() throws ServiceException {
    	GameConfiguration configuration = getConfiguration();
    	
    	if (configuration instanceof SlotConfiguration) {
    		return slot;
    	} else if (configuration instanceof VideoPokerConfiguration
    		|| configuration instanceof VideoPokerPlusConfiguration) {
    		return videoPoker;
    	} else if (configuration instanceof IslandStudPokerConfiguration) {
    		return islandStudPoker;
    	} else if (configuration instanceof TexasHoldemConfiguration) {
    		return texasHoldEmPoker;
    	} else if (configuration instanceof KenoConfiguration) {
    		return keno;
    	} else if (configuration instanceof BingoConfiguration) {
    		return bingo;
    	} else {
    		return other;
    	}
    }
}
