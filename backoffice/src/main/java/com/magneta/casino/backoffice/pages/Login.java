package com.magneta.casino.backoffice.pages;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;

import com.magneta.casino.backoffice.annotations.DisplayMenu;
import com.magneta.casino.internal.services.PrincipalService;
import com.magneta.casino.services.HostsWhitelistService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.SettingsService;
import com.magneta.casino.services.UserAuthenticationService;
import com.magneta.casino.services.enums.PrivilegeEnum;
import com.magneta.casino.services.enums.UserTypeEnum;
import com.magneta.casino.services.exceptions.InvalidAuthenticationCredentialsException;

@PermitAll
@DisplayMenu(false)
public class Login {
	
	@Inject
	private Logger log;
	
	@InjectService("UserAuthenticationService")
	private UserAuthenticationService authenticationService;
	
	@InjectService("SettingsService")
	private SettingsService settingsService;
	
	@InjectService("BackofficePrincipalService")
	private PrincipalService principalService;
	
	@InjectService("HostsWhitelistService")
	private HostsWhitelistService hostsWhitelistService;
	
	@Inject
	private Messages messages;
	
	@Inject
	private HttpServletRequest request;
	
	@Inject
	private Response response;
	
	@InjectComponent
	private Form loginForm;
		
	@Property
    private String userName;

	@Property
    private String password;
	
	private String redirectUrl;
	
	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	@OnEvent(value=EventConstants.PASSIVATE)
	public String passivate() {
		return redirectUrl;
	}
	
	@OnEvent(value=EventConstants.VALIDATE, component="loginForm")
	public void validateForm() {
		if(loginForm.getHasErrors()) {
			return;
		}
		
		authenticationService.logoutUser();
		
		try {
			if (settingsService.getSettingValue("system.backoffice.hosts_whitelist_enabled", Boolean.TYPE) && !checkWhiteList()) {
				loginForm.recordError(messages.get("access-denied"));
				return;
			}

			authenticationService.authenticateUser(userName, password, new UserTypeEnum[] {UserTypeEnum.STAFF_USER}, PrivilegeEnum.BACKOFFICE_ENTRY);
		} catch (InvalidAuthenticationCredentialsException e) {
			loginForm.recordError(messages.get("invalid-username-password"));
		} catch (ServiceException e) {
			log.error("login error", e);
			loginForm.recordError(messages.get("unknown-error"));
		}
	}
	
	private boolean checkWhiteList() throws ServiceException {
		boolean result = false;
		
		principalService.pushSystem();
		try{
			result = hostsWhitelistService.isHost(request.getRemoteAddr());
		} finally {
			principalService.popSystem();
		}
		return result;
	}
	
	@OnEvent(value=EventConstants.SUCCESS, component="loginForm")
	public Object successForm() throws IOException {
		if (redirectUrl == null) {
			return Index.class;
		}

		response.sendRedirect(redirectUrl);
		return null;
	}
}
