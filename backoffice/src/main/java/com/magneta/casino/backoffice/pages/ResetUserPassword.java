package com.magneta.casino.backoffice.pages;

import java.util.Locale;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.services.PageRenderLinkSource;

import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.UserDetailsService;
import com.magneta.casino.services.UserRegistrationService;
import com.magneta.casino.services.beans.UserDetailsBean;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(or={PrivilegeEnum.PROFILES,PrivilegeEnum.ROLE})
public class ResetUserPassword {

	@Component(id="resetForm")
	private Form resetForm;

	@InjectService("UserDetailsService")
	private UserDetailsService userDetailsService;

	@InjectService("UserRegistrationService")
	private UserRegistrationService registrationService;
	
	@Inject
	private PageRenderLinkSource linkSource;
	
	@Inject
	private Messages messages;

	@Inject
	private Locale locale;
	
	@Property
	private UserDetailsBean userDetailsBean;

	@Property
	private Long userId;

	@Property
	private String newPassword;

	@Property
	private String confirmNewPassword;

	@OnEvent(value=EventConstants.ACTIVATE)
	public void activate(Long userId) {
		this.userId = userId;
	}

	@OnEvent(value=EventConstants.PASSIVATE)
	public Long passivate() {
		return userId;
	}

	@OnEvent(value = EventConstants.PREPARE)
	public void onPrepare() throws ServiceException{
		userDetailsBean= userDetailsService.getUser(userId);
	}

	@OnEvent(value = EventConstants.VALIDATE,component="resetForm")
	public void onValidate(){
		if(resetForm.getHasErrors()) {
			return;
		}
		
		if(newPassword == null || newPassword.trim().equals("")){
			resetForm.recordError(messages.get("empty-pass"));
			return;
		}

		if(newPassword.length() < 6 || confirmNewPassword.length() < 6){
			resetForm.recordError(messages.get("pass-size-error"));
			return;
		}
		
		if(!newPassword.equals(confirmNewPassword)){
			resetForm.recordError(messages.get("not-equal"));
			return;
		}

		if(newPassword.equalsIgnoreCase(userDetailsBean.getUserName())){
			resetForm.recordError(messages.get("pass-eq-username"));
			return;
		}
	}

	@OnEvent(value = EventConstants.SUCCESS)
	public Object onSuccess() throws ServiceException{
		registrationService.updatePassword(userId, newPassword);
		
		return linkSource.createPageRenderLinkWithContext(UserDetails.class, userId);
	}
}

