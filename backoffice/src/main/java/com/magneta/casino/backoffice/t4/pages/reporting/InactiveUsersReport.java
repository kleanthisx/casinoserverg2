package com.magneta.casino.backoffice.t4.pages.reporting;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.form.Checkbox;
import org.apache.tapestry.form.DatePicker;
import org.apache.tapestry.form.PropertySelection;
import org.apache.tapestry.form.TextField;
import org.apache.tapestry.valid.FieldLabel;

import com.magneta.administration.commons.models.CountrySelectionModel;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.REPORTS)
public abstract class InactiveUsersReport extends DateTimeReportOptions {

		@Component(id = "minBalance",
				bindings = {"value=minBalance", "disabled=false","displayName=message:minBalance"})
		public abstract TextField getMinBalanceField();

		@Component(id = "minBalanceLabel",
				bindings = {"field=component:minBalance"})
		public abstract FieldLabel getMinBalanceLabel();
		
		@Component(id = "minCredits",
				bindings = {"value=minCredits", "disabled=false","displayName=message:minCredits"})
		public abstract TextField getMinCreditsField();

		@Component(id = "minCreditsLabel",
				bindings = {"field=component:minCredits"})
		public abstract FieldLabel getMinCreditsLabel();
		
		@Component(id = "country",
				bindings = {"value=country","model=ognl:countrySelectionModel", 
				"displayName=message:Country"})
		public abstract PropertySelection getCountrySelection();

		public CountrySelectionModel getCountrySelectionModel() throws ServiceException{
			return new CountrySelectionModel();
		}
		
		@Component(id = "countryLabel",
				bindings = {"field=component:country"})
		public abstract FieldLabel getCountryLabel();
			
		@Component(id = "toDateLogin",
				bindings={"value=maxDateLogin","displayName=message:To", "translator=translator:date,pattern=dd/MM/yyyy"})
		public abstract DatePicker getToDateLogin();
						
		@Component(id = "toDateLoginLabel",
				bindings={"field=component:toDateLogin"})
		public abstract FieldLabel getToDateLoginLabel();
			
		@Component(id = "periodLoginMsg",
				bindings = {"value=message:PeriodLogin"})
		public abstract Insert getPeriodLoginMsg();
		
		@Component(id = "periodRegMsg",
				bindings = {"value=message:PeriodReg"})
		public abstract Insert getPeriodRegMsg();
		
		@Component(id = "neverLoginCB",
				bindings={"value=neverLogin", "displayName=message:neverLogin"})
		public abstract Checkbox getNeverLoginCB();
		
		@Component(id = "neverLoginCBLabel",
				bindings={"field=component:neverLoginCB"})
		public abstract FieldLabel getNeverLoginCBLabel();
		
		public abstract boolean getNeverLogin();
				
		public abstract Date getMaxDateLogin();
		
		public abstract int getMinBalance();
		public abstract int getMinCredits();
	
		public abstract String getCountry(); 
		
		@Override
		public void fillReportParameters(Map<String,Object> reportParams) {
			super.fillReportParameters(reportParams);
	
			if (getMinBalance()!=0)
				reportParams.put("min_balance", getMinBalance());
			if (getMinCredits()!=0)
				reportParams.put("min_credits", getMinCredits());
			if (getCountry() != null)
				reportParams.put("country", getCountry());
			if (getNeverLogin()== true)
				reportParams.put("never_log", getNeverLogin());
			
			Date maxDateLogin = normalizeDate(getMaxDateLogin());
	        
			//NORMALIZE DATES
	        
	        if (maxDateLogin != null){
	        	Calendar calendar = Calendar.getInstance(getTZ(), getLocale());
	        	
	        	Date maxTime = normalizeDate(getMaxTime());
	        	
	        	if (maxTime != null){
	        		calendar.setTimeInMillis(maxTime.getTime());
	            	
	        		int hour = calendar.get(Calendar.HOUR_OF_DAY);
	        		int minute = calendar.get(Calendar.MINUTE);
	        		
	        		calendar.setTimeInMillis(maxDateLogin.getTime());
	        		calendar.set(Calendar.HOUR_OF_DAY, hour);
	            	calendar.set(Calendar.MINUTE, minute);
	        	} else {
	        		calendar.setTimeInMillis(maxDateLogin.getTime());
	        		calendar.set(Calendar.HOUR_OF_DAY, 24);
	        		calendar.set(Calendar.MINUTE, 0);
	        		calendar.add(Calendar.MILLISECOND, -1);
	        	}
	        	
	            if (getNeverLogin()== false)
	            	reportParams.put("max_login_date", calendar.getTime());
	        }
		}
}
