package com.magneta.casino.backoffice.t4.pages;

import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.annotations.Component;
import org.apache.tapestry.annotations.Persist;
import org.apache.tapestry.callback.ICallback;
import org.apache.tapestry.components.Insert;
import org.apache.tapestry.event.PageBeginRenderListener;
import org.apache.tapestry.event.PageEvent;
import org.apache.tapestry.form.Button;
import org.apache.tapestry.form.Form;
import org.apache.tapestry.form.Hidden;
import org.apache.tapestry.form.Submit;
import org.apache.tapestry.form.TextArea;
import org.apache.tapestry.form.TextField;

import com.magneta.administration.beans.WireTransferBean;
import com.magneta.administration.exceptions.BadRequestException;
import com.magneta.administration.utils.DepositsWithdrawalsUtils;
import com.magneta.casino.backoffice.annotations.RequiredPrivilege;
import com.magneta.casino.services.enums.PrivilegeEnum;

@RequiredPrivilege(PrivilegeEnum.FUNDS)
public abstract class EditWireTransferPayout extends SecurePage implements PageBeginRenderListener {

	@Persist("client")
	public abstract ICallback getCallback();
	public abstract void setCallback(ICallback callback);

	@Persist("client:page")
	public abstract Long getTransactionId();
	public abstract void setTransactionId(Long id);

	public abstract String getUsername();
	public abstract void setUsername(String username);

	public abstract double getBalance();
	public abstract void setBalance(double balance);

	public abstract double getAmount();
	public abstract void setAmount(double amount);

	public abstract String getAccountName();
	public abstract void setAccountName(String accName);

	public abstract String getAccountNo();
	public abstract void setAccountNo(String accNo);

	public abstract String getComment();
	public abstract void setComment(String comment);
	
	@Component(bindings={"listener=listener:onSubmit","cancel=listener:onCancel","clientValidationEnabled=true"})
	public abstract Form getEditForm();

	@Component(bindings={"value=accountName","disabled=!allowWrite()","displayName=message:NameOnAccount","validators=validators:maxLength=256"})
	public abstract TextField getChequeName();

	@Component(bindings={"value=accountNo","disabled=!allowWrite()","displayName=message:AccountNo","validators=validators:maxLength=128"})
	public abstract TextField getChequeNo();

	@Component(id="comment",bindings={"value=comment","disabled=!allowWrite()","displayName=message:Comment","validators=validators:maxLength=255"})
	public abstract TextArea getCommentText();

	@Component(id="transactionId",bindings={"value=transactionId"})
	public abstract Hidden getTransactionIdHidden();

	@Component(bindings={"disabled=!allowWrite()"})
	public abstract Submit getOkSubmit();

	@Component
	public abstract Button getCancelButton();

	@Component(id="username",bindings={"value=username"})
	public abstract Insert getUsernameInsert();

	@Component(id="balance",bindings={"value=formatAmount(balance)"})
	public abstract Insert getBalanceInsert();
	
	@Override
	public void activateExternalPage(Object[] params, IRequestCycle cycle) {
		super.activateExternalPage(params, cycle);
		if ((params != null) && (params.length > 0)) {
			setTransactionId((Long)params[0]);
		}
	}
	
	@Override
	public void pageBeginRender(PageEvent pEvt){
		if (getTransactionId() == null) {
			throw new BadRequestException("Invalid/missing transactionId");
		}
		
		WireTransferBean currentTransaction = DepositsWithdrawalsUtils.getWireTransferWithdrawal(getTransactionId());

		setUsername(currentTransaction.getUsername());
		setBalance(currentTransaction.getBalance());
		setAmount(currentTransaction.getAmount());
		setAccountName(currentTransaction.getAccountName());
		setAccountNo(currentTransaction.getAccountNo());
		setComment(currentTransaction.getComment());
	}


	public void onSubmit(IRequestCycle cycle){

		DepositsWithdrawalsUtils.updateWireTransferWithdrawal(getComment(), getAccountName(), getAccountNo(), getTransactionId());
		

		if(getCallback() == null) {
			redirectToPage("PayoutsCompleted");
		} else {
			getCallback().performCallback(cycle);
		}
	}

	public void onCancel(IRequestCycle cycle){
		if(getCallback() == null) {
			redirectToPage("PayoutsCompleted");
		} else {
			getCallback().performCallback(cycle);
		}
	}
}
