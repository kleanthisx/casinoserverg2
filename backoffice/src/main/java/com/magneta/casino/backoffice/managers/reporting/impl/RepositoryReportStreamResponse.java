package com.magneta.casino.backoffice.managers.reporting.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.services.Response;

import com.magneta.casino.backoffice.managers.reporting.RepositoryReport;

public class RepositoryReportStreamResponse implements StreamResponse {

	private final RepositoryReport report;
	private File outFile;
	
	public RepositoryReportStreamResponse(RepositoryReport report) {
		this.report = report;
	}

	@Override
	public String getContentType() {
		return report.getFormat().getContentType();
	}

	@Override
	public InputStream getStream() throws IOException {
		if (outFile == null) {
			throw new IOException("Report export file not found");
		}
		
		return new DeleteOnCloseFileInputStream(outFile);
	}

	@Override
	public void prepareResponse(Response response) {
		try {
			outFile = File.createTempFile("REPORT", report.getFormat().getFileExtension());
		} catch (IOException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to create temporary file for report");
			} catch (IOException e1) {
			}
			return;
		}
		
		FileOutputStream os;
		try {
			os = new FileOutputStream(outFile);
		} catch (FileNotFoundException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to create temporary file for report");
			} catch (IOException e1) {
			}
			return;
		}
		try {
			URL url = report.getFuture().get();
			
			InputStream is = url.openStream();
			
			byte[] buf = new byte[4096];
			int avail;
			while (is.available() > 0) {
				avail = is.read(buf);
				os.write(buf, 0, avail);
			}
		} catch (IOException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to export report: " + e.getMessage());
			} catch (IOException e1) {
			}
			return;
		} catch (InterruptedException e) {
			Thread.interrupted();
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to export report: " + e.getMessage());
			} catch (IOException e1) {
			}
			try {
				os.close();
			} catch (IOException e1) {
			}
			return;
		} catch (ExecutionException e) {
			Throwable realE = e.getCause();
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to export report: " + realE.getMessage());
			} catch (IOException e1) {
			}
			
			try {
				os.close();
			} catch (IOException e1) {
			}
			return;
		}
		
		try {
			os.flush();
			os.close();
		} catch (IOException e) {
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Unable to save exported report: " + e.getMessage());
			} catch (IOException e1) {
			}
			return;
		}
		
		response.setHeader("Content-Disposition","inline; filename=\""+report.getFilename() + "\"");
		response.setHeader("Content-Type", getContentType()+";charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength((int)outFile.length());
	}

	
	private static class DeleteOnCloseFileInputStream extends FileInputStream {
		private File file;

		public DeleteOnCloseFileInputStream(File file) throws FileNotFoundException {
			super(file);
			this.file = file;
		}

		@Override
		public void close() throws IOException {
			try {
				super.close();
			} finally {
				if(file != null) {
					file.delete();
					file = null;
				}
			}
		}
	}
}
