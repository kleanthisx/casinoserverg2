package com.magneta.casino.backoffice.valueencoders;

import org.apache.tapestry5.ValueEncoder;

import com.magneta.casino.services.CountryService;
import com.magneta.casino.services.ServiceException;
import com.magneta.casino.services.beans.CountryBean;

public class CountryEncoder implements ValueEncoder<CountryBean>{

	private final CountryService countryService;
	
	public CountryEncoder(CountryService countryService){
		this.countryService = countryService;
	}
	
	@Override
	public String toClient(CountryBean value) {
		return value.getCode();
	}

	@Override
	public CountryBean toValue(String clientValue) {
		try {
			return countryService.getCountry(clientValue);
		} catch (ServiceException e) {
			throw new RuntimeException("Country not found");
		}
	}
}
