function getJackpots(){
	var jackpotIds = new Array();
	var i =-1;
				    		
	for(i=-1;;i++) { 
		var currJpotId = null;
				    			
		if (i == -1){ 
			currJpotId = document.getElementById("jackpotID");
		} else {
			currJpotId = document.getElementById("jackpotID_"+i);
		}
				    			
		if (null != currJpotId){
			jackpotIds[i+1] = currJpotId.value;
		} else {
			break;
		}
				    			
	 }
				
	requestJackpots(jackpotIds);
}
				
function getRefreshedJackpots(jackpots) {
	var i =-1
				    		
	for(i=-1;;i++) { 
		var currAmt = null;
		var currTimes = null;
				    	
		if (i == -1){ 
			currAmt = document.getElementById("jackpotAmount");
			currTimes = document.getElementById("paidTimes");
		} else {
			currAmt = document.getElementById("jackpotAmount_"+i);
			currTimes = document.getElementById("paidTimes_"+i);
		}
				    			
		if (currAmt != null){
			var data = jackpots[i+1].split("_",2);
				    	
			currAmt.value = data[0];
			currTimes.value = data[1];
		} else {
			break;
		}
	 }
				    
 setTimeout("getJackpots()", 5000);
}
