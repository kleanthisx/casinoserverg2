function init(){
	var cb = document.getElementById("allTypesCB");
	cb.style.display="block";
}
			    
function cbChecked(){
					
	var cb = document.getElementById("allTypesCB");
	var checkAll = false;
			    	
	if (cb.checked){
		checkAll = true;
	}

	var t = document.getElementById("userTypesTable");
	var cbs = t.getElementsByTagName("input");
	var i=0;
	
	for (i=0;i<cbs.length;i++) {
		cbs[i].checked = checkAll;
	}
}
				
function cbUnchecked(cb) {
					
	if (!cb.checked) {
		var allNone = document.getElementById("allTypesCB");
		allNone.checked = false;
	}
}
